using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyProduct("Arcanos")]
[assembly: AssemblyCompany("ArcanosTeam")]
[assembly: AssemblyCopyright("Copyright � ArcanosTeam 2013")]
[assembly: AssemblyVersion("1.0.2523.1305")]
[assembly: AssemblyFileVersion("1.0.2523.1305")]
#if DEBUG
[assembly: AssemblyConfiguration("Debug")]
#else
[assembly: AssemblyConfiguration("Release")]
#endif
[assembly: ComVisible(false)]