﻿using System.Collections.Generic;
using System.IO;
using Arcanos.Client.GameClient.Rendering;
using Arcanos.Shared.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ObjLoader.Loader.Data.Elements;
using ObjLoader.Loader.Data.VertexData;
using ObjLoader.Loader.Loaders;

namespace Arcanos.Client.GameClient.Graphics
{
    public static class ModelHelper
    {
        private class MatrialStreamRedirection : IMaterialStreamProvider
        {
            private readonly string filename;

            public MatrialStreamRedirection(string modelFilePath)
            {
                filename = modelFilePath;
            }

            public Stream Open(string materialFilePath)
            {
                return File.OpenRead(Path.Combine(Path.GetDirectoryName(filename), materialFilePath));
            }
        }
        private static readonly ObjLoaderFactory factory = new ObjLoaderFactory();
        private static readonly Dictionary<WorldModel, ModelRenderGeometry> modelCache = new Dictionary<WorldModel, ModelRenderGeometry>();
        public static IEnumerable<ModelRenderGeometry> GetModelRenderContext(GraphicsDevice device, WorldModelInstance instance)
        {
            return GetModelRenderContext(device, instance == null ? null : instance.Model as WorldModel);
        }
        public static IEnumerable<ModelRenderGeometry> GetModelRenderContext(GraphicsDevice device, WorldModel model)
        {
            if (model == null)
                yield break;

            ModelRenderGeometry geometry;
            if (modelCache.TryGetValue(model, out geometry))
            {
                yield return geometry;
                yield break;
            }

            LoadResult result;
            using (FileStream stream = new FileStream(model.Filename, FileMode.Open, FileAccess.Read))
                result = factory.Create(new MatrialStreamRedirection(model.Filename)).Load(stream);

            int faceCount = 0;
            foreach (Group group in result.Groups)
                faceCount += group.Faces.Count;

            geometry = new ModelRenderGeometry(faceCount);
            bool swapYZ = (model.Flags & WorldModelFlags.SwapVertexYZ) != 0;
            bool flipV = (model.Flags & WorldModelFlags.FlipTextureV) != 0;
            foreach (Group group in result.Groups)
                foreach (Face face in group.Faces)
                    geometry.Add(model.Materials[result.Materials.IndexOf(group.Material)],
                        result.GetVertex(face[0].VertexIndex, swapYZ),
                        result.GetVertex(face[1].VertexIndex, swapYZ),
                        result.GetVertex(face[2].VertexIndex, swapYZ),
                        result.GetNormal(face[0].NormalIndex, swapYZ),
                        result.GetNormal(face[1].NormalIndex, swapYZ),
                        result.GetNormal(face[2].NormalIndex, swapYZ),
                        result.GetTexture(face[0].TextureIndex, flipV),
                        result.GetTexture(face[1].TextureIndex, flipV),
                        result.GetTexture(face[2].TextureIndex, flipV));
            modelCache.Add(model, geometry);
            geometry.UpdateBuffer(device);
            yield return geometry;
        }

        private static Vector3 GetVertex(this LoadResult result, int index, bool swapYZ)
        {
            Vertex vertex = result.Vertices[index < 0 ? result.Vertices.Count + index : index - 1];
            return new Vector3(vertex.X, swapYZ ? vertex.Z : vertex.Y, swapYZ ? vertex.Y : vertex.Z);
        }
        private static Vector3 GetNormal(this LoadResult result, int index, bool swapYZ)
        {
            Normal normal = result.Normals[index < 0 ? result.Normals.Count + index : index - 1];
            return new Vector3(normal.X, swapYZ ? normal.Z : normal.Y, swapYZ ? normal.Y : normal.Z);
        }
        private static Vector2 GetTexture(this LoadResult result, int index, bool flipV)
        {
            ObjLoader.Loader.Data.VertexData.Texture texture = result.Textures[index < 0 ? result.Textures.Count + index : index - 1];
            return new Vector2(texture.X, flipV ? 1 - texture.Y : texture.Y);
        }
    }
}