﻿using Arcanos.Shared.Items;
using Arcanos.Shared.Objects;

namespace Arcanos.Client.GameClient.Items
{
    public class Loot : LootBase
    {
        public int Money;
        public ItemDefinition[] Items;
        public bool AllItemsLoaded
        {
            get
            {
                if (Items == null)
                    return false;
                int length = Items.Length;
                if (length == 0)
                    return true;
                for (int i = 0; i < length; ++i)
                    if (Items[i] == null)
                        return false;
                return true;
            }
        }

        public Loot(WorldObjectBase source, Looter looter) : base(source, looter) { }
    }
}