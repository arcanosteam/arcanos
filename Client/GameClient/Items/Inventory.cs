﻿using Arcanos.Client.GameClient.Players;
using Arcanos.Shared.Items;

namespace Arcanos.Client.GameClient.Items
{
    public class Inventory : InventoryBase
    {
        public Inventory(Character owner) : base(owner) { }

        public Item FindItem(ItemTemplateBase template)
        {
            for (byte i = 0; i < BAG_LIMIT; ++i)
            {
                Bag bag = Bags[i] as Bag;
                if (bag == null)
                    continue;

                for (byte j = 0; j < bag.Size; ++j)
                {
                    Item item = bag.Items[j] as Item;
                    if (item == null)
                        continue;

                    if (item.Template == template)
                        return item;
                }
            }
            return null;
        }
    }
}