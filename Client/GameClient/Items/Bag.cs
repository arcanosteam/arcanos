﻿using Arcanos.Shared.Items;

namespace Arcanos.Client.GameClient.Items
{
    public class Bag : BagBase
    {
        public Bag(InventoryBase inventory, ItemBase bagItem, byte size) : base(inventory, bagItem, size) { }
    }
}