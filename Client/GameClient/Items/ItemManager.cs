﻿using System.Collections.Generic;
using System.Threading;
using Arcanos.Net;
using Arcanos.Shared.Net;

namespace Arcanos.Client.GameClient.Items
{
    public delegate void ItemLoadedHandler(Item item);
    public static class ItemManager
    {
        private static readonly Dictionary<ulong, Item> Items = new Dictionary<ulong, Item>();

        private static readonly Dictionary<ulong, ItemLoadedHandler> Requests = new Dictionary<ulong, ItemLoadedHandler>();
        private static ConnectionManager<WorldPackets, IWorldPacket> Connection;

        private static readonly object requestsLock = new object();
        private static readonly object threadLock = new object();

        public static Item Get(ulong guid)
        {
            if (guid == 0)
                return null;

            lock (threadLock)
            {
                if (Items.ContainsKey(guid))
                    return Items[guid];
            }

            Connection.Write(new CItemDataQueryPacket { GUID = guid });
            while (!Items.ContainsKey(guid))
                Thread.Sleep(5);
            return Items[guid];
        }
        public static void Get(ulong guid, ItemLoadedHandler loadedHandler)
        {
            if (guid == 0)
                return;

            lock (threadLock)
            {
                if (Items.ContainsKey(guid))
                    loadedHandler(Items[guid]);
                else
                {
                    ItemLoadedHandler handlers;
                    lock (requestsLock)
                        if (Requests.TryGetValue(guid, out handlers))
                            Requests[guid] = handlers + loadedHandler;
                        else
                            Requests.Add(guid, loadedHandler);

                    Connection.Write(new CItemDataQueryPacket { GUID = guid });
                }
            }
        }

        public static void Connect(ConnectionManager<WorldPackets, IWorldPacket> connection)
        {
            Connection = connection;
        }

        public static void Add(ulong guid, Item item)
        {
            lock (threadLock)
            {
                Items[guid] = item;

                ItemLoadedHandler handlers;
                bool found;
                lock (requestsLock)
                    found = Requests.TryGetValue(guid, out handlers);
                if (found)
                {
                    handlers(item);
                    lock (requestsLock)
                        Requests.Remove(guid);
                }
            }
        }
    }
}