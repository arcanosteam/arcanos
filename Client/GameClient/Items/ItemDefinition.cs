﻿using Arcanos.Client.GameClient.Data;
using Arcanos.Shared.Items;

namespace Arcanos.Client.GameClient.Items
{
    public class ItemDefinition : ItemDefinitionBase
    {
        protected ItemDefinition() { }
        public ItemDefinition(ItemTemplateBase template, int count) : base(template, count) { }

        protected override ItemTemplateBase GetTemplateForID(int id)
        {
            return DatabaseManager.Items.Get(id);
        }
    }
}