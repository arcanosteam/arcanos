﻿using System;
using Arcanos.Client.GameClient.UI;
using Arcanos.Shared.Items;
using Microsoft.Xna.Framework.Graphics;

namespace Arcanos.Client.GameClient.Items
{
    public class Item : ItemBase
    {
        public Item(ulong guid, ItemTemplateBase template, int count) : base(guid, template, count) { }

        public override void SetCount(int count)
        {
            base.SetCount(count);
            UIManager.RaiseEvent(UIEvents.ItemUpdated, this, GUID);
        }
        public override void Destroy() { }

        public static Color GetMoneyColor()
        {
            return Color.Gold;
        }
        public static Color GetQualityColor(ItemQuality quality)
        {
            switch (quality)
            {
                case ItemQuality.Junk:
                    return Color.Gray;
                case ItemQuality.Common:
                    return Color.White;
                case ItemQuality.Uncommon:
                    return Color.LightSeaGreen;
                case ItemQuality.Rare:
                    return Color.Yellow;
                case ItemQuality.Unique:
                    return Color.Purple;
                case ItemQuality.Artifact:
                    return Color.Red;
                default:
                    throw new ArgumentOutOfRangeException("quality");
            }
        }

        protected override int GetInitialID()
        {
            throw new NotImplementedException();
        }
    }
}