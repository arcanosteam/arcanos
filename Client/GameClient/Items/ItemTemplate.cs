﻿using System.IO;
using Arcanos.Client.GameClient.Data;
using Arcanos.Shared.Data;
using Arcanos.Shared.Items;

namespace Arcanos.Client.GameClient.Items
{
    public class ItemTemplate : ItemTemplateBase, IManualSerialization, IManualDeserialization
    {
        protected override int GetInitialID()
        {
            return DatabaseManager.Items.GetID(this);
        }
        public override ItemBase Create(int count)
        {
            return new Item(0, this, count);
        }

        public byte[] Serialize(string member)
        {
            MemoryStream stream = null;
            switch (member)
            {
                case "SpellTriggers":
                {
                    stream = new MemoryStream((byte)ItemSpellUsage.Max * 4);
                    BinaryWriter writer = new BinaryWriter(stream);
                    for (byte i = 0; i < (byte)ItemSpellUsage.Max; ++i)
                        writer.Write(SpellTriggers[i] == null ? 0 : SpellTriggers[i].ID);
                    break;
                }
            }
            byte[] result = stream.GetBuffer();
            stream.Close();
            return result;
        }
        public void Deserialize(string member, byte[] data)
        {
            MemoryStream stream = new MemoryStream(data);
            BinaryReader reader = new BinaryReader(stream);
            switch (member)
            {
                case "SpellTriggers":
                {
                    for (byte i = 0; i < (byte)ItemSpellUsage.Max; ++i)
                    {
                        int ii = i;
                        int id = reader.ReadInt32();
                        if (id == 0)
                            SpellTriggers[i] = null;
                        else
                            DatabaseManager.Spells.Get(id, spell => SpellTriggers[ii] = spell);
                    }
                    break;
                }
            }
            stream.Close();
        }
    }
}