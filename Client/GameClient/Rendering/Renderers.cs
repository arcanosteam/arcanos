﻿namespace Arcanos.Client.GameClient.Rendering
{
    public enum Renderers
    {
        None,
        DebugRenderer,
        TerrainRenderer,
        TerrainRendererDecals,
        SpriteRenderer,
        ModelRenderer,
        UIRenderer,
    }
}