﻿using System.Collections.Generic;
using Arcanos.Client.GameClient.Data;
using Arcanos.Client.GameClient.Objects;
using Arcanos.Client.GameClient.Shaders;
using Arcanos.Client.GameClient.World;
using Arcanos.Shared.Graphics;
using Arcanos.Shared.Objects;
using Arcanos.Shared.World;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Arcanos.Client.GameClient.Rendering
{
    public class TerrainRenderGeometry : RenderGeometry<TerrainData.TerrainPatch, VertexPositionNormalTextureTangentBinormal>
    {
        public TerrainRenderGeometry() { }
        public TerrainRenderGeometry(int capacity) : base(capacity) { }

        public override void Add(TerrainData.TerrainPatch texture, Vector3 aPos, Vector3 bPos, Vector3 cPos, Vector3 aNorm, Vector3 bNorm, Vector3 cNorm, Vector2 aTex, Vector2 bTex, Vector2 cTex)
        {
            // Calculating tangent and binormal
            float x1 = bPos.X - aPos.X;
            float x2 = cPos.X - aPos.X;
            float y1 = bPos.Y - aPos.Y;
            float y2 = cPos.Y - aPos.Y;
            float z1 = bPos.Z - aPos.Z;
            float z2 = cPos.Z - aPos.Z;

            float s1 = bTex.X - aTex.X;
            float s2 = cPos.X - aTex.X;
            float t1 = bTex.Y - aTex.Y;
            float t2 = cTex.Y - aTex.Y;

            float r = 1.0f / (s1 * t2 - s2 * t1);
            Vector3 sdir = new Vector3((t2 * x1 - t1 * x2) * r, (t2 * y1 - t1 * y2) * r, (t2 * z1 - t1 * z2) * r);
            Vector3 tdir = new Vector3((s1 * x2 - s2 * x1) * r, (s1 * y2 - s2 * y1) * r, (s1 * z2 - s2 * z1) * r);

            Vector3 aTan = Vector3.Normalize(sdir - aNorm * Vector3.Dot(aNorm, sdir));
            Vector3 bTan = Vector3.Normalize(sdir - bNorm * Vector3.Dot(bNorm, sdir));
            Vector3 cTan = Vector3.Normalize(sdir - cNorm * Vector3.Dot(cNorm, sdir));

            Vector3 aBin = Vector3.Cross(aNorm, aTan) * ((Vector3.Dot(Vector3.Cross(aNorm, sdir), tdir) <= 0.0f) ? 1.0f : -1.0f);
            Vector3 bBin = Vector3.Cross(bNorm, bTan) * ((Vector3.Dot(Vector3.Cross(bNorm, sdir), tdir) <= 0.0f) ? 1.0f : -1.0f);
            Vector3 cBin = Vector3.Cross(cNorm, cTan) * ((Vector3.Dot(Vector3.Cross(cNorm, sdir), tdir) <= 0.0f) ? 1.0f : -1.0f);

            VertexPositionNormalTextureTangentBinormal
                vA = new VertexPositionNormalTextureTangentBinormal(aPos, aNorm, aTex, aTan, aBin),
                vB = new VertexPositionNormalTextureTangentBinormal(bPos, bNorm, bTex, bTan, bBin),
                vC = new VertexPositionNormalTextureTangentBinormal(cPos, cNorm, cTex, cTan, cBin);
            
            InternalAdd(texture, vA, vB, vC);
        }
    }
    public static class TerrainRenderer
    {
        private struct LightContext
        {
            public IWorldEntity Source;
            public LightData? Data;
        }

        private static TerrainShader shader;
        private static ProjectedDecalsShader decalsShader;
        private static GraphicsDevice graphicsDevice;
        private static VertexDeclaration declaration;

        private const int POINT_LIGHTS_COUNT = TerrainShader.POINT_LIGHTS_COUNT;
        private static readonly LightContext[] lights = new LightContext[POINT_LIGHTS_COUNT];

        private static float ambientIntensity = 1.0f;

        public static void Prepare(GraphicsDevice device)
        {
            graphicsDevice = device;
            declaration = new VertexDeclaration(graphicsDevice, VertexPositionNormalTextureTangentBinormal.VertexElements);
            shader = new TerrainShader(device) { World = WorldManager.WorldMatrix };
            decalsShader = new ProjectedDecalsShader(device);
        }
        private static void Activate()
        {
            if (RenderManager.ActiveRenderer != Renderers.TerrainRenderer)
            {
                graphicsDevice.VertexDeclaration = declaration;
                graphicsDevice.RenderState.FillMode = FillMode.Solid;
                graphicsDevice.RenderState.DepthBufferEnable = true;
                graphicsDevice.RenderState.AlphaBlendEnable = true;
                graphicsDevice.RenderState.SourceBlend = Blend.SourceAlpha;
                graphicsDevice.RenderState.DestinationBlend = Blend.InverseSourceAlpha;
                graphicsDevice.RenderState.AlphaTestEnable = false;
                RenderManager.SwitchShader(Renderers.TerrainRenderer, shader);
            }
        }
        public static void ActivateDecals()
        {
            if (RenderManager.ActiveRenderer != Renderers.TerrainRendererDecals)
                RenderManager.SwitchShader(Renderers.TerrainRendererDecals, decalsShader);
        }

        public static void UpdateCamera(Camera camera)
        {
            shader.View = camera.View;
            shader.Projection = camera.Projection;
            shader.CameraPosition = camera.Position;
            RenderManager.ApplyShaderChanges();
        }

        public static void RegisterLight(IWorldEntity source, LightData data)
        {
            int i;
            for (i = 0; i < POINT_LIGHTS_COUNT; ++i)
                if (lights[i].Source == null)
                    break;
            if (i >= POINT_LIGHTS_COUNT)
                return;
            lights[i].Source = source;
            lights[i].Data = data;
        }
        public static void RegisterLight(ModelInstance model)
        {
            RegisterLight(model.Owner, model.Model.Light);
        }
        public static void UnregisterLight(IWorldEntity source)
        {
            for (int i = 0; i < POINT_LIGHTS_COUNT; ++i)
                if (lights[i].Source == source)
                {
                    lights[i].Source = null;
                    lights[i].Data = null;
                    break;
                }
        }
        public static void UnregisterLight(ModelInstance model)
        {
            UnregisterLight(model.Owner);
        }
        public static int RegisterDecal(Texture2D texture, Vector3 position, float radius)
        {
            int i = decalsShader.GetFreeIndex();
            if (i != -1)
                decalsShader.SetDecal(i, texture, position, radius);
            return i;
        }
        public static int RegisterDecal(DecalModelInstance model)
        {
            int texID = (model.Model as DecalModel).TextureID;
            Vector3 pos = model.Owner.GetPosition();
            float size = (model.Model.RelativeSize ? model.Owner.GetSize().Max() : 1) * (model.Model as DecalModel).Size;
            return RegisterDecal(DatabaseManager.TextureData[texID], pos, size);
        }
        public static void MoveDecal(int index, Vector3 position)
        {
            decalsShader.MoveDecal(index, position);
        }
        public static void ResizeDecal(int index, float radius)
        {
            decalsShader.ResizeDecal(index, radius);
        }
        public static void UnregisterDecal(int index)
        {
            decalsShader.RemoveDecal(index);
        }
        public static void SetDecalZDistribution(float distribution)
        {
            decalsShader.SetDecalZDistribution(distribution);
        }
        public static void ApplyLighting()
        {
            for (int i = 0; i < POINT_LIGHTS_COUNT; ++i)
                if (lights[i].Source != null && lights[i].Data.HasValue)
                {
                    TerrainShader.PointLight light = shader.PointLights[i];
                    light.Enabled = lights[i].Data.Value.Enabled;
                    light.Color = lights[i].Data.Value.Color;
                    light.Position = lights[i].Source.GetPosition() + new Vector3(0,0,2);
                    light.Radius = lights[i].Data.Value.Radius;
                }
                else
                    shader.PointLights[i].Enabled = false;
        }
        public static void SetAmbientLight(Vector4 color)
        {
            shader.AmbientLight = color;
        }
        public static void SetAmbientIntensity(float amount)
        {
            ambientIntensity = amount;
        }
        public static float GetAmbientIntensity()
        {
            return ambientIntensity;
        }

        public static void SetEnvironment(EnvironmentBase env)
        {
            shader.AmbientLight = new Vector4(env.AmbientLight * ambientIntensity, 1);
            shader.FogBounds = new Vector2(env.FogStart, env.FogEnd);
            shader.FogColor = env.FogColor;
        }
        public static void Render(MapBase map)
        {
            SetEnvironment(map.Environment);
            foreach (TerrainRenderGeometry geometry in (map as IRenderable<TerrainData.TerrainPatch, TerrainRenderGeometry>).GetRenderContext(graphicsDevice))
                Render(geometry);
        }
        public static void Render(TerrainRenderGeometry geometry)
        {
            ApplyLighting();

            Activate();

            graphicsDevice.Vertices[0].SetSource(geometry.VertexBuffer, 0, VertexPositionNormalTextureTangentBinormal.SizeInBytes);
            graphicsDevice.Indices = geometry.IndexBuffer;

            foreach (KeyValuePair<TerrainData.TerrainPatch, int> texture in geometry.TextureStart)
            {
                if (texture.Key.BaseMaterialID != 0)
                {
                    shader.BaseTexture = DatabaseManager.MaterialData[texture.Key.BaseMaterialID].GetDiffuseTexture();
                    shader.NormalTexture = DatabaseManager.MaterialData[texture.Key.BaseMaterialID].GetNormalTexture();
                }
                else
                {
                    shader.BaseTexture = null;
                    shader.NormalTexture = null;
                }
                if (texture.Key.Layer0MaterialID != 0)
                {
                    shader.Layer0Texture = DatabaseManager.MaterialData[texture.Key.Layer0MaterialID].GetDiffuseTexture();
                    shader.Normal0Texture = DatabaseManager.MaterialData[texture.Key.Layer0MaterialID].GetNormalTexture();
                    shader.Alpha0Texture = DatabaseManager.TextureData[texture.Key.Layer0AlphaID];
                    if (texture.Key.Layer1MaterialID != 0)
                    {
                        shader.Layer1Texture = DatabaseManager.MaterialData[texture.Key.Layer1MaterialID].GetDiffuseTexture();
                        shader.Normal1Texture = DatabaseManager.MaterialData[texture.Key.Layer1MaterialID].GetNormalTexture();
                        shader.Alpha1Texture = DatabaseManager.TextureData[texture.Key.Layer1AlphaID];
                        if (texture.Key.Layer2MaterialID != 0)
                        {
                            shader.Layer2Texture = DatabaseManager.MaterialData[texture.Key.Layer2MaterialID].GetDiffuseTexture();
                            shader.Normal2Texture = DatabaseManager.MaterialData[texture.Key.Layer2MaterialID].GetNormalTexture();
                            shader.Alpha2Texture = DatabaseManager.TextureData[texture.Key.Layer2AlphaID];
                            if (texture.Key.Layer3MaterialID != 0)
                            {
                                shader.Layer3Texture = DatabaseManager.MaterialData[texture.Key.Layer3MaterialID].GetDiffuseTexture();
                                shader.Normal3Texture = DatabaseManager.MaterialData[texture.Key.Layer3MaterialID].GetNormalTexture();
                                shader.Alpha3Texture = DatabaseManager.TextureData[texture.Key.Layer3AlphaID];
                                shader.LayersCount = 4;
                            }
                            else
                                shader.LayersCount = 3;
                        }
                        else
                            shader.LayersCount = 2;
                    }
                    else
                        shader.LayersCount = 1;
                }
                else
                    shader.LayersCount = 0;
                shader.TextureScale = texture.Key.Scale;
                RenderManager.ApplyShaderChanges();
                graphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, geometry.Geometry.Length, texture.Value, geometry.TextureLength[texture.Key] / 3);
            }
            ActivateDecals();
            foreach (KeyValuePair<TerrainData.TerrainPatch, int> texture in geometry.TextureStart)
                graphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, geometry.Geometry.Length, texture.Value, geometry.TextureLength[texture.Key] / 3);
        }
    }
}