﻿namespace Arcanos.Client.GameClient.Rendering
{
    public static class RenderManager
    {
        public static Renderers ActiveRenderer;
        private static IShader ActiveShader;
        private static bool Active;

        public static void ApplyShaderChanges()
        {
            if (ActiveShader != null)
            {
                if (Active)
                    ActiveShader.End();
                ActiveShader.Begin();
                Active = true;
            }   
        }
        public static void StopShader()
        {
            ActiveRenderer = Renderers.None;
            if (ActiveShader != null && Active)
                ActiveShader.End();
            ActiveShader = null;
            Active = false;
        }
        public static void SwitchShader(Renderers renderer, IShader shader)
        {
            ActiveRenderer = renderer;
            if (ActiveShader == shader)
                return;
            if (ActiveShader != null && Active)
                ActiveShader.End();
            ActiveShader = shader;
            if (ActiveShader != null)
            {
                ActiveShader.Begin();
                Active = true;
            }
            else
                Active = false;
        }
    }
}