﻿using System;
using Arcanos.Client.GameClient.Shaders;
using Arcanos.Client.GameClient.UI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Arcanos.Client.GameClient.Rendering
{
    public static class UIRenderer
    {
        private static SpriteBatch spriteBatch;
        private static float lastEffectAlpha = 0;

        private static UIShader shader;
        private static GraphicsDevice graphicsDevice;
        private static VertexDeclaration declaration;

        public static void Prepare(GraphicsDevice device)
        {
            graphicsDevice = device;
            declaration = new VertexDeclaration(graphicsDevice, VertexPositionTexture.VertexElements);
            shader = new UIShader(device);
            spriteBatch = new SpriteBatch(graphicsDevice);
        }
        public static void Activate()
        {
            if (RenderManager.ActiveRenderer != Renderers.UIRenderer)
            {
                graphicsDevice.VertexDeclaration = declaration;
                graphicsDevice.RenderState.FillMode = FillMode.Solid;
                graphicsDevice.RenderState.DepthBufferEnable = false;
                /*graphicsDevice.RenderState.AlphaBlendEnable = true;
                graphicsDevice.RenderState.SourceBlend = Blend.SourceAlpha;
                graphicsDevice.RenderState.DestinationBlend = Blend.InverseSourceAlpha;
                graphicsDevice.RenderState.AlphaTestEnable = false;*/
                RenderManager.SwitchShader(Renderers.UIRenderer, shader);
            }
        }

        public static void Render(UIDesktop desktop)
        {
            if (!desktop.Visible)
                return;

            Activate();

            shader.View = Matrix.CreateLookAt(
                new Vector3(graphicsDevice.Viewport.Width / 2f, graphicsDevice.Viewport.Height / 2f, 0),
                new Vector3(graphicsDevice.Viewport.Width / 2f, graphicsDevice.Viewport.Height / 2f, 1), -Vector3.UnitY);
            shader.Projection = Matrix.CreateTranslation(-0.5f, 0.5f, 0) * Matrix.CreateOrthographic(graphicsDevice.Viewport.Width, graphicsDevice.Viewport.Height, 1, 10);
            shader.Alpha = 1.0f;
            RenderManager.ApplyShaderChanges();

            foreach (UIControl c in desktop.Controls.Lock())
                Render(c, Vector2.Zero);
            desktop.Controls.Unlock();

            Render(desktop.Dropdown, Vector2.Zero);
            Render(desktop.DraggedItemControl, Vector2.Zero);
            Render(desktop.DraggedSpellControl, Vector2.Zero);
            Render(desktop.Tooltip, Vector2.Zero);
        }
        public static void Render(UIControl control, Vector2 offset)
        {
            if (control == null) return;
            if (!control.Visible) return;
            float prevAlpha = shader.Alpha;
            float opacity = control.Opacity;
            if (opacity > 1) opacity = 1;
            if (opacity < 0) opacity = 0;
            if (prevAlpha * opacity != lastEffectAlpha)
            {
                shader.Alpha *= opacity;
                lastEffectAlpha = shader.Alpha;
            }
            Vector2? textSize = null;
            if (control.TextUsesBackdropSize) textSize = control.BackdropSize;
            if (control.TextUsesOverlaySize) textSize = control.OverlaySize;
            Vector2 from = control.Location + offset;
            Vector2 size = control.Size;
            Matrix oldMatrix = Matrix.Identity;
            if (control.Scale != Vector2.One)
            {
                oldMatrix = shader.World;
                shader.World = shader.World * Matrix.CreateTranslation(new Vector3(-from.X - size.X / 2, -from.Y - size.Y / 2, 0)) * Matrix.CreateScale(control.Scale.X, control.Scale.Y, 1) * Matrix.CreateTranslation(new Vector3(from.X + size.X / 2, from.Y + size.Y / 2, 0));
            }
            Render(control.BackdropGraphic, null, control.Font, from, control.BackdropSize, control.ContentOffset, textSize, control.MultilineText, control.CutOverflownText, control.Scale);
            Render(control.CurrentGraphic, control.Text, control.Font, from, control.Size, control.ContentOffset, textSize, control.MultilineText, control.CutOverflownText, control.Scale);
            Render(control.OverlayGraphic, null, control.Font, from, control.OverlaySize, control.ContentOffset, textSize, control.MultilineText, control.CutOverflownText, control.Scale);
            control.AfterDraw();
            Rectangle prevRect = graphicsDevice.ScissorRectangle;
            bool prevScissor = graphicsDevice.RenderState.ScissorTestEnable;
            if (control.CutOverflownChildren)
            {
                Rectangle? intersection = Intersection(prevRect, new Rectangle((int)from.X, (int)from.Y, (int)size.X, (int)size.Y));
                if (!intersection.HasValue)
                    return;
                graphicsDevice.ScissorRectangle = intersection.Value;
                graphicsDevice.RenderState.ScissorTestEnable = true;
            }
            foreach (UIControl c in control.Controls.Lock())
                Render(c, from + control.ContentOffset);
            control.Controls.Unlock();
            if (control.Scale != Vector2.One)
                shader.World = oldMatrix;
            if (control.CutOverflownChildren)
            {
                graphicsDevice.ScissorRectangle = prevRect;
                graphicsDevice.RenderState.ScissorTestEnable = prevScissor;
            }
            if (shader.Alpha != prevAlpha)
                shader.Alpha = lastEffectAlpha = prevAlpha;
        }
        public static void Render(UITexture tex, string text, UIFont font, Vector2 location, Vector2 size, Vector2 textOffset, Vector2? textSize, bool multiline, bool cutText, Vector2 scale)
        {
            Vector2 from = location,
                    to = location + size;
            Vector2 textPosMin, textPosMax;
            const float depth = 10.0f;
            if (tex != null)
            {
                bool prevAlphaBlendEnable = graphicsDevice.RenderState.AlphaBlendEnable;
                Blend prevSourceBlend = graphicsDevice.RenderState.SourceBlend;
                Blend prevDestinationBlend = graphicsDevice.RenderState.DestinationBlend;
                bool prevAlphaTestEnable = graphicsDevice.RenderState.AlphaTestEnable;
                if (tex.Alpha)
                {
                    graphicsDevice.RenderState.AlphaBlendEnable = true;
                    graphicsDevice.RenderState.SourceBlend = Blend.SourceAlpha;
                    graphicsDevice.RenderState.DestinationBlend = Blend.InverseSourceAlpha;
                    graphicsDevice.RenderState.AlphaTestEnable = false;
                }
                if (tex.PaddingMin.X > 0 || tex.PaddingMin.Y > 0 || tex.PaddingMax.X > 0 || tex.PaddingMax.Y > 0)
                {
                    Vector2 padMin = tex.PaddingMin,
                            padMax = tex.PaddingMax;
                    Vector2 tA = new Vector2(tex.Offset.X, tex.Offset.Y),
                            tB = new Vector2(tex.Offset.X + padMin.X, tex.Offset.Y),
                            tC = new Vector2(tex.Offset.X + tex.Size.X - padMax.X, tex.Offset.Y),
                            tD = new Vector2(tex.Offset.X + tex.Size.X, tex.Offset.Y),
                            tE = new Vector2(tex.Offset.X, tex.Offset.Y + padMin.Y),
                            tF = new Vector2(tex.Offset.X + padMin.X, tex.Offset.Y + padMin.Y),
                            tG = new Vector2(tex.Offset.X + tex.Size.X - padMax.X, tex.Offset.Y + padMin.Y),
                            tH = new Vector2(tex.Offset.X + tex.Size.X, tex.Offset.Y + padMin.Y),
                            tI = new Vector2(tex.Offset.X, tex.Offset.Y + tex.Size.Y - padMax.Y),
                            tJ = new Vector2(tex.Offset.X + padMin.X, tex.Offset.Y + tex.Size.Y - padMax.Y),
                            tK = new Vector2(tex.Offset.X + tex.Size.X - padMax.X, tex.Offset.Y + tex.Size.Y - padMax.Y),
                            tL = new Vector2(tex.Offset.X + tex.Size.X, tex.Offset.Y + tex.Size.Y - padMax.Y),
                            tM = new Vector2(tex.Offset.X, tex.Offset.Y + tex.Size.Y),
                            tN = new Vector2(tex.Offset.X + padMin.X, tex.Offset.Y + tex.Size.Y),
                            tO = new Vector2(tex.Offset.X + tex.Size.X - padMax.X, tex.Offset.Y + tex.Size.Y),
                            tP = new Vector2(tex.Offset.X + tex.Size.X, tex.Offset.Y + tex.Size.Y);
                    padMin *= new Vector2(tex.Texture.Width, tex.Texture.Height);
                    padMax *= new Vector2(tex.Texture.Width, tex.Texture.Height);
                    Vector2 p = padMin + padMax;
                    Vector2 s = to - from;
                    if (p.X > s.X)
                    {
                        float downscaleFactor = s.X / p.X;
                        padMin.X *= downscaleFactor;
                        padMax.X *= downscaleFactor;
                    }
                    if (p.Y > s.Y)
                    {
                        float downscaleFactor = s.Y / p.Y;
                        padMin.Y *= downscaleFactor;
                        padMax.Y *= downscaleFactor;
                    }
                    Vector3 wA = new Vector3(from.X, from.Y, depth),
                            wB = new Vector3(from.X + padMin.X, from.Y, depth),
                            wC = new Vector3(to.X - padMax.X, from.Y, depth),
                            wD = new Vector3(to.X, from.Y, depth),
                            wE = new Vector3(from.X, from.Y + padMin.Y, depth),
                            wF = new Vector3(from.X + padMin.X, from.Y + padMin.Y, depth),
                            wG = new Vector3(to.X - padMax.X, from.Y + padMin.Y, depth),
                            wH = new Vector3(to.X, from.Y + padMin.Y, depth),
                            wI = new Vector3(from.X, to.Y - padMax.Y, depth),
                            wJ = new Vector3(from.X + padMin.X, to.Y - padMax.Y, depth),
                            wK = new Vector3(to.X - padMax.X, to.Y - padMax.Y, depth),
                            wL = new Vector3(to.X, to.Y - padMax.Y, depth),
                            wM = new Vector3(from.X, to.Y, depth),
                            wN = new Vector3(from.X + padMin.X, to.Y, depth),
                            wO = new Vector3(to.X - padMax.X, to.Y, depth),
                            wP = new Vector3(to.X, to.Y, depth);

                    shader.Texture = tex.Texture;
                    RenderManager.ApplyShaderChanges();
                    //graphicsDevice.SamplerStates[0].MagFilter = graphicsDevice.SamplerStates[0].MinFilter = graphicsDevice.SamplerStates[0].MipFilter = TextureFilter.None;
                    VertexPositionTexture[] list =
                    {
                        new VertexPositionTexture(wE, tE),
                        new VertexPositionTexture(wA, tA),
                        new VertexPositionTexture(wF, tF),
                        new VertexPositionTexture(wB, tB),
                        new VertexPositionTexture(wG, tG),
                        new VertexPositionTexture(wC, tC),
                        new VertexPositionTexture(wH, tH),
                        new VertexPositionTexture(wD, tD),

                        new VertexPositionTexture(wI, tI),
                        new VertexPositionTexture(wE, tE),
                        new VertexPositionTexture(wJ, tJ),
                        new VertexPositionTexture(wF, tF),
                        new VertexPositionTexture(wK, tK),
                        new VertexPositionTexture(wG, tG),
                        new VertexPositionTexture(wL, tL),
                        new VertexPositionTexture(wH, tH),

                        new VertexPositionTexture(wM, tM),
                        new VertexPositionTexture(wI, tI),
                        new VertexPositionTexture(wN, tN),
                        new VertexPositionTexture(wJ, tJ),
                        new VertexPositionTexture(wO, tO),
                        new VertexPositionTexture(wK, tK),
                        new VertexPositionTexture(wP, tP),
                        new VertexPositionTexture(wL, tL)
                    };
                    graphicsDevice.DrawUserPrimitives(PrimitiveType.TriangleStrip, list, 0, 6);
                    graphicsDevice.DrawUserPrimitives(PrimitiveType.TriangleStrip, list, 8, 6);
                    graphicsDevice.DrawUserPrimitives(PrimitiveType.TriangleStrip, list, 16, 6);
                    textPosMin = new Vector2(wF.X, wF.Y);
                    textPosMax = new Vector2(wK.X, wK.Y);
                }
                else
                {
                    shader.Texture = tex.Texture;
                    RenderManager.ApplyShaderChanges();
                    //graphicsDevice.SamplerStates[0].MagFilter = graphicsDevice.SamplerStates[0].MinFilter = graphicsDevice.SamplerStates[0].MipFilter = TextureFilter.None;
                    VertexPositionTexture[] list = tex.Wrap ?
                        new[]
                        {
                            new VertexPositionTexture(new Vector3(from.X, to.Y, depth), new Vector2(0, (to.Y - from.Y) / tex.PixelSize.Y)),
                            new VertexPositionTexture(new Vector3(from.X, from.Y, depth), Vector2.Zero),
                            new VertexPositionTexture(new Vector3(to.X, to.Y, depth), new Vector2((to.X - from.X) / tex.PixelSize.X, (to.Y - from.Y) / tex.PixelSize.Y)),
                            new VertexPositionTexture(new Vector3(to.X, from.Y, depth), new Vector2((to.X - from.X) / tex.PixelSize.X, 0))
                        } :
                        new[]
                        {
                            new VertexPositionTexture(new Vector3(from.X, to.Y, depth), tex.Offset + tex.Size * Vector2.UnitY),
                            new VertexPositionTexture(new Vector3(from.X, from.Y, depth), tex.Offset),
                            new VertexPositionTexture(new Vector3(to.X, to.Y, depth), tex.Offset + tex.Size),
                            new VertexPositionTexture(new Vector3(to.X, from.Y, depth), tex.Offset + tex.Size * Vector2.UnitX)
                        };
                    graphicsDevice.DrawUserPrimitives(PrimitiveType.TriangleStrip, list, 0, 2);
                    textPosMin = from;
                    textPosMax = to;
                }
                if (tex.Alpha)
                {
                    graphicsDevice.RenderState.AlphaBlendEnable = prevAlphaBlendEnable;
                    graphicsDevice.RenderState.SourceBlend = prevSourceBlend;
                    graphicsDevice.RenderState.DestinationBlend = prevDestinationBlend;
                    graphicsDevice.RenderState.AlphaTestEnable = prevAlphaTestEnable;
                }
            }
            else
            {
                textPosMin = from;
                textPosMax = to;
            }
            if (!string.IsNullOrEmpty(text) && font.Font != null)
            {
                if (textSize.HasValue)
                {
                    textPosMin = from;
                    textPosMax = textPosMin + textSize.Value;
                }
                if (scale != Vector2.One)
                {
                    textPosMin -= (size * scale - size) / 2;
                    textPosMax += (size * scale - size) / 2;
                }
                textPosMin += textOffset * scale;

                bool prevAlphaBlendEnable = graphicsDevice.RenderState.AlphaBlendEnable;
                Blend prevSourceBlend = graphicsDevice.RenderState.SourceBlend;
                Blend prevDestinationBlend = graphicsDevice.RenderState.DestinationBlend;
                bool prevAlphaTestEnable = graphicsDevice.RenderState.AlphaTestEnable;

                Rectangle prevRect = graphicsDevice.ScissorRectangle;
                bool prevScissor = graphicsDevice.RenderState.ScissorTestEnable;
                if (cutText)
                {
                    Vector2 cutFrom = from;
                    Vector2 cutTo = to;
                    if (scale != Vector2.One)
                    {
                        cutFrom -= (size * scale - size) / 2;
                        cutTo += (size * scale - size) / 2;
                    }
                    Rectangle? intersection = Intersection(prevRect, new Rectangle((int)cutFrom.X, (int)cutFrom.Y, (int)(cutTo.X - cutFrom.X), (int)(cutTo.Y - cutFrom.Y)));
                    if (!intersection.HasValue)
                        return;
                    graphicsDevice.ScissorRectangle = intersection.Value;
                    graphicsDevice.RenderState.ScissorTestEnable = true;
                }
                spriteBatch.Begin(SpriteBlendMode.AlphaBlend, SpriteSortMode.Deferred, SaveStateMode.SaveState); // TODO: Change to SaveStateMode.None
                bool outline = font.ShadowColor.A > 0 && font.ShadowOffset.X == 0 && font.ShadowOffset.Y == 0;
                int outlineCounter = 4;
                for (int i = 0; i < (outline ? 5 : 2); i++)
                {
                    bool shadow = outline ? i < 4 : i == 0;
                    if (font.Font == null) continue;
                    Vector2 stringSize;
                    string[] lines = new string[0];
                    if (multiline)
                        lines = font.FitString(text, textPosMax.X - textPosMin.X, out stringSize);
                    else
                        stringSize = font.MeasureString(text);
                    stringSize *= scale;
                    Vector2 pos = font.Offset;
                    if ((font.Align & UIFontAlign.Left) != 0)
                        pos.X += textPosMin.X;
                    if ((font.Align & UIFontAlign.Center) != 0)
                        pos.X += (textPosMin.X + textPosMax.X) / 2 - stringSize.X / 2;
                    if ((font.Align & UIFontAlign.Right) != 0)
                        pos.X += textPosMax.X - stringSize.X;
                    if ((font.Align & UIFontAlign.Top) != 0)
                        pos.Y += textPosMin.Y;
                    if ((font.Align & UIFontAlign.Middle) != 0)
                        pos.Y += (textPosMin.Y + textPosMax.Y) / 2 - stringSize.Y / 2;
                    if ((font.Align & UIFontAlign.Bottom) != 0)
                        pos.Y += textPosMax.Y - stringSize.Y;

                    if (outline && outlineCounter > 0)
                    {
                        switch (outlineCounter)
                        {
                            case 1:
                                pos.X += 1 * scale.X;
                                break;
                            case 2:
                                pos.X -= 1 * scale.X;
                                break;
                            case 3:
                                pos.Y -= 1 * scale.Y;
                                break;
                            case 4:
                                pos.Y += 1 * scale.Y;
                                break;
                        }
                        --outlineCounter;
                    }
                    else if (shadow)
                        pos += font.ShadowOffset;

                    pos.X = (int)pos.X;
                    pos.Y = (int)pos.Y;

                    Color color = shadow ? font.ShadowColor : font.Color;
                    color.A = (byte)(color.A * shader.Alpha);

                    if (multiline)
                        foreach (string line in lines)
                        {
                            spriteBatch.DrawString(font.Font, line, pos, color);
                            pos.Y += font.LineHeight + font.LineBreak;
                        }
                    else
                        spriteBatch.DrawString(font.Font, text, pos, color, 0, Vector2.Zero, scale, SpriteEffects.None, 0);
                }
                spriteBatch.End();
                graphicsDevice.RenderState.AlphaTestEnable = false;
                graphicsDevice.SamplerStates[0].AddressU = TextureAddressMode.Wrap;
                graphicsDevice.SamplerStates[0].AddressV = TextureAddressMode.Wrap;
                if (cutText)
                {
                    graphicsDevice.ScissorRectangle = prevRect;
                    graphicsDevice.RenderState.ScissorTestEnable = prevScissor;
                }
                graphicsDevice.RenderState.AlphaBlendEnable = prevAlphaBlendEnable;
                graphicsDevice.RenderState.SourceBlend = prevSourceBlend;
                graphicsDevice.RenderState.DestinationBlend = prevDestinationBlend;
                graphicsDevice.RenderState.AlphaTestEnable = prevAlphaTestEnable;
            }
        }

        public static Rectangle? Intersection(Rectangle rect, Rectangle other)
        {
            Rectangle intersection = new Rectangle
            {
                X = Math.Max(rect.Left, other.Left),
                Y = Math.Max(rect.Top, other.Top)
            };
            intersection.Width = Math.Min(rect.Right, other.Right) - intersection.X;
            intersection.Height = Math.Min(rect.Bottom, other.Bottom) - intersection.Y;
            if (intersection.Width < 0)
                return null;
            if (intersection.Height < 0)
                return null;
            return intersection;
        }
    }
}