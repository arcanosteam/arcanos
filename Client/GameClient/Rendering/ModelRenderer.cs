﻿using System.Collections.Generic;
using Arcanos.Client.GameClient.Data;
using Arcanos.Client.GameClient.Graphics;
using Arcanos.Client.GameClient.Objects;
using Arcanos.Client.GameClient.Shaders;
using Arcanos.Client.GameClient.World;
using Arcanos.Shared.Graphics;
using Arcanos.Shared.Grids;
using Arcanos.Shared.Objects;
using Arcanos.Shared.World;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Arcanos.Client.GameClient.Rendering
{
    public class ModelRenderGeometry : RenderGeometry<int, VertexPositionNormalTexture>
    {
        public ModelRenderGeometry() { }
        public ModelRenderGeometry(int capacity) : base(capacity) { }

        public override void Add(int material, Vector3 aPos, Vector3 bPos, Vector3 cPos, Vector3 aNorm, Vector3 bNorm, Vector3 cNorm, Vector2 aTex, Vector2 bTex, Vector2 cTex)
        {
            VertexPositionNormalTexture
                vA = new VertexPositionNormalTexture(aPos, aNorm, aTex),
                vB = new VertexPositionNormalTexture(bPos, bNorm, bTex),
                vC = new VertexPositionNormalTexture(cPos, cNorm, cTex);

            InternalAdd(material, vA, vB, vC);
        }
    }
    public static class ModelRenderer
    {
        private static ModelShader shader;
        private static GraphicsDevice graphicsDevice;
        private static VertexDeclaration declaration;

        public static void Prepare(GraphicsDevice device)
        {
            graphicsDevice = device;
            declaration = new VertexDeclaration(graphicsDevice, VertexPositionNormalTexture.VertexElements);
            shader = new ModelShader(device);
        }
        private static void Activate()
        {
            if (RenderManager.ActiveRenderer != Renderers.ModelRenderer)
            {
                graphicsDevice.VertexDeclaration = declaration;
                graphicsDevice.RenderState.FillMode = FillMode.Solid;
                graphicsDevice.RenderState.DepthBufferEnable = true;
                graphicsDevice.RenderState.AlphaBlendEnable = true;
                graphicsDevice.RenderState.SourceBlend = Blend.SourceAlpha;
                graphicsDevice.RenderState.DestinationBlend = Blend.InverseSourceAlpha;
                graphicsDevice.RenderState.AlphaTestEnable = true;
                graphicsDevice.RenderState.ReferenceAlpha = 128;
                graphicsDevice.RenderState.AlphaFunction = CompareFunction.GreaterEqual;
                RenderManager.SwitchShader(Renderers.ModelRenderer, shader);
            }
        }

        public static void UpdateOrtho(Vector2 location, Vector2 size)
        {
            Vector3 target = new Vector3(location + size / 2, -1000);
            shader.View = Matrix.CreateLookAt(target, target + Vector3.UnitZ, -Vector3.UnitY);
            shader.Projection = Matrix.CreateTranslation(-0.5f, 0.5f, 0) * Matrix.CreateOrthographic(size.X, size.Y, 0.1f, 10000);
            RenderManager.ApplyShaderChanges();
        }

        public static void Render(MapBase map, Camera camera, IWorldEntity center)
        {
            shader.SetWind(map.Environment.Wind, map.EnvironmentTime);
            RenderStatics(map, camera, center);
            Render(map.GetVisibleObjects(center.GetPosition()), camera);
        }

        private static void RenderStatics(MapBase map, Camera camera, IWorldEntity center)
        {
            GridBounds bounds = map.Grid.GetGridBounds(center.GetPosition(), WorldManager.StaticVisibilityDistance);
            for (int x = bounds.MinX; x <= bounds.MaxX; ++x)
                for (int y = bounds.MinY; y <= bounds.MaxY; ++y)
                {
                    bool? cellVisible = null;
                    for (int i = 0; i < 2; ++i)
                        foreach (StaticModelObject smo in (i == 0 ? map.Grid.Cells[x][y].SolidStatics : map.Grid.Cells[x][y].Statics))
                        {
                            if ((smo.Flags & StaticModelObjectFlags.CellOcclusion) != 0)
                            {
                                if (!cellVisible.HasValue)
                                    cellVisible = camera.TestFrustum(ref map.Grid.Cells[x][y].BoundingBox);
                                if (!cellVisible.Value)
                                    continue;
                            }
                            else
                                if (!camera.TestFrustum(ref smo.BoundingBox))
                                    continue;

                            if ((smo.Flags & StaticModelObjectFlags.ReducedVisibilityDistance) != 0)
                                if (Vector3.Distance(center.GetPosition(), smo.Position) > WorldManager.StaticReducedVisibilityDistance)
                                    continue;
                            
                            Render(null, smo.Model, smo.Matrix);
                        }
                }
        }
        private static void Render(IEnumerable<WorldObjectBase> objects, Camera camera)
        {
            foreach (WorldObjectBase wo in objects)
            {
                if (wo == null || wo.Model == null || wo.Model.Model.Type != ModelType.WorldModel)
                    continue;

                WorldModel model = wo.Model.Model as WorldModel;

                Vector3 size;
                Vector3 offset;
                wo.GetRenderInfo(out offset, out size);

                if (!camera.TestFrustum(ref wo.BoundingBox))
                    continue;

                Render(wo.Model as WorldModelInstance, model, Matrix.CreateRotationX(wo.Rotation.Roll) * Matrix.CreateRotationY(wo.Rotation.Pitch) * Matrix.CreateRotationZ(wo.Rotation.Yaw) * Matrix.CreateScale(model.RelativeSize ? size * model.Size : model.Size) * Matrix.CreateTranslation(wo.Position));
            }
        }
        public static void Render(WorldModelInstance instance, WorldModel model, Matrix matrix)
        {
            Matrix world = shader.World;
            shader.World *= matrix;

            if ((model.Flags & WorldModelFlags.InvertNormals) != 0)
                graphicsDevice.RenderState.CullMode = CullMode.CullClockwiseFace;
            if ((model.Flags & WorldModelFlags.DoubleFaced) != 0)
                graphicsDevice.RenderState.CullMode = CullMode.None;

            if (model.WindInfluenceZ == 0)
                shader.SetInfluenceBounds(float.MaxValue, float.MaxValue);
            else
            {
                Vector3 windMin = Vector3.Zero;
                Vector3 windMax = new Vector3(0, 0, model.WindInfluenceZ);
                Vector3.Transform(ref windMin, ref matrix, out windMin);
                Vector3.Transform(ref windMax, ref matrix, out windMax);
                shader.SetInfluenceBounds(windMin.Z, windMax.Z);
            }

            if (instance != null)
                foreach (ModelRenderGeometry geometry in ModelHelper.GetModelRenderContext(graphicsDevice, instance))
                    Render(geometry);
            else
                foreach (ModelRenderGeometry geometry in ModelHelper.GetModelRenderContext(graphicsDevice, model))
                    Render(geometry);
            // TODO: Currently broken
            //foreach (ModelRenderGeometry geometry in renderable.GetRenderContext(graphicsDevice))
            //    RenderDecals(geometry);

            if ((model.Flags & (WorldModelFlags.InvertNormals | WorldModelFlags.DoubleFaced)) != 0)
                graphicsDevice.RenderState.CullMode = CullMode.CullCounterClockwiseFace;

            shader.World = world;
        }
        private static void Render(ModelRenderGeometry geometry)
        {
            Activate();

            graphicsDevice.Vertices[0].SetSource(geometry.VertexBuffer, 0, VertexPositionNormalTexture.SizeInBytes);
            graphicsDevice.Indices = geometry.IndexBuffer;

            foreach (KeyValuePair<int, int> texture in geometry.TextureStart)
            {
                if (texture.Key != 0 && texture.Key != shader.LastTextureID)
                    shader.BaseTexture = DatabaseManager.MaterialData[shader.LastTextureID = texture.Key].GetDiffuseTexture();
                RenderManager.ApplyShaderChanges();
                graphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, geometry.Geometry.Length, texture.Value, geometry.TextureLength[texture.Key] / 3);
            }
        }
        private static void RenderDecals(ModelRenderGeometry geometry)
        {
            TerrainRenderer.ActivateDecals();

            graphicsDevice.Vertices[0].SetSource(geometry.VertexBuffer, 0, VertexPositionNormalTexture.SizeInBytes);
            graphicsDevice.Indices = geometry.IndexBuffer;

            foreach (KeyValuePair<int, int> texture in geometry.TextureStart)
                graphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, geometry.Geometry.Length, texture.Value, geometry.TextureLength[texture.Key] / 3);
        }
    }
}