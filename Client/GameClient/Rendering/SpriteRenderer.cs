﻿using System.Collections.Generic;
using Arcanos.Client.GameClient.Data;
using Arcanos.Client.GameClient.Objects;
using Arcanos.Client.GameClient.Shaders;
using Arcanos.Client.GameClient.Spells;
using Arcanos.Client.GameClient.World;
using Arcanos.Shared;
using Arcanos.Shared.Graphics;
using Arcanos.Shared.Movement;
using Arcanos.Shared.Objects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Arcanos.Client.GameClient.Rendering
{
    public class SpriteRenderGeometry : RenderGeometry<byte, VertexPositionNormalTexture>
    {
        public SpriteRenderGeometry() { }
        public SpriteRenderGeometry(int capacity) : base(capacity) { }

        public override void Add(byte texture, Vector3 aPos, Vector3 bPos, Vector3 cPos, Vector3 aNorm, Vector3 bNorm, Vector3 cNorm, Vector2 aTex, Vector2 bTex, Vector2 cTex)
        {
            VertexPositionNormalTexture
                vA = new VertexPositionNormalTexture(aPos, aNorm, aTex),
                vB = new VertexPositionNormalTexture(bPos, bNorm, bTex),
                vC = new VertexPositionNormalTexture(cPos, cNorm, cTex);

            InternalAdd(texture, vA, vB, vC);
        }
    }
    public static class SpriteRenderer
    {
        private static readonly SpriteRenderGeometry[] spriteContext = new SpriteRenderGeometry[(byte)ModelOrigin.Max];
        private static readonly SpriteRenderGeometry[] spriteContextFlippedX = new SpriteRenderGeometry[(byte)ModelOrigin.Max];

        private static SpriteShader shader;
        private static GraphicsDevice graphicsDevice;
        private static VertexDeclaration declaration;

        public static void Prepare(GraphicsDevice device)
        {
            graphicsDevice = device;
            declaration = new VertexDeclaration(graphicsDevice, VertexPositionNormalTexture.VertexElements);
            shader = new SpriteShader(device) { World = WorldManager.WorldMatrix };

            SpriteRenderGeometry context, contextFlippedX;
            spriteContext[(byte)ModelOrigin.Center] = context = new SpriteRenderGeometry(2);
            spriteContextFlippedX[(byte)ModelOrigin.Center] = contextFlippedX = new SpriteRenderGeometry(2);
            {
                Vector3 nn = new Vector3(0, -0.5f, -0.5f);
                Vector3 np = new Vector3(0, -0.5f, 0.5f);
                Vector3 pp = new Vector3(0, 0.5f, 0.5f);
                Vector3 pn = new Vector3(0, 0.5f, -0.5f);
                Vector3 nnN = -Vector3.UnitX;
                Vector3 npN = -Vector3.UnitX;
                Vector3 ppN = -Vector3.UnitX;
                Vector3 pnN = -Vector3.UnitX;
                context.Add(0, nn, np, pp, nnN, npN, ppN, Vector2.UnitY, Vector2.Zero, Vector2.UnitX);
                context.Add(0, nn, pp, pn, nnN, ppN, pnN, Vector2.UnitY, Vector2.UnitX, Vector2.One);
                context.UpdateBuffer(device);
                contextFlippedX.Add(0, nn, np, pp, nnN, npN, ppN, Vector2.One, Vector2.UnitX, Vector2.Zero);
                contextFlippedX.Add(0, nn, pp, pn, nnN, ppN, pnN, Vector2.One, Vector2.Zero, Vector2.UnitY);
                contextFlippedX.UpdateBuffer(device);
            }
            spriteContext[(byte)ModelOrigin.Bottom] = context = new SpriteRenderGeometry(2);
            spriteContextFlippedX[(byte)ModelOrigin.Bottom] = contextFlippedX = new SpriteRenderGeometry(2);
            {
                Vector3 nn = new Vector3(0, -0.5f, 0);
                Vector3 np = new Vector3(0, -0.5f, 1);
                Vector3 pp = new Vector3(0, 0.5f, 1);
                Vector3 pn = new Vector3(0, 0.5f, 0);
                Vector3 nnN = -Vector3.UnitX;
                Vector3 npN = -Vector3.UnitX;
                Vector3 ppN = -Vector3.UnitX;
                Vector3 pnN = -Vector3.UnitX;
                context.Add(0, nn, np, pp, nnN, npN, ppN, Vector2.UnitY, Vector2.Zero, Vector2.UnitX);
                context.Add(0, nn, pp, pn, nnN, ppN, pnN, Vector2.UnitY, Vector2.UnitX, Vector2.One);
                context.UpdateBuffer(device);
                contextFlippedX.Add(0, nn, np, pp, nnN, npN, ppN, Vector2.One, Vector2.UnitX, Vector2.Zero);
                contextFlippedX.Add(0, nn, pp, pn, nnN, ppN, pnN, Vector2.One, Vector2.Zero, Vector2.UnitY);
                contextFlippedX.UpdateBuffer(device);
            }
            spriteContext[(byte)ModelOrigin.Top] = context = new SpriteRenderGeometry(2);
            spriteContextFlippedX[(byte)ModelOrigin.Top] = contextFlippedX = new SpriteRenderGeometry(2);
            {
                Vector3 nn = new Vector3(0, -0.5f, -1);
                Vector3 np = new Vector3(0, -0.5f, 0);
                Vector3 pp = new Vector3(0, 0.5f, 0);
                Vector3 pn = new Vector3(0, 0.5f, -1);
                Vector3 nnN = -Vector3.UnitX;
                Vector3 npN = -Vector3.UnitX;
                Vector3 ppN = -Vector3.UnitX;
                Vector3 pnN = -Vector3.UnitX;
                context.Add(0, nn, np, pp, nnN, npN, ppN, Vector2.UnitY, Vector2.Zero, Vector2.UnitX);
                context.Add(0, nn, pp, pn, nnN, ppN, pnN, Vector2.UnitY, Vector2.UnitX, Vector2.One);
                context.UpdateBuffer(device);
                contextFlippedX.Add(0, nn, np, pp, nnN, npN, ppN, Vector2.One, Vector2.UnitX, Vector2.Zero);
                contextFlippedX.Add(0, nn, pp, pn, nnN, ppN, pnN, Vector2.One, Vector2.Zero, Vector2.UnitY);
                contextFlippedX.UpdateBuffer(device);
            }
        }
        private static void Activate()
        {
            if (RenderManager.ActiveRenderer != Renderers.SpriteRenderer)
            {
                graphicsDevice.VertexDeclaration = declaration;
                graphicsDevice.RenderState.FillMode = FillMode.Solid;
                graphicsDevice.RenderState.DepthBufferEnable = true;
                graphicsDevice.RenderState.AlphaBlendEnable = true;
                graphicsDevice.RenderState.SourceBlend = Blend.SourceAlpha;
                graphicsDevice.RenderState.DestinationBlend = Blend.InverseSourceAlpha;
                graphicsDevice.RenderState.AlphaTestEnable = true;
                graphicsDevice.RenderState.AlphaFunction = CompareFunction.GreaterEqual;
                graphicsDevice.RenderState.ReferenceAlpha = 255;
                RenderManager.SwitchShader(Renderers.SpriteRenderer, shader);
            }
        }

        public static void UpdateOrtho(Vector2 location, Vector2 size)
        {
            Vector3 target = new Vector3(location + size / 2, 0);
            shader.View = Matrix.CreateLookAt(target, target + Vector3.UnitZ, -Vector3.UnitY);
            shader.Projection = Matrix.CreateTranslation(-0.5f, 0.5f, 0) * Matrix.CreateOrthographic(size.X, size.Y, 0.1f, 10000);
            RenderManager.ApplyShaderChanges();
        }
        
        public static void Render(WorldObjectBase wo, Camera camera)
        {
            if (wo == null)
                return;
            if (wo.Model == null)
                return;
            switch (wo.Model.Model.Type)
            {
                case ModelType.SpriteModel:
                case ModelType.MultiAngleSpriteModel:
                    break;
                default:
                    return;
            }

            Vector3 size;
            Vector3 offset;
            wo.GetRenderInfo(out offset, out size);

            switch (wo.Model.Model.Type)
            {
                case ModelType.SpriteModel:
                {
                    SpriteModelInstance model = wo.Model as SpriteModelInstance;
                    Render(model, camera, wo.Position + offset, size);
                    break;
                }
                case ModelType.MultiAngleSpriteModel:
                {
                    MultiAngleSpriteModelInstance model = wo.Model as MultiAngleSpriteModelInstance;
                    Rotation rotation = wo.Rotation;
                    if (wo.Type.IsPlayer())
                    {
                        if (((wo as PlayerCreature).MovementFlags & MovementFlags.Movement) != 0)
                        {
                            rotation = (wo as PlayerCreature).MovementDirection;
                            if (((wo as PlayerCreature).MovementFlags & MovementFlags.Backward) != 0)
                                rotation.Yaw += MathHelper.Pi;
                        }
                    }
                    Render(model, camera, wo.Position + offset, rotation, size);
                    break;
                }
                default:
                    return;
            }
            Render(wo.Model.ParticleSystem, camera);
        }
        public static void Render(Spell spell, Camera camera)
        {
            if (spell == null)
                return;
            for (ClientSpellState state = 0; state < ClientSpellState.Max; ++state)
                Render(spell, state, camera);
        }
        public static void Render(Spell spell, ClientSpellState state, Camera camera)
        {
            if (spell == null)
                return;
            ModelInstance model = spell.StateModels[(byte)state];
            if (spell.StateModels[(byte)state] == null)
                return;
            switch (spell.StateModels[(byte)state].Model.Type)
            {
                case ModelType.SpriteModel:
                case ModelType.EmptyModel:
                case ModelType.DecalModel:
                    break;
                default:
                    return;
            }

            Render(model as SpriteModelInstance, camera, spell.GetPosition(), spell.GetSize());
            Render(model.ParticleSystem, camera);
        }
        public static void Render(ParticleSystemInstance particleSystem, Camera camera)
        {
            if (particleSystem == null)
                return;

            foreach (ParticleEmitterInstance emitter in particleSystem.Emitters)
            {
                for (int i = 0; i < emitter.Template.ParticleLimit; ++i)
                {
                    if (emitter.Particles[i].TimeToLive > 0)
                    {
                        ParticleInstance particle = emitter.Particles[i];
                        Render(ModelOrigin.Center, particle.Texture, camera, particle.Position, new Vector3(particle.Size), false);
                    }
                }
            }
        }
        public static void Render(ModelInstance model, Camera camera, Vector3 position, Rotation rotation, Vector3 size)
        {
            if (model == null)
                return;
            switch (model.Model.Type)
            {
                case ModelType.SpriteModel:
                    Render(model as SpriteModelInstance, camera, position, size);
                    break;
                case ModelType.MultiAngleSpriteModel:
                    Render(model as MultiAngleSpriteModelInstance, camera, position, rotation, size);
                    break;
            }
        }
        private static void Render(SpriteModelInstance model, Camera camera, Vector3 position, Vector3 size)
        {
            if (model == null)
                return;
            Render(model.Origin, model.GetFrameTexture(), camera, position, model.Model.RelativeSize ? size * (model.Model as SpriteModel).Size : (model.Model as SpriteModel).Size, false);
        }
        private static void Render(MultiAngleSpriteModelInstance model, Camera camera, Vector3 position, Rotation rotation, Vector3 size)
        {
            if (model == null)
                return;
            Rotation camRot = camera.Direction;

            bool flipX;
            int texture = model.GetFrameTexture(rotation.Yaw - camRot.Yaw + MathHelper.Pi, out flipX);
            Render(model.Origin, texture, camera, position, model.Model.RelativeSize ? size * (model.Model as MultiAngleSpriteModel).Size : (model.Model as MultiAngleSpriteModel).Size, flipX);
        }
        private static void Render(ModelOrigin origin, int texture, Camera camera, Vector3 position, Vector3 size, bool flipX)
        {
            Rotation camRot = camera.Direction;

            Activate();

            Push();
            shader.World *= Matrix.CreateRotationY(camRot.Pitch) * Matrix.CreateRotationZ(camRot.Yaw + MathHelper.Pi) * Matrix.CreateScale(size) * Matrix.CreateTranslation(position);

            shader.Texture = DatabaseManager.TextureData[texture];

            SpriteRenderGeometry spriteGeometry = (flipX ? spriteContextFlippedX[(byte)origin] : spriteContext[(byte)origin]);
            graphicsDevice.Vertices[0].SetSource(spriteGeometry.VertexBuffer, 0, VertexPositionNormalTexture.SizeInBytes);
            graphicsDevice.Indices = spriteGeometry.IndexBuffer;
            RenderManager.ApplyShaderChanges();
            graphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, 6, 0, 2);

            Pop();
        }

        private static readonly Stack<Matrix> matrixStack = new Stack<Matrix>();
        private static void Push()
        {
            matrixStack.Push(shader.World);
        }
        private static void Pop()
        {
            shader.World = matrixStack.Pop();
            RenderManager.ApplyShaderChanges();
        }
    }
}