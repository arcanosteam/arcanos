﻿using System;
using Arcanos.Client.GameClient.Objects;
using Arcanos.Client.GameClient.Shaders;
using Arcanos.Client.GameClient.World;
using Arcanos.Shared.Objects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Arcanos.Client.GameClient.Rendering
{
    public enum DebugRenderShape
    {
        Point,
        Axis,
        Diamond,
        BoundingBox,
        ObjectShape,
        Wireframe,
    }
    public static class DebugRenderer
    {
        private static DebugShader shader;
        private static GraphicsDevice graphicsDevice;
        private static DebugRenderShape currentShape;
        private static VertexDeclaration declaration;

        public static void Prepare(GraphicsDevice device)
        {
            graphicsDevice = device;
            declaration = new VertexDeclaration(graphicsDevice, VertexPositionColor.VertexElements);
            shader = new DebugShader(device) { World = WorldManager.WorldMatrix };
        }
        private static void Activate(DebugRenderShape shape)
        {
            bool force = false;
            if (RenderManager.ActiveRenderer != Renderers.DebugRenderer)
            {
                graphicsDevice.VertexDeclaration = declaration;
                graphicsDevice.RenderState.DepthBufferEnable = true;
                RenderManager.SwitchShader(Renderers.DebugRenderer, shader);
                force = true;
            }
            if (currentShape != shape || force)
            {
                switch (currentShape = shape)
                {
                    case DebugRenderShape.Point:
                        graphicsDevice.RenderState.FillMode = FillMode.Point;
                        break;
                    case DebugRenderShape.Axis:
                    case DebugRenderShape.Diamond:
                    case DebugRenderShape.BoundingBox:
                    case DebugRenderShape.ObjectShape:
                        graphicsDevice.RenderState.FillMode = FillMode.Solid;
                        break;
                    case DebugRenderShape.Wireframe:
                        graphicsDevice.RenderState.FillMode = FillMode.WireFrame;
                        break;
                }
            }
        }

        public static void UpdateCamera(Camera camera)
        {
            shader.View = camera.View;
            shader.Projection = camera.Projection;
            RenderManager.ApplyShaderChanges();
        }
        public static void SetWorld(Matrix matrix)
        {
            shader.World = matrix;
            RenderManager.ApplyShaderChanges();
        }
        public static void SetView(Matrix matrix)
        {
            shader.View = matrix;
            RenderManager.ApplyShaderChanges();
        }
        public static void SetProjection(Matrix matrix)
        {
            shader.Projection = matrix;
            RenderManager.ApplyShaderChanges();
        }
        
        public static void Render(WorldObjectBase wo)
        {
            Render(wo, currentShape);
        }
        public static void Render(WorldObjectBase wo, DebugRenderShape shape)
        {
            Activate(shape);
            Color c = Color.White;
            switch (shape)
            {
                case DebugRenderShape.Point:
                    graphicsDevice.DrawUserPrimitives(PrimitiveType.PointList, new[]
                    {
                        new VertexPositionColor(wo.Position, c)
                    }, 0, 1);
                    break;
                case DebugRenderShape.Axis:
                {
                    Vector3 min = Vector3.Subtract(wo.Position, wo.Size / 2);
                    Vector3 ctr = wo.Position;
                    Vector3 max = Vector3.Add(wo.Position, wo.Size / 2);
                    graphicsDevice.DrawUserPrimitives(PrimitiveType.LineList, new[]
                    {
                        new VertexPositionColor(new Vector3(min.X, ctr.Y, ctr.Z), Color.Red),
                        new VertexPositionColor(new Vector3(max.X, ctr.Y, ctr.Z), Color.Red),
                        new VertexPositionColor(new Vector3(ctr.X, min.Y, ctr.Z), Color.Green),
                        new VertexPositionColor(new Vector3(ctr.X, max.Y, ctr.Z), Color.Green),
                        new VertexPositionColor(new Vector3(ctr.X, ctr.Y, min.Z), Color.Blue),
                        new VertexPositionColor(new Vector3(ctr.X, ctr.Y, max.Z), Color.Blue),
                    }, 0, 3);
                    break;
                }
                case DebugRenderShape.Diamond:
                    throw new NotImplementedException();
                case DebugRenderShape.BoundingBox:
                {
                    Vector3 min;
                    Vector3 max;
                    wo.GetVolume(out min, out max);
                    graphicsDevice.DrawUserPrimitives(PrimitiveType.LineList, new[]
                    {
                        new VertexPositionColor(new Vector3(min.X, min.Y, min.Z), c),
                        new VertexPositionColor(new Vector3(max.X, min.Y, min.Z), c),
                        new VertexPositionColor(new Vector3(max.X, min.Y, min.Z), c),
                        new VertexPositionColor(new Vector3(max.X, max.Y, min.Z), c),
                        new VertexPositionColor(new Vector3(max.X, max.Y, min.Z), c),
                        new VertexPositionColor(new Vector3(min.X, max.Y, min.Z), c),
                        new VertexPositionColor(new Vector3(min.X, max.Y, min.Z), c),
                        new VertexPositionColor(new Vector3(min.X, min.Y, min.Z), c),

                        new VertexPositionColor(new Vector3(min.X, min.Y, min.Z), c),
                        new VertexPositionColor(new Vector3(min.X, min.Y, max.Z), c),
                        new VertexPositionColor(new Vector3(max.X, min.Y, min.Z), c),
                        new VertexPositionColor(new Vector3(max.X, min.Y, max.Z), c),
                        new VertexPositionColor(new Vector3(max.X, max.Y, min.Z), c),
                        new VertexPositionColor(new Vector3(max.X, max.Y, max.Z), c),
                        new VertexPositionColor(new Vector3(min.X, max.Y, min.Z), c),
                        new VertexPositionColor(new Vector3(min.X, max.Y, max.Z), c),
                        
                        new VertexPositionColor(new Vector3(min.X, min.Y, max.Z), c),
                        new VertexPositionColor(new Vector3(max.X, min.Y, max.Z), c),
                        new VertexPositionColor(new Vector3(max.X, min.Y, max.Z), c),
                        new VertexPositionColor(new Vector3(max.X, max.Y, max.Z), c),
                        new VertexPositionColor(new Vector3(max.X, max.Y, max.Z), c),
                        new VertexPositionColor(new Vector3(min.X, max.Y, max.Z), c),
                        new VertexPositionColor(new Vector3(min.X, max.Y, max.Z), c),
                        new VertexPositionColor(new Vector3(min.X, min.Y, max.Z), c),
                    }, 0, 12);
                    break;
                }
                case DebugRenderShape.ObjectShape:
                    throw new NotImplementedException();
                case DebugRenderShape.Wireframe:
                    throw new NotImplementedException();
            }
        }
        public static void RenderDirection(WorldObjectBase wo, Color color)
        {
            Activate(DebugRenderShape.Wireframe);
            
            Vector3 min;
            Vector3 max;
            wo.GetVolume(out min, out max);
            float dist = Math.Abs(max.X - min.X);

            Vector3 pos = wo.Position;
            Vector3 end = pos + wo.Rotation.Forward * dist * 2;
            Vector3 a1 = end + wo.Rotation.Backward * dist / 3 + wo.Rotation.Left * dist / 4;
            Vector3 a2 = end + wo.Rotation.Backward * dist / 3 + wo.Rotation.Right * dist / 4;
            graphicsDevice.DrawUserPrimitives(PrimitiveType.LineStrip, new[]
            {
                new VertexPositionColor(pos, color),
                new VertexPositionColor(end, color),
                new VertexPositionColor(a1, color),
                new VertexPositionColor(a2, color),
                new VertexPositionColor(end, color),
            }, 0, 4);
        }
        public static void RenderLine(Vector3 a, Vector3 b, Color color)
        {
            Activate(DebugRenderShape.Wireframe);
            graphicsDevice.DrawUserPrimitives(PrimitiveType.LineList, new[]
            {
                new VertexPositionColor(a, color),
                new VertexPositionColor(b, color),
            }, 0, 1);
        }
    }
}