﻿using System;
using System.Collections.Generic;
using Arcanos.Shared.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Arcanos.Client.GameClient.Rendering
{
    public struct VertexPositionNormalTextureTangentBinormal
    {
        public readonly Vector3 Position;
        public readonly Vector3 Normal;
        public readonly Vector2 TextureCoordinate;
        public readonly Vector3 Tangent;
        public readonly Vector3 Binormal;
        public static readonly VertexElement[] VertexElements =
        {
            new VertexElement(0, 0, VertexElementFormat.Vector3, VertexElementMethod.Default, VertexElementUsage.Position, 0),
            new VertexElement(0, sizeof(float) * 3, VertexElementFormat.Vector3, VertexElementMethod.Default, VertexElementUsage.Normal, 0),
            new VertexElement(0, sizeof(float) * 6, VertexElementFormat.Vector2, VertexElementMethod.Default, VertexElementUsage.TextureCoordinate, 0),
            new VertexElement(0, sizeof(float) * 8, VertexElementFormat.Vector3, VertexElementMethod.Default, VertexElementUsage.Tangent, 0),
            new VertexElement(0, sizeof(float) * 11, VertexElementFormat.Vector3, VertexElementMethod.Default, VertexElementUsage.Binormal, 0),
        };

        public static int SizeInBytes
        {
            get { return sizeof(float) * 14; }
        }

        public VertexPositionNormalTextureTangentBinormal(Vector3 position, Vector3 normal, Vector2 textureCoordinate, Vector3 tangent, Vector3 binormal)
        {
            Position = position;
            Normal = normal;
            TextureCoordinate = textureCoordinate;
            Tangent = tangent;
            Binormal = binormal;
        }

        public static bool operator ==(VertexPositionNormalTextureTangentBinormal a, VertexPositionNormalTextureTangentBinormal b)
        {
            return a.Position.Equals(b.Position);
        }
        public static bool operator !=(VertexPositionNormalTextureTangentBinormal a, VertexPositionNormalTextureTangentBinormal b)
        {
            return !a.Position.Equals(b.Position);
        }
        public bool Equals(VertexPositionNormalTextureTangentBinormal other)
        {
            return Position.Equals(other.Position);
        }
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is VertexPositionNormalTextureTangentBinormal && Equals((VertexPositionNormalTextureTangentBinormal)obj);
        }
        public override int GetHashCode()
        {
            return Position.GetHashCode();
        }
    }
    public abstract class RenderGeometry<T, TVertex> : IRenderGeometry<T> where TVertex : struct
    {
        public TVertex[] Geometry;
        public int[] Index;
        public readonly Dictionary<T, int> TextureStart = new Dictionary<T, int>();
        public readonly Dictionary<T, int> TextureLength = new Dictionary<T, int>();
        public VertexBuffer VertexBuffer;
        public IndexBuffer IndexBuffer;

        public int GeometryCount
        {
            get { return geomLen; }
        }
        public int IndexesCount
        {
            get { return indexLen; }
        }

        protected int geomLen, indexLen;
        protected T[] TextureIndexes;

        protected RenderGeometry() : this(0) { }
        protected RenderGeometry(int capacity)
        {
            Geometry = new TVertex[capacity * 3];
            Index = new int[capacity * 3];
        }

        protected void InternalAdd(T texture, TVertex vA, TVertex vB, TVertex vC)
        {
            int dataEnd = geomLen;
            // Check whether new vertexes should be added to the geometry array,
            // or just store their positions if they are already present in the array
            bool vAe = false,
                 vBe = false,
                 vCe = false;
            int vAi = dataEnd + 0,
                vBi = dataEnd + 1,
                vCi = dataEnd + 2;
            for (int i = 0; i < dataEnd; i++)
            {
                if (Geometry[i].Equals(vA))
                {
                    vAe = true;
                    vAi = i;
                }
                if (Geometry[i].Equals(vB))
                {
                    vBe = true;
                    vBi = i;
                }
                if (Geometry[i].Equals(vC))
                {
                    vCe = true;
                    vCi = i;
                }
            }
            // If at least one vertex needs to be added
            int needed = (vAe ? 0 : 1) + (vBe ? 0 : 1) + (vCe ? 0 : 1);
            if (needed > 0)
            {
                // Resize the geometry array to fit new vertexes
                if (dataEnd + needed >= Geometry.Length)
                    Array.Resize(ref Geometry, dataEnd + needed);
                // Add them at appropriate positions
                int i = dataEnd;
                if (!vAe)
                    Geometry[vAi = i++] = vA;
                if (!vBe)
                    Geometry[vBi = i++] = vB;
                if (!vCe)
                    Geometry[vCi = i++] = vC;
                geomLen = i;
            }
            // Index array needs to be resized in any way
            if (indexLen + 3 >= Index.Length)
                Array.Resize(ref Index, Index.Length + 3);
            int start;
            // If the index for the current texture was already defined
            if (TextureStart.TryGetValue(texture, out start))
            {
                start += TextureLength[texture];
                // Shift all index values towards the end of the array
                if (indexLen - 1 >= start)
                {
                    for (int i = indexLen - 1; i >= start; i--)
                        Index[i + 3] = Index[i];

                    for (int i = 0; i < TextureIndexes.Length; i++)
                    {
                        T key = TextureIndexes[i];
                        int val = TextureStart[key];
                        if (val >= start)
                            TextureStart[key] = val + 3;
                    }
                }
                TextureLength[texture] += 3;
            }
            else
            {
                start = indexLen;
                TextureStart.Add(texture, start);
                TextureLength.Add(texture, 3);
                TextureIndexes = new T[TextureStart.Count];
                TextureStart.Keys.CopyTo(TextureIndexes, 0);
            }
            // Fill the index array with vertex indexes
            Index[start + 0] = vAi;
            Index[start + 1] = vBi;
            Index[start + 2] = vCi;
            indexLen += 3;
        }

        /// <summary>
        /// Adds a textured triangle to the geometry and index arrays for this RenderData.
        /// </summary>
        public abstract void Add(T texture, Vector3 aPos, Vector3 bPos, Vector3 cPos, Vector3 aNorm, Vector3 bNorm, Vector3 cNorm, Vector2 aTex, Vector2 bTex, Vector2 cTex);

        private GraphicsDevice deviceCache;
        public void UpdateBuffer(GraphicsDevice device)
        {
            deviceCache = device;
            UpdateBuffer();
        }
        public void UpdateBuffer()
        {
            if (deviceCache == null)
                return;
            if (VertexBuffer != null)
                VertexBuffer.Dispose();
            VertexBuffer = new VertexBuffer(deviceCache, typeof(TVertex), Geometry.Length, BufferUsage.Points);
            VertexBuffer.SetData(Geometry);
            if (IndexBuffer != null)
                IndexBuffer.Dispose();
            IndexBuffer = new IndexBuffer(deviceCache, typeof(int), Index.Length, BufferUsage.Points);
            IndexBuffer.SetData(Index);
        }
    }
}