﻿using Arcanos.Shared.Data;
using Arcanos.Shared.Movement;
using Arcanos.Shared.Objects;

namespace Arcanos.Client.GameClient.Movement
{
    public class Mover : MoverBase, IPreDeserialization
    {
        public Mover(CreatureBase owner) : base(owner) { }

        public void StartedDeserialization()
        {
            MovementType = null;
        }
    }
}