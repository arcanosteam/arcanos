using System.Diagnostics;
using Arcanos.Utilities;

namespace Arcanos.Client.GameClient
{
    static class Program
    {
        static void Main(string[] args)
        {
            ArcanosGameClient.LaunchParams launchParams = new ArcanosGameClient.LaunchParams
            {
                URL = null,
                Username = "testaccount",
                Password = "testpass",
                LocaleCulture = "en-US",
                FPSLimit = 60,
            };
            for (int i = 0; i < args.Length; i++)
            {
                switch (args[i].ToLowerInvariant())
                {
                    case "-u":
                    case "-user":
                    case "-username":
                        launchParams.Username = args[++i];
                        break;
                    case "-p":
                    case "-pass":
                    case "-password":
                        launchParams.Password = args[++i];
                        break;
                    case "-a":
                    case "-addr":
                    case "-address":
                    case "-url":
                        launchParams.URL = args[++i];
                        break;
                    case "-l":
                    case "-loc":
                    case "-locale":
                    case "-lang":
                    case "-language":
                        launchParams.LocaleCulture = args[++i];
                        break;
                    case "-asynctex":
                    case "-asynctexture":
                        launchParams.UseAsyncTextureLoading = true;
                        break;
                    case "-noboost":
                        Process.GetCurrentProcess().PriorityBoostEnabled = false;
                        break;
                    case "-realtime":
                        Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.RealTime;
                        break;
                    case "-fps":
                    case "-fpslimit":
                        launchParams.FPSLimit = int.Parse(args[++i]);
                        break;
                    default:
                        Logger.Error(LogCategory.General, "Unknown argument: {0}", args[i]);
                        break;
                }
            }
            using (ArcanosGameClient game = new ArcanosGameClient(launchParams))
                game.Run();
        }
    }
}

