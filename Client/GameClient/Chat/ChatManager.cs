﻿using System;
using System.Collections.Generic;
using System.Text;
using Arcanos.Client.GameClient.UI;
using Arcanos.Client.GameClient.UI.Game;
using Arcanos.Client.GameClient.World;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Net;
using Arcanos.Shared.Timing;
using Microsoft.Xna.Framework.Graphics;

namespace Arcanos.Client.GameClient.Chat
{
    public static class ChatManager
    {
        private class WhisperChatChannel : ChatChannel
        {
            public string Listener;
            public WhisperChatChannel(string listener)
            {
                Listener = listener;
                Color = Color.Purple;
            }

            public override string GetNameForLocale(LocaleIndex locale)
            {
                return "> " + Listener;
            }
        }

        public static readonly Dictionary<int, ChatChannel> JoinedChannels = new Dictionary<int, ChatChannel>();
        public static ChatChannel ActiveChannel;

        static ChatManager()
        {
            JoinedChannels.Add(-1, new WhisperChatChannel(null));
        }

        public static ChatChannel CreateChannel(int id, Text name, Text alias, string customName, Color color)
        {
            return new ChatChannel
            {
                ID = id,
                Name = name,
                Alias = alias,
                CustomName = customName,
                Color = color,
            };
        }
        public static void Join(ChatChannel channel)
        {
            JoinedChannels.Add(channel.ID, channel);
            UIManager.RaiseEvent(UIEvents.ChatChannelJoined, null, channel.ID, channel.GetNameForLocale(Localization.CurrentLocale));
            if (ActiveChannel == null)
                PickActiveChannel();
        }
        public static void Leave(int id)
        {
            ChatChannel channel;
            if (!JoinedChannels.TryGetValue(id, out channel))
                return;

            JoinedChannels.Remove(id);
            UIManager.RaiseEvent(UIEvents.ChatChannelLeft, null, channel.ID, channel.GetNameForLocale(Localization.CurrentLocale));
            if (ActiveChannel == channel)
                PickActiveChannel();
        }

        public static ChatChannel GetChannelByName(string name)
        {
            foreach (KeyValuePair<int, ChatChannel> channel in JoinedChannels)
            {
                if (!string.IsNullOrEmpty(channel.Value.CustomName))
                    if (string.Compare(name, channel.Value.CustomName, StringComparison.CurrentCultureIgnoreCase) == 0)
                        return channel.Value;
                if (channel.Value.Name != null)
                    foreach (string channelName in channel.Value.Name.LocalizedStrings)
                        if (string.Compare(name, channelName, StringComparison.CurrentCultureIgnoreCase) == 0)
                            return channel.Value;
            }

            return null;
        }
        public static ChatChannel GetChannelByAlias(string alias)
        {
            alias = alias.TrimStart('/');

            foreach (KeyValuePair<int, ChatChannel> channel in JoinedChannels)
            {
                if (!string.IsNullOrEmpty(channel.Value.CustomName))
                    if (string.Compare(alias, channel.Value.CustomName, StringComparison.CurrentCultureIgnoreCase) == 0)
                        return channel.Value;
                if (channel.Value.Alias != null)
                    foreach (string channelAlias in channel.Value.Alias.LocalizedStrings)
                        if (string.Compare(alias, channelAlias, StringComparison.CurrentCultureIgnoreCase) == 0)
                            return channel.Value;
            }

            return null;
        }
        public static void SetActiveChannel(int id)
        {
            ChatChannel channel;
            if (!JoinedChannels.TryGetValue(id, out channel))
            {
                UIManager.RaiseEvent(UIEvents.GameError, null, Localization.Game.ErrorUnexistingChatChannel);
                return;
            }
            ActiveChannel = channel;
            UIManager.RaiseEvent(UIEvents.ChatActiveChannelChanged, null, ActiveChannel.ID, ActiveChannel.Name);
        }
        public static void SetActiveWhisper(string listener)
        {
            ActiveChannel = JoinedChannels[-1];
            (ActiveChannel as WhisperChatChannel).Listener = listener;
            UIManager.RaiseEvent(UIEvents.ChatActiveChannelChanged, null, ActiveChannel.ID, ActiveChannel.Name);
        }
        public static bool PickActiveChannel()
        {
            foreach (KeyValuePair<int, ChatChannel> joinedChannel in JoinedChannels)
            {
                if (joinedChannel.Key == -1)
                    continue;
                SetActiveChannel(joinedChannel.Key);
                return true;
            }
            ActiveChannel = null;
            UIManager.RaiseEvent(UIEvents.ChatActiveChannelChanged, null, 0, "");
            return false;
        }

        public static void SendMessage(string message)
        {
            message = message.Trim();
            if (message.Length == 0)
                return;

            if (ActiveChannel == null)
                if (!PickActiveChannel())
                    return;

            if (ActiveChannel is WhisperChatChannel)
            {
                WorldManager.PlayerSession.SendPacket(new CChatWhisperPacket
                {
                    TargetName = (ActiveChannel as WhisperChatChannel).Listener,
                    Timestamp = Time.Timestamp,
                    Flags = 0,
                }, Encoding.UTF8.GetBytes(message));
            }
            else
            {
                WorldManager.PlayerSession.SendPacket(new CChatMessagePacket
                {
                    ChannelID = ActiveChannel.ID,
                    Timestamp = Time.Timestamp,
                    Flags = 0,
                }, Encoding.UTF8.GetBytes(message));
            }
        }

        public static void PrintMessage(string format, params object[] args)
        {
            PrintMessage(Color.Yellow, format, args);
        }
        public static void PrintMessage(Color color, string format, params object[] args)
        {
            PrintMessage(color, string.Format(format, args));
        }
        public static void PrintMessage(string message)
        {
            PrintMessage(Color.Yellow, message);
        }
        public static UIText PrintRichMessage(params ITextToken[] tokens)
        {
            return PrintRichMessage(Color.Yellow, tokens);
        }
        public static UIText PrintRichMessage(Color color, params ITextToken[] tokens)
        {
            UIChatFrame frame = UIManager.CurrentDesktop.GetByKey<UIChatFrame>("ChatFrame");
            if (frame != null)
                return frame.AddLine(color, tokens);
            return null;
        }
        public static void PrintMessage(Color color, string message)
        {
            UIChatFrame frame = UIManager.CurrentDesktop.GetByKey<UIChatFrame>("ChatFrame");
            if (frame != null)
                frame.AddLine(color, message);
        }
    }
}