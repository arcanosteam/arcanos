﻿using Arcanos.Shared.Localization;
using Microsoft.Xna.Framework.Graphics;

namespace Arcanos.Client.GameClient.Chat
{
    public class ChatChannel
    {
        public int ID;
        public Text Name;
        public Text Alias;
        public string CustomName;
        public Color Color;

        public virtual string GetNameForLocale(LocaleIndex locale)
        {
            return CustomName ?? Name.LocalizedStrings[(byte)locale];
        }
    }
}