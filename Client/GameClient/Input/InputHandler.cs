﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Arcanos.Client.GameClient.Chat;
using Arcanos.Client.GameClient.Data;
using Arcanos.Client.GameClient.Items;
using Arcanos.Client.GameClient.Objects;
using Arcanos.Client.GameClient.Rendering;
using Arcanos.Client.GameClient.Spells;
using Arcanos.Client.GameClient.UI;
using Arcanos.Client.GameClient.UI.Game;
using Arcanos.Client.GameClient.World;
using Arcanos.Shared;
using Arcanos.Shared.Items;
using Arcanos.Shared.Movement;
using Arcanos.Shared.Net;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Spells;
using Arcanos.Shared.World;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Arcanos.Client.GameClient.Input
{
    public static class InputHandler
    {
        private static Vector2 leftPressedLocation;
        private static Vector2 rightPressedLocation;
        private static SpellTemplate groundTargetingSpell;
        private static CreatureBase groundTargetingCaster;
        private static int groundTargetDecalIndex = -1;

        public static void KeyPress(Keys key)
        {
            if (!WorldManager.IsCharacterInGame)
                return;

            WorldManager.Camera.KeyPressed(key);
            MovementFlags flag;
            if (IsDirectionalMovement(key, out flag))
                WorldManager.PlayerCreature.StartMovement(flag | MovementFlags.Walk);
            if (key == Keys.I)
                InviteToParty(WorldManager.PlayerCreature.Target as PlayerCreature);
            if (key == Keys.U)
                LeaveParty();
            if (key == Keys.K)
                RequestDuel(WorldManager.PlayerCreature.Target as PlayerCreature);
            if (key == Keys.Enter)
                UIManager.CurrentDesktop.GetByKey<UIChatInputBox>("ChatInputBox").Focus();
            if (key == Keys.OemQuestion)
            {
                UIChatInputBox input = UIManager.CurrentDesktop.GetByKey<UIChatInputBox>("ChatInputBox");
                input.Text = "/";
                input.Focus();
            }
            if (key == Keys.OemPeriod)
            {
                UIChatInputBox input = UIManager.CurrentDesktop.GetByKey<UIChatInputBox>("ChatInputBox");
                input.Text = ".";
                input.Focus();
            }
        }
        public static void KeyRelease(Keys key)
        {
            if (!WorldManager.IsCharacterInGame)
                return;

            WorldManager.Camera.KeyReleased(key);

            MovementFlags flag;
            if (IsDirectionalMovement(key, out flag))
                WorldManager.PlayerCreature.StopMovement(flag);
        }
        public static void HookMouse(UIDesktop desktop)
        {
            desktop.OnMouseLeft += (sender, args) => HandleMouseLeftState(args.NewValue && !args.OldValue);
            desktop.OnMouseRight += (sender, args) => HandleMouseRightState(args.NewValue && !args.OldValue);
            desktop.OnMouseMove += (sender, args) => HandleMouseMove(args.OldValue, args.NewValue);
            desktop.OnMouseScroll += (sender, args) => WorldManager.Camera.MouseScrolled(args.NewValue);
        }

        public static void HandleMouseLeftState(bool pressed)
        {
            if (!WorldManager.IsCharacterInGame)
                return;

            if (pressed)
            {
                leftPressedLocation = new Vector2(InputManager.MouseX, InputManager.MouseY);
                WorldManager.Camera.MouseKeyPressed(MouseKeys.Left);
            }
            else
            {
                WorldManager.Camera.MouseKeyReleased(MouseKeys.Left);
                Vector2 leftReleasedLocation = new Vector2(InputManager.MouseX, InputManager.MouseY);
                if (Vector2.Distance(leftPressedLocation, leftReleasedLocation) <= 4)
                {
                    if (groundTargetingSpell != null)
                    {
                        Vector3? groundTarget = GetGroundTargetPosition();
                        if (groundTarget != null)
                            (groundTargetingCaster as PlayerCreature).CastSpell(groundTargetingSpell, groundTarget.Value);
                        StopGroundTargeting();
                        return;
                    }
                    foreach (WorldObjectBase wo in TestMouseHover(WorldManager.Camera, ObjectType.All))
                    {
                        if (wo == WorldManager.PlayerCreature)
                            continue;
                        HandleLeftClickOn(wo);
                        break;
                    }
                }
            }
        }
        public static void HandleMouseRightState(bool pressed)
        {
            if (!WorldManager.IsCharacterInGame)
                return;

            if (pressed)
            {
                rightPressedLocation = new Vector2(InputManager.MouseX, InputManager.MouseY);
                WorldManager.Camera.MouseKeyPressed(MouseKeys.Right);
            }
            else
            {
                WorldManager.Camera.MouseKeyReleased(MouseKeys.Right);
                Vector2 rightReleasedLocation = new Vector2(InputManager.MouseX, InputManager.MouseY);
                if (Vector2.Distance(rightPressedLocation, rightReleasedLocation) <= 4)
                {
                    foreach (WorldObjectBase wo in TestMouseHover(WorldManager.Camera, ObjectType.All))
                    {
                        if (wo == WorldManager.PlayerCreature)
                            continue;
                        HandleRightClickOn(wo);
                        break;
                    }
                }
            }
        }
        public static void HandleMouseMove(Vector2 oldLoc, Vector2 newLoc)
        {
            if (!WorldManager.IsCharacterInGame)
                return;

            WorldManager.Camera.MouseMoved(newLoc);
            if (groundTargetDecalIndex != -1)
            {
                Vector3? groundTarget = TestTerrainHover(WorldManager.Camera);
                if (groundTarget != null)
                    TerrainRenderer.MoveDecal(groundTargetDecalIndex, groundTarget.Value);
            }
        }
        public static void HandleLeftClickOn(WorldObjectBase wo)
        {
            if (!WorldManager.IsCharacterInGame)
                return;

            if (wo.Type.IsCreature() && WorldManager.PlayerCreature.Target != wo)
            {
                WorldManager.PlayerCreature.SetTarget(wo as CreatureBase);
                WorldManager.PlayerSession.SendPacket(new CTargetUpdatePacket { TargetGUID = wo.GUID });
            }
        }
        public static void HandleRightClickOn(WorldObjectBase wo)
        {
            if (!WorldManager.IsCharacterInGame)
                return;

            if (wo.Type.IsCreature())
            {
                CreatureBase creature = wo as CreatureBase;
                WorldManager.PlayerCreature.SetTarget(creature);
                WorldManager.PlayerSession.SendPacket(new CTargetUpdatePacket { TargetGUID = creature.GUID });
                if (creature != WorldManager.PlayerCreature)
                {
                    if (creature.IsAlive)
                    {
                        if (WorldManager.PlayerCreature.GetReactionTo(creature) == Reaction.Friendly)
                        {
                            if (creature.HasFlag(ObjectFlags.HasDialog) &&
                                WorldManager.PlayerCreature.DistanceTo2D(creature) <= MapTemplateBase.INTERACT_DISTANCE &&
                                creature.IsAlive)
                                WorldManager.PlayerSession.SendPacket(new CStartDialogPacket { TalkerGUID = creature.GUID });
                        }
                        else
                        {
                            WorldManager.PlayerSession.SendPacket(new CCombatStartMeleeAttackPacket { TargetGUID = creature.GUID });
                        }
                    }
                    else
                    {
                        if (creature.HasFlag(ObjectFlags.Lootable) && !WorldManager.PlayerCreature.HasFlag(ObjectFlags.Looting))
                            LootCreature(creature);
                    }
                }
            }
        }

        public static void CastSpell(CreatureBase caster, SpellTemplate spell)
        {
            if (!WorldManager.IsCharacterInGame)
                return;

            if (caster.Type.IsPlayer())
            {
                if (spell.HasFlag(SpellFlags.GroundTargeted))
                    StartGroundTargeting(spell, caster);
                else
                {
                    if (spell.AcceptableTargets == SpellTargetMask.Self)
                        (caster as PlayerCreature).CastSpell(spell, caster);
                    else
                        (caster as PlayerCreature).CastSpell(spell, caster.Target);
                }
            }
        }
        public static void StartGroundTargeting(SpellTemplate spell, CreatureBase caster)
        {
            if (!WorldManager.IsCharacterInGame)
                return;

            if (groundTargetingSpell != null)
                StopGroundTargeting();
            groundTargetingSpell = spell;
            groundTargetingCaster = caster;
            Vector3? groundTarget = TestTerrainHover(WorldManager.Camera);
            groundTargetDecalIndex = TerrainRenderer.RegisterDecal(DatabaseManager.TextureData[spell.GroundTargetTexture], groundTarget == null ? caster.Position : groundTarget.Value, spell.Radius);
        }
        public static void StopGroundTargeting()
        {
            if (!WorldManager.IsCharacterInGame)
                return;
            if (groundTargetingSpell == null)
                return;

            if (groundTargetDecalIndex != -1)
            {
                TerrainRenderer.UnregisterDecal(groundTargetDecalIndex);
                groundTargetDecalIndex = -1;
            }
            groundTargetingSpell = null;
        }
        public static Vector3? GetGroundTargetPosition()
        {
            return TestTerrainHover(WorldManager.Camera);
        }

        public static void InviteToParty(PlayerCreature pc)
        {
            if (!WorldManager.IsCharacterInGame)
                return;
            if (pc == null)
                return;

            WorldManager.PlayerSession.SendPacket(new CGroupInvitePacket { InviteeGUID = WorldManager.PlayerCreature.Target.GUID });
        }
        public static void KickFromParty(PlayerCreature pc)
        {
            if (!WorldManager.IsCharacterInGame)
                return;
            if (pc == null)
                return;

            WorldManager.PlayerSession.SendPacket(new CGroupKickPacket { TargetGUID = WorldManager.PlayerCreature.Target.GUID });
        }
        public static void LeaveParty()
        {
            if (!WorldManager.IsCharacterInGame)
                return;
            if (WorldManager.Player.CurrentCharacter.Group == null)
                return;

            WorldManager.PlayerSession.SendPacket(new CLeaveGroupPacket());
        }

        public static void RequestDuel(PlayerCreature opponent)
        {
            if (!WorldManager.IsCharacterInGame)
                return;
            if (opponent == null)
                return;

            WorldManager.PlayerSession.SendPacket(new CDuelRequestPacket { OpponentGUID = opponent.GUID });
        }

        public static void LootCreature(CreatureBase target)
        {
            if (!WorldManager.IsCharacterInGame)
                return;
            if (!target.HasFlag(ObjectFlags.Lootable) || WorldManager.PlayerCreature.HasFlag(ObjectFlags.Looting))
                return;

            WorldManager.PlayerCreature.SetFlag(ObjectFlags.Looting);
            WorldManager.PlayerSession.SendPacket(new CLootCreaturePacket { GUID = target.GUID });
        }

        public static void EquipItem(Item item, EquipSlot slot)
        {
            if (!WorldManager.IsCharacterInGame)
                return;
            if (item.IsEquipped)
                return;

            WorldManager.PlayerSession.SendPacket(new CEquipItemPacket { GUID = item.GUID, Slot = slot });
        }
        public static void EquipItem(Item item)
        {
            EquipItem(item, EquipSlot.Max);
        }
        public static void UnequipItem(Item item, byte bagIndex, byte slot)
        {
            if (!WorldManager.IsCharacterInGame)
                return;
            if (!item.IsEquipped)
                return;

            WorldManager.PlayerSession.SendPacket(new CUnequipItemPacket { GUID = item.GUID, NewBagIndex = bagIndex, NewSlot = slot });
        }
        public static void UnequipItem(Item item)
        {
            if (!WorldManager.IsCharacterInGame)
                return;

            Inventory inventory = WorldManager.PlayerSession.Player.CurrentCharacter.Inventory;
            byte bag, slot;
            if (inventory.FindEmptySlot(out bag, out slot))
                UnequipItem(item, bag, slot);
        }
        public static void UseItem(Item item)
        {
            if (!WorldManager.IsCharacterInGame)
                return;

            WorldManager.PlayerSession.SendPacket(new CUseItemPacket { ItemGUID = item.GUID, TargetGUID = (WorldManager.PlayerCreature.Target ?? WorldManager.PlayerCreature).GUID });
        }
        public static void DestoryItem(Item item)
        {
            if (!WorldManager.IsCharacterInGame)
                return;

            WorldManager.PlayerSession.SendPacket(new CDestroyItemPacket { GUID = item.GUID });
        }
        public static void ItemDoubleClick(Item item)
        {
            if (!WorldManager.IsCharacterInGame)
                return;

            if (item.Template.IsEquippable)
            {
                if (item.Template.IsUsable && item.IsEquipped)
                    UseItem(item);
                else
                    EquipItem(item);
            }
            else
                UseItem(item);
        }

        public static IEnumerable<WorldObjectBase> TestMouseHover(Camera camera, ObjectType types)
        {
            if (!WorldManager.IsCharacterInGame)
                yield break;
            if (WorldManager.CurrentMap == null)
                yield break;

            Ray ray = camera.Unproject(new Vector2(InputManager.MouseX, InputManager.MouseY));

            foreach (WorldObjectBase wo in WorldManager.CurrentMap.GetVisibleObjects(WorldManager.PlayerCreature.Position))
            {
                if ((wo.Type & types) == 0 || !WorldManager.PlayerCreature.CanSee(wo))
                    continue;

                BoundingBox box;
                wo.GetVolume(out box.Min, out box.Max);
                if (box.Intersects(ray).HasValue)
                    yield return wo;
            }
        }
        public static Vector3? TestTerrainHover(Camera camera)
        {
            if (!WorldManager.IsCharacterInGame)
                return null;
            if (WorldManager.CurrentMap == null)
                return null;

            Ray ray = camera.Unproject(new Vector2(InputManager.MouseX, InputManager.MouseY));

            HeightMapData hmd = WorldManager.CurrentMap.Template.HeightMap;
            Vector3? point = null;
            float pointDist = float.MaxValue;
            for (int x = 0; x < hmd.DimX - 1; x++)
                for (int y = 0; y < hmd.DimY - 1; y++)
                {
                    Vector3 nn = hmd.Geometry[x, y];
                    Vector3 np = hmd.Geometry[x, y + 1];
                    Vector3 pp = hmd.Geometry[x + 1, y + 1];
                    Vector3 pn = hmd.Geometry[x + 1, y];
                    float dist;
                    if (RayIntersectTriangle(ray.Position, ray.Direction, np, pp, pn, out dist) ||
                        RayIntersectTriangle(ray.Position, ray.Direction, nn, np, pn, out dist))
                        if (dist < pointDist)
                        {
                            point = ray.Position + ray.Direction * dist;
                            pointDist = dist;
                        }
                }
            return point;
        }
        public static bool RayIntersectTriangle(Vector3 origin, Vector3 direction, Vector3 vert0, Vector3 vert1, Vector3 vert2, out float dist)
        {
            const float epsilon = 0.000001f;

            dist = Single.NaN;

            // Find vectors for two edges sharing vert0
            Vector3 edge1 = vert1 - vert0;
            Vector3 edge2 = vert2 - vert0;

            // Begin calculating the determinant
            Vector3 pvec = Vector3.Cross(direction, edge2);

            // If the determinant is near zero, ray lies in plane of triangle
            float determinant = Vector3.Dot(edge1, pvec);

            if (determinant > -epsilon && determinant < epsilon)
                return false;

            float invDeterminant = 1.0f / determinant;

            // Calculate distance from vert0 to ray origin
            Vector3 tvec = origin - vert0;

            // Calculate U parameter and test bounds
            float u = Vector3.Dot(tvec, pvec) * invDeterminant;
            if (u < 0.0f || u > 1.0f)
                return false;

            // Prepare to test V parameter
            Vector3 qvec = Vector3.Cross(tvec, edge1);

            // Calculate V parameter and test bounds
            float v = Vector3.Dot(direction, qvec) * invDeterminant;
            if (v < 0.0f || u + v > 1.0f)
                return false;

            dist = Vector3.Dot(edge2, qvec) * invDeterminant;

            return dist >= 0f;
        }

        private static bool IsDirectionalMovement(Keys key, out MovementFlags flag)
        {
            switch (key)
            {
                case Keys.W:
                    flag = MovementFlags.Forward;
                    return true;
                case Keys.S:
                    flag = MovementFlags.Backward;
                    return true;
                case Keys.A:
                    flag = MovementFlags.Left;
                    return true;
                case Keys.D:
                    flag = MovementFlags.Right;
                    return true;
                case Keys.Q:
                    flag = MovementFlags.RotationLeft;
                    return true;
                case Keys.E:
                    flag = MovementFlags.RotationRight;
                    return true;
                case Keys.PageUp:
                    flag = MovementFlags.RotationUp;
                    return true;
                case Keys.PageDown:
                    flag = MovementFlags.RotationDown;
                    return true;
                case Keys.Space:
                    flag = MovementFlags.Up;
                    return true;
                case Keys.LeftControl:
                case Keys.RightControl:
                    flag = MovementFlags.Down;
                    return true;
                default:
                    flag = 0;
                    return false;
            }
        }

        public static bool HandleChatCommand(string message)
        {
            if (!WorldManager.IsCharacterInGame)
                return false;

            string[] parts = message.Split(' ', '\t');
            string command = parts[0];
            string joinedArgs = string.Join(" ", parts, 1, parts.Length - 1);

            if (!command.StartsWith("/"))
                return false;
            command = command.TrimStart('/').ToLower(CultureInfo.CurrentCulture);

            switch (command)
            {
                case "join":
                    WorldManager.PlayerSession.SendPacket(new CChatChannelJoinPacket { ChannelName = joinedArgs });
                    break;
                case "leave":
                {
                    ChatChannel channel = ChatManager.GetChannelByName(joinedArgs);
                    if (channel != null)
                        WorldManager.PlayerSession.SendPacket(new CChatChannelLeavePacket { ChannelID = channel.ID });
                    break;
                }
                default:
                    return false;
            }
            // TODO: Add commands localization
            return true;
        }
        public static void HandleChatAlias(string message)
        {
            if (!WorldManager.IsCharacterInGame)
                return;

            message = message.Trim();
            if (!message.StartsWith("/"))
                return;
            message = message.TrimStart('/');

            ChatChannel channel = ChatManager.GetChannelByAlias(message);
            if (channel != null)
            {
                ChatManager.SetActiveChannel(channel.ID);
                UIChatInputBox input = UIManager.CurrentDesktop.GetByKey<UIChatInputBox>("ChatInputBox");
                if (input != null)
                    input.Value = string.Empty;
            }
        }
    }
}