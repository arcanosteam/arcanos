﻿using System.Collections.Generic;
using System.Runtime.InteropServices;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Arcanos.Client.GameClient.Input
{
    public enum MouseKeys : byte
    {
        Left,
        Right,
        Middle,
        Mouse4,
        Mouse5,
    }
    public enum MouseAxis : byte
    {
        X,
        Y,
        Z,
    }
    public static class InputManager
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        private static extern short GetKeyState(int keyCode);

        private static Game gameClient;
        private static bool wasUpdated = false;
        private static KeyboardState PrevKeyboard;
        private static KeyboardState Keyboard;
        private static MouseState PrevMouse;
        private static MouseState Mouse;
        private static bool MouseScrollHandled;
        public static int PrevMouseX, PrevMouseY, PrevMouseZ;
        public static int MouseX, MouseY, MouseZ;
        public static readonly List<Keys> DownKeys = new List<Keys>();
        public static readonly List<Keys> PressedKeys = new List<Keys>();
        public static readonly List<Keys> ReleasedKeys = new List<Keys>();

        public static bool CapsLock { get { return (((ushort)GetKeyState(0x14)) & 0xffff) != 0; } }
        public static bool NumLock { get { return (((ushort)GetKeyState(0x90)) & 0xffff) != 0; } }
        public static bool ScrollLock { get { return (((ushort)GetKeyState(0x91)) & 0xffff) != 0; } }

        public static bool Shift { get { return (System.Windows.Forms.Control.ModifierKeys & System.Windows.Forms.Keys.Shift) != 0; } }
        public static bool Control { get { return (System.Windows.Forms.Control.ModifierKeys & System.Windows.Forms.Keys.Control) != 0; } }
        public static bool Alt { get { return (System.Windows.Forms.Control.ModifierKeys & System.Windows.Forms.Keys.Alt) != 0; } }

        public static void UpdateInput(Game client, KeyboardState keyboardState, MouseState mouseState)
        {
            gameClient = client;
            if (!wasUpdated)
            {
                Keyboard = keyboardState;
                Mouse = mouseState;
                wasUpdated = true;
            }
            PrevKeyboard = Keyboard;
            PrevMouse = Mouse;
            PrevMouseX = MouseX;
            PrevMouseY = MouseY;
            PrevMouseZ = MouseZ;
            Keyboard = keyboardState;
            Mouse = mouseState;
            MouseX = Mouse.X;
            MouseY = Mouse.Y;
            MouseZ = Mouse.ScrollWheelValue;
            MouseScrollHandled = false;

            List<Keys> currentlyPressed = new List<Keys>(Keyboard.GetPressedKeys());
            //List<Keys> lastPressed = new List<Keys>(PressedKeys);
            PressedKeys.Clear();
            ReleasedKeys.Clear();
            foreach (Keys key in currentlyPressed)
            {
                if (!DownKeys.Contains(key))
                    PressedKeys.Add(key);
            }
            foreach (Keys key in DownKeys)
            {
                if (!currentlyPressed.Contains(key))
                    ReleasedKeys.Add(key);
            }
            DownKeys.Clear();
            DownKeys.AddRange(currentlyPressed);
        }

        public static bool IsKeyDown(Keys key)
        {
            return Keyboard.IsKeyDown(key);
        }
        public static bool IsKeyUp(Keys key)
        {
            return Keyboard.IsKeyUp(key);
        }
        public static bool IsKeyPressed(Keys key)
        {
            return Keyboard.IsKeyDown(key) && PrevKeyboard.IsKeyUp(key);
        }
        public static bool IsKeyReleased(Keys key)
        {
            return Keyboard.IsKeyUp(key) && PrevKeyboard.IsKeyDown(key);
        }
        public static bool IsKeyDown(MouseKeys key)
        {
            const ButtonState state = ButtonState.Pressed;
            switch (key)
            {
                case MouseKeys.Left:
                    return Mouse.LeftButton == state;
                case MouseKeys.Right:
                    return Mouse.RightButton == state;
                case MouseKeys.Middle:
                    return Mouse.MiddleButton == state;
                case MouseKeys.Mouse4:
                    return Mouse.XButton1 == state;
                case MouseKeys.Mouse5:
                    return Mouse.XButton2 == state;
            }
            return false;
        }
        public static bool IsKeyUp(MouseKeys key)
        {
            const ButtonState state = ButtonState.Released;
            switch (key)
            {
                case MouseKeys.Left:
                    return Mouse.LeftButton == state;
                case MouseKeys.Right:
                    return Mouse.RightButton == state;
                case MouseKeys.Middle:
                    return Mouse.MiddleButton == state;
                case MouseKeys.Mouse4:
                    return Mouse.XButton1 == state;
                case MouseKeys.Mouse5:
                    return Mouse.XButton2 == state;
            }
            return false;
        }
        public static bool IsKeyPressed(MouseKeys key)
        {
            const ButtonState state = ButtonState.Pressed;
            const ButtonState prevState = ButtonState.Released;
            switch (key)
            {
                case MouseKeys.Left:
                    return Mouse.LeftButton == state && PrevMouse.LeftButton == prevState;
                case MouseKeys.Right:
                    return Mouse.RightButton == state && PrevMouse.RightButton == prevState;
                case MouseKeys.Middle:
                    return Mouse.MiddleButton == state && PrevMouse.MiddleButton == prevState;
                case MouseKeys.Mouse4:
                    return Mouse.XButton1 == state && PrevMouse.XButton1 == prevState;
                case MouseKeys.Mouse5:
                    return Mouse.XButton2 == state && PrevMouse.XButton2 == prevState;
            }
            return false;
        }
        public static bool IsKeyReleased(MouseKeys key)
        {
            const ButtonState state = ButtonState.Released;
            const ButtonState prevState = ButtonState.Pressed;
            switch (key)
            {
                case MouseKeys.Left:
                    return Mouse.LeftButton == state && PrevMouse.LeftButton == prevState;
                case MouseKeys.Right:
                    return Mouse.RightButton == state && PrevMouse.RightButton == prevState;
                case MouseKeys.Middle:
                    return Mouse.MiddleButton == state && PrevMouse.MiddleButton == prevState;
                case MouseKeys.Mouse4:
                    return Mouse.XButton1 == state && PrevMouse.XButton1 == prevState;
                case MouseKeys.Mouse5:
                    return Mouse.XButton2 == state && PrevMouse.XButton2 == prevState;
            }
            return false;
        }
        public static int GetAxisChange(MouseAxis axis)
        {
            if (MouseScrollHandled)
                return 0;
            switch (axis)
            {
                case MouseAxis.X:
                    return PrevMouseX - MouseX;
                case MouseAxis.Y:
                    return PrevMouseY - MouseY;
                case MouseAxis.Z:
                    return PrevMouseZ - MouseZ;
            }
            return 0;
        }
        public static bool ScrollUp()
        {
            return Mouse.ScrollWheelValue > PrevMouse.ScrollWheelValue;
        }
        public static bool ScrollDown()
        {
            return Mouse.ScrollWheelValue < PrevMouse.ScrollWheelValue;
        }

        private static char TranslateAlphabetic(char c, bool shift, bool capsLock)
        {
            return (capsLock ^ shift) ? char.ToUpper(c) : c;
        }
        public static char GetChar(Keys key)
        {
            return GetChar(key, Shift, CapsLock, NumLock);
        }
        public static char GetChar(Keys key, bool upper)
        {
            return GetChar(key, false, upper, NumLock);
        }
        public static char GetChar(Keys key, bool shift, bool capsLock)
        {
            return GetChar(key, shift, capsLock, NumLock);
        }
        public static char GetChar(Keys key, bool shift, bool capsLock, bool numLock)
        {
            switch (key)
            {
                case Keys.A: return TranslateAlphabetic('a', shift, capsLock);
                case Keys.B: return TranslateAlphabetic('b', shift, capsLock);
                case Keys.C: return TranslateAlphabetic('c', shift, capsLock);
                case Keys.D: return TranslateAlphabetic('d', shift, capsLock);
                case Keys.E: return TranslateAlphabetic('e', shift, capsLock);
                case Keys.F: return TranslateAlphabetic('f', shift, capsLock);
                case Keys.G: return TranslateAlphabetic('g', shift, capsLock);
                case Keys.H: return TranslateAlphabetic('h', shift, capsLock);
                case Keys.I: return TranslateAlphabetic('i', shift, capsLock);
                case Keys.J: return TranslateAlphabetic('j', shift, capsLock);
                case Keys.K: return TranslateAlphabetic('k', shift, capsLock);
                case Keys.L: return TranslateAlphabetic('l', shift, capsLock);
                case Keys.M: return TranslateAlphabetic('m', shift, capsLock);
                case Keys.N: return TranslateAlphabetic('n', shift, capsLock);
                case Keys.O: return TranslateAlphabetic('o', shift, capsLock);
                case Keys.P: return TranslateAlphabetic('p', shift, capsLock);
                case Keys.Q: return TranslateAlphabetic('q', shift, capsLock);
                case Keys.R: return TranslateAlphabetic('r', shift, capsLock);
                case Keys.S: return TranslateAlphabetic('s', shift, capsLock);
                case Keys.T: return TranslateAlphabetic('t', shift, capsLock);
                case Keys.U: return TranslateAlphabetic('u', shift, capsLock);
                case Keys.V: return TranslateAlphabetic('v', shift, capsLock);
                case Keys.W: return TranslateAlphabetic('w', shift, capsLock);
                case Keys.X: return TranslateAlphabetic('x', shift, capsLock);
                case Keys.Y: return TranslateAlphabetic('y', shift, capsLock);
                case Keys.Z: return TranslateAlphabetic('z', shift, capsLock);

                case Keys.D0: return (shift) ? ')' : '0';
                case Keys.D1: return (shift) ? '!' : '1';
                case Keys.D2: return (shift) ? '@' : '2';
                case Keys.D3: return (shift) ? '#' : '3';
                case Keys.D4: return (shift) ? '$' : '4';
                case Keys.D5: return (shift) ? '%' : '5';
                case Keys.D6: return (shift) ? '^' : '6';
                case Keys.D7: return (shift) ? '&' : '7';
                case Keys.D8: return (shift) ? '*' : '8';
                case Keys.D9: return (shift) ? '(' : '9';

                case Keys.Add: return '+';
                case Keys.Divide: return '/';
                case Keys.Multiply: return '*';
                case Keys.Subtract: return '-';

                case Keys.Space: return ' ';
                case Keys.Tab: return '\t';

                case Keys.Decimal: if (numLock && !shift) return '.'; break;
                case Keys.NumPad0: if (numLock && !shift) return '0'; break;
                case Keys.NumPad1: if (numLock && !shift) return '1'; break;
                case Keys.NumPad2: if (numLock && !shift) return '2'; break;
                case Keys.NumPad3: if (numLock && !shift) return '3'; break;
                case Keys.NumPad4: if (numLock && !shift) return '4'; break;
                case Keys.NumPad5: if (numLock && !shift) return '5'; break;
                case Keys.NumPad6: if (numLock && !shift) return '6'; break;
                case Keys.NumPad7: if (numLock && !shift) return '7'; break;
                case Keys.NumPad8: if (numLock && !shift) return '8'; break;
                case Keys.NumPad9: if (numLock && !shift) return '9'; break;

                case Keys.OemBackslash: return shift ? '|' : '\\';
                case Keys.OemCloseBrackets: return shift ? '}' : ']';
                case Keys.OemComma: return shift ? '<' : ',';
                case Keys.OemMinus: return shift ? '_' : '-';
                case Keys.OemOpenBrackets: return shift ? '{' : '[';
                case Keys.OemPeriod: return shift ? '>' : '.';
                case Keys.OemPipe: return shift ? '|' : '\\';
                case Keys.OemPlus: return shift ? '+' : '=';
                case Keys.OemQuestion: return shift ? '?' : '/';
                case Keys.OemQuotes: return shift ? '"' : '\'';
                case Keys.OemSemicolon: return shift ? ':' : ';';
                case Keys.OemTilde: return shift ? '~' : '`';
            }
            return '\0';
        }

        public static void SetMousePosition(int x, int y)
        {
            Microsoft.Xna.Framework.Input.Mouse.SetPosition(x, y);
            PrevMouseX = x;
            PrevMouseY = y;
            MouseX = x;
            MouseY = y;
        }
        public static void SetMouseVisible(bool visible)
        {
            gameClient.IsMouseVisible = visible;
        }
    }
}