﻿using System;
using System.Collections.Generic;
using System.Text;
using Arcanos.Client.GameClient.Items;
using Arcanos.Client.GameClient.Spells;
using Arcanos.Client.GameClient.World;
using Arcanos.Shared.Items;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Players;
using Microsoft.Xna.Framework;

namespace Arcanos.Client.GameClient.UI
{
    public class UITooltip : UIControl
    {
        public static UITexture TooltipTexture { get; set; }
        public static UIFont TooltipFont { get; set; }

        public override UITexture CurrentGraphic
        {
            get { return TooltipTexture; }
        }
        public override UIFont Font
        {
            get { return TooltipFont; }
        }
        public override Vector2 Size
        {
            get
            {
                if (Font.Font == null) return Vector2.Zero;
                if (CurrentGraphic == null) return Font.Font.MeasureString(Text);
                return Font.MeasureString(Text ?? "", Desktop.Width / 4) + CurrentGraphic.PixelPaddingMin + CurrentGraphic.PixelPaddingMax;
            }
        }

        public UITooltip() : base(Vector2.Zero, Vector2.Zero)
        {
            ClickThrough = true;
            MultilineText = true;
        }

        public static string Build(ItemTemplate item, int count, ref bool m)
        {
            StringBuilder sb = new StringBuilder();

            if (count > 1)
                sb.AppendFormat(Localization.Game.TooltipItemNamePluralFormat.ToString(), TextCheck(ref m, item.Name), count).AppendLine();
            else
                sb.AppendFormat(Localization.Game.TooltipItemNameSingularFormat.ToString(), TextCheck(ref m, item.Name)).AppendLine();
            if (item.Slot != ItemSlot.None)
                sb.AppendFormat(Localization.Game.TooltipItemSlotFormat.ToString(), Localization.Game.ItemSlotNames[(byte)item.Slot]).AppendLine();
            sb.AppendLine();

            bool showMinLevel = item.RequiredLevel.Min != 0;
            bool showMaxLevel = item.RequiredLevel.Max != 0;
            if (showMinLevel && showMaxLevel && (WorldManager.PlayerCreature.Level < item.RequiredLevel.Min || WorldManager.PlayerCreature.Level > item.RequiredLevel.Max))
                sb.AppendFormat(Localization.Game.TooltipItemRequiredLevelBothFormat.ToString(), item.RequiredLevel.Min, item.RequiredLevel.Max).AppendLine().AppendLine();
            else if (showMinLevel && WorldManager.PlayerCreature.Level < item.RequiredLevel.Min)
                sb.AppendFormat(Localization.Game.TooltipItemRequiredLevelMinFormat.ToString(), item.RequiredLevel.Min).AppendLine().AppendLine();
            else if (showMaxLevel && WorldManager.PlayerCreature.Level > item.RequiredLevel.Max)
                sb.AppendFormat(Localization.Game.TooltipItemRequiredLevelMaxFormat.ToString(), item.RequiredLevel.Max).AppendLine().AppendLine();

            if (item.Type == ItemType.Weapon && !item.WeaponDamage.Empty)
            {
                sb.AppendFormat(Localization.Game.TooltipItemWeaponDamageFormat.ToString(), item.WeaponDamage.Min, item.WeaponDamage.Max).AppendLine();
                sb.AppendFormat(Localization.Game.TooltipItemWeaponSpeedFormat.ToString(), item.WeaponSpeed.Min, item.WeaponSpeed.Max).AppendLine();
                sb.AppendLine();
            }
            else if (item.Type == ItemType.Equipment)
            {
                int armorValue;
                if (item.Mods.TryGetValue(Stat.Armor, out armorValue))
                    sb.AppendFormat(Localization.Game.TooltipItemArmorFormat.ToString(), armorValue).AppendLine();
            }

            if (item.IsEquippable)
            {
                foreach (KeyValuePair<Stat, int> mod in item.Mods)
                {
                    if (mod.Key == Stat.Armor && item.Type == ItemType.Equipment)
                        continue;
                    sb.AppendFormat(Localization.Game.TooltipItemStatModFormat.ToString(), Localization.Game.StatNames[(byte)mod.Key], mod.Value).AppendLine();
                }
                sb.AppendLine();
            }

            if (item.HasSpellTriggers)
            {
                for (byte trigger = 0; trigger < (byte)ItemSpellUsage.Max; ++trigger)
                    if (item.SpellTriggers[trigger] != null)
                        sb.AppendFormat(Localization.Game.TooltipItemSpellTriggerFormats[trigger].ToString(), TextCheck(ref m, item.SpellTriggers[trigger].Description)).AppendLine();
                sb.AppendLine();
            }

            if (item.Description != null)
                sb.AppendFormat(Localization.Game.TooltipItemDescriptionFormat.ToString(), TextCheck(ref m, item.Description)).AppendLine();
            if (item.Lore != null)
                sb.AppendFormat(Localization.Game.TooltipItemLoreFormat.ToString(), TextCheck(ref m, item.Lore)).AppendLine();
            if (item.Description != null || item.Lore != null)
                sb.AppendLine();

            if (item.SellPrice == 0)
                sb.Append(Localization.Game.TooltipItemPriceless);
            else
                sb.AppendFormat(Localization.Game.TooltipItemPriceFormat.ToString(), item.SellPrice);

            return sb.ToString();
        }
        public static string Build(SpellTemplate spell, ref bool m)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(TextCheck(ref m, spell.Name)).AppendLine().AppendLine();

            if (spell.RequiredPowerAmount != 0)
                sb.AppendFormat(Localization.Game.TooltipSpellRequiredPowerFormat.ToString(), TextCheck(ref m, Localization.Game.PowerTypeNames[(byte)spell.RequiredPowerType]), spell.RequiredPowerAmount).AppendLine().AppendLine();
            if (spell.CooldownDuration > 0)
                sb.AppendFormat(Localization.Game.TooltipSpellCooldownFormat.ToString(), spell.CooldownDuration).AppendLine().AppendLine();
            
            sb.Append(TextCheck(ref m,spell.Description));

            return sb.ToString();
        }
        public static string Build(Aura aura, ref bool m)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(Build(aura.Spell as SpellTemplate, ref m));

            CreatureBase caster = ObjectManager.GetCreature(aura.CasterGUID);
            if (caster != null)
                sb.AppendLine().AppendLine().AppendFormat(Localization.Game.AuraCasterFormat.ToString(), caster.Name);

            return sb.ToString();
        }

        private static Text TextCheck(ref bool missing, Text text)
        {
            if (String.IsNullOrEmpty(text.ToString()))
                missing = true;
            return text;
        }
    }
}