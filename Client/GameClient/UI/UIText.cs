﻿using System;
using System.Collections.Generic;
using Arcanos.Shared.Localization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Arcanos.Client.GameClient.UI
{
    public interface ITextToken
    {
        string GetString();
        Text GetRef();
        Vector2 GetSize(UIFont font);
    }
    public struct TextToken : ITextToken
    {
        public readonly string Text;

        public TextToken(string text)
        {
            Text = text;
        }

        public string GetString()
        {
            return Text;
        }
        public Text GetRef()
        {
            return null;
        }
        public Vector2 GetSize(UIFont font)
        {
            return font.MeasureString(Text);
        }

        public static implicit operator TextToken(string text)
        {
            return new TextToken(text);
        }
    }
    public struct TextRefToken : ITextToken
    {
        public readonly Text TextRef;

        public TextRefToken(Text text)
        {
            TextRef = text;
        }

        public string GetString()
        {
            return TextRef.ToString();
        }
        public Text GetRef()
        {
            return TextRef;
        }
        public Vector2 GetSize(UIFont font)
        {
            return font.MeasureString(TextRef.ToString());
        }

        public static implicit operator TextRefToken(Text text)
        {
            return new TextRefToken(text);
        }
    }
    public struct SpaceToken : ITextToken
    {
        public string GetString()
        {
            return null;
        }
        public Text GetRef()
        {
            return null;
        }
        public Vector2 GetSize(UIFont font)
        {
            return font.MeasureString(" ");
        }
    }
    public struct ValueToken : ITextToken
    {
        public readonly Func<object> Function;

        public ValueToken(Func<object> function)
        {
            Function = function;
        }

        public string GetString()
        {
            object value = Function();
            return value == null ? null : value.ToString();
        }
        public Text GetRef()
        {
            return null;
        }
        public Vector2 GetSize(UIFont font)
        {
            return font.MeasureString(GetString());
        }
    }
    public struct ValueFormatToken : ITextToken
    {
        public readonly Text FormatRef;
        public readonly Func<object[]> Function;

        public ValueFormatToken(Text format, Func<object[]> function)
        {
            FormatRef = format;
            Function = function;
        }

        public string GetString()
        {
            object[] args = Function();
            return args == null ? "" : string.Format(FormatRef.ToString(), args);
        }
        public Text GetRef()
        {
            return FormatRef;
        }
        public Vector2 GetSize(UIFont font)
        {
            return font.MeasureString(GetString());
        }
    }
    public class UITextTag
    {
        public UIFont OriginalFont;
        public object Tag;
        public bool ColorSetUp;
    }
    public class UIText : UIControl
    {
        public UIFontAlign Align { get; set; }

        private readonly bool hasValueTokens;
        protected Vector2 MaxSize;
        protected Vector2 ActualSize;
        protected readonly List<ITextToken> Tokens;
        protected readonly List<UIControl> TokenControls;
        protected readonly Dictionary<int, int> IndexTransforms = new Dictionary<int, int>();

        public UIText(Vector2 location, Vector2 maxSize, UIFont font, params ITextToken[] tokens) : this(location, maxSize, font, UIFontAlign.Top | UIFontAlign.Left, tokens) { }
        public UIText(Vector2 location, Vector2 maxSize, UIFont font, UIFontAlign align, params ITextToken[] tokens) : base(location, maxSize)
        {
            MaxSize = maxSize;
            Font = font;
            Align = align;
            Tokens = new List<ITextToken>();
            OnResize += (sender, args) =>
            {
                if (args.Difference.X != 0)
                    AdjustPositions(0);
            };

            int index = 0;
            foreach (ITextToken token in tokens)
            {
                IndexTransforms[index++] = Tokens.Count;
                if (token is TextToken)
                {
                    string text = token.GetString();
                    if (text == " ")
                    {
                        Tokens.Add(new SpaceToken());
                        continue;
                    }
                    if (text.Contains(" "))
                    {
                        string[] words = text.Split(' ');
                        for (int i = 0; i < words.Length; i++)
                        {
                            if (i != 0)
                                Tokens.Add(new SpaceToken());
                            Tokens.Add(new TextToken(words[i]));
                        }
                        continue;
                    }
                }
                Tokens.Add(token);
            }

            TokenControls = new List<UIControl>(Tokens.Count);
            foreach (ITextToken token in Tokens)
            {
                if (!hasValueTokens && (token is ValueToken || token is ValueFormatToken))
                    hasValueTokens = true;
                Vector2 size = token.GetSize(Font);
                size.Y += Font.LineBreak;
                UIControl control = new UIControl(Vector2.Zero, size)
                {
                    Text = token.GetString(),
                    TextRef = token.GetRef(),
                    Font = Font,
                    Tag = new UITextTag { OriginalFont = Font },
                    ClickThrough = true,
                    CutOverflownText = false,
                };
                control.OnResize += (sender, args) => AdjustPositions(TokenControls.IndexOf((UIControl)sender) + 1);
                control.OnTextChanged += (sender, args) =>
                {
                    UIControl senderControl = (UIControl)sender;
                    string text = Tokens[TokenControls.IndexOf(senderControl)].GetString();
                    if (senderControl.Text != text)
                    {
                        senderControl.Text = text;
                        return;
                    }
                    Vector2 size2 = Tokens[TokenControls.IndexOf(senderControl)].GetSize(senderControl.Font);
                    size2.Y += senderControl.Font.LineBreak;
                    senderControl.Size = size2;
                };
                TokenControls.Add(control);
                Controls.Add(control);
            }
            AdjustPositions(0);
        }

        public override void Update()
        {
            base.Update();
            if (hasValueTokens)
                for (int i = 0; i < Tokens.Count; i++)
                    if (Tokens[i] is ValueToken || Tokens[i] is ValueFormatToken)
                        TokenControls[i].Text = Tokens[i].GetString();
        }

        private void AdjustPositions(int fromIndex)
        {
            ActualSize = new Vector2(0, Font.LineHeight + Font.LineBreak);
            Vector2 pos = Vector2.Zero;
            for (int i = 0; i < Tokens.Count; i++)
            {
                UIControl control = TokenControls[i];
                Vector2 size = Tokens[i].GetSize(Font);
                if (pos.X > 0 && MaxSize.X > 0 && pos.X + size.X > MaxSize.X)
                {
                    pos.X = 0;
                    pos.Y += Font.LineHeight + Font.LineBreak;
                    ActualSize.Y += Font.LineHeight + Font.LineBreak;
                }
                control.Location = pos;
                pos.X += size.X;
                if (pos.X > ActualSize.X)
                    ActualSize.X = pos.X;
            }

            Vector2 containerSize = MaxSize;
            if (ActualSize.X > containerSize.X)
                containerSize.X = ActualSize.X;
            if (ActualSize.Y > containerSize.Y)
                containerSize.Y = ActualSize.Y;
            Size = containerSize;
            if (Size == MaxSize)
            {
                Vector2 offset = Vector2.Zero;
                if ((Align & UIFontAlign.Center) != 0)
                    offset.X = (MaxSize.X - ActualSize.X) / 2;
                if ((Align & UIFontAlign.Right) != 0)
                    offset.X = MaxSize.X - ActualSize.X;
                if ((Align & UIFontAlign.Middle) != 0)
                    offset.Y = (MaxSize.Y - ActualSize.Y) / 2;
                if ((Align & UIFontAlign.Bottom) != 0)
                    offset.Y = MaxSize.Y - ActualSize.Y;
                ContentOffset = offset;
            }
        }

        public Vector2 GetActualSize()
        {
            return ActualSize;
        }
        public IEnumerable<UIControl> GetControls(int from, int to)
        {
            if (!IndexTransforms.TryGetValue(from, out from))
                from = 0;
            if (!IndexTransforms.TryGetValue(to + 1, out to))
                to = Tokens.Count;
            for (int i = from; i < to; i++)
                yield return TokenControls[i];
        }

        public void SetClickable(int from, int to, EventHandler handler)
        {
            const int BRIGHTNESS = 96;
            if (!IndexTransforms.TryGetValue(from, out from))
                from = 0;
            if (!IndexTransforms.TryGetValue(to + 1, out to))
                to = Tokens.Count;
            for (int i = from; i < to; i++)
            {
                UIControl control = TokenControls[i];
                control.ClickThrough = false;
                control.OnClick += handler;
                control.OnMouseEnter += (sender, args) =>
                {
                    for (int j = from; j < to; j++)
                    {
                        UITextTag tag = (UITextTag)TokenControls[j].Tag;
                        if (tag.ColorSetUp)
                            continue;
                        tag.ColorSetUp = true;
                        UIFont font = tag.OriginalFont;
                        TokenControls[j].Font = font.MakeColor((byte)(font.Color.R < 255 - BRIGHTNESS ? font.Color.R + BRIGHTNESS : 255), (byte)(font.Color.G < 255 - BRIGHTNESS ? font.Color.G + BRIGHTNESS : 255), (byte)(font.Color.B < 255 - BRIGHTNESS ? font.Color.B + BRIGHTNESS : 255), font.Color.A);
                    }
                };
                control.OnMouseLeave += (sender, args) =>
                {
                    for (int j = from; j < to; j++)
                    {
                        UITextTag tag = (UITextTag)TokenControls[j].Tag;
                        tag.ColorSetUp = false;
                        TokenControls[j].Font = tag.OriginalFont;
                    }
                };
            }
        }
        public void SetHoverable(int from, int to, EventHandler enterHandler, EventHandler leaveHandler)
        {
            const int BRIGHTNESS = 96;
            if (!IndexTransforms.TryGetValue(from, out from))
                from = 0;
            if (!IndexTransforms.TryGetValue(to + 1, out to))
                to = Tokens.Count;
            for (int i = from; i < to; i++)
            {
                UIControl control = TokenControls[i];
                control.OnMouseEnter += (sender, args) =>
                {
                    for (int j = from; j < to; j++)
                    {
                        UITextTag tag = (UITextTag)TokenControls[j].Tag;
                        if (tag.ColorSetUp)
                            continue;
                        tag.ColorSetUp = true;
                        UIFont font = tag.OriginalFont;
                        TokenControls[j].Font = font.MakeColor((byte)(font.Color.R < 255 - BRIGHTNESS ? font.Color.R + BRIGHTNESS : 255), (byte)(font.Color.G < 255 - BRIGHTNESS ? font.Color.G + BRIGHTNESS : 255), (byte)(font.Color.B < 255 - BRIGHTNESS ? font.Color.B + BRIGHTNESS : 255), font.Color.A);
                    }
                    enterHandler(sender, args);
                };
                control.OnMouseLeave += (sender, args) =>
                {
                    leaveHandler(sender, args);
                    for (int j = from; j < to; j++)
                    {
                        UITextTag tag = (UITextTag)TokenControls[j].Tag;
                        tag.ColorSetUp = false;
                        TokenControls[j].Font = tag.OriginalFont;
                    }
                };
            }
        }
        public void SetColor(int from, int to, Color color)
        {
            if (!IndexTransforms.TryGetValue(from, out from))
                from = 0;
            if (!IndexTransforms.TryGetValue(to + 1, out to))
                to = Tokens.Count;
            for (int i = from; i < to; i++)
                TokenControls[i].Tag = new UITextTag { OriginalFont = TokenControls[i].Font = TokenControls[i].Font.MakeColor(color) };
        }
    }
}