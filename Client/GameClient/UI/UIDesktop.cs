﻿using System;
using System.Collections.Generic;
using Arcanos.Client.GameClient.Input;
using Arcanos.Client.GameClient.Items;
using Arcanos.Client.GameClient.Spells;
using Arcanos.Client.GameClient.UI.Game;
using Arcanos.Utilities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Arcanos.Client.GameClient.UI
{
    public class UIDesktop : UIContainer, IDisposable
    {
        private Vector2 size;
        private Vector2 mouseLocation;
        private bool mouseLeft;
        private bool mouseRight;
        private int mouseScroll;

        public Vector2 Size
        {
            get { return size; }
            set
            {
                if (size == value) return;
                Vector2 old = size;
                size = value;
                if (OnResize != null)
                    OnResize(this, new VectorEventArgs() { OldValue = old, NewValue = value });
            }
        }
        public float Width
        {
            get { return Size.X; }
            set { Size = new Vector2(value, Size.Y); }
        }
        public float Height
        {
            get { return Size.Y; }
            set { Size = new Vector2(Size.X, value); }
        }

        public bool Enabled { get; set; }
        public bool Visible { get; set; }

        public Vector2 MouseLocation
        {
            get { return mouseLocation; }
            set
            {
                if (mouseLocation == value) return;
                Vector2 old = mouseLocation;
                mouseLocation = value;
                bool handled = false;
                handled |= Dropdown.OnMouseMove(value);
                handled |= DropdownBackdrop.OnMouseMove(value);
                foreach (UIControl control in Controls.LockReverse())
                    handled |= control.OnMouseMove(value);
                Controls.Unlock();
                if (handled)
                    MouseHandled();
                else
                    if (old != value && OnMouseMove != null)
                        OnMouseMove(this, new VectorEventArgs() { OldValue = old, NewValue = value });
            }
        }
        public bool MouseLeft
        {
            get { return mouseLeft; }
            set
            {
                if (mouseLeft == value) return;
                bool old = mouseLeft;
                mouseLeft = value;
                bool handled = false;
                if (Dropdown.OnMouseLeft(value))
                    handled = true;
                else if (DropdownBackdrop.OnMouseLeft(value))
                    handled = true;
                else
                {
                    foreach (UIControl control in Controls.LockReverse())
                        if (control.OnMouseLeft(value))
                        {
                            handled = true;
                            break;
                        }
                    Controls.Unlock();
                }
                if (handled)
                    MouseHandled();
                else
                    if (OnMouseLeft != null && MouseLocation.X > 0 && MouseLocation.Y > 0 && MouseLocation.X <= Width && MouseLocation.Y <= Height)
                        OnMouseLeft(this, new BoolEventArgs() { OldValue = old, NewValue = value });
            }
        }
        public bool MouseRight
        {
            get { return mouseRight; }
            set
            {
                if (mouseRight == value) return;
                bool old = mouseRight;
                mouseRight = value;
                bool handled = false;
                if (Dropdown.OnMouseRight(value))
                    handled = true;
                else if (DropdownBackdrop.OnMouseRight(value))
                    handled = true;
                else
                {
                    foreach (UIControl control in Controls.LockReverse())
                        if (control.OnMouseRight(value))
                        {
                            handled = true;
                            break;
                        }
                    Controls.Unlock();
                }
                if (handled)
                    MouseHandled();
                else
                    if (OnMouseRight != null && MouseLocation.X > 0 && MouseLocation.Y > 0 && MouseLocation.X <= Width && MouseLocation.Y <= Height)
                        OnMouseRight(this, new BoolEventArgs() { OldValue = old, NewValue = value });
            }
        }
        public int MouseScroll
        {
            get { return mouseScroll; }
            set
            {
                if (mouseScroll == value) return;
                int old = mouseScroll;
                mouseScroll = value;
                bool handled = false;
                if (Dropdown.OnMouseScrolled(value))
                    handled = true;
                else if (DropdownBackdrop.OnMouseScrolled(value))
                    handled = true;
                else
                {
                    foreach (UIControl control in Controls.LockReverse())
                        if (control.OnMouseScrolled(value))
                        {
                            handled = true;
                            break;
                        }
                    Controls.Unlock();
                }
                if (handled)
                    MouseHandled();
                else
                    if (OnMouseScroll != null && MouseLocation.X > 0 && MouseLocation.Y > 0 && MouseLocation.X <= Width && MouseLocation.Y <= Height)
                        OnMouseScroll(this, new Int32EventArgs() { OldValue = old, NewValue = value });
            }
        }
        public bool MouseOverControl
        {
            get { return mouseHandled; }
        }

        private readonly Dictionary<string, UIControl> keys = new Dictionary<string, UIControl>();
        private readonly Dictionary<UIEvents, UIEventHandler> events = new Dictionary<UIEvents, UIEventHandler>();

        private UIControl prevFocusedControl;
        public UIControl FocusedControl { get; set; }
        public UIControl NameplateContainer { get; set; }
        public UITooltip Tooltip { get; set; }
        public UIControl TooltipSource { get; set; }
        public UIControl DropdownBackdrop { get; set; }
        public UIStackPanel Dropdown { get; set; }
        public Item DraggedItem { get; private set; }
        public int DraggedItemCount { get; private set; }
        public SpellTemplate DraggedSpell { get; set; }
        public UIItemDefinitionSlot DraggedItemControl { get; private set; }
        public UISpell DraggedSpellControl { get; private set; }

        public event VectorEventHandler OnResize;
        public event VectorEventHandler OnMouseMove;
        public event BoolEventHandler OnMouseLeft;
        public event BoolEventHandler OnMouseRight;
        public event Int32EventHandler OnMouseScroll;

        public UIDesktop(Vector2 size)
        {
            Size = size;

            Enabled = true;
            Visible = true;

            Controls.Add(NameplateContainer = new UIControl(Vector2.Zero, Size) { Anchor = UIAnchor.All, ClickThrough = true });
            DraggedItemControl = new UIItemDefinitionSlot(Vector2.Zero, UIItemDefinitionSlot.DefaultSize, null) { Visible = false, QualityOverlayControl = { Enabled = false } };
            DraggedSpellControl = new UISpell(Vector2.Zero, UISpell.DefaultSize, null) { Visible = false };
            Tooltip = new UITooltip() { Parent = this, Visible = false };
            DropdownBackdrop = new UIControl(Vector2.Zero, Size) { Anchor = UIAnchor.All, ClickThrough = false, Visible = false };
            DropdownBackdrop.OnMouseLeftDown += (sender, args) => CloseDropdown();
            DropdownBackdrop.OnMouseRightDown += (sender, args) => CloseDropdown();
            Dropdown = new UIStackPanel(Vector2.Zero, Vector2.Zero, 0, false) { ClickThrough = false, Visible = false };

            OnResize += (sender, e) =>
            {
                foreach (UIControl control in Controls.Lock())
                    control.OnResized(e.Difference);
                Controls.Unlock();
            };
            bool pressed = false;
            OnMouseLeft += (sender, args) =>
            {
                if (args.NewValue && !args.OldValue)
                    pressed = true;
                if (pressed && !args.NewValue && args.OldValue)
                {
                    if (DraggedItem != null)
                    {
                        Item item = DraggedItem;
                        PutItem();
                        InputHandler.DestoryItem(item);
                    }
                    if (DraggedSpell != null)
                        PutItem();
                }
            };
        }
        public void Dispose()
        {
            foreach (UIControl control in Controls)
                control.Dispose();
        }

        public override void Update()
        {
            base.Update();
            prevFocusedControl = FocusedControl;
            if (DraggedItem != null)
                DraggedItemControl.Location = MouseLocation - DraggedItemControl.Size / 2;
            if (DraggedSpell != null)
                DraggedSpellControl.Location = MouseLocation - DraggedSpellControl.Size / 2;
        }

        public void Add(UIControl control)
        {
            UIControlList controls = Controls;
            if (control is UINameplate)
                controls = NameplateContainer.Controls;

            controls.Add(control);
        }
        public void Add(string key, UIControl control)
        {
            UIControlList controls = Controls;
            if (control is UINameplate)
                controls = NameplateContainer.Controls;

            if (keys.ContainsKey(key))
            {
                Logger.Error(LogCategory.UI, "Tried to add a control with key {0}, but a control with such key is already registered", key);
                return;
            }
            keys.Add(key, control);
            controls.Add(control);
        }
        public void Remove(UIControl control)
        {
            UIControlList controls = Controls;
            if (control is UINameplate)
                controls = NameplateContainer.Controls;

            if (keys.ContainsValue(control))
            {
                UIControl[] controlArray = new UIControl[keys.Count];
                string[] keyArray = new string[keys.Count];
                keys.Values.CopyTo(controlArray, 0);
                keys.Keys.CopyTo(keyArray, 0);
                keys.Remove(keyArray[Array.IndexOf(controlArray, control)]);
            }
            controls.Remove(control);
        }
        public void Remove(string key)
        {
            if (!keys.ContainsKey(key))
                return;
            UIControl control = keys[key];
            UIControlList controls = Controls;
            if (control is UINameplate)
                controls = NameplateContainer.Controls;
            controls.Remove(control);
            keys.Remove(key);
        }
        public void SetKey(string key, UIControl control)
        {
            if (control == null)
            {
                keys.Remove(key);
                return;
            }
            if (keys.ContainsKey(key))
            {
                Logger.Error(LogCategory.UI, "Tried to add a control with key {0}, but a control with such key is already registered", key);
                return;
            }
            keys.Add(key, control);
        }
        public UIControl GetByKey(string key)
        {
            UIControl control;
            return keys.TryGetValue(key, out control) ? control : null;
        }
        public T GetByKey<T>(string key) where T : UIControl
        {
            UIControl control;
            return keys.TryGetValue(key, out control) ? control as T : null; 
        }

        public void RegisterEvent(UIEvents eventName, UIEventHandler action)
        {
            UIEventHandler existingAction;
            if (events.TryGetValue(eventName, out existingAction))
                events[eventName] = existingAction + action;
            else
                events.Add(eventName, action);
        }
        public void RaiseEvent(UIEvents eventName, object sender, params object[] args)
        {
            UIEventHandler existingAction;
            if (events.TryGetValue(eventName, out existingAction))
                existingAction(this, sender, args);
        }

        public void PickItem(Item item)
        {
            PickItem(item, 0);
        }
        public void PickItem(Item item, int count)
        {
            PutItem();
            DraggedItem = item;
            DraggedItemCount = count;

            DraggedItemControl.SetDisplayItem(DraggedItem.Template as ItemTemplate, DraggedItemCount == 0 ? DraggedItem.Count : DraggedItemCount);
            DraggedItemControl.Location = MouseLocation - DraggedItemControl.Size / 2;
            DraggedItemControl.Visible = true;
        }
        public void PickSpell(SpellTemplate spell)
        {
            PutItem();
            DraggedSpell = spell;

            DraggedSpellControl.SetDisplaySpell(DraggedSpell);
            DraggedSpellControl.Location = MouseLocation - DraggedItemControl.Size / 2;
            DraggedSpellControl.Visible = true;
        }
        public void PutItem()
        {
            DraggedItem = null;
            DraggedItemCount = 0;
            DraggedSpell = null;

            DraggedItemControl.Visible = false;
            DraggedSpellControl.Visible = false;
        }

        private bool mouseHandled = false;
        public bool PerformMouseHandling(Vector2 mouseLoc, bool mouseLeftPressed, bool mouseRightPressed, int mouseScrolled)
        {
            mouseHandled = false;
            MouseLocation = mouseLoc;
            MouseLeft = mouseLeftPressed;
            MouseRight = mouseRightPressed;
            if (mouseScrolled != 0)
                MouseScroll = mouseScrolled;
            mouseScroll = 0;
            //if (!mouseHandled)
            //    ResetTooltip();
            return mouseHandled;
        }
        public bool PerformKeyPress(Keys key)
        {
            if (FocusedControl != null || prevFocusedControl != null)
                return (FocusedControl ?? prevFocusedControl).OnKeyPress(key);

            bool handled = false;
            foreach (UIControl control in Controls.LockReverse())
                if (control.OnKeyPress(key))
                {
                    handled = true;
                    break;
                }
            Controls.Unlock();
            return handled;
        }
        public bool PerformKeyRelease(Keys key)
        {
            if (FocusedControl != null || prevFocusedControl != null)
                return (FocusedControl ?? prevFocusedControl).OnKeyRelease(key);

            bool handled = false;
            foreach (UIControl control in Controls.LockReverse())
                if (control.OnKeyRelease(key))
                {
                    handled = true;
                    break;
                }
            Controls.Unlock();
            return handled;
        }
        private void MouseHandled()
        {
            mouseHandled = true;
        }

        public void ResetTooltip()
        {
            Tooltip.Visible = Tooltip.Enabled = false;
            if (TooltipSource != null)
                TooltipSource.OnHidden -= TooltipSourceOnHiddenHandler;
            TooltipSource = null;
        }
        public void ResetTooltip(UIControl source)
        {
            if (TooltipSource == source)
                ResetTooltip();
        }
        public void SetTooltip(UIControl control)
        {
            SetTooltip(control, control.Tooltip);
        }
        public void SetTooltip(UIControl control, string tooltip)
        {
            if (TooltipSource != null)
                TooltipSource.OnHidden -= TooltipSourceOnHiddenHandler;
            TooltipSource = control;
            if (TooltipSource != null)
                TooltipSource.OnHidden += TooltipSourceOnHiddenHandler;
            if (string.IsNullOrEmpty(tooltip))
            {
                Tooltip.Visible = Tooltip.Enabled = false;
                return;
            }
            Tooltip.Text = tooltip;
            Vector2 loc = MouseLocation - new Vector2(0, Tooltip.Size.Y);
            if (loc.X + Tooltip.Size.X > Size.X)
                loc.X = MouseLocation.X - Tooltip.Size.X;//Size.X - Tooltip.Size.X;
            if (loc.Y + Tooltip.Size.Y > Size.Y)
                loc.Y = MouseLocation.Y - Tooltip.Size.X;//Size.Y - Tooltip.Size.Y;
            if (loc.X < 0)
                loc.X = MouseLocation.X;//0;
            if (loc.Y < 0)
                loc.Y = MouseLocation.Y + 16;//0;
            Tooltip.Location = loc;
            Tooltip.Visible = Tooltip.Enabled = true;
        }
        private void TooltipSourceOnHiddenHandler(object sender, EventArgs e)
        {
            ResetTooltip();
        }

        public void OpenDropdown(UIControl control, UIDropdownItemData[] items)
        {
            if (items.Length == 0)
                return;
            Dropdown.Container.Controls.Clear();
            Dropdown.Location = control.DesktopLocation + new Vector2(0, control.Height);
            float itemsWidth = 0;
            foreach (UIDropdownItemData itemData in items)
            {
                Vector2 itemSize = UIDropdownItem.TextFont.MeasureString(itemData.Text);
                if (itemsWidth < itemSize.X)
                    itemsWidth = itemSize.X;
                float itemHeight = UIDropdownItem.NormalTexture.PixelPaddingV + itemSize.Y;
                UIButton itemButton = itemData.Text == null
                    ? new UIDropdownItem(Vector2.Zero, new Vector2(0, itemHeight), itemData.TextRef, itemData.Icon)
                    : new UIDropdownItem(Vector2.Zero, new Vector2(0, itemHeight), itemData.Text, itemData.Icon);
                UIDropdownItemData data = itemData;
                itemButton.OnClick += (sender, args) =>
                {
                    CloseDropdown();
                    data.OnClick(sender, args);
                };
                Dropdown.Container.Controls.Add(itemButton);
            }
            itemsWidth += UIButton.NormalTexture.PixelPaddingH;
            foreach (UIDropdownItem itemButton in Dropdown.Container.Controls)
                itemButton.Width = itemsWidth;
            Dropdown.Width = itemsWidth;
            Dropdown.Height = Dropdown.Container.Controls[Dropdown.Container.Controls.Count - 1].Bottom;
            if (Dropdown.Bottom >= Height)
                Dropdown.Top = control.DesktopLocation.Y - Dropdown.Height;
            Dropdown.Visible = true;
            DropdownBackdrop.Visible = true;
        }
        public void CloseDropdown()
        {
            Dropdown.Visible = false;
            DropdownBackdrop.Visible = false;
        }
    }
}