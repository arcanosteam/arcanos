﻿namespace Arcanos.Client.GameClient.UI
{
    public enum UIEvents : ushort
    {
        GameError,
        GameInfo,

        PlayerLoggedIn,
        PlayerReadyForLoading,
        PlayerLoaded,

        CharacterCreateResult,
        CharacterCreateTemplateResponse,

        ObjectEnteredVision,
        ObjectExitedVision,

        CreatureHealthChanged,
        CreatureUsesPowerChanged,
        CreaturePowerChanged,
        CreatureTargetChanged,
        CreatureLeveledUp,
        CreatureEnteredCombat,
        CreatureEvaded,
        CreatureStateChanged,

        /// <summary>
        /// sender:CreatureBase: Victim<br/>
        /// 0:ulong: Victim GUID<br/>
        /// 1:Damage: Damage dealt<br/>
        /// </summary>
        OutgoingDamage,
        /// <summary>
        /// sender:CreatureBase: Attacker<br/>
        /// 0:ulong: Attacker GUID<br/>
        /// 1:Damage: Damage dealt<br/>
        /// </summary>
        IncomingDamage,
        /// <summary>
        /// sender:CreatureBase: Target<br/>
        /// 0:ulong: Healer GUID<br/>
        /// 1:int: Healing done<br/>
        /// </summary>
        OutgoingHeal,
        /// <summary>
        /// sender:CreatureBase: Healer<br/>
        /// 0:ulong: Healer GUID<br/>
        /// 1:int: Healing done<br/>
        /// </summary>
        IncomingHeal,
        /// <summary>
        /// sender: CreatureBase: Attacker<br/>
        /// 0:ulong: Attacker GUID<br/>
        /// 1:ulong: Victim GUID<br/>
        /// 2:Damage: Damage dealt<br/>
        /// </summary>
        CreatureInflictedDamage,
        /// <summary>
        /// sender: CreatureBase: Healer<br/>
        /// 0:ulong: Healer GUID<br/>
        /// 1:ulong: Target GUID<br/>
        /// 2:int: Healing done<br/>
        /// </summary>
        CreatureDoneHealing,
        SpellCooldownUpdated,
        GlobalCooldownUpdated,
        CreatureSpellCasted,
        CreatureSpellCastingStarted,
        CreatureSpellCastingFailed,
        AuraApplied,
        AuraRemoved,

        StartDialog,
        EndDialog,
        ChangePage,
        PageOptionReceived,
        PageQuestOptionReceived,
        AcceptQuestPage,
        CompleteQuestPage,

        MoneyUpdated,
        ItemUpdated,
        BagChanged,
        BagSlotUpdated,
        LootOpened,
        LootClosed,
        LootItemRemoved,
        ItemsAcquired,
        ItemsLost,

        QuestAdded,
        QuestRemoved,
        QuestCompleted,
        QuestObjectiveUpdated,

        ChatMessage,
        ChatMessageSent,
        ChatChannelJoined,
        ChatChannelLeft,
        ChatActiveChannelChanged,

        GroupInvite,
        JoinedGroup,
        LeftGroup,
        GroupMemberJoined,
        GroupMemberLeft,
        GroupLeaderChanged,

        ExperienceChanged,
        LevelUp,
        StatsUpdated,
        SpellLearned,
        SpellForgotten,

        DuelRequest,
        DuelStatus,
        DuelParticipantStatus,
    }
}