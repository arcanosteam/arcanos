﻿namespace Arcanos.Client.GameClient.UI
{
    public abstract class UIContainer
    {
        public UIControlList Controls { get; protected set; }

        protected UIContainer()
        {
            Controls = new UIControlList(this);
        }

        public virtual void Update()
        {
            foreach (UIControl control in Controls.Lock())
                control.Update();
            Controls.Unlock();
        }
    }
}