﻿using System;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Timing;
using Microsoft.Xna.Framework;

namespace Arcanos.Client.GameClient.UI
{
    public interface IGaugeSource
    {
        void GetGaugeData(int gauge, out float value, out float min, out float max);
    }
    public class UIGauge : UIControl
    {
        public static UITexture BackdropTexture { get; set; }
        public static UITexture GaugeTexture { get; set; }
        public static UIFont TextFont { get; set; }

        private UITexture graphic;
        public override UITexture CurrentGraphic
        {
            get { return graphic ?? BackdropTexture; }
            set { graphic = value; }
        }
        public override UIFont Font
        {
            get { return TextFont; }
        }

        public Vector2 OffsetMin { get; set; }
        public Vector2 OffsetMax { get; set; }
        public IGaugeSource Source { get; set; }
        public int SourceGaugeIndex { get; set; }
        public string TextFormat { get; set; }
        public Text TextFormatRef { get; set; }
        public object ExtraFormatArg4 { get; set; }
        public object ExtraFormatArg5 { get; set; }
        public object ExtraFormatArg6 { get; set; }
        public bool AutoUpdate { get; set; }
        public float InterpolateTime { get; set; }

        public UIControl GaugeControl { get; set; }
        public UIControl TextControl { get; set; }

        private float finalWidth;
        protected float storedValue, storedMin, storedMax;

        public UIGauge(Vector2 location, Vector2 size, Vector2 offsetMin, Vector2 offsetMax, IGaugeSource source, int sourceGaugeIndex) : base(location, size)
        {
            OffsetMin = offsetMin;
            OffsetMax = offsetMax;
            Source = source;
            SourceGaugeIndex = sourceGaugeIndex;
            Controls.Add(GaugeControl = new UIControl(offsetMin, size - offsetMin - offsetMax)
            {
                Anchor = UIAnchor.All,
                CurrentGraphic = GaugeTexture,
            });
            Controls.Add(TextControl = new UIControl(offsetMin, size - offsetMin - offsetMax)
            {
                Anchor = UIAnchor.All,
                Font = Font,
            });
            InterpolateTime = 0.25f;
            OnResize += (sender, args) =>
            {
                finalWidth = Width * finalWidth / args.OldValue.X;
            };
        }

        public override void Update()
        {
            base.Update();
            if (!Visible)
                return;
            if (Source == null)
                return;

            if (AutoUpdate)
                UpdateTarget(false);
            if (InterpolateTime > 0 && GaugeControl.Width != finalWidth)
            {
                if (Math.Abs(GaugeControl.Width - finalWidth) < 1)
                    GaugeControl.Width = finalWidth;
                else
                    GaugeControl.Width += (finalWidth - GaugeControl.Width) * Time.PerSecond * 2 / InterpolateTime;
            }
        }

        public void SetSource(IGaugeSource source)
        {
            Source = source;
            UpdateTarget(true);
        }

        public void UpdateTarget()
        {
            UpdateTarget(false);
        }
        public void UpdateTarget(bool immediate)
        {
            Source.GetGaugeData(SourceGaugeIndex, out storedValue, out storedMin, out storedMax);
            float percent = (storedValue - storedMin) / (storedMax - storedMin);
            int newWidth = (int)((Width - OffsetMin.X - OffsetMax.X) * percent);
            if (InterpolateTime == 0 || immediate)
                GaugeControl.Width = newWidth;
            finalWidth = newWidth;

            if (string.IsNullOrEmpty(TextFormat) && TextFormatRef != null)
                TextFormat = TextFormatRef.ToString();
            if (!string.IsNullOrEmpty(TextFormat))
                TextControl.Text = string.Format(TextFormat, storedValue, storedMin, storedMax, percent * 100, ExtraFormatArg4, ExtraFormatArg5, ExtraFormatArg6);
            else if (!string.IsNullOrEmpty(Text))
                TextControl.Text = string.Empty;
        }
    }
}