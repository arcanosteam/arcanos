﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Microsoft.Xna.Framework;

namespace Arcanos.Client.GameClient.UI
{
    public class TypeEventArgs<T>
    {
        public T OldValue { get; set; }
        public T NewValue { get; set; }

        public TypeEventArgs() { }
        public TypeEventArgs(T oldValue, T newValue)
        {
            OldValue = oldValue;
            NewValue = newValue;
        }
    }
    public class VectorEventArgs : TypeEventArgs<Vector2>
    {
        public Vector2 Difference { get { return NewValue - OldValue; } set { NewValue = OldValue + value; } }
    }
    public class BoolEventArgs : TypeEventArgs<bool> { }
    public class Int32EventArgs : TypeEventArgs<int> { }
    public class UIContainerEventArgs : TypeEventArgs<UIContainer> { }
    public delegate void TypeEventHandler<T>(object sender, TypeEventArgs<T> e);
    public delegate void VectorEventHandler(object sender, VectorEventArgs e);
    public delegate void Int32EventHandler(object sender, Int32EventArgs e);
    public delegate void BoolEventHandler(object sender, BoolEventArgs e);
    public delegate void UIContainerEventHandler(object sender, UIContainerEventArgs e);
    public delegate void UIEventHandler(UIDesktop desktop, object sender, params object[] args);

    public class UIControlList : Collection<UIControl>
    {
        protected struct QueuedAction
        {
            public byte Action;
            public int Index;
            public UIControl Item;
        }

        protected readonly UIContainer Parent;
        public UIControlList(UIContainer parent)
        {
            Parent = parent;
        }

        protected readonly List<QueuedAction> actionQueue = new List<QueuedAction>();

        protected int locked = 0;
        private readonly object queueLock = new object();
        public IEnumerable<UIControl> Lock()
        {
            lock (queueLock)
                locked++;
            return Items;
        }
        public IEnumerable<UIControl> LockReverse()
        {
            lock (queueLock)
                locked++;
            for (int i = Count - 1; i >= 0; i--)
                yield return Items[i];
        }
        public void Unlock()
        {
            lock (queueLock)
                locked--;
            if (locked == 0)
                ProcessQueues();
        }
        protected void ProcessQueues()
        {
            lock (queueLock)
            {
                while (actionQueue.Count != 0)
                {
                    QueuedAction a = actionQueue[0];
                    actionQueue.RemoveAt(0);
                    switch (a.Action)
                    {
                        case 0:
                            InsertItem(Math.Min(a.Index, Count), a.Item);
                            for (int i = 0; i < actionQueue.Count; i++)
                                if (actionQueue[i].Index >= a.Index)
                                    actionQueue[i] = new QueuedAction { Action = actionQueue[i].Action, Item = actionQueue[i].Item, Index = actionQueue[i].Index + 1 };
                            break;
                        case 1:
                            RemoveItem(IndexOf(a.Item));
                            for (int i = 0; i < actionQueue.Count; i++)
                                if (actionQueue[i].Index >= a.Index)
                                    actionQueue[i] = new QueuedAction { Action = actionQueue[i].Action, Item = actionQueue[i].Item, Index = actionQueue[i].Index - 1 };
                            break;
                        case 2:
                            SetItem(a.Index, a.Item);
                            break;
                        case 3:
                            ClearItems();
                            for (int i = 0; i < actionQueue.Count; i++)
                                if (actionQueue[i].Index >= a.Index)
                                    actionQueue[i] = new QueuedAction { Action = actionQueue[i].Action, Item = actionQueue[i].Item, Index =  0 };
                            break;
                    }
                }
            }
        }

        protected override void InsertItem(int index, UIControl item)
        {
            lock (queueLock)
            {
                if (locked != 0)
                {
                    actionQueue.Add(new QueuedAction { Action = 0, Index = index, Item = item });
                    return;
                }
            }
            base.InsertItem(index, item);
            item.Parent = Parent;
        }
        protected override void RemoveItem(int index)
        {
            lock (queueLock)
            {
                if (locked != 0)
                {
                    actionQueue.Add(new QueuedAction { Action = 1, Item = this[index] });
                    return;
                }
            }
            Items[index].Parent = null;
            base.RemoveItem(index);
        }
        protected override void SetItem(int index, UIControl item)
        {
            lock (queueLock)
            {
                if (locked != 0)
                {
                    actionQueue.Add(new QueuedAction { Action = 2, Index = index, Item = item });
                    return;
                }
            }
            base.SetItem(index, item);
            item.Parent = Parent;
        }
        protected override void ClearItems()
        {
            lock (queueLock)
            {
                if (locked != 0)
                {
                    actionQueue.Add(new QueuedAction { Action = 3 });
                    return;
                }
            }
            foreach (UIControl item in Items)
                item.Parent = null;
            base.ClearItems();
        }
    }
}