﻿using Microsoft.Xna.Framework;

namespace Arcanos.Client.GameClient.UI
{
    public class UIStackPanel : UIControl
    {
        public static UITexture PanelTexture { get; set; }

        public UIStackPanelContainer Container { get; set; }
        public UIScrollbar Scrollbar { get; set; }

        public UIStackPanel(Vector2 location, Vector2 size, float margin, bool showScrollbar) : base(location, size)
        {
            Controls.Add(Container = new UIStackPanelContainer(Vector2.Zero, Size, margin)
            {
                Anchor = UIAnchor.Top | UIAnchor.Bottom | UIAnchor.Left | UIAnchor.Right,
            });
            if (showScrollbar)
            {
                Controls.Add(Scrollbar = new UIScrollbar(new Vector2(Container.Right - UIScrollbar.DefaultThickness, Container.Top), Container.Height, 0, 1, 1)
                {
                    Anchor = UIAnchor.Top | UIAnchor.Bottom | UIAnchor.Right,
                    Visible = false,
                    SmallIncrement = Container.Height / 10,
                    BigIncrement = Container.Height,
                });
                Container.ContentHeightChanged += (sender, e) =>
                {
                    Scrollbar.Maximum = Container.ContentHeight - Container.Height;
                    if (Scrollbar.Maximum < 0)
                        Scrollbar.Maximum = 0;
                    Scrollbar.Range = Container.Height;
                    if (Container.ContentHeight <= Container.Height)
                        Container.ContentOffset = Vector2.Zero;
                    Scrollbar.UpdateHeight();
                    UpdateScrollbarVisibility();
                };
                Scrollbar.Scrolled += (sender, e) =>
                {
                    Container.ContentOffset = new Vector2(0, -e.NewValue);
                };
                OnResize += (sender, e) =>
                {
                    Scrollbar.Maximum = Container.ContentHeight - Container.Height;
                    if (Scrollbar.Maximum < 0)
                        Scrollbar.Maximum = 0;
                    Scrollbar.Range = Container.Height;
                    if (Container.ContentHeight <= Container.Height)
                        Container.ContentOffset = Vector2.Zero;
                    Scrollbar.UpdateHeight();
                    UpdateScrollbarVisibility();
                };
                OnMouseScroll += (sender, e) => Scrollbar.Scroll(e.NewValue);
            }
            CutOverflownChildren = true;
        }

        protected virtual void UpdateScrollbarVisibility()
        {
            Container.IgnoreResizing = true;
            if (Container.ContentHeight > Container.Height)
            {
                Scrollbar.Visible = true;
                Container.Right = Scrollbar.Left;
            }
            else
            {
                Scrollbar.Visible = false;
                Container.Right = Scrollbar.Right;
            }
            Container.IgnoreResizing = false;
        }
    }
    public class UIHStackPanel : UIControl
    {
        public UIHStackPanelContainer Container { get; set; }

        public UIHStackPanel(Vector2 location, Vector2 size, float margin) : base(location, size)
        {
            Controls.Add(Container = new UIHStackPanelContainer(Vector2.Zero, new Vector2(Size.X, Size.Y), margin));
        }
    }
    public class UIStackPanelContainer : UIControl
    {
        private class UIStackPanelControlList : UIControlList
        {
            public UIStackPanelContainer StackPanelContainerParent { get; set; }

            public UIStackPanelControlList(UIStackPanelContainer parent) : base(parent)
            {
                StackPanelContainerParent = parent;
            }

            protected override void InsertItem(int index, UIControl item)
            {
                base.InsertItem(index, item);
                ++locked;
                if (locked == 1)
                    StackPanelContainerParent.ControlAdding(item);
                --locked;
            }
            protected override void RemoveItem(int index)
            {
                UIControl control = this[index];
                base.RemoveItem(index);
                ++locked;
                if (locked == 1)
                    StackPanelContainerParent.ControlRemoving(control);
                --locked;
            }
            protected override void ClearItems()
            {
                base.ClearItems();
                ++locked;
                if (locked == 1)
                    StackPanelContainerParent.ControlClearing();
                --locked;
            }
        }

        public float Margin { get; set; }
        public float ContentHeight { get; set; }
        public bool IgnoreResizing { get; set; }

        public event VectorEventHandler ContentHeightChanged;

        public UIStackPanelContainer(Vector2 location, Vector2 size, float margin) : base(location, size)
        {
            Margin = margin;
            Controls = new UIStackPanelControlList(this);
            CutOverflownChildren = true;
            ClickThrough = true;
        }

        private void ControlAdding(UIControl control)
        {
            float prevContentHeight = ContentHeight;
            control.OnResize += OnResizeHandler;
            control.OnVisibleChanged += OnVisibleChangedHandler;
            control.Left = 0;
            control.Top = ContentHeight == 0 ? ContentHeight : ContentHeight + Margin;
            ContentHeight = control.Visible ? control.Bottom : control.Top - Margin;
            if (ContentHeightChanged != null && ContentHeight != prevContentHeight)
                ContentHeightChanged(this, new VectorEventArgs() { OldValue = new Vector2(0, prevContentHeight), NewValue = new Vector2(0, ContentHeight) });
        }
        private void ControlRemoving(UIControl control)
        {
            float prevContentHeight = ContentHeight;
            control.OnResize -= OnResizeHandler;
            control.OnVisibleChanged -= OnVisibleChangedHandler;
            ContentHeight = 0;
            foreach (UIControl nextControl in Controls)
            {
                nextControl.Left = 0;
                nextControl.Top = ContentHeight == 0 ? ContentHeight : ContentHeight + Margin;
                ContentHeight = nextControl.Visible ? nextControl.Bottom : nextControl.Top - Margin;
            }
            if (ContentHeightChanged != null && ContentHeight != prevContentHeight)
                ContentHeightChanged(this, new VectorEventArgs() { OldValue = new Vector2(0, prevContentHeight), NewValue = new Vector2(0, ContentHeight) });
        }
        private void ControlClearing()
        {
            float prevContentHeight = ContentHeight;
            foreach (UIControl control in Controls)
            {
                control.OnResize -= OnResizeHandler;
                control.OnVisibleChanged -= OnVisibleChangedHandler;
            }
            ContentHeight = 0;
            if (ContentHeightChanged != null && ContentHeight != prevContentHeight)
                ContentHeightChanged(this, new VectorEventArgs() { OldValue = new Vector2(0, prevContentHeight), NewValue = new Vector2(0, ContentHeight) });
        }
        private void OnResizeHandler(object sender, VectorEventArgs e)
        {
            if (IgnoreResizing) return;
            float prevContentHeight = ContentHeight;
            ContentHeight = 0;
            foreach (UIControl nextControl in Controls)
            {
                nextControl.Left = 0;
                nextControl.Top = ContentHeight == 0 ? ContentHeight : ContentHeight + Margin;
                ContentHeight = nextControl.Visible ? nextControl.Bottom : nextControl.Top - Margin;
            }
            if (ContentHeightChanged != null && ContentHeight != prevContentHeight)
                ContentHeightChanged(this, new VectorEventArgs() { OldValue = new Vector2(0, prevContentHeight), NewValue = new Vector2(0, ContentHeight) });
        }
        private void OnVisibleChangedHandler(object sender, BoolEventArgs e)
        {
            float prevContentHeight = ContentHeight;
            ContentHeight = 0;
            foreach (UIControl nextControl in Controls)
            {
                nextControl.Left = 0;
                nextControl.Top = ContentHeight == 0 ? ContentHeight : ContentHeight + Margin;
                ContentHeight = nextControl.Visible ? nextControl.Bottom : nextControl.Top - Margin;
            }
            if (ContentHeightChanged != null)
                ContentHeightChanged(this, new VectorEventArgs() { OldValue = new Vector2(0, prevContentHeight), NewValue = new Vector2(0, ContentHeight) });
        }
    }
    public class UIHStackPanelContainer : UIControl
    {
        private class UIStackPanelControlList : UIControlList
        {
            public UIHStackPanelContainer StackPanelContainerParent { get; set; }

            public UIStackPanelControlList(UIHStackPanelContainer parent) : base(parent)
            {
                StackPanelContainerParent = parent;
            }

            protected override void InsertItem(int index, UIControl item)
            {
                base.InsertItem(index, item);
                ++locked;
                if (locked == 1)
                    StackPanelContainerParent.ControlAdding(item);
                --locked;
            }
            protected override void RemoveItem(int index)
            {
                UIControl control = this[index];
                base.RemoveItem(index);
                ++locked;
                if (locked == 1)
                    StackPanelContainerParent.ControlRemoving(control);
                --locked;
            }
            protected override void ClearItems()
            {
                base.ClearItems();
                ++locked;
                if (locked == 1)
                    StackPanelContainerParent.ControlClearing();
                --locked;
            }
        }

        public float Margin { get; set; }
        public float ContentWidth { get; set; }

        public event VectorEventHandler ContentWidthChanged;

        public UIHStackPanelContainer(Vector2 location, Vector2 size, float margin) : base(location, size)
        {
            Margin = margin;
            Controls = new UIStackPanelControlList(this);
            CutOverflownChildren = true;
        }

        private void ControlAdding(UIControl control)
        {
            float prevContentWidth = ContentWidth;
            control.OnResize += OnResizeHandler;
            control.Left = Controls.Count == 0 ? 0 : ContentWidth + Margin;
            control.Top = 0;
            ContentWidth = control.Right;
            if (ContentWidthChanged != null && ContentWidth != prevContentWidth)
                ContentWidthChanged(this, new VectorEventArgs() { OldValue = new Vector2(0, prevContentWidth), NewValue = new Vector2(0, ContentWidth) });
        }
        private void ControlRemoving(UIControl control)
        {
            float prevContentWidth = ContentWidth;
            control.OnResize -= OnResizeHandler;
            ContentWidth = control.Left;
            for (int i = Controls.IndexOf(control) + 1; i < Controls.Count; i++)
            {
                UIControl nextControl = Controls[i];
                nextControl.Left = ContentWidth + Margin;
                nextControl.Top = 0;
                ContentWidth = control.Right;
            }
            if (ContentWidthChanged != null && ContentWidth != prevContentWidth)
                ContentWidthChanged(this, new VectorEventArgs() { OldValue = new Vector2(0, prevContentWidth), NewValue = new Vector2(0, ContentWidth) });
        }
        private void ControlClearing()
        {
            foreach (UIControl control in Controls)
                control.OnResize -= OnResizeHandler;
            ContentWidth = 0;
        }
        private void OnResizeHandler(object sender, VectorEventArgs e)
        {
            float prevContentWidth = ContentWidth;
            UIControl control = (UIControl)sender;
            ContentWidth = control.Right;
            for (int i = Controls.IndexOf(control) + 1; i < Controls.Count; i++)
            {
                UIControl nextControl = Controls[i];
                nextControl.Left = ContentWidth + Margin;
                nextControl.Top = 0;
                ContentWidth = nextControl.Right;
            }
            if (ContentWidthChanged != null && ContentWidth != prevContentWidth)
                ContentWidthChanged(this, new VectorEventArgs() { OldValue = new Vector2(0, prevContentWidth), NewValue = new Vector2(0, ContentWidth) });
        }
    }
}
