﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Arcanos.Client.GameClient.UI
{
    [Flags]
    public enum UIAnchor
    {
        None,

        Top = 0x01,
        Left = 0x02,
        Right = 0x04,
        Bottom = 0x08,

        TopLeft = Top | Left,
        TopRight = Top | Right,
        BottomLeft = Bottom | Left,
        BottomRight = Bottom | Right,
        Horizontal = Left | Right,
        Vertical = Top | Bottom,
        All = Horizontal | Vertical,
    }
    [Flags]
    public enum UIFontAlign
    {
        Left = 0x01,
        Center = 0x02,
        Right = 0x04,

        Top = 0x08,
        Middle = 0x10,
        Bottom = 0x20,
    }
    public struct UIFont
    {
        private static readonly UIFont cachedEmpty = new UIFont();
        public static UIFont Empty
        {
            get { return cachedEmpty; }
        }

        public SpriteFont Font;
        public Vector2 Offset;
        public Color Color;
        public UIFontAlign Align;
        public float LineHeight;
        public float LineBreak;
        public Vector2 ShadowOffset;
        public Color ShadowColor;

        public UIFont(SpriteFont font, float height, float lineBreak, Vector2 offset, Color color, UIFontAlign align, Vector2 shadowOffset, Color shadowColor)
        {
            Font = font;
            LineHeight = height;
            LineBreak = lineBreak;
            Offset = offset;
            ShadowOffset = shadowOffset;
            Color = color;
            ShadowColor = shadowColor;
            Align = align;
            FixAlign();
        }
        public UIFont(SpriteFont font, float height, float lineBreak, Vector2 offset, Color color, UIFontAlign align) : this(font, height, lineBreak, offset, color, align, Vector2.Zero, Color.TransparentBlack) { }
        public UIFont(SpriteFont font, float height, float lineBreak, Vector2 offset, Color color) : this(font, height, lineBreak, offset, color, UIFontAlign.Center | UIFontAlign.Middle) { }
        public UIFont(SpriteFont font, float height, float lineBreak, Vector2 offset) : this(font, height, lineBreak, offset, Color.Black, UIFontAlign.Center | UIFontAlign.Middle) { }
        public UIFont(SpriteFont font, float height, float lineBreak, Vector2 offset, UIFontAlign align) : this(font, height, lineBreak, offset, Color.Black, align) { }
        public UIFont(SpriteFont font, float height, float lineBreak, Color color) : this(font, height, lineBreak, Vector2.Zero, color, UIFontAlign.Center | UIFontAlign.Middle) { }
        public UIFont(SpriteFont font, float height, float lineBreak, Color color, UIFontAlign align) : this(font, height, lineBreak, Vector2.Zero, color, align) { }
        public UIFont(UIFont source, UIFontAlign align) : this(source)
        {
            Align = align;
            FixAlign();
        }
        public UIFont(UIFont source, Color color) : this(source)
        {
            Color = color;
        }
        public UIFont(UIFont source, Vector2 offset, Color color) : this(source)
        {
            Offset = offset;
            Color = color;
        }
        public UIFont(UIFont source) : this(source.Font, source.LineHeight, source.LineBreak, source.Offset, source.Color, source.Align, source.ShadowOffset, source.ShadowColor) { }

        public UIFont MakeColor(Color color)
        {
            return new UIFont(this, color);
        }
        public UIFont MakeColor(byte r, byte g, byte b)
        {
            return MakeColor(new Color(r, g, b));
        }
        public UIFont MakeColor(byte r, byte g, byte b, byte a)
        {
            return MakeColor(new Color(r, g, b, a));
        }
        public UIFont MakeColor(float r, float g, float b)
        {
            return MakeColor(new Color(r, g, b));
        }
        public UIFont MakeColor(float r, float g, float b, float a)
        {
            return MakeColor(new Color(r, g, b, a));
        }
        public UIFont MakeShadow(float offset, byte alpha)
        {
            return new UIFont(this) { ShadowOffset = new Vector2(offset), ShadowColor = new Color(Color.Black, alpha) };
        }
        public UIFont MakeShadow(float offset, float alpha)
        {
            return new UIFont(this) { ShadowOffset = new Vector2(offset), ShadowColor = new Color(Color.Black, alpha) };
        }
        public UIFont MakeShadow(Vector2 offset, byte alpha)
        {
            return new UIFont(this) { ShadowOffset = offset, ShadowColor = new Color(Color.Black, alpha) };
        }
        public UIFont MakeShadow(Vector2 offset, float alpha)
        {
            return new UIFont(this) { ShadowOffset = offset, ShadowColor = new Color(Color.Black, alpha) };
        }
        public UIFont MakeOutline(byte alpha)
        {
            return new UIFont(this) { ShadowOffset = Vector2.Zero, ShadowColor = new Color(Color.Black, alpha) };
        }
        public UIFont MakeOutline(float alpha)
        {
            return new UIFont(this) { ShadowOffset = Vector2.Zero, ShadowColor = new Color(Color.Black, alpha) };
        }
        public UIFont MakeOutline(Color color)
        {
            return new UIFont(this) { ShadowOffset = Vector2.Zero, ShadowColor = color };
        }
        public UIFont MakeCentered()
        {
            return new UIFont(this, UIFontAlign.Middle | UIFontAlign.Center);
        }
        public UIFont MakeLeft()
        {
            return new UIFont(this, UIFontAlign.Left);
        }
        public UIFont MakeRight()
        {
            return new UIFont(this, UIFontAlign.Right);
        }
        public UIFont MakeTop()
        {
            return new UIFont(this, UIFontAlign.Top | UIFontAlign.Center);
        }
        public UIFont MakeTopLeft()
        {
            return new UIFont(this, UIFontAlign.Top | UIFontAlign.Left);
        }
        public UIFont MakeBottom()
        {
            return new UIFont(this, UIFontAlign.Bottom | UIFontAlign.Center);
        }
        public UIFont MakeBottomRight()
        {
            return new UIFont(this, UIFontAlign.Bottom | UIFontAlign.Right);
        }
        public UIFont MakeBottomLeft()
        {
            return new UIFont(this, UIFontAlign.Bottom | UIFontAlign.Left);
        }
        public UIFont MakeOffset(float x, float y)
        {
            return new UIFont(this) { Offset = new Vector2(x, y) };
        }
        public UIFont MakeOffset(Vector2 offset)
        {
            return new UIFont(this) { Offset = offset };
        }

        private void FixAlign()
        {
            if ((Align & (UIFontAlign.Left | UIFontAlign.Center | UIFontAlign.Right)) == 0)
                Align |= UIFontAlign.Center;
            if ((Align & (UIFontAlign.Top | UIFontAlign.Middle | UIFontAlign.Bottom)) == 0)
                Align |= UIFontAlign.Middle;
        }

        public Vector2 MeasureString(string str)
        {
            float w = 0;
            float h = 0;
            if (string.IsNullOrEmpty(str)) return Vector2.Zero;
            foreach (string line in str.Split(new[] { "\r\n", "\n\r", "\n", "\r" }, StringSplitOptions.None))
            {
                Vector2 lineSize = Font.MeasureString(line);
                if (lineSize.X > w) w = lineSize.X;
                h += LineHeight + LineBreak;
            }
            return new Vector2(w, h - LineBreak);
        }
        public Vector2 MeasureString(string str, float width)
        {
            float w = 0;
            float h = 0;
            foreach (string line in FitString(str, width))
            {
                Vector2 lineSize = Font.MeasureString(line);
                if (lineSize.X > w) w = lineSize.X;
                h += LineHeight + LineBreak;
            }
            return new Vector2(w, h);
        }

        private struct FitStringCacheKey
        {
            public readonly SpriteFont Font;
            public readonly float LineHeight;
            public readonly float LineBreak;
            public readonly string Text;
            public readonly float Width;

            public FitStringCacheKey(SpriteFont font, float lineHeight, float lineBreak, string text, float width)
            {
                Font = font;
                LineHeight = lineHeight;
                LineBreak = lineBreak;
                Text = text;
                Width = width;
            }

            public override bool Equals(object obj)
            {
                FitStringCacheKey other = (FitStringCacheKey)obj;
                return Width == other.Width && LineHeight == other.LineHeight && LineBreak == other.LineBreak && Font == other.Font && Text == other.Text;
            }
            public bool Equals(FitStringCacheKey other)
            {
                return Width.Equals(other.Width) && LineHeight.Equals(other.LineHeight) && LineBreak.Equals(other.LineBreak) && Equals(Font, other.Font) && string.Equals(Text, other.Text);
            }
            public override int GetHashCode()
            {
                unchecked
                {
                    int hashCode = (Font != null ? Font.GetHashCode() : 0);
                    hashCode = (hashCode * 397) ^ LineHeight.GetHashCode();
                    hashCode = (hashCode * 397) ^ LineBreak.GetHashCode();
                    hashCode = (hashCode * 397) ^ (Text != null ? Text.GetHashCode() : 0);
                    hashCode = (hashCode * 397) ^ Width.GetHashCode();
                    return hashCode;
                }
            }

            public override string ToString()
            {
                return "{" + string.Format("Width={1}, LineHeight={2}, LineBreak={3}, String=\"{0}\"", Text, Width, LineHeight, LineBreak) + "}";
            }
        }
        private static readonly Dictionary<FitStringCacheKey, string[]> FitStringCache = new Dictionary<FitStringCacheKey, string[]>();
        private static readonly Dictionary<FitStringCacheKey, Vector2> FitStringSizeCache = new Dictionary<FitStringCacheKey, Vector2>();
        public string[] FitString(string str, float width)
        {
            if (string.IsNullOrEmpty(str)) return new string[0];
            FitStringCacheKey key = new FitStringCacheKey(Font, LineHeight, LineBreak, str, width);
            string[] cachedResult;
            if (FitStringCache.TryGetValue(key, out cachedResult))
                return cachedResult;

            List<string> preFit = new List<string>(str.Split(new[] { "\r\n", "\n\r", "\n", "\r" }, StringSplitOptions.None));
            List<string> result = new List<string>();
            foreach (string line in preFit)
            {
                if (string.IsNullOrEmpty(line.Trim()))
                {
                    result.Add(line);
                    continue;
                }
                string lineSoFar = "";
                string prevLineSoFar = "";
                string[] words = line.Split(' ');
                for (int i = 0; i < words.Length; i++)
                {
                    lineSoFar += words[i];
                    Vector2 lineSize = MeasureString(lineSoFar);
                    if (i != words.Length - 1)
                        lineSoFar += " ";
                    if (lineSize.X > width)
                    {
                        result.Add(prevLineSoFar);
                        lineSoFar = words[i];
                        if (i != words.Length - 1)
                            lineSoFar += " ";
                        prevLineSoFar = "";
                    }
                    else
                        prevLineSoFar = lineSoFar;
                }
                if (!string.IsNullOrEmpty(lineSoFar))
                    result.Add(lineSoFar);
            }
            string[] resultArray = result.ToArray();
            FitStringCache[key] = resultArray;
            return resultArray;
        }
        public string[] FitString(string str, float width, out Vector2 size)
        {
            string[] lines = FitString(str, width);
            FitStringCacheKey key = new FitStringCacheKey(Font, LineHeight, LineBreak, str, width);
            if (FitStringSizeCache.TryGetValue(key, out size))
                return lines;
            float w = 0;
            float h = 0;
            foreach (string line in lines)
            {
                Vector2 lineSize = Font.MeasureString(line);
                if (lineSize.X > w) w = lineSize.X;
                h += LineHeight + LineBreak;
            }
            size = new Vector2(w, h);
            FitStringSizeCache.Add(key, size);
            return lines;
        }
    }
}