﻿using Microsoft.Xna.Framework;

namespace Arcanos.Client.GameClient.UI
{
    public class UIMaskedBox : UITextBox
    {
        public override bool HiddenInput
        {
            get { return true; }
        }

        public UIMaskedBox(Vector2 location, Vector2 size, string text, char maskChar) : base(location, size, text)
        {
            HiddenInputSymbol = maskChar;
        }
        public UIMaskedBox(Vector2 location, Vector2 size, string text) : this(location, size, text, '*') { }
        public UIMaskedBox(Vector2 location, Vector2 size, char maskChar) : this(location, size, "", maskChar) { }
        public UIMaskedBox(Vector2 location, Vector2 size) : this(location, size, "", '*') { }
    }
}