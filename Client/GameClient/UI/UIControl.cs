﻿using System;
using Arcanos.Client.GameClient.Input;
using Arcanos.Shared.Localization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Arcanos.Client.GameClient.UI
{
    public class UIControl : UIContainer, IDisposable
    {
        public static UITexture CommonGraphic { get; set; }
        public static UITexture CommonBackdrop { get; set; }
        public static UITexture CommonOverlay { get; set; }

        private UIContainer parent;
        public UIContainer Parent
        {
            get { return parent; }
            set
            {
                if (parent == value) return;
                UIContainer old = parent;
                parent = value;
                if (OnParentChanged != null)
                    OnParentChanged(this, new UIContainerEventArgs() { OldValue = old, NewValue = value });
                if (parent == null)
                    OnHide();
            }
        }
        public UIDesktop Desktop
        {
            get
            {
                UIControl parentControl = Parent as UIControl;
                if (parentControl != null)
                    return parentControl.Desktop;
                return (UIDesktop)Parent;
            }
        }

        private Vector2 location;
        public virtual Vector2 Location
        {
            get { return location; }
            set
            {
                if (location == value)
                    return;
                location = value;
                OnMouseMove(new Vector2(InputManager.MouseX, InputManager.MouseY));
            }
        }
        private Vector2 size;
        public virtual Vector2 Size
        {
            get { return size; }
            set
            {
                if (size == value) return;
                Vector2 old = size;
                size = value;
                if (OnResize != null)
                    OnResize(this, new VectorEventArgs() { OldValue = old, NewValue = value });
                foreach (UIControl control in Controls.Lock())
                    control.OnResized(Size - old);
                Controls.Unlock();
            }
        }
        public virtual Vector2 Scale { get; set; }
        public virtual Vector2 ContentOffset { get; set; }
        public virtual UIAnchor Anchor { get; set; }
        public float Top
        {
            get { return Location.Y; }
            set { Location = new Vector2(Location.X, value); }
        }
        public float Left
        {
            get { return Location.X; }
            set { Location = new Vector2(value, Location.Y); }
        }
        public float Right
        {
            get { return Location.X + Size.X; }
            set { Size = new Vector2(value - Location.X, Size.Y); }
        }
        public float Bottom
        {
            get { return Location.Y + Size.Y; }
            set { Size = new Vector2(Size.X, value - Location.Y); }
        }
        public float Width
        {
            get { return Size.X; }
            set { Size = new Vector2(value, Size.Y); }
        }
        public float Height
        {
            get { return Size.Y; }
            set { Size = new Vector2(Size.X, value); }
        }
        public Vector2 TopLeft
        {
            get { return Location; }
            set { Location = value; }
        }
        public Vector2 TopCenter
        {
            get { return new Vector2(Location.X + size.X / 2, Location.Y); }
            set { Location = new Vector2(value.X - size.X / 2, value.Y); }
        }
        public Vector2 TopRight
        {
            get { return new Vector2(Location.X + size.X, Location.Y); }
            set { Location = new Vector2(value.X - size.X, value.Y); }
        }
        public Vector2 MiddleLeft
        {
            get { return new Vector2(Location.X, Location.Y + size.Y / 2); }
            set { Location = new Vector2(value.X, value.Y - size.Y / 2); }
        }
        public Vector2 Center
        {
            get { return new Vector2(Location.X + size.X / 2, Location.Y + size.Y / 2); }
            set { Location = new Vector2(value.X - size.X / 2, value.Y - size.Y / 2); }
        }
        public Vector2 MiddleRight
        {
            get { return new Vector2(Location.X + size.X, Location.Y + size.Y / 2); }
            set { Location = new Vector2(value.X - size.X, value.Y - size.Y / 2); }
        }
        public Vector2 BottomLeft
        {
            get { return new Vector2(Location.X, Location.Y + size.Y); }
            set { Location = new Vector2(value.X, value.Y - size.Y); }
        }
        public Vector2 BottomCenter
        {
            get { return new Vector2(Location.X + size.X / 2, Location.Y + size.Y); }
            set { Location = new Vector2(value.X - size.X / 2, value.Y - size.Y); }
        }
        public Vector2 BottomRight
        {
            get { return new Vector2(Location.X + size.X, Location.Y + size.Y); }
            set { Location = new Vector2(value.X - size.X, value.Y - size.Y); }
        }

        public Vector2 DesktopLocation
        {
            get
            {
                UIControl parentControl = Parent as UIControl;
                if (parentControl != null)
                    return parentControl.DesktopLocation + Location + parentControl.ContentOffset;
                return Location;
            }
        }

        public virtual float Opacity { get; set; }
        public virtual UITexture[] StateGraphics { get; set; }
        public virtual UITexture BackdropGraphic { get; set; }
        public virtual Vector2 BackdropSize { get; set; }
        private UITexture currentGraphic;
        public virtual UITexture CurrentGraphic
        {
            get
            {
                if (StateGraphics == null)
                    return currentGraphic;
                if (!Enabled && StateGraphics.Length >= 3 && StateGraphics[3] != null) return StateGraphics[3];
                if (IsPressed && StateGraphics.Length >= 1 && StateGraphics[1] != null) return StateGraphics[1];
                if (IsHovered && StateGraphics.Length >= 2 && StateGraphics[2] != null) return StateGraphics[2];
                return StateGraphics[0];
            }
            set
            {
                StateGraphics = null;
                currentGraphic = value;
            }
        }
        public virtual UITexture OverlayGraphic { get; set; }
        public virtual Vector2 OverlaySize { get; set; }
        public virtual UIFont Font { get; set; }
        private string text;
        public virtual string Text
        {
            get { return text; }
            set
            {
                if (text == value) return;
                string old = text;
                text = value;
                if (OnTextChanged != null)
                    OnTextChanged(this, new TypeEventArgs<string>() { OldValue = old, NewValue = value });
            }
        }
        public Text TextRef { get; set; }
        public bool MultilineText { get; set; }
        public bool CutOverflownText { get; set; }
        public bool CutOverflownChildren { get; set; }
        public bool TextUsesBackdropSize { get; set; }
        public bool TextUsesOverlaySize { get; set; }
        public bool IgnoreBounds { get; set; }

        public virtual string Tooltip { get; set; }

        public bool Enabled { get; set; }

        private bool visible;
        public bool Visible
        {
            get { return visible; }
            set
            {
                if (visible == value) return;
                bool old = visible;
                visible = value;
                if (OnVisibleChanged != null)
                    OnVisibleChanged(this, new BoolEventArgs() { OldValue = old, NewValue = value });
                if (visible)
                    OnShow();
                else
                    OnHide();
            }
        }
        public bool ClickThrough { get; set; }
        public bool GetsFocus { get; set; }
        public bool IsHovered { get; set; }
        public bool IsPressed { get; set; }
        public bool IsRightPressed { get; set; }
        public bool IsFocused { get; set; }

        public object Tag { get; set; }

        public event EventHandler OnUpdate;
        public event EventHandler OnShown;
        public event EventHandler OnHidden;
        public event EventHandler OnClick;
        public event EventHandler OnRightClick;
        public event EventHandler OnMouseLeftDown;
        public event EventHandler OnMouseLeftUp;
        public event EventHandler OnMouseRightDown;
        public event EventHandler OnMouseRightUp;
        public event EventHandler OnMouseEnter;
        public event EventHandler OnMouseLeave;
        public event Int32EventHandler OnMouseScroll;
        public event VectorEventHandler OnResize;
        public event UIContainerEventHandler OnParentChanged;
        public event TypeEventHandler<string> OnTextChanged;
        public event BoolEventHandler OnVisibleChanged;

        public UIControl(Vector2 location, Vector2 size)
        {
            Location = location;
            this.size = size;
            Scale = Vector2.One;
            BackdropSize = size;
            OverlaySize = size;
            Anchor = UIAnchor.Top | UIAnchor.Left;

            Opacity = 1;
            CurrentGraphic = CommonGraphic;
            BackdropGraphic = CommonBackdrop;
            OverlayGraphic = CommonOverlay;

            CutOverflownText = true;

            Enabled = true;
            Visible = true;
            ClickThrough = false;
        }
        public virtual void Dispose()
        {
            foreach (UIControl control in Controls)
                control.Dispose();
        }

        public override void Update()
        {
            if (TextRef != null)
                if (!string.IsNullOrEmpty(TextRef.ToString()))
                {
                    Text = TextRef.ToString();
                    TextRef = null;
                }
            if (Enabled)
            {
                base.Update();
                if (OnUpdate != null)
                    OnUpdate(this, null);
            }
        }

        public virtual bool Focus()
        {
            if (!GetsFocus)
                return false;
            if (Desktop.FocusedControl != null)
                Desktop.FocusedControl.Unfocus();
            IsFocused = true;
            Desktop.FocusedControl = this;
            return true;
        }
        public virtual void Unfocus()
        {
            IsFocused = false;
            if (Desktop != null && Desktop.FocusedControl == this)
                Desktop.FocusedControl = null;
        }
        public virtual void FocusNext()
        {
            Unfocus();
            if (Controls.Count != 0)
                if (Controls[0].Focus())
                    return;
            UIContainer p = Parent;
            while (true)
            {
                if (p == null)
                    break;
                for (int i = p.Controls.IndexOf(this) + 1; i < p.Controls.Count; i++)
                    if (p.Controls[i].Focus())
                        break;
                if (p is UIControl)
                    p = ((UIControl)p).Parent;
                if (p is UIDesktop)
                    break;
            }
        }

        public virtual bool OnResized(Vector2 d)
        {
            bool handled = false;
            Vector2 newLoc = Location,
                    newSize = Size;

            if ((Anchor & UIAnchor.Right) == 0)
                if ((Anchor & UIAnchor.Left) == 0)
                {
                    newLoc.X += d.X / 2;
                    //newSize.X += d.X / 2;
                }
                else { }
            else
                if ((Anchor & UIAnchor.Left) == 0 )
                    newLoc.X += d.X;
                else
                    newSize.X += d.X;

            if ((Anchor & UIAnchor.Bottom) == 0)
                if ((Anchor & UIAnchor.Top) == 0)
                {
                    newLoc.Y += d.Y / 2;
                    //newSize.Y += d.Y / 2;
                }
                else { }
            else
                if ((Anchor & UIAnchor.Top) == 0)
                    newLoc.Y += d.Y;
                else
                    newSize.Y += d.Y;

            if (Location != newLoc || Size != newSize)
            {
                Location = newLoc;
                BackdropSize += Size - newSize;
                Vector2 oldSize = Size;
                size = newSize;
                if (OnResize != null)
                    OnResize(this, new VectorEventArgs() { OldValue = oldSize, NewValue = Size });
                foreach (UIControl control in Controls.Lock())
                    handled |= control.OnResized(Size - oldSize);
                Controls.Unlock();
            }
            return handled;
        }
        public virtual bool OnMouseMove(Vector2 m)
        {
            if (!Visible) return false;
            bool handled = false;
            //if (!ClickThrough)
            //{
            Vector2 newLoc = DesktopLocation;
            Vector2 newSize = Size;
            bool wasHovered = IsHovered;
            IsHovered = m.X >= newLoc.X && m.X < newLoc.X + newSize.X &&
                        m.Y >= newLoc.Y && m.Y < newLoc.Y + newSize.Y;
            UIControl parentControl = Parent as UIControl;
            while (IsHovered && parentControl != null)
            {
                if (!parentControl.IgnoreBounds)
                {
                    newLoc = parentControl.DesktopLocation;
                    newSize = parentControl.Size;
                    IsHovered &= m.X >= newLoc.X && m.X < newLoc.X + newSize.X &&
                                 m.Y >= newLoc.Y && m.Y < newLoc.Y + newSize.Y;
                }
                if (parentControl.Parent is UIControl)
                    parentControl = parentControl.Parent as UIControl;
                else
                    break;
            }
            if (wasHovered && !IsHovered)
            {
                if (OnMouseLeave != null)
                    OnMouseLeave(this, null);
                if (Desktop != null && Desktop.TooltipSource == this)
                    Desktop.ResetTooltip();
            }
            if (IsHovered)
            {
                if (!wasHovered)
                    if (OnMouseEnter != null)
                        OnMouseEnter(this, null);
                if (!ClickThrough)
                    handled = true;
                if (Desktop != null && !string.IsNullOrEmpty(Tooltip))
                    Desktop.SetTooltip(this);
            }
            //}
            foreach (UIControl control in Controls.LockReverse())
                if (control.OnMouseMove(m))
                    handled = true;
            Controls.Unlock();
            return handled;
        }
        public virtual bool OnMouseLeft(bool pressed)
        {
            if (!Visible) return false;
            bool handled = false;
            if (!ClickThrough)
            {
                if (IsPressed && !pressed)
                {
                    if (OnMouseLeftUp != null)
                        OnMouseLeftUp(this, null);
                    if (IsHovered)
                    {
                        if (OnClick != null)
                            OnClick(this, null);
                        //if (Desktop != null)
                        handled = true;
                    }
                }
                IsPressed = pressed && IsHovered;
                if (IsPressed)
                {
                    Focus();
                    if (OnMouseLeftDown != null)
                        OnMouseLeftDown(this, null);
                    //if (Desktop != null)
                    handled = true;
                }
            }
            foreach (UIControl control in Controls.LockReverse())
                if (control.OnMouseLeft(pressed))
                {
                    handled = true;
                    break;
                }
            Controls.Unlock();
            return handled;
        }
        public virtual bool OnMouseRight(bool pressed)
        {
            if (!Visible) return false;
            bool handled = false;
            if (!ClickThrough)
            {
                if (IsRightPressed && !pressed)
                {
                    if (OnMouseRightUp != null)
                        OnMouseRightUp(this, null);
                    if (IsHovered)
                    {
                        if (OnRightClick != null)
                            OnRightClick(this, null);
                        //if (Desktop != null)
                        handled = true;
                    }
                }
                IsRightPressed = pressed && IsHovered;
                if (IsRightPressed)
                {
                    Focus();
                    if (OnMouseRightDown != null)
                        OnMouseRightDown(this, null);
                    //if (Desktop != null)
                    handled = true;
                }
            }
            foreach (UIControl control in Controls.LockReverse())
                if (control.OnMouseRight(pressed))
                {
                    handled = true;
                    break;
                }
            Controls.Unlock();
            return handled;
        }
        public virtual bool OnMouseScrolled(int amount)
        {
            if (!Visible) return false;
            bool handled = false;
            if (IsHovered)
            {
                if (OnMouseScroll != null)
                {
                    OnMouseScroll(this, new Int32EventArgs() { NewValue = amount });
                    handled = true;
                }
            }
            foreach (UIControl control in Controls.LockReverse())
                if (control.OnMouseScrolled(amount))
                {
                    handled = true;
                    break;
                }
            Controls.Unlock();
            return handled;
        }
        public virtual bool OnKeyPress(Keys key)
        {
            bool handled = false;
            foreach (UIControl control in Controls.LockReverse())
                if (control.OnKeyPress(key))
                {
                    handled = true;
                    break;
                }
            Controls.Unlock();
            return handled;
        }
        public virtual bool OnKeyRelease(Keys key)
        {
            bool handled = false;
            foreach (UIControl control in Controls.LockReverse())
                if (control.OnKeyRelease(key))
                {
                    handled = true;
                    break;
                }
            Controls.Unlock();
            return handled;
        }
        protected virtual void OnShow()
        {
            if (OnShown != null)
                OnShown(this, null);
            foreach (UIControl control in Controls.Lock())
                control.OnShow();
            Controls.Unlock();
        }
        protected virtual void OnHide()
        {
            if (OnHidden != null)
                OnHidden(this, null);
            foreach (UIControl control in Controls.Lock())
                control.OnHide();
            Controls.Unlock();
        }

        public virtual void AfterDraw() { }
    }
}