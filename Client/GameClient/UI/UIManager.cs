﻿using System;
using System.Collections.Generic;
using System.IO;
using Arcanos.Shared.Localization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Arcanos.Client.GameClient.UI
{
    public static class UIManager
    {
        private static GraphicsDevice device;
        private static ContentManager content;

        public static readonly Dictionary<int, UIDesktop> Desktops = new Dictionary<int, UIDesktop>();
        public static UIDesktop CurrentDesktop { get; private set; }
        public static UIDesktop CurrentlyResizedDesktop { get; private set; }
        private static readonly Dictionary<string, Texture2D> loadedTextures = new Dictionary<string, Texture2D>(); 

        public static Vector2 Size { get; private set; }

        public static void Init(GraphicsDevice graphicsDevice, ContentManager contentManager)
        {
            device = graphicsDevice;
            content = contentManager;
        }

        public static UIDesktop MakeDesktop(int id)
        {
            return Desktops[id] = new UIDesktop(Size);
        }
        public static Texture2D LoadTexture(string filename)
        {
            Texture2D tex;
            if (loadedTextures.TryGetValue(filename, out tex))
                return tex;
            tex = Texture2D.FromFile(device, filename);
            loadedTextures.Add(filename, tex);
            return tex;
        }
        public static SpriteFont LoadFont(string filename)
        {
            string localizedFontFilename = filename + "." + Localization.CurrentCulture.Name;
            if (File.Exists(Path.Combine(content.RootDirectory, localizedFontFilename + ".xnb")))
                return content.Load<SpriteFont>(localizedFontFilename);

            return content.Load<SpriteFont>(filename);
        }
        
        public static void SetSize(Vector2 size)
        {
            Size = size;
            foreach (KeyValuePair<int, UIDesktop> desktop in Desktops)
            {
                CurrentlyResizedDesktop = desktop.Value;
                desktop.Value.Size = Size;
            }
            CurrentlyResizedDesktop = null;
        }
        public static void SetCurrent(int id)
        {
            UIDesktop desktop;
            CurrentDesktop = Desktops.TryGetValue(id, out desktop) ? desktop : null;
        }

        public static void ShowMessage(string message)
        {
            ShowMessage(message, null, null, null, null, null, null);
        }
        public static void ShowMessage(string message, string buttonA, EventHandler handlerA)
        {
            ShowMessage(message, buttonA, handlerA, null, null, null, null);
        }
        public static void ShowMessage(string message, string buttonA, EventHandler handlerA, string buttonB, EventHandler handlerB)
        {
            ShowMessage(message, buttonA, handlerA, buttonB, handlerB, null, null);
        }
        public static void ShowMessage(string message, string buttonA, EventHandler handlerA, string buttonB, EventHandler handlerB, string buttonC, EventHandler handlerC)
        {
            if (CurrentDesktop == null)
                return;

            Vector2 messageSize = UIWindow.ContentFont.MeasureString(message, Size.X / 3);
            Vector2 size = (messageSize + new Vector2(0, 24)).Int();
            Vector2 loc = ((Size - size) / 2).Int();
            UIBlackout blackout = new UIBlackout(true);
            UIWindow window = new UIWindow(loc, size, "Message") { Anchor = UIAnchor.None };
            window.Controls.Add(new UIControl(Vector2.Zero, messageSize)
            {
                Text = message,
                MultilineText = true,
                Font = UIWindow.ContentFont.MakeCentered(),
            });
            int buttonCount = buttonC == null ? buttonB == null ? buttonA == null ? 0 : 1 : 2 : 3;
            if (buttonCount == 0)
            {
                UIButton button;
                window.Controls.Add(button = new UIButton(new Vector2(size.X / 2 - 32, window.ContentHeight - 24).Int(), new Vector2(64, 24), UIButtonType.Medium, "OK"));
                button.OnClick += (sender, args) =>
                {
                    blackout.Disable();
                    CurrentDesktop.Controls.Remove(window);
                };
            }
            else
            {
                float w = size.X / (buttonCount + 1);
                string[] buttonTexts = { buttonA, buttonB, buttonC };
                EventHandler[] buttonHandlers = { handlerA, handlerB, handlerC };
                for (int i = 0; i < buttonCount; i++)
                {
                    int iClosure = i;
                    UIButton button;
                    window.Controls.Add(button = new UIButton(new Vector2(w + i * 64 - 32, window.ContentHeight - 24).Int(), new Vector2(64, 24), UIButtonType.Medium, buttonTexts[i]));
                    button.OnClick += (sender, args) =>
                    {
                        buttonHandlers[iClosure](sender, args);
                        blackout.Disable();
                        CurrentDesktop.Controls.Remove(window);
                    };
                }
            }
            CurrentDesktop.Controls.Add(blackout);
            CurrentDesktop.Controls.Add(window);
            blackout.Enable();
        }

        public static void RaiseEvent(UIEvents eventName, object sender, params object[] args)
        {
            //if (CurrentDesktop == null)
                //return;

            foreach (UIDesktop desktop in Desktops.Values)
                desktop.RaiseEvent(eventName, sender, args);
        }

        public static bool KeyPress(Keys key)
        {
            if (CurrentDesktop == null)
                return false;

            return CurrentDesktop.PerformKeyPress(key);
        }
        public static bool KeyRelease(Keys key)
        {
            if (CurrentDesktop == null)
                return false;

            return CurrentDesktop.PerformKeyRelease(key);
        }
        public static bool UpdateMouse(Vector2 loc, bool leftDown, bool rightDown, int scrollAmount)
        {
            if (CurrentDesktop == null)
                return false;

            return CurrentDesktop.PerformMouseHandling(loc, leftDown, rightDown, scrollAmount);
        }

        public static void Update()
        {
            if (CurrentDesktop == null)
                return;

            CurrentDesktop.Update();
        }
    }
}