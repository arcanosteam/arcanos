﻿using System;
using Arcanos.Shared.Localization;
using Microsoft.Xna.Framework;

namespace Arcanos.Client.GameClient.UI
{
    public struct UIDropdownItemData
    {
        public string Text;
        public Text TextRef;
        public UITexture Icon;
        public EventHandler OnClick;
    }
    public class UIDropdownItem : UIButton
    {
        public new static UITexture NormalTexture { get; set; }
        public new static UITexture HoveredTexture { get; set; }
        public new static UITexture PressedTexture { get; set; }
        public new static UITexture DisabledTexture { get; set; }
        public static UIFont TextFont { get; set; }

        public override UITexture BackdropGraphic
        {
            get
            {
                if (Icon == null) return null;
                if (!Enabled && DisabledTexture != null) return DisabledTexture;
                if (IsPressed && PressedTexture != null) return PressedTexture;
                if (IsHovered && HoveredTexture != null) return HoveredTexture;
                return NormalTexture;
            }
        }
        public override UITexture CurrentGraphic
        {
            get
            {
                if (Icon != null) return null;
                if (!Enabled && DisabledTexture != null) return DisabledTexture;
                if (IsPressed && PressedTexture != null) return PressedTexture;
                if (IsHovered && HoveredTexture != null) return HoveredTexture;
                return NormalTexture;
            }
        }
        public override UIFont Font
        {
            get { return TextFont; }
        }

        public UIDropdownItem(Vector2 location, Vector2 size, string text, UITexture icon) : base(location, size, UIButtonType.Medium, text, icon) { }
        public UIDropdownItem(Vector2 location, Vector2 size, Text text, UITexture icon) : base(location, size, UIButtonType.Medium, text, icon) { }
    }
    public class UIDropdown : UIButton
    {
        public static UITexture ArrowIcon { get; set; }

        public UIDropdownItemData[] Items { get; set; }

        public UIDropdown(Vector2 location, Vector2 size, UIButtonType type, string text) : base(location, size, type, text, ArrowIcon)
        {
            OnClick += OnClickHandler;
        }

        public void OpenDropdown()
        {
            Desktop.OpenDropdown(this, Items);
        }

        private void OnClickHandler(object sender, EventArgs eventArgs)
        {
            OpenDropdown();
        }
    }
}