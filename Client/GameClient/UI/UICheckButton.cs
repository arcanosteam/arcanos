﻿using Arcanos.Shared.Localization;
using Microsoft.Xna.Framework;

namespace Arcanos.Client.GameClient.UI
{
    public class UICheckButton : UIButton
    {
        public bool Checked
        {
            get { return AlwaysPressed; }
            set { AlwaysPressed = value; }
        }

        public UICheckButton(Vector2 location, Vector2 size, UIButtonType type, string text, UITexture icon) : base(location, size, type, text, icon)
        {
            Init();
        }
        public UICheckButton(Vector2 location, Vector2 size, UIButtonType type, Text text, UITexture icon) : base(location, size, type, text, icon)
        {
            Init();
        }
        public UICheckButton(Vector2 location, Vector2 size, UIButtonType type, string text) : base(location, size, type, text)
        {
            Init();
        }
        public UICheckButton(Vector2 location, Vector2 size, UIButtonType type, Text text) : base(location, size, type, text)
        {
            Init();
        }
        public UICheckButton(Vector2 location, Vector2 size, UIButtonType type, UITexture icon) : base(location, size, type, icon)
        {
            Init();
        }

        private void Init()
        {
            OnClick += (sender, args) => Checked = !Checked;
        }
    }
}