﻿using Arcanos.Client.GameClient.Data;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Arcanos.Client.GameClient.UI
{
    public class UITexture
    {
        private readonly int storedTextureID;
        private Texture2D storedTexture;

        public Texture2D Texture
        {
            get { return storedTexture ?? DatabaseManager.TextureData[storedTextureID]; }
            set { storedTexture = value; }
        }
        public Vector2 Offset;
        public Vector2 Size;
        public Vector2 PixelSize { get { return Texture == null ? Vector2.Zero : Size * new Vector2(Texture.Width, Texture.Height); } }
        public Vector2 PaddingMin;
        public Vector2 PixelPaddingMin { get { return Texture == null ? Vector2.Zero : PaddingMin * new Vector2(Texture.Width, Texture.Height); } }
        public Vector2 PaddingMax;
        public Vector2 PixelPaddingMax { get { return Texture == null ? Vector2.Zero : PaddingMax * new Vector2(Texture.Width, Texture.Height); } }
        public Vector2 PixelPadding { get { return PixelPaddingMin + PixelPaddingMax; } }
        public float PixelPaddingH { get { return PixelPaddingMin.X + PixelPaddingMax.X; } }
        public float PixelPaddingV { get { return PixelPaddingMin.Y + PixelPaddingMax.Y; } }
        public bool Alpha = true;
        public bool Wrap;
        
        public UITexture(string file) : this(UIManager.LoadTexture(file)) { }
        public UITexture(Texture2D texture)
        {
            if (texture == null) return;
            storedTexture = texture;
            Offset = Vector2.Zero;
            Size = Vector2.One;
            PaddingMin = Vector2.Zero;
            PaddingMax = Vector2.Zero;
        }
        public UITexture(int textureID)
        {
            storedTextureID = textureID;
            Offset = Vector2.Zero;
            Size = Vector2.One;
            PaddingMin = Vector2.Zero;
            PaddingMax = Vector2.Zero;
        }
        public UITexture(string file, int offsetX, int offsetY, int width, int height, int paddingTop, int paddingLeft, int paddingRight, int paddingBottom) : this(UIManager.LoadTexture(file), offsetX, offsetY, width, height, paddingTop, paddingLeft, paddingRight, paddingBottom) { }
        public UITexture(Texture2D texture, int offsetX, int offsetY, int width, int height, int paddingTop, int paddingLeft, int paddingRight, int paddingBottom)
        {
            if (texture == null) return;
            if (width == 0)
                width = texture.Width;
            if (height == 0)
                height = texture.Height;
            storedTexture = texture;
            Offset = new Vector2((float)offsetX / texture.Width, (float)offsetY / texture.Height);
            Size = new Vector2((float)width / texture.Width, (float)height / texture.Height);
            PaddingMin = new Vector2((float)paddingLeft / texture.Width, (float)paddingTop / texture.Height);
            PaddingMax = new Vector2((float)paddingRight / texture.Width, (float)paddingBottom / texture.Height);
        }

        public static UITexture[] FromStrip(Texture2D tex, int count)
        {
            return FromStrip(tex, count, 0, 0, 0, 0);
        }
        public static UITexture[] FromStrip(Texture2D tex, int count, int paddingTop, int paddingLeft, int paddingRight, int paddingBottom)
        {
            UITexture[] strip = new UITexture[count];
            for (int i = 0; i < count; i++)
                strip[i] = new UITexture(tex, i * tex.Width / count, 0, tex.Width / count, tex.Height, paddingTop, paddingLeft, paddingRight, paddingBottom);
            return strip;
        }
    }
}