﻿using System;
using Arcanos.Client.GameClient.Input;
using Arcanos.Client.GameClient.Objects;
using Arcanos.Client.GameClient.Rendering;
using Arcanos.Shared;
using Arcanos.Shared.Graphics;
using Arcanos.Shared.Objects;
using Microsoft.Xna.Framework;
using Environment = Arcanos.Client.GameClient.World.Environment;

namespace Arcanos.Client.GameClient.UI
{
    public class UIModel : UIControl
    {
        public static Environment DefaultEnvironment { get; set; }

        private class PlacementEntity : IWorldEntity
        {
            public Vector3 Position;
            public Rotation Rotation;
            public Vector3 Size;

            public PlacementEntity(Vector3 pos, Rotation rot, Vector3 size)
            {
                Position = pos;
                Rotation = rot;
                Size = size;
            }

            public Vector3 GetPosition()
            {
                return Position;
            }
            public Rotation GetRotation()
            {
                return Rotation;
            }
            public Vector3 GetSize()
            {
                return Size;
            }
        }
        private Model model;
        private ModelInstance instance;
        private readonly PlacementEntity entity;
        private readonly Camera camera;
        private Vector3 position, size = Vector3.One;
        private Rotation rotation;

        public Model Model
        {
            get { return model; }
            set
            {
                if (model == value)
                    return;
                if (OnModelChanged != null)
                    OnModelChanged(this, new TypeEventArgs<Model>(model, model = value));
                else
                    model = value;
            }
        }
        public ModelInstance Instance
        {
            get { return instance; }
        }
        public Vector3 ModelPosition
        {
            get { return position; }
            set
            {
                if (position == value)
                    return;
                if (OnPositionChanged != null)
                    OnPositionChanged(this, new TypeEventArgs<Vector3>(position, position = value));
                else
                    position = value;
            }
        }
        public Rotation ModelRotation
        {
            get { return rotation; }
            set
            {
                if (rotation == value)
                    return;
                if (OnRotationChanged != null)
                    OnRotationChanged(this, new TypeEventArgs<Rotation>(rotation, rotation = value));
                else
                    rotation = value;
            }
        }
        public Vector3 ModelSize
        {
            get { return size; }
            set
            {
                if (size == value)
                    return;
                if (OnSizeChanged != null)
                    OnSizeChanged(this, new TypeEventArgs<Vector3>(size, size = value));
                else
                    size = value;
            }
        }
        public Environment Environment { get; set; }

        public event TypeEventHandler<Model> OnModelChanged;
        public event TypeEventHandler<Vector3> OnPositionChanged;
        public event TypeEventHandler<Rotation> OnRotationChanged;
        public event TypeEventHandler<Vector3> OnSizeChanged;

        public UIModel(Vector2 location, Vector2 size) : base(location, size)
        {
            entity = new PlacementEntity(Vector3.Zero, Rotation.Zero, Vector3.Zero);
            camera = new Camera();
            Environment = DefaultEnvironment;
            OnModelChanged += (sender, args) =>
            {
                instance = Model == null ? null : Model.CreateInstance(entity);
                UpdateEntityPosition();
            };
            OnPositionChanged += (sender, args) => UpdateEntityPosition();
            OnRotationChanged += (sender, args) => UpdateEntityPosition();
            OnSizeChanged += (sender, args) => UpdateEntityPosition();
            OnParentChanged += (sender, args) => UpdateEntityPosition();
            OnResize += (sender, args) => UpdateEntityPosition();
        }

        public override void Update()
        {
            base.Update();
            if (instance != null)
                instance.Update();
        }

        private void UpdateEntityPosition()
        {
            entity.Position = new Vector3(DesktopLocation, 1) + ModelPosition;
            if (Model != null)
            {
                switch (Model.Origin)
                {
                    case ModelOrigin.Center:
                        entity.Position += new Vector3(Size / 2, 0);
                        break;
                    case ModelOrigin.Bottom:
                        entity.Position += new Vector3(Width / 2, Height, 0);
                        break;
                    case ModelOrigin.Top:
                        entity.Position += new Vector3(Width / 2, 0, 0);
                        break;
                }
            }
            entity.Rotation = new Rotation(-MathHelper.PiOver2) + ModelRotation;
            entity.Size = new Vector3(Math.Min(Width, Height)) * ModelSize;
            camera.Target = entity.Position;
            camera.Position = entity.Position + new Vector3(0, -0.01f, -1);
        }

        public override bool OnMouseMove(Vector2 m)
        {
            if (IsPressed)
                ModelRotation += new Rotation(MathHelper.Pi * (InputManager.MouseX - InputManager.PrevMouseX) / 200);
            return base.OnMouseMove(m);
        }

        public override void AfterDraw()
        {
            base.AfterDraw();
            if (model == null)
                return;
            UpdateEntityPosition();
            TerrainRenderer.SetEnvironment(Environment);
            switch (model.Type)
            {
                case ModelType.SpriteModel:
                case ModelType.MultiAngleSpriteModel:
                    SpriteRenderer.UpdateOrtho(Vector2.Zero, Desktop.Size);
                    SpriteRenderer.Render(instance, camera, entity.Position, entity.Rotation, entity.Size);
                    UIRenderer.Activate();
                    break;
                case ModelType.WorldModel:
                {
                    WorldModel m = model as WorldModel;
                    WorldModelInstance i = instance as WorldModelInstance;
                    ModelRenderer.UpdateOrtho(Vector2.Zero, Desktop.Size);
                    ModelRenderer.Render(i, m, Matrix.CreateRotationX(entity.Rotation.Roll) * Matrix.CreateRotationY(entity.Rotation.Pitch) * Matrix.CreateRotationZ(entity.Rotation.Yaw) * Matrix.CreateScale(model.RelativeSize ? entity.Size * m.Size : m.Size) * Matrix.CreateTranslation(entity.Position));
                    UIRenderer.Activate();
                    break;
                }
            }
        }
    }
}