﻿using Arcanos.Client.GameClient.World;
using Arcanos.Shared.Localization;
using Microsoft.Xna.Framework;

namespace Arcanos.Client.GameClient.UI.Game
{
    public class UIExperienceBar : UIGauge
    {
        public class UIExperienceBarLevel : UIControl
        {
            public override string Tooltip
            {
                get { return string.Format(Localization.Game.TooltipExperienceLevelFormat.ToString(), Text); }
            }

            public UIExperienceBarLevel(Vector2 location, Vector2 size) : base(location, size) { }
        }

        public static UIFont LevelFont { get; set; }
        public static UITexture ExperienceBarBackdrop { get; set; }
        public static UITexture ExperienceBarGaugeTexture { get; set; }

        public override string Tooltip
        {
            get { return string.Format(Localization.Game.TooltipExperienceBarFormat.ToString(), storedValue, storedMax, 100 * storedValue / storedMax); }
        }

        public UIExperienceBarLevel LevelControl { get; set; }

        public UIExperienceBar(Vector2 location, Vector2 size, Vector2 offsetMin, Vector2 offsetMax) : base(location, size, offsetMin, offsetMax, null, -3)
        {
            BackdropGraphic = ExperienceBarBackdrop;
            GaugeControl.CurrentGraphic = ExperienceBarGaugeTexture;
            TextFormat = "{0} / {2} ({3:0}%)";

            Controls.Add(LevelControl = new UIExperienceBarLevel(Vector2.Zero, new Vector2(32, Height))
            {
                Font = LevelFont,
                Text = "",
            });

            OnParentChanged += (sender, args) =>
            {
                Desktop.RegisterEvent(UIEvents.PlayerLoaded, PlayerLoadedHandler);
                Desktop.RegisterEvent(UIEvents.CreatureLeveledUp, CreatureLeveledUpHandler);
                Desktop.RegisterEvent(UIEvents.ExperienceChanged, ExperienceChangedHandler);
            };
        }

        private void PlayerLoadedHandler(UIDesktop desktop, object sender, params object[] args)
        {
            SetSource(WorldManager.PlayerCreature);
            LevelControl.Text = WorldManager.PlayerCreature.Character.Level.ToString();
        }
        private void CreatureLeveledUpHandler(UIDesktop desktop, object sender, params object[] args)
        {
            ulong guid = (ulong)args[0];
            if (guid == WorldManager.PlayerCreature.GUID)
            {
                LevelControl.Text = WorldManager.PlayerCreature.Character.Level.ToString();
                GaugeControl.Width = 0;
            }
        }
        private void ExperienceChangedHandler(UIDesktop desktop, object sender, params object[] args)
        {
            if (Source == null)
                SetSource(WorldManager.PlayerCreature);
            UpdateTarget();
        }
    }
}