﻿using System.Collections.Generic;
using Arcanos.Client.GameClient.Chat;
using Arcanos.Client.GameClient.Input;
using Arcanos.Shared.Localization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Arcanos.Client.GameClient.UI.Game
{
    public class UIChatInputBox : UITextBox
    {
        protected readonly List<string> MessageHistory = new List<string> { "" };
        protected int MessageHistoryCurrentIndex = 0;

        public UIDropdown ActiveChannelButton { get; set; }

        public UIChatInputBox(Vector2 location, Vector2 size) : this(location, size, "") { }
        public UIChatInputBox(Vector2 location, Vector2 size, string text) : base(location, size, text)
        {
            Controls.Add(ActiveChannelButton = new UIDropdown(Vector2.Zero, new Vector2(0, Height), UIButtonType.Tiny, "")
            {
                Visible = false,
            });
            OnParentChanged += (sender, args) =>
            {
                Desktop.RegisterEvent(UIEvents.ChatActiveChannelChanged, ChatActiveChannelChangedHandler);
                Desktop.RegisterEvent(UIEvents.ChatChannelJoined, ChatChannelUpdateHandler);
                Desktop.RegisterEvent(UIEvents.ChatChannelLeft, ChatChannelUpdateHandler);
            };
            CutOverflownChildren = false;
        }

        public override bool OnKeyPress(Keys key)
        {
            if (!base.OnKeyPress(key))
                return false;

            if (key == Keys.Up)
                Value = MessageHistory[MessageHistoryCurrentIndex == 0 ? MessageHistoryCurrentIndex = MessageHistory.Count - 1 : --MessageHistoryCurrentIndex];
            if (key == Keys.Down)
                Value = MessageHistory[MessageHistoryCurrentIndex == MessageHistory.Count - 1 ? MessageHistoryCurrentIndex = 0 : ++MessageHistoryCurrentIndex];
            if (key == Keys.Enter)
            {
                MessageHistoryCurrentIndex = 0;
                if (!string.IsNullOrEmpty(Value) && (MessageHistory.Count == 0 || MessageHistory[MessageHistory.Count - 1] != Value))
                    MessageHistory.Add(Value);
                if (!InputHandler.HandleChatCommand(Value))
                    ChatManager.SendMessage(Value);
                Unfocus();
                Value = "";
                return true;
            }

            char c = InputManager.GetChar(key);
            if (c == ' ' || c == '\t')
                InputHandler.HandleChatAlias(Value);

            return true;
        }

        private void ChatActiveChannelChangedHandler(UIDesktop desktop, object sender, params object[] args)
        {
            if (ChatManager.ActiveChannel == null)
            {
                ContentOffset = Vector2.Zero;
                ActiveChannelButton.Visible = false;
                return;
            }
            string name = ChatManager.ActiveChannel.GetNameForLocale(Localization.CurrentLocale);
            Vector2 nameSize = ActiveChannelButton.Font.MeasureString(name);
            float buttonWidth = nameSize.X + ActiveChannelButton.CurrentGraphic.PixelPaddingH;
            ContentOffset = new Vector2(buttonWidth - CurrentGraphic.PixelPaddingMin.X, 0);
            ActiveChannelButton.Left = -ContentOffset.X;
            ActiveChannelButton.Width = buttonWidth;
            ActiveChannelButton.Text = name;
            ActiveChannelButton.Visible = true;
        }
        private void ChatChannelUpdateHandler(UIDesktop desktop, object sender, params object[] args)
        {
            UIDropdownItemData[] items = new UIDropdownItemData[ChatManager.JoinedChannels.Count - 1];
            int i = 0;
            foreach (KeyValuePair<int, ChatChannel> channel in ChatManager.JoinedChannels)
            {
                if (channel.Key == -1)
                    continue;
                int id = channel.Value.ID;
                items[i].Text = channel.Value.GetNameForLocale(Localization.CurrentLocale);
                items[i].Icon = null;
                items[i].OnClick += (o, eventArgs) => ChatManager.SetActiveChannel(id);
                ++i;
            }
            ActiveChannelButton.Items = items;
        }
    }
}