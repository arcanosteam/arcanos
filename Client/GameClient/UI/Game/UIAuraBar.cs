﻿using Arcanos.Client.GameClient.Data;
using Arcanos.Client.GameClient.Spells;
using Arcanos.Shared.Objects;
using Microsoft.Xna.Framework;

namespace Arcanos.Client.GameClient.UI.Game
{
    public class UIAuraBar : UIHStackPanel
    {
        public CreatureBase Owner { get; set; }

        public UIAuraBar(Vector2 location, Vector2 size, float margin) : base(location, size, margin)
        {
            //ClickThrough = true;
            //Container.ClickThrough = true;
            OnParentChanged += (sender, args) =>
            {
                if (Desktop == null)
                    return;

                Desktop.RegisterEvent(UIEvents.AuraApplied, AuraAppliedHandler);
                Desktop.RegisterEvent(UIEvents.AuraRemoved, AuraRemovedHandler);
            };
        }

        private void AuraAppliedHandler(UIDesktop desktop, object sender, object[] args)
        {
            ulong guid = (ulong)args[0];
            int id = (int)args[1];

            if (Owner == null || guid != Owner.GUID)
                return;

            DatabaseManager.Spells.Get(id, spell =>
            {
                foreach (Aura aura in Owner.SpellBook.GetAuras(spell))
                    AddAura(aura);
            });
        }
        private void AuraRemovedHandler(UIDesktop desktop, object sender, object[] args)
        {
            ulong guid = (ulong)args[0];
            int id = (int)args[1];

            if (Owner == null || guid != Owner.GUID)
                return;

            DatabaseManager.Spells.Get(id, RemoveAura);
        }

        public void SetTarget(CreatureBase target)
        {
            if (Owner == target)
                return;
            Owner = target;
            Container.Controls.Clear();
            foreach (VisibleAura visAura in (target.SpellBook as SpellBook).VisibleAuras)
                AddAura(visAura.Aura);
        }
        public void AddAura(Aura aura)
        {
            for (int i = 0; i < Container.Controls.Count; i++)
                if ((Container.Controls[i] as UIAura).Aura.Spell == aura.Spell)
                    return;
            Container.Controls.Add(new UIAura(aura));
        }
        public void RemoveAura(SpellTemplate spell)
        {
            for (int i = 0; i < Container.Controls.Count; i++)
                if ((Container.Controls[i] as UIAura).Aura.Spell == spell)
                    Container.Controls.RemoveAt(i);
        }
    }
}