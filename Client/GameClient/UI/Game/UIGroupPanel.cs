﻿using Arcanos.Client.GameClient.World;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Players;
using Microsoft.Xna.Framework;

namespace Arcanos.Client.GameClient.UI.Game
{
    public class UIGroupPanel : UIControl
    {
        public UIStackPanel MembersControl { get; set; }
        public UICreatureFrame[] MemberFrames { get; set; }

        public UIGroupPanel(Vector2 location, Vector2 size) : base(location, size)
        {
            ClickThrough = true;
            Controls.Add(MembersControl = new UIStackPanel(Vector2.Zero, Size, 0, false)
            {
                Anchor = UIAnchor.All,
                ClickThrough = true,
            });
            OnParentChanged += (sender, args) =>
            {
                Desktop.RegisterEvent(UIEvents.JoinedGroup, JoinedGroupHandler);
                Desktop.RegisterEvent(UIEvents.LeftGroup, LeftGroupHandler);
                Desktop.RegisterEvent(UIEvents.GroupMemberJoined, GroupMemberJoinedHandler);
                Desktop.RegisterEvent(UIEvents.GroupMemberLeft, GroupMemberLeftHandler);
            };
        }

        private void JoinedGroupHandler(UIDesktop desktop, object sender, params object[] args)
        {
            Visible = true;
            if (MemberFrames == null)
            {
                MemberFrames = new UICreatureFrame[GroupBase.MAX_GROUP_MEMBERS];
                for (int i = 0; i < GroupBase.MAX_GROUP_MEMBERS; i++)
                    MembersControl.Container.Controls.Add(MemberFrames[i] = new UICreatureFrame(Vector2.Zero));
            }
        }
        private void LeftGroupHandler(UIDesktop desktop, object sender, params object[] args)
        {
            Visible = false;
            foreach (UICreatureFrame control in MemberFrames)
                control.SetTarget(null);
        }
        private void GroupMemberJoinedHandler(UIDesktop desktop, object sender, params object[] args)
        {
            ulong memberGUID = (ulong)args[0];
            byte index = (byte)args[1];
            //bool initial = (bool)args[2];
            if (memberGUID == WorldManager.PlayerCreature.GUID)
            {
                MemberFrames[index].SetTarget(null);
                return;
            }
            MemberFrames[index].SetTarget(ObjectManager.GetPlayer(memberGUID));
        }
        private void GroupMemberLeftHandler(UIDesktop desktop, object sender, params object[] args)
        {
            byte index = (byte)args[1];
            MemberFrames[index].SetTarget(null);
        }
    }
}