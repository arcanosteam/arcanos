﻿using Arcanos.Client.GameClient.Data;
using Arcanos.Client.GameClient.Spells;
using Microsoft.Xna.Framework;

namespace Arcanos.Client.GameClient.UI.Game
{
    public class UISpell : UIControl
    {
        public static Vector2 DefaultSize { get; set; }
        public static Vector2 IconSize { get; set; }

        public override string Tooltip
        {
            get
            {
                if (base.Tooltip != null)
                    return base.Tooltip;
                if (SpellTemplate == null)
                    return null;

                bool missing = false;
                string tooltip = UITooltip.Build(SpellTemplate, ref missing);
                if (!missing)
                    base.Tooltip = tooltip;

                return tooltip;
            }
            set { base.Tooltip = value; }
        }

        public SpellTemplate SpellTemplate { get; set; }

        public UIControl IconControl { get; set; }
        public UICooldown CooldownControl { get; set; }

        public UISpell(Vector2 location, Vector2 size, SpellTemplate spell) : base(location, size)
        {
            Controls.Add(IconControl = new UIControl((Size - IconSize) / 2, IconSize)
            {
                ClickThrough = true,
            });
            Controls.Add(CooldownControl = new UICooldown((Size - IconSize) / 2, IconSize)
            {
                Anchor = UIAnchor.None,
            });
            SetDisplaySpell(spell);
        }

        public void SetDisplaySpell(SpellTemplate spell)
        {
            bool itemChanged = SpellTemplate != spell;
            base.Tooltip = null;
            SpellTemplate = spell;
            if (spell == null)
                IconControl.CurrentGraphic = null;
            else if (itemChanged)
                IconControl.CurrentGraphic = new UITexture(spell.Icon);
        }
    }
}