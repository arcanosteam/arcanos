﻿using Arcanos.Client.GameClient.World;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Net;
using Microsoft.Xna.Framework;

namespace Arcanos.Client.GameClient.UI.Game
{
    public class UILootMoney : UIItemDefinitionSlot, IUILootItem
    {
        public static UITexture BackgroundTexture { get; set; }
        public static UITexture MoneyIcon { get; set; }
        public static UIFont NameFont { get; set; }

        public override UITexture CurrentGraphic
        {
            get { return BackgroundTexture; }
        }

        public ulong SourceGUID { get; set; }
        public byte ItemIndex { get; set; }

        public UIControl NameControl { get; set; }

        public UILootMoney(Vector2 location, Vector2 size, ulong sourceGUID, byte itemIndex, int count) : base(location, new Vector2(size.Y), null)
        {
            SourceGUID = sourceGUID;
            ItemIndex = itemIndex;
            IconControl.CurrentGraphic = MoneyIcon;
            CountControl.Text = count.ToString();
            Controls.Add(NameControl = new UIText(new Vector2(Width + 4, 0), new Vector2(size.X - Width - 4, Height), NameFont, UIFontAlign.Left | UIFontAlign.Middle,
                new ValueFormatToken(Localization.Game.LootMoneyFormat, () => new object[] { count })));
            OnClick += (sender, args) => WorldManager.PlayerSession.SendPacket(new CLootItemPacket { SourceGUID = SourceGUID, ItemIndex = ItemIndex });
        }
    }
}