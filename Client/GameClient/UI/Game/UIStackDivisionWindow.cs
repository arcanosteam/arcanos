﻿using System;
using Microsoft.Xna.Framework;

namespace Arcanos.Client.GameClient.UI.Game
{
    public class UIStackDivisionWindow : UIWindow
    {
        public event Action<int> Accepted;

        public UIStackDivisionWindow(int proposedCount, int min, int max) : base(Vector2.Zero, new Vector2(128, 0), "Divide Stack")
        {
            UIControl label;
            UIButton inc, dec;
            UITextBox textbox;
            UIButton button;
            Controls.Add(label = new UIControl(Vector2.Zero, new Vector2(ContentWidth, 12))
            {
                Anchor = UIAnchor.Top | UIAnchor.Left | UIAnchor.Right,
                Font = ContentFont,
                Text = "New stack size (" + min + "-" + max + "):",
            });
            Controls.Add(dec = new UIButton(label.BottomLeft, new Vector2(24, 24), UIButtonType.Medium, "<"));
            float h = UITextBox.NormalTexture.PixelPaddingV + UITextBox.TextFont.LineHeight;
            Controls.Add(textbox = new UITextBox(new Vector2(24, label.Bottom + (int)((24 - h) / 2)), new Vector2(ContentWidth - 48, UITextBox.NormalTexture.PixelPaddingV + UITextBox.TextFont.LineHeight))
            {
                Anchor = UIAnchor.Top | UIAnchor.Left | UIAnchor.Right,
                Text = proposedCount.ToString(),
                AllowDigits = true,
                AllowEnters = false,
                AllowLetters = false,
                AllowSpaces = false,
                AllowSymbols = false,
                AllowTabs = false,
            });
            Controls.Add(inc = new UIButton(textbox.TopRight, new Vector2(24, 24), UIButtonType.Medium, ">"));
            dec.OnClick += (sender, args) => Modify(textbox, -1, min, max);
            inc.OnClick += (sender, args) => Modify(textbox, 1, min, max);
            textbox.TextAccepted += (sender, args) => Click(textbox.Value, min, max);
            Controls.Add(button = new UIButton(new Vector2(ContentWidth / 2 - 32, textbox.Bottom + (int)((24 - h) / 2)), new Vector2(64, 24), UIButtonType.Medium, "OK"));
            button.OnClick += (sender, args) => textbox.Unfocus();
            ContentHeight = button.Bottom;
            button.Anchor = UIAnchor.Bottom;
            OnParentChanged += (sender, args) =>
            {
                if (Desktop == null)
                    return;
                Location = ((Desktop.Size - Size) / 2).Int();
                textbox.Focus();
            };
        }

        protected void Modify(UITextBox textbox, int d, int min, int max)
        {
            int result;
            if (!int.TryParse(textbox.Value, out result))
                return;
            result += d;
            if (result < min)
                result = min;
            if (result > max)
                result = max;
            textbox.Text = result.ToString();
        }
        protected void Click(string value, int min, int max)
        {
            int result;
            if (!int.TryParse(value, out result))
                return;
            if (result < min || result > max)
                return;
            Parent.Controls.Remove(this);
            if (Accepted != null)
                Accepted(result);
        }
    }
}