﻿using System.Collections.Generic;
using Arcanos.Client.GameClient.Items;
using Arcanos.Shared.Items;
using Microsoft.Xna.Framework;

namespace Arcanos.Client.GameClient.UI.Game
{
    public class UIItemDefinitionSlot : UIControl
    {
        public static Vector2 DefaultSize { get; set; }
        public static Vector2 IconSize { get; set; }
        public static UIFont CountFont { get; set; }
        public static Dictionary<ItemQuality, UITexture> QualityOverlayTextures { get; set; }

        public override string Tooltip
        {
            get
            {
                if (base.Tooltip != null)
                    return base.Tooltip;
                if (ItemTemplate == null)
                    return null;

                bool missing = false;
                string tooltip = UITooltip.Build(ItemTemplate, Count, ref missing);
                if (!missing)
                    base.Tooltip = tooltip;

                return tooltip;
            }
            set { base.Tooltip = value; }
        }

        public ItemTemplate ItemTemplate { get; set; }
        public int Count { get; set; }

        public UIControl IconControl { get; set; }
        public UIControl CountControl { get; set; }
        public UIControl QualityOverlayControl { get; set; }

        public UIItemDefinitionSlot(Vector2 location, Vector2 size, ItemDefinition item) : this(location, size, item == null ? null : item.Template as ItemTemplate, item == null ? 0 : item.Count) { }
        public UIItemDefinitionSlot(Vector2 location, Vector2 size, ItemTemplate item, int count) : base(location, size)
        {
            Controls.Add(IconControl = new UIControl((Size - IconSize) / 2, IconSize)
            {
                ClickThrough = true,
            });
            Controls.Add(CountControl = new UIControl(IconControl.Location, IconControl.Size)
            {
                Font = CountFont,
                ClickThrough = true,
            });
            Controls.Add(QualityOverlayControl = new UIControl(Vector2.Zero, Size));
            OnParentChanged += (sender, args) =>
            {
                if (Desktop == null)
                    return;
                Desktop.RegisterEvent(UIEvents.LevelUp, TooltipResetHandler);
            };
            SetDisplayItem(item, count);
        }

        public void SetDisplayItem(ItemTemplate item, int count)
        {
            bool itemChanged = ItemTemplate != item;
            base.Tooltip = null;
            ItemTemplate = item;
            Count = count;
            if (item == null)
            {
                IconControl.CurrentGraphic = null;
                CountControl.Text = "";
                QualityOverlayControl.Visible = false;
            }
            else
            {
                if (itemChanged)
                    IconControl.CurrentGraphic = new UITexture(item.Icon);
                CountControl.Text = item.Stacks ? count.ToString() : "";
                QualityOverlayControl.CurrentGraphic = QualityOverlayTextures[item.Quality];
                QualityOverlayControl.Visible = QualityOverlayControl.Enabled;
            }
        }

        private void TooltipResetHandler(UIDesktop desktop, object sender, params object[] args)
        {
            Tooltip = null;
        }
    }
}