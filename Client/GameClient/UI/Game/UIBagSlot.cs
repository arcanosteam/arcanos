﻿using System.Collections.Generic;
using Arcanos.Client.GameClient.Input;
using Arcanos.Client.GameClient.Items;
using Arcanos.Client.GameClient.World;
using Arcanos.Shared.Items;
using Arcanos.Shared.Net;
using Arcanos.Shared.Timing;
using Microsoft.Xna.Framework;

namespace Arcanos.Client.GameClient.UI.Game
{
    public class UIBagSlot : UIItemSlot
    {
        public static UITexture BackgroundTexture { get; set; }

        public override UITexture CurrentGraphic
        {
            get { return BackgroundTexture; }
        }

        public Bag Bag { get; set; }
        public byte Slot { get; set; }

        private long lastClickTimestamp;

        public UIBagSlot(Vector2 location, Vector2 size, Bag bag, byte slot) : base(location, size, null)
        {
            Bag = bag;
            Slot = slot;
            OnParentChanged += (sender, args) => Desktop.RegisterEvent(UIEvents.BagSlotUpdated, BagSlotUpdatedHandler);
            OnClick += (sender, args) =>
            {
                if (Desktop.DraggedItem == null && Desktop.DraggedSpell == null)
                {
                    if (Item != null)
                        Desktop.PickItem(Item);
                }
                else if (Desktop.DraggedItem != null)
                {
                    Item item = Desktop.DraggedItem;
                    int count = Desktop.DraggedItemCount;
                    Desktop.PutItem();
                    if (item == Bag.Items[Slot])
                    {
                        if (Time.Timestamp - lastClickTimestamp < 250)
                        {
                            InputHandler.ItemDoubleClick(Item);
                            lastClickTimestamp = 0;
                        }
                    }
                    else
                    {
                        if (Bag == WorldManager.Player.CurrentCharacter.Equipment)
                            InputHandler.EquipItem(item, (EquipSlot)Slot);
                        else if (item.Bag == WorldManager.Player.CurrentCharacter.Equipment)
                            InputHandler.UnequipItem(item, (byte)Bag.Inventory.IndexOf(Bag), Slot);
                        else if (count == 0)
                            WorldManager.PlayerSession.SendPacket(new CMoveItemPacket
                            {
                                OldBagIndex = (byte)item.Inventory.IndexOf(item.Bag),
                                OldSlot = (byte)item.Bag.SlotOf(item),
                                NewBagIndex = (byte)Bag.Inventory.IndexOf(Bag),
                                NewSlot = Slot,
                            });
                        else
                            WorldManager.PlayerSession.SendPacket(new CDivideItemPacket
                            {
                                OldBagIndex = (byte)item.Inventory.IndexOf(item.Bag),
                                OldSlot = (byte)item.Bag.SlotOf(item),
                                NewBagIndex = (byte)Bag.Inventory.IndexOf(Bag),
                                NewSlot = Slot,
                                NewCount = count,
                            });
                    }
                }
                lastClickTimestamp = Time.Timestamp;
            };
            OnRightClick += (sender, args) =>
            {
                if (Item == null)
                    return;

                List<UIDropdownItemData> items = new List<UIDropdownItemData>();
                if (Item.Template.IsEquippable)
                {
                    if (Item.IsEquipped)
                        items.Add(new UIDropdownItemData
                        {
                            Text = "Unequip",
                            OnClick = (o, eventArgs) => InputHandler.UnequipItem(Item),
                        });
                    else
                        items.Add(new UIDropdownItemData
                        {
                            Text = "Equip",
                            OnClick = (o, eventArgs) => InputHandler.EquipItem(Item),
                        });
                }
                if (Item.Template.IsUsable)
                    items.Add(new UIDropdownItemData
                    {
                        Text = "Use",
                        OnClick = (o, eventArgs) => InputHandler.UseItem(Item),
                    });
                if (Item.Template.Stacks && Item.Count > 1)
                    items.Add(new UIDropdownItemData
                    {
                        Text = "Split",
                        OnClick = (o, eventArgs) =>
                        {
                            UIStackDivisionWindow window;
                            Desktop.Add(window = new UIStackDivisionWindow(Item.Count / 2, 1, Item.Count - 1));
                            window.Accepted += count => Desktop.PickItem(Item, count);
                        }
                    });
                items.Add(new UIDropdownItemData
                {
                    Text = "Destroy",
                    OnClick = (o, eventArgs) =>
                        UIManager.ShowMessage(string.Format("Do you really want to destroy {1}x[{0}]?", Item.Template.Name, Item.Count),
                            "Yes", (sender1, args1) => InputHandler.DestoryItem(Item),
                            "No", (sender1, args1) => { })
                });
                Desktop.OpenDropdown(this, items.ToArray());
            };
        }

        private void BagSlotUpdatedHandler(UIDesktop desktop, object sender, params object[] args)
        {
            sbyte bagIndex = (sbyte)args[0];
            byte slot = (byte)args[1];
            ulong itemGUID = (ulong)args[2];

            if (bagIndex == Bag.Inventory.IndexOf(Bag) && slot == Slot)
                if (itemGUID == 0)
                    SetDisplayItem(null);
                else
                    ItemManager.Get(itemGUID, SetDisplayItem);
        }
    }
}