﻿using System.Collections.Generic;
using Arcanos.Client.GameClient.Items;
using Microsoft.Xna.Framework;

namespace Arcanos.Client.GameClient.UI.Game
{
    public class UIBag : UIControl
    {
        public const int SLOTS_PER_ROW = 4;
        public static UITexture BackgroundTexture { get; set; }

        private UITexture overrideGraphic;
        public override UITexture CurrentGraphic
        {
            get { return overrideGraphic ?? BackgroundTexture; }
            set { overrideGraphic = value; }
        }

        private Bag bag;
        public Bag Bag
        {
            get { return bag; }
            set
            {
                if (bag == value)
                    return;
                if (BagChanged != null)
                    BagChanged(this, new TypeEventArgs<Bag>(bag, bag = value));
            }
        }

        public List<UIBagSlot> Slots { get; set; }

        public event TypeEventHandler<Bag> BagChanged;

        public UIBag(Vector2 location, Bag bag) : base(location, Vector2.Zero)
        {
            Visible = false;
            ContentOffset = CurrentGraphic.PixelPaddingMin;

            BagChanged += (sender, args) =>
            {
                Slots = new List<UIBagSlot>(Bag.Size);
                for (byte slot = 0; slot < Bag.Size; slot++)
                {
                    lastSlot = new UIBagSlot(GetSlotPosition(slot), UIItemDefinitionSlot.DefaultSize, Bag, slot);

                    if ((slot % SLOTS_PER_ROW) == 0)
                        lastRowFistSlot = lastSlot;

                    Controls.Add(lastSlot);
                    Slots.Add(lastSlot);
                }
                Size = GetSize();
                Visible = true;
            };
            Bag = bag;
        }

        private UIBagSlot lastRowFistSlot = null, lastSlot = null;
        protected virtual Vector2 GetSlotPosition(byte slot)
        {
            Vector2 pos = slot == 0 ? Vector2.Zero : (slot % SLOTS_PER_ROW) == 0 ? lastRowFistSlot.BottomLeft : lastSlot.TopRight;

            if ((slot % SLOTS_PER_ROW) == 0)
                lastRowFistSlot = lastSlot;

            return pos;
        }
        protected virtual Vector2 GetSize()
        {
            return new Vector2(CurrentGraphic.PixelPaddingH + SLOTS_PER_ROW * UIItemDefinitionSlot.DefaultSize.X, CurrentGraphic.PixelPaddingV + (lastSlot == null ? 0 : lastSlot.Bottom));
        }
    }
}