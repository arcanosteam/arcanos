﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Arcanos.Client.GameClient.UI.Game
{
    public class UIChatFrame : UIControl
    {
        private class UIChatFrameStackPanel : UIStackPanel
        {
            public UIChatFrameStackPanel(Vector2 location, Vector2 size, float margin, bool showScrollbar) : base(location, size, margin, showScrollbar)
            {
                Scrollbar.Left = 0;
                Container.Left = Scrollbar.Right + 4;
                Container.Right -= 4;
            }

            protected override void UpdateScrollbarVisibility()
            {
                Scrollbar.Visible = Container.ContentHeight > Container.Height;
            }
        }

        public static UITexture BackgroundTexture { get; set; }
        public static UIFont MessageFont { get; set; }

        public override UITexture CurrentGraphic
        {
            get { return BackgroundTexture; }
        }

        public UIStackPanel LinesContainer { get; set; }

        public UIChatFrame(Vector2 location, Vector2 size) : base(location, size)
        {
            ClickThrough = true;
            Controls.Add(LinesContainer = new UIChatFrameStackPanel(Vector2.Zero, Size, 0, true)
            {
                Anchor = UIAnchor.All,
                ClickThrough = true,
            });
        }

        public void AddLine(string text)
        {
            AddLine(MessageFont.Color, text);
        }
        public void AddLine(Color color, string text)
        {
            const float INSET = 1;
            UIControl lineControl = new UIControl(Vector2.Zero, new Vector2(LinesContainer.Container.Width - INSET * 2, MessageFont.MeasureString(text, LinesContainer.Container.Width).Y))
            {
                Anchor = UIAnchor.Top | UIAnchor.Horizontal,
                Font = MessageFont.Color == color ? MessageFont : MessageFont.MakeColor(color),
                Text = text,
                CutOverflownText = false,
                MultilineText = true,
                ClickThrough = true,
                ContentOffset = new Vector2(INSET, 0),
            };
            lineControl.OnResize += (sender, args) =>
            {
                (sender as UIControl).Height = MessageFont.MeasureString((sender as UIControl).Text, LinesContainer.Container.Width).Y + 2;
            };
            AddLine(lineControl);
        }
        public UIText AddLine(Color color, params ITextToken[] tokens)
        {
            const float INSET = 1;
            UIFont font = MessageFont.Color == color ? MessageFont : MessageFont.MakeColor(color);
            UIText lineControl = new UIText(Vector2.Zero, new Vector2(LinesContainer.Container.Width - INSET * 2, 0), font, tokens)
            {
                Anchor = UIAnchor.Top | UIAnchor.Horizontal,
                CutOverflownText = false,
                ClickThrough = true,
                ContentOffset = new Vector2(INSET, 0),
            };
            AddLine(lineControl);
            return lineControl;
        }
        private void AddLine(UIControl lineControl)
        {
            bool isScrolledDown = LinesContainer.Scrollbar.GetScrollPercent() > 0.99 || LinesContainer.Scrollbar.Minimum == LinesContainer.Scrollbar.Maximum;
            bool isOverflown = LinesContainer.Container.Controls.Count == 0 || LinesContainer.Container.Controls[LinesContainer.Container.Controls.Count - 1].Bottom > LinesContainer.Container.Height;

            LinesContainer.Container.Controls.Add(lineControl);

            if (!isOverflown && LinesContainer.Container.Controls[LinesContainer.Container.Controls.Count - 1].Bottom > LinesContainer.Container.Height)
                isScrolledDown = true;
            if (isScrolledDown)
                LinesContainer.Scrollbar.ScrollToPercent(1);
        }
    }
}