﻿using Arcanos.Client.GameClient.Items;
using Arcanos.Client.GameClient.World;
using Arcanos.Shared.Net;
using Microsoft.Xna.Framework;

namespace Arcanos.Client.GameClient.UI.Game
{
    public class UILootSlot : UIItemDefinitionSlot, IUILootItem
    {
        public static UITexture BackgroundTexture { get; set; }
        public static UIFont NameFont { get; set; }

        public override UITexture CurrentGraphic
        {
            get { return BackgroundTexture; }
        }

        public ulong SourceGUID { get; set; }
        public byte ItemIndex { get; set; }

        public UIControl NameControl { get; set; }

        public UILootSlot(Vector2 location, Vector2 size, ulong sourceGUID, byte itemIndex, ItemDefinition item) : base(location, new Vector2(size.Y), item)
        {
            SourceGUID = sourceGUID;
            ItemIndex = itemIndex;
            CutOverflownChildren = false;
            Controls.Add(NameControl = new UIControl(new Vector2(Width + 4, 0), new Vector2(size.X - Width - 4, Height))
            {
                Font = NameFont.MakeColor(Item.GetQualityColor(item.Template.Quality)),
                Text = item.Template.Name.ToString(),
                TextRef = item.Template.Name,
                MultilineText = true,
            });
            OnClick += (sender, args) => WorldManager.PlayerSession.SendPacket(new CLootItemPacket { SourceGUID = SourceGUID, ItemIndex = ItemIndex });
        }
    }
}