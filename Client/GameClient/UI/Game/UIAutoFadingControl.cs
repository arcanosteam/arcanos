﻿using Arcanos.Shared.Timing;
using Microsoft.Xna.Framework;

namespace Arcanos.Client.GameClient.UI.Game
{
    public class UIAutoFadingControl : UIControl
    {
        public float Delay { get; set; }
        public float FadeDuration { get; set; }

        public UIAutoFadingControl(Vector2 location, Vector2 size) : this(location, size, 2, 0.5f) { }
        public UIAutoFadingControl(Vector2 location, Vector2 size, float delay, float duration) : base(location, size)
        {
            Delay = delay;
            FadeDuration = duration;
        }

        private float time;
        public override void Update()
        {
            base.Update();
            if (!Visible)
                return;
            time += Time.PerSecond;
            if (time > Delay)
            {
                float fadeTime = time - Delay;
                Opacity = 1 - fadeTime / FadeDuration;
                if (Opacity <= 0)
                {
                    Opacity = 0;
                    Visible = false;
                }
            }
        }

        public void Show()
        {
            Visible = true;
            Opacity = 1;
            time = 0;
        }
        public void Show(float opacity)
        {
            if (opacity <= 0)
            {
                Visible = false;
                Opacity = 0;
                time = Delay + FadeDuration;
                return;
            }
            if (opacity >= 1)
            {
                Visible = true;
                Opacity = 1;
                time = 0;
            }

            Visible = true;
            Opacity = opacity;
            time = Delay + (1 - opacity) * FadeDuration;
        }
        public void TransferTo(UIAutoFadingControl control)
        {
            control.Visible = Visible;
            control.Opacity = Opacity;
            control.time = time;
        }
    }
}