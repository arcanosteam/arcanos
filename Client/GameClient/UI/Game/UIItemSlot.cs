﻿using Arcanos.Client.GameClient.Items;
using Arcanos.Client.GameClient.Spells;
using Arcanos.Shared.Items;
using Microsoft.Xna.Framework;

namespace Arcanos.Client.GameClient.UI.Game
{
    public class UIItemSlot : UIItemDefinitionSlot
    {
        public Item Item { get; set; }
        
        public UICooldown CooldownControl { get; set; }

        public UIItemSlot(Vector2 location, Vector2 size, Item item) : base(location, size, item == null ? null : item.Template as ItemTemplate, item == null ? 0 : item.Count)
        {
            Controls.Add(CooldownControl = new UICooldown((Size - IconSize) / 2, IconSize)
            {
                Anchor = UIAnchor.None,
            });
            OnParentChanged += (sender, args) =>
            {
                Desktop.RegisterEvent(UIEvents.ItemUpdated, ItemUpdatedHandler);
                Desktop.RegisterEvent(UIEvents.SpellCooldownUpdated, SpellCooldownUpdatedHandler);
                Desktop.RegisterEvent(UIEvents.GlobalCooldownUpdated, SpellCooldownUpdatedHandler);
            };
            SetDisplayItem(Item);
        }

        public void SetDisplayItem(Item item)
        {
            if (item == null)
                SetDisplayItem(null, 0);
            else
                SetDisplayItem(item.Template as ItemTemplate, item.Count);
            Item = item;
        }

        private void ItemUpdatedHandler(UIDesktop desktop, object sender, params object[] args)
        {
            ulong guid = (ulong)args[0];
            if (Item != null && Item.GUID == guid)
                SetDisplayItem(Item);
        }
        private void SpellCooldownUpdatedHandler(UIDesktop desktop, object sender, params object[] args)
        {
            if (Item == null)
                return;

            float cooldownLeft = (float)args[0];
            float cooldownTotal = (float)args[1];
            int cooldownGroup = args.Length > 2 ? (int)args[2] : 0;

            CooldownControl.Visible = false;
            for (ItemSpellUsage i = 0; i < ItemSpellUsage.Max; ++i)
            {
                SpellTemplate spell = Item.Template.SpellTriggers[(byte)i] as SpellTemplate;
                if (spell == null)
                    continue;
                if (cooldownGroup != 0 && spell.CooldownGroup != cooldownGroup)
                    return;
                CooldownControl.CooldownDuration = cooldownTotal;
                CooldownControl.CooldownLeft = cooldownLeft;
                CooldownControl.Visible = CooldownControl.CooldownLeft > 0;
            }
        }
    }
}