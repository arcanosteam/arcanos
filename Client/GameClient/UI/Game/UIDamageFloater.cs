﻿using System.Collections.Generic;
using Arcanos.Client.GameClient.World;
using Arcanos.Shared.Timing;
using Microsoft.Xna.Framework;

namespace Arcanos.Client.GameClient.UI.Game
{
    public enum DamageFloaterType
    {
        IncomingMelee,
        IncomingMagic,
        IncomingHeal,
        OutgoingMelee,
        OutgoingMagic,
        OutgoingHeal,
    }
    public class UIDamageFloater : UIControl
    {
        public const float FADE_TIME = 0.2f;

        public static Dictionary<DamageFloaterType, UIFont> DamageFonts { get; set; }

        public Vector3 WorldPosition { get; set; }
        public float StayTime { get; set; }

        private bool fadingIn, fadingOut;
        private bool delete;

        public UIDamageFloater(DamageFloaterType type, string text, Vector3 worldPosition) : base(Vector2.Zero, Vector2.Zero)
        {
            WorldPosition = worldPosition;
            StayTime = 0.5f;
            Font = DamageFonts[type];
            Text = text;
            Scale = new Vector2(0.5f, 0.5f);
            Opacity = 0;
            Visible = true;
            CutOverflownText = false;
            fadingIn = true;
            UpdatePosition();
        }

        public override void Update()
        {
            UpdatePosition();
            if (delete)
            {
                Parent.Controls.Remove(this);
                return;
            }
            base.Update();
            if (fadingIn)
            {
                Opacity += (1 / FADE_TIME) * Time.PerSecond;
                Scale += new Vector2((0.5f / FADE_TIME) * Time.PerSecond);
                if (Opacity >= 1)
                {
                    Opacity = 1;
                    fadingIn = false;
                }
            }
            else if (fadingOut)
            {
                Opacity -= (1 / FADE_TIME) * Time.PerSecond;
                Scale += new Vector2((0.25f / FADE_TIME) * Time.PerSecond);
                if (Opacity <= 0)
                {
                    Opacity = 0;
                    Visible = false;
                    fadingOut = false;
                    Parent.Controls.Remove(this);
                }
            }
            else
            {
                StayTime -= Time.PerSecond;
                if (StayTime <= 0)
                    fadingOut = true;
            }
        }

        private void UpdatePosition()
        {
            float depth;
            Vector3 loc = WorldManager.Camera.Project(WorldPosition, out depth);
            if (depth <= 0 || depth >= 1)
            {
                delete = true;
                return;
            }
            Location = new Vector2(loc.X, loc.Y);
        }
    }
}