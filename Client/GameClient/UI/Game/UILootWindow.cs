﻿using System.Collections.Generic;
using Arcanos.Client.GameClient.Items;
using Arcanos.Shared.Localization;
using Microsoft.Xna.Framework;

namespace Arcanos.Client.GameClient.UI.Game
{
    public interface IUILootItem
    {
        ulong SourceGUID { get; }
        byte ItemIndex { get; }
    }
    public class UILootWindow : UIWindow
    {
        public ulong SourceGUID { get; set; }
        public int Money { get; set; }

        public UIStackPanel ItemsControl { get; set; }

        public UILootWindow(Vector2 location, Vector2 size, ulong sourceGUID, int money, IEnumerable<ItemDefinition> items) : base(location, size, Localization.Game.LootWindowTitle)
        {
            SourceGUID = sourceGUID;
            Money = money;
            ContentOffset = CurrentGraphic.PixelPaddingMin;

            Controls.Add(ItemsControl = new UIStackPanel(Vector2.Zero, Size - CurrentGraphic.PixelPadding, 0, true)
            {
                Anchor = UIAnchor.All,
            });

            byte i = 0;
            if (Money != 0)
                ItemsControl.Container.Controls.Add(new UILootMoney(Vector2.Zero, new Vector2(ContentWidth, UIItemDefinitionSlot.DefaultSize.Y), SourceGUID, 0, Money));
            foreach (ItemDefinition item in items)
                if (item.Template != null)
                    ItemsControl.Container.Controls.Add(new UILootSlot(Vector2.Zero, new Vector2(ContentWidth, UIItemDefinitionSlot.DefaultSize.Y), SourceGUID, i++, item));
                else
                    ++i;

            OnParentChanged += (sender, args) =>
            {
                if (Desktop == null)
                    return;
                Desktop.RegisterEvent(UIEvents.LootClosed, LootClosedHandler);
                Desktop.RegisterEvent(UIEvents.LootItemRemoved, LootItemRemovedHandler);
            };
        }

        private void LootClosedHandler(UIDesktop desktop, object sender, params object[] args)
        {
            if (!Visible)
                return;

            Visible = false;
            if (Parent != null)
                Parent.Controls.Remove(this);
        }
        private void LootItemRemovedHandler(UIDesktop desktop, object sender, params object[] args)
        {
            if (!Visible)
                return;

            ulong sourceGUID = (ulong)args[0];
            byte itemIndex = (byte)args[1];

            if (sourceGUID != SourceGUID)
                return;

            for (int i = 0; i < ItemsControl.Container.Controls.Count; i++)
            {
                IUILootItem slot = (IUILootItem)ItemsControl.Container.Controls[i];
                if (slot.SourceGUID == sourceGUID && slot.ItemIndex == itemIndex)
                    ItemsControl.Container.Controls.RemoveAt(i--);
            }
        }
    }
}