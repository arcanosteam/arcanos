﻿using System.Collections.Generic;
using Arcanos.Client.GameClient.Data;
using Arcanos.Client.GameClient.Spells;
using Arcanos.Client.GameClient.World;
using Arcanos.Shared;
using Arcanos.Shared.Items;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Players;
using Microsoft.Xna.Framework;

namespace Arcanos.Client.GameClient.UI.Game
{
    public class UICharacterWindow : UIWindow
    {
        public static UIFont StatTextFont { get; set; }
        public static UIFont WeaponDamageFont { get; set; }
        public static UIFont WeaponIntervalFont { get; set; }
        public static UIFont SpellNameFont { get; set; }
        
        public List<UIControl> TabControls { get; set; }
        public UIEquipment EquipmentControl { get; set; }
        public UIStackPanel MiniStatsPanel { get; set; }
        public UIStackPanel StatsPanel { get; set; }
        public UIStackPanel SpellsPanel { get; set; }

        static UICharacterWindow()
        {
            UIEquipment.SlotLocations = new Dictionary<EquipSlot, Vector2>()
            {
                { EquipSlot.Back, new Vector2(10, 10) },
                { EquipSlot.Head, new Vector2(60, 20) },
                { EquipSlot.Shoulder, new Vector2(20, 60) },
                { EquipSlot.Chest, new Vector2(60, 60) },
                { EquipSlot.Neck, new Vector2(100, 60) },
                { EquipSlot.Wrists, new Vector2(20, 100) },
                { EquipSlot.Waist, new Vector2(60, 100) },
                { EquipSlot.Hands, new Vector2(100, 100) },
                { EquipSlot.Legs, new Vector2(60, 140) },
                { EquipSlot.Feet, new Vector2(60, 180) },
                { EquipSlot.MainHand, new Vector2(10, 150) },
                { EquipSlot.OffHand, new Vector2(110, 150) },
                { EquipSlot.Finger1, new Vector2(160, 25) },
                { EquipSlot.Finger2, new Vector2(160, 75) },
                { EquipSlot.Finger3, new Vector2(160, 125) },
                { EquipSlot.Finger4, new Vector2(160, 175) },
            };
        }

        public UICharacterWindow(Vector2 location, Vector2 size) : base(location, size)
        {
            TabControls = new List<UIControl>();
            {
                UIButton tabButton = new UIButton(new Vector2(0, 0), new Vector2(70, 30), UIButtonType.Medium, Localization.Game.CharacterWindowTabTitleEquipment);
                tabButton.OnClick += (sender, args) => SwitchTab(0);
                UIControl tab = new UIControl(new Vector2(0, 30), ContentSize - new Vector2(0, 30) )
                {
                    Anchor = UIAnchor.All,
                };

                tab.Controls.Add(EquipmentControl = new UIEquipment(new Vector2(0, 0), new Vector2(210, 230), null)
                {
                    Anchor = UIAnchor.Top | UIAnchor.Horizontal,
                    CurrentGraphic = UIButton.NormalTexture,
                    ContentOffset = Vector2.Zero,
                });
                EquipmentControl.Controls.Add(new UIText(new Vector2(0, 190), new Vector2(60, 15), WeaponDamageFont, UIFontAlign.Middle | UIFontAlign.Center,
                    new ValueFormatToken(Localization.Game.CharacterWindowWeaponDamageFormat, () =>
                    {
                        if (!WorldManager.IsCharacterInGame)
                            return null;
                        ItemBase item = WorldManager.Player.CurrentCharacter.Equipment.Items[(byte)EquipSlot.MainHand];
                        IntBounds damage = item == null ? new IntBounds() : item.Template.WeaponDamage;
                        return new object[] { damage.Min, damage.Max };
                    })));
                EquipmentControl.Controls.Add(new UIText(new Vector2(0, 205), new Vector2(60, 15), WeaponIntervalFont, UIFontAlign.Middle | UIFontAlign.Center,
                    new ValueFormatToken(Localization.Game.CharacterWindowWeaponInvervalFormat, () =>
                    {
                        if (!WorldManager.IsCharacterInGame)
                            return null;
                        ItemBase item = WorldManager.Player.CurrentCharacter.Equipment.Items[(byte)EquipSlot.MainHand];
                        FloatBounds damage = item == null ? new FloatBounds() : item.Template.WeaponSpeed;
                        return new object[] { damage.Min, damage.Max };
                    })));
                EquipmentControl.Controls.Add(new UIText(new Vector2(100, 190), new Vector2(60, 15), WeaponDamageFont, UIFontAlign.Middle | UIFontAlign.Center,
                    new ValueFormatToken(Localization.Game.CharacterWindowWeaponDamageFormat, () =>
                    {
                        if (!WorldManager.IsCharacterInGame)
                            return null;
                        ItemBase item = WorldManager.Player.CurrentCharacter.Equipment.Items[(byte)EquipSlot.OffHand];
                        IntBounds damage = item == null ? new IntBounds() : item.Template.WeaponDamage;
                        return new object[] { damage.Min, damage.Max };
                    })));
                EquipmentControl.Controls.Add(new UIText(new Vector2(100, 205), new Vector2(60, 15), WeaponIntervalFont, UIFontAlign.Middle | UIFontAlign.Center,
                    new ValueFormatToken(Localization.Game.CharacterWindowWeaponInvervalFormat, () =>
                    {
                        if (!WorldManager.IsCharacterInGame)
                            return null;
                        ItemBase item = WorldManager.Player.CurrentCharacter.Equipment.Items[(byte)EquipSlot.OffHand];
                        FloatBounds damage = item == null ? new FloatBounds() : item.Template.WeaponSpeed;
                        return new object[] { damage.Min, damage.Max };
                    })));
                tab.Controls.Add(MiniStatsPanel = new UIStackPanel(new Vector2(0, 230), new Vector2(210, 80), 0, true)
                {
                    Anchor = UIAnchor.All,
                    CurrentGraphic = UIStackPanel.PanelTexture,
                });
                MiniStatsPanel.Container.Location += MiniStatsPanel.CurrentGraphic.PixelPaddingMin;
                MiniStatsPanel.Container.Size -= MiniStatsPanel.CurrentGraphic.PixelPadding;
                MiniStatsPanel.Scrollbar.Top += MiniStatsPanel.CurrentGraphic.PixelPaddingMin.Y;
                MiniStatsPanel.Scrollbar.Left -= MiniStatsPanel.CurrentGraphic.PixelPaddingMax.X;
                MiniStatsPanel.Scrollbar.Height -= MiniStatsPanel.CurrentGraphic.PixelPaddingV;

                Controls.Add(tabButton);
                Controls.Add(tab);
                TabControls.Add(tab);
            }
            {
                UIButton tabButton = new UIButton(new Vector2(70, 0), new Vector2(70, 30), UIButtonType.Medium, Localization.Game.CharacterWindowTabTitleStats);
                tabButton.OnClick += (sender, args) => SwitchTab(1);
                UIControl tab = new UIControl(new Vector2(0, 30), ContentSize - new Vector2(0, 30))
                {
                    Anchor = UIAnchor.All,
                };

                tab.Controls.Add(StatsPanel = new UIStackPanel(Vector2.Zero, tab.Size, 0, true)
                {
                    Anchor = UIAnchor.All,
                    CurrentGraphic = UIStackPanel.PanelTexture,
                });
                StatsPanel.Container.Location += StatsPanel.CurrentGraphic.PixelPaddingMin;
                StatsPanel.Container.Size -= StatsPanel.CurrentGraphic.PixelPadding;
                StatsPanel.Scrollbar.Top += StatsPanel.CurrentGraphic.PixelPaddingMin.Y;
                StatsPanel.Scrollbar.Left -= StatsPanel.CurrentGraphic.PixelPaddingMax.X;
                StatsPanel.Scrollbar.Height -= StatsPanel.CurrentGraphic.PixelPaddingV;

                Controls.Add(tabButton);
                Controls.Add(tab);
                TabControls.Add(tab);
            }
            {
                UIButton tabButton = new UIButton(new Vector2(140, 0), new Vector2(70, 30), UIButtonType.Medium, Localization.Game.CharacterWindowTabTitleSpells);
                tabButton.OnClick += (sender, args) => SwitchTab(2);
                UIControl tab = new UIControl(new Vector2(0, 30), ContentSize - new Vector2(0, 30))
                {
                    Anchor = UIAnchor.All,
                };

                tab.Controls.Add(SpellsPanel = new UIStackPanel(Vector2.Zero, tab.Size, 0, true)
                {
                    Anchor = UIAnchor.All,
                    CurrentGraphic = UIStackPanel.PanelTexture,
                });
                SpellsPanel.Container.Location += SpellsPanel.CurrentGraphic.PixelPaddingMin;
                SpellsPanel.Container.Size -= SpellsPanel.CurrentGraphic.PixelPadding;
                SpellsPanel.Scrollbar.Top += SpellsPanel.CurrentGraphic.PixelPaddingMin.Y;
                SpellsPanel.Scrollbar.Left -= SpellsPanel.CurrentGraphic.PixelPaddingMax.X;
                SpellsPanel.Scrollbar.Height -= SpellsPanel.CurrentGraphic.PixelPaddingV;

                Controls.Add(tabButton);
                Controls.Add(tab);
                TabControls.Add(tab);
            }
            SwitchTab(0);

            OnParentChanged += (sender, args) =>
            {
                Desktop.RegisterEvent(UIEvents.PlayerLoaded, HandlePlayerLoaded);
                Desktop.RegisterEvent(UIEvents.StatsUpdated, HandleStatsUpdated);
                Desktop.RegisterEvent(UIEvents.SpellLearned, HandleSpellLearned);
                Desktop.RegisterEvent(UIEvents.SpellForgotten, HandleSpellForgotten);

                foreach (SpellTemplate spell in WorldManager.PlayerCreature.SpellBook.Spells)
                    HandleSpellLearned(null, null, spell.ID);
            };

            for (byte stat = 0; stat < (byte)Stat.Max; stat++)
            {
                if (stat == (byte)Stat.PhysicalResistance || stat == (byte)Stat.FireResistance || stat == (byte)Stat.Armor)
                {
                    UIControl separatorControl = new UIControl(Vector2.Zero, new Vector2(0, 8));
                    MiniStatsPanel.Container.Controls.Add(separatorControl);
                    StatsPanel.Container.Controls.Add(separatorControl);
                }

                byte currentStat = stat;
                for (int i = 0; i < 2; i++)
                {
                    UIText statControl = new UIText(Vector2.Zero, Vector2.Zero, StatTextFont,
                        new TextRefToken(Localization.Game.StatNames[stat]),
                        new ValueFormatToken(Localization.Game.CharacterWindowStatFormat, () =>
                        {
                            if (!WorldManager.IsCharacterInGame)
                                return null;
                            int value = WorldManager.Player.CurrentCharacter.Stats.ModifiedValues[currentStat];
                            int mod = WorldManager.Player.CurrentCharacter.Stats.Modifiers[currentStat];
                            return new object[] { value, mod };
                        }));
                    (i == 0 ? MiniStatsPanel : StatsPanel).Container.Controls.Add(statControl);
                }
            }
        }

        protected void SwitchTab(int tab)
        {
            for (int i = 0; i < TabControls.Count; i++)
                TabControls[i].Visible = i == tab;
        }

        private void HandlePlayerLoaded(UIDesktop desktop, object sender, params object[] args)
        {
            TitleControl.Text = WorldManager.PlayerCreature.Name;
        }
        private void HandleStatsUpdated(UIDesktop desktop, object sender, params object[] args)
        {
        }
        private void HandleSpellLearned(UIDesktop desktop, object sender, params object[] args)
        {
            int id = (int)args[0];

            DatabaseManager.Spells.Get(id, spell =>
            {
                UISpellSlot control = new UISpellSlot(Vector2.Zero, UISpell.DefaultSize, spell)
                {
                    CutOverflownChildren = false,
                };
                control.Controls.Add(new UIControl(new Vector2(control.Width + 4, 0), new Vector2(SpellsPanel.Container.Width - control.Width - 4, control.Height))
                {
                    Anchor = UIAnchor.Top | UIAnchor.Horizontal,
                    Font = SpellNameFont,
                    Text = spell.Name.ToString(),
                    TextRef = spell.Name,
                    MultilineText = true,
                });
                SpellsPanel.Container.Controls.Add(control);
            });
        }
        private void HandleSpellForgotten(UIDesktop desktop, object sender, params object[] args)
        {
            int id = (int)args[0];

            foreach (UISpellSlot control in SpellsPanel.Container.Controls)
            {
                if (control.SpellTemplate.ID == id)
                    SpellsPanel.Container.Controls.Remove(control);
                return;
            }
        }
    }
}