﻿using Arcanos.Client.GameClient.Data;
using Arcanos.Client.GameClient.Spells;
using Arcanos.Shared.Localization;
using Microsoft.Xna.Framework;

namespace Arcanos.Client.GameClient.UI.Game
{
    public class UIAura : UIControl
    {
        public static Vector2 IconSize { get; set; }
        public static UIFont DurationFont { get; set; }

        public override UIFont Font
        {
            get { return DurationFont; }
        }
        public override string Tooltip
        {
            get
            {
                if (base.Tooltip != null)
                    return base.Tooltip;

                bool missing = false;
                string tooltip = UITooltip.Build(Aura, ref missing);
                if (!missing)
                    base.Tooltip = tooltip;

                return tooltip;
            }
            set { base.Tooltip = value; }
        }

        public Aura Aura { get; set; }

        public UICooldown CooldownControl { get; set; }

        public UIAura(Aura aura) : base(Vector2.Zero, IconSize)
        {
            Aura = aura;
            CurrentGraphic = new UITexture(Aura.Spell.AuraIcon == 0 ? Aura.Spell.Icon : Aura.Spell.AuraIcon);
            Controls.Add(CooldownControl = new UICooldown(this, true) { Visible = Aura.DurationTotal > 0 });
            CutOverflownChildren = false;
            CutOverflownText = false;
            Update();
        }

        public override void Update()
        {
            base.Update();
            if (CooldownControl.Visible)
            {
                CooldownControl.CooldownDuration = Aura.DurationTotal;
                CooldownControl.CooldownLeft = Aura.DurationLeft;
                Text format;
                if (Aura.DurationLeft >= 60)
                    format = Localization.Game.AuraDurationMinutesFormat;
                else if (Aura.DurationLeft >= 10)
                    format = Localization.Game.AuraDurationTenSecsFormat;
                else
                    format = Localization.Game.AuraDurationSecondsFormat;
                Text = string.Format(format.ToString(), Aura.DurationLeft > 0 ? Aura.DurationLeft : 0, Aura.DurationTotal, 100 * Aura.DurationLeft / Aura.DurationTotal);
            }
        }
    }
}