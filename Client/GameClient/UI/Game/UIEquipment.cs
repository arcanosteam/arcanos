﻿using System.Collections.Generic;
using Arcanos.Client.GameClient.Items;
using Arcanos.Shared.Items;
using Microsoft.Xna.Framework;

namespace Arcanos.Client.GameClient.UI.Game
{
    public class UIEquipment : UIBag
    {
        public static Dictionary<EquipSlot, Vector2> SlotLocations { get; set; }

        private readonly Vector2 forcedSize;

        public UIEquipment(Vector2 location, Vector2 size, Bag bag) : base(location, bag)
        {
            Size = forcedSize = size;
        }

        protected override Vector2 GetSlotPosition(byte slot)
        {
            Vector2 pos;
            if (SlotLocations != null && SlotLocations.TryGetValue((EquipSlot)slot, out pos))
                return pos;
            return base.GetSlotPosition(slot);
        }
        protected override Vector2 GetSize()
        {
            return forcedSize;
        }
    }
}