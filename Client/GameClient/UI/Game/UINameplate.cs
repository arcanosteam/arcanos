﻿using System.Collections.Generic;
using Arcanos.Client.GameClient.Input;
using Arcanos.Client.GameClient.World;
using Arcanos.Shared;
using Arcanos.Shared.Objects;
using Microsoft.Xna.Framework;

namespace Arcanos.Client.GameClient.UI.Game
{
    public class UINameplate : UIControl
    {
        public static UIFont NoReactionFont { get; set; }
        public static UIFont CorpseFont { get; set; }
        public static Dictionary<Reaction, UIFont> ReactionFonts { get; set; }

        private Reaction reactionToTarget;

        public WorldObjectBase Target { get; private set; }

        public UIControl NameControl { get; set; }
        public UIGauge HealthGaugeControl { get; set; }

        public UINameplate(WorldObjectBase wo) : base(Vector2.Zero, new Vector2(128, 32))
        {
            UICreatureFrame.LoadStaticDataIfNeeded();
            Visible = false;
            Controls.Add(NameControl = new UIControl(new Vector2(0, 0), new Vector2(128, 16))
            {
                Anchor = UIAnchor.Top | UIAnchor.Left | UIAnchor.Right,
                Text = "",
                Font = NoReactionFont,
            });
            Controls.Add(HealthGaugeControl = new UIGauge(NameControl.BottomLeft, new Vector2(NameControl.Width, 12), Vector2.Zero, Vector2.Zero, null, -1)
            {
                Anchor = UIAnchor.Top | UIAnchor.Left | UIAnchor.Right,
                CutOverflownChildren = false,
            });
            HealthGaugeControl.GaugeControl.CurrentGraphic = UICreatureFrame.HealthTexture;
            Height = HealthGaugeControl.Bottom;
            SetTarget(wo);
            OnParentChanged += (sender, args) =>
            {
                if (Desktop == null)
                    return;
                Desktop.RegisterEvent(UIEvents.CreatureHealthChanged, UpdateTarget);
                Desktop.RegisterEvent(UIEvents.CreatureStateChanged, UpdateTarget);
                Desktop.RegisterEvent(UIEvents.CreatureEnteredCombat, UpdateTarget);
                Desktop.RegisterEvent(UIEvents.CreatureEvaded, UpdateTarget);
            };
            OnMouseLeftDown += (sender, args) => InputHandler.HandleMouseLeftState(true);
            OnMouseLeftUp += (sender, args) =>
            {
                InputHandler.HandleMouseLeftState(false);
                Desktop.MouseLocation = new Vector2(InputManager.MouseX, InputManager.MouseY);
            };
            OnMouseRightDown += (sender, args) => InputHandler.HandleMouseRightState(true);
            OnMouseRightUp += (sender, args) =>
            {
                InputHandler.HandleMouseRightState(false);
                Desktop.MouseLocation = new Vector2(InputManager.MouseX, InputManager.MouseY);
            };
            OnClick += (sender, args) => InputHandler.HandleLeftClickOn(Target);
            OnRightClick += (sender, args) => InputHandler.HandleRightClickOn(Target);
        }

        public override void Update()
        {
            base.Update();
            if (Target != null)
            {
                if (WorldManager.IsCharacterInGame)
                {
                    if (Target != WorldManager.PlayerCreature && (!WorldManager.PlayerCreature.CanSee(Target) || WorldManager.PlayerCreature.DistanceTo(Target) > 30))
                    {
                        Visible = false;
                        return;
                    }
                    Reaction newReaction;
                    if (Target.Type.IsCreature() && reactionToTarget != (newReaction = (Target as CreatureBase).GetReactionTo(WorldManager.PlayerCreature)))
                        NameControl.Font = ReactionFonts[reactionToTarget = newReaction];
                }
                if (!Visible)
                    Visible = true;
                Vector3 pos = Target.Position;
                pos.Z += Target.GetHeight();
                float depth;
                Vector3 loc = WorldManager.Camera.Project(pos, out depth);
                Visible = depth > 0 && depth < 1;
                if (Visible)
                    Location = new Vector2(loc.X - Width / 2, loc.Y - Height);
            }
        }

        public override bool OnMouseMove(Vector2 m)
        {
            bool handled = base.OnMouseMove(m);
            if (IsHovered)
                InputHandler.HandleMouseMove(m, m);
            return handled;
        }

        public void SetTarget(WorldObjectBase target)
        {
            Target = target;
            if (target == null)
            {
                Visible = false;
                return;
            }
            Visible = true;

            NameControl.Text = target.Name;
            NameControl.TextRef = target.NameRef;
            reactionToTarget = Reaction.Neutral;
            if (WorldManager.IsCharacterInGame)
                NameControl.Font = target.Type.IsCreature() ? (target as CreatureBase).IsAlive ? ReactionFonts[reactionToTarget = (target as CreatureBase).GetReactionTo(WorldManager.PlayerCreature)] : CorpseFont : NoReactionFont;
            else
                NameControl.Font = NoReactionFont;
            if (target.Type.IsCreature() && target is IGaugeSource)
            {
                HealthGaugeControl.Source = target as IGaugeSource;
                HealthGaugeControl.UpdateTarget();
                HealthGaugeControl.Visible = true;
                Height = HealthGaugeControl.Bottom;
            }
            else
            {
                HealthGaugeControl.Visible = false;
                Height = NameControl.Bottom;
            }
        }

        private void UpdateTarget(UIDesktop desktop, object sender, object[] args)
        {
            if (Target == sender)
                SetTarget(Target);
        }
    }
}