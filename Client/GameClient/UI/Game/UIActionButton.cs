﻿using System.Text;
using Arcanos.Client.GameClient.Data;
using Arcanos.Client.GameClient.Input;
using Arcanos.Client.GameClient.Items;
using Arcanos.Client.GameClient.Objects;
using Arcanos.Client.GameClient.Spells;
using Arcanos.Client.GameClient.World;
using Arcanos.Shared;
using Arcanos.Shared.Items;
using Arcanos.Shared.Objects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Arcanos.Client.GameClient.UI.Game
{
    public struct ActionButtonKey
    {
        public Keys Key;
        public bool Control;
        public bool Alt;
        public bool Shift;
    }
    public struct ActionButtonFunction
    {
        public ActionButtonType Type;
        public int ID;
    }
    public enum ActionButtonType
    {
        None,
        Spell,
        Item,
    }
    public class UIActionButton : UIControl
    {
        public static UITexture BackdropTexture { get; set; }
        public static UIFont KeyFont { get; set; }
        public static UIFont CountFont { get; set; }
        public static Vector2 DefaultSize { get; set; }
        public static Vector2 IconSize { get; set; }

        public override UITexture CurrentGraphic
        {
            get { return BackdropTexture; }
        }
        public override string Tooltip
        {
            get
            {
                if (base.Tooltip != null)
                    return base.Tooltip;
                if (Function.Type == ActionButtonType.None)
                    return null;

                bool missing = false;
                string tooltip = null;
                switch (Function.Type)
                {
                    case ActionButtonType.Spell:
                        tooltip = UITooltip.Build(DatabaseManager.Spells.Get(Function.ID), ref missing);
                        break;
                    case ActionButtonType.Item:
                        tooltip = UITooltip.Build(DatabaseManager.Items.Get(Function.ID), (Owner as PlayerCreature).Character.Inventory.GetItemCount(item), ref missing);
                        break;
                }
                if (!missing)
                    base.Tooltip = tooltip;

                return tooltip;
            }
            set { base.Tooltip = value; }
        }

        private CreatureBase owner;
        public CreatureBase Owner
        {
            get { return owner; }
            set
            {
                if (owner == value)
                    return;
                if (OwnerChanged != null)
                    OwnerChanged(this, new TypeEventArgs<CreatureBase>(owner, owner = value));
            }
        }
        private ActionButtonKey key;
        public ActionButtonKey Key
        {
            get { return key; }
            set
            {
                if (value.Equals(key))
                    return;
                if (KeyChanged != null)
                    KeyChanged(this, new TypeEventArgs<ActionButtonKey>(key, key = value));
            }
        }
        private ActionButtonFunction function;
        public ActionButtonFunction Function
        {
            get { return function; }
            set
            {
                if (value.Equals(function))
                    return;
                function = value;
                if (FunctionChanged != null)
                    FunctionChanged(this, new TypeEventArgs<ActionButtonFunction>(function, function = value));
            }
        }

        private SpellTemplate spell;
        private ItemTemplate item;

        public event TypeEventHandler<CreatureBase> OwnerChanged;
        public event TypeEventHandler<ActionButtonKey> KeyChanged;
        public event TypeEventHandler<ActionButtonFunction> FunctionChanged;

        public UIControl IconControl { get; set; }
        public UICooldown CooldownControl { get; set; }
        public UIControl KeyControl { get; set; }
        public UIControl CountControl { get; set; }

        public UIActionButton(Vector2 location, Vector2 size, CreatureBase owner) : base(location, size)
        {
            Controls.Add(IconControl = new UIControl((Size - IconSize) / 2, IconSize)
            {
                ClickThrough = true,
            });
            Controls.Add(CooldownControl = new UICooldown((Size - IconSize) / 2, IconSize)
            {
                Anchor = UIAnchor.None,
            });
            Controls.Add(KeyControl = new UIControl(Vector2.Zero, Size)
            {
                Anchor = UIAnchor.All,
                Font = KeyFont,
            });
            Controls.Add(CountControl = new UIControl(IconControl.Location, IconControl.Size)
            {
                Anchor = UIAnchor.All,
                Font = CountFont,
                ClickThrough = true,
            });
            OnParentChanged += (sender, args) =>
            {
                Desktop.RegisterEvent(UIEvents.SpellCooldownUpdated, SpellCooldownUpdatedHandler);
                Desktop.RegisterEvent(UIEvents.GlobalCooldownUpdated, SpellCooldownUpdatedHandler);
                Desktop.RegisterEvent(UIEvents.ItemUpdated, ItemUpdatedHandler);
                Desktop.RegisterEvent(UIEvents.ItemsAcquired, ItemsAcquiredHandler);
                Desktop.RegisterEvent(UIEvents.ItemsLost, ItemsLostHandler);
                Desktop.RegisterEvent(UIEvents.LevelUp, TooltipResetHandler);
            };
            OwnerChanged += (sender, args) => FunctionChanged(null, null);
            KeyChanged += (sender, args) =>
            {
                StringBuilder sb = new StringBuilder(10);
                if (key.Control)
                    sb.Append("C+");
                if (key.Alt)
                    sb.Append("A+");
                if (key.Shift)
                    sb.Append("S+");
                sb.Append(key.Key >= Keys.D0 && key.Key <= Keys.D9 ? key.Key.ToString().Substring(1) : key.Key.ToString());
                KeyControl.Text = sb.ToString();
            };
            FunctionChanged += (sender, args) =>
            {
                Tooltip = null;
                switch (function.Type)
                {
                    case ActionButtonType.None:
                        IconControl.CurrentGraphic = null;
                        CooldownControl.Visible = false;
                        break;
                    case ActionButtonType.Spell:
                        if (!DatabaseManager.Spells.Connected)
                            return;
                        DatabaseManager.Spells.Get(function.ID, spell =>
                        {
                            this.spell = spell;
                            IconControl.CurrentGraphic = new UITexture(spell.Icon);
                            if (Owner != null && Owner.SpellBook.KnowsSpell(spell))
                            {
                                float cooldownLeft;
                                float cooldownTotal;
                                Owner.SpellBook.IsOnCooldown(spell, out cooldownLeft, out cooldownTotal);
                                CooldownControl.CooldownLeft = cooldownLeft;
                                CooldownControl.CooldownDuration = cooldownTotal;
                                Enabled = CooldownControl.Visible = true;
                            }
                            else
                                Enabled = CooldownControl.Visible = false;
                            // TODO: ActiveIcon
                        });
                        break;
                    case ActionButtonType.Item:
                        if (!DatabaseManager.Items.Connected)
                            return;
                        DatabaseManager.Items.Get(function.ID, item =>
                        {
                            this.item = item;
                            IconControl.CurrentGraphic = new UITexture(item.Icon);
                            if (Owner != null && Owner.Type.IsPlayer())
                            {
                                int count = (Owner as PlayerCreature).Character.Inventory.GetItemCount(item);
                                if (count == 0)
                                {
                                    Enabled = false;
                                    return;
                                }
                                CountControl.Text = count.ToString();
                                if ((spell = item.SpellTriggers[(byte)ItemSpellUsage.OnUse] as SpellTemplate) != null)
                                {
                                    float cooldownLeft;
                                    float cooldownTotal;
                                    Owner.SpellBook.IsOnCooldown(spell, out cooldownLeft, out cooldownTotal);
                                    CooldownControl.CooldownLeft = cooldownLeft;
                                    CooldownControl.CooldownDuration = cooldownTotal;
                                    CooldownControl.Visible = true;
                                }
                                else
                                    CooldownControl.Visible = false;
                                Enabled = true;
                            }
                            else
                                Enabled = CooldownControl.Visible = false;
                                // TODO: ActiveIcon
                        });
                        break;
                }
            };
            OnMouseLeftDown += (sender, args) => Click();
            Owner = owner;
        }

        public void Click()
        {
            if (Desktop.DraggedItem != null)
            {
                Item draggedItem = Desktop.DraggedItem;
                Desktop.PutItem();
                Function = new ActionButtonFunction
                {
                    ID = draggedItem.Template.ID,
                    Type = ActionButtonType.Item,
                };
                return;
            }
            if (Desktop.DraggedSpell != null)
            {
                SpellTemplate draggedSpell = Desktop.DraggedSpell;
                Desktop.PutItem();
                Function = new ActionButtonFunction
                {
                    ID = draggedSpell.ID,
                    Type = ActionButtonType.Spell,
                };
                return;
            }
            switch (function.Type)
            {
                case ActionButtonType.Spell:
                    if (Owner.Type.IsPlayer())
                        InputHandler.CastSpell(Owner, spell);
                    break;
                case ActionButtonType.Item:
                    Item foundItem = WorldManager.PlayerCreature.Character.Inventory.FindItem(item);
                    if (foundItem != null)
                        InputHandler.ItemDoubleClick(foundItem);
                    break;
            }
        }

        public override bool OnKeyPress(Keys k)
        {
            if (key.Key == k && key.Control == InputManager.Control && key.Alt == InputManager.Alt && key.Shift == InputManager.Shift)
                Click();

            return base.OnKeyPress(k);
        }

        private void SpellCooldownUpdatedHandler(UIDesktop desktop, object sender, params object[] args)
        {
            float cooldownLeft = (float)args[0];
            float cooldownTotal = (float)args[1];
            int cooldownGroup = args.Length > 2 ? (int)args[2] : 0;
            switch (function.Type)
            {
                case ActionButtonType.Spell:
                    if (cooldownGroup != 0 && spell.CooldownGroup != cooldownGroup)
                        return;
                    CooldownControl.CooldownDuration = cooldownTotal;
                    CooldownControl.CooldownLeft = cooldownLeft;
                    CooldownControl.Visible = CooldownControl.CooldownLeft > 0;
                    break;
                case ActionButtonType.Item:
                    if (cooldownGroup != 0 && (item.SpellTriggers[(byte)ItemSpellUsage.OnUse] == null || item.SpellTriggers[(byte)ItemSpellUsage.OnUse].CooldownGroup != cooldownGroup))
                        return;
                    CooldownControl.CooldownDuration = cooldownTotal;
                    CooldownControl.CooldownLeft = cooldownLeft;
                    CooldownControl.Visible = CooldownControl.CooldownLeft > 0;
                    break;
            }
        }
        private void ItemUpdatedHandler(UIDesktop desktop, object sender, params object[] args)
        {
            ulong guid = (ulong)args[0];
            Item foundItem = ItemManager.Get(guid);
            if (item != null && foundItem.Template.ID == item.ID)
                CountControl.Text = (Owner as PlayerCreature).Character.Inventory.GetItemCount(item).ToString();
        }
        private void ItemsAcquiredHandler(UIDesktop desktop, object sender, params object[] args)
        {
            int itemID = (int)args[0];
            int itemCount = (int)args[1];
            if (item != null && itemID == item.ID)
            {
                int count = (Owner as PlayerCreature).Character.Inventory.GetItemCount(item);
                if (count < itemCount)
                    count = itemCount;
                Enabled = true;
                CountControl.Text = count.ToString();
            }
        }
        private void ItemsLostHandler(UIDesktop desktop, object sender, params object[] args)
        {
            int itemID = (int)args[0];
            if (item != null && itemID == item.ID)
            {
                int count = (Owner as PlayerCreature).Character.Inventory.GetItemCount(item);
                Enabled = count != 0;
                CountControl.Text = count.ToString();
            }
        }
        private void TooltipResetHandler(UIDesktop desktop, object sender, params object[] args)
        {
            Tooltip = null;
        }
    }
}