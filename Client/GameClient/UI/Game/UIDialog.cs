﻿using System.Collections.Generic;
using Arcanos.Client.GameClient.Data;
using Arcanos.Client.GameClient.World;
using Arcanos.Shared.Dialog;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Net;
using Arcanos.Shared.Objects;
using Microsoft.Xna.Framework;

namespace Arcanos.Client.GameClient.UI.Game
{
    public class UIDialog : UIWindow
    {
        public static UIFont TextFont { get; set; }
        public static UIFont OptionFont { get; set; }
        public static Dictionary<DialogQuestOptionState, UITexture> QuestOptionTextures { get; set; }

        protected WorldObjectBase Talker;
        protected int ExpectedOptionCount;
        protected int NonQuestOptionCount;
        private readonly Queue<KeyValuePair<UIButton, Text>> heightUpdateQueue = new Queue<KeyValuePair<UIButton, Text>>();
        private readonly Queue<KeyValuePair<UIButton, Text>> heightUpdateRequeue = new Queue<KeyValuePair<UIButton, Text>>();

        public UIStackPanel TextContainerControl { get; set; }

        public UIDialog(Vector2 location, Vector2 size) : base(location, size)
        {
            Visible = false;
            Controls.Add(TextContainerControl = new UIStackPanel(Vector2.Zero, ContentSize, 0, true)
            {
                Anchor = UIAnchor.All,
            });

            OnParentChanged += (sender, args) =>
            {
                Desktop.RegisterEvent(UIEvents.StartDialog, StartDialogHandler);
                Desktop.RegisterEvent(UIEvents.EndDialog, EndDialogHandler);
                Desktop.RegisterEvent(UIEvents.ChangePage, ChangePageHandler);
                Desktop.RegisterEvent(UIEvents.PageOptionReceived, PageOptionReceivedHandler);
                Desktop.RegisterEvent(UIEvents.PageQuestOptionReceived, PageQuestOptionReceivedHandler);
                Desktop.RegisterEvent(UIEvents.AcceptQuestPage, AcceptQuestPageHandler);
                Desktop.RegisterEvent(UIEvents.CompleteQuestPage, CompleteQuestPageHandler);
            };
        }

        public override void Update()
        {
            while (heightUpdateQueue.Count != 0)
            {
                KeyValuePair<UIButton, Text> pair = heightUpdateQueue.Dequeue();
                if (string.IsNullOrEmpty(pair.Value.ToString()))
                    heightUpdateRequeue.Enqueue(pair);
                else
                {
                    pair.Key.Height = OptionFont.MeasureString(pair.Value.ToString(), TextContainerControl.Container.Width).Y + UIButton.NormalTexture.PixelPaddingV;
                    if (pair.Key.IconControl != null)
                        pair.Key.IconControl.Top -= 4;
                }
            }
            while (heightUpdateRequeue.Count != 0)
                heightUpdateQueue.Enqueue(heightUpdateRequeue.Dequeue());
            base.Update();
        }

        public void StartDialog(WorldObjectBase talker)
        {
            Talker = talker;
            Text = Talker.Name;
            TextRef = Talker.Template.Name;
            Visible = false;
        }
        public void EndDialog(DialogEndReason reason)
        {
            Talker = null;
            Text = null;
            TextRef = null;
            Visible = false;
        }
        public void StartChangePage(Text pageText, int optionCount, int questOptionCount)
        {
            Visible = optionCount + questOptionCount == 0;
            NonQuestOptionCount = optionCount;
            ExpectedOptionCount = optionCount + questOptionCount;
            TextContainerControl.Container.Controls.Clear();

            float width = TextContainerControl.Container.Width;
            string text = pageText.ToString();
            Vector2 textSize = TextFont.MeasureString(text, width);

            UIControl textControl = new UIControl(Vector2.Zero, new Vector2(width, textSize.Y + 8))
            {
                Anchor = UIAnchor.Top | UIAnchor.Horizontal,
                Text = pageText.ToString(),
                TextRef = pageText,
                CutOverflownText = false,
                MultilineText = true,
                Font = TextFont,
            };
            textControl.OnTextChanged += (sender, args) =>
            {
                textControl.Height = TextFont.MeasureString(args.NewValue, TextContainerControl.Container.Width).Y + 8;
            };
            TextContainerControl.Container.Controls.Add(textControl);

            for (int i = 0; i < ExpectedOptionCount; i++)
            {
                UIButton button = new UIButton(Vector2.Zero, new Vector2(width, 0), UIButtonType.Medium, (string)null, null)
                {
                    Anchor = UIAnchor.Top | UIAnchor.Horizontal,
                    MultilineText = true,
                    Font = OptionFont,
                    AdditionalSpace = 3,
                };
                button.OnTextChanged += (sender, args) =>
                {
                    button.Height = OptionFont.MeasureString(args.NewValue, TextContainerControl.Container.Width).Y + UIButton.NormalTexture.PixelPaddingV;
                    if (button.IconControl != null)
                        button.IconControl.Top -= 4;
                };
                TextContainerControl.Container.Controls.Add(button);
            }
        }
        public void AddOption(byte index, Text optionText, UITexture icon, bool quest)
        {
            float width = TextContainerControl.Container.Width;
            string text = optionText.ToString();
            Vector2 textSize = OptionFont.MeasureString(text, width);

            UIButton button = (UIButton)TextContainerControl.Container.Controls[1 + (quest ? NonQuestOptionCount : 0) + index];
            if (string.IsNullOrEmpty(optionText.ToString()))
                heightUpdateQueue.Enqueue(new KeyValuePair<UIButton, Text>(button, optionText));
            button.Text = optionText.ToString();
            button.TextRef = optionText;
            button.Icon = icon;
            button.Height = OptionFont.MeasureString(optionText.ToString(), TextContainerControl.Container.Width).Y + UIButton.NormalTexture.PixelPaddingV;
            if (button.IconControl != null)
                button.IconControl.Top -= 4;
            button.OnClick += (sender, args) => WorldManager.PlayerSession.SendPacket(new CSelectOptionPacket { IsQuestOption = quest, Index = index });

            if (--ExpectedOptionCount == 0)
                Visible = true;
        }

        private void StartDialogHandler(UIDesktop desktop, object sender, params object[] args)
        {
            ulong guid = (ulong)args[0];

            WorldObjectBase talker = ObjectManager.GetWorldObject(guid);
            if (talker == null)
                return;

            StartDialog(talker);
        }
        private void EndDialogHandler(UIDesktop desktop, object sender, params object[] args)
        {
            ulong guid = (ulong)args[0];
            DialogEndReason reason = (DialogEndReason)((byte)args[1]);

            if (Talker != null && Talker.GUID == guid)
                EndDialog(reason);
        }
        private void ChangePageHandler(UIDesktop desktop, object sender, params object[] args)
        {
            int textID = (int)args[0];
            byte optionCount = (byte)args[1];
            byte questOptionCount = (byte)args[2];

            StartChangePage(DatabaseManager.Text.Get(textID), optionCount, questOptionCount);
        }
        private void PageOptionReceivedHandler(UIDesktop desktop, object sender, params object[] args)
        {
            byte index = (byte)args[0];
            int textID = (int)args[1];
            int iconID = (int)args[2];

            AddOption(index, DatabaseManager.Text.Get(textID), iconID == 0 ? null : new UITexture(iconID), false);
        }
        private void PageQuestOptionReceivedHandler(UIDesktop desktop, object sender, params object[] args)
        {
            byte index = (byte)args[0];
            int textID = (int)args[1];
            DialogQuestOptionState state = (DialogQuestOptionState)(byte)args[2];
            int questID = (int)args[3];

            AddOption(index, DatabaseManager.Text.Get(textID), QuestOptionTextures[state], true);
        }
        private void AcceptQuestPageHandler(UIDesktop desktop, object sender, params object[] args)
        {
            int questID = (int)args[0];
            int textID = (int)args[1];
            bool canAccept = (bool)args[2];

            StartChangePage(DatabaseManager.Text.Get(textID), canAccept ? 2 : 1, 0);
        }
        private void CompleteQuestPageHandler(UIDesktop desktop, object sender, params object[] args)
        {
            int questID = (int)args[0];
            int textID = (int)args[1];
            bool canComplete = (bool)args[2];

            StartChangePage(DatabaseManager.Text.Get(textID), canComplete ? 2 : 1, 0);
        }
    }
}