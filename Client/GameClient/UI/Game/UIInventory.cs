﻿using System.Collections.Generic;
using Arcanos.Client.GameClient.Items;
using Arcanos.Shared.Items;
using Microsoft.Xna.Framework;

namespace Arcanos.Client.GameClient.UI.Game
{
    public class UIInventory : UIControl
    {
        public static UITexture BackgroundTexture { get; set; }

        public override UITexture CurrentGraphic
        {
            get { return BackgroundTexture; }
        }
        
        private Inventory inventory;
        public Inventory Inventory
        {
            get { return inventory; }
            set
            {
                if (inventory == value)
                    return;
                if (InventoryChanged != null)
                    InventoryChanged(this, new TypeEventArgs<Inventory>(inventory, inventory = value));
            }
        }

        public List<UIBag> Bags { get; set; }
        public UIControl BagsControl { get; set; }

        public event TypeEventHandler<Inventory> InventoryChanged; 

        public UIInventory(Vector2 location, Vector2 size, Inventory inventory) : base(location, size)
        {
            ClickThrough = true;
            Bags = new List<UIBag>(InventoryBase.BAG_LIMIT);
            Controls.Add(BagsControl = new UIControl(Vector2.Zero, size)
            {
                Anchor = UIAnchor.All,
                ClickThrough = true,
            });
            InventoryChanged += (sender, args) =>
            {
                Bags.Clear();
                BagsControl.Controls.Clear();
                if (Inventory == null)
                    return;
                for (int i = 0; i < InventoryBase.BAG_LIMIT; i++)
                {
                    UIBag bagControl = new UIBag(Vector2.Zero, null);

                    BagsControl.Controls.Add(bagControl);
                    Bags.Add(bagControl);

                    bagControl.Bag = Inventory.Bags[i] as Bag;
                }
                UpdateSize();
            };
            Inventory = inventory;
            OnParentChanged += (sender, args) => Desktop.RegisterEvent(UIEvents.BagChanged, BagChangedHandler);
        }

        private void BagChangedHandler(UIDesktop desktop, object sender, params object[] args)
        {
            byte bagIndex = (byte)args[0];
            Bag bag = args[1] as Bag;

            if (Bags.Count > bagIndex)
            {
                BagsControl.Controls.RemoveAt(bagIndex);
                Bags.RemoveAt(bagIndex);
            }
            UIBag bagControl = new UIBag(Vector2.Zero, null);

            BagsControl.Controls.Insert(bagIndex, bagControl);
            Bags.Insert(bagIndex, bagControl);

            bagControl.Bag = bag;

            UpdateSize();
        }

        protected void UpdateSize()
        {
            Vector2 oldSize = Size;
            Vector2 newSize = Vector2.Zero;

            foreach (UIBag control in Bags)
            {
                newSize.X += control.Width;
                if (control.Height > newSize.Y)
                    newSize.Y = control.Height;
            }

            Size = newSize;
            Vector2 d = newSize - oldSize;

            Vector2 pos = Vector2.Zero;
            foreach (UIBag control in Bags)
            {
                control.Location = (Anchor & UIAnchor.Bottom) != 0 ? new Vector2(pos.X, pos.Y + Height - control.Height) : pos;
                pos.X += control.Width;
            }

            if ((Anchor & UIAnchor.Right) != 0)
                Left -= d.X;
            if ((Anchor & UIAnchor.Bottom) != 0)
                Top -= d.Y;
        }
    }
}