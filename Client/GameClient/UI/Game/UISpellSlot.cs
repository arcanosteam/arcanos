﻿using Arcanos.Client.GameClient.Input;
using Arcanos.Client.GameClient.Spells;
using Arcanos.Client.GameClient.World;
using Arcanos.Shared.Timing;
using Microsoft.Xna.Framework;

namespace Arcanos.Client.GameClient.UI.Game
{
    public class UISpellSlot : UISpell
    {
        public static UITexture BackgroundTexture { get; set; }

        public override UITexture CurrentGraphic
        {
            get { return BackgroundTexture; }
        }

        private long lastClickTimestamp;

        public UISpellSlot(Vector2 location, Vector2 size, SpellTemplate spell) : base(location, size, spell)
        {
            OnParentChanged += (sender, args) =>
            {
                Desktop.RegisterEvent(UIEvents.SpellCooldownUpdated, SpellCooldownUpdatedHandler);
                Desktop.RegisterEvent(UIEvents.GlobalCooldownUpdated, SpellCooldownUpdatedHandler);
            };
            OnClick += (sender, args) =>
            {
                if (Desktop.DraggedItem == null && Desktop.DraggedSpell == null)
                {
                    Desktop.PickSpell(SpellTemplate);
                }
                else if (Desktop.DraggedSpell != null)
                {
                    SpellTemplate draggedSpell = Desktop.DraggedSpell;
                    Desktop.PutItem();
                    if (draggedSpell == SpellTemplate && Time.Timestamp - lastClickTimestamp < 250)
                    {
                        InputHandler.CastSpell(WorldManager.PlayerCreature, SpellTemplate);
                        lastClickTimestamp = 0;
                    }
                }
                lastClickTimestamp = Time.Timestamp;
            };
        }
        private void SpellCooldownUpdatedHandler(UIDesktop desktop, object sender, params object[] args)
        {
            if (SpellTemplate == null)
                return;

            float cooldownLeft = (float)args[0];
            float cooldownTotal = (float)args[1];
            int cooldownGroup = args.Length > 2 ? (int)args[2] : 0;

            if (cooldownGroup != 0 && SpellTemplate.CooldownGroup != cooldownGroup)
                return;
            CooldownControl.CooldownDuration = cooldownTotal;
            CooldownControl.CooldownLeft = cooldownLeft;
            CooldownControl.Visible = CooldownControl.CooldownLeft > 0;
        }
    }
}