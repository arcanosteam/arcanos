﻿using System.Collections.Generic;
using Arcanos.Client.GameClient.Chat;
using Arcanos.Client.GameClient.Input;
using Arcanos.Client.GameClient.Objects;
using Arcanos.Client.GameClient.Players;
using Arcanos.Client.GameClient.Spells;
using Arcanos.Client.GameClient.World;
using Arcanos.Shared;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Objects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Arcanos.Client.GameClient.UI.Game
{
    public class UICreatureFrame : UIControl
    {
        public static UITexture CreatureFramePortrait { get; set; }
        public static UITexture CreatureFramePlate { get; set; }
        public static UITexture GaugeBackdrop { get; set; }
        public static UITexture HealthTexture { get; set; }
        public static UITexture[] PowerTypeTextures { get; set; }
        public static UITexture CastingTexture { get; set; }
        public static UIFont NameFont { get; set; }

        public override UITexture CurrentGraphic
        {
            get { return CreatureFramePortrait; }
        }

        public CreatureBase Target { get; set; }

        public UIControl PlateControl { get; set; }
        public UIControl NameControl { get; set; }
        public UIGauge HealthGaugeControl { get; set; }
        public UIGauge[] PowerGaugeControls { get; set; }
        public UIGauge CastingGaugeControl { get; set; }
        public UIAuraBar AuraBarControl { get; set; }

        static UICreatureFrame()
        {
            PowerTypeTextures = new UITexture[(byte)PowerType.Max];
        }

        public UICreatureFrame(Vector2 location) : base(location, new Vector2(204, 72))
        {
            CutOverflownChildren = false;
            IgnoreBounds = true;
            LoadStaticDataIfNeeded();
            PowerGaugeControls = new UIGauge[(byte)PowerType.Max];
            Controls.Add(PlateControl = new UIControl(Vector2.Zero, Size)
            {
                CurrentGraphic = CreatureFramePlate,
                ContentOffset = new Vector2(73, 4),
                CutOverflownChildren = false,
                IgnoreBounds = true,
            });
            PlateControl.Controls.Add(NameControl = new UIControl(new Vector2(0, 0), new Vector2(126, 18))
            {
                CurrentGraphic = UIStackPanel.PanelTexture,
                Anchor = UIAnchor.Top | UIAnchor.Horizontal,
                Text = "",
                Font = NameFont,
            });
            PlateControl.Controls.Add(HealthGaugeControl = new UIGauge(NameControl.BottomLeft, new Vector2(126, 18), Vector2.One, Vector2.One, null, -1)
            {
                CurrentGraphic = GaugeBackdrop,
                Anchor = UIAnchor.Top | UIAnchor.Horizontal,
                CutOverflownChildren = false,
                TextFormatRef = Localization.Game.HealthFormat,
                ExtraFormatArg4 = "Health"
            });
            HealthGaugeControl.GaugeControl.CurrentGraphic = HealthTexture;
            for (byte type = 0; type < (byte)PowerType.Max; type++)
            {
                PlateControl.Controls.Add(PowerGaugeControls[type] = new UIGauge((type > 0 ? PowerGaugeControls[type - 1] : HealthGaugeControl).BottomLeft, new Vector2(126, 12), Vector2.One, Vector2.One, null, type)
                {
                    CurrentGraphic = GaugeBackdrop,
                    Anchor = UIAnchor.Top | UIAnchor.Horizontal,
                    CutOverflownChildren = false,
                    TextFormatRef = Localization.Game.PowerFormat,
                    ExtraFormatArg4 = Localization.Game.PowerTypeNames[type],
                });
                PowerGaugeControls[type].GaugeControl.CurrentGraphic = PowerTypeTextures[type];
            }
            PlateControl.Controls.Add(CastingGaugeControl = new UIGauge(PowerGaugeControls[(byte)PowerType.Max - 1].BottomLeft + new Vector2(0, 8), new Vector2(126, 12), Vector2.Zero, Vector2.Zero, null, -2)
            {
                Anchor = UIAnchor.Bottom | UIAnchor.Horizontal,
                CutOverflownChildren = false,
                AutoUpdate = true,
                InterpolateTime = 0,
                TextFormatRef = Localization.Game.CastingFormat,
            });
            AuraBarControl = new UIAuraBar(CastingGaugeControl.TopLeft + new Vector2(-4, -4), new Vector2(132, UIAura.IconSize.Y + UIAura.DurationFont.Offset.Y + 3), 0)
            {
                Anchor = UIAnchor.Bottom | UIAnchor.Left,
            };
            CastingGaugeControl.GaugeControl.CurrentGraphic = CastingTexture;
            SetTarget(null);
            OnParentChanged += (sender, args) =>
            {
                PlateControl.Controls.Add(AuraBarControl);
                Desktop.RegisterEvent(UIEvents.CreatureHealthChanged, CreatureHealthChangedHandler);
                Desktop.RegisterEvent(UIEvents.CreatureUsesPowerChanged, UpdateTarget);
                Desktop.RegisterEvent(UIEvents.CreaturePowerChanged, CreaturePowerChangedHandler);
                Desktop.RegisterEvent(UIEvents.CreatureSpellCasted, UpdateTarget);
                Desktop.RegisterEvent(UIEvents.CreatureSpellCastingStarted, UpdateTarget);
                Desktop.RegisterEvent(UIEvents.CreatureSpellCastingFailed, UpdateTarget);
                Desktop.RegisterEvent(UIEvents.AuraApplied, UpdateTarget);
                Desktop.RegisterEvent(UIEvents.AuraRemoved, UpdateTarget);
            };
            OnClick += (sender, args) =>
            {
                if (Target != null)
                    InputHandler.HandleLeftClickOn(Target);
            };
            OnRightClick += (sender, args) =>
            {
                List<UIDropdownItemData> items = new List<UIDropdownItemData>();
                if (Target == null)
                    return;
                Player player = WorldManager.Player;
                Character playerCharacter = WorldManager.Player.CurrentCharacter;
                PlayerCreature playerCreature = WorldManager.PlayerCreature;
                Group playerGroup = playerCharacter.Group;
                bool playerIsGroupLeader = playerGroup != null && playerGroup.Leader == playerCharacter;
                if (Target == WorldManager.PlayerCreature)
                {
                    // Self
                    if (playerGroup != null)
                        items.Add(new UIDropdownItemData { Text = "Leave party", OnClick = (o, eventArgs) => InputHandler.LeaveParty() });
                }
                else
                {
                    if (Target.Type.IsPlayer())
                    {
                        // Other player
                        PlayerCreature pcTarget = Target as PlayerCreature;
                        items.Add(new UIDropdownItemData { Text = "Whisper", OnClick = (o, eventArgs) =>
                        {
                            ChatManager.SetActiveWhisper(Target.Name);
                            UIManager.CurrentDesktop.GetByKey("ChatInputBox").Focus();
                        } });
                        if (pcTarget.Character.GroupID == 0)
                        {
                            // Without a group
                            if (playerGroup == null || playerIsGroupLeader)
                                items.Add(new UIDropdownItemData { Text = "Invite", OnClick = (o, eventArgs) => InputHandler.InviteToParty(Target as PlayerCreature) });
                        }
                        else
                        {
                            // In a group
                            if (playerGroup != null && pcTarget.Character.GroupID == playerCharacter.GroupID)
                            {
                                // In player's group
                                if (playerIsGroupLeader)
                                    items.Add(new UIDropdownItemData { Text = "Kick", OnClick = (o, eventArgs) => InputHandler.KickFromParty(Target as PlayerCreature) });
                            }
                        }
                        items.Add(new UIDropdownItemData { Text = "Duel", OnClick = (o, eventArgs) => InputHandler.RequestDuel(Target as PlayerCreature) });
                    }
                    else
                    {
                        // Creature
                        Creature creatureTarget = Target as Creature;
                    }
                    if (Target.HasFlag(ObjectFlags.Lootable))
                        items.Add(new UIDropdownItemData { Text = "Loot", OnClick = (o, eventArgs) => InputHandler.LootCreature(Target) });
                }
                if (items.Count != 0)
                    Desktop.OpenDropdown(this, items.ToArray());
            };
        }

        public void SetTarget(CreatureBase target)
        {
            Target = target;
            if (target == null)
            {
                Visible = false;
                return;
            }
            Visible = true;

            NameControl.Text = target.Name;
            HealthGaugeControl.SetSource(target as IGaugeSource);
            float bottom = HealthGaugeControl.Bottom;
            for (byte type = 0; type < (byte)PowerType.Max; type++)
            {
                UIGauge gauge = PowerGaugeControls[type];
                if (target.UsesPowerType[type])
                {
                    gauge.Visible = true;
                    gauge.SetSource(target as IGaugeSource);
                    gauge.Top = bottom;
                    bottom = gauge.Bottom;
                }
                else
                {
                    gauge.Visible = false;
                    gauge.Source = null;
                }
            }
            PlateControl.Height = bottom + 10;

            CastingGaugeControl.SetSource(target as IGaugeSource);
            if (target.SpellBook.CastingSpell != null)
            {
                CastingGaugeControl.Top = ((target.SpellBook as SpellBook).VisibleAuras.Count > 0 ? AuraBarControl.Bottom : AuraBarControl.Top) + 4;
                CastingGaugeControl.ExtraFormatArg4 = target.SpellBook.CastingSpell.Name;
                CastingGaugeControl.ExtraFormatArg5 = target.SpellBook.CastingTarget != null ? target.SpellBook.CastingTarget.Name : "Ground";
                CastingGaugeControl.UpdateTarget();
                CastingGaugeControl.Visible = true;
            }
            else
                CastingGaugeControl.Visible = false;

            AuraBarControl.SetTarget(target);
        }

        private void UpdateTarget(UIDesktop desktop, object sender, object[] args)
        {
            if (Target == sender)
                SetTarget(Target);
        }
        private void CreatureHealthChangedHandler(UIDesktop desktop, object sender, params object[] args)
        {
            if (Target == sender)
                HealthGaugeControl.UpdateTarget();
        }
        private void CreaturePowerChangedHandler(UIDesktop desktop, object sender, params object[] args)
        {
            if (Target == sender && Target.MaxPower[(byte)(PowerType)args[0]] > 0)
                PowerGaugeControls[(byte)(PowerType)args[0]].UpdateTarget();
        }

        public static void LoadStaticDataIfNeeded()
        {
            if (CreatureFramePortrait == null)
                CreatureFramePortrait = new UITexture(UIManager.LoadTexture("Textures\\creatureframetemplate_portrait.png"), 0, 0, 204, 72, 0, 0, 0, 0);
            if (CreatureFramePlate == null)
                CreatureFramePlate = new UITexture(UIManager.LoadTexture("Textures\\creatureframetemplate_plate.png"), 0, 0, 204, 72, 4, 73, 5, 4);
            if (GaugeBackdrop == null)
                GaugeBackdrop = new UITexture(UIManager.LoadTexture("Textures\\gaugetemplate_backdrop_noshadow.png"), 0, 0, 98, 18, 6, 6, 6, 6);
            if (HealthTexture == null)
                HealthTexture = new UITexture(UIManager.LoadTexture("Textures\\gaugetemplate_bar.png"), 0, 0, 96, 16, 6, 4, 4, 3);
            if (PowerTypeTextures[(byte)PowerType.Mana] == null)
                PowerTypeTextures[(byte)PowerType.Mana] = new UITexture(UIManager.LoadTexture("Textures\\gaugetemplate_bar.png"), 0, 32, 96, 16, 6, 4, 4, 3);
            if (PowerTypeTextures[(byte)PowerType.Energy] == null)
                PowerTypeTextures[(byte)PowerType.Energy] = new UITexture(UIManager.LoadTexture("Textures\\gaugetemplate_bar.png"), 0, 48, 96, 16, 6, 4, 4, 3);
            if (CastingTexture == null)
                CastingTexture = new UITexture(UIManager.LoadTexture("Textures\\gaugetemplate_bar.png"), 0, 32, 96, 16, 6, 4, 4, 3);
            if (NameFont.Font == null)
                NameFont = new UIFont(UIManager.LoadFont("Fonts\\LogFont"), 8, 0, Color.White).MakeShadow(1, 0.5f);
        }
    }
}