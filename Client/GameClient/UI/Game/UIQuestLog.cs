﻿using System;
using System.Collections.Generic;
using Arcanos.Client.GameClient.Data;
using Arcanos.Client.GameClient.Quests;
using Arcanos.Client.GameClient.World;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Net;
using Arcanos.Shared.Quests;
using Microsoft.Xna.Framework;

namespace Arcanos.Client.GameClient.UI.Game
{
    public class UIQuestLog : UIControl
    {
        public UIQuestLog(Vector2 location, Vector2 size) : base(location, size)
        {
            ClickThrough = true;
            OnParentChanged += (sender, args) =>
            {
                Desktop.RegisterEvent(UIEvents.QuestAdded, QuestAddedHandler);
                Desktop.RegisterEvent(UIEvents.QuestRemoved, QuestRemovedHandler);
                Desktop.RegisterEvent(UIEvents.QuestCompleted, QuestCompletedHandler);
                Desktop.RegisterEvent(UIEvents.QuestObjectiveUpdated, QuestObjectiveUpdatedHandler);
            };
        }

        private void QuestAddedHandler(UIDesktop desktop, object sender, params object[] args)
        {
            int questID = (int)args[0];
            bool completed = (bool)args[1];

            DatabaseManager.Quests.Get(questID, quest =>
            {
                UIQuest control = new UIQuest(new Vector2(0, Controls.Count == 0 ? 0 : Controls[Controls.Count - 1].Bottom), Width, quest);
                Controls.Add(control);
                if (completed)
                    control.Complete();
            });
        }
        private void QuestRemovedHandler(UIDesktop desktop, object sender, object[] args)
        {
            int questID = (int)args[0];

            DatabaseManager.Quests.Get(questID, quest => Controls.Remove(GetControlForQuest(questID)));
        }
        private void QuestCompletedHandler(UIDesktop desktop, object sender, object[] args)
        {
            int questID = (int)args[0];

            DatabaseManager.Quests.Get(questID, quest => GetControlForQuest(questID).Complete());
        }
        private void QuestObjectiveUpdatedHandler(UIDesktop desktop, object sender, object[] args)
        {
            int questID = (int)args[0];
            byte index = (byte)args[1];
            int progress = (int)args[2];
            bool completed = (bool)args[3];

            DatabaseManager.Quests.Get(questID, quest => GetControlForQuest(questID).UpdateObjective(index, progress, completed));
        }

        public UIQuest GetControlForQuest(int questID)
        {
            foreach (UIQuest control in Controls)
                if (control.Quest.ID == questID)
                    return control;

            return null;
        }
    }
    public class UIQuest : UIControl
    {
        public static UIFont TitleFont { get; set; }
        public static UIFont ObjectiveActiveFont { get; set; }
        public static UIFont ObjectiveCompletedFont { get; set; }
        public static string ObjectiveStatusFormatActive { get; set; }
        public static string ObjectiveStatusFormatActiveBoolean { get; set; }
        public static string ObjectiveStatusFormatComplete { get; set; }

        public Quest Quest { get; set; }

        public UIControl TitleControl { get; set; }
        public List<UIControl> ObjectiveControls { get; set; }
        public List<UIControl> ObjectiveStatusControls { get; set; }

        public UIQuest(Vector2 location, float width, Quest quest) : base(location, new Vector2(width, 0))
        {
            ClickThrough = true;

            Quest = quest;
            ObjectiveControls = new List<UIControl>();
            ObjectiveStatusControls = new List<UIControl>();

            Vector2 pos = Vector2.Zero;
            string titleText = Quest.Title.ToString();
            float lineHeight = TitleFont.Font.MeasureString(" ").Y;
            Controls.Add(TitleControl = new UIControl(pos, new Vector2(Width, lineHeight))
            {
                Text = titleText,
                TextRef = Quest.Title,
                Font = TitleFont,
            });
            TitleControl.OnRightClick += (sender, args) =>
            {
                List<UIDropdownItemData> items = new List<UIDropdownItemData>
                {
                    new UIDropdownItemData
                    {
                        Text = "Abandon",
                        OnClick = (o, eventArgs) => WorldManager.PlayerSession.SendPacket(new CAbandonQuestPacket { ID = Quest.ID }),
                    }
                };
                Desktop.OpenDropdown(TitleControl, items.ToArray());
            };
            pos.Y += lineHeight;

            for (int i = 0; i < quest.Objectives.Count; i++)
            {
                QuestObjective objective = quest.Objectives[i];
                lineHeight = ObjectiveActiveFont.Font.MeasureString(" ").Y;
                UIControl objectiveControl = new UIControl(pos, new Vector2(Width, lineHeight))
                {
                    Font = ObjectiveActiveFont,
                    ClickThrough = true,
                };
                UIControl objectiveStatusControl = new UIControl(pos, new Vector2(Width, lineHeight))
                {
                    Text = FormatObjectiveStatus(0, Quest.Objectives[i].Required, false),
                    Font = ObjectiveActiveFont,
                    ClickThrough = true,
                };
                objectiveControl.OnTextChanged += (sender, args) =>
                {
                    float lineWidth = objectiveControl.Font.MeasureString(objectiveControl.Text).X;
                    objectiveStatusControl.Left = lineWidth;
                    objectiveStatusControl.Width = Width - lineWidth;
                };
                GetObjectiveText(objective, text =>
                {
                    objectiveControl.Text = text.ToString();
                    objectiveControl.TextRef = text;
                });
                ObjectiveControls.Add(objectiveControl);
                ObjectiveStatusControls.Add(objectiveStatusControl);
                Controls.Add(objectiveControl);
                Controls.Add(objectiveStatusControl);
                pos.Y += lineHeight;
            }

            Height = pos.Y;
        }

        public void UpdateObjective(byte index, int progress, bool completed)
        {
            UIControl objectiveControl = ObjectiveControls[index];
            objectiveControl.Font = completed ? ObjectiveCompletedFont : ObjectiveActiveFont;

            UIControl objectiveStatusControl = ObjectiveStatusControls[index];
            objectiveStatusControl.Text = FormatObjectiveStatus(progress, Quest.Objectives[index].Required, completed);
            objectiveStatusControl.Font = completed ? ObjectiveCompletedFont : ObjectiveActiveFont;
        }
        public void Complete()
        {
        }

        public static void GetObjectiveText(QuestObjective objective, Action<Text> loadedHandler)
        {
            if (objective.OverrideText != null)
            {
                loadedHandler(objective.OverrideText);
                return;
            }

            switch (objective.Type)
            {
                case QuestObjectiveType.Kill:
                    DatabaseManager.Creatures.Get(objective.ID, asset => loadedHandler(asset.Name));
                    break;
                case QuestObjectiveType.Use:
                    DatabaseManager.WorldObjects.Get(objective.ID, asset => loadedHandler(asset.Name));
                    break;
                case QuestObjectiveType.Item:
                    DatabaseManager.Items.Get(objective.ID, asset => loadedHandler(asset.Name));
                    break;
                case QuestObjectiveType.Cast:
                    DatabaseManager.Spells.Get(objective.ID, asset => loadedHandler(asset.Name));
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        public static string FormatObjectiveStatus(int progress, int total, bool completed)
        {
            return string.Format(completed ? ObjectiveStatusFormatComplete : (total <= 1 ? ObjectiveStatusFormatActiveBoolean : ObjectiveStatusFormatActive), progress, total);
        }
    }
}