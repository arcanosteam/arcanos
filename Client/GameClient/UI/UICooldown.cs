﻿using System;
using Microsoft.Xna.Framework;

namespace Arcanos.Client.GameClient.UI
{
    public class UICooldown : UIControl
    {
        public static UITexture[] CooldownTextures { get; set; }
        public static UITexture[] DurationTextures { get; set; }

        public float CooldownDuration { get; set; }
        public float CooldownLeft { get; set; }
        public bool ShowDuration { get; set; }

        public override UITexture CurrentGraphic
        {
            get
            {
                if (CooldownDuration == 0) return null;
                if (CooldownTextures == null) return null;
                int frames = CooldownTextures.Length - 1;
                float progress = 1 - CooldownLeft / CooldownDuration;
                if (progress < 0) progress = 0;
                if (progress > 1) progress = 1;
                return ShowDuration && DurationTextures != null
                    ? DurationTextures[(int)Math.Round(frames * progress)]
                    : CooldownTextures[(int)Math.Round(frames * progress)];
            }
        }

        public UICooldown(Vector2 location, Vector2 size, bool showDuration) : base(location, size)
        {
            ShowDuration = showDuration;
            ClickThrough = true;
        }
        public UICooldown(Vector2 location, Vector2 size) : this(location, size, false) { }
        public UICooldown(UIControl container, bool showDuration) : this(Vector2.Zero, container.Size, showDuration)
        {
            Anchor = UIAnchor.All;
        }
        public UICooldown(UIControl container) : this(container, false) { }
    }
}