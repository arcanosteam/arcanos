﻿using Arcanos.Client.GameClient.Input;
using Arcanos.Shared.Localization;
using Microsoft.Xna.Framework;

namespace Arcanos.Client.GameClient.UI
{
    public class UIWindow : UIControl
    {
        public static UITexture WindowTexture { get; set; }
        public static UIFont TitleFont { get; set; }
        public static UIFont ContentFont { get; set; }

        public override UITexture CurrentGraphic
        {
            get { return WindowTexture; }
        }
        public override string Text
        {
            get { return TitleControl.Text; }
            set { TitleControl.Text = value; }
        }

        public bool Movable { get; set; }
        public Vector2 ContentSize
        {
            get { return Size - CurrentGraphic.PixelPaddingMin - CurrentGraphic.PixelPaddingMax; }
            set { Size = value + CurrentGraphic.PixelPaddingMin + CurrentGraphic.PixelPaddingMax; }
        }
        public float ContentWidth
        {
            get { return ContentSize.X; }
            set { ContentSize = new Vector2(value, ContentSize.Y); }
        }
        public float ContentHeight
        {
            get { return ContentSize.Y; }
            set { ContentSize = new Vector2(ContentSize.X, value); }
        }

        public UIControl TitleControl { get; set; }
        public UIButton CloseButtonControl { get; set; }

        private bool dragging;
        private Vector2 dragStartLoc;

        public UIWindow(Vector2 location, Vector2 size) : this(location, size, "") { }
        public UIWindow(Vector2 location, Vector2 size, string title) : base(location, Vector2.Zero)
        {
            ContentSize = size;
            ContentOffset = CurrentGraphic.PixelPaddingMin;
            CutOverflownChildren = false;
            Controls.Add(TitleControl = new UIControl(new Vector2(4, 4) - ContentOffset, new Vector2(Width - 8, 16))
            {
                Anchor = UIAnchor.Top | UIAnchor.Horizontal,
                Font = TitleFont,
            });
            TitleControl.OnMouseLeftDown += (sender, args) =>
            {
                if (!Movable)
                    return;
                dragging = true;
                dragStartLoc = new Vector2(InputManager.MouseX, InputManager.MouseY);
            };
            TitleControl.OnMouseLeftUp += (sender, args) =>
            {
                dragging = false;
            };
            Text = title;
            Movable = true;
        }
        protected UIWindow(Vector2 location, Vector2 size, Text title) : this(location, size, title.ToString())
        {
            TitleControl.TextRef = title;
        }

        public override void Update()
        {
            base.Update();
            if (dragging)
            {
                Vector2 dragLoc = new Vector2(InputManager.MouseX, InputManager.MouseY);
                Vector2 offset = dragLoc - dragStartLoc;
                dragStartLoc = dragLoc;
                Location += offset;
            }
        }
    }
}