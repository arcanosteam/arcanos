﻿using Arcanos.Client.GameClient.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Arcanos.Client.GameClient.UI
{
    public class UITextBox : UIControl
    {
        public static UITexture NormalTexture { get; set; }
        public static UITexture HoveredTexture { get; set; }
        public static UITexture PressedTexture { get; set; }
        public static UITexture DisabledTexture { get; set; }
        public static UIFont TextFont { get; set; }

        public override UITexture CurrentGraphic
        {
            get
            {
                if (!Enabled && DisabledTexture != null) return DisabledTexture;
                if (IsPressed && PressedTexture != null) return PressedTexture;
                if (IsHovered && HoveredTexture != null) return HoveredTexture;
                return NormalTexture;
            }
        }
        public override UIFont Font
        {
            get { return TextFont; }
        }
        public override string Text
        {
            get
            {
                string text = HiddenInput ? new string(HiddenInputSymbol, base.Text.Length) : base.Text;
                if (IsFocused)
                    return text.Insert(SelectionStart, "_");
                return text;
            }
            set
            {
                if (base.Text == value) return;
                string old = base.Text;
                base.Text = value;
                if (TextChanged != null)
                    TextChanged(this, new TypeEventArgs<string>(old, value));
            }
        }
        public virtual string Value
        {
            get { return base.Text; }
            set
            {
                Text = value;
                SelectionStart = value.Length;
            }
        }

        public virtual bool AllowLetters { get; set; }
        public virtual bool AllowDigits { get; set; }
        public virtual bool AllowSymbols { get; set; }
        public virtual bool AllowSpaces { get; set; }
        public virtual bool AllowTabs { get; set; }
        public virtual bool AllowEnters { get; set; }
        public virtual int MaxLength { get; set; }
        public virtual string AcceptedText { get; set; }
        public virtual bool HiddenInput { get; set; }
        public virtual char HiddenInputSymbol { get; set; }

        public virtual int SelectionStart { get; set; }

        public event TypeEventHandler<string> TextAccepted;
        public event TypeEventHandler<string> TextDeclined;
        public event TypeEventHandler<string> TextChanged;

        public UITextBox(Vector2 location, Vector2 size, string text) : base(location, size)
        {
            GetsFocus = true;
            Text = text;
            AllowLetters = true;
            AllowDigits = true;
            AllowSymbols = true;
            AllowSpaces = true;
            AllowTabs = false;
            AllowEnters = false;
            MaxLength = int.MaxValue;
        }
        public UITextBox(Vector2 location, Vector2 size) : this(location, size, "")
        {
        }

        private bool wasFocused; // Used to not allow key pressed to fall to next focused control after switching focus with key press
        public override void Update()
        {
            base.Update();
            wasFocused = IsFocused;
        }

        public override bool Focus()
        {
            bool focused = base.Focus();
            SelectionStart = Value.Length;
            return focused;
        }
        public override void Unfocus()
        {
            base.Unfocus();
            string oldAcceptedText = AcceptedText;
            AcceptedText = base.Text;
            if (oldAcceptedText != AcceptedText)
                if (TextAccepted != null)
                    TextAccepted(this, new TypeEventArgs<string>(oldAcceptedText, AcceptedText));
        }

        public override bool OnKeyPress(Keys key)
        {
            bool handled = false;
            char c = InputManager.GetChar(key);
            if (wasFocused)
            {
                switch (key)
                {
                    case Keys.Back:
                        if (base.Text.Length > 0)
                        {
                            if (SelectionStart == 1)
                                Text = base.Text.Substring(1);
                            else if (SelectionStart == Value.Length)
                                Text = base.Text.Substring(0, base.Text.Length - 1);
                            else if (SelectionStart > 1)
                                Text = base.Text.Substring(0, SelectionStart - 1) + base.Text.Substring(SelectionStart);
                            else
                                ++SelectionStart;
                            --SelectionStart;
                        }
                        break;
                    case Keys.Enter:
                        if (AllowEnters)
                            Text = base.Text + '\n';
                        else
                        {
                            FocusNext();
                            string oldAcceptedText = AcceptedText;
                            AcceptedText = base.Text;
                            if (TextAccepted != null)
                                TextAccepted(this, new TypeEventArgs<string>(oldAcceptedText, AcceptedText));
                        }
                        break;
                    case Keys.Tab:
                        if (AllowTabs)
                            Text = base.Text + '\t';
                        else
                        {
                            FocusNext();
                            string oldAcceptedText = AcceptedText;
                            AcceptedText = base.Text;
                            if (TextAccepted != null)
                                TextAccepted(this, new TypeEventArgs<string>(oldAcceptedText, AcceptedText));
                        }
                        break;
                    case Keys.Escape:
                        Unfocus();
                        string declinedText = base.Text;
                        Text = AcceptedText;
                        if (TextDeclined != null)
                            TextDeclined(this, new TypeEventArgs<string>(declinedText, AcceptedText));
                        break;
                    case Keys.Left:
                        if (SelectionStart > 0)
                            --SelectionStart;
                        break;
                    case Keys.Right:
                        if (SelectionStart < Value.Length)
                            ++SelectionStart;
                        break;
                    default:
                        if (base.Text.Length < MaxLength && ((AllowLetters && char.IsLetter(c)) ||
                                                             (AllowDigits && char.IsDigit(c)) ||
                                                             (AllowSymbols && (char.IsSymbol(c) || char.IsPunctuation(c))) ||
                                                             (AllowSpaces && c == ' ')))
                        {
                            Text = base.Text.Insert(SelectionStart, c.ToString());
                            ++SelectionStart;
                        }
                        break;
                }
                handled = true;
            }
            handled |= base.OnKeyPress(key);
            return handled;
        }
        public override bool OnKeyRelease(Keys key)
        {
            bool handled = wasFocused;
            handled |= base.OnKeyRelease(key);
            return handled;
        }
        protected override void OnHide()
        {
            base.OnHide();
            Unfocus();
        }
    }
}