﻿using System;
using Microsoft.Xna.Framework;

namespace Arcanos.Client.GameClient.UI
{
    public class UIScrollbar : UIControl
    {
        public static UITexture UpArrowIcon { get; set; }
        public static UITexture DownArrowIcon { get; set; }
        public static UITexture BarIcon { get; set; }
        public static float DefaultThickness { get; set; }

        public float Minimum { get; set; }
        public float Maximum { get; set; }
        public float Range { get; set; }
        public float SmallIncrement { get; set; }
        public float BigIncrement { get; set; }
        public float ScrollPosition { get; set; }

        public UIScrollbarButton UpControl { get; set; }
        public UIScrollbarButton DownControl { get; set; }
        public UIScrollbarButton BarControl { get; set; }

        private bool grabbedBar = false;
        private Vector2 grabbedBarOffset = Vector2.Zero;
        protected float RangeHeight
        {
            get { return (Maximum - Minimum) * (MaxBarHeight / (Maximum - Minimum + Range)); }
        }
        protected float MaxBarHeight
        {
            get { return DownControl.Top - UpControl.Bottom; }
        }

        public event TypeEventHandler<float> Scrolled;

        public UIScrollbar(Vector2 location, float height, float minimum, float maximum, float range) : base(location, new Vector2(DefaultThickness, height))
        {
            Minimum = minimum;
            Maximum = maximum;
            Range = range;
            Controls.Add(UpControl = new UIScrollbarButton(Vector2.Zero, new Vector2(Width), UIButtonType.Tiny, UpArrowIcon)
            {
                Anchor = UIAnchor.Top | UIAnchor.Horizontal,
            });
            Controls.Add(DownControl = new UIScrollbarButton(new Vector2(0, Height - Width), new Vector2(Width), UIButtonType.Tiny, DownArrowIcon)
            {
                Anchor = UIAnchor.Bottom | UIAnchor.Horizontal,
            });
            Controls.Add(BarControl = new UIScrollbarButton(new Vector2(0, UpControl.Bottom), new Vector2(Width, DownControl.Top - UpControl.Bottom - RangeHeight), UIButtonType.Tiny, BarIcon)
            {
                Anchor = UIAnchor.Top | UIAnchor.Horizontal,
            });
            UpControl.OnClick += (sender, e) => Scroll(-SmallIncrement);
            DownControl.OnClick += (sender, e) => Scroll(SmallIncrement);
            BarControl.OnMouseLeftDown += (sender, e) =>
            {
                grabbedBar = true;
                grabbedBarOffset = Desktop.MouseLocation - BarControl.DesktopLocation;
            };
            BarControl.OnMouseLeftUp += (sender, e) =>
            {
                grabbedBar = false;
            };
            OnResize += (sender, e) => UpdateHeight();
            ClickThrough = true;
        }

        public void Scroll(float d)
        {
            Vector2 prevLocation = BarControl.Location;
            float prevPosition = ((prevLocation.Y - UpControl.Bottom) / RangeHeight) * (Maximum - Minimum) + Minimum;
            ScrollTo(prevPosition + d);
        }
        public void ScrollTo(float pos)
        {
            if (Minimum == Maximum)
            {
                BarControl.Size = new Vector2(BarControl.Width, MaxBarHeight);
                BarControl.Location = new Vector2(BarControl.Left, UpControl.Bottom);
            }
            else
            {
                if (pos > Maximum) pos = Maximum;
                if (pos < Minimum) pos = Minimum;
                Vector2 prevLocation = BarControl.Location;
                float prevPosition = ((prevLocation.Y - UpControl.Bottom) / RangeHeight) * (Maximum - Minimum) + Minimum;
                float differenceLocation = ((pos - Minimum) / (Maximum - Minimum)) * RangeHeight;
                BarControl.Size = new Vector2(BarControl.Width, MaxBarHeight - RangeHeight);
                BarControl.Location = new Vector2(BarControl.Left, Math.Max(UpControl.Bottom, Math.Min(DownControl.Top - BarControl.Height, UpControl.Bottom + differenceLocation)));
                ScrollPosition = ((BarControl.Top - UpControl.Bottom) / RangeHeight) * (Maximum - Minimum) + Minimum;
                if (Scrolled != null)
                    Scrolled(this, new TypeEventArgs<float>() { OldValue = prevPosition, NewValue = ScrollPosition });
            }
        }
        public void ScrollToPercent(float percent)
        {
            ScrollTo(Minimum + percent * (Maximum - Minimum));
        }
        public float GetScrollPercent()
        {
            return (ScrollPosition - Minimum) / (Maximum - Minimum);
        }
        public void UpdateHeight()
        {
            if (Minimum == Maximum)
            {
                BarControl.Size = new Vector2(BarControl.Width, MaxBarHeight);
                BarControl.Location = new Vector2(BarControl.Left, UpControl.Bottom);
            }
            else
            {
                Vector2 prevLocation = BarControl.Location;
                float prevPosition = ((prevLocation.Y - UpControl.Bottom) / RangeHeight) * (Maximum - Minimum) + Minimum;
                float differenceLocation = ((prevPosition - Minimum) / (Maximum - Minimum)) * RangeHeight;
                BarControl.Size = new Vector2(BarControl.Width, Math.Max(BarControl.Width, MaxBarHeight - RangeHeight));
                BarControl.Bottom++;
                BarControl.Bottom--;
                BarControl.Location = new Vector2(BarControl.Left, Math.Max(UpControl.Bottom, Math.Min(DownControl.Top - BarControl.Height, UpControl.Bottom + differenceLocation)));
            }
        }

        public override bool OnMouseMove(Vector2 m)
        {
            if (!Visible) return false;
            bool handled = base.OnMouseMove(m);
            if (grabbedBar)
            {
                ScrollTo(Minimum + (Maximum - Minimum) * (Desktop.MouseLocation.Y - grabbedBarOffset.Y - UpControl.DesktopLocation.Y - UpControl.Height) / RangeHeight);
                handled = true;
            }
            return handled;
        }
        public override bool OnMouseLeft(bool pressed)
        {
            bool handled = base.OnMouseLeft(pressed);
            if (!handled && IsHovered && !IsPressed && pressed)
            {
                if (Desktop.MouseLocation.Y < DesktopLocation.Y + BarControl.Top + BarControl.Height / 2)
                    Scroll(-BigIncrement);
                else
                    Scroll(BigIncrement);
            }
            return handled;
        }
    }
    public class UIHScrollbar : UIControl
    {
        public static UITexture LeftArrowIcon { get; set; }
        public static UITexture RightArrowIcon { get; set; }
        public static UITexture BarIcon { get; set; }
        public static float DefaultThickness { get; set; }

        public float Minimum { get; set; }
        public float Maximum { get; set; }
        public float Range { get; set; }
        public float SmallIncrement { get; set; }
        public float BigIncrement { get; set; }
        public float ScrollPosition { get; set; }

        public UIScrollbarButton UpControl { get; set; }
        public UIScrollbarButton DownControl { get; set; }
        public UIScrollbarButton BarControl { get; set; }

        private bool grabbedBar = false;
        private Vector2 grabbedBarOffset = Vector2.Zero;
        protected float RangeWidth
        {
            get { return (Maximum - Minimum) * (MaxBarWidth / (Maximum - Minimum + Range)); }
        }
        protected float MaxBarWidth
        {
            get { return DownControl.Left - UpControl.Right; }
        }

        public event TypeEventHandler<float> Scrolled;

        public UIHScrollbar(Vector2 location, float width, float minimum, float maximum, float range, bool showButtons) : base(location, new Vector2(width, DefaultThickness))
        {
            Minimum = minimum;
            Maximum = maximum;
            Range = range;
            Controls.Add(UpControl = new UIScrollbarButton(Vector2.Zero, new Vector2(showButtons ? Height : 0, Height), UIButtonType.Tiny, LeftArrowIcon)
            {
                Anchor = UIAnchor.Vertical | UIAnchor.Left,
                Visible = showButtons,
            });
            Controls.Add(DownControl = new UIScrollbarButton(new Vector2(Width - (showButtons ? Height : 0), 0), new Vector2(showButtons ? Height : 0, Height), UIButtonType.Tiny, RightArrowIcon)
            {
                Anchor = UIAnchor.Vertical | UIAnchor.Right,
                Visible = showButtons,
            });
            Controls.Add(BarControl = new UIScrollbarButton(new Vector2(UpControl.Right, 0), new Vector2(DownControl.Left - UpControl.Right - RangeWidth, Height), UIButtonType.Tiny, BarIcon)
            {
                Anchor = UIAnchor.Vertical | UIAnchor.Left,
            });
            UpControl.OnClick += (sender, e) => Scroll(-SmallIncrement);
            DownControl.OnClick += (sender, e) => Scroll(SmallIncrement);
            BarControl.OnMouseLeftDown += (sender, e) =>
            {
                grabbedBar = true;
                grabbedBarOffset = Desktop.MouseLocation - BarControl.DesktopLocation;
            };
            BarControl.OnMouseLeftUp += (sender, e) =>
            {
                grabbedBar = false;
            };
            OnResize += (sender, e) => UpdateWidth();
            ClickThrough = true;
        }

        public void Scroll(float d)
        {
            Vector2 prevLocation = BarControl.Location;
            float prevPosition = ((prevLocation.X - UpControl.Right) / RangeWidth) * (Maximum - Minimum) + Minimum;
            ScrollTo(prevPosition + d);
        }
        public void ScrollTo(float pos)
        {
            if (Minimum == Maximum)
            {
                BarControl.Size = new Vector2(MaxBarWidth, BarControl.Height);
                BarControl.Location = new Vector2(UpControl.Right, BarControl.Top);
            }
            else
            {
                if (pos > Maximum) pos = Maximum;
                if (pos < Minimum) pos = Minimum;
                Vector2 prevLocation = BarControl.Location;
                float prevPosition = ((prevLocation.X - UpControl.Right) / RangeWidth) * (Maximum - Minimum) + Minimum;
                float differenceLocation = ((pos - Minimum) / (Maximum - Minimum)) * RangeWidth;
                BarControl.Size = new Vector2(MaxBarWidth - RangeWidth, BarControl.Height);
                BarControl.Location = new Vector2(Math.Max(UpControl.Right, Math.Min(DownControl.Left - BarControl.Width, UpControl.Right + differenceLocation)), BarControl.Top);
                ScrollPosition = ((BarControl.Left - UpControl.Right) / RangeWidth) * (Maximum - Minimum) + Minimum;
                if (Scrolled != null)
                    Scrolled(this, new TypeEventArgs<float>() { OldValue = prevPosition, NewValue = ScrollPosition });
            }
        }
        public void ScrollToPercent(float percent)
        {
            ScrollTo(Minimum + percent * (Maximum - Minimum));
        }
        public float GetScrollPercent()
        {
            return (ScrollPosition - Minimum) / (Maximum - Minimum);
        }
        public void UpdateWidth()
        {
            if (Minimum == Maximum)
            {
                BarControl.Size = new Vector2(MaxBarWidth, BarControl.Height);
                BarControl.Location = new Vector2(UpControl.Right, BarControl.Top);
            }
            else
            {
                Vector2 prevLocation = BarControl.Location;
                float prevPosition = ((prevLocation.X - UpControl.Right) / RangeWidth) * (Maximum - Minimum) + Minimum;
                float differenceLocation = ((prevPosition - Minimum) / (Maximum - Minimum)) * RangeWidth;
                BarControl.Size = new Vector2(Math.Max(BarControl.Width, MaxBarWidth - RangeWidth), BarControl.Height);
                BarControl.Right++;
                BarControl.Right--;
                BarControl.Location = new Vector2(Math.Max(UpControl.Right, Math.Min(DownControl.Left - BarControl.Width, UpControl.Right + differenceLocation)), BarControl.Top);
            }
        }

        public override bool OnMouseMove(Vector2 m)
        {
            if (!Visible) return false;
            bool handled = base.OnMouseMove(m);
            if (grabbedBar)
            {
                ScrollTo(Minimum + (Maximum - Minimum) * (Desktop.MouseLocation.X - grabbedBarOffset.X - UpControl.DesktopLocation.X - UpControl.Width) / RangeWidth);
                handled = true;
            }
            return handled;
        }
        public override bool OnMouseLeft(bool pressed)
        {
            bool handled = base.OnMouseLeft(pressed);
            if (!handled && IsHovered && !IsPressed && pressed)
            {
                if (Desktop.MouseLocation.X < DesktopLocation.X + BarControl.Left + BarControl.Width / 2)
                    Scroll(-BigIncrement);
                else
                    Scroll(BigIncrement);
            }
            return handled;
        }
    }
    public class UIScrollbarButton : UIButton
    {
        public static UITexture ScrollbarNormalTexture { get; set; }
        public static UITexture ScrollbarHoveredTexture { get; set; }
        public static UITexture ScrollbarPressedTexture { get; set; }
        public static UITexture ScrollbarDisabledTexture { get; set; }

        public override UITexture BackdropGraphic
        {
            get
            {
                if (Icon == null) return null;
                if (!Enabled && DisabledTexture != null) return ScrollbarDisabledTexture;
                if ((AlwaysPressed || IsPressed) && PressedTexture != null) return ScrollbarPressedTexture;
                if (IsHovered && HoveredTexture != null) return ScrollbarHoveredTexture;
                return ScrollbarNormalTexture;
            }
        }
        public override UITexture CurrentGraphic
        {
            get
            {
                if (Icon != null) return null;
                if (!Enabled && DisabledTexture != null) return ScrollbarDisabledTexture;
                if ((AlwaysPressed || IsPressed) && PressedTexture != null) return ScrollbarPressedTexture;
                if (IsHovered && HoveredTexture != null) return ScrollbarHoveredTexture;
                return ScrollbarNormalTexture;
            }
        }

        public UIScrollbarButton(Vector2 location, Vector2 size, UIButtonType type, UITexture icon) : base(location, size, type, icon)
        {
            
        }
    }
}
