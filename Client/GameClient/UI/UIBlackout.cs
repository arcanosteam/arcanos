﻿using Arcanos.Shared.Timing;
using Microsoft.Xna.Framework;

namespace Arcanos.Client.GameClient.UI
{
    public class UIBlackout : UIControl
    {
        public static UITexture Texture { get; set; }
        public static float FadeDuration { get; set; }

        public override UITexture CurrentGraphic
        {
            get { return Texture; }
        }

        private bool fadingIn, fadingOut;
        private readonly bool remove;

        public UIBlackout() : this(false) { }
        public UIBlackout(bool removeOnFadeOut) : base(Vector2.Zero, Vector2.Zero)
        {
            Anchor = UIAnchor.All;
            remove = removeOnFadeOut;
            ClickThrough = true;
            Opacity = 0;
            Visible = false;
            OnParentChanged += (sender, args) =>
            {
                if (Parent != null)
                    if (Parent is UIDesktop)
                        Size = ((UIDesktop)Parent).Size;
                    else
                        Size = ((UIControl)Parent).Size;
            };
        }

        public override void Update()
        {
            base.Update();
            if (fadingIn)
            {
                Opacity += (1 / FadeDuration) * Time.PerSecond;
                if (Opacity >= 1)
                {
                    Opacity = 1;
                    fadingIn = false;
                }
            }
            else if (fadingOut)
            {
                Opacity -= (1 / FadeDuration) * Time.PerSecond;
                if (Opacity <= 0)
                {
                    Opacity = 0;
                    Visible = false;
                    fadingOut = false;
                    if (remove && Parent != null)
                        Parent.Controls.Remove(this);
                }
            }
        }

        public void Enable()
        {
            ClickThrough = false;
            Visible = true;
            fadingOut = false;
            fadingIn = true;
        }
        public void Disable()
        {
            ClickThrough = true;
            fadingOut = true;
            fadingIn = false;
        }
    }
}