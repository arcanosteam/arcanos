﻿using System.Collections.Generic;
using Arcanos.Shared.Localization;
using Microsoft.Xna.Framework;

namespace Arcanos.Client.GameClient.UI
{
    public enum UIButtonType
    {
        Tiny,
        Small,
        Medium,
    }
    public class UIButton : UIControl
    {
        public static UITexture NormalTexture { get; set; }
        public static UITexture HoveredTexture { get; set; }
        public static UITexture PressedTexture { get; set; }
        public static UITexture DisabledTexture { get; set; }
        public static Dictionary<UIButtonType, UIFont> TextFonts { get; set; }

        public override UITexture BackdropGraphic
        {
            get
            {
                if (Icon == null) return null;
                if (!Enabled && DisabledTexture != null) return DisabledTexture;
                if ((AlwaysPressed || IsPressed) && PressedTexture != null) return PressedTexture;
                if (IsHovered && HoveredTexture != null) return HoveredTexture;
                return NormalTexture;
            }
        }
        public override UITexture CurrentGraphic
        {
            get
            {
                if (Icon != null) return null;
                if (!Enabled && DisabledTexture != null) return DisabledTexture;
                if ((AlwaysPressed || IsPressed) && PressedTexture != null) return PressedTexture;
                if (IsHovered && HoveredTexture != null) return HoveredTexture;
                return NormalTexture;
            }
        }
        private UIFont? overrideFont;
        public override UIFont Font
        {
            get { return overrideFont ?? TextFonts[ButtonType]; }
            set { overrideFont = value; }
        }
        public override Vector2 Size
        {
            get
            {
                return base.Size;
            }
            set
            {
                base.Size = value;
                BackdropSize = value;
            }
        }

        public UIButtonType ButtonType { get; set; }
        private UITexture icon;
        public UITexture Icon
        {
            get { return icon; }
            set
            {
                icon = value;
                if (IconControl != null)
                {
                    IconControl.CurrentGraphic = value;
                    ResizedHandler(this, null);
                }
                else if (icon != null)
                {
                    Controls.Add(IconControl = new UIControl(Vector2.Zero, Vector2.Zero) { CurrentGraphic = Icon, Anchor = 0 });
                    Controls.Add(TextControl = new UIControl(Vector2.Zero, Vector2.Zero) { Text = Text, TextRef = TextRef, Anchor = 0, Font = Font, CutOverflownText = false });
                    //ContentOffset = BackdropGraphic.PixelPaddingMin - new Vector2(AdditionalSpace);
                    OnResize += ResizedHandler;
                    ResizedHandler(this, null);
                    Text = null;
                    TextRef = null;
                }
            }
        }
        public bool AlwaysPressed { get; set; }
        public int AdditionalSpace { get; set; }

        public UIControl IconControl { get; set; }
        public UIControl TextControl { get; set; }

        static UIButton()
        {
            TextFonts = new Dictionary<UIButtonType, UIFont>();
        }

        public UIButton(Vector2 location, Vector2 size, UIButtonType type, string text, UITexture icon) : base(location, size)
        {
            ButtonType = type;
            this.icon = icon;
            if (Icon != null)
            {
                Controls.Add(IconControl = new UIControl(Vector2.Zero, Vector2.Zero) { CurrentGraphic = Icon, Anchor = 0 });
                Controls.Add(TextControl = new UIControl(Vector2.Zero, Vector2.Zero) { Text = text, Anchor = 0, Font = Font, CutOverflownText = false });
                //ContentOffset = BackdropGraphic.PixelPaddingMin - new Vector2(AdditionalSpace);
                OnResize += ResizedHandler;
                ResizedHandler(this, null);
            }
            else
                Text = text;
        }
        public UIButton(Vector2 location, Vector2 size, UIButtonType type, Text text, UITexture icon) : this(location, size, type, text.ToString(), icon)
        {
            (Icon != null ? TextControl : this).TextRef = text;
        }
        public UIButton(Vector2 location, Vector2 size, UIButtonType type, string text) : this(location, size, type, text, null) { }
        public UIButton(Vector2 location, Vector2 size, UIButtonType type, Text text) : this(location, size, type, text.ToString(), null)
        {
            (Icon != null ? TextControl : this).TextRef = text;
        }
        public UIButton(Vector2 location, Vector2 size, UIButtonType type, UITexture icon) : this(location, size, type, (string)null, icon) { }

        private void ResizedHandler(object sender, VectorEventArgs e)
        {
            BackdropSize = Size;
            Vector2 iconSize = new Vector2(Icon.PixelSize.X, Icon.PixelSize.Y);
            Vector2 contentSize = new Vector2(Width - BackdropGraphic.PixelPaddingH + AdditionalSpace * 2, Height - BackdropGraphic.PixelPaddingV + AdditionalSpace * 2);
            Vector2 textSize;// = Font.MeasureString(TextControl.Text, contentSize.X - iconSize.X);
            TextControl.MultilineText = Font.FitString(TextControl.Text, contentSize.X - iconSize.X, out textSize).Length > 1;
            if (iconSize.X + textSize.X > contentSize.X)
                iconSize *= contentSize.X / (iconSize.X + textSize.X);
            if (iconSize.Y > contentSize.Y)
                iconSize *= contentSize.Y / iconSize.Y;
            Vector2 iconPos = Vector2.Zero;
            IconControl.Anchor = UIAnchor.None;
            TextControl.Anchor = UIAnchor.None;
            if ((Font.Align & UIFontAlign.Left) != 0)
            {
                iconPos.X = 0;
                IconControl.Anchor |= UIAnchor.Left;
                TextControl.Anchor |= UIAnchor.Left;
            }
            if ((Font.Align & UIFontAlign.Center) != 0)
            {
                iconPos.X = (contentSize.X - iconSize.X - textSize.X) / 2;
            }
            if ((Font.Align & UIFontAlign.Right) != 0)
            {
                iconPos.X = contentSize.X - iconSize.X - textSize.X;
                IconControl.Anchor |= UIAnchor.Right;
                TextControl.Anchor |= UIAnchor.Right;
            }
            if ((Font.Align & UIFontAlign.Top) != 0)
            {
                iconPos.Y = 0;
                IconControl.Anchor |= UIAnchor.Top;
                TextControl.Anchor |= UIAnchor.Top;
            }
            if ((Font.Align & UIFontAlign.Middle) != 0)
            {
                iconPos.Y = (contentSize.Y - iconSize.Y) / 2;
            }
            if ((Font.Align & UIFontAlign.Bottom) != 0)
            {
                iconPos.Y = contentSize.Y - iconSize.Y;
                IconControl.Anchor |= UIAnchor.Bottom;
                TextControl.Anchor |= UIAnchor.Bottom;
            }
            iconPos.X -= AdditionalSpace;
            iconPos.Y -= AdditionalSpace;
            IconControl.Location = (BackdropGraphic.PixelPaddingMin + iconPos).Int();
            IconControl.Size = iconSize.Int();
            TextControl.Location = new Vector2(
                IconControl.Right,
                (contentSize.Y - textSize.Y) / 2).Int();
            TextControl.Size = textSize.Int();
        }
    }
}