﻿using System;

namespace Arcanos.Client.GameClient.Spells
{
    public struct VisibleAura : IEquatable<VisibleAura>
    {
        public readonly SpellTemplate Spell;
        public readonly Aura Aura;

        public VisibleAura(SpellTemplate spell, Aura aura)
        {
            Spell = spell;
            Aura = aura;
        }

        public bool Equals(VisibleAura other)
        {
            return Equals((object)other);
        }
        public override bool Equals(object obj)
        {
            return obj is VisibleAura && ((VisibleAura)obj).Spell == Spell;
        }
        public override int GetHashCode()
        {
            unchecked
            {
                return ((Spell != null ? Spell.GetHashCode() : 0) * 397) ^ (Aura != null ? Aura.GetHashCode() : 0);
            }
        }
    }
}