﻿using Arcanos.Client.GameClient.World;
using Arcanos.Shared.Spells;
using Microsoft.Xna.Framework;

namespace Arcanos.Client.GameClient.Spells
{
    public class AreaEffect : AreaEffectBase
    {
        public readonly Spell VisualSpell;

        public AreaEffect(ulong guid, SpellTemplate spell, Map map, Vector3 position) : base(guid)
        {
            VisualSpell = spell.Create(map, position) as Spell;
            VisualSpell.SetState(ClientSpellState.GroundStart);
        }

        public override void Remove()
        {
            VisualSpell.SetState(ClientSpellState.GroundEnd);
            VisualSpell.Remove();
            base.Remove();
        }
    }
}