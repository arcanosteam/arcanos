﻿using System.Collections.Generic;
using Arcanos.Client.GameClient.UI;
using Arcanos.Client.GameClient.World;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Spells;
using Arcanos.Shared.Timing;
using Microsoft.Xna.Framework;

namespace Arcanos.Client.GameClient.Spells
{
    public class SpellBook : SpellBookBase
    {
        public readonly List<VisibleAura> VisibleAuras = new List<VisibleAura>();
        public readonly List<Spell> CurrentSpells = new List<Spell>();

        public SpellBook(CreatureBase owner) : base(owner) { }

        public override void Update()
        {
            foreach (AuraBase aura in AppliedAuras)
                aura.Update();
            if (IsCasting)
            {
                CastingTime += Time.PerSecond;
                if (CastingTime >= CastingSpell.CastingTime)
                    StopCasting();
            }
            if (GlobalCooldown > 0)
            {
                GlobalCooldown -= Time.PerSecond;
                if (GlobalCooldown < 0)
                    GlobalCooldown = 0;
                if (Owner == WorldManager.PlayerCreature)
                    UIManager.RaiseEvent(UIEvents.GlobalCooldownUpdated, this, GlobalCooldown, GlobalCooldownTotal);
            }
            foreach (KeyValuePair<int, CooldownInstance> pair in Cooldowns)
                if (pair.Value.Left > 0)
                {
                    pair.Value.Left -= Time.PerSecond;
                    if (pair.Value.Left < 0)
                        pair.Value.Left = 0;
                    if (Owner == WorldManager.PlayerCreature)
                        UIManager.RaiseEvent(UIEvents.SpellCooldownUpdated, this, pair.Value.Left, pair.Value.Total, pair.Key);
                }
        }

        public override CastSpellResult CastSpell(SpellTemplateBase spell, CreatureBase target, CastSpellFlags flags)
        {
            if (spell.HasFlag(SpellFlags.AutoTargetSelf))
                target = Owner;

            if ((flags & CastSpellFlags.IgnoreCastingTime) == 0 && !spell.IsInstantCast())
                return StartCasting(spell, target, flags);

            return FinishCast(spell, target, flags);
        }
        public override CastSpellResult CastSpell(SpellTemplateBase spell, Vector3 target, CastSpellFlags flags)
        {
            if (spell.HasFlag(SpellFlags.AutoTargetSelf))
                target = Owner.Position;

            if ((flags & CastSpellFlags.IgnoreCastingTime) == 0 && !spell.IsInstantCast())
                return StartCasting(spell, target, flags);

            return FinishCast(spell, target, flags);
        }
        public override CastSpellResult CanCastSpell(SpellTemplateBase spell, CreatureBase target, CastSpellFlags flags)
        {
            if (spell.HasFlag(SpellFlags.AutoTargetSelf))
                target = Owner;

            CastSpellResult genericCheckResult = CanCastSpell(spell, flags);
            if (genericCheckResult != CastSpellResult.Ok)
                return genericCheckResult;
            if ((flags & CastSpellFlags.IgnoreTargetCheck) == 0)
            {
                CastSpellResult targetCheckResult = spell.CanBeCastOn(Owner, target);
                if (targetCheckResult != CastSpellResult.Ok)
                    return targetCheckResult;
            }

            return CastSpellResult.Ok;
        }
        public override CastSpellResult CanCastSpell(SpellTemplateBase spell, Vector3 target, CastSpellFlags flags)
        {
            if (spell.HasFlag(SpellFlags.AutoTargetSelf))
                target = Owner.Position;

            CastSpellResult genericCheckResult = CanCastSpell(spell, flags);
            if (genericCheckResult != CastSpellResult.Ok)
                return genericCheckResult;
            if ((flags & CastSpellFlags.IgnoreTargetCheck) == 0)
            {
                CastSpellResult targetCheckResult = spell.CanBeCastAt(Owner, target);
                if (targetCheckResult != CastSpellResult.Ok)
                    return targetCheckResult;
            }

            return CastSpellResult.Ok;
        }
        public override void Interrupt(InterruptMask interrupt)
        {
            if (IsCasting)
                InterruptCasting(interrupt);
        }
        public override void InterruptCasting(InterruptMask interrupt)
        {
            if (!IsCasting)
                return;
            if (!CastingSpell.IsCastingInterruptedBy(interrupt))
                return;

            Owner.SpellCastingFailed(CastingSpell, CastingTarget, CastSpellResult.Interrupted);
            StopCasting();
        }
        public override void InterruptAuras(InterruptMask interrupt)
        {
            foreach (AuraBase aura in AppliedAuras)
                if (aura.Spell.IsAuraInterruptedBy(interrupt))
                    RemoveAura(aura);
        }
        public override CastSpellResult FinishCast(SpellTemplateBase spell, CreatureBase target, CastSpellFlags flags)
        {
            if (spell.HasFlag(SpellFlags.AutoTargetSelf))
                target = Owner;

            CastSpellResult result = CanCastSpell(spell, target, flags);
            if (result != CastSpellResult.Ok)
                return result;

            PutOnCooldown(spell);
            return CastSpellResult.Ok;
        }
        public override CastSpellResult FinishCast(SpellTemplateBase spell, Vector3 target, CastSpellFlags flags)
        {
            if (spell.HasFlag(SpellFlags.AutoTargetSelf))
                target = Owner.Position;

            CastSpellResult result = CanCastSpell(spell, target, flags);
            if (result != CastSpellResult.Ok)
                return result;

            PutOnCooldown(spell);
            return CastSpellResult.Ok;
        }
        public override CastSpellResult StartCasting(SpellTemplateBase spell, CreatureBase target, CastSpellFlags flags)
        {
            if (spell.HasFlag(SpellFlags.AutoTargetSelf))
                target = Owner;

            CastSpellResult result = CanCastSpell(spell, target, flags);
            if (result != CastSpellResult.Ok)
                return result;

            CastingSpell = spell;
            CastingTarget = target;
            CastingGroundTarget = Vector3.Zero;
            CastingTime = 0;
            CastingFlags = flags;
            Owner.SpellCastingStarted(spell, target);
            return CastSpellResult.Ok;
        }
        public override CastSpellResult StartCasting(SpellTemplateBase spell, Vector3 target, CastSpellFlags flags)
        {
            if (spell.HasFlag(SpellFlags.AutoTargetSelf))
                target = Owner.Position;

            CastSpellResult result = CanCastSpell(spell, target, flags);
            if (result != CastSpellResult.Ok)
                return result;

            CastingSpell = spell;
            CastingTarget = null;
            CastingGroundTarget = target;
            CastingTime = 0;
            CastingFlags = flags;
            Owner.SpellCastingStarted(spell, target);
            return CastSpellResult.Ok;
        }
        public override void FailCasting(CastSpellResult result)
        {
            Owner.SpellCastingFailed(CastingSpell, CastingTarget, result);
            StopCasting();
        }
        public override void StopCasting()
        {
            CastingSpell = null;
            CastingTarget = null;
            CastingGroundTarget = Vector3.Zero;
            CastingTime = 0;
            CastingFlags = 0;
        }

        public override void ApplyAura(ulong casterGUID, SpellTemplateBase spell, int effectID)
        {
            Aura aura = new Aura(Owner, casterGUID, spell, effectID);
            AppliedAuras.Add(aura);
            
            VisibleAura visAura = new VisibleAura(spell as SpellTemplate, aura);
            if (!spell.HasFlag(SpellFlags.HiddenAura) && !VisibleAuras.Contains(visAura))
            {
                VisibleAuras.Add(visAura);
                UIManager.RaiseEvent(UIEvents.AuraApplied, Owner, Owner.GUID, spell.ID);
            }
            
            Owner.AuraApplied(aura);
        }
        public override void ApplyAura(ulong casterGUID, SpellTemplateBase spell, int effectID, float duration)
        {
            Aura aura = new Aura(Owner, casterGUID, spell, effectID);
            aura.DurationTotal = aura.DurationLeft = duration;
            AppliedAuras.Add(aura);

            VisibleAura visAura = new VisibleAura(spell as SpellTemplate, aura);
            if (!spell.HasFlag(SpellFlags.HiddenAura) && !VisibleAuras.Contains(visAura))
            {
                VisibleAuras.Add(visAura);
                UIManager.RaiseEvent(UIEvents.AuraApplied, Owner, Owner.GUID, spell.ID);
            }

            Owner.AuraApplied(aura);
        }
        public override void RemoveAura(AuraBase aura)
        {
            if (!AppliedAuras.Remove(aura))
                return;

            VisibleAura visAura = new VisibleAura(aura.Spell as SpellTemplate, aura as Aura);
            if (!aura.Spell.HasFlag(SpellFlags.HiddenAura) && !HasAura(aura.Spell))
            {
                VisibleAuras.Remove(visAura);
                UIManager.RaiseEvent(UIEvents.AuraRemoved, Owner, Owner.GUID, aura.Spell.ID);
            }

            Owner.AuraRemoved(aura);
        }

        public void RemoveCurrentSpell(Spell spell)
        {
            CurrentSpells.Remove(spell);
        }
        public void SetStateForCurrentSpell(SpellTemplateBase spell, ulong targetGUID, Vector3 groundTarget, ClientSpellState state)
        {
            foreach (Spell currentSpell in CurrentSpells)
            {
                if (currentSpell.Template != spell || currentSpell.State >= state)
                    continue;
                if (targetGUID != 0 && currentSpell.ExplicitTarget.GUID != targetGUID)
                    continue;
                if (targetGUID == 0 && currentSpell.GroundTarget != groundTarget)
                    continue;

                currentSpell.SetState(state);
                return;
            }
            Spell newSpell = (targetGUID != 0 ? spell.Create(Owner, ObjectManager.GetCreature(targetGUID)) : spell.Create(Owner, groundTarget)) as Spell;
            CurrentSpells.Add(newSpell);
            newSpell.SetState(state);
        }
    }
}