﻿using System;
using Arcanos.Shared.Graphics;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Spells;
using Arcanos.Shared.World;
using Microsoft.Xna.Framework;

namespace Arcanos.Client.GameClient.Spells
{
    public enum ClientSpellState : byte
    {
        Casting,
        CastSuccess,
        CastFail,
        Channeling,
        Flying,
        Hit,
        Miss,
        GroundStart,
        GroundIdle,
        GroundTrigger,
        GroundEnd,

        Max,
    }
    public class Spell : SpellBase
    {
        public new SpellTemplate Template { get { return (SpellTemplate)template; } }

        public ClientSpellState State = ClientSpellState.Casting;
        public readonly ModelInstance[] StateModels = new ModelInstance[(int)ClientSpellState.Max];

        private bool markedForRemoval;
        private string prevCasterAnim, prevTargetAnim;

        public Spell(SpellTemplateBase template, MapBase map, Vector3 pos) : base(template, map, pos)
        {
        }
        public Spell(SpellTemplateBase template, CreatureBase caster, CreatureBase target) : base(template, caster, target)
        {
            markedForRemoval = false;
        }
        public Spell(SpellTemplateBase template, CreatureBase caster, Vector3 target) : base(template, caster, target)
        {
            markedForRemoval = false;
        }

        public override void Update()
        {
            if (State < ClientSpellState.Flying)
                Position = Caster.Position + Caster.GetSpellCastOffset();
            else if (State == ClientSpellState.Flying)
                base.Update();
            else if (ExplicitTarget != null)
                Position = ExplicitTarget.Position + Caster.GetSpellHitOffset();

            byte inactiveModels = 0;
            for (byte state = 0; state < (byte)ClientSpellState.Max; state++)
            {
                if (StateModels[state] != null)
                {
                    StateModels[state].Update();
                    if (!StateModels[state].IsActive)
                    {
                        ++inactiveModels;
                        if (State != (ClientSpellState)state)
                        {
                            StateModels[state].Destroy();
                            StateModels[state] = null;
                        }
                    }
                }
                else
                    ++inactiveModels;
            }
            if (markedForRemoval && inactiveModels == (byte)ClientSpellState.Max)
            {
                if (Caster != null)
                    (Caster.SpellBook as SpellBook).RemoveCurrentSpell(this);
                base.Remove();
            }
        }
        public override void Destroy()
        {
            base.Destroy();
            for (byte state = 0; state < (byte)ClientSpellState.Max; state++)
                if (StateModels[state] != null)
                    StateModels[state].Destroy();
        }

        public void SetState(ClientSpellState state)
        {
            State = state;

            if (Template.Visual != null)
            {
                SpellVisualSet set;
                switch (State)
                {
                    case ClientSpellState.Casting:
                        set = Template.Visual.Casting;
                        break;
                    case ClientSpellState.CastSuccess:
                        StopModel(ClientSpellState.Casting);
                        set = Template.Visual.CastSuccess;
                        break;
                    case ClientSpellState.CastFail:
                        StopModel(ClientSpellState.Casting);
                        StopModel(ClientSpellState.Channeling);
                        set = Template.Visual.CastFail;
                        break;
                    case ClientSpellState.Channeling:
                        StopModel(ClientSpellState.Casting);
                        StopModel(ClientSpellState.CastSuccess);
                        set = Template.Visual.Channel;
                        break;
                    case ClientSpellState.Flying:
                        StopModel(ClientSpellState.CastSuccess);
                        set = Template.Visual.Flying;
                        break;
                    case ClientSpellState.Hit:
                        StopModel(ClientSpellState.CastSuccess);
                        StopModel(ClientSpellState.Flying);
                        set = Template.Visual.Hit;
                        break;
                    case ClientSpellState.Miss:
                        StopModel(ClientSpellState.CastSuccess);
                        StopModel(ClientSpellState.Flying);
                        set = Template.Visual.Miss;
                        break;
                    case ClientSpellState.GroundStart:
                        StopModel(ClientSpellState.CastSuccess);
                        StopModel(ClientSpellState.Flying);
                        set = Template.Visual.GroundStart;
                        break;
                    case ClientSpellState.GroundIdle:
                        StopModel(ClientSpellState.GroundStart);
                        set = Template.Visual.GroundIdle;
                        break;
                    case ClientSpellState.GroundTrigger:
                        set = Template.Visual.GroundTrigger;
                        break;
                    case ClientSpellState.GroundEnd:
                        StopModel(ClientSpellState.GroundStart);
                        StopModel(ClientSpellState.GroundIdle);
                        set = Template.Visual.GroundEnd;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException("state");
                }

                if (set.Model != null)
                {
                    if (StateModels[(byte)State] != null)
                        StateModels[(byte)State].Destroy();
                    StateModels[(byte)State] = set.Model.CreateInstance(this);
                    StateModels[(byte)State].Update();
                    if (set.ModelAnimation != null)
                        StateModels[(byte)State].PlayAnimation(set.ModelAnimation);
                }
                if (prevCasterAnim != null)
                    Caster.Model.StopAnimation(prevCasterAnim);
                if (set.CasterAnimation != null && Caster.IsAlive)
                    Caster.Model.PlayAnimation(prevCasterAnim = set.CasterAnimation);
                if (ExplicitTarget != null)
                {
                    if (prevTargetAnim != null)
                        ExplicitTarget.Model.StopAnimation(prevTargetAnim);
                    if (set.TargetAnimation != null && ExplicitTarget.IsAlive)
                        ExplicitTarget.Model.PlayAnimation(prevTargetAnim = set.TargetAnimation);
                }
            }

            switch (State)
            {
                case ClientSpellState.Casting:
                case ClientSpellState.Channeling:
                case ClientSpellState.Flying:
                case ClientSpellState.GroundIdle:
                    break;
                case ClientSpellState.CastSuccess:
                    SetState(ClientSpellState.Flying);
                    break;
                case ClientSpellState.CastFail:
                case ClientSpellState.Hit:
                case ClientSpellState.Miss:
                case ClientSpellState.GroundEnd:
                    for (ClientSpellState s = 0; s < ClientSpellState.Max; ++s)
                        StopModel(s);
                    Remove();
                    break;
                case ClientSpellState.GroundStart:
                    SetState(ClientSpellState.GroundIdle);
                    break;
                case ClientSpellState.GroundTrigger:
                    SetState(ClientSpellState.GroundIdle);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public override void Hit()
        {
            Remove();
        }
        public override void Remove()
        {
            markedForRemoval = true;
        }

        private void StopModel(ClientSpellState state)
        {
            if (StateModels[(byte)state] != null)
            {
                if (StateModels[(byte)state].IsActive)
                    StateModels[(byte)state].RequestStop();
                else
                {
                    StateModels[(byte)state].Destroy();
                    StateModels[(byte)state] = null;
                }
            }
        }
    }
}