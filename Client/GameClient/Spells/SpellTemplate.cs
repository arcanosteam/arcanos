﻿using Arcanos.Client.GameClient.Data;
using Arcanos.Shared.Data;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Spells;
using Arcanos.Shared.World;
using Microsoft.Xna.Framework;

namespace Arcanos.Client.GameClient.Spells
{
    [SerializationSettings(Method = SerializationMethod.RefByID, ReferenceResolverType = typeof(DatabaseManager), ReferenceResolverMethod = "GetSpellTemplate")]
    public class SpellTemplate : SpellTemplateBase
    {
        public override SpellBase Create(MapBase map, Vector3 pos)
        {
            return new Spell(this, map, pos);
        }
        public override SpellBase Create(CreatureBase caster, CreatureBase target)
        {
            return new Spell(this, caster, target);
        }
        public override SpellBase Create(CreatureBase caster, Vector3 target)
        {
            return new Spell(this, caster, target);
        }

        protected override int GetInitialID()
        {
            return DatabaseManager.Spells.GetID(this);
        }
    }
}