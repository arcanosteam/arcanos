﻿using Arcanos.Shared.Objects;
using Arcanos.Shared.Spells;

namespace Arcanos.Client.GameClient.Spells
{
    public class Aura : AuraBase
    {
        public Aura(CreatureBase owner, ulong casterGUID, SpellTemplateBase spell, int effectID) : base(owner, casterGUID, spell, effectID) { }

        protected override void MarkForRemoval() { }

        public override byte[] Serialize(string member)
        {
            return new byte[0];
        }
        public override void Deserialize(string member, byte[] data) { }
    }
}