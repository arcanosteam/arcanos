﻿using System;
using System.Text;
using Arcanos.Client.GameClient.Data;
using Arcanos.Shared.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Arcanos.Client.GameClient
{
    public static class Extensions
    {
        public static Vector2 Floor(this Vector2 v)
        {
            return new Vector2((float)Math.Floor(v.X), (float)Math.Floor(v.Y));
        }
        public static Vector2 Round(this Vector2 v)
        {
            return new Vector2((float)Math.Round(v.X), (float)Math.Round(v.Y));
        }
        public static Vector2 Ceiling(this Vector2 v)
        {
            return new Vector2((float)Math.Ceiling(v.X), (float)Math.Ceiling(v.Y));
        }
        public static Vector2 Int(this Vector2 v)
        {
            return new Vector2((int)v.X, (int)v.Y);
        }
        public static float Min(this Vector3 v)
        {
            return Math.Min(Math.Min(v.X, v.Y), v.Z);
        }
        public static float Max(this Vector3 v)
        {
            return Math.Max(Math.Max(v.X, v.Y), v.Z);
        }
        public static string ToHex(this byte[] bytes, bool upperCase)
        {
            StringBuilder result = new StringBuilder(bytes.Length * 2);

            for (int i = 0; i < bytes.Length; i++)
                result.Append(bytes[i].ToString(upperCase ? "X2" : "x2"));

            return result.ToString();
        }
        public static Texture2D GetDiffuseTexture(this Material material)
        {
            return material.TextureID == 0 ? null : DatabaseManager.TextureData[material.TextureID];
        }
        public static Texture2D GetNormalTexture(this Material material)
        {
            return material.NormalMapID == 0 ? null : DatabaseManager.TextureData[material.NormalMapID];
        }
    }
}