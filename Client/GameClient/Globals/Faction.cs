﻿using Arcanos.Client.GameClient.Data;
using Arcanos.Shared;

namespace Arcanos.Client.GameClient
{
    public class Faction : FactionBase
    {
        protected Faction() { }

        protected override int GetInitialID()
        {
            return DatabaseManager.Factions.GetID(this);
        }
    }
}