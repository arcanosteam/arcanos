﻿using Arcanos.Client.GameClient.Data;
using Arcanos.Shared.Quests;

namespace Arcanos.Client.GameClient.Quests
{
    public class Quest : QuestBase
    {
        protected override int GetInitialID()
        {
            return DatabaseManager.Quests.GetID(this);
        }
        public override QuestInstanceBase Create()
        {
            return new QuestInstance(this);
        }
    }
}