﻿using System;
using Arcanos.Client.GameClient.Data;
using Arcanos.Shared.Quests;

namespace Arcanos.Client.GameClient.Quests
{
    public class QuestInstance : QuestInstanceBase
    {
        public QuestInstance(QuestBase quest) : base(quest) { }

        public override void Deserialize(string member, byte[] data)
        {
            if (member == "Quest")
                Quest = DatabaseManager.Quests.Get(BitConverter.ToInt32(data, 0));
        }
    }
}