﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Arcanos.Client.GameClient.Data;
using Arcanos.Client.GameClient.Players;
using Arcanos.Client.GameClient.Server;
using Arcanos.Client.GameClient.World;
using Arcanos.Net;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Net;
using Arcanos.Shared.Server;

namespace Arcanos.Client.GameClient.Net
{
    public enum ConnectionState
    {
        NotRunning,
        InProgress,
        ResolvedDNS,
        Connected,
    }
    public static class ServerConnection
    {
        public static ConnectionState LoginServerConnectionState;
        public static ConnectionState WorldServerConnectionState;
        public static ConnectionManager<LoginPackets, ILoginPacket> LoginServerConnection;
        public static ConnectionManager<WorldPackets, IWorldPacket> WorldServerConnection;

        public static Player LocalPlayer;
        public static byte[] LoginToken;

        public static event Action<Exception> DNSError;

        public static event Action<Exception> LoginConnectionError;
        public static event Action LoggedIn;
        public static event Action<LoginResult> LoginFailed;

        public static event Action<Exception> WorldConnectionError;
        public static event Action<ConnectionManager<WorldPackets, IWorldPacket>> ConnectedToWorld;
        public static event Action<Player> LoggedIntoWorld;
        public static event Action<WorldServerEntry> WorldServerEntryReceived;

        public static void ConnectToLoginServer(string url, string name, string password)
        {
            if (LoginServerConnectionState != ConnectionState.NotRunning)
                return;
            ResetConnection();
            LoginServerConnectionState = ConnectionState.InProgress;
            Dns.BeginGetHostAddresses(url, iar =>
            {
                IPAddress[] addresses;
                try
                {
                    addresses = Dns.EndGetHostAddresses(iar);
                }
                catch (Exception ex)
                {
                    if (DNSError != null)
                        DNSError(ex);
                    LoginServerConnectionState = ConnectionState.NotRunning;
                    return;
                }
                if (addresses.Length == 0)
                    throw new Exception();
                LoginServerConnectionState = ConnectionState.ResolvedDNS;
                ConnectToLoginServer(addresses[0], name, password);
            }, null);
        }
        public static void ConnectToLoginServer(IPAddress ip, string name, string password)
        {
            if (LoginServerConnectionState != ConnectionState.NotRunning && LoginServerConnectionState != ConnectionState.ResolvedDNS)
                return;
            ResetConnection();
            LoginServerConnectionState = ConnectionState.InProgress;
            TcpClient client = new TcpClient();
            client.BeginConnect(ip, LoginConstants.PORT, iar =>
            {
                try
                {
                    client.EndConnect(iar);
                }
                catch (Exception ex)
                {
                    if (LoginConnectionError != null)
                        LoginConnectionError(ex);
                    LoginServerConnectionState = ConnectionState.NotRunning;
                    return;
                }
                if (!client.Connected)
                    throw new Exception();
                LoginServerConnectionState = ConnectionState.Connected;
                StartLoginServerConnection(client, name, password);
            }, null);
        }

        private static void ResetConnection()
        {
            LoginServerConnectionState = ConnectionState.NotRunning;
            WorldServerConnectionState = ConnectionState.NotRunning;
            LocalPlayer = null;
            LoginToken = null;
            DatabaseManager.Players.Clear();
            DatabaseManager.Characters.Clear();
            DatabaseManager.WorldServers.Clear();
        }
        private static void StartLoginServerConnection(TcpClient client, string name, string password)
        {
            LoginServerConnection = new ConnectionManager<LoginPackets, ILoginPacket>(client);
            LoginPacketsTable.Fill(LoginServerConnection.Reader.PacketTypes);
            LoginPacketsTable.Fill(LoginServerConnection.Writer.PacketTypes);
            LoginServerConnection.Start();

            // Protocol verification
            bool protocolVerified = false;
            LoginServerConnection.AddReadHandler(LoginPackets.SLoginProtocolResponse, (sPacket, rawData) =>
            {
                SLoginProtocolResponsePacket packet = (SLoginProtocolResponsePacket)sPacket;
                if (packet.ProtocolVersion != LoginConstants.PROTOCOL_VERSION)
                    throw new Exception("Wrong protocol version!");
                protocolVerified = true;
            });
            LoginServerConnection.Write(new CLoginProtocolQueryPacket());
            while (!protocolVerified) { Thread.Sleep(5); }

            // Logging in
            byte[] loginToken = null;
            LoginServerConnection.SetReadHandler(LoginPackets.SLoginLoginResult, (sPacket, rawData) =>
            {
                SLoginLoginResultPacket packet = (SLoginLoginResultPacket)sPacket;
                if (packet.Result != LoginResult.Ok)
                {
                    if (LoginFailed != null)
                        LoginFailed(packet.Result);
                    ResetConnection();
                    return;
                }
                loginToken = packet.LoginToken;
            });
            LoginServerConnection.SetReadHandler(LoginPackets.SWorldServerEntry, (sPacket, rawData) =>
            {
                SWorldServerEntryPacket packet = (SWorldServerEntryPacket)sPacket;
                WorldServerEntry entry = new WorldServerEntry(packet.ServerName, packet.URL);
                DatabaseManager.WorldServers.Add(packet.ID, entry);
                if (WorldServerEntryReceived != null)
                    WorldServerEntryReceived(entry);
            });
            LoginServerConnection.Write(new CLoginLoginRequestPacket { UsernameOrEmail = name, Password = password });
            while (loginToken == null) { Thread.Sleep(5); }

            LoginToken = loginToken;

            if (LoggedIn != null)
                LoggedIn();
        }

        public static void ConnectToWorldServer(WorldServerEntry entry)
        {
            ConnectToWorldServer(entry.URL);
        }
        public static void ConnectToWorldServer(string url)
        {
            if (WorldServerConnectionState != ConnectionState.NotRunning)
                return;
            if (LoginToken == null)
                return;
            WorldServerConnectionState = ConnectionState.InProgress;
            Dns.BeginGetHostAddresses(url, iar =>
            {
                IPAddress[] addresses;
                try
                {
                    addresses = Dns.EndGetHostAddresses(iar);
                }
                catch (Exception ex)
                {
                    if (DNSError != null)
                        DNSError(ex);
                    WorldServerConnectionState = ConnectionState.NotRunning;
                    return;
                }
                if (addresses.Length == 0)
                    throw new Exception();
                WorldServerConnectionState = ConnectionState.ResolvedDNS;
                ConnectToWorldServer(addresses[0]);
            }, null);
        }
        public static void ConnectToWorldServer(IPAddress ip)
        {
            if (WorldServerConnectionState != ConnectionState.NotRunning && WorldServerConnectionState != ConnectionState.ResolvedDNS)
                return;
            if (LoginToken == null)
                return;
            WorldServerConnectionState = ConnectionState.InProgress;
            TcpClient client = new TcpClient();
            client.BeginConnect(ip, WorldConstants.PORT, iar =>
            {
                try
                {
                    client.EndConnect(iar);
                }
                catch (Exception ex)
                {
                    if (WorldConnectionError != null)
                        WorldConnectionError(ex);
                    WorldServerConnectionState = ConnectionState.NotRunning;
                    return;
                }
                if (!client.Connected)
                    throw new Exception();
                WorldServerConnectionState = ConnectionState.Connected;
                StartWorldServerConnection(client);
            }, null);
        }

        private static void StartWorldServerConnection(TcpClient client)
        {
            if (!new PlayerSession().Connect(client))
                return;
            WorldServerConnection = WorldManager.PlayerSession.Connection;

            // Protocol verification
            bool protocolVerified = false;
            WorldServerConnection.AddReadHandler(WorldPackets.SWorldProtocolResponse, (sPacket, rawData) =>
            {
                SWorldProtocolResponsePacket packet = (SWorldProtocolResponsePacket)sPacket;
                if (packet.ProtocolVersion != WorldConstants.PROTOCOL_VERSION)
                    throw new Exception("Wrong protocol version!");
                protocolVerified = true;
            });
            WorldServerConnection.Write(new CWorldProtocolQueryPacket());
            while (!protocolVerified) { Thread.Sleep(5); }

            if (ConnectedToWorld != null)
                ConnectedToWorld(WorldServerConnection);

            // Logging in
            int playerID = 0;
            WorldServerConnection.SetReadHandler(WorldPackets.SWorldLoginResult, (sPacket, rawData) =>
            {
                SWorldLoginResultPacket packet = (SWorldLoginResultPacket)sPacket;
                if (packet.Result != WorldLoginResult.Ok)
                    throw new Exception();
                playerID = packet.PlayerID;
            });
            WorldServerConnection.Write(new CWorldLoginRequestPacket { LoginToken = LoginToken, Locale = Localization.CurrentLocale });
            while (playerID == 0) { Thread.Sleep(5); }

            DatabaseManager.Players.Get(playerID, player =>
            {
                WorldManager.PlayerSession.SetPlayer(player);
                WorldManager.Player = LocalPlayer = player;
                if (LoggedIntoWorld != null)
                    LoggedIntoWorld(LocalPlayer);
            });
        }

        public static void Disconnect()
        {
            if (LoginServerConnectionState != ConnectionState.NotRunning && LoginServerConnection != null)
                LoginServerConnection.Stop();
            if (WorldServerConnectionState != ConnectionState.NotRunning && WorldServerConnection != null)
                WorldServerConnection.Stop();
            ResetConnection();
        }
    }
}