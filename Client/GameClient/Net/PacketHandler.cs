﻿using System;
using System.Collections.Generic;
using Arcanos.Client.GameClient.Net.Handlers;
using Arcanos.Shared.Net;
using Arcanos.Shared.Objects;

namespace Arcanos.Client.GameClient.Net
{
    public delegate void PacketHandler(PlayerSession session, IWorldPacket packet);
    public delegate void PacketHandlerWithRawData(PlayerSession session, IWorldPacket packet, byte[] rawData);
    public static class PacketHandling
    {
        [Flags]
        private enum HandlingFlags : byte
        {
            ThreadSafe  = 0x01,
            InGame      = 0x02,
        }
        private struct HandlerDef
        {
            public readonly WorldPackets Type;
            public readonly PacketHandler Handler;
            public readonly PacketHandlerWithRawData HandlerWithRawData;
            public readonly HandlingFlags Flags;

            public HandlerDef(WorldPackets type, PacketHandler handler)
            {
                Type = type;
                Handler = handler;
                HandlerWithRawData = null;
                Flags = 0;
            }
            public HandlerDef(WorldPackets type, PacketHandlerWithRawData handler)
            {
                Type = type;
                Handler = null;
                HandlerWithRawData = handler;
                Flags = 0;
            }
            public HandlerDef(WorldPackets type, PacketHandler handler, HandlingFlags flags)
            {
                Type = type;
                Handler = handler;
                HandlerWithRawData = null;
                Flags = flags;
            }
            public HandlerDef(WorldPackets type, PacketHandlerWithRawData handler, HandlingFlags flags)
            {
                Type = type;
                Handler = null;
                HandlerWithRawData = handler;
                Flags = flags;
            }
        }

        private static readonly List<HandlerDef> handlers = new List<HandlerDef>
        {
            new HandlerDef(WorldPackets.SCharacterResponse,             CharacterHandlers.HandleSCharacterResponsePacket, HandlingFlags.ThreadSafe),
            new HandlerDef(WorldPackets.SCharacterTemplateResponse,     CharacterHandlers.HandleSCharacterTemplateResponsePacket, HandlingFlags.ThreadSafe),
            new HandlerDef(WorldPackets.SCharacterCreateResult,         CharacterHandlers.HandleSCharacterCreateResultPacket),
            new HandlerDef(WorldPackets.SCharacterCreateTemplateResponse, CharacterHandlers.HandleSCharacterCreateTemplateResponsePacket),
            new HandlerDef(WorldPackets.SKnowsSpell,                    CharacterHandlers.HandleSKnowsSpellPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SExperienceChanged,             CharacterHandlers.HandleSExperienceChangedPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SLevelUp,                       CharacterHandlers.HandleSLevelUpPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SStatsData,                     CharacterHandlers.HandleSStatsDataPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SBaseStatUpdate,                CharacterHandlers.HandleSBaseStatUpdatePacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SModifiedStatUpdate,            CharacterHandlers.HandleSModifiedStatUpdatePacket, HandlingFlags.InGame),

            new HandlerDef(WorldPackets.SChatMessage,                   ChatHandlers.HandleSChatMessagePacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SChatMessageResult,             ChatHandlers.HandleSChatMessageResultPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SChatChannelJoinResult,         ChatHandlers.HandleSChatChannelJoinResultPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SChatChannelLeaveResult,        ChatHandlers.HandleSChatChannelLeaveResultPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SWorldSystemMessage,            ChatHandlers.HandleSSystemMessagePacket, HandlingFlags.InGame),

            new HandlerDef(WorldPackets.SCombatInflictedDamage,         CombatHandlers.HandleSCombatInflictedDamagePacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SCombatHealed,                  CombatHandlers.HandleSCombatHealedPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SCombatPerformedMeleeAttack,    CombatHandlers.HandleSCombatPerformedMeleeAttackPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SCombatPerformedSpellAttack,    CombatHandlers.HandleSCombatPerformedSpellAttackPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SCombatSpellCasted,             CombatHandlers.HandleSCombatSpellCastedPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SCombatSpellCastingStarted,     CombatHandlers.HandleSCombatSpellCastingStartedPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SCombatSpellCastingFailed,      CombatHandlers.HandleSCombatSpellCastingFailedPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SHasAura,                       CombatHandlers.HandleSHasAuraPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SAppliedAura,                   CombatHandlers.HandleSAppliedAuraPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SRemovedAura,                   CombatHandlers.HandleSRemovedAuraPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SAreaEffectPlaced,              CombatHandlers.HandleSAreaEffectPlacedPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SAreaEffectRemoved,             CombatHandlers.HandleSAreaEffectRemovedPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SDuelRequest,                   CombatHandlers.HandleSDuelRequestPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SDuelStatus,                    CombatHandlers.HandleSDuelStatusPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SDuelParticipantStatus,         CombatHandlers.HandleSDuelParticipantStatusPacket, HandlingFlags.InGame),

            new HandlerDef(WorldPackets.SCreatureEnteredCombat,         CreatureHandlers.HandleSCreatureEnteredCombatPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SCreatureEvaded,                CreatureHandlers.HandleSCreatureEvadedPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SCreatureHealthUpdate,          CreatureHandlers.HandleSCreatureHealthUpdatePacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SCreatureUsesPowerUpdate,       CreatureHandlers.HandleSCreatureUsesPowerUpdatePacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SCreaturePowerUpdate,           CreatureHandlers.HandleSCreaturePowerUpdatePacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SCreatureStateUpdate,           CreatureHandlers.HandleSCreatureStateUpdatePacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SCreatureTargetUpdate,          CreatureHandlers.HandleSCreatureTargetUpdatePacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SCreatureMovementTypeUpdate,    CreatureHandlers.HandleSCreatureMovementTypeUpdatePacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SCreatureFactionUpdate,         CreatureHandlers.HandleSCreatureFactionUpdatePacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SCreatureModelUpdate,           CreatureHandlers.HandleSCreatureModelUpdatePacket, HandlingFlags.InGame),

            new HandlerDef(WorldPackets.SWorldObjectTemplateResponse,   DataHandlers.HandleSWorldObjectTemplateResponsePacket, HandlingFlags.ThreadSafe),
            new HandlerDef(WorldPackets.SCreatureTemplateResponse,      DataHandlers.HandleSCreatureTemplateResponsePacket, HandlingFlags.ThreadSafe),
            new HandlerDef(WorldPackets.SSpellTemplateResponse,         DataHandlers.HandleSSpellTemplateResponsePacket, HandlingFlags.ThreadSafe),
            new HandlerDef(WorldPackets.SSpellVisualResponse,           DataHandlers.HandleSSpellVisualResponsePacket, HandlingFlags.ThreadSafe),
            new HandlerDef(WorldPackets.SItemTemplateResponse,          DataHandlers.HandleSItemTemplateResponsePacket, HandlingFlags.ThreadSafe),
            new HandlerDef(WorldPackets.SQuestResponse,                 DataHandlers.HandleSQuestResponsePacket, HandlingFlags.ThreadSafe),
            new HandlerDef(WorldPackets.SMapTemplateResponse,           DataHandlers.HandleSMapTemplateResponsePacket, HandlingFlags.ThreadSafe),
            new HandlerDef(WorldPackets.SFactionResponse,               DataHandlers.HandleSFactionResponsePacket, HandlingFlags.ThreadSafe),
            new HandlerDef(WorldPackets.SEnvironmentResponse,           DataHandlers.HandleSEnvironmentResponsePacket, HandlingFlags.ThreadSafe),
            new HandlerDef(WorldPackets.STextResponse,                  DataHandlers.HandleSTextResponsePacket, HandlingFlags.ThreadSafe),
            new HandlerDef(WorldPackets.SGameStringsResponse,           DataHandlers.HandleSGameStringsResponsePacket, HandlingFlags.ThreadSafe),
            new HandlerDef(WorldPackets.SMaterialResponse,              DataHandlers.HandleSMaterialResponsePacket, HandlingFlags.ThreadSafe),
            new HandlerDef(WorldPackets.STextureInfoResponse,           DataHandlers.HandleSTextureInfoResponsePacket, HandlingFlags.ThreadSafe),
            new HandlerDef(WorldPackets.SModelResponse,                 DataHandlers.HandleSModelResponsePacket, HandlingFlags.ThreadSafe),
            new HandlerDef(WorldPackets.SParticleSystemResponse,        DataHandlers.HandleSParticleSystemResponsePacket, HandlingFlags.ThreadSafe),

            new HandlerDef(WorldPackets.SStartDialogResult,             DialogHandlers.HandleSStartDialogResultPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SEndDialog,                     DialogHandlers.HandleSEndDialogPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SChangedPage,                   DialogHandlers.HandleSChangedPagePacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SPageOption,                    DialogHandlers.HandleSPageOptionPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SPageQuestOption,               DialogHandlers.HandleSPageQuestOptionPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SSelectOptionResult,            DialogHandlers.HandleSSelectOptionResultPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SDialogQuestAcceptPage,         DialogHandlers.HandleSDialogQuestAcceptPagePacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SDialogQuestCompletePage,       DialogHandlers.HandleSDialogQuestCompletePagePacket, HandlingFlags.InGame),

            new HandlerDef(WorldPackets.SCharacterChangedGroup,         GroupHandlers.HandleSCharacterChangedGroupPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SGroupInviteResult,             GroupHandlers.HandleSGroupInviteResultPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SGroupKickResult,               GroupHandlers.HandleSGroupKickResultPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SGroupInvitation,               GroupHandlers.HandleSGroupInvitationPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SGroupMemberJoined,             GroupHandlers.HandleSGroupMemberJoinedPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SGroupMemberLeft,               GroupHandlers.HandleSGroupMemberLeftPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SGroupLeaderChanged,            GroupHandlers.HandleSGroupLeaderChangedPacket, HandlingFlags.InGame),

            new HandlerDef(WorldPackets.SMoneyChanged,                  ItemHandlers.HandleSMoneyChangedPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SBagChanged,                    ItemHandlers.HandleSBagChangedPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SBagSlotUpdated,                ItemHandlers.HandleSBagSlotUpdatedPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SItemCountUpdated,              ItemHandlers.HandleSItemCountUpdatedPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SItemDataResponse,              ItemHandlers.HandleSItemDataResponsePacket, HandlingFlags.InGame | HandlingFlags.ThreadSafe),
            new HandlerDef(WorldPackets.SMoveItemResult,                ItemHandlers.HandleSMoveItemResultPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SDivideItemResult,              ItemHandlers.HandleSDivideItemResultPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SDestroyItemResult,             ItemHandlers.HandleSDestroyItemResultPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SUseItemResult,                 ItemHandlers.HandleSUseItemResultPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SEquipItemResult,               ItemHandlers.HandleSEquipItemResultPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SUnequipItemResult,             ItemHandlers.HandleSUnequipItemResultPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SItemsAdded,                    ItemHandlers.HandleSItemsAddedPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SItemsRemoved,                  ItemHandlers.HandleSItemsRemovedPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SCancelLooting,                 ItemHandlers.HandleSCancelLootingPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SLootResult,                    ItemHandlers.HandleSLootResultPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SLootResultItem,                ItemHandlers.HandleSLootResultItemPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SLootItemResult,                ItemHandlers.HandleSLootItemResultPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SLootItemRemoved,               ItemHandlers.HandleSLootItemRemovedPacket, HandlingFlags.InGame),

            new HandlerDef(WorldPackets.SChangeMap,                     MapHandlers.HandleSChangeMapPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SChangeEnvironment,             MapHandlers.HandleSChangeEnvironmentPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SObjectEnteredVisibility,       MapHandlers.HandleSObjectEnteredVisibilityPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SObjectLeftVisibility,          MapHandlers.HandleSObjectLeftVisibilityPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SObjectUpdateLocation,          MapHandlers.HandleSObjectUpdateLocationPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SObjectLootRecipientUpdate,     MapHandlers.HandleSObjectLootRecipientUpdatePacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SObjectFlagsUpdate,             MapHandlers.HandleSObjectFlagsUpdatePacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SObjectLoadingFinalized,        MapHandlers.HandleSObjectLoadingFinalizedPacket, HandlingFlags.InGame),

            new HandlerDef(WorldPackets.CMovementUpdate,                MovementHandlers.HandleCMovementUpdatePacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SCreatureMovementUpdate,        MovementHandlers.HandleSCreatureMovementUpdatePacket, HandlingFlags.InGame),

            new HandlerDef(WorldPackets.SPlayerResponse,                PlayerHandlers.HandleSPlayerResponsePacket, HandlingFlags.ThreadSafe),
            new HandlerDef(WorldPackets.SCharacterListStatusResponse,   PlayerHandlers.HandleSCharacterListStatusResponsePacket),
            new HandlerDef(WorldPackets.SCharacterStatusResponse,       PlayerHandlers.HandleSCharacterStatusResponsePacket),
            new HandlerDef(WorldPackets.SWorldEnterResponse,            PlayerHandlers.HandleSWorldEnterResponsePacket),

            new HandlerDef(WorldPackets.SQuestAdded,                    QuestHandlers.HandleSQuestAddedPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SQuestRemoved,                  QuestHandlers.HandleSQuestRemovedPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SQuestCompleted,                QuestHandlers.HandleSQuestCompletedPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SQuestObjectiveUpdate,          QuestHandlers.HandleSQuestObjectiveUpdatePacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SAddQuestResult,                QuestHandlers.HandleSAddQuestResultPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SRemoveQuestResult,             QuestHandlers.HandleSRemoveQuestResultPacket, HandlingFlags.InGame),
            new HandlerDef(WorldPackets.SCompleteQuestResult,           QuestHandlers.HandleSCompleteQuestResultPacket, HandlingFlags.InGame),
        };
        private static readonly int handlersCount = handlers.Count;

        public static bool IsThreadSafe(WorldPackets type)
        {
            for (int i = 0; i < handlersCount; i++)
                if (handlers[i].Type == type)
                    return (handlers[i].Flags & HandlingFlags.ThreadSafe) != 0;
            return false;
        }
        public static bool IsInGame(WorldPackets type)
        {
            for (int i = 0; i < handlersCount; i++)
                if (handlers[i].Type == type)
                    return (handlers[i].Flags & HandlingFlags.InGame) != 0;
            return false;
        }
        public static bool Handle(PlayerSession session, WorldPackets type, IWorldPacket packet, byte[] rawData)
        {
            for (int i = 0; i < handlersCount; i++)
                if (handlers[i].Type == type)
                {
                    HandlerDef handler = handlers[i];
                    if (handler.Handler != null)
                        handler.Handler(session, packet);
                    else if (handler.HandlerWithRawData != null)
                        handler.HandlerWithRawData(session, packet, rawData);
                    return true;
                }
            return false;
        }

        public static bool GetOrQueue(WorldPackets type, IWorldPacket packet, ulong guid, out CreatureBase creature)
        {
            creature = ObjectManager.GetCreature(guid);
            if (creature == null)
            {
                ObjectManager.EnqueuePacket(guid, type, packet);
                return true;
            }
            return false;
        }
    }
}