﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading;
using Arcanos.Client.GameClient.Objects;
using Arcanos.Client.GameClient.Players;
using Arcanos.Client.GameClient.Timing;
using Arcanos.Client.GameClient.World;
using Arcanos.Net;
using Arcanos.Shared.Net;
using Arcanos.Utilities;
using Arcanos.Utilities.Debug;

namespace Arcanos.Client.GameClient.Net
{
    public class PlayerSession : PlayerSessionBase
    {
        private struct PacketQueueEntry
        {
            public WorldPackets Type;
            public IWorldPacket Packet;
            public byte[] RawData;
        }

        public new Player Player
        {
            get { return (Player)base.Player; }
            set { base.Player = value; }
        }

        public ConnectionManager<WorldPackets, IWorldPacket> Connection;
        public bool Connected { get; protected set; }

        public event Action CharacterListStatusLoaded;
        public event Action<int, Character> CharacterLoaded;
        public event Action CharacterListLoaded;
        public event Action EnteredWorld;

        public PlayerSession()
        {
            WorldManager.PlayerSession = this;
        }

        public void SetPlayer(Player player)
        {
            Player = player;
            player.Session = this;
        }
        public void SelectCharacter(Character character)
        {
            D.NotNull(character, "Character you want to select is null");
            D.Assert(Player.Characters.Contains(character), "Player account does not contain the character you want to select");
            SendPacket(new CWorldEnterRequestPacket
            {
                CharacterIndex = (byte)Player.Characters.IndexOf(character)
            });
        }
        public void EnterWorld(Map map, PlayerCreature pc)
        {
            if (EnteredWorld != null)
                EnteredWorld();
        }
        public void ForceLogout()
        {
            SendPacket(new CForceLogoutPacket());
        }

        public bool Connect(TcpClient client)
        {
            if (!client.Connected)
                return false;
            Connection = new ConnectionManager<WorldPackets, IWorldPacket>(client);
            WorldPacketsTable.Fill(Connection.Reader.PacketTypes);
            WorldPacketsTable.Fill(Connection.Writer.PacketTypes);
            Connection.Reader.ImmediateProcessing = true;
            Connection.PacketReceived += ProcessPacket;
            Connection.Start();
            Connected = true;
            return true;
        }

        private readonly Queue<PacketQueueEntry> packetQueue = new Queue<PacketQueueEntry>();
        private readonly object packetQueueLock = new object();
        public override void ProcessPackets()
        {
            //if (Connected)
            //    Connection.Reader.ProcessQueue();
            while (true)
            {
                bool queueEmpty;
                lock (packetQueueLock)
                    queueEmpty = packetQueue.Count == 0;
                if (queueEmpty)
                    break;

                PacketQueueEntry entry;
                lock (packetQueueLock)
                    entry = packetQueue.Dequeue();
                if (!PacketHandling.Handle(this, entry.Type, entry.Packet, entry.RawData))
                    Logger.Trace(LogCategory.Packets, "Received packet of type {0} was unhandled", entry.Type);
            }
        }
        public override void ProcessPacket(WorldPackets type, IWorldPacket packet, byte[] rawData)
        {
            if (!WorldManager.IsCharacterInGame && PacketHandling.IsInGame(type))
            {
                Waiter.WaitFor(() => WorldManager.IsCharacterInGame, () => ProcessPacket(type, packet, rawData));
                return;
            }
            if (PacketHandling.IsThreadSafe(type))
            {
                new Thread(() =>
                {
                    if (!PacketHandling.Handle(this, type, packet, rawData))
                        Logger.Trace(LogCategory.Packets, "Received packet of type {0} was unhandled", type);
                }).Start();
            }
            else
                lock (packetQueueLock)
                    packetQueue.Enqueue(new PacketQueueEntry
                    {
                        Type = type,
                        Packet = packet,
                        RawData = rawData,
                    });
        }

        public void SendPacket(IWorldPacket packet)
        {
            Connection.Write(packet);
        }
        public void SendPacket(IWorldPacket packet, byte[] rawData)
        {
            Connection.Write(packet, rawData);
        }

        internal void InvokeCharacterListStatusLoaded()
        {
            if (CharacterListStatusLoaded != null)
                CharacterListStatusLoaded();
        }
        internal void InvokeCharacterLoaded(int index, Character character)
        {
            if (CharacterLoaded != null)
                CharacterLoaded(index, character);
        }
        internal void InvokeCharacterListLoaded()
        {
            if (CharacterListLoaded != null)
                CharacterListLoaded();
        }
    }
}