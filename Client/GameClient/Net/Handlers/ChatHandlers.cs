﻿using System.Text;
using Arcanos.Client.GameClient.Chat;
using Arcanos.Client.GameClient.Data;
using Arcanos.Client.GameClient.UI;
using Arcanos.Shared.Chat;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Net;
using Arcanos.Shared.Objects;

namespace Arcanos.Client.GameClient.Net.Handlers
{
    public static class ChatHandlers
    {
        public static void HandleSChatMessagePacket(PlayerSession session, IWorldPacket sPacket, byte[] rawData)
        {
            SChatMessagePacket packet = (SChatMessagePacket)sPacket;
            // TODO: Name cache
            string senderName = "???";
            WorldObjectBase wo = ObjectManager.GetWorldObject(packet.SenderGUID);
            if (wo != null)
                senderName = wo.Name;
            UIManager.RaiseEvent(UIEvents.ChatMessage, null, packet.SenderGUID, packet.ChannelID, senderName, packet.Timestamp, packet.Flags, Encoding.UTF8.GetString(rawData));
        }
        public static void HandleSChatMessageResultPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SChatMessageResultPacket packet = (SChatMessageResultPacket)sPacket;

            if (packet.Result != ChatMessageResult.Ok)
            {
                UIManager.RaiseEvent(UIEvents.GameError, null, Localization.Game.ErrorChatMessageResult[(byte)packet.Result]);
                return;
            }

            UIManager.RaiseEvent(UIEvents.ChatMessageSent, null);
        }
        public static void HandleSChatChannelJoinResultPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SChatChannelJoinResultPacket packet = (SChatChannelJoinResultPacket)sPacket;

            if (packet.Result != ChatChannelJoinResult.Ok)
            {
                UIManager.RaiseEvent(UIEvents.GameError, null, Localization.Game.ErrorChatChannelJoinResult[(byte)packet.Result]);
                return;
            }

            ChatManager.Join(ChatManager.CreateChannel(
                packet.ChannelID,
                packet.ChannelNameID == 0 ? null : DatabaseManager.Text.Get(packet.ChannelNameID),
                packet.ChannelAliasID == 0 ? null : DatabaseManager.Text.Get(packet.ChannelAliasID),
                packet.CustomChannelName,
                packet.ChannelColor));
        }
        public static void HandleSChatChannelLeaveResultPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SChatChannelLeaveResultPacket packet = (SChatChannelLeaveResultPacket)sPacket;

            if (packet.Result != ChatChannelLeaveResult.Ok)
            {
                UIManager.RaiseEvent(UIEvents.GameError, null, Localization.Game.ErrorChatChannelLeaveResult[(byte)packet.Result]);
                return;
            }

            ChatManager.Leave(packet.ChannelID);
        }
        public static void HandleSSystemMessagePacket(PlayerSession session, IWorldPacket sPacket)
        {
            SWorldSystemMessagePacket packet = (SWorldSystemMessagePacket)sPacket;

            ChatManager.PrintMessage(packet.Text);
        }
    }
}