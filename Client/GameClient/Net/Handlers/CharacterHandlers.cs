﻿using System;
using Arcanos.Client.GameClient.Data;
using Arcanos.Client.GameClient.Objects;
using Arcanos.Client.GameClient.Players;
using Arcanos.Client.GameClient.UI;
using Arcanos.Client.GameClient.World;
using Arcanos.Shared;
using Arcanos.Shared.Data;
using Arcanos.Shared.Net;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Players;

namespace Arcanos.Client.GameClient.Net.Handlers
{
    public static class CharacterHandlers
    {
        public static void HandleSCharacterResponsePacket(PlayerSession session, IWorldPacket sPacket, byte[] rawData)
        {
            SCharacterResponsePacket packet = (SCharacterResponsePacket)sPacket;
            Character character = Serialization.Deserialize<Character>(rawData, null, typeof(CharacterBase));
            DatabaseManager.Characters.Add(packet.ID, character);
        }
        public static void HandleSCharacterTemplateResponsePacket(PlayerSession session, IWorldPacket sPacket, byte[] rawData)
        {
            SCharacterTemplateResponsePacket packet = (SCharacterTemplateResponsePacket)sPacket;
            PlayerCreatureBase.FriendlyTargetDecalTextureID = packet.FriendlyTargetDecalTextureID;
            PlayerCreatureBase.NeutralTargetDecalTextureID = packet.NeutralTargetDecalTextureID;
            PlayerCreatureBase.HostileTargetDecalTextureID = packet.HostileTargetDecalTextureID;
            WorldManager.CharacterTemplatesCache[packet.ID] = Serialization.Deserialize<CreatureTemplate>(rawData, null, typeof(CreatureTemplateBase));
        }
        public static void HandleSCharacterCreateResultPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SCharacterCreateResultPacket packet = (SCharacterCreateResultPacket)sPacket;

            UIManager.RaiseEvent(UIEvents.CharacterCreateResult, null, packet.Result, packet.ModelIndexCount);
        }
        public static void HandleSCharacterCreateTemplateResponsePacket(PlayerSession session, IWorldPacket sPacket, byte[] rawData)
        {
            SCharacterCreateTemplateResponsePacket packet = (SCharacterCreateTemplateResponsePacket)sPacket;

            CreatureTemplate template = rawData.Length == 0 ? null : Serialization.Deserialize<CreatureTemplate>(rawData, null, typeof(CreatureTemplateBase));

            UIManager.RaiseEvent(UIEvents.CharacterCreateTemplateResponse, template, packet.Race, packet.Class, packet.ModelIndex, packet.Spells, packet.Items, packet.Equipment);
        }
        public static void HandleSKnowsSpellPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SKnowsSpellPacket packet = (SKnowsSpellPacket)sPacket;
            WorldManager.PlayerCreature.SpellBook.AddSpell(DatabaseManager.Spells.Get(packet.SpellID));

            UIManager.RaiseEvent(UIEvents.SpellLearned, WorldManager.PlayerCreature, packet.SpellID);
        }
        public static void HandleSExperienceChangedPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SExperienceChangedPacket packet = (SExperienceChangedPacket)sPacket;

            WorldManager.Player.CurrentCharacter.XP = packet.XP;
            WorldManager.Player.CurrentCharacter.XPToLevel = packet.XPToLevel;
            UIManager.RaiseEvent(UIEvents.ExperienceChanged, null, packet.XP, packet.XPToLevel);
        }
        public static void HandleSLevelUpPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SLevelUpPacket packet = (SLevelUpPacket)sPacket;
            CreatureBase creature;
            if (PacketHandling.GetOrQueue(WorldPackets.SLevelUp, packet, packet.GUID, out creature))
                return;

            creature.Level = packet.Level;
            if (creature.Type.IsPlayer())
                (creature as PlayerCreature).Character.Level = packet.Level;

            UIManager.RaiseEvent(UIEvents.CreatureLeveledUp, creature, packet.GUID, packet.Level);
            if (creature == WorldManager.PlayerCreature)
                UIManager.RaiseEvent(UIEvents.LevelUp, WorldManager.PlayerCreature, packet.Level);
        }
        public static void HandleSStatsDataPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SStatsDataPacket packet = (SStatsDataPacket)sPacket;

            Array.Copy(packet.BaseValues, WorldManager.Player.CurrentCharacter.Stats.BaseValues, (byte)Stat.Max);
            Array.Copy(packet.ModifiedValues, WorldManager.Player.CurrentCharacter.Stats.ModifiedValues, (byte)Stat.Max);
            WorldManager.Player.CurrentCharacter.Stats.RecalcModifiers();

            UIManager.RaiseEvent(UIEvents.StatsUpdated, WorldManager.PlayerCreature);
        }
        public static void HandleSBaseStatUpdatePacket(PlayerSession session, IWorldPacket sPacket)
        {
            SBaseStatUpdatePacket packet = (SBaseStatUpdatePacket)sPacket;

            WorldManager.Player.CurrentCharacter.Stats.SetBase(packet.Stat, packet.Value);

            UIManager.RaiseEvent(UIEvents.StatsUpdated, WorldManager.PlayerCreature);
        }
        public static void HandleSModifiedStatUpdatePacket(PlayerSession session, IWorldPacket sPacket)
        {
            SModifiedStatUpdatePacket packet = (SModifiedStatUpdatePacket)sPacket;

            WorldManager.Player.CurrentCharacter.Stats.SetModified(packet.Stat, packet.Value);

            UIManager.RaiseEvent(UIEvents.StatsUpdated, WorldManager.PlayerCreature);
        }
    }
}