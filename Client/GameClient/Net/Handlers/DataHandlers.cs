﻿using Arcanos.Client.GameClient.Data;
using Arcanos.Client.GameClient.Items;
using Arcanos.Client.GameClient.Objects;
using Arcanos.Client.GameClient.Quests;
using Arcanos.Client.GameClient.Spells;
using Arcanos.Client.GameClient.World;
using Arcanos.Shared;
using Arcanos.Shared.Data;
using Arcanos.Shared.Graphics;
using Arcanos.Shared.Items;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Net;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Quests;
using Arcanos.Shared.Spells;
using Arcanos.Shared.World;

namespace Arcanos.Client.GameClient.Net.Handlers
{
    public static class DataHandlers
    {
        public static void HandleSWorldObjectTemplateResponsePacket(PlayerSession session, IWorldPacket sPacket, byte[] rawData)
        {
            SWorldObjectTemplateResponsePacket packet = (SWorldObjectTemplateResponsePacket)sPacket;
            WorldObjectTemplate template = Serialization.Deserialize<WorldObjectTemplate>(rawData, null, typeof(WorldObjectTemplateBase));
            DatabaseManager.WorldObjects.Add(packet.ID, template);
        }
        public static void HandleSCreatureTemplateResponsePacket(PlayerSession session, IWorldPacket sPacket, byte[] rawData)
        {
            SCreatureTemplateResponsePacket packet = (SCreatureTemplateResponsePacket)sPacket;
            CreatureTemplate template = Serialization.Deserialize<CreatureTemplate>(rawData, null, typeof(CreatureTemplateBase));
            DatabaseManager.Creatures.Add(packet.ID, template);
        }
        public static void HandleSSpellTemplateResponsePacket(PlayerSession session, IWorldPacket sPacket, byte[] rawData)
        {
            SSpellTemplateResponsePacket packet = (SSpellTemplateResponsePacket)sPacket;
            SpellTemplate spell = Serialization.Deserialize<SpellTemplate>(rawData, null, typeof(SpellTemplateBase));
            DatabaseManager.Spells.Add(packet.ID, spell);
        }
        public static void HandleSSpellVisualResponsePacket(PlayerSession session, IWorldPacket sPacket, byte[] rawData)
        {
            SSpellVisualResponsePacket packet = (SSpellVisualResponsePacket)sPacket;
            SpellVisual visual = Serialization.Deserialize<SpellVisual>(rawData, null);
            DatabaseManager.SpellVisuals.Add(packet.ID, visual);
        }
        public static void HandleSItemTemplateResponsePacket(PlayerSession session, IWorldPacket sPacket, byte[] rawData)
        {
            SItemTemplateResponsePacket packet = (SItemTemplateResponsePacket)sPacket;
            ItemTemplate item = Serialization.Deserialize<ItemTemplate>(rawData, null, typeof(ItemTemplateBase));
            DatabaseManager.Items.Add(packet.ID, item);
        }
        public static void HandleSQuestResponsePacket(PlayerSession session, IWorldPacket sPacket, byte[] rawData)
        {
            SQuestResponsePacket packet = (SQuestResponsePacket)sPacket;
            Quest quest = Serialization.Deserialize<Quest>(rawData, null, typeof(QuestBase));
            DatabaseManager.Quests.Add(packet.ID, quest);
        }
        public static void HandleSMapTemplateResponsePacket(PlayerSession session, IWorldPacket sPacket, byte[] rawData)
        {
            SMapTemplateResponsePacket packet = (SMapTemplateResponsePacket)sPacket;
            MapTemplate template = Serialization.Deserialize<MapTemplate>(rawData, null, typeof(MapTemplateBase));
            DatabaseManager.Maps.Add(packet.ID, template);
        }
        public static void HandleSFactionResponsePacket(PlayerSession session, IWorldPacket sPacket, byte[] rawData)
        {
            SFactionResponsePacket packet = (SFactionResponsePacket)sPacket;
            Faction faction = Serialization.Deserialize<Faction>(rawData, null, typeof(FactionBase));
            DatabaseManager.Factions.Add(packet.ID, faction);
        }
        public static void HandleSEnvironmentResponsePacket(PlayerSession session, IWorldPacket sPacket, byte[] rawData)
        {
            SEnvironmentResponsePacket packet = (SEnvironmentResponsePacket)sPacket;
            Environment env = Serialization.Deserialize<Environment>(rawData, null, typeof(EnvironmentBase));
            DatabaseManager.Environments.Add(packet.ID, env);
        }
        public static void HandleSTextResponsePacket(PlayerSession session, IWorldPacket sPacket, byte[] rawData)
        {
            STextResponsePacket packet = (STextResponsePacket)sPacket;
            string text = Serialization.Deserialize<string>(rawData, null);
            DatabaseManager.Text.Get(packet.ID).LocalizedStrings[(int)Localization.CurrentLocale] = text;
        }
        public static void HandleSGameStringsResponsePacket(PlayerSession session, IWorldPacket sPacket, byte[] rawData)
        {
            //SGameStringsResponsePacket packet = (SGameStringsResponsePacket)sPacket;
            Localization.Game = Serialization.Deserialize(rawData, Localization.Game);
        }
        public static void HandleSMaterialResponsePacket(PlayerSession session, IWorldPacket sPacket, byte[] rawData)
        {
            SMaterialResponsePacket packet = (SMaterialResponsePacket)sPacket;
            Material material = Serialization.Deserialize<Material>(rawData, null, typeof(Material));
            DatabaseManager.Materials.Add(packet.ID, material);
        }
        public static void HandleSTextureInfoResponsePacket(PlayerSession session, IWorldPacket sPacket, byte[] rawData)
        {
            STextureInfoResponsePacket packet = (STextureInfoResponsePacket)sPacket;
            TextureInfo textureInfo = Serialization.Deserialize<TextureInfo>(rawData, null);
            DatabaseManager.Textures.Add(packet.ID, textureInfo);
        }
        public static void HandleSModelResponsePacket(PlayerSession session, IWorldPacket sPacket, byte[] rawData)
        {
            SModelResponsePacket packet = (SModelResponsePacket)sPacket;
            Model model = Serialization.Deserialize<Model>(rawData, null);
            DatabaseManager.Models.Add(packet.ID, model);
        }
        public static void HandleSParticleSystemResponsePacket(PlayerSession session, IWorldPacket sPacket, byte[] rawData)
        {
            SParticleSystemResponsePacket packet = (SParticleSystemResponsePacket)sPacket;
            ParticleSystem particleSystem = Serialization.Deserialize<ParticleSystem>(rawData, null);
            DatabaseManager.ParticleSystems.Add(packet.ID, particleSystem);
        }
    }
}