﻿using Arcanos.Client.GameClient.Data;
using Arcanos.Client.GameClient.Items;
using Arcanos.Client.GameClient.Objects;
using Arcanos.Client.GameClient.Timing;
using Arcanos.Client.GameClient.World;
using Arcanos.Shared.Net;
using Arcanos.Shared.Objects;

namespace Arcanos.Client.GameClient.Net.Handlers
{
    public static class MapHandlers
    {
        public static void HandleSChangeMapPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SChangeMapPacket packet = (SChangeMapPacket)sPacket;

            WorldManager.CurrentMap.Remove(WorldManager.PlayerCreature);
            WorldManager.ChangeMap(packet.MapGUID, DatabaseManager.Maps.Get(packet.MapTemplateID), DatabaseManager.Environments.Get(packet.MapEnvironmentID));
            WorldManager.PlayerCreature.Position = packet.Position;
            WorldManager.PlayerCreature.GetVolume(out WorldManager.PlayerCreature.BoundingBox.Min, out WorldManager.PlayerCreature.BoundingBox.Max);
            WorldManager.PlayerCreature.Rotation = packet.Rotation;
            WorldManager.CurrentMap.Add(WorldManager.PlayerCreature);
            session.SendPacket(new CTeleportAckPacket());
        }
        public static void HandleSChangeEnvironmentPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SChangeEnvironmentPacket packet = (SChangeEnvironmentPacket)sPacket;

            DatabaseManager.Environments.Get(packet.ID, env => (WorldManager.CurrentMap.Environment as Environment).ChangeTo(env));
        }
        public static void HandleSObjectEnteredVisibilityPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SObjectEnteredVisibilityPacket packet = (SObjectEnteredVisibilityPacket)sPacket;
            ObjectManager.MarkAsLoading(packet.GUID);
            switch (packet.Type)
            {
                case ObjectType.WorldObject:
                    DatabaseManager.WorldObjects.Get(packet.ID, template =>
                    {
                        WorldObject wo = new WorldObject(packet.GUID, template, packet.Position, packet.Rotation)
                        {
                            Size = packet.Size,
                            Shape = packet.Shape,
                        };
                        Waiter.WaitForFinalization(wo, () =>
                        {
                            WorldManager.CurrentMap.Add(wo);
                            ObjectManager.ProcessPackets(session, wo.GUID);
                        });
                    });
                    break;
                case ObjectType.Creature:
                    DatabaseManager.Creatures.Get(packet.ID, template =>
                    {
                        Creature creature = new Creature(packet.GUID, template, packet.Position, packet.Rotation)
                        {
                            Size = packet.Size,
                            Shape = packet.Shape,
                        };
                        Waiter.WaitForFinalization(creature, () =>
                        {
                            WorldManager.CurrentMap.Add(creature);
                            ObjectManager.ProcessPackets(session, creature.GUID);
                        });
                    });
                    break;
                case ObjectType.PlayerCreature:
                    DatabaseManager.Characters.Get(packet.ID, character =>
                    {
                        if (!WorldManager.CharacterTemplatesCache.ContainsKey(character.ID))
                            session.SendPacket(new CCharacterTemplateQueryPacket { ID = character.ID });
                        Waiter.WaitFor(() => WorldManager.IsCharacterInGame && WorldManager.CharacterTemplatesCache.ContainsKey(character.ID), () =>
                        {
                            PlayerCreature pc = new PlayerCreature(packet.GUID, WorldManager.CharacterTemplatesCache[character.ID], character, packet.Position, packet.Rotation)
                            {
                                Size = packet.Size,
                                Shape = packet.Shape,
                            };
                            Waiter.WaitForFinalization(pc, () =>
                            {
                                WorldManager.CurrentMap.Add(pc);
                                ObjectManager.ProcessPackets(session, pc.GUID);
                            });
                        });
                    });
                    break;
            }
        }
        public static void HandleSObjectLeftVisibilityPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SObjectLeftVisibilityPacket packet = (SObjectLeftVisibilityPacket)sPacket;

            WorldObjectBase wo = ObjectManager.GetObject(packet.GUID) as WorldObjectBase;
            if (wo == null)
            {
                ObjectManager.EnqueuePacket(packet.GUID, WorldPackets.SObjectLeftVisibility, packet);
                return;
            }

            if (session.Player.CurrentCharacter.Creature.Map != null)
                session.Player.CurrentCharacter.Creature.Map.Remove(wo);
            ObjectManager.Unregister(packet.GUID);
            wo.Destroy();
        }
        public static void HandleSObjectUpdateLocationPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SObjectUpdateLocationPacket packet = (SObjectUpdateLocationPacket)sPacket;

            WorldObjectBase wo = ObjectManager.GetWorldObject(packet.GUID);
            if (wo == null)
            {
                ObjectManager.EnqueuePacket(packet.GUID, WorldPackets.SObjectUpdateLocation, packet);
                return;
            }

            wo.MoveTo(packet.Position);
            wo.RotateTo(packet.Rotation);
        }
        public static void HandleSObjectLootRecipientUpdatePacket(PlayerSession session, IWorldPacket sPacket)
        {
            SObjectLootRecipientUpdatePacket packet = (SObjectLootRecipientUpdatePacket)sPacket;

            WorldObjectBase wo = ObjectManager.GetWorldObject(packet.GUID);
            if (wo == null)
            {
                ObjectManager.EnqueuePacket(packet.GUID, WorldPackets.SObjectLootRecipientUpdate, packet);
                return;
            }

            wo.Loot = new Loot(wo, packet.LootRecipient);
        }
        public static void HandleSObjectFlagsUpdatePacket(PlayerSession session, IWorldPacket sPacket)
        {
            SObjectFlagsUpdatePacket packet = (SObjectFlagsUpdatePacket)sPacket;

            WorldObjectBase wo = ObjectManager.GetWorldObject(packet.GUID);
            if (wo == null)
            {
                ObjectManager.EnqueuePacket(packet.GUID, WorldPackets.SObjectFlagsUpdate, packet);
                return;
            }

            wo.SetFlag(packet.Flags);
            wo.UnsetFlag(~packet.Flags);
        }
        public static void HandleSObjectLoadingFinalizedPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SObjectLoadingFinalizedPacket packet = (SObjectLoadingFinalizedPacket)sPacket;
            
            WorldObjectBase wo = ObjectManager.GetWorldObject(packet.GUID);
            if (wo == null)
                ObjectManager.FinalizeLoading(packet.GUID);
            else
                wo.Finalized = true;
            Waiter.UpdateAllWaitingForFinalization();
        }
    }
}