﻿using System.Collections.Generic;
using Arcanos.Client.GameClient.Data;
using Arcanos.Client.GameClient.Objects;
using Arcanos.Client.GameClient.Spells;
using Arcanos.Client.GameClient.UI;
using Arcanos.Client.GameClient.World;
using Arcanos.Shared;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Net;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Spells;
using Microsoft.Xna.Framework;

namespace Arcanos.Client.GameClient.Net.Handlers
{
    public static class CombatHandlers
    {
        public static void HandleSCombatInflictedDamagePacket(PlayerSession session, IWorldPacket sPacket)
        {
            SCombatInflictedDamagePacket packet = (SCombatInflictedDamagePacket)sPacket;

            CreatureBase attacker = ObjectManager.GetCreature(packet.AttackerGUID);
            if (attacker == null)
            {
                ObjectManager.EnqueuePacket(packet.AttackerGUID, WorldPackets.SCombatInflictedDamage, packet);
                return;
            }
            CreatureBase victim = ObjectManager.GetCreature(packet.VictimGUID);
            if (victim == null)
            {
                ObjectManager.EnqueuePacket(packet.VictimGUID, WorldPackets.SCombatInflictedDamage, packet);
                return;
            }

            if (attacker == WorldManager.PlayerCreature && attacker != victim)
                UIManager.RaiseEvent(UIEvents.OutgoingDamage, victim, victim.GUID, packet.Damage);
            if (victim == WorldManager.PlayerCreature)
                UIManager.RaiseEvent(UIEvents.IncomingDamage, attacker, attacker.GUID, packet.Damage);
            UIManager.RaiseEvent(UIEvents.CreatureInflictedDamage, attacker, attacker.GUID, victim.GUID, packet.Damage);
        }
        public static void HandleSCombatHealedPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SCombatHealedPacket packet = (SCombatHealedPacket)sPacket;

            CreatureBase healer = ObjectManager.GetCreature(packet.HealerGUID);
            if (healer == null)
            {
                ObjectManager.EnqueuePacket(packet.HealerGUID, WorldPackets.SCombatHealed, packet);
                return;
            }
            CreatureBase target = ObjectManager.GetCreature(packet.TargetGUID);
            if (target == null)
            {
                ObjectManager.EnqueuePacket(packet.TargetGUID, WorldPackets.SCombatHealed, packet);
                return;
            }

            if (healer == WorldManager.PlayerCreature && healer != target)
                UIManager.RaiseEvent(UIEvents.OutgoingHeal, target, target.GUID, packet.Heal);
            if (target == WorldManager.PlayerCreature)
                UIManager.RaiseEvent(UIEvents.IncomingHeal, healer, healer.GUID, packet.Heal);
            UIManager.RaiseEvent(UIEvents.CreatureDoneHealing, healer, healer.GUID, target.GUID, packet.Heal);
        }
        public static void HandleSCombatPerformedMeleeAttackPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SCombatPerformedMeleeAttackPacket packet = (SCombatPerformedMeleeAttackPacket)sPacket;

            CreatureBase attacker = ObjectManager.GetCreature(packet.AttackerGUID);
            if (attacker == null)
            {
                ObjectManager.EnqueuePacket(packet.AttackerGUID, WorldPackets.SCombatPerformedMeleeAttack, packet);
                return;
            }
            CreatureBase victim = ObjectManager.GetCreature(packet.VictimGUID);
            if (victim == null)
            {
                ObjectManager.EnqueuePacket(packet.VictimGUID, WorldPackets.SCombatPerformedMeleeAttack, packet);
                return;
            }
            
            if (attacker.Type.IsPlayer())
                (attacker as PlayerCreature).PerformedMeleeAttack(victim, packet.AttackType, packet.Result);
            else if (attacker.Type.IsCreature())
                (attacker as Creature).PerformedMeleeAttack(victim, packet.AttackType, packet.Result);
        }
        public static void HandleSCombatPerformedSpellAttackPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SCombatPerformedSpellAttackPacket packet = (SCombatPerformedSpellAttackPacket)sPacket;

            CreatureBase caster = ObjectManager.GetCreature(packet.CasterGUID);
            if (caster == null)
            {
                ObjectManager.EnqueuePacket(packet.CasterGUID, WorldPackets.SCombatPerformedSpellAttack, packet);
                return;
            }
            CreatureBase target = ObjectManager.GetCreature(packet.TargetGUID);
            if (target == null)
            {
                ObjectManager.EnqueuePacket(packet.TargetGUID, WorldPackets.SCombatPerformedSpellAttack, packet);
                return;
            }

            SpellTemplate spell = DatabaseManager.Spells.Get(packet.SpellID);
            if (spell != null)
            {
                if (caster.Type.IsPlayer())
                    (caster as PlayerCreature).PerformedSpellAttack(target, spell, packet.Result);
                else if (caster.Type.IsCreature())
                    (caster as Creature).PerformedSpellAttack(target, spell, packet.Result);
                (caster.SpellBook as SpellBook).SetStateForCurrentSpell(spell, packet.TargetGUID, Vector3.Zero, packet.Result == AttackResult.Hit ? ClientSpellState.Hit : ClientSpellState.Miss);
            }
        }
        public static void HandleSCombatSpellCastedPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SCombatSpellCastedPacket packet = (SCombatSpellCastedPacket)sPacket;

            CreatureBase caster = ObjectManager.GetCreature(packet.CasterGUID);
            if (caster == null)
            {
                ObjectManager.EnqueuePacket(packet.CasterGUID, WorldPackets.SCombatSpellCasted, packet);
                return;
            }
            if (packet.TargetGUID == 0)
            {
                SpellTemplate spell = DatabaseManager.Spells.Get(packet.SpellID);
                if (spell != null)
                {
                    if (spell.CastingTime > 0)
                        caster.SpellBook.StopCasting();
                    caster.SpellBook.FinishCast(spell, packet.GroundTarget, CastSpellFlags.IgnoreAllChecks);
                    (caster.SpellBook as SpellBook).SetStateForCurrentSpell(spell, packet.TargetGUID, packet.GroundTarget, ClientSpellState.CastSuccess);
                    UIManager.RaiseEvent(UIEvents.CreatureSpellCasted, caster, caster.GUID, packet.TargetGUID, spell.ID, packet.GroundTarget);
                }
            }
            else
            {
                CreatureBase target = ObjectManager.GetCreature(packet.TargetGUID);
                if (target == null)
                {
                    ObjectManager.EnqueuePacket(packet.TargetGUID, WorldPackets.SCombatSpellCasted, packet);
                    return;
                }

                SpellTemplate spell = DatabaseManager.Spells.Get(packet.SpellID);
                if (spell != null)
                {
                    if (spell.CastingTime > 0)
                        caster.SpellBook.StopCasting();
                    caster.SpellBook.FinishCast(spell, target, CastSpellFlags.IgnoreAllChecks);
                    (caster.SpellBook as SpellBook).SetStateForCurrentSpell(spell, packet.TargetGUID, packet.GroundTarget, ClientSpellState.CastSuccess);
                    UIManager.RaiseEvent(UIEvents.CreatureSpellCasted, caster, caster.GUID, packet.TargetGUID, spell.ID, packet.GroundTarget);
                }
            }
        }
        public static void HandleSCombatSpellCastingStartedPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SCombatSpellCastingStartedPacket packet = (SCombatSpellCastingStartedPacket)sPacket;

            CreatureBase caster = ObjectManager.GetCreature(packet.CasterGUID);
            if (caster == null)
            {
                ObjectManager.EnqueuePacket(packet.CasterGUID, WorldPackets.SCombatSpellCastingStarted, packet);
                return;
            }
            if (packet.TargetGUID == 0)
            {
                SpellTemplate spell = DatabaseManager.Spells.Get(packet.SpellID);
                if (spell != null)
                {
                    caster.SpellBook.StartCasting(spell, packet.GroundTarget, CastSpellFlags.IgnoreAllChecks);
                    (caster.SpellBook as SpellBook).SetStateForCurrentSpell(spell, packet.TargetGUID, packet.GroundTarget, ClientSpellState.Casting);
                    UIManager.RaiseEvent(UIEvents.CreatureSpellCastingStarted, caster, caster.GUID, packet.TargetGUID, spell.ID, packet.GroundTarget);
                }
            }
            else
            {
                CreatureBase target = ObjectManager.GetCreature(packet.TargetGUID);
                if (target == null)
                {
                    ObjectManager.EnqueuePacket(packet.TargetGUID, WorldPackets.SCombatSpellCastingStarted, packet);
                    return;
                }

                SpellTemplate spell = DatabaseManager.Spells.Get(packet.SpellID);
                if (spell != null)
                {
                    caster.SpellBook.StartCasting(spell, target, CastSpellFlags.IgnoreAllChecks);
                    (caster.SpellBook as SpellBook).SetStateForCurrentSpell(spell, packet.TargetGUID, packet.GroundTarget, ClientSpellState.Casting);
                    UIManager.RaiseEvent(UIEvents.CreatureSpellCastingStarted, caster, caster.GUID, packet.TargetGUID, spell.ID, packet.GroundTarget);
                }
            }
        }
        public static void HandleSCombatSpellCastingFailedPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SCombatSpellCastingFailedPacket packet = (SCombatSpellCastingFailedPacket)sPacket;

            CreatureBase caster = ObjectManager.GetCreature(packet.CasterGUID);
            if (caster == null)
            {
                ObjectManager.EnqueuePacket(packet.CasterGUID, WorldPackets.SCombatSpellCastingFailed, packet);
                return;
            }
            /*CreatureBase target = ObjectManager.GetCreature(packet.TargetGUID);
            if (target == null)
                return;*/

            SpellTemplate spell = DatabaseManager.Spells.Get(packet.SpellID);
            if (spell != null)
            {
                caster.SpellBook.FailCasting(packet.Result);
                (caster.SpellBook as SpellBook).SetStateForCurrentSpell(spell, packet.TargetGUID, packet.GroundTarget, ClientSpellState.CastFail);
                UIManager.RaiseEvent(UIEvents.CreatureSpellCastingFailed, caster, caster.GUID, packet.TargetGUID, spell.ID, packet.GroundTarget);
            };
            if (caster == WorldManager.PlayerCreature)
                UIManager.RaiseEvent(UIEvents.GameError, caster, Localization.Game.ErrorCastSpellResult[(byte)packet.Result]);
        }
        public static void HandleSHasAuraPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SHasAuraPacket packet = (SHasAuraPacket)sPacket;

            CreatureBase owner = ObjectManager.GetCreature(packet.GUID);
            if (owner == null)
            {
                ObjectManager.EnqueuePacket(packet.CasterGUID, WorldPackets.SHasAura, packet);
                return;
            }

            SpellTemplate spell = DatabaseManager.Spells.Get(packet.SpellID);
            if (spell != null)
                owner.SpellBook.ApplyAura(packet.CasterGUID, spell, packet.EffectID);
        }
        public static void HandleSAppliedAuraPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SAppliedAuraPacket packet = (SAppliedAuraPacket)sPacket;

            CreatureBase owner = ObjectManager.GetCreature(packet.GUID);
            if (owner == null)
            {
                ObjectManager.EnqueuePacket(packet.CasterGUID, WorldPackets.SAppliedAura, packet);
                return;
            }

            SpellTemplate spell = DatabaseManager.Spells.Get(packet.SpellID);
            if (spell != null)
                owner.SpellBook.ApplyAura(packet.CasterGUID, spell, packet.EffectID);
        }
        public static void HandleSRemovedAuraPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SRemovedAuraPacket packet = (SRemovedAuraPacket)sPacket;

            CreatureBase owner = ObjectManager.GetCreature(packet.GUID);
            if (owner == null)
            {
                ObjectManager.EnqueuePacket(packet.CasterGUID, WorldPackets.SRemovedAura, packet);
                return;
            }

            SpellTemplate spell = DatabaseManager.Spells.Get(packet.SpellID);
            if (spell != null)
            {
                List<AuraBase> toRemove = new List<AuraBase>();
                foreach (AuraBase aura in owner.SpellBook.GetAuras())
                    if (aura.CasterGUID == packet.CasterGUID && aura.Spell == spell && aura.EffectID == packet.EffectID)
                        toRemove.Add(aura);
                foreach (AuraBase aura in toRemove)
                    owner.SpellBook.RemoveAura(aura);
            }
        }
        public static void HandleSAreaEffectPlacedPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SAreaEffectPlacedPacket packet = (SAreaEffectPlacedPacket)sPacket;

            SpellTemplate spell = DatabaseManager.Spells.Get(packet.SpellID);
            if (spell != null)
            {
                CreatureBase caster = ObjectManager.GetCreature(packet.CasterGUID);
                if (caster != null)
                    (caster.SpellBook as SpellBook).SetStateForCurrentSpell(spell, 0, packet.Position, ClientSpellState.Hit);
                new AreaEffect(packet.GUID, spell, WorldManager.CurrentMap, packet.Position);
            }
        }
        public static void HandleSAreaEffectRemovedPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SAreaEffectRemovedPacket packet = (SAreaEffectRemovedPacket)sPacket;

            AreaEffectBase areaEffect = ObjectManager.GetAreaEffect(packet.GUID);
            if (areaEffect == null)
            {
                ObjectManager.EnqueuePacket(packet.GUID, WorldPackets.SAreaEffectRemoved, packet);
                return;
            }

            areaEffect.Remove();
        }
        public static void HandleSDuelRequestPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SDuelRequestPacket packet = (SDuelRequestPacket)sPacket;
            CreatureBase pc;
            if (PacketHandling.GetOrQueue(WorldPackets.SDuelRequest, packet, packet.ChallengerGUID, out pc))
                return;

            UIManager.RaiseEvent(UIEvents.DuelRequest, null, packet.ChallengerGUID);
        }
        public static void HandleSDuelStatusPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SDuelStatusPacket packet = (SDuelStatusPacket)sPacket;

            UIManager.RaiseEvent(UIEvents.DuelStatus, null, packet.Status);
        }
        public static void HandleSDuelParticipantStatusPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SDuelParticipantStatusPacket packet = (SDuelParticipantStatusPacket)sPacket;

            UIManager.RaiseEvent(UIEvents.DuelParticipantStatus, null, packet.ParticipantGUID, packet.Status);
        }
    }
}