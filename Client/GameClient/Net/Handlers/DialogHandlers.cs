﻿using Arcanos.Client.GameClient.Data;
using Arcanos.Client.GameClient.UI;
using Arcanos.Shared.Dialog;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Net;

namespace Arcanos.Client.GameClient.Net.Handlers
{
    public static class DialogHandlers
    {
        public static void HandleSStartDialogResultPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SStartDialogResultPacket packet = (SStartDialogResultPacket)sPacket;

            if (packet.Result != DialogStartResult.Ok)
            {
                UIManager.RaiseEvent(UIEvents.GameError, null, Localization.Game.ErrorDialogStartResult[(byte)packet.Result]);
                return;
            }

            UIManager.RaiseEvent(UIEvents.StartDialog, null, packet.TalkerGUID);
        }
        public static void HandleSEndDialogPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SEndDialogPacket packet = (SEndDialogPacket)sPacket;

            UIManager.RaiseEvent(UIEvents.EndDialog, null, packet.TalkerGUID, packet.Reason);
        }
        public static void HandleSChangedPagePacket(PlayerSession session, IWorldPacket sPacket)
        {
            SChangedPagePacket packet = (SChangedPagePacket)sPacket;

            UIManager.RaiseEvent(UIEvents.ChangePage, null, packet.TextID, packet.OptionCount, packet.QuestOptionCount);
        }
        public static void HandleSPageOptionPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SPageOptionPacket packet = (SPageOptionPacket)sPacket;

            UIManager.RaiseEvent(UIEvents.PageOptionReceived, null, packet.Index, packet.TextID, packet.IconID);
        }
        public static void HandleSPageQuestOptionPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SPageQuestOptionPacket packet = (SPageQuestOptionPacket)sPacket;

            DatabaseManager.Quests.Get(packet.QuestID, quest => { });
            UIManager.RaiseEvent(UIEvents.PageQuestOptionReceived, null, packet.Index, packet.TextID, packet.State, packet.QuestID);
        }
        public static void HandleSSelectOptionResultPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SSelectOptionResultPacket packet = (SSelectOptionResultPacket)sPacket;
            
            if (packet.Result != DialogSelectOptionResult.Ok)
                UIManager.RaiseEvent(UIEvents.GameError, null, Localization.Game.ErrorDialogSelectOptionResult[(byte)packet.Result]);
        }
        public static void HandleSDialogQuestAcceptPagePacket(PlayerSession session, IWorldPacket sPacket)
        {
            SDialogQuestAcceptPagePacket packet = (SDialogQuestAcceptPagePacket)sPacket;

            DatabaseManager.Quests.Get(packet.QuestID, quest => { });
            UIManager.RaiseEvent(UIEvents.AcceptQuestPage, null, packet.QuestID, packet.TextID, packet.CanAccept);
        }
        public static void HandleSDialogQuestCompletePagePacket(PlayerSession session, IWorldPacket sPacket)
        {
            SDialogQuestCompletePagePacket packet = (SDialogQuestCompletePagePacket)sPacket;

            DatabaseManager.Quests.Get(packet.QuestID, quest => { });
            UIManager.RaiseEvent(UIEvents.CompleteQuestPage, null, packet.QuestID, packet.TextID, packet.CanComplete);
        }
    }
}