﻿using Arcanos.Client.GameClient.Objects;
using Arcanos.Shared.Net;
using Arcanos.Shared.Objects;

namespace Arcanos.Client.GameClient.Net.Handlers
{
    public static class MovementHandlers
    {
        private const float DISTANCE_TOLERANCE_THRESHOLD = 5;

        public static void HandleCMovementUpdatePacket(PlayerSession session, IWorldPacket sPacket)
        {
            CMovementUpdatePacket packet = (CMovementUpdatePacket)sPacket;
            CreatureBase creature = ObjectManager.GetCreature(packet.GUID);
            if (creature == null)
                return;
            creature.MoveTo(packet.Data.Position);
            creature.RotateTo(packet.Data.Rotation);
            if (creature is PlayerCreature)
                (creature as PlayerCreature).MovementFlags = packet.Data.Flags;
        }
        public static void HandleSCreatureMovementUpdatePacket(PlayerSession session, IWorldPacket sPacket, byte[] rawData)
        {
            SCreatureMovementUpdatePacket packet = (SCreatureMovementUpdatePacket)sPacket;
            CreatureBase creature = ObjectManager.GetCreature(packet.GUID);
            if (creature == null)
                return;
            if (creature.DistanceTo(packet.Position) > DISTANCE_TOLERANCE_THRESHOLD)
                creature.MoveTo(packet.Position);
            creature.Mover.FillFromPacket(ref packet);
        }
    }
}