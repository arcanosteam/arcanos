﻿using Arcanos.Client.GameClient.Data;
using Arcanos.Client.GameClient.Objects;
using Arcanos.Client.GameClient.UI;
using Arcanos.Shared;
using Arcanos.Shared.Net;
using Arcanos.Shared.Objects;

namespace Arcanos.Client.GameClient.Net.Handlers
{
    public static class CreatureHandlers
    {
        public static void HandleSCreatureEnteredCombatPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SCreatureEnteredCombatPacket packet = (SCreatureEnteredCombatPacket)sPacket;
            CreatureBase creature;
            if (PacketHandling.GetOrQueue(WorldPackets.SCreatureEnteredCombat, packet, packet.GUID, out creature))
                return;

            creature.IsInCombat = true;
            UIManager.RaiseEvent(UIEvents.CreatureEnteredCombat, creature);
        }
        public static void HandleSCreatureEvadedPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SCreatureEvadedPacket packet = (SCreatureEvadedPacket)sPacket;
            CreatureBase creature;
            if (PacketHandling.GetOrQueue(WorldPackets.SCreatureEvaded, packet, packet.GUID, out creature))
                return;

            creature.IsInCombat = false;
            UIManager.RaiseEvent(UIEvents.CreatureEvaded, creature);
        }
        public static void HandleSCreatureHealthUpdatePacket(PlayerSession session, IWorldPacket sPacket)
        {
            SCreatureHealthUpdatePacket packet = (SCreatureHealthUpdatePacket)sPacket;
            CreatureBase creature;
            if (PacketHandling.GetOrQueue(WorldPackets.SCreatureHealthUpdate, packet, packet.GUID, out creature))
                return;

            creature.SetHealth(packet.CurrentHealth, packet.MaxHealth);
        }
        public static void HandleSCreatureUsesPowerUpdatePacket(PlayerSession session, IWorldPacket sPacket)
        {
            SCreatureUsesPowerUpdatePacket packet = (SCreatureUsesPowerUpdatePacket)sPacket;
            CreatureBase creature;
            if (PacketHandling.GetOrQueue(WorldPackets.SCreatureUsesPowerUpdate, packet, packet.GUID, out creature))
                return;

            creature.SetUsesPower(packet.PowerType, packet.Uses);
        }
        public static void HandleSCreaturePowerUpdatePacket(PlayerSession session, IWorldPacket sPacket)
        {
            SCreaturePowerUpdatePacket packet = (SCreaturePowerUpdatePacket)sPacket;
            CreatureBase creature;
            if (PacketHandling.GetOrQueue(WorldPackets.SCreaturePowerUpdate, packet, packet.GUID, out creature))
                return;

            creature.SetPower(packet.PowerType, packet.CurrentPower, packet.MaxPower);
        }
        public static void HandleSCreatureStateUpdatePacket(PlayerSession session, IWorldPacket sPacket)
        {
            SCreatureStateUpdatePacket packet = (SCreatureStateUpdatePacket)sPacket;
            CreatureBase creature;
            if (PacketHandling.GetOrQueue(WorldPackets.SCreatureStateUpdate, packet, packet.GUID, out creature))
                return;

            creature.SetState(packet.State);
        }
        public static void HandleSCreatureTargetUpdatePacket(PlayerSession session, IWorldPacket sPacket)
        {
            SCreatureTargetUpdatePacket packet = (SCreatureTargetUpdatePacket)sPacket;
            CreatureBase creature;
            if (PacketHandling.GetOrQueue(WorldPackets.SCreatureTargetUpdate, packet, packet.GUID, out creature))
                return;

            if (creature.Type.IsPlayer())
                (creature as PlayerCreature).SetTarget(packet.TargetGUID);
            else
                (creature as Creature).SetTarget(packet.TargetGUID);
        }
        public static void HandleSCreatureMovementTypeUpdatePacket(PlayerSession session, IWorldPacket sPacket)
        {
            SCreatureMovementTypeUpdatePacket packet = (SCreatureMovementTypeUpdatePacket)sPacket;
            CreatureBase creature;
            if (PacketHandling.GetOrQueue(WorldPackets.SCreatureMovementTypeUpdate, packet, packet.GUID, out creature))
                return;

            creature.SetMovementType(packet.MovementType);
            creature.SetMovementSpeedFactor(packet.MovementType, packet.MovementSpeedFactor);
        }
        public static void HandleSCreatureFactionUpdatePacket(PlayerSession session, IWorldPacket sPacket)
        {
            SCreatureFactionUpdatePacket packet = (SCreatureFactionUpdatePacket)sPacket;
            CreatureBase creature;
            if (PacketHandling.GetOrQueue(WorldPackets.SCreatureFactionUpdate, packet, packet.GUID, out creature))
                return;

            DatabaseManager.Factions.Get(packet.FactionID, faction => creature.Faction = faction);
        }
        public static void HandleSCreatureModelUpdatePacket(PlayerSession session, IWorldPacket sPacket)
        {
            SCreatureModelUpdatePacket packet = (SCreatureModelUpdatePacket)sPacket;
            CreatureBase creature;
            if (PacketHandling.GetOrQueue(WorldPackets.SCreatureModelUpdate, packet, packet.GUID, out creature))
                return;

            DatabaseManager.Models.Get(packet.ModelID, model => creature.Model = model.CreateInstance(creature));
        }
    }
}