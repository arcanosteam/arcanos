﻿using Arcanos.Client.GameClient.Data;
using Arcanos.Client.GameClient.UI;
using Arcanos.Client.GameClient.UI.Game;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Net;
using Arcanos.Shared.Quests;

namespace Arcanos.Client.GameClient.Net.Handlers
{
    public static class QuestHandlers
    {
        public static void HandleSQuestAddedPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SQuestAddedPacket packet = (SQuestAddedPacket)sPacket;

            UIManager.RaiseEvent(UIEvents.QuestAdded, null, packet.ID, packet.Completed, packet.Initial);
        }
        public static void HandleSQuestRemovedPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SQuestRemovedPacket packet = (SQuestRemovedPacket)sPacket;

            UIManager.RaiseEvent(UIEvents.QuestRemoved, null, packet.ID);
        }
        public static void HandleSQuestCompletedPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SQuestCompletedPacket packet = (SQuestCompletedPacket)sPacket;

            UIManager.RaiseEvent(UIEvents.QuestCompleted, null, packet.ID, packet.Initial);
            if (!packet.Initial)
                DatabaseManager.Quests.Get(packet.ID, quest =>
                    UIManager.RaiseEvent(UIEvents.GameInfo, null, quest.Title + " completed!"));
        }
        public static void HandleSQuestObjectiveUpdatePacket(PlayerSession session, IWorldPacket sPacket)
        {
            SQuestObjectiveUpdatePacket packet = (SQuestObjectiveUpdatePacket)sPacket;

            UIManager.RaiseEvent(UIEvents.QuestObjectiveUpdated, null, packet.QuestID, packet.ObjectiveIndex, packet.CurrentProgress, packet.Completed, packet.Initial);
            if (!packet.Initial)
                DatabaseManager.Quests.Get(packet.QuestID, quest =>
                    UIQuest.GetObjectiveText(quest.Objectives[packet.ObjectiveIndex], text =>
                        UIManager.RaiseEvent(UIEvents.GameInfo, null, text + UIQuest.FormatObjectiveStatus(packet.CurrentProgress, quest.Objectives[packet.ObjectiveIndex].Required, packet.Completed))));
        }
        public static void HandleSAddQuestResultPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SAddQuestResultPacket packet = (SAddQuestResultPacket)sPacket;

            if (packet.Result != QuestAcceptResult.Ok)
                UIManager.RaiseEvent(UIEvents.GameError, null, Localization.Game.ErrorQuestAcceptResult[(byte)packet.Result]);
        }
        public static void HandleSRemoveQuestResultPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SRemoveQuestResultPacket packet = (SRemoveQuestResultPacket)sPacket;

            if (packet.Result != QuestRemoveResult.Ok)
                UIManager.RaiseEvent(UIEvents.GameError, null, Localization.Game.ErrorQuestRemoveResult[(byte)packet.Result]);
        }
        public static void HandleSCompleteQuestResultPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SCompleteQuestResultPacket packet = (SCompleteQuestResultPacket)sPacket;

            if (packet.Result != QuestCompleteResult.Ok)
                UIManager.RaiseEvent(UIEvents.GameError, null, Localization.Game.ErrorQuestCompleteResult[(byte)packet.Result]);
        }
    }
}