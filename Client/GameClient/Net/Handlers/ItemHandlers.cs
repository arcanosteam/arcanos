﻿using Arcanos.Client.GameClient.Data;
using Arcanos.Client.GameClient.Items;
using Arcanos.Client.GameClient.UI;
using Arcanos.Client.GameClient.World;
using Arcanos.Shared.Items;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Net;
using Arcanos.Shared.Objects;
using Arcanos.Utilities;

namespace Arcanos.Client.GameClient.Net.Handlers
{
    public static class ItemHandlers
    {
        public static void HandleSMoneyChangedPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SMoneyChangedPacket packet = (SMoneyChangedPacket)sPacket;
            int oldMoney = WorldManager.PlayerCreature.Character.Money;
            WorldManager.PlayerCreature.Character.Money = packet.Count;
            UIManager.RaiseEvent(UIEvents.MoneyUpdated, WorldManager.PlayerCreature, packet.Count, oldMoney, packet.Initial);
        }
        public static void HandleSBagChangedPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SBagChangedPacket packet = (SBagChangedPacket)sPacket;

            Inventory inventory = session.Player.CurrentCharacter.Inventory;
            Bag bag = new Bag(inventory, ItemManager.Get(packet.BagItemGUID), packet.BagSize);
            inventory.Bags[packet.BagIndex] = bag;
            UIManager.RaiseEvent(UIEvents.BagChanged, inventory, packet.BagIndex, bag);
        }
        public static void HandleSBagSlotUpdatedPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SBagSlotUpdatedPacket packet = (SBagSlotUpdatedPacket)sPacket;

            if (packet.ItemGUID == 0)
            {
                Inventory inventory = session.Player.CurrentCharacter.Inventory;
                BagBase bag = packet.BagIndex == -1 ? session.Player.CurrentCharacter.Equipment : inventory.Bags[packet.BagIndex];
                if (bag.Items[packet.BagSlot] != null)
                    bag.Items[packet.BagSlot].Bag = null;
                bag.Items[packet.BagSlot] = null;
                UIManager.RaiseEvent(UIEvents.BagSlotUpdated, inventory, packet.BagIndex, packet.BagSlot, 0ul);
            }
            else
                ItemManager.Get(packet.ItemGUID, item =>
                {
                    Inventory inventory = session.Player.CurrentCharacter.Inventory;
                    BagBase bag = packet.BagIndex == -1 ? session.Player.CurrentCharacter.Equipment : inventory.Bags[packet.BagIndex];
                    item.Bag = bag;
                    bag.Items[packet.BagSlot] = item;
                    UIManager.RaiseEvent(UIEvents.BagSlotUpdated, inventory, packet.BagIndex, packet.BagSlot, item.GUID);
                });
        }
        public static void HandleSItemCountUpdatedPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SItemCountUpdatedPacket packet = (SItemCountUpdatedPacket)sPacket;
            ItemManager.Get(packet.GUID, item => item.SetCount(packet.Count));
        }
        public static void HandleSItemDataResponsePacket(PlayerSession session, IWorldPacket sPacket)
        {
            SItemDataResponsePacket packet = (SItemDataResponsePacket)sPacket;
            DatabaseManager.Items.Get(packet.ID, template =>
            {
                Item item = new Item(packet.GUID, template, packet.Count);
                ItemManager.Add(packet.GUID, item);
            });
        }
        public static void HandleSMoveItemResultPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SMoveItemResultPacket packet = (SMoveItemResultPacket)sPacket;

            if (packet.Result != ItemMoveResult.Ok)
                UIManager.RaiseEvent(UIEvents.GameError, session.Player.CurrentCharacter, Localization.Game.ErrorItemMoveResult[(byte)packet.Result]);
        }
        public static void HandleSDivideItemResultPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SDivideItemResultPacket packet = (SDivideItemResultPacket)sPacket;

            if (packet.Result != ItemDivideResult.Ok)
                UIManager.RaiseEvent(UIEvents.GameError, session.Player.CurrentCharacter, Localization.Game.ErrorItemDivideResult[(byte)packet.Result]);
        }
        public static void HandleSDestroyItemResultPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SDestroyItemResultPacket packet = (SDestroyItemResultPacket)sPacket;

            if (packet.Result != ItemDestroyResult.Ok)
                UIManager.RaiseEvent(UIEvents.GameError, session.Player.CurrentCharacter, Localization.Game.ErrorItemDestroyResult[(byte)packet.Result]);
        }
        public static void HandleSUseItemResultPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SUseItemResultPacket packet = (SUseItemResultPacket)sPacket;

            if (packet.Result == ItemUseResult.SpellCastError)
                return; // Spell cast errors are sent in their own packets
            if (packet.Result != ItemUseResult.Ok)
                UIManager.RaiseEvent(UIEvents.GameError, session.Player.CurrentCharacter, Localization.Game.ErrorItemUseResult[(byte)packet.Result]);
        }
        public static void HandleSEquipItemResultPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SEquipItemResultPacket packet = (SEquipItemResultPacket)sPacket;

            if (packet.Result != ItemEquipResult.Ok)
                UIManager.RaiseEvent(UIEvents.GameError, session.Player.CurrentCharacter, Localization.Game.ErrorItemEquipResult[(byte)packet.Result]);
        }
        public static void HandleSUnequipItemResultPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SUnequipItemResultPacket packet = (SUnequipItemResultPacket)sPacket;

            if (packet.Result != ItemUnequipResult.Ok)
                UIManager.RaiseEvent(UIEvents.GameError, session.Player.CurrentCharacter, Localization.Game.ErrorItemUnequipResult[(byte)packet.Result]);
        }
        public static void HandleSItemsAddedPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SItemsAddedPacket packet = (SItemsAddedPacket)sPacket;
            UIManager.RaiseEvent(UIEvents.ItemsAcquired, session.Player.CurrentCharacter, packet.ID, packet.Count);
        }
        public static void HandleSItemsRemovedPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SItemsRemovedPacket packet = (SItemsRemovedPacket)sPacket;
            UIManager.RaiseEvent(UIEvents.ItemsLost, session.Player.CurrentCharacter, packet.ID, packet.Count);
        }
        public static void HandleSCancelLootingPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SCancelLootingPacket packet = (SCancelLootingPacket)sPacket;

            UIManager.RaiseEvent(UIEvents.LootClosed, null, packet.GUID);
        }
        public static void HandleSLootResultPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SLootResultPacket packet = (SLootResultPacket)sPacket;

            WorldObjectBase wo = ObjectManager.GetWorldObject(packet.SourceGUID);
            if (wo == null)
                return;

            if (packet.Result != LootingResult.Ok)
            {
                WorldManager.PlayerCreature.UnsetFlag(ObjectFlags.Looting);
                UIManager.RaiseEvent(UIEvents.GameError, wo, Localization.Game.ErrorLootingResult[(byte)packet.Result]);
                return;
            }

            if (wo.Loot == null)
            {
                wo.Loot = new Loot(wo, new Looter(WorldManager.PlayerCreature));
                Logger.Error(LogCategory.General, "Opened loot but loot recipient is null");
            }
            Loot loot = wo.Loot as Loot;
            loot.Money = packet.Money;
            loot.Items = new ItemDefinition[packet.ItemCount];
        }
        public static void HandleSLootResultItemPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SLootResultItemPacket packet = (SLootResultItemPacket)sPacket;

            WorldObjectBase wo = ObjectManager.GetWorldObject(packet.SourceGUID);
            if (wo == null)
                return;

            Loot loot = wo.Loot as Loot;
            switch (packet.LootType)
            {
                case LootType.None:
                    loot.Items[packet.ItemIndex] = new ItemDefinition(null, 0);
                    if (loot.AllItemsLoaded)
                        UIManager.RaiseEvent(UIEvents.LootOpened, wo, loot.Source.GUID, loot.Money, loot.Items);
                    break;
                case LootType.Item:
                    DatabaseManager.Items.Get(packet.ItemID, item =>
                    {
                        loot.Items[packet.ItemIndex] = new ItemDefinition(item, packet.ItemCount);
                        if (loot.AllItemsLoaded)
                            UIManager.RaiseEvent(UIEvents.LootOpened, wo, loot.Source.GUID, loot.Money, loot.Items);
                    });
                    break;
                case LootType.Money:
                {
                    //loot.Money = packet.ItemCount; // Already handled in HandleSLootResultPacket
                    loot.Items[packet.ItemIndex] = new ItemDefinition(null, packet.ItemCount);
                    if (loot.AllItemsLoaded)
                        UIManager.RaiseEvent(UIEvents.LootOpened, wo, loot.Source.GUID, loot.Money, loot.Items);
                    break;
                }
            }
        }
        public static void HandleSLootItemResultPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SLootItemResultPacket packet = (SLootItemResultPacket)sPacket;

            if (packet.Result != ItemTakeResult.Ok)
                UIManager.RaiseEvent(UIEvents.GameError, session.Player.CurrentCharacter, Localization.Game.ErrorItemTakeResult[(byte)packet.Result]);
        }
        public static void HandleSLootItemRemovedPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SLootItemRemovedPacket packet = (SLootItemRemovedPacket)sPacket;

            UIManager.RaiseEvent(UIEvents.LootItemRemoved, null, packet.SourceGUID, packet.ItemIndex);
        }
    }
}