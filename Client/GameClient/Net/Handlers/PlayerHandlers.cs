﻿using System.Collections.Generic;
using Arcanos.Client.GameClient.Data;
using Arcanos.Client.GameClient.Objects;
using Arcanos.Client.GameClient.Players;
using Arcanos.Client.GameClient.Timing;
using Arcanos.Client.GameClient.UI;
using Arcanos.Client.GameClient.World;
using Arcanos.Shared.Data;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Net;
using Arcanos.Shared.Players;
using Arcanos.Shared.Server;

namespace Arcanos.Client.GameClient.Net.Handlers
{
    public static class PlayerHandlers
    {
        public static void HandleSPlayerResponsePacket(PlayerSession session, IWorldPacket sPacket, byte[] rawData)
        {
            SPlayerResponsePacket packet = (SPlayerResponsePacket)sPacket;
            Player player = Serialization.Deserialize<Player>(rawData, null, typeof(PlayerBase));
            DatabaseManager.Players.Add(packet.ID, player);
        }

        public static void HandleSCharacterListStatusResponsePacket(PlayerSession session, IWorldPacket sPacket)
        {
            SCharacterListStatusResponsePacket packet = (SCharacterListStatusResponsePacket)sPacket;

            session.Player.Characters = new List<CharacterBase>(packet.MaxCount);
            for (byte i = 0; i < packet.MaxCount; i++)
                session.Player.Characters.Add(null);
            session.InvokeCharacterListStatusLoaded();

            for (byte i = 0; i < packet.MaxCount; i++)
                session.SendPacket(new CCharacterStatusQueryPacket { Index = i });
        }
        public static void HandleSCharacterStatusResponsePacket(PlayerSession session, IWorldPacket sPacket)
        {
            SCharacterStatusResponsePacket packet = (SCharacterStatusResponsePacket)sPacket;
            DatabaseManager.Characters.Get(packet.ID, character =>
            {
                character.Player = session.Player;
                List<CharacterBase> characters = session.Player.Characters;
                characters[packet.Index] = character;
                session.InvokeCharacterLoaded(packet.Index, character);
                for (byte i = 0; i < characters.Count; i++)
                    if (characters[i] == null)
                        return;
                session.InvokeCharacterListLoaded();
            });
        }
 
        public static void HandleSWorldEnterResponsePacket(PlayerSession session, IWorldPacket sPacket)
        {
            SWorldEnterResponsePacket packet = (SWorldEnterResponsePacket)sPacket;

            if (packet.Result != WorldEnterResult.Ok)
            {
                UIManager.RaiseEvent(UIEvents.GameError, null, Localization.Game.ErrorWorldEnterResult[(byte)packet.Result]);
                return;
            }

            DatabaseManager.Characters.Get(packet.CharacterID, character =>
            {
                session.Player.CurrentCharacter = character;
                DatabaseManager.Maps.Get(packet.MapTemplateID, mapTemplate => DatabaseManager.Environments.Get(packet.MapEnvironmentID, env =>
                {
                    if (!WorldManager.CharacterTemplatesCache.ContainsKey(packet.CharacterID))
                        session.SendPacket(new CCharacterTemplateQueryPacket { ID = packet.CharacterID });
                    Waiter.WaitFor(() => WorldManager.CharacterTemplatesCache.ContainsKey(packet.CharacterID), () =>
                    {
                        Map map = WorldManager.ChangeMap(packet.MapGUID, mapTemplate, env);
                        PlayerCreature playerCreature = new PlayerCreature(packet.PlayerCreatureGUID, WorldManager.CharacterTemplatesCache[packet.CharacterID], session.Player.CurrentCharacter, packet.Position, packet.Rotation);
                        session.Player.CurrentCharacter.Creature = playerCreature;
                        UIManager.RaiseEvent(UIEvents.PlayerLoggedIn, null);
                        Waiter.WaitForFinalization(playerCreature, () =>
                        {
                            playerCreature.Finalized = false;
                            map.Add(playerCreature);
                            session.EnterWorld(map, playerCreature);
                            UIManager.RaiseEvent(UIEvents.PlayerReadyForLoading, null);
                            Waiter.WaitForFinalization(playerCreature, () =>
                            {
                                UIManager.RaiseEvent(UIEvents.PlayerLoaded, null);
                                session.SendPacket(new CTeleportAckPacket());
                            });
                        });
                    });
                }));
            });
        }
    }
}