﻿using System;
using Arcanos.Client.GameClient.Objects;
using Arcanos.Client.GameClient.Players;
using Arcanos.Client.GameClient.UI;
using Arcanos.Client.GameClient.World;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Net;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Players;

namespace Arcanos.Client.GameClient.Net.Handlers
{
    public static class GroupHandlers
    {
        public static void HandleSCharacterChangedGroupPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SCharacterChangedGroupPacket packet = (SCharacterChangedGroupPacket)sPacket;

            PlayerCreature pc = ObjectManager.GetPlayer(packet.GUID) as PlayerCreature;
            if (pc == null)
            {
                ObjectManager.EnqueuePacket(packet.GUID, WorldPackets.SCharacterChangedGroup, packet);
                return;
            }

            pc.Character.GroupID = packet.GroupID;
            if (pc == WorldManager.PlayerCreature)
            {
                if (packet.GroupID == 0)
                {
                    pc.Character.Group = null;
                    UIManager.RaiseEvent(UIEvents.LeftGroup, pc.Character, 0);
                }
                else
                {
                    pc.Character.Group = new Group();
                    pc.Character.Group.AddMember(packet.Index, pc.Character);
                    UIManager.RaiseEvent(UIEvents.JoinedGroup, pc.Character, packet.GroupID, packet.Index);
                }
            }
        }
        public static void HandleSGroupInviteResultPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SGroupInviteResultPacket packet = (SGroupInviteResultPacket)sPacket;

            if (packet.Result != GroupInviteResult.Ok)
                UIManager.RaiseEvent(UIEvents.GameError, null, Localization.Game.ErrorGroupInviteResult[(byte)packet.Result]);
        }
        public static void HandleSGroupKickResultPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SGroupKickResultPacket packet = (SGroupKickResultPacket)sPacket;

            if (packet.Result != GroupKickResult.Ok)
                UIManager.RaiseEvent(UIEvents.GameError, null, Localization.Game.ErrorGroupKickResult[(byte)packet.Result]);
        }
        public static void HandleSGroupInvitationPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SGroupInvitationPacket packet = (SGroupInvitationPacket)sPacket;

            UIManager.RaiseEvent(UIEvents.GroupInvite, WorldManager.Player.CurrentCharacter, packet.InviterGUID, packet.InviterName);
        }
        public static void HandleSGroupMemberJoinedPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SGroupMemberJoinedPacket packet = (SGroupMemberJoinedPacket)sPacket;

            PlayerCreature pc = ObjectManager.GetPlayer(packet.MemberGUID) as PlayerCreature;
            if (pc == null)
            {
                ObjectManager.EnqueuePacket(packet.MemberGUID, WorldPackets.SGroupMemberJoined, packet);
                return;
            }

            Group group = WorldManager.Player.CurrentCharacter.Group;
            if (group == null)
                throw new Exception();

            group.AddMember(packet.Index, pc.Character);

            UIManager.RaiseEvent(UIEvents.GroupMemberJoined, WorldManager.Player.CurrentCharacter, packet.MemberGUID, packet.Index, packet.Initial);
        }
        public static void HandleSGroupMemberLeftPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SGroupMemberLeftPacket packet = (SGroupMemberLeftPacket)sPacket;

            Group group = WorldManager.Player.CurrentCharacter.Group;
            if (group == null)
                throw new Exception();

            group.RemoveMember(packet.Index);

            UIManager.RaiseEvent(UIEvents.GroupMemberLeft, WorldManager.Player.CurrentCharacter, packet.MemberGUID, packet.Index);
        }
        public static void HandleSGroupLeaderChangedPacket(PlayerSession session, IWorldPacket sPacket)
        {
            SGroupLeaderChangedPacket packet = (SGroupLeaderChangedPacket)sPacket;

            PlayerCreature pc = ObjectManager.GetPlayer(packet.LeaderGUID) as PlayerCreature;
            if (pc == null)
            {
                ObjectManager.EnqueuePacket(packet.LeaderGUID, WorldPackets.SGroupLeaderChanged, packet);
                return;
            }

            Group group = WorldManager.Player.CurrentCharacter.Group;
            if (group == null)
                throw new Exception();

            group.Leader = pc.Character;

            UIManager.RaiseEvent(UIEvents.GroupLeaderChanged, WorldManager.Player.CurrentCharacter, pc.GUID, group.IndexOf(pc.Character), packet.Initial);
        }
    }
}