﻿using Arcanos.Client.GameClient.Data;
using Arcanos.Client.GameClient.Items;
using Arcanos.Client.GameClient.Objects;
using Arcanos.Client.GameClient.Quests;
using Arcanos.Shared;
using Arcanos.Shared.Data;
using Arcanos.Shared.Items;
using Arcanos.Shared.Players;

namespace Arcanos.Client.GameClient.Players
{
    public class Character : CharacterBase, IPostDeserialization
    {
        public new Player Player
        {
            get { return (Player)base.Player; }
            set { base.Player = value; }
        }
        public new PlayerCreature Creature
        {
            get { return (PlayerCreature)base.Creature; }
            set { base.Creature = value; }
        }

        public int XP, XPToLevel;
        public int Money;
        public readonly Stats Stats;
        public readonly Inventory Inventory;
        public readonly Bag Equipment;
        public readonly QuestLog QuestLog;
        public Group Group;
        public int GroupID;

        protected Character()
        {
            Inventory = new Inventory(this);
            Equipment = new Bag(Inventory, null, (byte)EquipSlot.Max);
            QuestLog = new QuestLog(this);
            Stats = new Stats(this);
        }
        public Character(Player player, string name, PlayerRace playerRace, PlayerClass playerClass, byte modelIndex) : base(player, name, playerRace, playerClass, modelIndex)
        {
            Inventory = new Inventory(this);
            Equipment = new Bag(Inventory, null, (byte)EquipSlot.Max);
            QuestLog = new QuestLog(this);
            Stats = new Stats(this);
        }

        protected override int GetInitialID()
        {
            return DatabaseManager.Characters.GetID(this);
        }

        public void MemberDeserialized(string member, object value)
        {
        }
        public void Deserialized(object parent)
        {
            Inventory.Owner = this;
            QuestLog.Owner = this;
        }
    }
}