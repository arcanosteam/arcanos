﻿using System.IO;
using Arcanos.Client.GameClient.Data;
using Arcanos.Shared.Players;

namespace Arcanos.Client.GameClient.Players
{
    public class Group : GroupBase
    {
        protected override int GetInitialID()
        {
            return 0;
        }

        public override byte[] Serialize(string member)
        {
            return new byte[0];
        }
        public override void Deserialize(string member, byte[] data)
        {
            MemoryStream stream = new MemoryStream(data);
            BinaryReader reader = new BinaryReader(stream);
            switch (member)
            {
                case "Leader":
                {
                    int id = reader.ReadInt32();
                    Leader = id == 0 ? null : DatabaseManager.Characters.Get(id);
                    break;
                }
                case "Members":
                {
                    byte count = reader.ReadByte();
                    Members = new CharacterBase[count];
                    for (byte i = 0; i < count; ++i)
                    {
                        int id = reader.ReadInt32();
                        Members[i] = id == 0 ? null : DatabaseManager.Characters.Get(id);
                    }
                    break;
                }
            }
            stream.Close();
        }
    }
}