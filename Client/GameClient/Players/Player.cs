﻿using System.Collections.Generic;
using Arcanos.Client.GameClient.Data;
using Arcanos.Client.GameClient.Net;
using Arcanos.Shared.Data;
using Arcanos.Shared.Players;

namespace Arcanos.Client.GameClient.Players
{
    public class Player : PlayerBase
    {
        public new Character CurrentCharacter
        {
            get { return (Character)base.CurrentCharacter; }
            set { base.CurrentCharacter = value; }
        }
        public PlayerSession Session;

        [SerializationSettings(Method = SerializationMethod.RefByID, ReferenceResolverType = typeof(DatabaseManager), ReferenceResolverMethod = "GetCharacter")]
        public override List<CharacterBase> Characters { get; set; }

        protected Player() { }
        public Player(string name) : base(name) { }

        protected override int GetInitialID()
        {
            return DatabaseManager.Players.GetID(this);
        }
    }
}