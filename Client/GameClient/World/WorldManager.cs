﻿using System.Collections.Generic;
using Arcanos.Client.GameClient.Net;
using Arcanos.Client.GameClient.Objects;
using Arcanos.Client.GameClient.Players;
using Arcanos.Shared.Objects;
using Microsoft.Xna.Framework;

namespace Arcanos.Client.GameClient.World
{
    public static class WorldManager
    {
        public static readonly Dictionary<int, CreatureTemplate> CharacterTemplatesCache = new Dictionary<int, CreatureTemplate>();

        public static Player Player;
        public static PlayerSession PlayerSession;
        public static Camera Camera;
        public static Map CurrentMap;

        public static float StaticVisibilityDistance = 100.0f;
        public static float StaticReducedVisibilityDistance = 50.0f;

        public static Matrix WorldMatrix = Matrix.CreateWorld(Vector3.Zero, -Vector3.UnitZ, Vector3.UnitY);

        public static bool IsPlayerLoaded
        {
            get { return Player != null; }
        }
        public static bool IsCharacterSelected
        {
            get { return Player != null && Player.CurrentCharacter != null; }
        }
        public static bool IsCharacterInGame
        {
            get { return Player != null && Player.CurrentCharacter != null && Player.CurrentCharacter.Creature != null; }
        }
        public static PlayerCreature PlayerCreature
        {
            get { return Player.CurrentCharacter.Creature; }
        }

        public static void Update()
        {
            if (PlayerSession != null)
                PlayerSession.ProcessPackets();

            if (CurrentMap != null)
                CurrentMap.Update();
        }

        public static Map ChangeMap(ulong guid, MapTemplate template, Environment env)
        {
            if (CurrentMap != null)
                ObjectManager.Unregister(CurrentMap.GUID);
            return CurrentMap = new Map(guid, template, env);
        }
    }
}