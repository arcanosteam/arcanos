﻿using Arcanos.Client.GameClient.Data;
using Arcanos.Client.GameClient.Objects;
using Arcanos.Shared.World;

namespace Arcanos.Client.GameClient.World
{
    public class MapTemplate : MapTemplateBase
    {
        public MapTemplate() { }
        public MapTemplate(float sizeX, float sizeY, int hmapDimX, int hmapDimY, int patchSize) : this(sizeX, sizeY, DEFAULT_GRID_CELL_SIZE, hmapDimX, hmapDimY, patchSize) { }
        public MapTemplate(float sizeX, float sizeY, float cellSize, int hmapDimX, int hmapDimY, int patchSize) : base(sizeX, sizeY, cellSize, hmapDimX, hmapDimY, patchSize) { }

        public override void Apply(MapBase map)
        {
            foreach (StaticModelObject smo in Statics)
                map.Add(smo);
        }

        protected override int GetInitialID()
        {
            return DatabaseManager.Maps.GetID(this);
        }
    }
}