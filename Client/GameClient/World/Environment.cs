using Arcanos.Client.GameClient.Data;
using Arcanos.Shared.Data;
using Arcanos.Shared.Timing;
using Arcanos.Shared.World;

namespace Arcanos.Client.GameClient.World
{
    [SerializationSettings(Method = SerializationMethod.RefByID, ReferenceResolverType = typeof(DatabaseManager), ReferenceResolverMethod = "GetEnvironment")]
    public class Environment : EnvironmentBase
    {
        [SerializationIgnore]
        private EnvironmentBase interpolatingFrom;
        [SerializationIgnore]
        private EnvironmentBase interpolatingTo;
        [SerializationIgnore]
        private float interpolatingTimePassed;

        public Environment() { }
        public Environment(EnvironmentBase source)
        {
            interpolatingFrom = source;
            Clone(source);
        }

        public override void Update(MapBase map)
        {
            if (interpolatingTo != null)
            {
                interpolatingTimePassed += Time.PerSecond;
                if (interpolatingTimePassed >= interpolatingTo.ChangeDuration)
                {
                    Clone(interpolatingTo);
                    interpolatingFrom = interpolatingTo;
                    interpolatingTo = null;
                }
                else
                    Clone(interpolatingFrom, interpolatingTo, interpolatingTimePassed / interpolatingTo.ChangeDuration);
            }
        }

        public void ChangeTo(Environment env)
        {
            interpolatingFrom = new Environment(this);
            interpolatingTo = env;
            interpolatingTimePassed = 0;
        }

        protected override int GetInitialID()
        {
            return DatabaseManager.Environments.GetID(this);
        }
    }
}