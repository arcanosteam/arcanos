﻿using System;
using System.Collections.Generic;
using Arcanos.Client.GameClient.Objects;
using Arcanos.Client.GameClient.Rendering;
using Arcanos.Client.GameClient.UI;
using Arcanos.Shared;
using Arcanos.Shared.Graphics;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Spells;
using Arcanos.Shared.World;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Arcanos.Client.GameClient.World
{
    public class Map : MapBase, IRenderable<TerrainData.TerrainPatch, TerrainRenderGeometry>
    {
        public readonly TerrainRenderGeometry[,] TerrainPatchesRenderGeometry;
        public readonly List<WorldObjectBase> VisibleObjects = new List<WorldObjectBase>();

        public Map(ulong guid, MapTemplate template, Environment env) : base(guid, template)
        {
            Environment = new Environment(env);

            TerrainPatchesRenderGeometry = new TerrainRenderGeometry[template.Terrain.DimX, template.Terrain.DimY];
        }

        public override void Update()
        {
            UpdateCycle++;
            if (WorldManager.IsCharacterInGame && WorldManager.PlayerCreature.Map == this)
                WorldManager.PlayerCreature.Update();
            UpdateCycle--;
            base.Update();
            Grid.UpdateAllCells();
        }

        public IEnumerable<TerrainRenderGeometry> GetRenderContext(GraphicsDevice device)
        {
            for (int pX = 0; pX < Template.Terrain.DimX; ++pX)
                for (int pY = 0; pY < Template.Terrain.DimY; ++pY)
                {
                    float patchX = Template.Terrain.SizeX * (pX + 0.5f) / Template.Terrain.DimX;
                    float patchY = Template.Terrain.SizeY * (pY + 0.5f) / Template.Terrain.DimY;
                    Camera camera = WorldManager.Camera;
                    if (Vector2.Distance(new Vector2(camera.Position.X, camera.Position.Y), new Vector2(patchX, patchY)) > Environment.FogEnd + 1.5f * Math.Max(Template.Terrain.SizeX / Template.Terrain.DimX, Template.Terrain.SizeY / Template.Terrain.DimY) / 2)
                        continue;

                    TerrainRenderGeometry geometry = TerrainPatchesRenderGeometry[pX, pY];
                    TerrainData.TerrainPatch patch = Template.Terrain.Patches[pX, pY];
                    if (geometry == null)
                    {
                        HeightMapData hmd = Template.HeightMap;
                        geometry = new TerrainRenderGeometry((hmd.DimX - 1) * (hmd.DimY - 1) * 2 / 3);
                        int hXFrom = (int)(((float)pX / Template.Terrain.DimX) * hmd.DimX),
                            hXTo = (int)(((float)(pX + 1) / Template.Terrain.DimX) * hmd.DimX),
                            hYFrom = (int)(((float)pY / Template.Terrain.DimY) * hmd.DimY),
                            hYTo = (int)(((float)(pY + 1) / Template.Terrain.DimY) * hmd.DimY);
                        for (int x = hXFrom; x < hXTo; x++)
                        {
                            if (x >= hmd.DimX - 1)
                                break;
                            for (int y = hYFrom; y < hYTo; y++)
                            {
                                if (y >= hmd.DimY - 1)
                                    break;
                                Vector3 nn = hmd.Geometry[x, y];
                                Vector3 np = hmd.Geometry[x, y + 1];
                                Vector3 pp = hmd.Geometry[x + 1, y + 1];
                                Vector3 pn = hmd.Geometry[x + 1, y];
                                Vector3 nnN = hmd.Normals[x, y];
                                Vector3 npN = hmd.Normals[x, y + 1];
                                Vector3 ppN = hmd.Normals[x + 1, y + 1];
                                Vector3 pnN = hmd.Normals[x + 1, y];
                                Vector2 nnT = new Vector2((float)(x - hXFrom) / (hXTo - hXFrom), (float)(hYTo - y) / (hYTo - hYFrom));
                                Vector2 npT = new Vector2((float)(x - hXFrom) / (hXTo - hXFrom), (float)(hYTo - y - 1) / (hYTo - hYFrom));
                                Vector2 ppT = new Vector2((float)(x - hXFrom + 1) / (hXTo - hXFrom), (float)(hYTo - y - 1) / (hYTo - hYFrom));
                                Vector2 pnT = new Vector2((float)(x - hXFrom + 1) / (hXTo - hXFrom), (float)(hYTo - y) / (hYTo - hYFrom));
                                geometry.Add(patch, np, pp, pn, npN, ppN, pnN, npT, ppT, pnT);
                                geometry.Add(patch, nn, np, pn, nnN, npN, pnN, nnT, npT, pnT);
                            }
                        }
                        geometry.UpdateBuffer(device);
                        TerrainPatchesRenderGeometry[pX, pY] = geometry;
                    }
                    yield return geometry;
                }
        }
        
        private readonly object visibleObjectsLock = new object();
        public override void Add(WorldObjectBase wo)
        {
            base.Add(wo);
            lock (visibleObjectsLock)
                VisibleObjects.Add(wo);

            UIManager.RaiseEvent(UIEvents.ObjectEnteredVision, wo);

            if (wo.Type.IsCreature())
                TerrainRenderer.RegisterLight(wo, wo.Template.LightData);
        }
        public override void Remove(WorldObjectBase wo)
        {
            base.Remove(wo);
            lock (visibleObjectsLock)
                VisibleObjects.Remove(wo);

            UIManager.RaiseEvent(UIEvents.ObjectExitedVision, wo);

            if (wo.Type.IsCreature())
                TerrainRenderer.UnregisterLight(wo);
        }
        public override void Add(SpellBase spell)
        {
            base.Add(spell);

            if (spell.Template.LightData.Enabled)
                TerrainRenderer.RegisterLight(spell, spell.Template.LightData);
        }
        public override void Remove(SpellBase spell)
        {
            base.Remove(spell);

            if (spell.Template.LightData.Enabled)
                TerrainRenderer.UnregisterLight(spell);
        }
        public override IEnumerable<WorldObjectBase> GetVisibleObjects(Vector3 pos)
        {
            lock (visibleObjectsLock)
                foreach (WorldObjectBase wo in VisibleObjects)
                    yield return wo;
        }
    }
}