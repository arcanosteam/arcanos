﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Arcanos.Client.GameClient.Rendering;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Arcanos.Client.GameClient.Shaders
{
    public abstract class Shader : IShader
    {
        protected readonly Effect effect;

        protected Shader(GraphicsDevice graphicsDevice, string shaderFile)
        {
            string code;
            using (StreamReader reader = new StreamReader(shaderFile))
                code = reader.ReadToEnd();

            byte[] effectCode;
            string cacheFilename = MD5.Create().ComputeHash(Encoding.ASCII.GetBytes(code)).ToHex(true);
            string cachePath = Path.Combine(Path.GetDirectoryName(shaderFile), cacheFilename + ".fxcache");
            if (!File.Exists(cachePath))
            {
                CompiledEffect compiledEffect = Effect.CompileEffectFromSource(code, null, null, CompilerOptions.None, TargetPlatform.Windows);
                if (!compiledEffect.Success)
                    throw new Exception(compiledEffect.ErrorsAndWarnings);
                effectCode = compiledEffect.GetEffectCode();
                File.WriteAllBytes(cachePath, effectCode);
            }
            else
                effectCode = File.ReadAllBytes(cachePath);

            effect = new Effect(graphicsDevice, effectCode, CompilerOptions.None, GetEffectPool());
            CacheEffectParams(graphicsDevice);
            InitializeEffectParams();
        }

        public virtual void Begin()
        {
            effect.Begin();
            effect.CurrentTechnique.Passes[0].Begin();
        }
        public virtual void End()
        {
            effect.CurrentTechnique.Passes[0].End();
            effect.End();
        }

        protected virtual EffectPool GetEffectPool()
        {
            return null;
        }

        protected abstract void CacheEffectParams(GraphicsDevice graphicsDevice);
        protected abstract void InitializeEffectParams();
    }
}