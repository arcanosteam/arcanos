﻿namespace Arcanos.Client.GameClient.Rendering
{
    public interface IShader
    {
        void Begin();
        void End();
    }
}