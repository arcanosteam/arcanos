#define POINT_LIGHTS_COUNT 32
#define FILTER LINEAR

shared float4x4 World;
shared float4x4 View;
shared float4x4 Projection;
shared float3 CameraPosition;
shared float2 FogBounds;
shared float4 FogColor;

shared float4 AmbientLight;
shared float4 PointLightsColor[POINT_LIGHTS_COUNT];
shared float4 PointLightsPosition[POINT_LIGHTS_COUNT];

Texture BaseTexture;
float4 Wind;
float2 WindInfluenceBounds;

sampler BaseTextureSampler = sampler_state { texture = <BaseTexture>; magfilter = FILTER; minfilter = FILTER; mipfilter = FILTER; AddressU = wrap; AddressV = wrap; };

struct VertexShaderInput
{
    float4 Position : POSITION0;
    float4 Normal : NORMAL0;
    float2 TexCoords : TEXCOORD0;
};

struct VertexShaderOutput
{
    float4 Position : POSITION0;
    float2 TexCoords : TEXCOORD0;
    float4 WorldPosition : TEXCOORD1;
    float3 Normal : TEXCOORD2;
};

VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
    VertexShaderOutput output;

    float4 worldPosition = mul(input.Position, World);
    if (worldPosition.z > WindInfluenceBounds.x)
    {
        float influence = (worldPosition.z - WindInfluenceBounds.x) / (WindInfluenceBounds.y - WindInfluenceBounds.x);
        worldPosition.xy += Wind.xy * (sin(sin((worldPosition.x + worldPosition.y) / Wind.z) + Wind.w) * 0.6 + 0.4) * influence;
    }
    float4 viewPosition = mul(worldPosition, View);
    output.Position = mul(viewPosition, Projection);
    output.TexCoords = input.TexCoords;
    output.WorldPosition = worldPosition;
    output.WorldPosition.w = clamp((length(CameraPosition - worldPosition) - FogBounds.x) / (FogBounds.y - FogBounds.x), 0, 1);
    output.Normal = mul(input.Normal, World);

    return output;
}

float4 PixelShaderFunction(VertexShaderOutput input) : COLOR0
{
    float4 texColor = tex2D(BaseTextureSampler, input.TexCoords);

    float3 lightColor = 0;
    for (int i = 0; i < POINT_LIGHTS_COUNT; ++i)
    {
        float3 dir = PointLightsPosition[i].xyz - input.WorldPosition.xyz;
        float dist = length(dir);
        if (dist < PointLightsPosition[i].w)
        {
            float falloff = 1 - dist / PointLightsPosition[i].w;
            lightColor += texColor * PointLightsColor[i].rgb * PointLightsColor[i].a * falloff * falloff * falloff * falloff;
        }
    }
    lightColor += texColor * AmbientLight;
    
    lightColor = lerp(lightColor, FogColor.rgb, input.WorldPosition.w * FogColor.a);

    return float4(lightColor, texColor.a);
}

technique ModelShader
{
    pass ModelShaderPass
    {
        VertexShader = compile vs_1_1 VertexShaderFunction();
        PixelShader = compile ps_3_0 PixelShaderFunction();
    }
}
