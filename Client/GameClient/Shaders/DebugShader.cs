﻿using Microsoft.Xna.Framework.Graphics;

namespace Arcanos.Client.GameClient.Shaders
{
    public class DebugShader : WorldSpaceShader
    {
        public DebugShader(GraphicsDevice graphicsDevice) : base(graphicsDevice, "Shaders\\Debug.fx") { }

        protected override void CacheEffectParams(GraphicsDevice device)
        {
            base.CacheEffectParams(device);
            effect.CurrentTechnique = effect.Techniques["DebugShader"];
        }
    }
}