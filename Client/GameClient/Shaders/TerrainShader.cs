﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Arcanos.Client.GameClient.Shaders
{
    public class TerrainShader : WorldSpaceShader
    {
        public static readonly EffectPool SharedEffectPool = new EffectPool();

        public struct PointLight
        {
            public bool Enabled
            {
                get { return shader.pointLightsPosition[index].W > 0; }
                set
                {
                    if (!value)
                        shader.pointLightsPosition[index].W = 0;
                    shader.lightsUpdated = true;
                }
            }
            public Vector4 Color
            {
                get { return shader.pointLightsColor[index]; }
                set
                {
                    if (shader.pointLightsColor[index] == value)
                        return;
                    shader.pointLightsColor[index] = value;
                    shader.lightsUpdated = true;
                }
            }
            public Vector3 Position
            {
                get
                {
                    Vector4 v = shader.pointLightsPosition[index];
                    return new Vector3(v.X, v.Y, v.Z);
                }
                set
                {
                    Vector4 v = shader.pointLightsPosition[index];
                    if (v.X == value.X && v.Y == value.Y && v.Z == value.Z)
                        return;
                    shader.pointLightsPosition[index].X = value.X;
                    shader.pointLightsPosition[index].Y = value.Y;
                    shader.pointLightsPosition[index].Z = value.Z;
                    shader.lightsUpdated = true;
                }
            }
            public float Radius
            {
                get { return shader.pointLightsPosition[index].W; }
                set
                {
                    if (shader.pointLightsPosition[index].W == value)
                        return;
                    shader.pointLightsPosition[index].W = value;
                    shader.lightsUpdated = true;
                }
            }

            private readonly TerrainShader shader;
            private readonly int index;

            public PointLight(TerrainShader shader, int index)
            {
                this.shader = shader;
                this.index = index;
            }
        }

        private EffectParameter cameraPositionParam;
        private EffectParameter fogBoundsParam;
        private EffectParameter fogColorParam;

        private EffectParameter baseTextureParam;
        private EffectParameter normalTextureParam;
        private EffectParameter layer0TextureParam;
        private EffectParameter layer1TextureParam;
        private EffectParameter layer2TextureParam;
        private EffectParameter layer3TextureParam;
        private EffectParameter normal0TextureParam;
        private EffectParameter normal1TextureParam;
        private EffectParameter normal2TextureParam;
        private EffectParameter normal3TextureParam;
        private EffectParameter alpha0TextureParam;
        private EffectParameter alpha1TextureParam;
        private EffectParameter alpha2TextureParam;
        private EffectParameter alpha3TextureParam;
        private EffectParameter layersCountParam;
        private EffectParameter textureScaleParam;
        private EffectParameter pointLightsColorParam;
        private EffectParameter pointLightsPositionParam;
        private EffectParameter ambientLightParam;

        public const int POINT_LIGHTS_COUNT = 32;
        private bool lightsUpdated;
        private readonly Vector4[] pointLightsColor;
        private readonly Vector4[] pointLightsPosition;
        public readonly PointLight[] PointLights = new PointLight[POINT_LIGHTS_COUNT];

        public Vector3 CameraPosition
        {
            get { return cameraPositionParam.GetValueVector3(); }
            set { cameraPositionParam.SetValue(value); }
        }
        public Vector2 FogBounds
        {
            get { return fogBoundsParam.GetValueVector2(); }
            set { fogBoundsParam.SetValue(value); }
        }
        public Vector4 FogColor
        {
            get { return fogColorParam.GetValueVector4(); }
            set { fogColorParam.SetValue(value); }
        }

        public Texture2D BaseTexture
        {
            get { return baseTextureParam.GetValueTexture2D(); }
            set { baseTextureParam.SetValue(value); }
        }
        public Texture2D NormalTexture
        {
            get { return normalTextureParam.GetValueTexture2D(); }
            set { normalTextureParam.SetValue(value); }
        }
        public Texture2D Layer0Texture
        {
            get { return layer0TextureParam.GetValueTexture2D(); }
            set { layer0TextureParam.SetValue(value); }
        }
        public Texture2D Layer1Texture
        {
            get { return layer1TextureParam.GetValueTexture2D(); }
            set { layer1TextureParam.SetValue(value); }
        }
        public Texture2D Layer2Texture
        {
            get { return layer2TextureParam.GetValueTexture2D(); }
            set { layer2TextureParam.SetValue(value); }
        }
        public Texture2D Layer3Texture
        {
            get { return layer3TextureParam.GetValueTexture2D(); }
            set { layer3TextureParam.SetValue(value); }
        }
        public Texture2D Normal0Texture
        {
            get { return normal0TextureParam.GetValueTexture2D(); }
            set { normal0TextureParam.SetValue(value); }
        }
        public Texture2D Normal1Texture
        {
            get { return normal1TextureParam.GetValueTexture2D(); }
            set { normal1TextureParam.SetValue(value); }
        }
        public Texture2D Normal2Texture
        {
            get { return normal2TextureParam.GetValueTexture2D(); }
            set { normal2TextureParam.SetValue(value); }
        }
        public Texture2D Normal3Texture
        {
            get { return normal3TextureParam.GetValueTexture2D(); }
            set { normal3TextureParam.SetValue(value); }
        }
        public Texture2D Alpha0Texture
        {
            get { return alpha0TextureParam.GetValueTexture2D(); }
            set { alpha0TextureParam.SetValue(value); }
        }
        public Texture2D Alpha1Texture
        {
            get { return alpha1TextureParam.GetValueTexture2D(); }
            set { alpha1TextureParam.SetValue(value); }
        }
        public Texture2D Alpha2Texture
        {
            get { return alpha2TextureParam.GetValueTexture2D(); }
            set { alpha2TextureParam.SetValue(value); }
        }
        public Texture2D Alpha3Texture
        {
            get { return alpha3TextureParam.GetValueTexture2D(); }
            set { alpha3TextureParam.SetValue(value); }
        }
        public int LayersCount
        {
            get { return layersCountParam.GetValueInt32(); }
            set { layersCountParam.SetValue(value); }
        }
        public float TextureScale
        {
            get { return textureScaleParam.GetValueSingle(); }
            set { textureScaleParam.SetValue(value); }
        }
        public Vector4 AmbientLight
        {
            get { return ambientLightParam.GetValueVector4(); }
            set { ambientLightParam.SetValue(value); }
        }

        public TerrainShader(GraphicsDevice graphicsDevice) : base(graphicsDevice, "Shaders\\Terrain.fx")
        {
            for (int i = 0; i < POINT_LIGHTS_COUNT; i++)
                PointLights[i] = new PointLight(this, i);
            pointLightsColor = pointLightsColorParam.GetValueVector4Array(POINT_LIGHTS_COUNT);
            pointLightsPosition = pointLightsPositionParam.GetValueVector4Array(POINT_LIGHTS_COUNT);
        }

        public override void Begin()
        {
            if (lightsUpdated)
            {
                pointLightsColorParam.SetValue(pointLightsColor);
                pointLightsPositionParam.SetValue(pointLightsPosition);
            }
 	        base.Begin();
        }

        protected override EffectPool GetEffectPool()
        {
            return SharedEffectPool;
        }

        protected override void CacheEffectParams(GraphicsDevice device)
        {
            base.CacheEffectParams(device);

            cameraPositionParam = effect.Parameters["CameraPosition"];
            fogBoundsParam = effect.Parameters["FogBounds"];
            fogColorParam = effect.Parameters["FogColor"];

            baseTextureParam = effect.Parameters["BaseTexture"];
            normalTextureParam = effect.Parameters["NormalTexture"];
            layer0TextureParam = effect.Parameters["Layer0Texture"];
            layer1TextureParam = effect.Parameters["Layer1Texture"];
            layer2TextureParam = effect.Parameters["Layer2Texture"];
            layer3TextureParam = effect.Parameters["Layer3Texture"];
            normal0TextureParam = effect.Parameters["Normal0Texture"];
            normal1TextureParam = effect.Parameters["Normal1Texture"];
            normal2TextureParam = effect.Parameters["Normal2Texture"];
            normal3TextureParam = effect.Parameters["Normal3Texture"];
            alpha0TextureParam = effect.Parameters["Alpha0Texture"];
            alpha1TextureParam = effect.Parameters["Alpha1Texture"];
            alpha2TextureParam = effect.Parameters["Alpha2Texture"];
            alpha3TextureParam = effect.Parameters["Alpha3Texture"];
            layersCountParam = effect.Parameters["LayersCount"];
            textureScaleParam = effect.Parameters["TextureScale"];
            pointLightsColorParam = effect.Parameters["PointLightsColor"];
            pointLightsPositionParam = effect.Parameters["PointLightsPosition"];
            ambientLightParam = effect.Parameters["AmbientLight"];
            effect.CurrentTechnique = effect.Techniques["TerrainShader"];
        }
        protected override void InitializeEffectParams()
        {
            base.InitializeEffectParams();
            AmbientLight = Vector4.One;
            TextureScale = 1;
        }
    }
}