#define DECALS_COUNT 16
#define DECAL(i) Texture DecalTexture##i##; float4 DecalPosition##i##; sampler DecalTextureSampler##i## = sampler_state { texture = <DecalTexture##i##>; magfilter = LINEAR; minfilter = LINEAR; mipfilter = LINEAR; AddressU = clamp; AddressV = clamp; };

shared float4x4 World;
shared float4x4 View;
shared float4x4 Projection;

float ZDistribution;

DECAL(0) DECAL(1) DECAL(2) DECAL(3) DECAL(4) DECAL(5) DECAL(6) DECAL(7) DECAL(8) DECAL(9) DECAL(10) DECAL(11) DECAL(12) DECAL(13) DECAL(14) DECAL(15)

struct VertexShaderInput
{
    float4 Position : POSITION0;
    float2 TexCoords : TEXCOORD0;
};

struct VertexShaderOutput
{
    float4 Position : POSITION0;
    float2 TexCoords : TEXCOORD0;
    float3 VSPosition : TEXCOORD1;
};

struct PixelShaderInput
{
    float2 TexCoords : TEXCOORD0;
    float3 Position : TEXCOORD1;
};


VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
    VertexShaderOutput output;

    float4 worldPosition = mul(input.Position, World);
    float4 viewPosition = mul(worldPosition, View);
    output.Position = mul(viewPosition, Projection);
    output.TexCoords = input.TexCoords;
    output.VSPosition = worldPosition.xyz;

    return output;
}

#define ITERATION(i) \
if (DecalPosition##i##.w > 0 && \
    input.Position.x >= DecalPosition##i##.x-DecalPosition##i##.w && \
    input.Position.x <= DecalPosition##i##.x+DecalPosition##i##.w && \
    input.Position.y >= DecalPosition##i##.y-DecalPosition##i##.w && \
    input.Position.y <= DecalPosition##i##.y+DecalPosition##i##.w) \
{ \
    float mul = (1 - abs(input.Position.z - DecalPosition##i##.z) / ZDistribution); \
    mul = clamp(mul * 2, 0, 1); \
    mul = sqrt(mul); \
    float4 decalColor = tex2D(DecalTextureSampler##i##, 0.5f+((input.Position.xy-DecalPosition##i##.xy)/(DecalPosition##i##.w*2))); \
    decalColor.a *= mul; \
    if (texColor.a == 0) \
        texColor.rgb = decalColor.rgb; \
    else \
        texColor.rgb = texColor.rgb * (1 - decalColor.a) + decalColor.rgb * decalColor.a; \
    texColor.a += decalColor.a; \
}

float4 PixelShaderFunction(PixelShaderInput input) : COLOR0
{
    float4 texColor = float4(0,0,0,0);
    ITERATION(0)
    ITERATION(1)
    ITERATION(2)
    ITERATION(3)
    ITERATION(4)
    ITERATION(5)
    ITERATION(6)
    ITERATION(7)
    ITERATION(8)
    ITERATION(9)
    ITERATION(10)
    ITERATION(11)
    ITERATION(12)
    ITERATION(13)
    ITERATION(14)
    ITERATION(15)
    return texColor;
}

technique ProjectedDecals
{
    pass ProjectedDecalsPass
    {
        VertexShader = compile vs_1_1 VertexShaderFunction();
        PixelShader = compile ps_3_0 PixelShaderFunction();
    }
}
