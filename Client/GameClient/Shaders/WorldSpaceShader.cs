﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Arcanos.Client.GameClient.Shaders
{
    public abstract class WorldSpaceShader : Shader
    {
        private EffectParameter worldParam;
        private EffectParameter viewParam;
        private EffectParameter projectionParam;

        public Matrix World
        {
            get { return worldParam.GetValueMatrix(); }
            set { worldParam.SetValue(value); }
        }
        public Matrix View
        {
            get { return viewParam.GetValueMatrix(); }
            set { viewParam.SetValue(value); }
        }
        public Matrix Projection
        {
            get { return projectionParam.GetValueMatrix(); }
            set { projectionParam.SetValue(value); }
        }

        protected WorldSpaceShader(GraphicsDevice graphicsDevice, string shaderFile) : base(graphicsDevice, shaderFile) { }

        protected override void CacheEffectParams(GraphicsDevice device)
        {
            worldParam = effect.Parameters["World"];
            viewParam = effect.Parameters["View"];
            projectionParam = effect.Parameters["Projection"];
        }
        protected override void InitializeEffectParams()
        {
            World = Matrix.Identity;
            View = Matrix.Identity;
            Projection = Matrix.Identity;
        }
    }
}