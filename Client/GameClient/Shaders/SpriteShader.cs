﻿using Microsoft.Xna.Framework.Graphics;

namespace Arcanos.Client.GameClient.Shaders
{
    public class SpriteShader : WorldSpaceShader
    {
        private EffectParameter basicTextureParam;

        public Texture2D Texture
        {
            get { return basicTextureParam.GetValueTexture2D(); }
            set { basicTextureParam.SetValue(value); }
        }

        public SpriteShader(GraphicsDevice graphicsDevice) : base(graphicsDevice, "Shaders\\Sprite.fx") { }

        protected override EffectPool GetEffectPool()
        {
            return TerrainShader.SharedEffectPool;
        }

        protected override void CacheEffectParams(GraphicsDevice device)
        {
            base.CacheEffectParams(device);
            basicTextureParam = effect.Parameters["Texture"];
            effect.CurrentTechnique = effect.Techniques["SpriteShader"];
        }
    }
}