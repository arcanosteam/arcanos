﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Arcanos.Client.GameClient.Shaders
{
    public class ModelShader : WorldSpaceShader
    {
        public int LastTextureID;

        private EffectParameter baseTextureParam;
        private EffectParameter windParam;
        private EffectParameter windInfluenceBoundsParam;

        public Texture2D BaseTexture
        {
            get { return baseTextureParam.GetValueTexture2D(); }
            set { baseTextureParam.SetValue(value); }
        }

        public ModelShader(GraphicsDevice graphicsDevice) : base(graphicsDevice, "Shaders\\Model.fx") { }

        public void SetWind(Vector2 wind, float distribution, float period)
        {
            windParam.SetValue(new Vector4(wind, distribution, period));
        }
        public void SetWind(Vector3 wind, float period)
        {
            windParam.SetValue(new Vector4(wind, period));
        }
        public void SetInfluenceBounds(float minZ, float maxZ)
        {
            windInfluenceBoundsParam.SetValue(new Vector2(minZ, maxZ));
        }

        protected override EffectPool GetEffectPool()
        {
            return TerrainShader.SharedEffectPool;
        }

        protected override void CacheEffectParams(GraphicsDevice device)
        {
            base.CacheEffectParams(device);
            baseTextureParam = effect.Parameters["BaseTexture"];
            windParam = effect.Parameters["Wind"];
            windInfluenceBoundsParam = effect.Parameters["WindInfluenceBounds"];
            effect.CurrentTechnique = effect.Techniques["ModelShader"];
        }
    }
}