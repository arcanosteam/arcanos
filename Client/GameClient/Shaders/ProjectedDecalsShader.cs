﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Arcanos.Client.GameClient.Shaders
{
    public class ProjectedDecalsShader : WorldSpaceShader
    {
        private EffectParameter zDistributionParam;
        private readonly EffectParameter[] decalTexturesParam = new EffectParameter[DECAL_COUNT];
        private readonly EffectParameter[] decalPositionsParam = new EffectParameter[DECAL_COUNT];
        private readonly bool[] decalsUsed = new bool[DECAL_COUNT];

        public const int DECAL_COUNT = 16;

        public ProjectedDecalsShader(GraphicsDevice graphicsDevice) : base(graphicsDevice, "Shaders\\ProjectedDecals.fx") { }

        public int GetFreeIndex()
        {
            for (int i = 0; i < DECAL_COUNT; ++i)
                if (!decalsUsed[i])
                    return i;
            return -1;
        }
        public void SetDecal(int index, Texture2D texture, Vector3 position, float radius)
        {
            decalTexturesParam[index].SetValue(texture);
            decalPositionsParam[index].SetValue(new Vector4(position, radius));
            decalsUsed[index] = true;
        }
        public void MoveDecal(int index, Vector3 position)
        {
            decalPositionsParam[index].SetValue(new Vector4(position, decalPositionsParam[index].GetValueVector4().W));
        }
        public void ResizeDecal(int index, float radius)
        {
            Vector4 vec = decalPositionsParam[index].GetValueVector4();
            decalPositionsParam[index].SetValue(new Vector4(vec.X, vec.Y, vec.Z, radius));
        }
        public void RemoveDecal(int index)
        {
            decalTexturesParam[index].SetValue((Texture2D)null);
            decalPositionsParam[index].SetValue(Vector4.Zero);
            decalsUsed[index] = false;
        }
        public void SetDecalZDistribution(float distribution)
        {
            zDistributionParam.SetValue(distribution);
        }

        protected override EffectPool GetEffectPool()
        {
            return TerrainShader.SharedEffectPool;
        }

        protected override void CacheEffectParams(GraphicsDevice device)
        {
            base.CacheEffectParams(device);
            zDistributionParam = effect.Parameters["ZDistribution"];
            for (int i = 0; i < DECAL_COUNT; ++i)
            {
                decalTexturesParam[i] = effect.Parameters["DecalTexture" + i];
                decalPositionsParam[i] = effect.Parameters["DecalPosition" + i];
            }
            effect.CurrentTechnique = effect.Techniques["ProjectedDecals"];
        }
        protected override void InitializeEffectParams()
        {
            base.InitializeEffectParams();
            zDistributionParam.SetValue(1f);
        }
    }
}