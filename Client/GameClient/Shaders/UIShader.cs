﻿using Microsoft.Xna.Framework.Graphics;

namespace Arcanos.Client.GameClient.Shaders
{
    public class UIShader : WorldSpaceShader
    {
        private EffectParameter basicTextureParam;
        private EffectParameter alphaParam;

        public Texture2D Texture
        {
            get { return basicTextureParam.GetValueTexture2D(); }
            set { basicTextureParam.SetValue(value); }
        }
        public float Alpha
        {
            get { return alphaParam.GetValueSingle(); }
            set { alphaParam.SetValue(value); }
        }

        public UIShader(GraphicsDevice graphicsDevice) : base(graphicsDevice, "Shaders\\UI.fx") { }

        protected override void CacheEffectParams(GraphicsDevice device)
        {
            base.CacheEffectParams(device);
            basicTextureParam = effect.Parameters["Texture"];
            alphaParam = effect.Parameters["Alpha"];
            effect.CurrentTechnique = effect.Techniques["UIShader"];
        }
    }
}