#define POINT_LIGHTS_COUNT 32
#define FILTER LINEAR

shared float4x4 World;
shared float4x4 View;
shared float4x4 Projection;
shared float3 CameraPosition;
shared float2 FogBounds;
shared float4 FogColor;

shared float4 AmbientLight;
shared float4 PointLightsColor[POINT_LIGHTS_COUNT];
shared float4 PointLightsPosition[POINT_LIGHTS_COUNT];

Texture BaseTexture;
Texture NormalTexture;
Texture Layer0Texture;
Texture Layer1Texture;
Texture Layer2Texture;
Texture Layer3Texture;
Texture Normal0Texture;
Texture Normal1Texture;
Texture Normal2Texture;
Texture Normal3Texture;
Texture Alpha0Texture;
Texture Alpha1Texture;
Texture Alpha2Texture;
Texture Alpha3Texture;
float TextureScale;
int LayersCount;

sampler BaseTextureSampler = sampler_state { texture = <BaseTexture>; magfilter = FILTER; minfilter = FILTER; mipfilter = FILTER; AddressU = wrap; AddressV = wrap; };
sampler NormalTextureSampler = sampler_state { texture = <NormalTexture>; magfilter = FILTER; minfilter = FILTER; mipfilter = FILTER; AddressU = wrap; AddressV = wrap; };
sampler Layer0TextureSampler = sampler_state { texture = <Layer0Texture>; magfilter = FILTER; minfilter = FILTER; mipfilter = FILTER; AddressU = wrap; AddressV = wrap; };
sampler Layer1TextureSampler = sampler_state { texture = <Layer1Texture>; magfilter = FILTER; minfilter = FILTER; mipfilter = FILTER; AddressU = wrap; AddressV = wrap; };
sampler Layer2TextureSampler = sampler_state { texture = <Layer2Texture>; magfilter = FILTER; minfilter = FILTER; mipfilter = FILTER; AddressU = wrap; AddressV = wrap; };
sampler Layer3TextureSampler = sampler_state { texture = <Layer3Texture>; magfilter = FILTER; minfilter = FILTER; mipfilter = FILTER; AddressU = wrap; AddressV = wrap; };
sampler Normal0TextureSampler = sampler_state { texture = <Normal0Texture>; magfilter = FILTER; minfilter = FILTER; mipfilter = FILTER; AddressU = wrap; AddressV = wrap; };
sampler Normal1TextureSampler = sampler_state { texture = <Normal1Texture>; magfilter = FILTER; minfilter = FILTER; mipfilter = FILTER; AddressU = wrap; AddressV = wrap; };
sampler Normal2TextureSampler = sampler_state { texture = <Normal2Texture>; magfilter = FILTER; minfilter = FILTER; mipfilter = FILTER; AddressU = wrap; AddressV = wrap; };
sampler Normal3TextureSampler = sampler_state { texture = <Normal3Texture>; magfilter = FILTER; minfilter = FILTER; mipfilter = FILTER; AddressU = wrap; AddressV = wrap; };
sampler Alpha0TextureSampler = sampler_state { texture = <Alpha0Texture>; magfilter = FILTER; minfilter = FILTER; mipfilter = FILTER; AddressU = clamp; AddressV = clamp; };
sampler Alpha1TextureSampler = sampler_state { texture = <Alpha1Texture>; magfilter = FILTER; minfilter = FILTER; mipfilter = FILTER; AddressU = clamp; AddressV = clamp; };
sampler Alpha2TextureSampler = sampler_state { texture = <Alpha2Texture>; magfilter = FILTER; minfilter = FILTER; mipfilter = FILTER; AddressU = clamp; AddressV = clamp; };
sampler Alpha3TextureSampler = sampler_state { texture = <Alpha3Texture>; magfilter = FILTER; minfilter = FILTER; mipfilter = FILTER; AddressU = clamp; AddressV = clamp; };

struct VertexShaderInput
{
    float4 Position : POSITION0;
    float4 Normal : NORMAL0;
    float3 Tangent : TANGENT0;
    float3 Binormal : BINORMAL0;
    float2 TexCoords : TEXCOORD0;
};

struct VertexShaderOutput
{
    float4 Position : POSITION0;
    float2 TexCoords : TEXCOORD0;
    float4 WorldPosition : TEXCOORD1;
    float3 Normal : TEXCOORD2;
    float3 Tangent : TEXCOORD3;
    float3 Binormal : TEXCOORD4;
};

VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
    VertexShaderOutput output;

    float4 worldPosition = mul(input.Position, World);
    float4 viewPosition = mul(worldPosition, View);
    output.Position = mul(viewPosition, Projection);
    output.TexCoords = input.TexCoords;
    output.WorldPosition = worldPosition;
    output.WorldPosition.w = clamp((length(CameraPosition - worldPosition) - FogBounds.x) / (FogBounds.y - FogBounds.x), 0, 1);
    output.Normal = mul(input.Normal, World);
    output.Tangent = mul(input.Tangent, World);
    output.Binormal = mul(input.Binormal, World);

    return output;
}

float4 PixelShaderFunction(VertexShaderOutput input) : COLOR0
{
    float2 coords = input.TexCoords * TextureScale;
    float3 texColor = tex2D(BaseTextureSampler, coords).rgb;
    float4 tex;
    float alpha;
    float3 bump = tex2D(NormalTextureSampler, coords).rgb;

    if (LayersCount > 0)
    {
        tex = tex2D(Layer0TextureSampler, coords);
        alpha = tex2D(Alpha0TextureSampler, input.TexCoords).r;
        alpha = clamp(clamp(alpha, 0.0, 0.5) * 2 * tex.a + 2 * (clamp(alpha, 0.5, 1.0) - 0.5), 0, 1);
        texColor = texColor * (1 - alpha) + tex.rgb * alpha;
        bump = bump * (1 - alpha) + (tex2D(Normal0TextureSampler, input.TexCoords * TextureScale).rgb - (0.5, 0.5, 0.5)) * alpha;
    }
    if (LayersCount > 1)
    {
        tex = tex2D(Layer1TextureSampler, coords);
        alpha = tex2D(Alpha1TextureSampler, input.TexCoords).r;
        alpha = clamp(clamp(alpha, 0.0, 0.5) * 2 * tex.a + 1 * (clamp(alpha, 0, 1.0) - 0), 0, 1);
        texColor = texColor * (1 - alpha) + tex.rgb * alpha;
        bump = bump * (1 - alpha) + (tex2D(Normal1TextureSampler, input.TexCoords * TextureScale).rgb - (0.5, 0.5, 0.5)) * alpha;
    }
    if (LayersCount > 2)
    {
        tex = tex2D(Layer2TextureSampler, coords);
        alpha = tex2D(Alpha2TextureSampler, input.TexCoords).r;
        alpha = clamp(clamp(alpha, 0.0, 0.5) * 2 * tex.a + 2 * (clamp(alpha, 0.5, 1.0) - 0.5), 0, 1);
        texColor = texColor * (1 - alpha) + tex.rgb * alpha;
        bump = bump * (1 - alpha) + (tex2D(Normal2TextureSampler, input.TexCoords * TextureScale).rgb - (0.5, 0.5, 0.5)) * alpha;
    }
    if (LayersCount > 3)
    {
        tex = tex2D(Layer3TextureSampler, coords);
        alpha = tex2D(Alpha3TextureSampler, input.TexCoords).r;
        alpha = clamp(clamp(alpha, 0.0, 0.5) * 2 * tex.a + 2 * (clamp(alpha, 0.5, 1.0) - 0.5), 0, 1);
        texColor = texColor * (1 - alpha) + tex.rgb * alpha;
        bump = bump * (1 - alpha) + (tex2D(Normal3TextureSampler, input.TexCoords * TextureScale).rgb - (0.5, 0.5, 0.5)) * alpha;
    }

    float3 bumpNormal = normalize(input.Normal + bump.x * input.Tangent - bump.y * input.Binormal);

    float3 lightColor = 0;
    float diffuseIntensity = 0;
    for (int i = 0; i < POINT_LIGHTS_COUNT; ++i)
    {
        float3 dir = PointLightsPosition[i].xyz - input.WorldPosition.xyz;
        float dist = length(dir);
        if (dist < PointLightsPosition[i].w)
        {
            float falloff = 1 - dist / PointLightsPosition[i].w;
            diffuseIntensity += clamp(dot(normalize(dir), bumpNormal) * falloff, 0, 1);
            lightColor += texColor * PointLightsColor[i].rgb * PointLightsColor[i].a * falloff * falloff;
        }
    }
    lightColor *= diffuseIntensity;
    lightColor += texColor * AmbientLight;
    
    lightColor = lerp(lightColor, FogColor.rgb, input.WorldPosition.w * FogColor.a);

    return float4(lightColor, 1);
}

technique TerrainShader
{
    pass TerrainShaderPass
    {
        VertexShader = compile vs_1_1 VertexShaderFunction();
        PixelShader = compile ps_3_0 PixelShaderFunction();
    }
}
