﻿using System;
using System.Collections.Generic;
using Arcanos.Shared.Data;

namespace Arcanos.Client.GameClient.Data
{
    public class PlaceholderMemoryDatabase<T> : MemoryDatabase<T>
    {
        public T Placeholder = default(T);
        public bool UsePlaceholder = true;
        public event Func<int, T> PlaceholderHandler;
        public event Action<int> RequestHandler; 

        private readonly List<int> requested = new List<int>(); 

        public override T this[int id]
        {
            get
            {
                if (!UsePlaceholder)
                    return base[id];

                T asset;
                if (Assets.TryGetValue(id, out asset))
                    return asset;

                if (!requested.Contains(id))
                {
                    requested.Add(id);
                    RequestHandler(id);
                }
                return PlaceholderHandler != null ? PlaceholderHandler(id) : Placeholder;
            }
        }

        public override int Add(int id, T asset)
        {
            if (UsePlaceholder)
                requested.Remove(id);
            return base.Add(id, asset);
        }
    }
}