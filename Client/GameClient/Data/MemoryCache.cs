﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;
using Arcanos.Net;
using Arcanos.Shared.Data;
using Arcanos.Shared.Net;

namespace Arcanos.Client.GameClient.Data
{
    public class MemoryCache<T, TQueryPacket> : MemoryDatabase<T>, ICache<T> where TQueryPacket : IWorldPacket
    {
        private struct CacheQueryPacket : IWorldPacket
        {
            private int id;

            public CacheQueryPacket(int id)
            {
                this.id = id;
            }
        }

        protected readonly Dictionary<int, CacheLoadedHandler<T>> Requests = new Dictionary<int, CacheLoadedHandler<T>>();
        protected ConnectionManager<WorldPackets, IWorldPacket> Connection;

        public bool Connected
        {
            get { return true; }
        }
        public override T this[int id]
        {
            get
            {
                throw new Exception();
            }
        }

        private readonly object requestsLock = new object();

        public T Get(int id)
        {
            lock (threadLock)
            {
                if (Assets.ContainsKey(id))
                    return Assets[id];
            }

            Connection.Write(CastPacket<TQueryPacket>(new CacheQueryPacket(id)));
            while (!Assets.ContainsKey(id))
                Thread.Sleep(5);
            return Assets[id];
        }
        public void Get(int id, CacheLoadedHandler<T> loadedHandler)
        {
            lock (threadLock)
            {
                if (Assets.ContainsKey(id))
                    loadedHandler(Assets[id]);
                else
                {
                    CacheLoadedHandler<T> handlers;
                    lock (requestsLock)
                        if (Requests.TryGetValue(id, out handlers))
                            Requests[id] = handlers + loadedHandler;
                        else
                            Requests.Add(id, loadedHandler);

                    Connection.Write(CastPacket<TQueryPacket>(new CacheQueryPacket(id)));
                }
            }
        }

        public bool Connect(ConnectionManager<WorldPackets, IWorldPacket> connection)
        {
            Connection = connection;
            return base.Connect();
        }

        public override int Add(int id, T asset)
        {
            lock (threadLock)
            {
                int result = base.Add(id, asset);

                CacheLoadedHandler<T> handlers;
                bool found;
                lock (requestsLock)
                    found = Requests.TryGetValue(id, out handlers);
                if (found)
                {
                    handlers(asset);
                    lock (requestsLock)
                        Requests.Remove(id);
                }

                return result;
            }
        }

        static TPacket CastPacket<TPacket>(IWorldPacket packet)
        {
            GCHandle handle = GCHandle.Alloc(packet, GCHandleType.Pinned);
            TPacket typedStruct = (TPacket)Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(TPacket));
            handle.Free();
            return typedStruct;
        }
    }
}