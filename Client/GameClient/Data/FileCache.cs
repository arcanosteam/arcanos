﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using Arcanos.Net;
using Arcanos.Shared.Data;
using Arcanos.Shared.Net;

namespace Arcanos.Client.GameClient.Data
{
    public class FileCache<T, TQueryPacket> : FileDatabase<T>, ICache<T> where TQueryPacket : IWorldPacket
    {
        private struct CacheQueryPacket : IWorldPacket
        {
            private int id;

            public CacheQueryPacket(int id)
            {
                this.id = id;
            }
        }

        protected readonly Dictionary<int, CacheLoadedHandler<T>> Requests = new Dictionary<int, CacheLoadedHandler<T>>();
        protected readonly List<int> Requested = new List<int>();
        protected ConnectionManager<WorldPackets, IWorldPacket> Connection;

        public override T this[int id]
        {
            get
            {
                throw new Exception();
            }
        }

        private readonly object requestsLock = new object();

        public FileCache(string filename, char[] magic) : this(filename, magic, Encoding.UTF8) { }
        public FileCache(string filename, char[] magic, Encoding encoding) : base(filename, magic, encoding) { }

        public virtual T Get(int id)
        {
            lock (threadLock)
            {
                if (!Requested.Contains(id))
                {
                    if (Assets.ContainsKey(id))
                        return Assets[id];

                    if (Load(id))
                        return Assets[id];

                    Requested.Add(id);
                    Request(id);
                }
            }
            while (!Assets.ContainsKey(id))
                Thread.Sleep(5);
            return Assets[id];
        }
        public virtual void Get(int id, CacheLoadedHandler<T> loadedHandler)
        {
            lock (threadLock)
            {
                if (Assets.ContainsKey(id))
                {
                    if (loadedHandler != null)
                        loadedHandler(Assets[id]);
                }
                else if (Load(id))
                {
                    if (loadedHandler != null)
                        loadedHandler(Assets[id]);
                }
                else
                {
                    if (loadedHandler != null)
                    {
                        lock (requestsLock)
                        {
                            CacheLoadedHandler<T> handlers;
                            if (Requests.TryGetValue(id, out handlers))
                                Requests[id] = handlers + loadedHandler;
                            else
                            {
                                Requests.Add(id, loadedHandler);
                                if (!Requested.Contains(id))
                                    Request(id);
                            }
                        }
                    }
                }
            }
        }

        public virtual void Request(int id)
        {
            Connection.Write(CastPacket<TQueryPacket>(new CacheQueryPacket(id)));
        }

        public virtual bool Connect(ConnectionManager<WorldPackets, IWorldPacket> connection)
        {
            Connection = connection;
            return base.Connect();
        }

        public override int Add(int id, T asset)
        {
            int result;
            lock (threadLock)
                result = base.Add(id, asset);

            CacheLoadedHandler<T> handlers;
            bool found;
            lock (requestsLock)
                found = Requests.TryGetValue(id, out handlers);
            if (found)
            {
                handlers(asset);
                lock (requestsLock)
                    Requests.Remove(id);
            }

            return result;
        }

        static TPacket CastPacket<TPacket>(IWorldPacket packet)
        {
            GCHandle handle = GCHandle.Alloc(packet, GCHandleType.Pinned);
            TPacket typedStruct = (TPacket)Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(TPacket));
            handle.Free();
            return typedStruct;
        }
    }
}