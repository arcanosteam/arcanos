﻿using System;
using System.Reflection;
using System.Text;
using Arcanos.Shared.Net;
using Arcanos.Shared.Objects;

namespace Arcanos.Client.GameClient.Data
{
    public class PlaceholderFileCache<T, TQueryPacket> : FileCache<T, TQueryPacket> where TQueryPacket : IWorldPacket
    {
        public PlaceholderFileCache(string filename, char[] magic) : this(filename, magic, Encoding.UTF8) { }
        public PlaceholderFileCache(string filename, char[] magic, Encoding encoding) : base(filename, magic, encoding) { }

        public override T Get(int id)
        {
            if (Assets.ContainsKey(id))
                return base.Get(id);
            if (indexCache.ContainsKey(id))
            {
                if (Load(id))
                    return base.Get(id);
            }
            T asset = (T)Activator.CreateInstance(typeof(T), BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.CreateInstance, null, null, null);
            
            lock (threadLock)
            {
                Assets.Add(id, asset);
                if (asset != null)
                {
                    AssetIndex.Add(asset, NextID);
                    if (asset is IReferencedByID)
                        (asset as IReferencedByID).ID = id;
                }
                if (id >= NextID)
                    NextID = id + 1;
            }

            Request(id);
            return asset;
        }
        public override void Get(int id, CacheLoadedHandler<T> loadedHandler)
        {
            throw new Exception();
        }
    }
}