﻿using Arcanos.Client.GameClient.Items;
using Arcanos.Client.GameClient.Movement;
using Arcanos.Client.GameClient.Objects;
using Arcanos.Client.GameClient.Players;
using Arcanos.Client.GameClient.Quests;
using Arcanos.Client.GameClient.Spells;
using Arcanos.Client.GameClient.World;
using Arcanos.Shared.Data;
using Arcanos.Shared.Graphics;
using Arcanos.Shared.Items;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Movement;
using Arcanos.Shared.Quests;
using Arcanos.Shared.Spells;
using Arcanos.Shared.World;
using Arcanos.Utilities;

namespace Arcanos.Client.GameClient.Data
{
    public static class SerializationManager
    {
        public static void Initialize()
        {
            Serialization.Register(typeof(Player));
            Serialization.Register(typeof(Character));
            Serialization.Register(typeof(Stats));
            Serialization.Register(); // Guild
            Serialization.Register(); // GuildMember

            Serialization.Register(typeof(Text));
            Serialization.Register(typeof(GameStrings));

            Serialization.Register(typeof(WorldObjectTemplate));
            Serialization.Register(typeof(CreatureTemplate));

            Serialization.Register(typeof(SpellTemplate));
            Serialization.Register(typeof(SpellEffect));
            Serialization.Register(typeof(SpellVisual));
            Serialization.Register(typeof(SpellVisualSet));

            Serialization.Register(typeof(ItemDefinition));
            Serialization.Register(typeof(ItemTemplate));
            Serialization.Register(); // Loot
            Serialization.Register(typeof(Looter));

            Serialization.Register(typeof(Quest));
            Serialization.Register(typeof(QuestObjective));

            Serialization.Register(typeof(Mover));
            Serialization.Register(typeof(PointOverTimeMovement));
            Serialization.Register(typeof(PointWithSpeedMovement));
            Serialization.Register(typeof(ClosestPointWithSpeedMovement));

            Serialization.Register(typeof(MapTemplate));
            Serialization.Register(typeof(HeightMapData));
            Serialization.Register(typeof(TerrainData));
            Serialization.Register(typeof(TerrainData.TerrainPatch));
            Serialization.Register(typeof(StaticModelObject));

            Serialization.Register(typeof(Faction));

            Serialization.Register(typeof(Material));
            Serialization.Register(typeof(TextureInfo));
            Serialization.Register(typeof(LightData));
            Serialization.Register(typeof(EmptyModel));
            Serialization.Register(typeof(SpriteModel));
            Serialization.Register(typeof(SpriteModelAnimation));
            Serialization.Register(typeof(MultiAngleSpriteModel));
            Serialization.Register(typeof(MultiAngleSpriteModelAnimation));
            Serialization.Register(typeof(WorldModel));
            Serialization.Register(typeof(DecalModel));
            Serialization.Register(typeof(ParticleSystem));
            Serialization.Register(typeof(ParticleEmitter));

            Serialization.Register(typeof(Environment));

            // Client-side only types
            Serialization.SetRegisterID(0x10000000);

            Serialization.outputLock = Logger.outputLock;
        }
    }
}