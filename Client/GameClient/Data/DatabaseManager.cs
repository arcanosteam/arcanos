﻿using Arcanos.Client.GameClient.Items;
using Arcanos.Client.GameClient.Objects;
using Arcanos.Client.GameClient.Players;
using Arcanos.Client.GameClient.Quests;
using Arcanos.Client.GameClient.Server;
using Arcanos.Client.GameClient.Spells;
using Arcanos.Client.GameClient.World;
using Arcanos.Net;
using Arcanos.Shared.Data;
using Arcanos.Shared.Graphics;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Net;
using Arcanos.Shared.Spells;
using Microsoft.Xna.Framework.Graphics;
using Model = Arcanos.Shared.Graphics.Model;

namespace Arcanos.Client.GameClient.Data
{
    public static class DatabaseManager
    {
        public static readonly ICache<Player> Players = new MemoryCache<Player, CPlayerQueryPacket>();
        public static readonly ICache<Character> Characters = new MemoryCache<Character, CCharacterQueryPacket>();
        //public static readonly ICache<Guild> Guilds = new MemoryCache<Guild, CGuildQueryPacket>();
        public static readonly Database<WorldServerEntry> WorldServers = new MemoryDatabase<WorldServerEntry>();

        // Game-related databases
        public static readonly ICache<Text> Text = new PlaceholderFileCache<Text, CTextQueryPacket>("text.clientdb", "TEXT".ToCharArray()) { MaxAssets = 4096, UploadChanges = true };
        public static readonly ICache<MapTemplate> Maps = new FileCache<MapTemplate, CMapTemplateQueryPacket>("maps.clientdb", "MAPS".ToCharArray()) { UploadChanges = true };
        public static readonly ICache<WorldObjectTemplate> WorldObjects = new FileCache<WorldObjectTemplate, CWorldObjectTemplateQueryPacket>("wo.clientdb", "WOBJ".ToCharArray()) { UploadChanges = true };
        public static readonly ICache<CreatureTemplate> Creatures = new FileCache<CreatureTemplate, CCreatureTemplateQueryPacket>("creatures.clientdb", "CRTS".ToCharArray()) { UploadChanges = true };
        public static readonly ICache<SpellTemplate> Spells = new FileCache<SpellTemplate, CSpellTemplateQueryPacket>("spells.clientdb", "SPLS".ToCharArray()) { UploadChanges = true };
        public static readonly ICache<SpellVisual> SpellVisuals = new FileCache<SpellVisual, CSpellVisualQueryPacket>("spellvisuals.clientdb", "SVLS".ToCharArray()) { UploadChanges = true };
        public static readonly ICache<ItemTemplate> Items = new FileCache<ItemTemplate, CItemTemplateQueryPacket>("items.clientdb", "ITMS".ToCharArray()) { UploadChanges = true };
        public static readonly ICache<Quest> Quests = new FileCache<Quest, CQuestQueryPacket>("quests.clientdb", "QEST".ToCharArray()) { UploadChanges = true };
        public static readonly ICache<Faction> Factions = new FileCache<Faction, CFactionQueryPacket>("factions.clientdb", "FCTS".ToCharArray()) { UploadChanges = true };
        public static readonly ICache<Environment> Environments = new FileCache<Environment, CEnvironmentQueryPacket>("environments.clientdb", "ENVS".ToCharArray()) { UploadChanges = true };

        // Graphics-related databases
        public static readonly ICache<Material> Materials = new FileCache<Material, CMaterialQueryPacket>("materials.clientdb", "MTLS".ToCharArray()) { UploadChanges = true };
        public static readonly ICache<TextureInfo> Textures = new FileCache<TextureInfo, CTextureInfoQueryPacket>("textures.clientdb", "TXTR".ToCharArray()) { MaxAssets = 65536, UploadChanges = true };
        public static readonly ICache<Model> Models = new FileCache<Model, CModelQueryPacket>("models.clientdb", "MDLS".ToCharArray()) { Asynchronous = true, AsyncPlaceholder = null, UploadChanges = true };
        public static readonly ICache<ParticleSystem> ParticleSystems = new FileCache<ParticleSystem, CParticleSystemQueryPacket>("particlesystems.clientdb", "PRTS".ToCharArray()) { Asynchronous = true, AsyncPlaceholder = null, UploadChanges = true };
        public static readonly PlaceholderMemoryDatabase<Texture2D> TextureData = new PlaceholderMemoryDatabase<Texture2D>();
        public static readonly MemoryDatabase<Material> MaterialData = new MemoryDatabase<Material>();

        public static void ConnectLocal()
        {
            TextureData.Connect();
            MaterialData.Connect();
        }
        public static void ConnectRemote(ConnectionManager<WorldPackets, IWorldPacket> cacheConnection)
        {
            Players.Connect(cacheConnection);
            Characters.Connect(cacheConnection);
            //Guilds.Connect(cacheConnection);
            WorldServers.Connect();

            Text.Connect(cacheConnection);
            Maps.Connect(cacheConnection);
            WorldObjects.Connect(cacheConnection);
            Creatures.Connect(cacheConnection);
            Spells.Connect(cacheConnection);
            SpellVisuals.Connect(cacheConnection);
            Items.Connect(cacheConnection);
            ItemManager.Connect(cacheConnection);
            Quests.Connect(cacheConnection);
            Factions.Connect(cacheConnection);
            Environments.Connect(cacheConnection);

            Materials.Connect(cacheConnection);
            Textures.Connect(cacheConnection);
            Models.Connect(cacheConnection);
            ParticleSystems.Connect(cacheConnection);
        }
        public static void CloseLocal()
        {
            TextureData.Close();
            MaterialData.Close();
        }
        public static void CloseRemote()
        {
            Players.Close();
            Characters.Close();
            //Guilds.Close();
            WorldServers.Close();

            Text.Close();
            Maps.Close();
            WorldObjects.Close();
            Creatures.Close();
            Spells.Close();
            SpellVisuals.Close();
            Items.Close();
            Quests.Close();
            Factions.Close();
            Environments.Close();

            Materials.Close();
            Textures.Close();
            Models.Close();
            ParticleSystems.Close();
        }

        public static Character GetCharacter(int id)
        {
            return Characters.Get(id);
        }
        public static Faction GetFaction(int id)
        {
            return Factions.Get(id);
        }
        public static Environment GetEnvironment(int id)
        {
            return Environments.Get(id);
        }
        public static SpellTemplate GetSpellTemplate(int id)
        {
            return Spells.Get(id);
        }
    }
}