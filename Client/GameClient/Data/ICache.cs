﻿using Arcanos.Net;
using Arcanos.Shared.Net;

namespace Arcanos.Client.GameClient.Data
{
    public delegate void CacheLoadedHandler<T>(T asset);
    public interface ICache<T>
    {
        bool Connected { get; }

        T Get(int id);
        void Get(int id, CacheLoadedHandler<T> loadedHandler);
        int GetID(T asset);

        int Add(int id, T asset);
        void Clear();

        bool Connect(ConnectionManager<WorldPackets, IWorldPacket> connection);
        bool Close();
    }
}