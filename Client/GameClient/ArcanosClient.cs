using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using Arcanos.Client.GameClient.Chat;
using Arcanos.Client.GameClient.Data;
using Arcanos.Client.GameClient.Input;
using Arcanos.Client.GameClient.Items;
using Arcanos.Client.GameClient.Net;
using Arcanos.Client.GameClient.Objects;
using Arcanos.Client.GameClient.Rendering;
using Arcanos.Client.GameClient.Spells;
using Arcanos.Client.GameClient.Timing;
using Arcanos.Client.GameClient.UI;
using Arcanos.Client.GameClient.UI.Game;
using Arcanos.Client.GameClient.World;
using Arcanos.Shared;
using Arcanos.Shared.Chat;
using Arcanos.Shared.Dialog;
using Arcanos.Shared.Graphics;
using Arcanos.Shared.Items;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Net;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Players;
using Arcanos.Shared.PvP;
using Arcanos.Shared.Spells;
using Arcanos.Shared.Timing;
using Arcanos.Shared.World;
using Arcanos.Utilities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Color = Microsoft.Xna.Framework.Graphics.Color;
using Environment = Arcanos.Client.GameClient.World.Environment;
using Keys = Microsoft.Xna.Framework.Input.Keys;
using Model = Arcanos.Shared.Graphics.Model;

namespace Arcanos.Client.GameClient
{
    public class ArcanosGameClient : Game
    {
        public struct LaunchParams
        {
            public string URL;
            public string Username;
            public string Password;
            public string LocaleCulture;
            public bool UseAsyncTextureLoading;
            public int FPSLimit;
        }

        // Settings
        private DebugSettings debugSettings;

        // Graphics
        private readonly GraphicsDeviceManager graphics;

        // Starting parameters
        private LaunchParams launchParams;
        private DateTime launchTime;
        private float fpsLimit;

        // Common UI
        public enum Desktops
        {
            Menu,
            Game,
            CharacterCreate
        }
        private UIFont logFont, tinyFont, damageFont, errorFont;

        // Character Create
        private struct CharacterTemplateCacheKey
        {
            public PlayerRace Race;
            public PlayerClass Class;
            public byte ModelIndex;
        }
        private struct CharacterTemplateCacheValue
        {
            public CreatureTemplate Template;
            public int[] Spells;
            public int[] Items;
            public int[] Equipment;
        }
        private PlayerRace chosenRace = 0;
        private PlayerClass chosenClass = 0;
        private byte chosenModel = 0;
        Action updateModel = null;
        private bool loadedCharacterCreateUI;

        public ArcanosGameClient(LaunchParams launchParams)
        {
            this.launchParams = launchParams;
            launchTime = DateTime.Now;
            fpsLimit = 1f / launchParams.FPSLimit;
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "";

            IsFixedTimeStep = false;
            InactiveSleepTime = TimeSpan.Zero;
            graphics.SynchronizeWithVerticalRetrace = false;
            Window.AllowUserResizing = true;
            Form form = Control.FromHandle(Window.Handle) as Form;
            if (form != null)
            {
                form.ClientSizeChanged += (sender, e) =>
                {
                    UIManager.SetSize(new Vector2(Window.ClientBounds.Width, Window.ClientBounds.Height));
                    graphics.PreferredBackBufferWidth = Math.Max(1, Window.ClientBounds.Width);
                    graphics.PreferredBackBufferHeight = Math.Max(1, Window.ClientBounds.Height);
                    graphics.ApplyChanges();
                    Update(new GameTime());
                };
            }

            WorldManager.Camera = new FreeCamera();
        }

        protected override void LoadContent()
        {
            InitRendering();

            InitDatabases();
            InitConnection();
            InitCommonUI();
            InitMenuUI();
            UIManager.SetCurrent((int)Desktops.Menu);
            LoadSettings();
        }
        private void InitRendering()
        {
            DebugRenderer.Prepare(GraphicsDevice);
            TerrainRenderer.Prepare(GraphicsDevice);
            SpriteRenderer.Prepare(GraphicsDevice);
            ModelRenderer.Prepare(GraphicsDevice);
            UIRenderer.Prepare(GraphicsDevice);
            ModelInstance.RegisterLight += TerrainRenderer.RegisterLight;
            ModelInstance.UnregisterLight += TerrainRenderer.UnregisterLight;
            DecalModelInstance.RegisterDecal += TerrainRenderer.RegisterDecal;
            DecalModelInstance.MoveDecal += TerrainRenderer.MoveDecal;
            DecalModelInstance.UnregisterDecal += TerrainRenderer.UnregisterDecal;
        }
        private void InitDatabases()
        {
            Localization.CurrentCulture = new CultureInfo(launchParams.LocaleCulture);
            Text.LoadHandler = id => DatabaseManager.Text.Get(id);
            Material.LoadHandler = id => DatabaseManager.Materials.Get(id);
            TextureInfo.LoadHandler = id => DatabaseManager.Textures.Get(id);
            Model.LoadHandler = id => DatabaseManager.Models.Get(id);
            ParticleSystem.LoadHandler = id => DatabaseManager.ParticleSystems.Get(id);
            SpellVisual.LoadHandler = id => DatabaseManager.SpellVisuals.Get(id);
            FactionBase.LoadHandler = id => DatabaseManager.Factions.Get(id);
            SerializationManager.Initialize();
            DatabaseManager.ConnectLocal();
            DatabaseManager.MaterialData.LoadHandler += id => DatabaseManager.Materials.Get(id);
            DatabaseManager.TextureData.UsePlaceholder = launchParams.UseAsyncTextureLoading;
            DatabaseManager.TextureData.LoadHandler += id =>
            {
                if (id == 0)
                    return null;
                TextureInfo info = DatabaseManager.Textures.Get(id);
                try { return info == null ? null : Texture2D.FromFile(GraphicsDevice, info.Filename); }
                catch { return null; }
            };
            DatabaseManager.TextureData.RequestHandler += id =>
            {
                if (id == 0)
                {
                    DatabaseManager.TextureData.Add(0, null);
                    return;
                }
                DatabaseManager.Textures.Get(id, info => ThreadPool.QueueUserWorkItem(o2 =>
                {
                    Texture2D tex;
                    try { tex = Texture2D.FromFile(GraphicsDevice, info.Filename); }
                    catch { tex = null; }
                    DatabaseManager.TextureData.Add(id, tex);
                }));
            };
        }
        private void InitCommonUI()
        {
            UIManager.Init(GraphicsDevice, Content);
            UIManager.SetSize(new Vector2(graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight));

            // Loading fonts
            logFont = new UIFont(UIManager.LoadFont("Fonts\\LogFont"), 8, 2, Color.Black);
            tinyFont = new UIFont(UIManager.LoadFont("Fonts\\PixelFont"), 4, 0, Color.Black);
            damageFont = new UIFont(UIManager.LoadFont("Fonts\\DamageFont"), 22, 0, Color.Black);
            errorFont = logFont.MakeColor(Color.Red);

            // Loading control defaults
            UIButton.NormalTexture = new UITexture("Textures\\buttontemplate_n.png", 0, 0, 40, 40, 7, 7, 7, 7);
            UIButton.HoveredTexture = new UITexture("Textures\\buttontemplate_h.png", 0, 0, 40, 40, 7, 7, 7, 7);
            UIButton.PressedTexture = new UITexture("Textures\\buttontemplate_p.png", 0, 0, 40, 40, 7, 7, 7, 7);
            UIButton.DisabledTexture = new UITexture("Textures\\buttontemplate_d.png", 0, 0, 40, 40, 7, 7, 7, 7);
            UIButton.TextFonts = new Dictionary<UIButtonType, UIFont>
            {
                { UIButtonType.Medium, logFont.MakeCentered().MakeShadow(1, 0.25f) },
                { UIButtonType.Tiny, tinyFont.MakeCentered().MakeShadow(1, 0.25f) },
            };
            UIDropdownItem.NormalTexture = new UITexture("Textures\\buttontemplate_n.png", 3, 3, 34, 34, 4, 4, 4, 4);
            UIDropdownItem.HoveredTexture = new UITexture("Textures\\buttontemplate_h.png", 3, 3, 34, 34, 4, 4, 4, 4);
            UIDropdownItem.PressedTexture = new UITexture("Textures\\buttontemplate_p.png", 3, 3, 34, 34, 4, 4, 4, 4);
            UIDropdownItem.DisabledTexture = new UITexture("Textures\\buttontemplate_d.png", 3, 3, 34, 34, 4, 4, 4, 4);
            UIDropdownItem.TextFont = UIButton.TextFonts[UIButtonType.Medium].MakeLeft();
            UIStackPanel.PanelTexture = new UITexture("Textures\\paneltemplate.png", 0, 0, 34, 34, 4, 4, 4, 4);
            UITextBox.NormalTexture = new UITexture("Textures\\textboxtemplate.png", 0, 0, 40, 24, 6, 7, 7, 6);
            UITextBox.TextFont = logFont.MakeLeft();
            UIWindow.WindowTexture = new UITexture("Textures\\windowtemplate2.png", 0, 0, 72, 72, 24, 7, 7, 7);
            UIWindow.TitleFont = logFont.MakeCentered().MakeShadow(1, 0.25f);
            UIWindow.ContentFont = logFont.MakeTopLeft();
            UIBlackout.Texture = new UITexture(UIManager.LoadTexture("Textures\\black.png"));
            UIBlackout.FadeDuration = 1.0f;
            UIActionButton.BackdropTexture = UIButton.PressedTexture;
            UIActionButton.KeyFont = logFont.MakeColor(Color.White).MakeTopLeft().MakeOffset(6, 5).MakeOutline(0.5f);
            UIActionButton.CountFont = logFont.MakeColor(Color.White).MakeBottomRight().MakeOffset(-1, -2).MakeOutline(0.5f);
            UIActionButton.DefaultSize = new Vector2(40, 40);
            UIActionButton.IconSize = new Vector2(32, 32);
            UICooldown.CooldownTextures = UITexture.FromStrip(UIManager.LoadTexture("Textures\\CooldownOverlay.png"), 128);
            UICooldown.DurationTextures = UITexture.FromStrip(UIManager.LoadTexture("Textures\\DurationOverlay.png"), 128);
            UIBag.BackgroundTexture = UIWindow.WindowTexture;
            UIBagSlot.BackgroundTexture = UIButton.PressedTexture;
            UILootMoney.BackgroundTexture = UIButton.PressedTexture;
            UILootMoney.MoneyIcon = new UITexture("Textures\\I_GoldCoin.png", 1, 1, 32, 32, 0, 0, 0, 0);
            UILootMoney.NameFont = logFont.MakeLeft().MakeShadow(1, 0.25f);
            UILootSlot.BackgroundTexture = UIButton.PressedTexture;
            UILootSlot.NameFont = logFont.MakeColor(0xFF, 0xD2, 0x00).MakeLeft().MakeShadow(1, 0.25f);
            UIItemDefinitionSlot.DefaultSize = new Vector2(40, 40);
            UIItemDefinitionSlot.IconSize = new Vector2(32, 32);
            UIItemDefinitionSlot.CountFont = logFont.MakeColor(Color.White).MakeBottomRight().MakeOffset(new Vector2(-1, -1)).MakeOutline(0.5f);
            UIItemDefinitionSlot.QualityOverlayTextures = new Dictionary<ItemQuality, UITexture>
            {
                { ItemQuality.Junk, new UITexture("Textures\\itemqualityoverlay0.png", 0, 0, 40, 40, 4, 4, 4, 4) },
                { ItemQuality.Common, new UITexture("Textures\\itemqualityoverlay1.png", 0, 0, 40, 40, 4, 4, 4, 4) },
                { ItemQuality.Uncommon, new UITexture("Textures\\itemqualityoverlay2.png", 0, 0, 40, 40, 4, 4, 4, 4) },
                { ItemQuality.Rare, new UITexture("Textures\\itemqualityoverlay3.png", 0, 0, 40, 40, 4, 4, 4, 4) },
                { ItemQuality.Unique, new UITexture("Textures\\itemqualityoverlay4.png", 0, 0, 40, 40, 4, 4, 4, 4) },
                { ItemQuality.Artifact, new UITexture("Textures\\itemqualityoverlay5.png", 0, 0, 40, 40, 4, 4, 4, 4) },
            };
            UITooltip.TooltipTexture = new UITexture("Textures\\tooltiptemplate.png", 0, 0, 0, 0, 6, 6, 6, 6);
            UITooltip.TooltipFont = logFont.MakeColor(new Color(36, 28, 12)).MakeTopLeft();
            UIChatFrame.MessageFont = logFont.MakeColor(Color.Yellow).MakeTopLeft().MakeOutline(0.75f);
            UIDialog.TextFont = logFont.MakeColor(Color.Black).MakeTopLeft();
            UIDialog.OptionFont = logFont.MakeColor(Color.Black).MakeLeft();
            UIDialog.QuestOptionTextures = new Dictionary<DialogQuestOptionState, UITexture>
            {
                { DialogQuestOptionState.NotAcceptable, new UITexture("Textures\\AvailableQuestIconInactive.png") },
                { DialogQuestOptionState.Acceptable, new UITexture("Textures\\AvailableQuestIcon.png") },
                { DialogQuestOptionState.NotCompleted, new UITexture("Textures\\ActiveQuestIconInactive.png") },
                { DialogQuestOptionState.Completed, new UITexture("Textures\\ActiveQuestIcon.png") },
            };
            UIQuest.TitleFont = logFont.MakeColor(0xFF, 0xD2, 0x00).MakeTopLeft().MakeShadow(1, 0.5f);
            UIQuest.ObjectiveActiveFont = logFont.MakeColor(0xC0, 0xC0, 0xC0).MakeTopLeft().MakeShadow(1, 0.5f);
            UIQuest.ObjectiveCompletedFont = logFont.MakeColor(0xFF, 0xD2, 0x00).MakeTopLeft().MakeShadow(1, 0.5f);
            UIQuest.ObjectiveStatusFormatActive = ": {0}/{1}";
            UIQuest.ObjectiveStatusFormatActiveBoolean = "";
            UIQuest.ObjectiveStatusFormatComplete = " (done)";
            UIGauge.TextFont = tinyFont.MakeCentered().MakeColor(Color.White).MakeShadow(1, 0.5f);
            UIDamageFloater.DamageFonts = new Dictionary<DamageFloaterType, UIFont>
            {
                { DamageFloaterType.IncomingMelee, damageFont.MakeCentered().MakeColor(Color.Red).MakeOutline(0.5f) },
                { DamageFloaterType.IncomingMagic, damageFont.MakeCentered().MakeColor(Color.Purple).MakeOutline(0.5f) },
                { DamageFloaterType.IncomingHeal, damageFont.MakeCentered().MakeColor(Color.Lime).MakeOutline(0.5f) },
                { DamageFloaterType.OutgoingMelee, damageFont.MakeCentered().MakeColor(Color.White).MakeOutline(0.5f) },
                { DamageFloaterType.OutgoingMagic, damageFont.MakeCentered().MakeColor(Color.LightBlue).MakeOutline(0.5f) },
                { DamageFloaterType.OutgoingHeal, damageFont.MakeCentered().MakeColor(Color.Lime).MakeOutline(0.5f) },
            };
            UICharacterWindow.StatTextFont = logFont.MakeColor(Color.White).MakeTopLeft().MakeShadow(1, 0.5f);
            UICharacterWindow.WeaponDamageFont = tinyFont.MakeColor(Color.Red).MakeTopLeft().MakeShadow(1, 0.25f);
            UICharacterWindow.WeaponIntervalFont = tinyFont.MakeColor(Color.White).MakeTopLeft().MakeShadow(1, 0.25f);
            UICharacterWindow.SpellNameFont = logFont.MakeColor(0xFF, 0xD2, 0x00).MakeLeft().MakeShadow(1, 0.25f);
            UIExperienceBar.ExperienceBarGaugeTexture = new UITexture(UIManager.LoadTexture("Textures\\gaugetemplate_bar.png"), 0, 48, 96, 16, 6, 4, 4, 3);
            UIExperienceBar.LevelFont = logFont.MakeColor(Color.White).MakeCentered().MakeOutline(0.5f);
            UIAura.IconSize = new Vector2(24, 24);
            UIAura.DurationFont = tinyFont.MakeColor(0xFF, 0xD2, 0x00).MakeBottom().MakeOffset(new Vector2(0, tinyFont.LineHeight + tinyFont.LineBreak)).MakeOutline(0.5f);
            UISpell.DefaultSize = new Vector2(40, 40);
            UISpell.IconSize = new Vector2(32, 32);
            UISpellSlot.BackgroundTexture = UIButton.PressedTexture;
            UIScrollbar.DefaultThickness = 16;
            UIScrollbar.UpArrowIcon = new UITexture("Textures\\scrollbarup.png");
            UIScrollbar.BarIcon = new UITexture("Textures\\scrollbarbar.png");
            UIScrollbar.DownArrowIcon = new UITexture("Textures\\scrollbardown.png");
            UIHScrollbar.DefaultThickness = 16;
            UIScrollbarButton.ScrollbarNormalTexture = new UITexture("Textures\\buttontemplate_tiny_n.png", 0, 0, 34, 34, 4, 4, 4, 4);
            UIScrollbarButton.ScrollbarHoveredTexture = new UITexture("Textures\\buttontemplate_tiny_h.png", 0, 0, 34, 34, 4, 4, 4, 4);
            UIScrollbarButton.ScrollbarPressedTexture = new UITexture("Textures\\buttontemplate_tiny_p.png", 0, 0, 34, 34, 4, 4, 4, 4);
            UIScrollbarButton.ScrollbarDisabledTexture = new UITexture("Textures\\buttontemplate_tiny_d.png", 0, 0, 34, 34, 4, 4, 4, 4);
            UINameplate.NoReactionFont = logFont.MakeColor(Color.White).MakeShadow(1, 0.5f).MakeOutline(0.5f);
            UINameplate.CorpseFont = UINameplate.NoReactionFont.MakeColor(Color.Gray);
            UINameplate.ReactionFonts = new Dictionary<Reaction, UIFont>
            {
                { Reaction.Friendly, UINameplate.NoReactionFont.MakeColor(Color.Lerp(UINameplate.NoReactionFont.Color, Color.Lime, 0.5f)) },
                { Reaction.Neutral, UINameplate.NoReactionFont.MakeColor(Color.Lerp(UINameplate.NoReactionFont.Color, Color.Yellow, 0.5f)) },
                { Reaction.Hostile, UINameplate.NoReactionFont.MakeColor(Color.Lerp(UINameplate.NoReactionFont.Color, Color.Red, 0.5f)) },
            };
            UIModel.DefaultEnvironment = new Environment()
            {
                AmbientLight = Vector3.One,
                FogStart = float.MaxValue,
                FogEnd = float.MaxValue,
                FogColor = Vector4.Zero,
            };
        }
        private void InitMenuUI()
        {
            UIDesktop desktop = UIManager.MakeDesktop((int)Desktops.Menu);

            UIMenuBackground background;
            desktop.Add("Background", background = new UIMenuBackground(new Vector2(-874, desktop.Height - 1536), new Vector2(8192, 1536))
            {
                CurrentGraphic = new UITexture("Textures\\menubackground.png", 0, 0, 8192, 1536, 0, 0, 0, 0),
            });

            // Login window
            UITextBox loginControl,
                passControl;
            UIButton loginButton;
            UIBlackout loginBlackout;
            desktop.Add(loginControl = new UITextBox(new Vector2(92, desktop.Height - 128), new Vector2(128, 24), launchParams.Username ?? "") { Anchor = UIAnchor.BottomLeft });
            desktop.Add(new UIControl(new Vector2(loginControl.Left - 64, loginControl.Top), new Vector2(64, loginControl.Height))
            {
                Anchor = UIAnchor.BottomLeft,
                Text = "Login:",
                Font = logFont.MakeRight(),
            });
            desktop.Add(passControl = new UIMaskedBox(new Vector2(loginControl.Left, loginControl.Bottom), loginControl.Size, launchParams.Password ?? "") { Anchor = UIAnchor.BottomLeft });
            desktop.Add(new UIControl(new Vector2(passControl.Left - 64, passControl.Top), new Vector2(64, passControl.Height))
            {
                Anchor = UIAnchor.BottomLeft,
                Text = "Pass:",
                Font = logFont.MakeRight(),
            });
            desktop.Add(loginButton = new UIButton(new Vector2(passControl.Left, passControl.Bottom), new Vector2(64, 24), UIButtonType.Medium, "Login") { Anchor = UIAnchor.BottomLeft });
            desktop.Add(loginBlackout = new UIBlackout());

            // WorldServer select window
            UIWindow worldServerListWindow;
            UIControl worldServerListContainer;
            UIStackPanel worldServerList;
            Vector2 worldServerListSize = new Vector2(desktop.Width / 3, desktop.Height / 2).Int();
            desktop.Add(worldServerListWindow = new UIWindow(((desktop.Size - worldServerListSize) / 2).Int(), worldServerListSize, "Select a world:") { Anchor = UIAnchor.None, Visible = false });
            worldServerListWindow.Controls.Add(worldServerListContainer = new UIControl(Vector2.Zero, worldServerListWindow.ContentSize)
            {
                CurrentGraphic = UIStackPanel.PanelTexture,
            });
            worldServerListContainer.Controls.Add(worldServerList = new UIStackPanel(Vector2.Zero, worldServerListContainer.Size, 0, true));

            // Character select window
            UIWindow characterListWindow;
            UIButton characterListCreateButton;
            UIControl characterListContainer;
            UIStackPanel characterList;
            Vector2 characterListSize = new Vector2(desktop.Width / 3, desktop.Height / 2).Int();
            desktop.Add(characterListWindow = new UIWindow(((desktop.Size - characterListSize) / 2).Int(), characterListSize, "Select a character:") { Anchor = UIAnchor.None, Visible = false });
            characterListWindow.Controls.Add(characterListCreateButton = new UIButton(new Vector2(0, characterListWindow.ContentHeight - 24), new Vector2(characterListWindow.ContentWidth, 24), UIButtonType.Medium, "Create New Character"));
            characterListWindow.Controls.Add(characterListContainer = new UIControl(Vector2.Zero, characterListWindow.ContentSize - new Vector2(0, 24 + 4))
            {
                CurrentGraphic = UIStackPanel.PanelTexture,
            });
            characterListContainer.Controls.Add(characterList = new UIStackPanel(Vector2.Zero, characterListContainer.Size, 0, true));

            // Setting up events
            loginControl.TextChanged += (sender, args) => { launchParams.Username = loginControl.Value; };
            passControl.TextChanged += (sender, args) => { launchParams.Password = passControl.Value; };
            loginButton.OnClick += (sender, args) =>
            {
                loginButton.Enabled = false;
                loginBlackout.Enable();
                StartConnection();
            };
            characterListCreateButton.OnClick += (sender, args) =>
            {
                InitCharacterCreateUI();
                UIManager.SetCurrent((int)Desktops.CharacterCreate);
            };
            ServerConnection.WorldServerEntryReceived += entry =>
            {
                UIButton button;
                worldServerList.Container.Controls.Add(button = new UIButton(Vector2.Zero, new Vector2(worldServerList.Container.Width, 32), UIButtonType.Medium, entry.Name + "\n" + entry.URL)
                {
                    MultilineText = true,
                });
                button.OnClick += (sender, args) =>
                {
                    ServerConnection.ConnectToWorldServer(entry);
                    worldServerListWindow.Visible = false;
                };
            };
            ServerConnection.LoggedIn += () =>
            {
                loginButton.Visible = false;
                //background.Visible = false;
                worldServerListWindow.Visible = true;
            };
            ServerConnection.LoggedIntoWorld += player =>
            {
                player.Session.CharacterListStatusLoaded += () => characterList.Container.Controls.Clear();
                player.Session.CharacterLoaded += (index, character) =>
                {
                    UIButton button;
                    characterList.Container.Controls.Add(button = new UIButton(Vector2.Zero, new Vector2(characterList.Container.Width, 32), UIButtonType.Medium, character.Name + "\nLevel " + character.Level + " " + character.Race + " " + character.Class)
                    {
                        MultilineText = true,
                    });
                    button.OnClick += (sender, args) =>
                    {
                        player.Session.SelectCharacter(character);
                        characterListWindow.Visible = false;
                    };
                };
                player.Session.EnteredWorld += () =>
                {
                    loginBlackout.Disable();
                    UIManager.SetCurrent((int)Desktops.Game);
                };
                characterListWindow.Visible = true;
            };
            Action<Exception> errorHandler = result =>
            {
                loginButton.Visible = true;
                background.Visible = true;
                worldServerListWindow.Visible = false;
                characterListWindow.Visible = false;
                worldServerList.Container.Controls.Clear();
                characterList.Container.Controls.Clear();
                loginButton.Enabled = true;
                loginBlackout.Disable();
            };
            ServerConnection.LoginFailed += result =>
            {
                errorHandler(null);
                UIManager.ShowMessage("Login failed with error:\n\n" + result);
            };
            ServerConnection.DNSError += errorHandler;
            ServerConnection.LoginConnectionError += errorHandler;
            ServerConnection.WorldConnectionError += errorHandler;

            //InputHandler.HookMouse(desktop);
        }
        private void InitCharacterCreateUI()
        {
            if (loadedCharacterCreateUI)
            {
                updateModel();
                return;
            }
            loadedCharacterCreateUI = true;

            Dictionary<CharacterTemplateCacheKey, CharacterTemplateCacheValue> templateCache = new Dictionary<CharacterTemplateCacheKey, CharacterTemplateCacheValue>();

            UIDesktop desktop = UIManager.MakeDesktop((int)Desktops.CharacterCreate);

            desktop.Add(UIManager.Desktops[(int)Desktops.Menu].GetByKey("Background"));

            UIModel modelControl;
            UITextBox nameText;
            UIButton prevModelButton, nextModelButton, animStandButton, animWalkButton, animAttackButton, raceButton, classButton, createButton;
            UIHStackPanel spellsPanel;

            desktop.Add(modelControl = new UIModel(new Vector2(0, 20), new Vector2(desktop.Width, desktop.Height / 2 - 40)) { Anchor = UIAnchor.None });
            desktop.Add(prevModelButton = new UIButton(new Vector2(desktop.Width / 2 - 32 - 64 - 24, desktop.Height / 2), new Vector2(24, 24), UIButtonType.Medium, Localization.Game.MenuCreateCharacterModelPrev) { Anchor = UIAnchor.None });
            desktop.Add(animStandButton = new UIButton(prevModelButton.TopRight, new Vector2(64, 24), UIButtonType.Medium, Localization.Game.MenuCreateCharacterAnimStand) { Anchor = UIAnchor.None });
            desktop.Add(animWalkButton = new UIButton(animStandButton.TopRight, new Vector2(64, 24), UIButtonType.Medium, Localization.Game.MenuCreateCharacterAnimWalk) { Anchor = UIAnchor.None });
            desktop.Add(animAttackButton = new UIButton(animWalkButton.TopRight, new Vector2(64, 24), UIButtonType.Medium, Localization.Game.MenuCreateCharacterAnimAttack) { Anchor = UIAnchor.None });
            desktop.Add(nextModelButton = new UIButton(animAttackButton.TopRight, new Vector2(24, 24), UIButtonType.Medium, Localization.Game.MenuCreateCharacterModelNext) { Anchor = UIAnchor.None });
            desktop.Add(nameText = new UITextBox(new Vector2(desktop.Width / 2 - 100, desktop.Height / 2 + 24 + 24), new Vector2(200, 22))
            {
                Anchor = UIAnchor.None,
                AllowDigits = false,
                AllowEnters = false,
                AllowSymbols = false,
                AllowTabs = false,
            });
            desktop.Add(new UIControl(nameText.TopLeft + new Vector2(0, -20), new Vector2(nameText.Width, 20))
            {
                Anchor = UIAnchor.None,
                Font = logFont.MakeBottom().MakeOffset(0, -2).MakeColor(0xFF, 0xD2, 0x00).MakeShadow(1, 0.5f),
                Text = Localization.Game.MenuCreateCharacterName.ToString(),
                TextRef = Localization.Game.MenuCreateCharacterName,
                CutOverflownText = false,
            });
            desktop.Add(raceButton = new UIButton(new Vector2(desktop.Width / 2 - 125, nameText.Bottom + 40), new Vector2(100, 24), UIButtonType.Medium, Localization.Game.RaceNames[(byte)chosenRace]) { Anchor = UIAnchor.None });
            desktop.Add(new UIControl(raceButton.TopLeft + new Vector2(0, -20), new Vector2(raceButton.Width, 20))
            {
                Anchor = UIAnchor.None,
                Font = logFont.MakeBottomLeft().MakeOffset(0, -2).MakeColor(0xFF, 0xD2, 0x00).MakeShadow(1, 0.5f),
                Text = Localization.Game.MenuCreateCharacterRace.ToString(),
                TextRef = Localization.Game.MenuCreateCharacterRace,
                CutOverflownText = false,
            });
            desktop.Add(classButton = new UIButton(new Vector2(desktop.Width / 2 + 25, nameText.Bottom + 40), new Vector2(100, 24), UIButtonType.Medium, Localization.Game.ClassNames[(byte)chosenClass]) { Anchor = UIAnchor.None });
            desktop.Add(new UIControl(classButton.TopLeft + new Vector2(0, -20), new Vector2(raceButton.Width, 20))
            {
                Anchor = UIAnchor.None,
                Font = logFont.MakeBottomLeft().MakeOffset(0, -2).MakeColor(0xFF, 0xD2, 0x00).MakeShadow(1, 0.5f),
                Text = Localization.Game.MenuCreateCharacterClass.ToString(),
                TextRef = Localization.Game.MenuCreateCharacterClass,
                CutOverflownText = false,
            });
            desktop.Add(spellsPanel = new UIHStackPanel(new Vector2(0, classButton.Bottom + 40), new Vector2(desktop.Width, UISpell.IconSize.Y), 4) { Anchor = UIAnchor.None });
            desktop.Add(new UIControl(spellsPanel.TopLeft + new Vector2(0, -20), new Vector2(spellsPanel.Width, 20))
            {
                Anchor = UIAnchor.None,
                Font = logFont.MakeBottom().MakeOffset(0, -2).MakeColor(0xFF, 0xD2, 0x00).MakeShadow(1, 0.5f),
                Text = Localization.Game.MenuCreateCharacterSpells.ToString(),
                TextRef = Localization.Game.MenuCreateCharacterSpells,
                CutOverflownText = false,
            });
            desktop.Add(createButton = new UIButton(new Vector2(desktop.Width / 2 - 40, spellsPanel.Bottom + 40), new Vector2(80, 22), UIButtonType.Medium, Localization.Game.MenuCreateCharacterCreate) { Anchor = UIAnchor.None });

            updateModel = () =>
            {
                CharacterTemplateCacheKey key = new CharacterTemplateCacheKey
                {
                    Race = chosenRace,
                    Class = chosenClass,
                    ModelIndex = chosenModel,
                };
                CharacterTemplateCacheValue template;
                if (!templateCache.TryGetValue(key, out template))
                {
                    WorldManager.PlayerSession.SendPacket(new CCharacterCreateTemplateQueryPacket
                    {
                        Race = chosenRace,
                        Class = chosenClass,
                        ModelIndex = chosenModel,
                    });
                    return;
                }

                spellsPanel.Container.Controls.Clear();
                if (template.Template != null)
                {
                    modelControl.Model = template.Template.Model;
                    modelControl.Visible = true;

                    for (int i = 0; i < template.Spells.Length && template.Spells[i] != 0; i++)
                    {
                        DatabaseManager.Spells.Get(template.Spells[i], spell =>
                        {
                            spellsPanel.Container.Controls.Add(new UISpell(Vector2.Zero, UISpell.IconSize, spell));
                            spellsPanel.Left = (desktop.Width - spellsPanel.Container.ContentWidth) / 2;
                        });
                    }
                }
                else
                    modelControl.Visible = false;
            };

            prevModelButton.OnClick += (sender, args) =>
            {
                if (chosenModel > 0)
                {
                    --chosenModel;
                    updateModel();
                }
            };
            nextModelButton.OnClick += (sender, args) =>
            {
                if (chosenModel < byte.MaxValue)
                {
                    ++chosenModel;
                    updateModel();
                }
            };
            animStandButton.OnClick += (sender, args) => modelControl.Instance.PlayAnimation("Stand");
            animWalkButton.OnClick += (sender, args) => modelControl.Instance.PlayAnimation("Walk");
            animAttackButton.OnClick += (sender, args) => modelControl.Instance.PlayAnimation("MeleeAttackMiss");
            raceButton.OnClick += (sender, args) =>
            {
                UIDropdownItemData[] items = new UIDropdownItemData[(byte)PlayerRace.Max];
                for (byte i = 0; i < (byte)PlayerRace.Max; i++)
                {
                    byte currentRace = i;
                    items[i].Text = Localization.Game.RaceNames[i].ToString();
                    items[i].TextRef = Localization.Game.RaceNames[i];
                    items[i].OnClick += (o, e) =>
                    {
                        chosenRace = (PlayerRace)currentRace;
                        chosenModel = 0;
                        raceButton.Text = (o as UIControl).Text;
                        raceButton.TextRef = (o as UIControl).TextRef;
                        updateModel();
                    };
                }
                desktop.OpenDropdown(raceButton, items);
            };
            classButton.OnClick += (sender, args) =>
            {
                UIDropdownItemData[] items = new UIDropdownItemData[(byte)PlayerClass.Max];
                for (byte i = 0; i < (byte)PlayerClass.Max; i++)
                {
                    byte currentClass = i;
                    items[i].Text = Localization.Game.ClassNames[i].ToString();
                    items[i].TextRef = Localization.Game.ClassNames[i];
                    items[i].OnClick += (o, e) =>
                    {
                        chosenClass = (PlayerClass)currentClass;
                        classButton.Text = (o as UIControl).Text;
                        classButton.TextRef = (o as UIControl).TextRef;
                        updateModel();
                    };
                }
                desktop.OpenDropdown(classButton, items);
            };
            createButton.OnClick += (sender, args) =>
            {
                createButton.Enabled = false;
                ServerConnection.WorldServerConnection.Write(new CCharacterCreatePacket
                {
                    Name = nameText.Value,
                    Race = chosenRace,
                    Class = chosenClass,
                    ModelIndex = chosenModel,
                });
            };

            desktop.RegisterEvent(UIEvents.CharacterCreateResult, (d, sender, args) =>
            {
                CharacterCreateResult result = (CharacterCreateResult)args[0];
                byte modelIndexCount = (byte)args[1];

                createButton.Enabled = true;
                if (result == CharacterCreateResult.Ok)
                {
                    modelControl.Model = null;
                    UIManager.SetCurrent((int)Desktops.Menu);
                    WorldManager.PlayerSession.SendPacket(new CCharacterListStatusQueryPacket());
                }
                else
                {
                    if (result == CharacterCreateResult.ModelIndexOutOfBounds)
                        chosenModel = (byte)(modelIndexCount - 1);
                    UIManager.ShowMessage(Localization.Game.MenuCreateCharacterResult[(byte)result].ToString());
                }
            });
            desktop.RegisterEvent(UIEvents.CharacterCreateTemplateResponse, (d, sender, args) =>
            {
                PlayerRace templateRace = (PlayerRace)args[0];
                PlayerClass templateClass = (PlayerClass)args[1];
                byte templateModel = (byte)args[2];
                int[] spellIDs = (int[])args[3];
                int[] itemIDs = (int[])args[4];
                int[] equipmentIDs = (int[])args[5];
                CharacterTemplateCacheKey key = new CharacterTemplateCacheKey
                {
                    Race = templateRace,
                    Class = templateClass,
                    ModelIndex = templateModel,
                };
                CreatureTemplate template = sender as CreatureTemplate;
                templateCache[key] = new CharacterTemplateCacheValue
                {
                    Template = template,
                    Spells = spellIDs,
                    Items = itemIDs,
                    Equipment = equipmentIDs,
                };
                updateModel();
            });

            updateModel();
        }
        private void InitGameUI()
        {
            const int ERROR_FRAMES_COUNT = 3;

            UIDesktop desktop = UIManager.MakeDesktop((int)Desktops.Game);
            /*desktop.Add(new UIDebugHoverTarget(Vector2.One * 16, Vector2.Zero)
            {
                Font = logFont.MakeTopLeft().MakeColor(Color.White).MakeShadow(1, 0.25f),
            });*/
            desktop.Add("PlayerFrame", new UICreatureFrame(new Vector2(16, 16)));
            desktop.Add("PlayerTargetFrame", new UICreatureFrame(desktop.GetByKey("PlayerFrame").TopRight));
            desktop.Add("PlayerTargetTargetFrame", new UICreatureFrame(desktop.GetByKey("PlayerTargetFrame").TopRight));
            desktop.Add("GroupFrame", new UIGroupPanel(new Vector2(32, 128), new Vector2(204, 72 * GroupBase.MAX_GROUP_MEMBERS)));
            desktop.Add("QuestFrame", new UIQuestLog(new Vector2(desktop.Width - 16 - 150, 16), new Vector2(150, 500)) { Anchor = UIAnchor.TopRight });
            desktop.Add("InventoryFrame", new UIInventory(new Vector2(desktop.Width - 16, desktop.Height - 16), Vector2.Zero, null) { Anchor = UIAnchor.BottomRight });
            desktop.Add("ChatFrame", new UIChatFrame(new Vector2(16, desktop.Height - 16 - UIActionButton.DefaultSize.Y - 16 - 22 - 200), new Vector2(400, 200)) { Anchor = UIAnchor.BottomLeft });
            desktop.Add("ChatInputBox", new UIChatInputBox(new Vector2(16, desktop.Height - 16 - UIActionButton.DefaultSize.Y - 16 - 22), new Vector2(400, 22), "") { Anchor = UIAnchor.BottomLeft });
            desktop.Add("DialogFrame", new UIDialog(new Vector2(16, 96), new Vector2(200, 350)) { Anchor = UIAnchor.Left });
            desktop.Add("CharacterFrame", new UICharacterWindow(new Vector2(64, 64), new Vector2(210, 340)) { Anchor = UIAnchor.Left });
            desktop.Add("ExperienceBar", new UIExperienceBar(new Vector2(0, desktop.Height - 12), new Vector2(desktop.Width, 12), Vector2.Zero, Vector2.Zero) { Anchor = UIAnchor.Bottom | UIAnchor.Horizontal });
            for (int i = 0; i < ERROR_FRAMES_COUNT; i++)
                desktop.Add("ErrorFrame" + i, new UIAutoFadingControl(i == 0 ? new Vector2(0, desktop.Height / 4) : desktop.GetByKey("ErrorFrame" + (i - 1)).BottomLeft, new Vector2(desktop.Width, errorFont.LineHeight))
                {
                    Anchor = UIAnchor.Horizontal,
                    Font = errorFont.MakeShadow(2, 0.25f),
                    CutOverflownText = false,
                    ClickThrough = true,
                });

            desktop.RegisterEvent(UIEvents.PlayerReadyForLoading, (d, sender, args) =>
            {
                d.GetByKey<UICreatureFrame>("PlayerFrame").SetTarget(WorldManager.PlayerCreature);
                d.GetByKey<UIInventory>("InventoryFrame").Inventory = WorldManager.Player.CurrentCharacter.Inventory;
                d.GetByKey<UICharacterWindow>("CharacterFrame").EquipmentControl.Bag = WorldManager.Player.CurrentCharacter.Equipment;
            });
            desktop.RegisterEvent(UIEvents.CreatureTargetChanged, (d, sender, args) =>
            {
                CreatureBase creature = sender as CreatureBase;
                if (creature == WorldManager.PlayerCreature)
                {
                    d.GetByKey<UICreatureFrame>("PlayerTargetFrame").SetTarget(creature.Target);
                    d.GetByKey<UICreatureFrame>("PlayerTargetTargetFrame").SetTarget(creature.Target != null ? creature.Target.Target : null);
                }
                if (creature == WorldManager.PlayerCreature.Target)
                    d.GetByKey<UICreatureFrame>("PlayerTargetTargetFrame").SetTarget(creature.Target);
            });
            desktop.RegisterEvent(UIEvents.MoneyUpdated, (d, sender, args) =>
            {
                if ((bool)args[2])
                    return;
                int money = (int)args[0];
                int oldMoney = (int)args[1];
                UIText text;
                if (money > oldMoney)
                    text = ChatManager.PrintRichMessage(Color.Green,
                        new TextRefToken(Localization.Game.LogMoneyAdded),
                        new TextToken(": "),
                        new TextToken((money - oldMoney).ToString()));
                else if (money < oldMoney)
                    text = ChatManager.PrintRichMessage(Color.Green,
                        new TextRefToken(Localization.Game.LogMoneyLost),
                        new TextToken(": "),
                        new TextToken((oldMoney - money).ToString()));
                else
                    return;
                text.SetColor(2, 2, Item.GetMoneyColor());
            });
            desktop.RegisterEvent(UIEvents.ItemsAcquired, (d, sender, args) =>
            {
                int itemID = (int)args[0];
                int itemCount = (int)args[1];
                DatabaseManager.Items.Get(itemID, item =>
                {
                    UIText text = ChatManager.PrintRichMessage(Color.Green,
                        new TextRefToken(itemCount == 1 ? Localization.Game.LogItemsAddedSingular : Localization.Game.LogItemsAddedPlural),
                        new TextToken(": "),
                        new TextToken(itemCount.ToString()),
                        new TextToken("x"),
                        new TextToken("["),
                        new TextRefToken(item.Name),
                        new TextToken("]"));
                    if (text == null)
                        return;
                    bool m = false;
                    text.SetClickable(4, 6, (o, eventArgs) => { });
                    text.SetHoverable(4, 6, (o, eventArgs) => UIManager.CurrentDesktop.SetTooltip(text, UITooltip.Build(item, itemCount, ref m)), (o, eventArgs) => UIManager.CurrentDesktop.ResetTooltip(text));
                    text.SetColor(4, 6, Item.GetQualityColor(item.Quality));
                });
            });
            desktop.RegisterEvent(UIEvents.ItemsLost, (d, sender, args) =>
            {
                int itemID = (int)args[0];
                int itemCount = (int)args[1];
                DatabaseManager.Items.Get(itemID, item =>
                {
                    UIText text = ChatManager.PrintRichMessage(Color.Green,
                        new TextRefToken(itemCount == 1 ? Localization.Game.LogItemsLostSingular : Localization.Game.LogItemsLostPlural),
                        new TextToken(": "),
                        new TextToken(itemCount.ToString()),
                        new TextToken("x"),
                        new TextToken("["),
                        new TextRefToken(item.Name),
                        new TextToken("]"));
                    if (text == null)
                        return;
                    bool m = false;
                    text.SetClickable(4, 6, (o, eventArgs) => { });
                    text.SetHoverable(4, 6, (o, eventArgs) => UIManager.CurrentDesktop.SetTooltip(text, UITooltip.Build(item, itemCount, ref m)), (o, eventArgs) => UIManager.CurrentDesktop.ResetTooltip(text));
                    text.SetColor(4, 6, Item.GetQualityColor(item.Quality));
                });
            });
            desktop.RegisterEvent(UIEvents.LootOpened, (d, sender, args) =>
            {
                ulong sourceGUID = (ulong)args[0];
                int money = (int)args[1];
                ItemDefinition[] items = args[2] as ItemDefinition[];
                d.Add(new UILootWindow(d.Size / 2, new Vector2(128, 192), sourceGUID, money, items));
            });
            desktop.RegisterEvent(UIEvents.ObjectEnteredVision, (d, sender, args) =>
            {
                WorldObjectBase wo = sender as WorldObjectBase;
                if (wo == WorldManager.PlayerCreature)
                    return;
                d.Add("Nameplate_" + wo.GUID, new UINameplate(wo));
            });
            desktop.RegisterEvent(UIEvents.ObjectExitedVision, (d, sender, args) =>
            {
                WorldObjectBase wo = sender as WorldObjectBase;
                if (wo == WorldManager.PlayerCreature)
                    return;
                d.Remove("Nameplate_" + wo.GUID);
            });
            desktop.RegisterEvent(UIEvents.ChatMessage, (d, sender, args) =>
            {
                ulong senderGUID = (ulong)args[0];
                int channelID = (int)args[1];
                string senderName = args[2] as string;
                long timestamp = (long)args[3];
                ChatMessageFlags flags = (ChatMessageFlags)args[4];
                string message = args[5] as string;

                string channelName;
                if (channelID == -1) // Whisper
                    channelName = (flags & ChatMessageFlags.WhisperFromYou) != 0 ? Localization.Game.ChatAliasWhisperTo.ToString() : Localization.Game.ChatAliasWhisperFrom.ToString();
                else if (ChatManager.JoinedChannels[channelID].Alias != null)
                    channelName = ChatManager.JoinedChannels[channelID].Alias.ToString();
                else
                    channelName = ChatManager.JoinedChannels[channelID].CustomName;

                UIText text = ChatManager.PrintRichMessage(ChatManager.JoinedChannels[channelID].Color,
                    new TextToken(Time.FormatTimestamp(Localization.Game.ChatTimestampFormat.ToString(), timestamp)),
                    new SpaceToken(),
                    new TextToken("[" + channelName + "]"),
                    new SpaceToken(),
                    new TextToken(senderName),
                    new TextToken(": "),
                    new TextToken(message));
                if (text == null)
                    return;
                text.SetColor(0, 0, Color.Gray);
                text.SetClickable(2, 2, (o, eventArgs) =>
                {
                    ChatManager.SetActiveChannel(channelID);
                    d.GetByKey<UIChatInputBox>("ChatInputBox").Focus();
                });
                text.SetClickable(4, 4, (o, eventArgs) =>
                {
                    ChatManager.SetActiveWhisper(senderName);
                    d.GetByKey<UIChatInputBox>("ChatInputBox").Focus();
                });
            });
            desktop.RegisterEvent(UIEvents.ChatChannelJoined, (d, sender, args) =>
            {
                UIText text = ChatManager.PrintRichMessage(
                    new TextRefToken(Localization.Game.LogChannelJoined),
                    new TextToken(": "),
                    new TextToken((string)args[1]));
                if (text == null)
                    return;
                text.SetClickable(2, 2, (o, eventArgs) =>
                {
                    ChatManager.SetActiveChannel((int)args[0]);
                    d.GetByKey<UIChatInputBox>("ChatInputBox").Focus();
                });
            });
            desktop.RegisterEvent(UIEvents.ChatChannelLeft, (d, sender, args) =>
            {
                UIText text = ChatManager.PrintRichMessage(
                    new TextRefToken(Localization.Game.LogChannelLeft),
                    new TextToken(": "),
                    new TextToken((string)args[1]));
                if (text == null)
                    return;
                text.SetClickable(2, 2, (o, eventArgs) => { });
            });
            desktop.RegisterEvent(UIEvents.GroupInvite, (d, sender, args) =>
            {
                ulong guid = (ulong)args[0];
                string name = (string)args[1];
                UIManager.ShowMessage(string.Format(Localization.Game.GroupInviteRequestFormat.ToString(), name),
                    Localization.Game.GroupInviteAccept.ToString(), (s, a) => WorldManager.PlayerSession.SendPacket(new CGroupInvitationResponsePacket { InviterGUID = guid, Response = GroupInvitationResponse.Accept }),
                    Localization.Game.GroupInviteDecline.ToString(), (s, a) => WorldManager.PlayerSession.SendPacket(new CGroupInvitationResponsePacket { InviterGUID = guid, Response = GroupInvitationResponse.Decline }));
            });
            desktop.RegisterEvent(UIEvents.GroupMemberJoined, (d, sender, args) =>
            {
                if ((bool)args[2])
                    return;
                string name = ObjectManager.GetPlayer((ulong)args[0]).Name;
                UIText text = ChatManager.PrintRichMessage(
                    new TextToken(name),
                    new TextToken(" "),
                    new TextRefToken(Localization.Game.LogPartyMemberJoined));
                if (text == null)
                    return;
                text.SetClickable(0, 0, (o, eventArgs) =>
                {
                    ChatManager.SetActiveWhisper(name);
                    d.GetByKey<UIChatInputBox>("ChatInputBox").Focus();
                });
            });
            desktop.RegisterEvent(UIEvents.GroupMemberLeft, (d, sender, args) =>
            {
                string name = ObjectManager.GetPlayer((ulong)args[0]).Name;
                UIText text = ChatManager.PrintRichMessage(
                    new TextToken(name),
                    new TextToken(" "),
                    new TextRefToken(Localization.Game.LogPartyMemberLeft));
                if (text == null)
                    return;
                text.SetClickable(0, 0, (o, eventArgs) =>
                {
                    ChatManager.SetActiveWhisper(name);
                    d.GetByKey<UIChatInputBox>("ChatInputBox").Focus();
                });
            });
            desktop.RegisterEvent(UIEvents.GroupLeaderChanged, (d, sender, args) =>
            {
                if ((bool)args[2])
                    return;
                string name = ObjectManager.GetPlayer((ulong)args[0]).Name;
                UIText text = ChatManager.PrintRichMessage(
                    new TextToken(name),
                    new TextToken(" "),
                    new TextRefToken(Localization.Game.LogPartyMemberLeader));
                if (text == null)
                    return;
                text.SetClickable(0, 0, (o, eventArgs) =>
                {
                    ChatManager.SetActiveWhisper(name);
                    d.GetByKey<UIChatInputBox>("ChatInputBox").Focus();
                });
            });
            desktop.RegisterEvent(UIEvents.DuelRequest, (d, sender, args) =>
            {
                ulong guid = (ulong)args[0];
                PlayerCreature pc = ObjectManager.GetPlayer(guid) as PlayerCreature;
                if (pc == null)
                    return;
                UIManager.ShowMessage(string.Format(Localization.Game.DuelRequestFormat.ToString(), pc.Name),
                    Localization.Game.DuelAccept.ToString(), (s, a) => WorldManager.PlayerSession.SendPacket(new CDuelRequestResponsePacket { ChallengerGUID = guid, Response = DuelResponse.Accept }),
                    Localization.Game.DuelDecline.ToString(), (s, a) => WorldManager.PlayerSession.SendPacket(new CDuelRequestResponsePacket { ChallengerGUID = guid, Response = DuelResponse.Decline }));
            });
            desktop.RegisterEvent(UIEvents.DuelStatus, (d, sender, args) =>
            {
                DuelStatus status = (DuelStatus)args[0];
                switch (status)
                {
                    case DuelStatus.Started:
                        ChatManager.PrintMessage(Localization.Game.LogDuelStatusStarted.ToString());
                        break;
                    case DuelStatus.Finished:
                        ChatManager.PrintMessage(Localization.Game.LogDuelStatusFinished.ToString());
                        break;
                }
            });
            desktop.RegisterEvent(UIEvents.DuelParticipantStatus, (d, sender, args) =>
            {
                ulong guid = (ulong)args[0];
                DuelParticipantStatus status = (DuelParticipantStatus)args[1];
                PlayerCreature pc = ObjectManager.GetPlayer(guid) as PlayerCreature;
                Text statusText = null;
                switch (status)
                {
                    case DuelParticipantStatus.Win:
                        statusText = Localization.Game.LogDuelParticipantStatusWin;
                        break;
                    case DuelParticipantStatus.Lose:
                        statusText = Localization.Game.LogDuelParticipantStatusLose;
                        break;
                }
                UIText text = ChatManager.PrintRichMessage(
                    new TextToken(pc.Name),
                    new TextToken(" "),
                    new TextRefToken(statusText));
                if (text == null)
                    return;
                text.SetClickable(0, 0, (o, eventArgs) =>
                {
                    ChatManager.SetActiveWhisper(pc.Name);
                    d.GetByKey<UIChatInputBox>("ChatInputBox").Focus();
                });
            });
            desktop.RegisterEvent(UIEvents.IncomingDamage, (d, sender, args) =>
            {
                Damage damage = (Damage)args[1];
                if (damage.IsGeneric)
                    return;

                CreatureBase creature = WorldManager.PlayerCreature;
                Vector3 pos = creature.Position;
                pos.Z += creature.GetHeight() / 2;

                if (damage.IsPhysical)
                    d.Add(new UIDamageFloater(DamageFloaterType.IncomingMelee, (damage.IsMagic ? damage.PhysicalDamage : damage).Sum.ToString(), R.Radius3D(pos, creature.GetHeight() / 8)));
                if (damage.IsMagic)
                    d.Add(new UIDamageFloater(DamageFloaterType.IncomingMagic, (damage.IsPhysical ? damage.MagicDamage : damage).Sum.ToString(), R.Radius3D(pos, creature.GetHeight() / 8)));
            });
            desktop.RegisterEvent(UIEvents.OutgoingDamage, (d, sender, args) =>
            {
                ulong victimGUID = (ulong)args[0];
                Damage damage = (Damage)args[1];
                if (damage.IsGeneric)
                    return;

                CreatureBase creature = ObjectManager.GetCreature(victimGUID);
                Vector3 pos = creature.Position;
                pos.Z += creature.GetHeight() / 2;

                if (damage.IsPhysical)
                    d.Add(new UIDamageFloater(DamageFloaterType.OutgoingMelee, (damage.IsMagic ? damage.PhysicalDamage : damage).Sum.ToString(), R.Radius3D(pos, creature.GetHeight() / 8)));
                if (damage.IsMagic)
                    d.Add(new UIDamageFloater(DamageFloaterType.OutgoingMagic, (damage.IsPhysical ? damage.MagicDamage : damage).Sum.ToString(), R.Radius3D(pos, creature.GetHeight() / 8)));
            });
            desktop.RegisterEvent(UIEvents.IncomingHeal, (d, sender, args) =>
            {
                int heal = (int)args[1];

                CreatureBase creature = WorldManager.PlayerCreature;
                Vector3 pos = creature.Position;
                pos.Z += creature.GetHeight() / 2;

                d.Add(new UIDamageFloater(DamageFloaterType.IncomingHeal, heal.ToString(), R.Radius3D(pos, creature.GetHeight() / 8)));
            });
            desktop.RegisterEvent(UIEvents.OutgoingHeal, (d, sender, args) =>
            {
                ulong targetGUID = (ulong)args[0];
                int heal = (int)args[1];

                CreatureBase creature = ObjectManager.GetCreature(targetGUID);
                Vector3 pos = creature.Position;
                pos.Z += creature.GetHeight() / 2;

                d.Add(new UIDamageFloater(DamageFloaterType.OutgoingHeal, heal.ToString(), R.Radius3D(pos, creature.GetHeight() / 8)));
            });
            Action<UIDesktop> shiftErrorMessages = d =>
            {
                for (int i = ERROR_FRAMES_COUNT - 1; i > 0; i--)
                {
                    UIAutoFadingControl prev = d.GetByKey<UIAutoFadingControl>("ErrorFrame" + (i - 1));
                    UIAutoFadingControl cur = d.GetByKey<UIAutoFadingControl>("ErrorFrame" + i);
                    if (prev.Visible)
                    {
                        cur.Text = prev.Text;
                        cur.Font = prev.Font;
                        prev.TransferTo(cur);
                    }
                }
            };
            desktop.RegisterEvent(UIEvents.GameError, (d, sender, args) =>
            {
                string message = args[0].ToString();
                ChatManager.PrintMessage(Color.Red, message);
                shiftErrorMessages(d);
                UIAutoFadingControl topErrorFrame = d.GetByKey<UIAutoFadingControl>("ErrorFrame0");
                topErrorFrame.Text = message;
                topErrorFrame.Font = errorFont;
                topErrorFrame.Show();
            });
            desktop.RegisterEvent(UIEvents.GameInfo, (d, sender, args) =>
            {
                string message = (string)args[0];
                ChatManager.PrintMessage(Color.Yellow, message);
                shiftErrorMessages(d);
                UIAutoFadingControl topErrorFrame = d.GetByKey<UIAutoFadingControl>("ErrorFrame0");
                topErrorFrame.Text = message;
                topErrorFrame.Font = errorFont.MakeColor(Color.Yellow);
                topErrorFrame.Show();
            });

            UIActionButton prevButton = null;
            for (int i = 0; i < 10; i++)
            {
                int closureIndex = i;
                Keys key = Keys.D1 + (i >= 5 ? i - 5 : i);
                bool shift = i >= 5;
                UIActionButton button = prevButton = new UIActionButton(prevButton == null ? new Vector2(16, desktop.Height - 16 - UIActionButton.DefaultSize.Y) : prevButton.TopRight, UIActionButton.DefaultSize, null)
                {
                    Anchor = UIAnchor.BottomLeft,
                    Key = new ActionButtonKey { Key = key, Shift = shift },
                    Function = new ActionButtonFunction { Type = ActionButtonType.Spell, ID = 1 + i % 2 },
                };
                desktop.RegisterEvent(UIEvents.PlayerLoaded, (d, sender, args) =>
                {
                    if (WorldManager.PlayerCreature.SpellBook.Spells.Count == 0)
                        return;
                    button.Owner = WorldManager.PlayerCreature;
                    button.Function = new ActionButtonFunction
                    {
                        Type = ActionButtonType.Spell,
                        ID = WorldManager.PlayerCreature.SpellBook.Spells[closureIndex % WorldManager.PlayerCreature.SpellBook.Spells.Count].ID,
                    };
                });
                desktop.Add(button);
            }

            InputHandler.HookMouse(desktop);
        }
        private void LoadSettings()
        {
            debugSettings = new DebugSettings();
            using (FileStream fs = new FileStream("debug.settings", FileMode.OpenOrCreate))
                debugSettings.Load(fs);
        }
        private void SaveSettings()
        {
            using (FileStream fs = new FileStream("debug.settings", FileMode.OpenOrCreate))
                debugSettings.Save(fs);
        }
        private void InitConnection()
        {
            ServerConnection.DNSError += ex =>
            {
                ServerConnection.Disconnect();
                UIManager.ShowMessage("DNS Resolve Error:\n\n" + ex.Message);
            };
            ServerConnection.LoginConnectionError += ex =>
            {
                ServerConnection.Disconnect();
                UIManager.ShowMessage("Login Server Connection Error:\n\n" + ex.Message);
            };
            ServerConnection.WorldConnectionError += ex =>
            {
                ServerConnection.Disconnect();
                UIManager.ShowMessage("World Server Connection Error:\n\n" + ex.Message);
            };
            ServerConnection.ConnectedToWorld += DatabaseManager.ConnectRemote;
            ServerConnection.LoggedIntoWorld += player =>
            {
                player.Session.SendPacket(new CGameStringsQueryPacket());
                while (Localization.Game == null)
                    Thread.Sleep(10);
                player.Session.EnteredWorld += () =>
                {
                    InitGameUI();
                    WorldManager.Camera = new TargetedCamera()
                    {
                        TargetedObject = player.CurrentCharacter.Creature,
                        Distance = 4,
                        Rotation = Rotation.Zero,
                    };
                };
                player.Session.SendPacket(new CCharacterListStatusQueryPacket());
            };
        }
        private void StartConnection()
        {
            ServerConnection.ConnectToLoginServer(launchParams.URL ?? "127.0.0.1", launchParams.Username, launchParams.Password);
        }

        protected override void UnloadContent()
        {
            if (WorldManager.IsPlayerLoaded)
                WorldManager.PlayerSession.ForceLogout();
            Thread.Sleep(100);
            DatabaseManager.CloseRemote();
            DatabaseManager.CloseLocal();
            SaveSettings();
            base.UnloadContent();
        }

        private static readonly Stopwatch timingStopwatch = new Stopwatch();
        protected override void Update(GameTime gameTime)
        {
            // Timing measurements
            if (timingStopwatch.IsRunning)
            {
                Time.PerSecond = (float)timingStopwatch.Elapsed.TotalSeconds;
                timingStopwatch.Reset();
            }
            else
                Time.PerSecond = 0.00001f;

            if (launchParams.FPSLimit > 0 && Time.PerSecond < fpsLimit)
            {
                Thread.Sleep((int)(1000 * (fpsLimit - Time.PerSecond)));
                Time.PerSecond = fpsLimit;
            }

            timingStopwatch.Start();
            Time.Timestamp = DateTime.UtcNow.Ticks / 10000;

            // Handling input
            InputManager.UpdateInput(this, Keyboard.GetState(), Mouse.GetState());

            if (InputManager.IsKeyDown(Keys.Escape) && InputManager.IsKeyDown(Keys.LeftShift))
                Exit();

            bool focused = false;
            Form form;
            if ((form = (Form)Control.FromHandle(Window.Handle)).Focused)
                focused = true;

            if (focused)
            {
                foreach (Keys key in InputManager.PressedKeys)
                    if (!UIManager.KeyPress(key))
                    {
                        switch (key)
                        {
                            case Keys.Escape:
                                UIManager.ShowMessage("Do you really want to exit?", "Yes", (sender, args) => Exit(), "No", (sender, args) => { });
                                break;
                            case Keys.F1:
                                debugSettings.DrawBoundingBoxes = !debugSettings.DrawBoundingBoxes;
                                break;
                            case Keys.F2:
                                debugSettings.DrawDirections = !debugSettings.DrawDirections;
                                break;
                            case Keys.F3:
                                debugSettings.DrawNormals = !debugSettings.DrawNormals;
                                break;
                            case Keys.F12:
                                InitRendering();
                                break;
                            case Keys.OemPlus:
                                TerrainRenderer.SetAmbientIntensity(TerrainRenderer.GetAmbientIntensity() + 0.05f);
                                break;
                            case Keys.OemMinus:
                                TerrainRenderer.SetAmbientIntensity(TerrainRenderer.GetAmbientIntensity() - 0.05f);
                                break;
                            default:
                                InputHandler.KeyPress(key);
                                break;
                        }
                    }
                foreach (Keys key in InputManager.ReleasedKeys)
                    if (!UIManager.KeyRelease(key))
                        InputHandler.KeyRelease(key);
                UIManager.UpdateMouse(new Vector2(InputManager.MouseX, InputManager.MouseY), InputManager.IsKeyDown(MouseKeys.Left), InputManager.IsKeyDown(MouseKeys.Right), InputManager.GetAxisChange(MouseAxis.Z));

                //IsMouseVisible = false;
                WorldManager.Camera.SetSize(form.ClientSize.Width, form.ClientSize.Height);
                WorldManager.Camera.Update();
            }
            else
                IsMouseVisible = true;

            // Updating world
            WorldManager.Update();
            Waiter.UpdateAll();

            // Updating UI
            UIManager.Update();

            // Updating renderers
            DebugRenderer.UpdateCamera(WorldManager.Camera);
            TerrainRenderer.UpdateCamera(WorldManager.Camera);

            base.Update(gameTime);
        }

        private int frame = 0;
        public double sample = 0;
        protected override void Draw(GameTime gameTime)
        {
            if (WorldManager.CurrentMap != null)
            {
                GraphicsDevice.Clear(new Color(WorldManager.CurrentMap.Environment.FogColor));

                TerrainRenderer.Render(WorldManager.CurrentMap);

                ModelRenderer.Render(WorldManager.CurrentMap, WorldManager.Camera, WorldManager.PlayerCreature);

                if (debugSettings.DrawNormals && WorldManager.IsCharacterInGame)
                {
                    PlayerCreature playerCreature = WorldManager.PlayerCreature;
                    MapTemplateBase map = playerCreature.Map.Template;
                    for (int x = 0; x <= map.SizeX; x += 50)
                        for (int y = 0; y <= map.SizeY; y += 50)
                        {
                            float height = map.GetHeight(x, y);
                            Vector3 playerPos = new Vector3(x, y, height);
                            Vector3 normal = map.GetNormal(x, y);
                            DebugRenderer.RenderLine(playerPos, playerPos + normal * 10, Color.Blue);
                        }
                    DebugRenderer.RenderLine(playerCreature.Position, playerCreature.Position + map.GetSmoothNormal(playerCreature.Position.X, playerCreature.Position.Y) * 10, Color.Red);
                }

                foreach (WorldObjectBase wo in WorldManager.CurrentMap.VisibleObjects)
                {
                    if (wo != WorldManager.PlayerCreature && !WorldManager.PlayerCreature.CanSee(wo))
                        continue;
                    if (debugSettings.DrawBoundingBoxes)
                        DebugRenderer.Render(wo, DebugRenderShape.BoundingBox);
                    if (debugSettings.DrawDirections)
                        DebugRenderer.RenderDirection(wo, Color.WhiteSmoke);
                    SpriteRenderer.Render(wo, WorldManager.Camera);
                }
                foreach (Spell spell in WorldManager.CurrentMap.Spells)
                {
                    SpriteRenderer.Render(spell, WorldManager.Camera);
                }
            }
            else
            {
                GraphicsDevice.Clear(Color.Black);
                TerrainRenderer.SetAmbientLight(Vector4.One);
            }

            if (UIManager.CurrentDesktop != null)
                UIRenderer.Render(UIManager.CurrentDesktop);

            RenderManager.StopShader();

            frame++;
            if (frame == 100)
            {
                Window.Title = (1 / Time.PerSecond).ToString("0") + " fps";
                frame = 0;
            }

            base.Draw(gameTime);
        }
    }
    public class DebugSettings : Settings
    {
        public bool DrawBoundingBoxes;
        public bool DrawNormals;
        public bool DrawDirections;
    }
    class UIMenuBackground : UIControl
    {
        private float aspect;

        public UIMenuBackground(Vector2 location, Vector2 size) : base(location, size)
        {
            Anchor = UIAnchor.Left | UIAnchor.Vertical;
            aspect = Width / Height;
            ClickThrough = true;
        }

        public override bool OnResized(Vector2 d)
        {
            if (UIManager.CurrentlyResizedDesktop != UIManager.Desktops[(int)ArcanosGameClient.Desktops.Menu])
                return false;
            bool result = base.OnResized(d);
            Width = Height * aspect;
            return result;
        }

        public override void Update()
        {
            base.Update();
            Left -= 20 * Time.PerSecond;
            if (Left < -Width / 2)
                Left += Width / 2;
        }
    }
    class UIDebugHoverTarget : UIControl
    {
        public UIDebugHoverTarget(Vector2 location, Vector2 size) : base(location, size)
        {
            CutOverflownText = false;
        }

        public override void Update()
        {
            base.Update();
            if (!WorldManager.IsCharacterInGame)
                return;
            Text = "Hovering:";
            foreach (WorldObjectBase wo in InputHandler.TestMouseHover(WorldManager.Camera, ObjectType.All))
            {
                Text += "\n[" + wo.GUID + "] " + wo.Name;

                UICreatureFrame frame = Desktop.GetByKey("PlayerFrame") as UICreatureFrame;
                if (frame != null && wo is CreatureBase && (wo as CreatureBase).Target != null)
                    Text += " > [" + (wo as CreatureBase).Target.GUID + "] " + (wo as CreatureBase).Target.Name;
            }
        }
    }
}
