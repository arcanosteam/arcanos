﻿using System;
using System.Collections.Generic;
using Arcanos.Shared.Objects;

namespace Arcanos.Client.GameClient.Timing
{
    public delegate bool WaitHandler();
    public delegate bool ObjectWaitHandler(object obj);
    public static class Waiter
    {
        private struct WaitEntry
        {
            private readonly WaitHandler condition;
            private readonly ObjectWaitHandler objectCondition;
            private readonly object Object;
            public readonly Action Action;
            private readonly bool invert;

            public bool IsObjectWaiter
            {
                get { return objectCondition != null; }
            }

            public WaitEntry(WaitHandler condition, Action action, bool invert)
            {
                this.condition = condition;
                objectCondition = null;
                Object = null;
                Action = action;
                this.invert = invert;
            }
            public WaitEntry(ObjectWaitHandler condition, object obj, Action action, bool invert)
            {
                this.condition = null;
                objectCondition = condition;
                Object = obj;
                Action = action;
                this.invert = invert;
            }

            public bool Test()
            {
                if (invert)
                    if (objectCondition != null)
                        return !objectCondition(Object);
                    else
                        return !condition();
                if (objectCondition != null)
                    return objectCondition(Object);
                return condition();
            }
        }

        private readonly static List<WaitEntry> entries = new List<WaitEntry>();

        public static void UpdateAll()
        {
            int count = entries.Count;
            for (int i = 0; i < count; i++)
                if (entries[i].Test())
                {
                    entries[i].Action();
                    entries.RemoveAt(i--);
                    --count;
                }
        }
        public static void UpdateAllWaitingForFinalization()
        {
            int count = entries.Count;
            for (int i = 0; i < count; i++)
                if (entries[i].IsObjectWaiter && entries[i].Test())
                {
                    entries[i].Action();
                    entries.RemoveAt(i--);
                    --count;
                }
        }

        public static void WaitFor(WaitHandler condition, Action action)
        {
            if (condition())
            {
                action();
                return;
            }
            entries.Add(new WaitEntry(condition, action, false));
        }
        public static void WaitWhile(WaitHandler condition, Action action)
        {
            if (!condition())
            {
                action();
                return;
            }
            entries.Add(new WaitEntry(condition, action, true));
        }
        public static void WaitForFinalization(WorldObjectBase wo, Action action)
        {
            if (wo.Finalized)
            {
                action();
                return;
            }
            entries.Add(new WaitEntry(WaitForFinalizationHandler, wo, action, false));
        }

        private static bool WaitForFinalizationHandler(object obj)
        {
            return (obj as WorldObjectBase).Finalized || ObjectManager.IsFinalized(obj as WorldObjectBase);
        }
    }
}