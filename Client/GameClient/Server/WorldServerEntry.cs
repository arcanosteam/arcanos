﻿using Arcanos.Client.GameClient.Data;
using Arcanos.Shared.Server;

namespace Arcanos.Client.GameClient.Server
{
    public class WorldServerEntry : WorldServerEntryBase
    {
        protected WorldServerEntry() { }
        public WorldServerEntry(string name, string url) : base(name, url) { }

        protected override int GetInitialID()
        {
            return DatabaseManager.WorldServers.GetID(this);
        }
    }
}