﻿using Arcanos.Client.GameClient.Data;
using Arcanos.Shared.Objects;

namespace Arcanos.Client.GameClient.Objects
{
    public class WorldObjectTemplate : WorldObjectTemplateBase
    {
        protected override int GetInitialID()
        {
            return DatabaseManager.WorldObjects.GetID(this);
        }
    }
}