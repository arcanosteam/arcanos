﻿using Arcanos.Client.GameClient.Data;
using Arcanos.Shared.Objects;

namespace Arcanos.Client.GameClient.Objects
{
    public class CreatureTemplate : CreatureTemplateBase
    {
        protected override int GetInitialID()
        {
            return DatabaseManager.Creatures.GetID(this);
        }
    }
}