﻿using System.Collections.Generic;
using Arcanos.Client.GameClient.Graphics;
using Arcanos.Client.GameClient.Movement;
using Arcanos.Client.GameClient.Rendering;
using Arcanos.Client.GameClient.Spells;
using Arcanos.Client.GameClient.UI;
using Arcanos.Client.GameClient.World;
using Arcanos.Shared;
using Arcanos.Shared.Graphics;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Objects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Arcanos.Client.GameClient.Objects
{
    public class Creature : CreatureBase, IGaugeSource, IRenderable<int, ModelRenderGeometry>
    {
        public new CreatureTemplate Template { get { return (CreatureTemplate)template; } }

        public override string Name
        {
            get { return template.Name == null ? Text.MissingString : template.Name.ToString(); }
        }
        public override Text NameRef
        {
            get { return template.Name; }
        }

        public ulong TargetGUID;

        public Creature(ulong guid, CreatureTemplate template, Vector3 pos, Rotation rot) : base(guid, template, pos, rot)
        {
            Mover = new Mover(this);

            SpellBook = new SpellBook(this);

            ResetFromTemplate();
        }

        public override bool Update()
        {
            if (!base.Update())
                return false;

            UpdateTarget();
            UpdateOrientation();
            SpellBook.Update();
            if (!Mover.Finished)
                Model.PlayAnimation("Walk");
            else
                Model.StopAnimation("Walk");

            return true;
        }

        private void UpdateTarget()
        {
            SetTarget(ObjectManager.GetCreature(TargetGUID));
        }
        private void UpdateOrientation()
        {
            if (Target != null)
                RotateTo(Position.GetRotationTo(Target.Position));
        }

        public void PerformedMeleeAttack(CreatureBase target, AttackType type, AttackResult result)
        {
            switch (result)
            {
                case AttackResult.Hit:
                    Model.PlayAnimationOnce("MeleeAttackHit");
                    target.Model.PlayAnimationOnce("MeleeHit");
                    break;
                case AttackResult.Miss:
                    Model.PlayAnimationOnce("MeleeAttackMiss");
                    target.Model.PlayAnimationOnce("MeleeMiss");
                    break;
            }
        }
        public void PerformedSpellAttack(CreatureBase target, SpellTemplate spell, AttackResult result)
        {
            // Animations handled in SpellVisual
        }

        public override void SetState(CreatureState state)
        {
            base.SetState(state);
            switch (state)
            {
                case CreatureState.JustSpawned:
                    Model.PlayAnimation("Spawn", "Stand");
                    break;
                case CreatureState.Alive:
                    if (Model.CurrentAnimationName != "Spawn")
                        Model.PlayAnimation("Stand");
                    break;
                case CreatureState.JustDied:
                    Model.PlayAnimation("Death", "Dead");
                    break;
                case CreatureState.Corpse:
                    if (Model.CurrentAnimationName != "Death")
                        Model.PlayAnimation("Dead");
                    break;
                case CreatureState.Despawned:
                    break;
            }
        }
        public override void SetTarget(CreatureBase target)
        {
            TargetGUID = target == null ? 0 : target.GUID;
            CreatureBase oldTarget = Target;
            base.SetTarget(target);
            if (Target != oldTarget)
                UIManager.RaiseEvent(UIEvents.CreatureTargetChanged, this, Target);
        }
        public void SetTarget(ulong targetGUID)
        {
            TargetGUID = targetGUID;

            if (TargetGUID == 0)
                SetTarget(null);
            else
            {
                CreatureBase target = ObjectManager.GetCreature(TargetGUID);
                if (target != null)
                    SetTarget(target);
            }
            UIManager.RaiseEvent(UIEvents.CreatureStateChanged, this, State);
        }

        public override void SetHealth(int currentHealth)
        {
            base.SetHealth(currentHealth);
            UIManager.RaiseEvent(UIEvents.CreatureHealthChanged, this, CurrentHealth, MaxHealth);
        }
        public override void SetHealth(int currentHealth, int maxHealth)
        {
            base.SetHealth(currentHealth, maxHealth);
            UIManager.RaiseEvent(UIEvents.CreatureHealthChanged, this, CurrentHealth, MaxHealth);
        }
        public override void SetUsesPower(PowerType type, bool uses)
        {
            base.SetUsesPower(type, uses);
            UIManager.RaiseEvent(UIEvents.CreatureUsesPowerChanged, this, type, UsesPowerType[(byte)type]);
        }
        public override void SetPower(PowerType type, int currentPower)
        {
            base.SetPower(type, currentPower);
            UIManager.RaiseEvent(UIEvents.CreaturePowerChanged, this, type, CurrentPower[(byte)type], MaxPower[(byte)type]);
        }
        public override void SetPower(PowerType type, int currentPower, int maxPower)
        {
            base.SetPower(type, currentPower, maxPower);
            UIManager.RaiseEvent(UIEvents.CreaturePowerChanged, this, type, CurrentPower[(byte)type], MaxPower[(byte)type]);
        }

        public override void OnPositionChanged()
        {
            if (WorldManager.IsCharacterInGame && WorldManager.PlayerCreature.Target == this)
                TerrainRenderer.MoveDecal(PlayerCreature.TargetDecalIndex, Position);
        }
        public override void OnRotationChanged()
        {
        }
        public override void OnAddedToMap()
        {
        }
        public override void OnRemovedFromMap()
        {
        }

        public void GetGaugeData(int gauge, out float value, out float min, out float max)
        {
            switch (gauge)
            {
                case -1:
                    value = CurrentHealth;
                    min = 0;
                    max = MaxHealth;
                    break;
                case -2:
                    if (SpellBook.CastingSpell != null)
                    {
                        value = SpellBook.CastingTime;
                        min = 0;
                        max = SpellBook.CastingSpell.CastingTime;
                    }
                    else
                    {
                        value = 0;
                        min = 0;
                        max = 1;
                    }
                    break;
                default:
                    if (gauge < 0)
                    {
                        value = 0;
                        min = 0;
                        max = 0;
                    }
                    else
                    {
                        value = CurrentPower[gauge];
                        min = 0;
                        max = MaxPower[gauge];
                    }
                    break;
            }
        }

        public IEnumerable<ModelRenderGeometry> GetRenderContext(GraphicsDevice device)
        {
            return ModelHelper.GetModelRenderContext(device, Model as WorldModelInstance);
        }
    }
}