﻿using System.Collections.Generic;
using Arcanos.Client.GameClient.Graphics;
using Arcanos.Client.GameClient.Rendering;
using Arcanos.Shared;
using Arcanos.Shared.Collisions;
using Arcanos.Shared.Data;
using Arcanos.Shared.Graphics;
using Arcanos.Shared.Objects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Arcanos.Client.GameClient.Objects
{
    public class StaticModelObject : StaticModelObjectBase, IRenderable<int, ModelRenderGeometry>, IPostDeserialization
    {
        protected StaticModelObject() { }
        public StaticModelObject(WorldModel model, StaticModelObjectFlags flags, Shape shape, Vector3 position, Rotation rotation, Vector3 size)
        {
            Model = model;
            Flags = flags;
            Shape = shape;
            Position = position;
            Rotation = rotation;
            Size = size;
            Build();
        }

        public IEnumerable<ModelRenderGeometry> GetRenderContext(GraphicsDevice device)
        {
            return ModelHelper.GetModelRenderContext(device, Model);
        }

        public void MemberDeserialized(string member, object value) { }
        public void Deserialized(object parent)
        {
            Build();
        }
    }
}