﻿using System.Collections.Generic;
using Arcanos.Client.GameClient.Graphics;
using Arcanos.Client.GameClient.Rendering;
using Arcanos.Shared;
using Arcanos.Shared.Graphics;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Objects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Arcanos.Client.GameClient.Objects
{
    public class WorldObject : WorldObjectBase, IRenderable<int, ModelRenderGeometry>
    {
        public new WorldObjectTemplate Template { get { return (WorldObjectTemplate)template; } }

        public override string Name
        {
            get { return template.Name == null ? Text.MissingString : template.Name.ToString(); }
        }
        public override Text NameRef
        {
            get { return template.Name; }
        }

        public WorldObject(ulong guid, WorldObjectTemplate template, Vector3 pos, Rotation rot) : base(guid, template, pos, rot)
        {
            ResetFromTemplate();
        }

        public override void OnPositionChanged()
        {
        }
        public override void OnRotationChanged()
        {
        }
        public override void OnAddedToMap()
        {
        }
        public override void OnRemovedFromMap()
        {
        }

        public IEnumerable<ModelRenderGeometry> GetRenderContext(GraphicsDevice device)
        {
            return ModelHelper.GetModelRenderContext(device, Model as WorldModelInstance);
        }
    }
}