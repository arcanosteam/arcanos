﻿using System;
using Arcanos.Client.GameClient.Input;
using Arcanos.Client.GameClient.World;
using Arcanos.Shared;
using Arcanos.Shared.Movement;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Timing;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Arcanos.Client.GameClient.Objects
{
    public class Camera : IWorldEntity
    {
        protected const float PITCH_LIMIT_MARGIN = 0.0001f;

        public virtual Vector3 Position { get; set; }
        public virtual Vector3 Target { get; set; }
        public virtual Rotation Rotation
        {
            get { return Position.GetRotationTo(Target); }
            set { Target = Position + value.Forward * Distance; }
        }
        public virtual Rotation Direction
        {
            get { return Rotation; }
            set { Rotation = value; }
        }
        public virtual Vector3 Up { get { return Rotation.Up; } }
        public virtual float Distance
        {
            get { return Vector3.Distance(Position, Target); }
            set { Target = Position + Rotation.Forward * value; }
        }

        private int width = 1, height = 1;
        private float near = 1, far = 10000;
        private float fov;
        private Matrix projection;
        private Viewport viewport;
        public int Width
        {
            get { return width; }
            private set
            {
                width = value;
                RebuildProjection();
            }
        }
        public int Height
        {
            get { return height; }
            private set
            {
                height = value;
                RebuildProjection();
            }
        }
        public float NearClip
        {
            get { return near; }
            protected set
            {
                near = value;
                RebuildProjection();
            }
        }
        public float FarClip
        {
            get { return far; }
            protected set
            {
                far = value;
                RebuildProjection();
            }
        }
        public float FOV
        {
            get { return fov; }
            set
            {
                fov = value;
                RebuildProjection();
            }
        }

        public Matrix Projection
        {
            get { return projection; }
        }
        public Matrix View
        {
            get { return Matrix.CreateLookAt(Position, Target, Up); } // TODO: Optimize
        }

        public Camera()
        {
            FOV = MathHelper.PiOver2;
        }

        public virtual void Update() { }

        public virtual void Translate(Vector3 d)
        {
            Position += d;
            Target += d;
        }

        public void SetSize(int w, int h)
        {
            Width = w;
            Height = h;
        }
        public Vector3 Project(Vector3 worldPos)
        {
            Vector3 screenLoc = viewport.Project(worldPos, projection, View, WorldManager.WorldMatrix);
            Vector3 worldPos2 = viewport.Unproject(new Vector3(screenLoc.X, screenLoc.Y, near), projection, View, WorldManager.WorldMatrix);
            Vector3.Distance(ref worldPos2, ref worldPos, out screenLoc.Z);
            screenLoc.Z += near;
            return screenLoc;
        }
        public Vector3 Project(Vector3 worldPos, out float nativeZ)
        {
            Vector3 screenLoc = viewport.Project(worldPos, projection, View, WorldManager.WorldMatrix);
            nativeZ = (screenLoc.Z - near) / (far - near);
            /*Vector3 worldPos2 = viewport.Unproject(new Vector3(screenLoc.X, screenLoc.Y, near), projection, View, WorldManager.WorldMatrix);
            Vector3.Distance(ref worldPos2, ref worldPos, out screenLoc.Z);
            screenLoc.Z += near;*/
            return screenLoc;
        }
        public Vector3 Unproject(Vector2 screenLoc, float worldZ)
        {
            Ray r = Unproject(screenLoc);

            Vector3 n = new Vector3(0, 0, 1);
            Plane p = new Plane(n, 0);

            float denominator = Vector3.Dot(p.Normal, r.Direction);
            float numerator = Vector3.Dot(p.Normal, r.Position) + p.D;
            float t = -(numerator / denominator);

            return r.Position + r.Direction * t;
        }
        public Ray Unproject(Vector2 screenLoc)
        {
            Vector3 nearPoint = viewport.Unproject(new Vector3(screenLoc, near), projection, View, WorldManager.WorldMatrix),
                    farPoint = viewport.Unproject(new Vector3(screenLoc, far), projection, View, WorldManager.WorldMatrix);

            Vector3 direction = farPoint - nearPoint;
            direction.Normalize();

            return new Ray(nearPoint, direction);
        }

        public bool TestFrustum(ref BoundingBox bbox)
        {
            BoundingFrustum frustum = new BoundingFrustum(View * Projection);
            ContainmentType containment;
            frustum.Contains(ref bbox, out containment);
            return containment != ContainmentType.Disjoint;
        }

        protected virtual void RebuildProjection()
        {
            projection = Matrix.CreatePerspectiveFieldOfView(fov, (float)width / height, near, far);
            viewport = new Viewport { Width = width, Height = height, MinDepth = near, MaxDepth = far };
        }

        public virtual void KeyPressed(Keys key) { }
        public virtual void KeyReleased(Keys key) { }
        public virtual void MouseKeyPressed(MouseKeys key) { }
        public virtual void MouseKeyReleased(MouseKeys key) { }
        public virtual void MouseMoved(Vector2 loc) { }
        public virtual void MouseScrolled(int amount) { }

        public Vector3 GetPosition()
        {
            return Position;
        }
        public Rotation GetRotation()
        {
            return Rotation;
        }
        public Vector3 GetSize()
        {
            return Vector3.One;
        }
    }
    public class FreeCamera : Camera
    {
        public float MoveSpeed = 50,
                     RotateSpeed = 0.01f;
        public override float Distance
        {
            get { return 1; }
        }

        private bool moveLeft, moveRight, moveForward, moveBackward, moveUp, moveDown;
        private bool speedUp, rotating;
        public override void Update()
        {
            base.Update();
            Vector3 oldPos = Position;
            Rotation oldRot = Rotation;
            MovementFlags flags = MovementFlags.Fly;
            float mul = speedUp ? 2.5f : 1;
            if (moveForward && (flags |= MovementFlags.Forward) != 0)
                Translate(Rotation.Forward * MoveSpeed * mul * Time.PerSecond);
            if (moveBackward && (flags |= MovementFlags.Backward) != 0)
                Translate(Rotation.Backward * MoveSpeed * mul * Time.PerSecond);
            if (moveLeft && (flags |= MovementFlags.Left) != 0)
                Translate(Rotation.Left * MoveSpeed * mul * Time.PerSecond);
            if (moveRight && (flags |= MovementFlags.Right) != 0)
                Translate(Rotation.Right * MoveSpeed * mul * Time.PerSecond);
            if (moveUp && (flags |= MovementFlags.Up) != 0)
                Translate(Vector3.UnitZ * MoveSpeed * mul * Time.PerSecond);
            if (moveDown && (flags |= MovementFlags.Down) != 0)
                Translate(-Vector3.UnitZ * MoveSpeed * mul * Time.PerSecond);
            if (rotating)
            {
                int dX = InputManager.MouseX - Width / 2,
                    dY = InputManager.MouseY - Height / 2;
                Rotation rotation = Rotation;
                if (dX != 0)
                    rotation = new Rotation(rotation.Yaw - dX * RotateSpeed, rotation.Pitch, rotation.Roll);
                if (dY != 0)
                    rotation = new Rotation(rotation.Yaw, rotation.Pitch - dY * RotateSpeed, rotation.Roll);
                if (rotation.Pitch > (float)Math.PI / 2 - PITCH_LIMIT_MARGIN)
                    rotation.Pitch = MathHelper.Pi / 2 - PITCH_LIMIT_MARGIN;
                if (rotation.Pitch < -(float)Math.PI / 2 + PITCH_LIMIT_MARGIN)
                    rotation.Pitch = -MathHelper.Pi / 2 + PITCH_LIMIT_MARGIN;
                Rotation = rotation;
                InputManager.SetMousePosition(Width / 2, Height / 2);
            }
        }

        public override void KeyPressed(Keys key)
        {
            base.KeyPressed(key);
            switch (key)
            {
                case Keys.W:
                    moveForward = true;
                    break;
                case Keys.S:
                    moveBackward = true;
                    break;
                case Keys.A:
                    moveLeft = true;
                    break;
                case Keys.D:
                    moveRight = true;
                    break;
                case Keys.Space:
                    moveUp = true;
                    break;
                case Keys.LeftControl:
                case Keys.RightControl:
                    moveDown = true;
                    break;
                case Keys.LeftShift:
                case Keys.RightShift:
                    speedUp = true;
                    break;
            }
        }
        public override void KeyReleased(Keys key)
        {
            base.KeyReleased(key);
            switch (key)
            {
                case Keys.W:
                    moveForward = false;
                    break;
                case Keys.S:
                    moveBackward = false;
                    break;
                case Keys.A:
                    moveLeft = false;
                    break;
                case Keys.D:
                    moveRight = false;
                    break;
                case Keys.Space:
                    moveUp = false;
                    break;
                case Keys.LeftControl:
                case Keys.RightControl:
                    moveDown = false;
                    break;
                case Keys.LeftShift:
                case Keys.RightShift:
                    speedUp = false;
                    break;
            }
        }
        private int mX, mY;
        public override void MouseKeyPressed(MouseKeys key)
        {
            base.MouseKeyPressed(key);
            switch (key)
            {
                case MouseKeys.Left:
                    rotating = true;
                    mX = InputManager.MouseX;
                    mY = InputManager.MouseY;
                    InputManager.SetMouseVisible(false);
                    InputManager.SetMousePosition(Width / 2, Height / 2);
                    break;
            }
        }
        public override void MouseKeyReleased(MouseKeys key)
        {
            base.MouseKeyReleased(key);
            switch (key)
            {
                case MouseKeys.Left:
                    rotating = false;
                    InputManager.SetMousePosition(mX, mY);
                    InputManager.SetMouseVisible(true);
                    break;
            }
        }
    }
    public class TargetedCamera : Camera
    {
        public float RotateSpeed = 0.01f;
        public WorldObjectBase TargetedObject;

        public override Vector3 Target
        {
            get
            {
                Vector3 pos = TargetedObject.Position;
                pos.Z += TargetedObject.GetHeight() * TargetedObject.Template.CameraHeightPercent;
                return pos;
            }
        }
        public override Vector3 Position
        {
            get { return base.Position = Target + Direction.Backward * Distance; }
        }
        public override Rotation Rotation { get; set; }
        public override Rotation Direction
        {
            get { return Rotation + TargetedObject.Rotation; }
            set { Rotation = value - TargetedObject.Rotation; }
        }
        public override Vector3 Up
        {
            get { return (Rotation + TargetedObject.Rotation).Up; }
        }
        public override float Distance { get; set; }

        public override void Update()
        {
            base.Update();
            if (!rotating)
                return;
            float dX = (InputManager.MouseX - Width / 2)  * RotateSpeed,
                  dY = (InputManager.MouseY - Height / 2) * RotateSpeed;
            if (rotatingObject)
            {
                Rotation rotation = new Rotation(TargetedObject.Rotation.Yaw - dX, TargetedObject.Rotation.Pitch - dY, TargetedObject.Rotation.Roll);
                if (rotation.Pitch > MathHelper.Pi / 2 - PITCH_LIMIT_MARGIN)
                    rotation.Pitch = MathHelper.Pi / 2 - PITCH_LIMIT_MARGIN;
                if (rotation.Pitch < -MathHelper.Pi / 2 + PITCH_LIMIT_MARGIN)
                    rotation.Pitch = -MathHelper.Pi / 2 + PITCH_LIMIT_MARGIN;
                if (TargetedObject.Type == ObjectType.PlayerCreature)
                    (TargetedObject as PlayerCreature).RotateTo(rotation);
                else
                    TargetedObject.RotateTo(rotation);
                Rotation = new Rotation();
            }
            else
            {
                Rotation rotation = new Rotation(Rotation.Yaw - dX, Rotation.Pitch - dY, Rotation.Roll);
                if (rotation.Pitch + TargetedObject.Rotation.Pitch > (float)Math.PI / 2 - PITCH_LIMIT_MARGIN)
                    rotation.Pitch = MathHelper.Pi / 2 - PITCH_LIMIT_MARGIN - TargetedObject.Rotation.Pitch;
                if (rotation.Pitch + TargetedObject.Rotation.Pitch < -(float)Math.PI / 2 + PITCH_LIMIT_MARGIN)
                    rotation.Pitch = -MathHelper.Pi / 2 + PITCH_LIMIT_MARGIN - TargetedObject.Rotation.Pitch;
                Rotation = rotation;
            }
            InputManager.SetMousePosition(Width / 2, Height / 2);
        }

        private bool rotating, rotatingObject;
        private bool rotatingQueued, rotatingObjectQueued;
        private int mX, mY, movedCounter;
        private Vector2 queuedStartLoc;
        public override void MouseKeyPressed(MouseKeys key)
        {
            base.MouseKeyPressed(key);
            queuedStartLoc = new Vector2(InputManager.MouseX, InputManager.MouseY);
            switch (key)
            {
                case MouseKeys.Right:
                    rotatingObjectQueued = true;
                    break;
                case MouseKeys.Left:
                    rotatingQueued = true;
                    break;
            }
        }
        public override void MouseKeyReleased(MouseKeys key)
        {
            base.MouseKeyReleased(key);
            movedCounter = 0;
            rotatingQueued = rotatingObjectQueued = false;
            switch (key)
            {
                case MouseKeys.Right:
                    rotatingObject = false;
                    rotatingObjectQueued = false;
                    goto case MouseKeys.Left;
                case MouseKeys.Left:
                    if (rotating)
                        InputManager.SetMousePosition(mX, mY);
                    InputManager.SetMouseVisible(true);
                    rotating = false;
                    rotatingQueued = false;
                    break;
            }
        }
        public override void MouseMoved(Vector2 loc)
        {
            base.MouseMoved(loc);
            if ((!rotatingQueued && !rotatingObjectQueued) || rotating || rotatingObject)
                return;
            if (++movedCounter >= 8 || Vector2.Distance(queuedStartLoc, loc) >= 8)
            {
                movedCounter = 0;
                if (rotatingObjectQueued)
                {
                    rotatingObject = true;
                    if (TargetedObject.Type == ObjectType.PlayerCreature)
                        (TargetedObject as PlayerCreature).RotateTo(Direction);
                    else
                        TargetedObject.RotateTo(Direction);
                    Rotation = new Rotation();
                }
                if (rotatingQueued || rotatingObjectQueued)
                {
                    rotating = true;
                    mX = InputManager.MouseX;
                    mY = InputManager.MouseY;
                    InputManager.SetMouseVisible(false);
                    InputManager.SetMousePosition(Width / 2, Height / 2);
                }
                rotatingQueued = rotatingObjectQueued = false;
            }

        }
        public override void MouseScrolled(int amount)
        {
            base.MouseScrolled(amount);
            if (amount > 0)
                Distance *= 1.2f;
            else
                Distance /= 1.2f;
            Rotation = Rotation;
        }
    }
}