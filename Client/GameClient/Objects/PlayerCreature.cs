﻿using System;
using System.Collections.Generic;
using Arcanos.Client.GameClient.Data;
using Arcanos.Client.GameClient.Graphics;
using Arcanos.Client.GameClient.Movement;
using Arcanos.Client.GameClient.Players;
using Arcanos.Client.GameClient.Rendering;
using Arcanos.Client.GameClient.Spells;
using Arcanos.Client.GameClient.UI;
using Arcanos.Client.GameClient.World;
using Arcanos.Shared;
using Arcanos.Shared.Graphics;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Movement;
using Arcanos.Shared.Net;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Spells;
using Arcanos.Shared.Timing;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Arcanos.Client.GameClient.Objects
{
    public class PlayerCreature : PlayerCreatureBase, IGaugeSource, IRenderable<int, ModelRenderGeometry>
    {
        public static int TargetDecalIndex;

        public new Character Character
        {
            get { return (Character)character; }
        }

        public override string Name
        {
            get { return character.Name ?? Text.MissingString; }
        }
        public override Text NameRef
        {
            get { return null; }
        }

        public MovementFlags MovementFlags;
        public Rotation MovementDirection;
        public float RotationHorizontalSpeed = MathHelper.Pi;
        public float RotationVerticalSpeed = MathHelper.Pi;

        public ulong TargetGUID;

        private long nextMovementUpdate;

        public PlayerCreature(ulong guid, CreatureTemplate template, Character character, Vector3 pos, Rotation rot) : base(guid, template, character, pos, rot)
        {
            Mover = new Mover(this);

            SpellBook = new SpellBook(this);

            if (this == WorldManager.PlayerCreature)
                TargetDecalIndex = TerrainRenderer.RegisterDecal(null, new Vector3(-1000), 1);

            ResetFromTemplate();
        }

        public override bool Update()
        {
            if (!base.Update())
                return false;

            UpdateMovementFlags();
            UpdateRotationFlags();
            UpdateTarget();
            SpellBook.Update();
            /*if (!Mover.Finished)
                Model.PlayAnimation("Walk");
            else
                Model.StopAnimation("Walk");*/

            return true;
        }

        private void UpdateMovementFlags()
        {
            if (!IsAlive || HasFlag(ObjectFlags.Stunned | ObjectFlags.Rooted))
                return;

            if ((MovementFlags & MovementFlags.Directions) != 0)
            {
                // Calculating new position (raw)
                float yaw = 0,
                      pitch = 0,
                      dist = GetMovementSpeed() * Time.PerSecond;
                bool backwards = false;
                bool handled = false;
                switch (MovementFlags & (MovementFlags.Forward | MovementFlags.Backward))
                {
                    case MovementFlags.Forward:
                        yaw = Rotation.Yaw;
                        if ((MovementFlags & MovementFlags.Left) != 0)
                            yaw += MathHelper.PiOver4;
                        if ((MovementFlags & MovementFlags.Right) != 0)
                            yaw -= MathHelper.PiOver4;

                        if ((MovementFlags & MovementFlags.FreeVerticalMovement) != 0)
                            pitch = Rotation.Pitch;

                        handled = true;
                        break;
                    case MovementFlags.Backward:
                        yaw = Rotation.Yaw + MathHelper.Pi;
                        if ((MovementFlags & MovementFlags.Left) != 0)
                            yaw -= MathHelper.PiOver4;
                        if ((MovementFlags & MovementFlags.Right) != 0)
                            yaw += MathHelper.PiOver4;

                        if ((MovementFlags & MovementFlags.FreeVerticalMovement) != 0)
                            pitch = -Rotation.Pitch;

                        backwards = true;
                        handled = true;
                        break;
                }

                if (!handled)
                    switch (MovementFlags & (MovementFlags.Left | MovementFlags.Right))
                    {
                        case MovementFlags.Left:
                            yaw = Rotation.Yaw + MathHelper.PiOver2;
                            handled = true;
                            break;
                        case MovementFlags.Right:
                            yaw = Rotation.Yaw - MathHelper.PiOver2;
                            handled = true;
                            break;
                    }

                if (!handled)
                    return;

                // Calculating new position
                Vector3 newPos = new Vector3(
                    Position.X + (float)Math.Cos(yaw) * dist,
                    Position.Y + (float)Math.Sin(yaw) * dist,
                    Position.Z + (float)Math.Sin(pitch) * dist);
                if ((MovementFlags & MovementFlags.FreeVerticalMovement) != 0)
                {
                    float newZ = Map.Template.GetHeight(newPos.X, newPos.Y);
                    if (newPos.Z < newZ)
                        newPos.Z = newZ;
                }
                else
                    newPos.Z = Map.Template.GetHeight(newPos.X, newPos.Y);

                // Finalization
                if (IsMovementValid(Position, newPos, true) == MovementValidity.Ok)
                {
                    MoveTo(newPos);
                    MovementDirection.Yaw = yaw;
                    MovementDirection.Pitch = pitch;
                    Model.PlayAnimation(backwards ? "WalkBackwards" : "Walk");
                    if (WorldManager.IsCharacterInGame && WorldManager.PlayerCreature == this && Time.Timestamp >= nextMovementUpdate)
                    {
                        nextMovementUpdate = Time.Timestamp + 250;
                        SendMovementUpdatePacket();
                    }
                }
            }
            else
            {
                Model.StopAnimation("Walk");
                Model.StopAnimation("WalkBackwards");
            }
        }
        private void UpdateRotationFlags()
        {
            if (!IsAlive || HasFlag(ObjectFlags.Stunned))
                return;

            if ((MovementFlags & MovementFlags.Rotation) != 0)
            {
                bool failed = false;

                float yaw = 0,
                      pitch = 0;
                switch (MovementFlags & (MovementFlags.RotationLeft | MovementFlags.RotationRight))
                {
                    case MovementFlags.RotationLeft:
                        yaw += RotationHorizontalSpeed;
                        break;
                    case MovementFlags.RotationRight:
                        yaw -= RotationHorizontalSpeed;
                        break;
                }
                switch (MovementFlags & (MovementFlags.RotationUp | MovementFlags.RotationDown))
                {
                    case MovementFlags.RotationUp:
                        pitch += RotationVerticalSpeed;
                        break;
                    case MovementFlags.RotationDown:
                        pitch -= RotationVerticalSpeed;
                        break;
                }

                if (yaw != 0)
                    yaw *= Time.PerSecond;
                if (pitch != 0)
                    pitch *= Time.PerSecond;

                // TODO: Checks

                if (!failed)
                    base.RotateTo(new Rotation(Rotation.Yaw + yaw, Rotation.Pitch + pitch, Rotation.Roll));
            }
        }
        private void UpdateTarget()
        {
            SetTarget(ObjectManager.GetCreature(TargetGUID));
        }

        public void PerformedMeleeAttack(CreatureBase target, AttackType type, AttackResult result)
        {
            switch (result)
            {
                case AttackResult.Hit:
                    Model.PlayAnimationOnce("MeleeAttackHit");
                    target.Model.PlayAnimationOnce("MeleeHit");
                    break;
                case AttackResult.Miss:
                    Model.PlayAnimationOnce("MeleeAttackMiss");
                    target.Model.PlayAnimationOnce("MeleeMiss");
                    break;
            }
        }
        public void PerformedSpellAttack(CreatureBase target, SpellTemplate spell, AttackResult result)
        {
            // Animations handled in SpellVisual
        }

        public void CastSpell(SpellTemplate spell, CreatureBase target)
        {
            CastSpellResult result = SpellBook.CanCastSpell(spell, target, CastSpellFlags.None);
            if (result != CastSpellResult.Ok)
            {
                if (this == WorldManager.PlayerCreature)
                    UIManager.RaiseEvent(UIEvents.GameError, this, Localization.Game.ErrorCastSpellResult[(byte)result]);
                return;
            }

            WorldManager.PlayerSession.SendPacket(new CCombatCastSpellPacket { SpellID = spell.ID, TargetGUID = target == null ? 0 : target.GUID });
        }
        public void CastSpell(SpellTemplate spell, Vector3 target)
        {
            CastSpellResult result = SpellBook.CanCastSpell(spell, target, CastSpellFlags.None);
            if (result != CastSpellResult.Ok)
            {
                if (this == WorldManager.PlayerCreature)
                    UIManager.RaiseEvent(UIEvents.GameError, this, Localization.Game.ErrorCastSpellResult[(byte)result]);
                return;
            }

            WorldManager.PlayerSession.SendPacket(new CCombatCastGroundSpellPacket { SpellID = spell.ID, Target = target });
        }

        public override void SetState(CreatureState state)
        {
            base.SetState(state);
            switch (state)
            {
                case CreatureState.JustSpawned:
                    Model.PlayAnimation("Spawn", "Stand");
                    break;
                case CreatureState.Alive:
                    if (Model.CurrentAnimationName != "Spawn")
                        Model.PlayAnimation("Stand");
                    break;
                case CreatureState.JustDied:
                    Model.PlayAnimation("Death", "Dead");
                    break;
                case CreatureState.Corpse:
                    if (Model.CurrentAnimationName != "Death")
                        Model.PlayAnimation("Dead");
                    break;
                case CreatureState.Despawned:
                    break;
            }
            UIManager.RaiseEvent(UIEvents.CreatureStateChanged, this, State);
        }
        public override void SetTarget(CreatureBase target)
        {
            TargetGUID = target == null ? 0 : target.GUID;
            CreatureBase oldTarget = Target;
            base.SetTarget(target);
            if (Target != oldTarget)
            {
                UIManager.RaiseEvent(UIEvents.CreatureTargetChanged, this, Target);
                if (this == WorldManager.PlayerCreature)
                {
                    if (Target == null)
                    {
                        TerrainRenderer.MoveDecal(TargetDecalIndex, new Vector3(-1000));
                        return;
                    }
                    int textureID = 0;
                    switch (GetReactionTo(Target))
                    {
                        case Reaction.Friendly:
                            textureID = FriendlyTargetDecalTextureID;
                            break;
                        case Reaction.Neutral:
                            textureID = NeutralTargetDecalTextureID;
                            break;
                        case Reaction.Hostile:
                            textureID = HostileTargetDecalTextureID;
                            break;
                    }
                    TerrainRenderer.UnregisterDecal(TargetDecalIndex);
                    TargetDecalIndex = TerrainRenderer.RegisterDecal(DatabaseManager.TextureData[textureID], Target.Position, Target.GetApproachRadius());
                }
            }
        }
        public void SetTarget(ulong targetGUID)
        {
            TargetGUID = targetGUID;

            if (TargetGUID == 0)
                SetTarget(null);
            else
            {
                CreatureBase target = ObjectManager.GetCreature(TargetGUID);
                if (target != null)
                    SetTarget(target);
            }
        }

        public override void SetHealth(int currentHealth)
        {
            base.SetHealth(currentHealth);
            UIManager.RaiseEvent(UIEvents.CreatureHealthChanged, this, CurrentHealth, MaxHealth);
        }
        public override void SetHealth(int currentHealth, int maxHealth)
        {
            base.SetHealth(currentHealth, maxHealth);
            UIManager.RaiseEvent(UIEvents.CreatureHealthChanged, this, CurrentHealth, MaxHealth);
        }
        public override void SetUsesPower(PowerType type, bool uses)
        {
            base.SetUsesPower(type, uses);
            UIManager.RaiseEvent(UIEvents.CreatureUsesPowerChanged, this, type, UsesPowerType[(byte)type]);
        }
        public override void SetPower(PowerType type, int currentPower)
        {
            base.SetPower(type, currentPower);
            UIManager.RaiseEvent(UIEvents.CreaturePowerChanged, this, type, CurrentPower[(byte)type], MaxPower[(byte)type]);
        }
        public override void SetPower(PowerType type, int currentPower, int maxPower)
        {
            base.SetPower(type, currentPower, maxPower);
            UIManager.RaiseEvent(UIEvents.CreaturePowerChanged, this, type, CurrentPower[(byte)type], MaxPower[(byte)type]);
        }

        public new void RotateTo(Rotation rot)
        {
            if (Rotation == rot)
                return;
            base.RotateTo(rot);
            if (WorldManager.IsCharacterInGame && WorldManager.PlayerCreature == this && Time.Timestamp >= nextMovementUpdate)
            {
                nextMovementUpdate = Time.Timestamp + 250;
                SendMovementUpdatePacket();
            }
        }

        public override void OnPositionChanged()
        {
            if (WorldManager.IsCharacterInGame && WorldManager.PlayerCreature.Target == this)
                TerrainRenderer.MoveDecal(TargetDecalIndex, Position);
        }
        public override void OnRotationChanged()
        {
        }
        public override void OnAddedToMap()
        {
        }
        public override void OnRemovedFromMap()
        {
        }

        public IEnumerable<ModelRenderGeometry> GetRenderContext(GraphicsDevice device)
        {
            return ModelHelper.GetModelRenderContext(device, Model as WorldModelInstance);
        }

        public void GetGaugeData(int gauge, out float value, out float min, out float max)
        {
            switch (gauge)
            {
                case -1:
                    value = CurrentHealth;
                    min = 0;
                    max = MaxHealth;
                    break;
                case -2:
                    if (SpellBook.CastingSpell != null)
                    {
                        value = SpellBook.CastingTime;
                        min = 0;
                        max = SpellBook.CastingSpell.CastingTime;
                    }
                    else
                    {
                        value = 0;
                        min = 0;
                        max = 1;
                    }
                    break;
                case -3:
                    value = Character.XP;
                    min = 0;
                    max = Character.XPToLevel;
                    break;
                default:
                    if (gauge < 0)
                    {
                        value = 0;
                        min = 0;
                        max = 0;
                    }
                    else
                    {
                        value = CurrentPower[gauge];
                        min = 0;
                        max = MaxPower[gauge];
                    }
                    break;
            }
        }

        public void StartMovement(MovementFlags direction)
        {
            MovementFlags |= direction;
            SendMovementUpdatePacket();
        }
        public void StopMovement(MovementFlags direction)
        {
            MovementFlags &= ~direction;
            if ((MovementFlags & MovementFlags.Directions) == 0)
                MovementFlags &= ~MovementFlags.Movement;
            SendMovementUpdatePacket();
        }

        private void SendMovementUpdatePacket()
        {
            WorldManager.PlayerSession.SendPacket(new CMovementUpdatePacket
            {
                GUID = GUID,
                Data = new MovementData
                {
                    Position = Position,
                    Rotation = Rotation,
                    Flags = MovementFlags,
                },
            });
        }
    }
}