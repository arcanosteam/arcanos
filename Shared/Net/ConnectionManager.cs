﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using Arcanos.Net.Packets;

namespace Arcanos.Net
{
    public delegate void PacketHandler<TInterface>(TInterface sPacket, byte[] rawData);
    public delegate void PacketTypeHandler<TEnum, TInterface>(TEnum type, TInterface sPacket, byte[] rawData) where TEnum : struct;
    public delegate TInterface RespondedPacketHandler<TInterface>(TInterface sPacket, byte[] rawData);
    public delegate IEnumerable<TInterface> MultipleRespondedPacketHandler<TInterface>(TInterface sPacket, byte[] rawData);
    public class ConnectionManager<TEnum, TInterface> where TEnum : struct
    {
        public TcpClient Client;
        public AsyncPacketReader<TEnum, TInterface> Reader;
        public AsyncPacketWriter<TEnum, TInterface> Writer;

        protected Dictionary<TEnum, PacketHandler<TInterface>> Handlers = new Dictionary<TEnum, PacketHandler<TInterface>>();
        protected Dictionary<TEnum, RespondedPacketHandler<TInterface>> ResponseHandlers = new Dictionary<TEnum, RespondedPacketHandler<TInterface>>();
        protected Dictionary<TEnum, MultipleRespondedPacketHandler<TInterface>> MultipleResponseHandlers = new Dictionary<TEnum, MultipleRespondedPacketHandler<TInterface>>();

        public event PacketTypeHandler<TEnum, TInterface> PacketReceived;
        public event PacketTypeHandler<TEnum, TInterface> PacketSent;
        public event Action ConnectionClosed;

        public ConnectionManager(TcpClient client)
        {
            Client = client;
            Stream stream = Client.GetStream();
            Reader = new AsyncPacketReader<TEnum, TInterface>(stream);
            Reader.PacketRead += PacketReadHandler;
            Writer = new AsyncPacketWriter<TEnum, TInterface>(stream);
            Writer.ConnectionClosed += ConnectionClosedHandler;
        }

        public void Start()
        {
            Reader.Start();
        }
        public void Stop()
        {
            Reader.Stop();
            Writer.Dispose();
        }

        protected void PacketReadHandler(TEnum type, TInterface packet, byte[] rawData)
        {
            if (PacketReceived != null)
                PacketReceived(type, packet, rawData);
            {
                PacketHandler<TInterface> handler;
                if (Handlers.TryGetValue(type, out handler))
                    handler(packet, rawData);
            }
            {
                RespondedPacketHandler<TInterface> handler;
                if (ResponseHandlers.TryGetValue(type, out handler))
                    Write(handler(packet, rawData));
            }
            {
                MultipleRespondedPacketHandler<TInterface> handler;
                if (MultipleResponseHandlers.TryGetValue(type, out handler))
                    foreach (TInterface response in handler(packet, rawData))
                        Write(response);
            }
        }
        protected void ConnectionClosedHandler(Exception ex)
        {
            if (ConnectionClosed != null)
                ConnectionClosed();
        }

        public void AddReadHandler(TEnum type, PacketHandler<TInterface> handler)
        {
            Handlers.Add(type, handler);
        }
        public void AddReadHandler(TEnum type, RespondedPacketHandler<TInterface> handler)
        {
            ResponseHandlers.Add(type, handler);
        }
        public void AddReadHandler(TEnum type, MultipleRespondedPacketHandler<TInterface> handler)
        {
            MultipleResponseHandlers.Add(type, handler);
        }
        public void SetReadHandler(TEnum type, PacketHandler<TInterface> handler)
        {
            Handlers[type] = handler;
        }
        public void Write(TInterface packet)
        {
            Writer.Write(packet);
            if (PacketSent != null)
                PacketSent(Writer.GetPacketType(packet), packet, null);
        }
        public void Write(TInterface packet, byte[] rawData)
        {
            Writer.Write(packet, rawData);
            if (PacketSent != null)
                PacketSent(Writer.GetPacketType(packet), packet, rawData);
        }
    }
}