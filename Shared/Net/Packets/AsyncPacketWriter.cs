﻿using System;
using System.IO;
using System.Threading;

namespace Arcanos.Net.Packets
{
    public class AsyncPacketWriter<TEnum, TInterface> : PacketWriter<TEnum, TInterface> where TEnum : struct
    {
        protected struct QueueEntry
        {
            public TInterface Packet;
            public byte[] RawData;

            public QueueEntry(TInterface packet)
            {
                Packet = packet;
                RawData = null;
            }
            public QueueEntry(TInterface packet, byte[] rawData)
            {
                Packet = packet;
                RawData = rawData;
            }
        }
        protected readonly Thread Thread;
        protected readonly BlockingQueue<QueueEntry> WriteQueue = new BlockingQueue<QueueEntry>();

        public event Action<Exception> ConnectionClosed;

        public AsyncPacketWriter(Stream stream) : base(stream)
        {
            Thread = new Thread(ThreadFunc) { Name = "AsyncPacketWriter", IsBackground = true };
            Thread.Start();
        }

        public override void Dispose()
        {
            base.Dispose();
            WriteQueue.Stop();
        }

        protected void ThreadFunc()
        {
            while (!disposed)
            {
                QueueEntry entry = WriteQueue.Dequeue();
                if (disposed)
                    break;
                try
                {
                    if (entry.RawData != null)
                        base.Write(entry.Packet, entry.RawData);
                    else
                        base.Write(entry.Packet);
                }
                catch (Exception ex)
                {
                    if (ConnectionClosed != null)
                        ConnectionClosed(ex);
                    break;
                }
            }
        }

        public override void Write(TInterface packet)
        {
            if (disposed)
                throw new ObjectDisposedException("this");

            WriteQueue.Enqueue(new QueueEntry(packet));
        }
        public override void Write(TInterface packet, byte[] rawData)
        {
            if (disposed)
                throw new ObjectDisposedException("this");

            WriteQueue.Enqueue(new QueueEntry(packet, rawData));
        }
    }
}