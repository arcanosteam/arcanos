﻿using System;
using System.IO;
using System.Threading;

namespace Arcanos.Net.Packets
{
    public class AsyncPacketReader<TEnum, TInterface> : PacketReader<TEnum, TInterface> where TEnum : struct
    {
        protected struct QueueEntry
        {
            public TEnum Type;
            public TInterface Packet;
            public byte[] RawData;

            public QueueEntry(TEnum type, TInterface packet)
            {
                Type = type;
                Packet = packet;
                RawData = null;
            }
            public QueueEntry(TEnum type, TInterface packet, byte[] rawData)
            {
                Type = type;
                Packet = packet;
                RawData = rawData;
            }
        }
        protected Thread Thread;
        protected Thread ExecuteThread;
        protected readonly BlockingQueue<QueueEntry> ReadQueue = new BlockingQueue<QueueEntry>();
        protected readonly BlockingQueue<QueueEntry> ExecuteQueue = new BlockingQueue<QueueEntry>();
        public bool InvokeOnSameThread = false;
        public bool ImmediateProcessing = true;

        public event PacketTypeHandler<TEnum, TInterface> PacketRead;

        public AsyncPacketReader(Stream stream) : base(stream)
        {
            stream.ReadTimeout = Timeout.Infinite;
        }

        public override void Dispose()
        {
            base.Dispose();
            ReadQueue.Stop();
            ExecuteQueue.Stop();
        }

        public void Start()
        {
            if (disposed)
                throw new ObjectDisposedException("this");

            stopRequested = false;
            Thread = new Thread(ThreadFunc) { Name = "AsyncPacketReader", IsBackground = true };
            Thread.Start();
            ExecuteThread = new Thread(ExecuteThreadFunc) { Name = "AsyncPacketReader Execute", IsBackground = true };
            ExecuteThread.Start();
        }
        public void Stop()
        {
            stopRequested = true;
        }

        public void ProcessQueue()
        {
            while (!ReadQueue.Empty)
            {
                QueueEntry entry = ReadQueue.Dequeue();
                if (PacketRead != null)
                    PacketRead(entry.Type, entry.Packet, entry.RawData);
            }
        }

        protected bool stopRequested;
        protected void ThreadFunc()
        {
            try
            {
                while (!stopRequested && !disposed)
                {
                    TEnum type;
                    byte[] rawData;
                    TInterface packet = base.Read(out type, out rawData);
                    if (disposed)
                        break;
                    if (ImmediateProcessing)
                        if (InvokeOnSameThread)
                        {
                            if (PacketRead != null)
                                PacketRead(type, packet, rawData);
                        }
                        else
                            ExecuteQueue.Enqueue(new QueueEntry(type, packet, rawData));
                    else
                        ReadQueue.Enqueue(new QueueEntry(type, packet, rawData));
                }
                Dispose();
            }
            catch { }
        }
        protected void ExecuteThreadFunc()
        {
            try
            {
                while (!stopRequested && !disposed)
                {
                    QueueEntry entry = ExecuteQueue.Dequeue();
                    if (disposed)
                        break;
                    if (PacketRead != null)
                        PacketRead(entry.Type, entry.Packet, entry.RawData);
                }
            }
            catch { }
        }
    }
}