﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;

namespace Arcanos.Net.Packets
{
    public class PacketReader<TEnum, TInterface> : IDisposable where TEnum : struct
    {
        protected Type EnumType;
        public Stream Stream;
        public BinaryReader Reader;
        public Dictionary<TEnum, Type> PacketTypes;
        protected bool disposed;

        public PacketReader(Stream stream)
        {
            if (!typeof(TEnum).IsEnum)
                throw new Exception();

            EnumType = Enum.GetUnderlyingType(typeof(TEnum));
            PacketTypes = new Dictionary<TEnum, Type>();
            Stream = stream;
            Reader = new BinaryReader(stream);
        }

        public virtual void Dispose()
        {
            Reader.Close();
            disposed = true;
        }

        public virtual TInterface Read()
        {
            if (disposed)
                throw new ObjectDisposedException("this");

            TEnum type;
            byte[] rawData;
            return Read(out type, out rawData);
        }
        public virtual TInterface Read(out TEnum type)
        {
            if (disposed)
                throw new ObjectDisposedException("this");

            byte[] rawData;
            return Read(out type, out rawData);
        }
        public virtual TInterface Read(out byte[] rawData)
        {
            if (disposed)
                throw new ObjectDisposedException("this");

            TEnum type;
            return Read(out type, out rawData);
        }
        public virtual TInterface Read(out TEnum type, out byte[] rawData)
        {
            if (disposed)
                throw new ObjectDisposedException("this");

            if (EnumType == typeof(Byte))
                type = (TEnum)(object)Reader.ReadByte();
            else if (EnumType == typeof(SByte))
                type = (TEnum)(object)Reader.ReadSByte();
            else if (EnumType == typeof(Int16))
                type = (TEnum)(object)Reader.ReadInt16();
            else if (EnumType == typeof(UInt16))
                type = (TEnum)(object)Reader.ReadUInt16();
            else if (EnumType == typeof(Int32))
                type = (TEnum)(object)Reader.ReadInt32();
            else if (EnumType == typeof(UInt32))
                type = (TEnum)(object)Reader.ReadUInt32();
            else if (EnumType == typeof(Int64))
                type = (TEnum)(object)Reader.ReadInt64();
            else if (EnumType == typeof(UInt64))
                type = (TEnum)(object)Reader.ReadUInt64();
            else
                throw new Exception();
            Type packetType = PacketTypes[type];
            
            byte[] packetData = Reader.ReadBytes(Marshal.SizeOf(packetType));

            GCHandle dataHandle = GCHandle.Alloc(packetData, GCHandleType.Pinned);
            object packet = Marshal.PtrToStructure(dataHandle.AddrOfPinnedObject(), packetType);
            dataHandle.Free();

            object[] attributes = packetType.GetCustomAttributes(typeof(PacketTypeAttribute), true);
            if (attributes.Length == 0)
                throw new Exception();
            rawData = ((PacketTypeAttribute)attributes[0]).RawDataFollows ? ReadRawData() : null;

            return (TInterface)packet;
        }
        protected virtual byte[] ReadRawData()
        {
            int length = Reader.ReadInt32();
            return length > 0 ? Reader.ReadBytes(length) : new byte[0];
        }
    }
}