﻿using System;

namespace Arcanos.Net.Packets
{
    public class PacketTypeAttribute : Attribute
    {
        public object Type;
        public bool RawDataFollows;

        public PacketTypeAttribute(object type)
        {
            Type = type;
        }
        public PacketTypeAttribute(object type, bool rawDataFollows)
        {
            Type = type;
            RawDataFollows = rawDataFollows;
        }
    }
}