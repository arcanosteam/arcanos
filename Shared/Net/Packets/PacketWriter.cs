﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;

namespace Arcanos.Net.Packets
{
    public class PacketWriter<TEnum, TInterface> : IDisposable where TEnum : struct
    {
        protected Type EnumType;
        public Stream Stream;
        public BinaryWriter Writer;
        public Dictionary<TEnum, Type> PacketTypes;
        protected bool disposed;

        public PacketWriter(Stream stream)
        {
            if (!typeof(TEnum).IsEnum)
                throw new Exception();

            EnumType = Enum.GetUnderlyingType(typeof(TEnum));
            PacketTypes = new Dictionary<TEnum, Type>();
            Stream = stream;
            Writer = new BinaryWriter(stream);
        }

        public virtual void Dispose()
        {
            Writer.Close();
            disposed = true;
        }

        public virtual TEnum GetPacketType(TInterface packet)
        {
            TEnum type = default(TEnum);
            bool foundType = false;
            Type packetType = packet.GetType();
            foreach (KeyValuePair<TEnum, Type> pair in PacketTypes)
                if (pair.Value == packetType)
                {
                    type = pair.Key;
                    foundType = true;
                    break;
                }
            if (!foundType)
                throw new KeyNotFoundException("Packet of type " + packet.GetType() + " is not defined in PacketTypes collection.");
            return type;
        }
        
        public virtual void Write(TInterface packet)
        {
            RawWrite(packet);
        }
        public void RawWrite(TInterface packet)
        {
            if (disposed)
                throw new ObjectDisposedException("this");

            TEnum type = GetPacketType(packet);

            if (EnumType == typeof(Byte))
                Writer.Write((Byte)(object)type);
            else if (EnumType == typeof(SByte))
                Writer.Write((SByte)(object)type);
            else if (EnumType == typeof(Int16))
                Writer.Write((Int16)(object)type);
            else if (EnumType == typeof(UInt16))
                Writer.Write((UInt16)(object)type);
            else if (EnumType == typeof(Int32))
                Writer.Write((Int32)(object)type);
            else if (EnumType == typeof(UInt32))
                Writer.Write((UInt32)(object)type);
            else if (EnumType == typeof(Int64))
                Writer.Write((Int64)(object)type);
            else if (EnumType == typeof(UInt64))
                Writer.Write((UInt64)(object)type);
            else
                throw new Exception();

            int packetLength = Marshal.SizeOf(packet.GetType());

            IntPtr packetPointer = Marshal.AllocHGlobal(packetLength);
            byte[] packetData = new byte[packetLength];
            Marshal.StructureToPtr(packet, packetPointer, false);
            Marshal.Copy(packetPointer, packetData, 0, packetLength);
            Marshal.FreeHGlobal(packetPointer);

            Writer.Write(packetData);
        }
        public virtual void Write(TInterface packet, byte[] rawData)
        {
            if (disposed)
                throw new ObjectDisposedException("this");

            RawWrite(packet);

            Writer.Write(rawData.Length);
            if (rawData.Length > 0)
                Writer.Write(rawData);
        }
    }
}