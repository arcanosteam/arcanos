﻿using System;

namespace Arcanos.Utilities.Debug
{
    public static class D
    {
        public static void Fail(string message)
        {
            throw new Exception(message);
        }
        public static void Assert(bool condition, string message)
        {
            if (!condition)
                throw new Exception(message);
        }
        public static void NotNull(object obj, string message)
        {
            if (obj == null)
                throw new Exception(message);
        }
    }
}