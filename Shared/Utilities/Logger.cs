﻿using System;
using System.IO;
using System.Reflection;
using System.Text;

namespace Arcanos.Utilities
{
    [Flags]
    public enum LogCategory : ulong
    {
        General     = 0x0000000000000001,
        Status      = 0x0000000000000002,
        Database    = 0x0000000000000004,
        Packets     = 0x0000000000000008,
        Grid        = 0x0000000000000010,
        World       = 0x0000000000000020,
        Player      = 0x0000000000000040,
        Scripting   = 0x0000000000000080,
        Connection  = 0x0000000000000100,
        Auras       = 0x0000000000000200,
        Spells      = 0x0000000000000400,
        UI          = 0x0000000000000800,
    }
    public enum LogSeverity : byte
    {
        Trace,
        Debug,
        Info,
        Warning,
        Error,
        Fatal,
    }
    public static class Logger
    {
        public static LoggerSettings Settings = new LoggerSettings();
        private static StreamWriter LogFile;

        static Logger()
        {
            System.Console.ForegroundColor = ConsoleColor.White;
        }

        public static string Unwrap(object obj)
        {
            string result = "{ ";
            foreach (FieldInfo field in obj.GetType().GetFields())
                if (field.FieldType == typeof(string))
                    result += field.Name + "=\"" + field.GetValue(obj) + "\" ";
                else
                    result += field.Name + "=" + field.GetValue(obj) + " ";
            foreach (PropertyInfo property in obj.GetType().GetProperties())
                if (property.PropertyType == typeof(string))
                    result += property.Name + "=\"" + property.GetValue(obj, null) + "\" ";
                else
                    result += property.Name + "=" + property.GetValue(obj, null) + " ";
            return result.Remove(result.Length - 1) + " }";
        }

        public static readonly object outputLock = new object();
        public static void Trace(LogCategory category, object obj)
        {
            Log(category, LogSeverity.Trace, obj);
        }
        public static void Trace(LogCategory category, string format, params object[] args)
        {
            Log(category, LogSeverity.Trace, format, args);
        }
        public static void Trace(LogCategory category, string entry)
        {
            Log(category, LogSeverity.Trace, entry);
        }
        public static void Debug(LogCategory category, object obj)
        {
            Log(category, LogSeverity.Debug, obj);
        }
        public static void Debug(LogCategory category, string format, params object[] args)
        {
            Log(category, LogSeverity.Debug, format, args);
        }
        public static void Debug(LogCategory category, string entry)
        {
            Log(category, LogSeverity.Debug, entry);
        }
        public static void Info(LogCategory category, object obj)
        {
            Log(category, LogSeverity.Info, obj);
        }
        public static void Info(LogCategory category, string format, params object[] args)
        {
            Log(category, LogSeverity.Info, format, args);
        }
        public static void Info(LogCategory category, string entry)
        {
            Log(category, LogSeverity.Info, entry);
        }
        public static void Warning(LogCategory category, object obj)
        {
            Log(category, LogSeverity.Warning, obj);
        }
        public static void Warning(LogCategory category, string format, params object[] args)
        {
            Log(category, LogSeverity.Warning, format, args);
        }
        public static void Warning(LogCategory category, string entry)
        {
            Log(category, LogSeverity.Warning, entry);
        }
        public static void Error(LogCategory category, object obj)
        {
            Log(category, LogSeverity.Error, obj);
        }
        public static void Error(LogCategory category, string format, params object[] args)
        {
            Log(category, LogSeverity.Error, format, args);
        }
        public static void Error(LogCategory category, string entry)
        {
            Log(category, LogSeverity.Error, entry);
        }
        public static void Fatal(LogCategory category, object obj)
        {
            Log(category, LogSeverity.Fatal, obj);
        }
        public static void Fatal(LogCategory category, string format, params object[] args)
        {
            Log(category, LogSeverity.Fatal, format, args);
        }
        public static void Fatal(LogCategory category, string entry)
        {
            Log(category, LogSeverity.Fatal, entry);
        }
        public static void Log(LogCategory category, LogSeverity severity, object obj)
        {
            if (!Settings.ShouldLog(category, severity))
                return;
            lock (outputLock)
            {
                System.Console.ForegroundColor = Settings.CategoryColor;
                System.Console.Write(category + ": ");
                System.Console.ForegroundColor = Settings.GetSeverityColor(severity);
                System.Console.WriteLine(obj);
                if (FileLogging())
                    LogLineToFile(category + ": " + obj);
            }
        }
        public static void Log(LogCategory category, LogSeverity severity, string format, params object[] args)
        {
            if (!Settings.ShouldLog(category, severity))
                return;
            lock (outputLock)
            {
                System.Console.ForegroundColor = Settings.CategoryColor;
                System.Console.Write(category + ": ");
                System.Console.ForegroundColor = Settings.GetSeverityColor(severity);
                System.Console.WriteLine(format, args);
                if (FileLogging())
                    LogLineToFile(category + ": " + string.Format(format, args));
            }
        }
        public static void Log(LogCategory category, LogSeverity severity, string entry)
        {
            if (!Settings.ShouldLog(category, severity))
                return;
            lock (outputLock)
            {
                System.Console.ForegroundColor = Settings.CategoryColor;
                System.Console.Write(category + ": ");
                System.Console.ForegroundColor = Settings.GetSeverityColor(severity);
                System.Console.WriteLine(entry);
                if (FileLogging())
                    LogLineToFile(entry);
            }
        }

        public static void RawLine(object obj)
        {
            lock (outputLock)
            {
                System.Console.WriteLine(obj);
                if (FileLogging())
                    LogLineToFile(obj.ToString());
            }
        }
        public static void RawLine(string format, params object[] args)
        {
            lock (outputLock)
            {
                System.Console.WriteLine(format, args);
                if (FileLogging())
                    LogLineToFile(string.Format(format, args));
            }
        }
        public static void RawLine(string entry)
        {
            lock (outputLock)
            {
                System.Console.WriteLine(entry);
                if (FileLogging())
                    LogLineToFile(entry);
            }
        }
        public static void Raw(object obj)
        {
            lock (outputLock)
            {
                System.Console.Write(obj);
                if (FileLogging())
                    LogToFile(obj.ToString());
            }
        }
        public static void Raw(string format, params object[] args)
        {
            lock (outputLock)
            {
                System.Console.Write(format, args);
                if (FileLogging())
                    LogToFile(string.Format(format, args));
            }
        }
        public static void Raw(string entry)
        {
            lock (outputLock)
            {
                System.Console.Write(entry);
                if (FileLogging())
                    LogToFile(entry);
            }
        }

        private static bool FileLogging()
        {
            if (Settings.LogFile == null)
                return false;
            if (LogFile == null)
                LogFile = new StreamWriter(Settings.LogFile, false, Encoding.UTF8) { AutoFlush = true };
            return true;
        }
        private static void LogLineToFile(string entry)
        {
            LogFile.WriteLine(DateTime.Now.ToString(Settings.FileTimestampFormat) + entry);
        }
        private static void LogToFile(string entry)
        {
            LogFile.Write(entry);
        }
    }
    public class LoggerSettings : Settings
    {
        public ulong EnabledCategories = ulong.MaxValue;
        public LogSeverity MinimumSeverity = LogSeverity.Trace;
        public ConsoleColor CategoryColor = ConsoleColor.Blue;
        public ConsoleColor TraceSeverityColor = ConsoleColor.DarkCyan;
        public ConsoleColor DebugSeverityColor = ConsoleColor.Cyan;
        public ConsoleColor InfoSeverityColor = ConsoleColor.White;
        public ConsoleColor WarningSeverityColor = ConsoleColor.Yellow;
        public ConsoleColor ErrorSeverityColor = ConsoleColor.Red;
        public ConsoleColor FatalSeverityColor = ConsoleColor.DarkRed;
        public string LogFile = "console.log";
        public string FileTimestampFormat = "[MM-dd HH:mm:ss.fff] ";

        public bool ShouldLog(LogCategory category)
        {
            return (EnabledCategories & (ulong)category) != 0;
        }
        public bool ShouldLog(LogCategory category, LogSeverity severity)
        {
            return severity >= MinimumSeverity && (EnabledCategories & (ulong)category) != 0;
        }
        public ConsoleColor GetSeverityColor(LogSeverity severity)
        {
            switch (severity)
            {
                case LogSeverity.Trace:
                    return TraceSeverityColor;
                case LogSeverity.Debug:
                    return DebugSeverityColor;
                case LogSeverity.Info:
                    return InfoSeverityColor;
                case LogSeverity.Warning:
                    return WarningSeverityColor;
                case LogSeverity.Error:
                    return ErrorSeverityColor;
                case LogSeverity.Fatal:
                    return FatalSeverityColor;
                default:
                    throw new ArgumentOutOfRangeException("severity");
            }
        }

        public void SetEnabled(LogCategory category, bool enabled)
        {
            if (enabled)
                EnabledCategories |= (ulong)category;
            else
                EnabledCategories &= ~(ulong)category;
        }
    }
}