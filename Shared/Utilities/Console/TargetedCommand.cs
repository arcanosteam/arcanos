﻿using System;

namespace Arcanos.Utilities.Console
{
    public delegate bool TargetedCommandHandler(ulong target, string[] args);
    public class TargetedCommand : Command
    {
        public static ulong TargetGUID;

        public readonly TargetedCommandHandler TargetedHandler;

        public TargetedCommand(string name, TargetedCommandHandler handler) : base(name, null)
        {
            TargetedHandler = handler;
        }
        public TargetedCommand(string name, string syntaxHelp, TargetedCommandHandler handler) : base(name, syntaxHelp, null)
        {
            TargetedHandler = handler;
        }

        public override bool Execute(string args)
        {
            if (TargetedHandler == null)
                return true;

            string[] splitArgs = SplitArgs(args);
            bool wasNull = false;
            if (TargetGUID == 0)
            {
                if (splitArgs.Length == 0)
                    return ErrorHandler(ShowConsoleOnlyArgument(SyntaxHelp));
                wasNull = true;
                if (!ulong.TryParse(splitArgs[0], out TargetGUID))
                    return ErrorHandler(ShowConsoleOnlyArgument(SyntaxHelp));
                Array.ConstrainedCopy(splitArgs, 1, splitArgs, 0, splitArgs.Length - 1);
            }

            try
            {
                if (TargetedHandler(TargetGUID, splitArgs))
                    return true;
            }
            catch { }
            finally
            {
                if (wasNull)
                    TargetGUID = 0;
            }

            if (TargetGUID == 0)
                return ErrorHandler(ShowConsoleOnlyArgument(SyntaxHelp));
            return ErrorHandler(HideConsoleOnlyArgument(SyntaxHelp));
        }
    }
}