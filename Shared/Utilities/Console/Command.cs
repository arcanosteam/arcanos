﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Arcanos.Utilities.Console
{
    public interface ICommand
    {
        string Name { get; }
        bool Execute(string args);
    }
    public delegate bool CommandHandler(string[] args);
    public delegate void CommandOutputInfoHandler(string format, params object[] args);
    public delegate bool CommandOutputErrorHandler(string format, params object[] args);
    public class Command : ICommand
    {
        public static char Separator;
        public static CommandOutputInfoHandler InfoHandler;
        public static CommandOutputErrorHandler ErrorHandler;

        public string Name { get; private set; }
        public string SyntaxHelp;
        public readonly CommandHandler Handler;

        public Command(string name, CommandHandler handler) : this(name, null, handler) { }
        public Command(string name, string syntaxHelp, CommandHandler handler)
        {
            Name = name;
            SyntaxHelp = syntaxHelp;
            Handler = handler;
        }

        protected string[] SplitArgs(string args)
        {
            List<string> splitArgs = new List<string>();
            foreach (string argPart in Regex.Split(args, "(?<=^[^\"]*(?:\"[^\"]*\"[^\"]*)*)" + Separator + "(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)"))
            {
                string arg = argPart;
                if (arg.Length == 0)
                    continue;
                arg = arg.Trim(' ');
                if (arg.Length == 0)
                    continue;
                if (arg.StartsWith("\"") || arg.EndsWith("\""))
                    arg = arg.Trim('"');
                if (arg.Length == 0)
                    continue;
                splitArgs.Add(arg);
            }
            return splitArgs.ToArray();
        }
        public virtual bool Execute(string args)
        {
            if (Handler == null)
                return true;

            try
            {
                if (Handler(SplitArgs(args)))
                    return true;
            }
            catch { }

            return ErrorHandler(ShowConsoleOnlyArgument(SyntaxHelp));
        }

        protected string ShowConsoleOnlyArgument(string message)
        {
            return message.Replace('{', '<').Replace('}', '>');
        }
        protected string HideConsoleOnlyArgument(string message)
        {
            int from;
            while ((from = message.IndexOf('{')) != -1)
            {
                int to = message.IndexOf('}');
                int count = to - from + 1;
                message = message.Remove(from - 1, count + 1);
            }
            return message;
        }
    }
}