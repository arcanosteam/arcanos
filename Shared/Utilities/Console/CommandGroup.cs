﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Arcanos.Utilities.Console
{
    public class CommandGroup : ICommand, ICollection<ICommand>
    {
        public static char Separator;
        public static CommandOutputInfoHandler InfoHandler;
        public static CommandOutputErrorHandler ErrorHandler;

        public string Name { get; private set; }

        protected List<ICommand> commands = new List<ICommand>();

        public CommandGroup(string name)
        {
            Name = name;
        }

        public bool Execute(string args)
        {
            if (args.Trim() == string.Empty)
            {
                foreach (ICommand command in commands)
                    InfoHandler("   {0}", command.Name);
                return false;
            }

            int cmdSplitterIndex = args.IndexOf(Separator);
            string cmd, nextArgs;
            if (cmdSplitterIndex == -1)
            {
                cmd = args;
                nextArgs = string.Empty;
            }
            else
            {
                cmd = args.Substring(0, cmdSplitterIndex);
                nextArgs = args.Substring(cmdSplitterIndex + 1);
            }

            foreach (ICommand command in commands)
                if (command.Name.ToLowerInvariant().StartsWith(cmd.ToLowerInvariant()))
                {
                    command.Execute(nextArgs);
                    return true;
                }

            return ErrorHandler("Unknown command: \"{0}\"", cmd);
        }

        public IEnumerator<ICommand> GetEnumerator()
        {
            return commands.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        public void Add(ICommand item)
        {
            commands.Add(item);
            commands.Sort((a, b) => String.Compare(a.Name, b.Name, StringComparison.InvariantCultureIgnoreCase));
        }
        public void Clear()
        {
            commands.Clear();
        }
        public bool Contains(ICommand item)
        {
            return commands.Contains(item);
        }
        public void CopyTo(ICommand[] array, int arrayIndex)
        {
            commands.CopyTo(array, arrayIndex);
        }
        public bool Remove(ICommand item)
        {
            return commands.Remove(item);
        }
        public int Count
        {
            get { return commands.Count; }
        }
        public bool IsReadOnly
        {
            get { return false; }
        }
    }
}