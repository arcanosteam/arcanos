﻿using System;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;

namespace Arcanos.Utilities
{
    public class Settings
    {
        public const string LINE_SAVE_FORMAT = "{0}={1}";
        public const string LINE_LOAD_REGEX = ".*?=.*";
        public void Save(Stream stream)
        {
            Type settingsIgnoreAttribute = typeof(SettingsIgnoreAttribute);
            StreamWriter sw = new StreamWriter(stream);
            foreach (FieldInfo field in GetType().GetFields())
                if (field.GetCustomAttributes(settingsIgnoreAttribute, true).Length == 0)
                    sw.Write(LINE_SAVE_FORMAT, field.Name, field.GetValue(this));
        }
        public void Load(Stream stream)
        {
            Type settingsIgnoreAttribute = typeof(SettingsIgnoreAttribute);
            Regex parser = new Regex(LINE_LOAD_REGEX);
            StreamReader sr = new StreamReader(stream);
            string line;
            while ((line = sr.ReadLine()) != null)
            {
                Match match = parser.Match(line);
                if (match.Success)
                {
                    FieldInfo field = GetType().GetField(match.Groups[0].Value);
                    if (field != null)
                        if (field.GetCustomAttributes(settingsIgnoreAttribute, true).Length == 0)
                            field.SetValue(this, field.FieldType.GetMethod("Parse", new[] { typeof(string) }).Invoke(null, new object[] { match.Groups[1].Value }));
                }
            }
        }
    }
    public class SettingsIgnoreAttribute : Attribute { }
    public interface ISettings<T> where T : Settings
    {
        T Settings { get; set; }
    }
}