﻿namespace Arcanos.Shared.World
{
    public class TerrainData
    {
        public struct TerrainPatch
        {
            public int BaseMaterialID;
            public int Layer0MaterialID;
            public int Layer0AlphaID;
            public int Layer1MaterialID;
            public int Layer1AlphaID;
            public int Layer2MaterialID;
            public int Layer2AlphaID;
            public int Layer3MaterialID;
            public int Layer3AlphaID;
            public float Scale;
        }

        public readonly int DimX, DimY;
        public readonly float SizeX, SizeY;

        public readonly TerrainPatch[,] Patches;

        internal TerrainData() { }
        public TerrainData(int dimX, int dimY, float sizeX, float sizeY)
        {
            DimX = dimX;
            DimY = dimY;
            SizeX = sizeX;
            SizeY = sizeY;
            Patches = new TerrainPatch[DimX, DimY];
        }
    }
}