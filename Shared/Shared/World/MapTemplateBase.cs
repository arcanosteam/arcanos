﻿using System.Collections.Generic;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Objects;
using Microsoft.Xna.Framework;

namespace Arcanos.Shared.World
{
    public abstract class MapTemplateBase : Template
    {
        protected const float DEFAULT_GRID_CELL_SIZE = 20f;
        public const float INTERACT_DISTANCE = 2f;

        public override EditorEntry EditorEntry
        {
            get { return new EditorEntry(ID, Name); }
        }

        public Text Name;
        public readonly float SizeX, SizeY, SizeZ;
        public readonly float CellSize;
        public HeightMapData HeightMap;
        public readonly TerrainData Terrain;
        public readonly List<StaticModelObjectBase> Statics = new List<StaticModelObjectBase>();

        protected MapTemplateBase() { }
        protected MapTemplateBase(float sizeX, float sizeY, float sizeZ, int hmapDimX, int hmapDimY, int patchSize) : this(sizeX, sizeY, sizeZ, DEFAULT_GRID_CELL_SIZE, hmapDimX, hmapDimY, patchSize) { }
        protected MapTemplateBase(float sizeX, float sizeY, float sizeZ, float cellSize, int hmapDimX, int hmapDimY, int patchSize) : this()
        {
            SizeX = sizeX;
            SizeY = sizeY;
            SizeZ = sizeZ;
            CellSize = cellSize;
            HeightMap = new HeightMapData(hmapDimX, hmapDimY, SizeX, SizeY, SizeZ);
            Terrain = new TerrainData(hmapDimX / patchSize, hmapDimY / patchSize, SizeX, SizeY);
        }

        public abstract void Apply(MapBase map);

        public float GetHeight(float x, float y)
        {
            return HeightMap.GetHeight((x / SizeX) * HeightMap.DimX, (y / SizeY) * HeightMap.DimY) * SizeZ;
        }
        public Vector3 GetNormal(float x, float y)
        {
            return HeightMap.GetNormal((x / SizeX) * HeightMap.DimX, (y / SizeY) * HeightMap.DimY);
        }
        public Vector3 GetSmoothNormal(float x, float y)
        {
            return HeightMap.GetSmoothNormal((x / SizeX) * HeightMap.DimX, (y / SizeY) * HeightMap.DimY);
        }
    }
}