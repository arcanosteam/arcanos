﻿using System;
using System.Collections.Generic;
using Arcanos.Shared.Grids;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Spells;
using Arcanos.Shared.Timing;
using Microsoft.Xna.Framework;

namespace Arcanos.Shared.World
{
    public abstract class MapBase : GameObject
    {
        public readonly MapTemplateBase Template;

        public readonly Grid Grid;
        public readonly float SizeX, SizeY;
        public readonly float CellSize;
        public ulong UpdateCycle;

        public EnvironmentBase Environment;
        public float EnvironmentTime;

        public readonly List<SpellBase> Spells = new List<SpellBase>();
        private bool updatingSpells;
        private readonly Queue<SpellBase> spellsToAdd = new Queue<SpellBase>();
        private readonly Queue<SpellBase> spellsToRemove = new Queue<SpellBase>();

        protected MapBase(ulong guid, MapTemplateBase template)
        {
            RegisterGUID(guid);
            Template = template;
            SizeX = template.SizeX;
            SizeY = template.SizeY;
            CellSize = template.CellSize;
            Grid = new Grid(this, (int)Math.Ceiling(template.SizeX / template.CellSize), (int)Math.Ceiling(template.SizeY / template.CellSize), template.SizeX, template.SizeY);

            Template.Apply(this);
        }

        public virtual void Update()
        {
            EnvironmentTime += Time.PerSecond;
            Environment.Update(this);

            UpdateCycle++;
            updatingSpells = true;
            foreach (SpellBase spell in Spells)
                spell.Update();
            while (spellsToAdd.Count != 0)
                Spells.Add(spellsToAdd.Dequeue());
            while (spellsToRemove.Count != 0)
            {
                SpellBase spell = spellsToRemove.Dequeue();
                Spells.Remove(spell);
                spell.Destroy();
            }
            updatingSpells = false;
        }

        public virtual void Add(WorldObjectBase wo)
        {
            wo.Map = this;
            Grid.Add(wo);
            wo.OnAddedToMap();
        }
        public virtual void Remove(WorldObjectBase wo)
        {
            wo.OnRemovedFromMap();
            Grid.Remove(wo);
            wo.Map = null;
        }
        public virtual void Add(SpellBase spell)
        {
            if (updatingSpells)
                spellsToAdd.Enqueue(spell);
            else
                Spells.Add(spell);
        }
        public virtual void Remove(SpellBase spell)
        {
            if (updatingSpells)
                spellsToRemove.Enqueue(spell);
            else
            {
                Spells.Remove(spell);
                spell.Destroy();
            }
        }
        public void Add(StaticModelObjectBase smo)
        {
            Grid.Add(smo);
        }

        public virtual void MoveObject(WorldObjectBase wo)
        {
            Grid.MoveObject(wo);
        }

        public IEnumerable<WorldObjectBase> GetNearbyObjects(Vector3 pos, float radius)
        {
            GridBounds bounds = Grid.GetGridBounds(pos, radius);
            for (int x = bounds.MinX; x <= bounds.MaxX; x++)
                for (int y = bounds.MinY; y <= bounds.MaxY; y++)
                {
                    GridCell cell = Grid.GetCell(x, y);
                    int count = cell.Objects.Count;
                    for (int i = 0; i < count; i++)
                        if (cell.Objects[i].IsInWorld && cell.Objects[i].DistanceTo2D(pos) <= radius)
                            yield return cell.Objects[i];
                }
        }
        public WorldObjectBase GetNearestObjectByID(Vector3 pos, int id, float radius)
        {
            float nearestDistance = float.MaxValue;
            WorldObjectBase nearestObject = null;
            foreach (WorldObjectBase wo in GetNearbyObjects(pos, radius))
            {
                if (wo.Template.ID != id)
                    continue;

                float dist = wo.DistanceTo2D(pos);
                if (dist < nearestDistance)
                {
                    nearestDistance = dist;
                    nearestObject = wo;
                }
            }
            return nearestObject;
        }
        public abstract IEnumerable<WorldObjectBase> GetVisibleObjects(Vector3 pos);
    }
}