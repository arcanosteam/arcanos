﻿using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using Arcanos.Shared.Data;
using Microsoft.Xna.Framework;
using Rectangle = System.Drawing.Rectangle;

namespace Arcanos.Shared.World
{
    public class HeightMapData : IPostDeserialization
    {
        public readonly int DimX, DimY;
        public readonly float SizeX, SizeY, SizeZ;
        public readonly float[,] Heights;
        [SerializationIgnore]
        public Vector3[,] Geometry;
        [SerializationIgnore]
        public Vector3[,] Normals;

        internal HeightMapData() { }
        public HeightMapData(int dimX, int dimY, float sizeX, float sizeY, float sizeZ)
        {
            DimX = dimX;
            DimY = dimY;
            SizeX = sizeX;
            SizeY = sizeY;
            SizeZ = sizeZ;
            Heights = new float[DimX, DimY];
            Geometry = new Vector3[DimX, DimY];
            Normals = new Vector3[DimX, DimY];
        }

        public static HeightMapData FromBitmap(Bitmap hmap, float sizeX, float sizeY, float sizeZ)
        {
            int w = hmap.Width,
                h = hmap.Height;
            int step = Image.GetPixelFormatSize(hmap.PixelFormat) / 8;
            BitmapData bitmapData = hmap.LockBits(new Rectangle(0, 0, w, h), ImageLockMode.ReadOnly, hmap.PixelFormat);
            byte[] raw = new byte[w * h * step];
            Marshal.Copy(bitmapData.Scan0, raw, 0, raw.Length);
            hmap.UnlockBits(bitmapData);

            HeightMapData result = new HeightMapData(w, h, sizeX, sizeY, sizeZ);

            int len = w * h * step;
            for (int i = 0; i < len; i += step)
            {
                int j = i / step;
                byte value = raw[i];
                result.Heights[j % w, h - 1 - j / w] = value / 255f;
            }

            result.UpdateGeometry();
            result.UpdateNormals();

            return result;
        }

        public float GetHeight(float x, float y)
        {
            int xx = (int)x;
            int yy = (int)y;

            if (xx < 0)
                xx = 0;
            if (yy < 0)
                yy = 0;
            if (xx >= DimX)
                xx = DimX - 1;
            if (yy >= DimY)
                yy = DimY - 1;

            int xPlusOne = xx + 1;
            int yPlusOne = yy + 1;

            if (xPlusOne >= DimX)
                xPlusOne = xx;
            if (yPlusOne >= DimY)
                yPlusOne = yy;

            float t0 = Heights[xx, yy];
            float t1 = Heights[xPlusOne, yy];
            float t2 = Heights[xx, yPlusOne];
            float t3 = Heights[xPlusOne, yPlusOne];

            float sqX = x - xx;
            float sqY = y - yy;
            return (sqX + sqY) < 1
                ? t0 + (t1 - t0) * sqX + (t2 - t0) * sqY
                : t3 + (t1 - t3) * (1 - sqY) + (t2 - t3) * (1 - sqX);
        }
        public Vector3 GetNormal(float x, float y)
        {
            int xx = (int)x;
            int yy = (int)y;

            if (xx < 0)
                xx = 0;
            if (yy < 0)
                yy = 0;
            if (xx >= DimX)
                xx = DimX - 1;
            if (yy >= DimY)
                yy = DimY - 1;

            int xPlusOne = xx + 1;
            int yPlusOne = yy + 1;

            if (xPlusOne >= DimX)
                xPlusOne = xx;
            if (yPlusOne >= DimY)
                yPlusOne = yy;

            Vector3 t0 = Geometry[xx, yy];
            Vector3 t1 = Geometry[xPlusOne, yy];
            Vector3 t2 = Geometry[xx, yPlusOne];
            Vector3 t3 = Geometry[xPlusOne, yPlusOne];

            float sqX = x - xx;
            float sqY = y - yy;

            return Vector3.Normalize((sqX + sqY) < 1 ? Vector3.Cross(t1 - t0, t2 - t0) : Vector3.Cross(t2 - t3, t1 - t3));
        }
        public Vector3 GetSmoothNormal(float x, float y)
        {
            int xx = (int)x;
            int yy = (int)y;

            if (xx < 0)
                xx = 0;
            if (yy < 0)
                yy = 0;
            if (xx >= DimX)
                xx = DimX - 1;
            if (yy >= DimY)
                yy = DimY - 1;

            int xPlusOne = xx + 1;
            int yPlusOne = yy + 1;

            if (xPlusOne >= DimX)
                xPlusOne = xx;
            if (yPlusOne >= DimY)
                yPlusOne = yy;

            Vector3 t0 = Normals[xx, yy];
            Vector3 t1 = Normals[xPlusOne, yy];
            Vector3 t2 = Normals[xx, yPlusOne];
            Vector3 t3 = Normals[xPlusOne, yPlusOne];

            float sqX = x - xx;
            float sqY = y - yy;

            return (sqX + sqY) < 1
                ? Vector3.Add(t0, Vector3.Add(Vector3.Multiply(Vector3.Subtract(t1, t0), sqX), Vector3.Multiply(Vector3.Subtract(t2, t0), sqY)))
                : Vector3.Add(t3, Vector3.Add(Vector3.Multiply(Vector3.Subtract(t1, t3), 1 - sqY), Vector3.Multiply(Vector3.Subtract(t2, t3), 1 - sqX)));
        }

        private void UpdateGeometry()
        {
            for (int x = 0; x < DimX; x++)
                for (int y = 0; y < DimY; y++)
                    Geometry[x, y] = new Vector3(SizeX * x / DimX, SizeY * y / DimY, SizeZ * Heights[x, y]);
        }
        private void UpdateNormals()
        {
            int dimXMinusOne = DimX - 1;
            int dimYMinusOne = DimY - 1;
            for (int x = 0; x < DimX; x++)
                for (int y = 0; y < DimY; y++)
                {
                    if (x == 0 || y == 0 || x == dimXMinusOne || y == dimYMinusOne)
                        Normals[x, y] = Vector3.UnitZ;
                    else
                    {
                        /*Vector3 normal = new Vector3(Geometry[x - 1, y].Z - Geometry[x + 1, y].Z, Geometry[x, y - 1].Z - Geometry[x, y + 1].Z, 2);
                        float length = (float)Math.Sqrt((normal.X * normal.X) + (normal.Y * normal.Y) + (normal.Z * normal.Z));
                        float num = 1 / length;
                        normal.X *= num;
                        normal.Y *= num;
                        normal.Z *= num;*/
                        Vector3 c = Geometry[x, y];
                        Vector3 cu = Geometry[x, y - 1] - c,
                                cl = Geometry[x - 1, y] - c,
                                cr = Geometry[x + 1, y] - c,
                                cd = Geometry[x, y + 1] - c;
                        Vector3 cur = Vector3.Normalize(Vector3.Cross(cu, cr)),
                                crd = Vector3.Normalize(Vector3.Cross(cr, cd)),
                                cdl = Vector3.Normalize(Vector3.Cross(cd, cl)),
                                clu = Vector3.Normalize(Vector3.Cross(cl, cu));
                        Vector3 n = new Vector3(
                            (cur.X + crd.X + cdl.X + clu.X) / 4,
                            (cur.Y + crd.Y + cdl.Y + clu.Y) / 4,
                            (cur.Z + crd.Z + cdl.Z + clu.Z) / 4);

                        Normals[x, y] = Vector3.Normalize(n);
                    }
                }
        }

        public void MemberDeserialized(string member, object value) { }
        public void Deserialized(object parent)
        {
            Geometry = new Vector3[DimX, DimY];
            Normals = new Vector3[DimX, DimY];
            UpdateGeometry();
            UpdateNormals();
        }
    }
}