using Arcanos.Shared.Data;
using Microsoft.Xna.Framework;

namespace Arcanos.Shared.World
{
    public abstract class EnvironmentBase : Template
    {
        public override EditorEntry EditorEntry
        {
            get { return new EditorEntry(ID, string.Format("Sky: {0} Ambient: {1} Wind: {2}", FogColor, AmbientLight, Wind)); }
        }

        public int SkyboxMaterial;
        public Vector3 AmbientLight;

        public float FogStart;
        public float FogEnd;
        public Vector4 FogColor;

        public Vector3 Wind;
        [SerializationIgnore]
        public float WindDistribution
        {
            get { return Wind.Z; }
            set { Wind.Z = value; }
        }

        public float ChangeDuration;

        public abstract void Update(MapBase map);

        protected virtual void Clone(EnvironmentBase source)
        {
            SkyboxMaterial = source.SkyboxMaterial;
            AmbientLight = source.AmbientLight;

            FogStart = source.FogStart;
            FogEnd = source.FogEnd;
            FogColor = source.FogColor;

            Wind = source.Wind;
            ChangeDuration = source.ChangeDuration;
        }
        protected virtual void Clone(EnvironmentBase from, EnvironmentBase to, float progress)
        {
            bool first = progress <= 0.5;

            SkyboxMaterial = first ? from.SkyboxMaterial : to.SkyboxMaterial;
            Vector3.Lerp(ref from.AmbientLight, ref to.AmbientLight, progress, out AmbientLight);
            Extensions.Lerp(from.FogStart, to.FogStart, progress, out FogStart);
            Extensions.Lerp(from.FogEnd, to.FogEnd, progress, out FogEnd);
            Vector4.Lerp(ref from.FogColor, ref to.FogColor, progress, out FogColor);

            Vector3.Lerp(ref from.Wind, ref to.Wind, progress, out Wind);

            ChangeDuration = first ? from.ChangeDuration : to.ChangeDuration;
        }
    }
}