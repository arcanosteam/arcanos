﻿namespace Arcanos.Shared.Server
{
    public static class WorldConstants
    {
        public const int PORT = 52556;
        public const int PROTOCOL_VERSION = 1;
    }
    public enum WorldLoginResult : byte
    {
        Ok,
        TemporarilyUnavailable,
        NotLoggedIn,
        AccountNotFound,
    }
    public enum WorldEnterResult : byte
    {
        Ok,
        WorldFull,
        InvalidCharacter,
        CharacterBanned,
        InvalidLocation,
    }
}
