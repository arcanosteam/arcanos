﻿namespace Arcanos.Shared.Server
{
    public abstract class WorldServerEntryBase : Template
    {
        public override EditorEntry EditorEntry
        {
            get { return new EditorEntry(ID, string.Format("{0} ({1})", Name, URL)); }
        }

        public string Name;
        public string URL;

        protected WorldServerEntryBase() { }
        protected WorldServerEntryBase(string name, string url)
        {
            Name = name;
            URL = url;
        }
    }
}