﻿namespace Arcanos.Shared.Server
{
    public static class LoginConstants
    {
        public const int PORT = 52555;
        public const int PROTOCOL_VERSION = 1;
    }
    public enum LoginResult : byte
    {
        Ok,
        TemporarilyUnavailable,
        UsernameNotFound,
        IncorrectPassword,
        AccountBanned,
    }
}
