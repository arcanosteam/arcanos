﻿namespace Arcanos.Shared.Chat
{
    public struct ChatMessage
    {
        public long Timestamp;
        public string Message;
        public ChatMessageFlags Flags;
    }
}