﻿using System;

namespace Arcanos.Shared.Chat
{
    [Flags]
    public enum ChatMessageFlags : byte
    {
        None = 0,

        WhisperFromYou = 0x01,
    }
    public enum ChatMessageResult
    {
        Ok,
        NoListeners,

        UnknownSender,
        UnknownTarget,
        UnknownChannel,
        GloballyBanned,
        BannedInChannel,
        GloballyMuted,
        MutedInChannel,
        WrongMap,
    }
    public enum ChatChannelJoinResult
    {
        Ok,

        NotFound,
        Banned,
        AlreadyJoined,
    }
    public enum ChatChannelLeaveResult
    {
        Ok,
        
        NotFound,
        NotJoined,
    }
}