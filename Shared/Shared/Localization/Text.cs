﻿using System;
using Arcanos.Shared.Data;
using Arcanos.Shared.Objects;

namespace Arcanos.Shared.Localization
{
    [SerializationSettings(Method = SerializationMethod.RefByID, ReferenceResolverType = typeof(Text), ReferenceResolverMethod = "ReferenceResolver")]
    public class Text : IReferencedByID, IEditableEntry
    {
        private static readonly int localeLength;
        public static Func<int, Text> LoadHandler;
        public static Func<Text, int> MakeHandler;
        public static string MissingString = "<???>";

        [SerializationIgnore]
        public int ID { get; set; }
        [SerializationIgnore]
        public EditorEntry EditorEntry
        {
            get { return new EditorEntry(ID, LocalizedStrings[0]); }
        }

        public readonly string[] LocalizedStrings = new string[localeLength];

        static Text()
        {
            localeLength = Enum.GetValues(typeof(LocaleIndex)).Length;
        }
        
        internal Text() { }

        public static Text Make(string str)
        {
            if (string.IsNullOrEmpty(str))
                str = " ";
            Text data = new Text();
            for (byte locale = 0; locale < localeLength; ++locale)
                data.LocalizedStrings[locale] = str;
            data.ID = MakeHandler(data);
            return data;
        }
        public static Text Make(LocaleIndex locale, string str)
        {
            if (string.IsNullOrEmpty(str))
                str = " ";
            Text data = new Text();
            data.LocalizedStrings[(byte)locale] = str;
            data.ID = MakeHandler(data);
            return data;
        }
        public static Text Make(params string[] str)
        {
            for (int i = 0; i < str.Length; i++)
                if (string.IsNullOrEmpty(str[i]))
                    str[i] = " ";
            Text data = new Text();
            str.CopyTo(data.LocalizedStrings, 0);
            data.ID = MakeHandler(data);
            return data;
        }

        public override string ToString()
        {
            return LocalizedStrings[(byte)Localization.CurrentLocale];
        }

        internal static object ReferenceResolver(int id)
        {
            Text data = LoadHandler(id);
            data.ID = id;
            return data;
        }
    }
}