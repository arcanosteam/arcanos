﻿using Arcanos.Shared.Items;
using Arcanos.Shared.Players;

namespace Arcanos.Shared.Localization
{
    public class GameStrings
    {
        public Text[] RaceNames = new Text[(byte)PlayerRace.Max];
        public Text[] ClassNames = new Text[(byte)PlayerClass.Max];
        public Text[] StatNames = new Text[(byte)Stat.Max];
        public Text[] ItemSlotNames = new Text[(byte)ItemSlot.Max];
        public Text[] PowerTypeNames = new Text[(byte)PowerType.Max];

        public Text MenuCreateCharacterModelPrev;
        public Text MenuCreateCharacterModelNext;
        public Text MenuCreateCharacterAnimStand;
        public Text MenuCreateCharacterAnimWalk;
        public Text MenuCreateCharacterAnimAttack;
        public Text MenuCreateCharacterName;
        public Text MenuCreateCharacterRace;
        public Text MenuCreateCharacterClass;
        public Text MenuCreateCharacterSpells;
        public Text MenuCreateCharacterCreate;
        public Text[] MenuCreateCharacterResult = new Text[(byte)CharacterCreateResult.Max];

        public Text HealthFormat;
        public Text PowerFormat;
        public Text AuraDurationMinutesFormat;
        public Text AuraDurationTenSecsFormat;
        public Text AuraDurationSecondsFormat;
        public Text CastingFormat;

        public Text AuraCasterFormat;

        public Text CharacterWindowTabTitleEquipment;
        public Text CharacterWindowTabTitleStats;
        public Text CharacterWindowTabTitleSpells;
        public Text CharacterWindowStatFormat;
        public Text CharacterWindowWeaponDamageFormat;
        public Text CharacterWindowWeaponInvervalFormat;

        public Text LootWindowTitle;
        public Text LootMoneyFormat;

        public Text DialogAcceptQuestOption;
        public Text DialogDeclineQuestOption;
        public Text DialogCompleteQuestOption;

        public Text GroupInviteRequestFormat;
        public Text GroupInviteAccept;
        public Text GroupInviteDecline;

        public Text DuelRequestFormat;
        public Text DuelAccept;
        public Text DuelDecline;

        public Text ChatAliasWhisperTo;
        public Text ChatAliasWhisperFrom;
        public Text ChatTimestampFormat;

        public Text LogMoneyAdded;
        public Text LogMoneyLost;
        public Text LogItemsAddedSingular;
        public Text LogItemsAddedPlural;
        public Text LogItemsLostSingular;
        public Text LogItemsLostPlural;
        public Text LogChannelJoined;
        public Text LogChannelLeft;
        public Text LogPartyMemberJoined;
        public Text LogPartyMemberLeft;
        public Text LogPartyMemberLeader;
        public Text LogDuelStatusStarted;
        public Text LogDuelStatusFinished;
        public Text LogDuelParticipantStatusWin;
        public Text LogDuelParticipantStatusLose;

        public Text TooltipItemNameSingularFormat;
        public Text TooltipItemNamePluralFormat;
        public Text TooltipItemSlotFormat;
        public Text TooltipItemRequiredLevelBothFormat;
        public Text TooltipItemRequiredLevelMinFormat;
        public Text TooltipItemRequiredLevelMaxFormat;
        public Text TooltipItemWeaponDamageFormat;
        public Text TooltipItemWeaponSpeedFormat;
        public Text TooltipItemArmorFormat;
        public Text TooltipItemStatModFormat;
        public Text[] TooltipItemSpellTriggerFormats = new Text[(byte)ItemSpellUsage.Max];
        public Text TooltipItemDescriptionFormat;
        public Text TooltipItemLoreFormat;
        public Text TooltipItemPriceless;
        public Text TooltipItemPriceFormat;

        public Text TooltipSpellRequiredPowerFormat;
        public Text TooltipSpellCooldownFormat;

        public Text TooltipExperienceLevelFormat;
        public Text TooltipExperienceBarFormat;

        public Text[] ErrorWorldLoginResult;
        public Text[] ErrorWorldEnterResult;
        public Text[] ErrorCharacterCreateResult;
        public Text[] ErrorGroupInviteResult;
        public Text[] ErrorGroupKickResult;
        public Text[] ErrorChatMessageResult;
        public Text[] ErrorChatChannelJoinResult;
        public Text[] ErrorChatChannelLeaveResult;
        public Text[] ErrorItemTakeResult;
        public Text[] ErrorItemStoreResult;
        public Text[] ErrorItemMoveResult;
        public Text[] ErrorItemDivideResult;
        public Text[] ErrorItemDestroyResult;
        public Text[] ErrorItemUseResult;
        public Text[] ErrorItemEquipResult;
        public Text[] ErrorItemUnequipResult;
        public Text[] ErrorLootingResult;
        public Text[] ErrorQuestAcceptResult;
        public Text[] ErrorQuestRemoveResult;
        public Text[] ErrorQuestCompleteResult;
        public Text[] ErrorDialogStartResult;
        public Text[] ErrorDialogSelectOptionResult;
        public Text[] ErrorCastSpellResult;
        public Text ErrorUnexistingChatChannel;
    }
}