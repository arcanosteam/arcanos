﻿using System;
using System.Globalization;
using System.Threading;

namespace Arcanos.Shared.Localization
{
    public static class Localization
    {
        private static readonly CultureInfo[] cachedCultures; 

        public static LocaleIndex CurrentLocale;
        public static CultureInfo CurrentCulture
        {
            get
            {
                CultureInfo culture = cachedCultures[(byte)CurrentLocale];
                if (culture != null)
                    return culture;
                culture = new CultureInfo(LocaleToCulture(CurrentLocale));
                cachedCultures[(byte)CurrentLocale] = culture;
                return culture;
            }
            set
            {
                try
                {
                    CurrentLocale = CultureToLocale(CultureInfo.CreateSpecificCulture(value.Name).Name);
                    Thread.CurrentThread.CurrentCulture = CurrentCulture;
                }
                catch
                {
                    CurrentLocale = LocaleIndex.EnUs;
                }
            }
        }

        public static GameStrings Game;

        static Localization()
        {
            cachedCultures = new CultureInfo[Enum.GetValues(typeof(LocaleIndex)).Length];
        }

        public static string LocaleToCulture(LocaleIndex locale)
        {
            switch (locale)
            {
                case LocaleIndex.EnUs:
                    return "en-US";
                case LocaleIndex.RuRu:
                    return "ru-RU";
                default:
                    throw new ArgumentOutOfRangeException("locale");
            }
        }
        public static LocaleIndex CultureToLocale(string culture)
        {
            switch (culture.ToLower())
            {
                case "en-us":
                case "en-gb":
                    return LocaleIndex.EnUs;
                case "ru-ru":
                    return LocaleIndex.RuRu;
                default:
                    throw new ArgumentOutOfRangeException("culture");
            }
        }
    }
}