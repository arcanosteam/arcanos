﻿namespace Arcanos.Shared.Localization
{
    public enum LocaleIndex : byte
    {
        EnUs,
        RuRu,
    }
}