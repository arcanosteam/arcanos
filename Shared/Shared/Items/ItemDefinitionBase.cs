﻿using System;
using Arcanos.Shared.Data;

namespace Arcanos.Shared.Items
{
    public abstract class ItemDefinitionBase : IManualSerialization, IManualDeserialization
    {
        [SerializationSettings(Method = SerializationMethod.Manual)]
        public virtual ItemTemplateBase Template { get; set; }
        [SerializationSettings(Method = SerializationMethod.Manual)]
        public int Count;

        protected ItemDefinitionBase() { }
        protected ItemDefinitionBase(ItemTemplateBase template, int count)
        {
            Template = template;
            Count = count;
        }

        protected abstract ItemTemplateBase GetTemplateForID(int id);

        public byte[] Serialize(string member)
        {
            switch (member)
            {
                case "Template":
                    return BitConverter.GetBytes(Template.ID);
                case "Count":
                    return BitConverter.GetBytes(Count);
            }
            return new byte[0];
        }
        public void Deserialize(string member, byte[] data)
        {
            switch (member)
            {
                case "Template":
                    Template = GetTemplateForID(BitConverter.ToInt32(data, 0));
                    break;
                case "Count":
                    Count = BitConverter.ToInt32(data, 0);
                    break;
            }
        }
    }
}