﻿using Arcanos.Shared.Data;
using Arcanos.Shared.Players;

namespace Arcanos.Shared.Items
{
    public abstract class InventoryBase
    {
        public const int BAG_LIMIT = 5;

        [SerializationIgnore]
        public CharacterBase Owner;
        public readonly BagBase[] Bags = new BagBase[BAG_LIMIT];

        protected InventoryBase() { }
        protected InventoryBase(CharacterBase owner)
        {
            Owner = owner;
        }

        public sbyte IndexOf(BagBase bag)
        {
            for (sbyte i = 0; i < BAG_LIMIT; ++i)
                if (Bags[i] == bag)
                    return i;
            return -1;
        }
        public bool HasItem(ItemBase item)
        {
            for (int i = 0; i < BAG_LIMIT; ++i)
            {
                BagBase bag = Bags[i];
                if (bag == null)
                    continue;

                if (bag.HasItem(item))
                    return true;
            }
            return false;
        }
        public int GetItemCount(ItemTemplateBase item)
        {
            int count = 0;
            for (int i = 0; i < BAG_LIMIT; ++i)
            {
                BagBase bag = Bags[i];
                if (bag == null)
                    continue;

                count += bag.GetItemCount(item);
            }
            return count;
        }
        public bool FindEmptySlot(out byte bag, out byte slot)
        {
            for (bag = 0; bag < BAG_LIMIT; bag++)
            {
                if (Bags[bag] == null)
                    continue;
                for (slot = 0; slot < Bags[bag].Size; slot++)
                {
                    if (Bags[bag].Items[slot] == null)
                        return true;
                }
            }
            bag = 0;
            slot = 0;
            return false;
        }
    }
}