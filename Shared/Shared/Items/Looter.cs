﻿using Arcanos.Shared.Objects;
using Arcanos.Shared.Players;

namespace Arcanos.Shared.Items
{
    public struct Looter
    {
        public readonly int LootingCharacter;
        public readonly int LootingGroup;

        public Looter(PlayerCreatureBase looter)
        {
            LootingCharacter = looter.Character.ID;
            LootingGroup = 0;
        }
        public Looter(GroupBase looters)
        {
            LootingGroup = looters.ID;
            LootingCharacter = looters.Leader.ID;
        }
    }
}