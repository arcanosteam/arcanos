﻿using Arcanos.Shared.Data;
using Arcanos.Utilities.Debug;

namespace Arcanos.Shared.Items
{
    public abstract class BagBase
    {
        [SerializationIgnore]
        public InventoryBase Inventory;
        public readonly ItemBase BagItem;
        [SerializationSettings(Method = SerializationMethod.Manual)]
        public ItemBase[] Items;
        public readonly byte Size;

        protected BagBase() { }
        protected BagBase(InventoryBase inventory, ItemBase bagItem, byte size)
        {
            Inventory = inventory;
            BagItem = bagItem;
            Items = new ItemBase[size];
            Size = size;
        }

        public bool CanStoreItem(ItemBase item)
        {
            for (byte i = 0; i < Size; ++i)
                if (Items[i] == null)
                    return true;
            return false;
        }
        public bool CanStoreItem(ItemTemplateBase item, int count, out int slot, out int remainder)
        {
            bool first = true;
            int originalCount = count;
            slot = -1;
            remainder = count;
            for (byte i = 0; i < Size; ++i)
            {
                ItemBase storedItem = Items[i];
                if (storedItem == null)
                {
                    int toAdd = item.Stack > count ? count : item.Stack;
                    count -= toAdd;
                    remainder -= toAdd;
                    if (count == 0)
                    {
                        if (first)
                            slot = i;
                        return true;
                    }
                }
                else if (storedItem.Template == item && !storedItem.StackFull)
                {
                    int toAdd = storedItem.StackRemainder > count ? count : storedItem.StackRemainder;
                    count -= toAdd;
                    remainder -= toAdd;
                }
                first = false;
                if (count <= 0)
                    return true;
            }
            return originalCount != count;
        }
        public bool HasItem(ItemBase item)
        {
            for (byte i = 0; i < Size; ++i)
                if (Items[i] == item)
                    return true;
            return false;
        }
        public bool HasItem(ItemTemplateBase item)
        {
            for (byte i = 0; i < Size; ++i)
            {
                ItemBase storedItem = Items[i];
                if (storedItem != null && storedItem.Template == item)
                    return true;
            }
            return false;
        }
        public bool HasItem(ItemTemplateBase item, int count)
        {
            return GetItemCount(item) >= count;
        }
        public sbyte SlotOf(ItemBase item)
        {
            for (sbyte i = 0; i < Size; ++i)
                if (Items[i] == item)
                    return i;
            return -1;
        }
        public int GetItemCount(ItemTemplateBase item)
        {
            int count = 0;
            for (byte i = 0; i < Size; ++i)
            {
                ItemBase storedItem = Items[i];
                if (storedItem != null && storedItem.Template == item)
                    count += storedItem.Count;
            }
            return count;
        }

        public virtual bool AddItem(byte slot, ItemBase item)
        {
            D.Assert(slot < Size, "Slot out of bounds");
            D.Assert(item != null, "Item is null");

            if (Items[slot] != null)
                return false;

            Items[slot] = item;
            item.Bag = this;
            return true;
        }
        public virtual bool RemoveItem(byte slot)
        {
            D.Assert(slot < Size, "Slot out of bounds");

            if (Items[slot] == null)
                return false;

            Items[slot].Bag = null;
            Items[slot] = null;
            return true;
        }
    }
}