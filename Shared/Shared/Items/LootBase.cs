﻿using Arcanos.Shared.Objects;

namespace Arcanos.Shared.Items
{
    public abstract class LootBase
    {
        public readonly WorldObjectBase Source;
        public Looter Looter;

        protected LootBase(WorldObjectBase source, Looter looter)
        {
            Source = source;
            Looter = looter;
        }
    }
}