﻿using System;
using Arcanos.Shared.Data;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Players;

namespace Arcanos.Shared.Items
{
    public abstract class ItemBase : Template, IReferencedByGUID
    {
        public override EditorEntry EditorEntry
        {
            get { return new EditorEntry(ID, Template.Name); }
        }

        [SerializationSettings(Method = SerializationMethod.Manual)]
        protected ItemTemplateBase template;
        public ItemTemplateBase Template
        {
            get { return template; }
        }

        [SerializationIgnore]
        public ulong GUID
        {
            get { return (ulong)ID; }
            set { ID = (int)value; }
        }
        
        [SerializationIgnore]
        public BagBase Bag;
        public InventoryBase Inventory
        {
            get { return Bag.Inventory; }
        }
        public CharacterBase Owner
        {
            get { return Bag.Inventory.Owner; }
        }
        public PlayerCreatureBase OwnerCreature
        {
            get { return Bag.Inventory.Owner.Creature; }
        }

        public int Count = 1;
        public bool StackFull
        {
            get { return Count >= template.Stack; }
        }
        public int StackRemainder
        {
            get { return template.Stack - Count; }
        }

        public bool IsEquipped
        {
            get
            {
                return Inventory.IndexOf(Bag) == -1;
            }
        }

        protected ItemBase() { }
        protected ItemBase(ulong guid, ItemTemplateBase template, int count)
        {
            ID = (int)guid;
            //RegisterGUID(guid);
            this.template = template;
            Count = count;
        }

        public virtual void SetCount(int count)
        {
            Count = count;
        }

        public abstract void Destroy();

        public static bool CanEquipInSlot(ItemBase item, EquipSlot slot)
        {
            return CanEquipInSlot(item.Template.Slot, slot);
        }
        public static bool CanEquipInSlot(ItemTemplateBase item, EquipSlot slot)
        {
            return CanEquipInSlot(item.Slot, slot);
        }
        public static bool CanEquipInSlot(ItemSlot item, EquipSlot slot)
        {
            switch (slot)
            {
                case EquipSlot.Head:
                    return item == ItemSlot.Head;
                case EquipSlot.Neck:
                    return item == ItemSlot.Neck;
                case EquipSlot.Back:
                    return item == ItemSlot.Back;
                case EquipSlot.Shoulder:
                    return item == ItemSlot.Shoulder;
                case EquipSlot.Hands:
                    return item == ItemSlot.Hands;
                case EquipSlot.Wrists:
                    return item == ItemSlot.Wrists;
                case EquipSlot.Chest:
                    return item == ItemSlot.Chest;
                case EquipSlot.Waist:
                    return item == ItemSlot.Waist;
                case EquipSlot.Legs:
                    return item == ItemSlot.Legs;
                case EquipSlot.Feet:
                    return item == ItemSlot.Feet;
                case EquipSlot.MainHand:
                    return item == ItemSlot.MainHand || item == ItemSlot.OneHand || item == ItemSlot.TwoHand;
                case EquipSlot.OffHand:
                    return item == ItemSlot.OffHand || item == ItemSlot.OneHand;
                case EquipSlot.Finger1:
                case EquipSlot.Finger2:
                case EquipSlot.Finger3:
                case EquipSlot.Finger4:
                    return item == ItemSlot.Accessory;
                default:
                    throw new ArgumentOutOfRangeException("slot");
            }
        }
    }
}