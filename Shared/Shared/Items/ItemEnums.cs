﻿using System;

namespace Arcanos.Shared.Items
{
    public enum ItemType : byte
    {
        General,
        Equipment,
        Weapon,
        Quest,
        Book,
    }
    public enum ItemSlot : byte
    {
        None,

        Head,
        Neck,
        Back,
        Shoulder,
        Hands,
        Wrists,
        Chest,
        Waist,
        Legs,
        Feet,

        Accessory,

        OneHand,
        TwoHand,
        MainHand,
        OffHand,

        Max,
    }
    public enum ItemQuality : byte
    {
        Junk,
        Common,
        Uncommon,
        Rare,
        Unique,
        Artifact,

        Max,
    }
    [Flags]
    public enum ItemFlags : uint
    {
        Consumable          = 0x0001,
        Unique              = 0x0002,
        SoulboundOnPickup   = 0x0004,
        SoulboundOnEquip    = 0x0008,
        SoulboundOnUse      = 0x0010,
        Indestructible      = 0x0020,
    }
    public enum ItemSpellUsage : byte
    {
        OnUse,
        OnEquip,
        OnUnequip,
        OnPickup,
        OnLose,

        Max,
    }
    public enum ItemTakeResult : byte
    {
        Ok,

        NoSpace,
        AlreadyHavingUnique,
        MissingSource,
        UnknownItem,
        AlreadyLooted,
    }
    public enum ItemStoreResult : byte
    {
        Ok,

        WrongBagType,
    }
    public enum ItemMoveResult : byte
    {
        Ok,

        WrongBagType,
        CantSwapItems,
        InvalidBagIndex,
        BagDoesNotExist,
        InvalidSlot,
        ItemDoesNotExist,
        ItemIsNotInInventory,
    }
    public enum ItemDivideResult : byte
    {
        Ok,

        WrongBagType,
        DestinationNotEmpty,
        InvalidBagIndex,
        BagDoesNotExist,
        InvalidSlot,
        ItemDoesNotExist,
        ItemIsNotInInventory,
        WrongStackSize,
    }
    public enum ItemDestroyResult : byte
    {
        Ok,

        ItemNotFound,
        NotOwner,
        Indestructible,
    }
    public enum ItemUseResult : byte
    {
        Ok,

        ItemNotFound,
        NotOwner,
        NoOnUseTrigger,
        TargetNotFound,
        SpellCastError,
        LevelTooLow,
        LevelTooHigh,
    }
    public enum ItemEquipResult : byte
    {
        Ok,

        ItemNotFound,
        NotOwner,
        AlreadyEquipped,
        NoCompatibleSlots,
        IncompatibleSlot,
        LevelTooLow,
        LevelTooHigh,
        UsingTwoHanded,
    }
    public enum ItemUnequipResult : byte
    {
        Ok,

        ItemNotFound,
        NotEquipped,
        SlotOccupied,
        WrongBagType,
    }
    public enum LootType : byte
    {
        None,
        Item,
        Money,
    }
    public enum LootingResult : byte
    {
        Ok,

        CreatureNotFound,
        CreatureNotDead,
        AlreadyLooted,
        AlreadyLooting,
        AlreadyBeingLooted,
        NotLootOwner,
        OutOfRange,
    }
    public enum EquipSlot : byte
    {
        Head,
        Neck,
        Back,
        Shoulder,
        Hands,
        Wrists,
        Chest,
        Waist,
        Legs,
        Feet,

        MainHand,
        OffHand,

        Finger1,
        Finger2,
        Finger3,
        Finger4,

        Max,
    }
}