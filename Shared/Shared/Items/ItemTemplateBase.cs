﻿using System.Collections.Generic;
using Arcanos.Shared.Data;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Players;
using Arcanos.Shared.Spells;

namespace Arcanos.Shared.Items
{
    public abstract class ItemTemplateBase : Template
    {
        public override EditorEntry EditorEntry
        {
            get { return new EditorEntry(ID, Name); }
        }

        public Text Name;
        public int Icon;
        public Text Description;
        public Text Lore;

        public ItemType Type;
        public ItemSlot Slot;
        public ItemQuality Quality;
        public ItemFlags Flags;
        public int Stack = 1;
        public bool Stacks
        {
            get { return Stack > 1; }
        }
        public int BuyPrice;
        public int SellPrice;

        public bool HasSpellTriggers
        {
            get
            {
                for (byte i = 0; i < (byte)ItemSpellUsage.Max; i++)
                    if (SpellTriggers[i] != null)
                        return true;
                return false;
            }
        }
        public bool IsUsable
        {
            get
            {
                return SpellTriggers[(byte)ItemSpellUsage.OnUse] != null;
            }
        }
        public bool IsEquippable
        {
            get
            {
                switch (Type)
                {
                    case ItemType.Equipment:
                    case ItemType.Weapon:
                        return true;
                    default:
                        return false;
                }
            }
        }

        public IntBounds RequiredLevel;

        // Equipment
        public Dictionary<Stat, int> Mods = new Dictionary<Stat, int>();

        // Weapons
        public IntBounds WeaponDamage;
        public FloatBounds WeaponSpeed;

        // Spells
        [SerializationSettings(Method = SerializationMethod.Manual)]
        public SpellTemplateBase[] SpellTriggers = new SpellTemplateBase[(byte)ItemSpellUsage.Max];

        public bool HasFlag(ItemFlags flag)
        {
            return (Flags & flag) == flag;
        }

        public abstract ItemBase Create(int count);
    }
}