﻿using Arcanos.Shared.Data;

namespace Arcanos.Shared.Players
{
    public abstract class StatsBase : IPostDeserialization
    {
        [SerializationIgnore]
        protected CharacterBase Owner;

        public readonly int[] BaseValues = new int[(byte)Stat.Max];
        public readonly int[] Modifiers = new int[(byte)Stat.Max];
        [SerializationIgnore]
        public readonly int[] ModifiedValues = new int[(byte)Stat.Max];

        public int this[Stat stat]
        {
            get { return ModifiedValues[(byte)stat]; }
        }

        protected StatsBase(CharacterBase owner)
        {
            Owner = owner;
        }

        public virtual void SetBase(Stat stat, int value)
        {
            BaseValues[(byte)stat] = value;
            ModifiedValues[(byte)stat] = value + Modifiers[(byte)stat];
        }
        public virtual void SetModified(Stat stat, int value)
        {
            Modifiers[(byte)stat] = value - BaseValues[(byte)stat];
            ModifiedValues[(byte)stat] = value;
        }
        public virtual void Modify(Stat stat, int mod)
        {
            Modifiers[(byte)stat] += mod;
            ModifiedValues[(byte)stat] += mod;
        }

        public void RecalcModifiers()
        {
            for (byte i = 0; i < (byte)Stat.Max; ++i)
                Modifiers[i] = ModifiedValues[i] - BaseValues[i];
        }
        public void RecalcModifiedValues()
        {
            for (byte i = 0; i < (byte)Stat.Max; ++i)
                ModifiedValues[i] = BaseValues[i] + Modifiers[i];
        }

        public void MemberDeserialized(string member, object value) { }
        public void Deserialized(object parent)
        {
            RecalcModifiedValues();
        }
    }
}