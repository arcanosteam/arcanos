﻿namespace Arcanos.Shared.Players
{
    public enum Stat : byte
    {
        MeleePower,
        MagicPower,
        Swiftness,
        Stamina,
        Intellect,
        Spirit,

        PhysicalResistance,
        MagicResistance,

        FireResistance,
        WaterResistance,
        GroundResistance,
        AirResistance,
        LightResistance,
        DarkResistance,

        Armor,

        Max,
    }
}