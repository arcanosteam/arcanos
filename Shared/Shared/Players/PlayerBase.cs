﻿using System.Collections.Generic;
using Arcanos.Shared.Data;

namespace Arcanos.Shared.Players
{
    /// <summary>
    /// Represents an instance of player's account.
    /// </summary>
    public abstract class PlayerBase : Template
    {
        public override EditorEntry EditorEntry
        {
            get { return new EditorEntry(ID, Name); }
        }

        /// <summary>
        /// Represents an account name.
        /// </summary>
        public readonly string Name;

        /// <summary>
        /// Represents a character that is currently being played from this account.
        /// </summary>
        [SerializationIgnore]
        protected CharacterBase CurrentCharacter { get; set; }

        /// <summary>
        /// Represents a collection of all characters on this account.
        /// </summary>
        [SerializationIgnore]
        public virtual List<CharacterBase> Characters { get; set; }

        protected PlayerBase()
        {
            Characters = new List<CharacterBase>();
        }
        protected PlayerBase(string name) : this()
        {
            Name = name;
        }
    }
}