﻿namespace Arcanos.Shared.Players
{
    public enum CharacterCreateResult
    {
        Ok,

        AccountBanned,
        CharacterCreationUnavailable,
        NameUnavailable,
        NameInappropriate,
        RaceUnavailable,
        ClassUnavailable,
        ModelIndexOutOfBounds,

        Max,
    }
}