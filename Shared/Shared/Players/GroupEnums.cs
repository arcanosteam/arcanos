﻿namespace Arcanos.Shared.Players
{
    public enum GroupInviteResult
    {
        Ok,

        PlayerNotFound,
        PlayerIsInGroup,
        NoSpaceInGroup,
        CannotInviteSelf,
    }
    public enum GroupKickResult
    {
        Ok,

        PlayerNotFound,
        PlayerNotInYourGroup,
        NotLeader,
    }
    public enum GroupInvitationResponse
    {
        Accept,
        Decline,
        TimedOut,
    }
}