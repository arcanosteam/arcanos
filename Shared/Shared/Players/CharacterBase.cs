﻿using Arcanos.Shared.Data;
using Arcanos.Shared.Objects;
using Microsoft.Xna.Framework;

namespace Arcanos.Shared.Players
{
    public abstract class CharacterBase : Template
    {
        public override EditorEntry EditorEntry
        {
            get { return new EditorEntry(ID, Name); }
        }

        [SerializationIgnore]
        public PlayerBase Player;
        [SerializationIgnore]
        public PlayerCreatureBase Creature;

        public string Name;

        public PlayerRace Race;
        public PlayerClass Class;
        public byte ModelIndex;
        public int Level = 1;

        public ulong LocationMapGUID;
        public Vector3 LocationPosition;
        public Rotation LocationRotation;

        protected CharacterBase() { }
        protected CharacterBase(PlayerBase player, string name, PlayerRace playerRace, PlayerClass playerClass, byte modelIndex)
        {
            Player = player;
            Name = name;
            Race = playerRace;
            Class = playerClass;
            ModelIndex = modelIndex;
        }
    }
}