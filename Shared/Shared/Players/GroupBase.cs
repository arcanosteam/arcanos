﻿using System.Collections.Generic;
using Arcanos.Shared.Data;

namespace Arcanos.Shared.Players
{
    public abstract class GroupBase : Template, IManualSerialization, IManualDeserialization
    {
        public override EditorEntry EditorEntry
        {
            get { return new EditorEntry(ID, Leader.Name); }
        }

        public const byte MAX_GROUP_MEMBERS = 5;

        [SerializationSettings(Method = SerializationMethod.Manual)]
        public CharacterBase Leader;
        [SerializationSettings(Method = SerializationMethod.Manual)]
        public CharacterBase[] Members = new CharacterBase[MAX_GROUP_MEMBERS];
        public byte MembersCount;

        public int FindAvailableIndex()
        {
            for (byte i = 0; i < MAX_GROUP_MEMBERS; ++i)
                if (Members[i] == null)
                    return i;
            return -1;
        }
        public int IndexOf(CharacterBase member)
        {
            for (byte i = 0; i < MAX_GROUP_MEMBERS; ++i)
                if (Members[i] == member)
                    return i;
            return -1;
        }

        public bool AddMember(CharacterBase member)
        {
            int index = FindAvailableIndex();
            if (index == -1)
                return false;
            return AddMember((byte)index, member);
        }
        public virtual bool AddMember(byte index, CharacterBase member)
        {
            if (IndexOf(member) != -1)
                return false;
            if (MembersCount == 0)
                SetLeader(member);
            Members[index] = member;
            ++MembersCount;
            return true;
        }
        public bool RemoveMember(CharacterBase member)
        {
            int index = IndexOf(member);
            if (index == -1)
                return false;
            return RemoveMember((byte)index);
        }
        public virtual bool RemoveMember(byte index)
        {
            if (Members[index] == null)
                return false;
            if (Members[index] == Leader)
                for (byte i = 0; i < MAX_GROUP_MEMBERS; ++i)
                    if (i != index && Members[i] != null)
                    {
                        SetLeader(Members[i]);
                        break;
                    }
            Members[index] = null;
            --MembersCount;
            return true;
        }
        public virtual void SetLeader(CharacterBase leader)
        {
            Leader = leader;
        }

        public IEnumerable<CharacterBase> GetMembers()
        {
            for (byte i = 0; i < MAX_GROUP_MEMBERS; ++i)
                if (Members[i] != null)
                    yield return Members[i];
        }

        public abstract byte[] Serialize(string member);
        public abstract void Deserialize(string member, byte[] data);
    }
}