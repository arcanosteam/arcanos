﻿using Arcanos.Shared.Objects;
using Arcanos.Shared.World;
using Microsoft.Xna.Framework;

namespace Arcanos.Shared.Grids
{
    public class Grid
    {
        public readonly MapBase Map;

        public readonly GridCell[][] Cells;
        public readonly int DimX, DimY;
        public readonly float SizeX, SizeY;
        public readonly float CellSizeX, CellSizeY;

        public Grid(MapBase map, int dimX, int dimY, float sizeX, float sizeY)
        {
            Map = map;
            CellSizeX = sizeX / dimX;
            CellSizeY = sizeY / dimY;
            DimX = dimX;
            DimY = dimY;
            SizeX = sizeX;
            SizeY = sizeY;
            Cells = new GridCell[DimX][];
            for (int x = 0; x < DimX; x++)
            {
                Cells[x] = new GridCell[DimY];
                for (int y = 0; y < DimY; y++)
                    Cells[x][y] = new GridCell(this, x, y, x * CellSizeX, y * CellSizeY, CellSizeX, CellSizeY);
            }
        }

        public void UpdateAllCells()
        {
            for (int x = 0; x < DimX; x++)
                for (int y = 0; y < DimY; y++)
                    Cells[x][y].Update();
        }
        public void UpdateActiveCells()
        {
            for (int x = 0; x < DimX; x++)
                for (int y = 0; y < DimY; y++)
                    if (Cells[x][y].IsActive)
                        Cells[x][y].Update();
        }

        public void Add(WorldObjectBase wo)
        {
            GridCell cell = GetCell(wo);
            if (cell != null)
                cell.Add(wo);
        }
        public void Add(StaticModelObjectBase smo)
        {
            GridCell cell = GetCell(smo.Position);
            if (cell != null)
                cell.Add(smo);
        }
        public void Remove(WorldObjectBase wo)
        {
            GridCell cell = GetCell(wo);
            if (cell != null)
                cell.Remove(wo);
        }

        public GridCell GetCell(WorldObjectBase wo)
        {
            Vector3 pos = wo.Position;
            float x = pos.X;
            float y = pos.Y;
            if (x < 0 || y < 0) return null;
            if (x >= SizeX || y >= SizeY) return null;
            return Cells[(int)(x / CellSizeX)][(int)(y / CellSizeY)];
        }
        public GridCell GetCell(Vector3 pos)
        {
            float x = pos.X;
            float y = pos.Y;
            if (x < 0 || y < 0) return null;
            if (x >= SizeX || y >= SizeY) return null;
            return Cells[(int)(x / CellSizeX)][(int)(y / CellSizeY)];
        }
        public GridCell GetCell(float x, float y)
        {
            if (x < 0 || y < 0) return null;
            if (x >= SizeX || y >= SizeY) return null;
            return Cells[(int)(x / CellSizeX)][(int)(y / CellSizeY)];
        }
        public GridCell GetCell(int x, int y)
        {
            if (x < 0 || y < 0) return null;
            if (x >= DimX || y >= DimY) return null;
            return Cells[x][y];
        }
        public GridBounds GetGridBounds(Vector3 pos, float radius)
        {
            if (float.IsInfinity(radius))
                return new GridBounds(0, 0, DimX - 1, DimY - 1);
            int minX = (int)((pos.X - radius) / CellSizeX);
            int minY = (int)((pos.Y - radius) / CellSizeY);
            int maxX = (int)((pos.X + radius) / CellSizeX);
            int maxY = (int)((pos.Y + radius) / CellSizeY);
            if (minX < 0)
                minX = 0;
            if (minY < 0)
                minY = 0;
            if (maxX >= DimX)
                maxX = DimX - 1;
            if (maxY >= DimY)
                maxY = DimY - 1;
            return new GridBounds(minX, minY, maxX, maxY);
        }

        public void MoveObject(WorldObjectBase wo)
        {
            GridCell newCell = GetCell(wo);
            if (wo.Cell == newCell)
                return;
            if (wo.Cell != null)
                wo.Cell.Remove(wo);
            if (newCell != null)
                newCell.Add(wo);
        }
    }
}