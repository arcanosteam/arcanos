﻿namespace Arcanos.Shared.Grids
{
    public struct GridBounds
    {
        public int MinX, MinY, MaxX, MaxY;

        public GridBounds(int minX, int minY, int maxX, int maxY)
        {
            MinX = minX;
            MinY = minY;
            MaxX = maxX;
            MaxY = maxY;
        }
    }
}