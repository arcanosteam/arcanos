﻿using System.Collections.Generic;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Timing;
using Arcanos.Shared.World;
using Arcanos.Utilities;
using Microsoft.Xna.Framework;

namespace Arcanos.Shared.Grids
{
    public delegate void GridCellWorldObjectHandler(GridCell cell, WorldObjectBase wo);
    public class GridCell
    {
        private const int DEFAULT_CAPACITY_OBJECTS = 10;
        private const int DEFAULT_CAPACITY_STATICS = 25;
        private const int DEACTIVATE_DELAY = 10000;

        public static event GridCellWorldObjectHandler ObjectAdded;
        public static event GridCellWorldObjectHandler ObjectRemoved;

        public readonly MapBase Map;

        public bool IsActive;
        public long DeactivateTime;

        public readonly int X, Y;
        public readonly float PosX, PosY, SizeX, SizeY;
        public BoundingBox BoundingBox;

        public readonly List<WorldObjectBase> Objects = new List<WorldObjectBase>(DEFAULT_CAPACITY_OBJECTS);
        public readonly List<StaticModelObjectBase> SolidStatics = new List<StaticModelObjectBase>(DEFAULT_CAPACITY_STATICS);
        public readonly List<StaticModelObjectBase> Statics = new List<StaticModelObjectBase>(DEFAULT_CAPACITY_STATICS);

        public GridCell(Grid grid, int x, int y, float posX, float posY, float sizeX, float sizeY)
        {
            Map = grid.Map;
            X = x;
            Y = y;
            PosX = posX;
            PosY = posY;
            SizeX = sizeX;
            SizeY = sizeY;
            BoundingBox = new BoundingBox(new Vector3(PosX, PosY, 0), new Vector3(PosX + SizeX, PosY + SizeY, 0));
        }

        public void Update()
        {
            if (IsActive && Time.Timestamp >= DeactivateTime)
            {
                IsActive = false;
                Logger.Debug(LogCategory.Grid, "Cell [{0},{1}] deactivated", X, Y);
                return;
            }
            foreach (WorldObjectBase wo in Objects.ToArray())
                wo.Update();
        }

        public void Add(WorldObjectBase wo)
        {
            Objects.Add(wo);
            wo.Cell = this;
            if (ObjectAdded != null)
                ObjectAdded(this, wo);
        }
        public void Add(StaticModelObjectBase smo)
        {
            if ((smo.Flags & StaticModelObjectFlags.Solid) != 0)
                SolidStatics.Add(smo);
            else
                Statics.Add(smo);
            if (smo.BoundingBox.Min.Z < BoundingBox.Min.Z)
                BoundingBox.Min.Z = smo.BoundingBox.Min.Z;
            if (smo.BoundingBox.Max.Z > BoundingBox.Max.Z)
                BoundingBox.Max.Z = smo.BoundingBox.Max.Z;
        }
        public void Remove(WorldObjectBase wo)
        {
            wo.Cell = null;
            Objects.Remove(wo);
            if (ObjectRemoved != null)
                ObjectRemoved(this, wo);
        }

        public void MarkAsActive()
        {
            if (!IsActive)
            {
                IsActive = true;
                Logger.Debug(LogCategory.Grid, "Cell [{0},{1}] activated", X, Y);
            }
            DeactivateTime = Time.Timestamp + DEACTIVATE_DELAY;
        }
    }
}