﻿namespace Arcanos.Shared.Data
{
    public interface IDatabase
    {
        bool Connect();
        bool Close();
        void Clear();
        bool Load();
        bool Load(int id);
    }
}