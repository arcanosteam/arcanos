using System.Threading;

namespace Arcanos.Shared.Data
{
    public delegate T DatabaseLoadAsset<T>(int id);
    public class MemoryDatabase<T> : Database<T>
    {
        public event DatabaseLoadAsset<T> LoadHandler; 

        public override bool Connect()
        {
            return true;
        }
        public override bool Close()
        {
            return true;
        }

        public override bool Load()
        {
            return true;
        }
        public override bool Load(int id)
        {
            if (Asynchronous)
            {
                Add(id, AsyncPlaceholder);
                if (LoadHandler != null)
                    ThreadPool.QueueUserWorkItem(o =>
                    {
                        T asset = LoadHandler(id);
                        Assets[id] = asset;
                        AssetIndex[asset] = id;
                    });
                return true;
            }

            if (LoadHandler != null)
            {
                Add(id, LoadHandler(id));
                return true;
            }

            return false;
        }
        public override bool Load(int idMin, int idMax)
        {
            if (Asynchronous)
            {
                for (int id = idMin; id <= idMax; id++)
                    Add(id, AsyncPlaceholder);
                if (LoadHandler != null)
                    ThreadPool.QueueUserWorkItem(o =>
                    {
                        for (int id = idMin; id <= idMax; id++)
                        {
                            T asset = LoadHandler(id);
                            Assets[id] = asset;
                            AssetIndex[asset] = id;
                        }
                    });
                return true;
            }

            if (LoadHandler != null)
            {
                for (int id = idMin; id <= idMax; id++)
                    Add(id, LoadHandler(id));
                return true;
            }

            return false;
        }
        public override void Save(int id) { }
    }
}