using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;

namespace Arcanos.Shared.Data
{
    public class FileDatabase<T> : Database<T>
    {
        protected struct IndexEntry
        {
            public int ID;
            public int IndexOffset;
            public int DataOffset;
            public int DataLength;
        }

        protected const int HEADER_SIZE = 4 + 4 + 4 + 4;
        protected const int INDEX_ENTRY_SIZE = 4 + 4 + 4;
        private const int MAX_ASSETS_TO_START_AT_1000 = (0x1000 - HEADER_SIZE) / INDEX_ENTRY_SIZE;

        public readonly string Filename;
        public readonly char[] Magic;
        public readonly Encoding Encoding;
        public bool Connected { get; protected set; }

        protected int maxAssets;
        public int MaxAssets
        {
            get { return maxAssets; }
            set
            {
                maxAssets = value;
                if (Connected)
                    RewriteFile(false);
            }
        }

        protected FileStream stream;
        protected BinaryReader reader;
        protected BinaryWriter writer;

        protected int indexOffset, dataOffset;
        protected int usedAssets;
        protected Dictionary<int, IndexEntry> indexCache;
        protected bool allLoaded;

        public FileDatabase(string filename, char[] magic) : this(filename, magic, Encoding.UTF8) { }
        public FileDatabase(string filename, char[] magic, Encoding encoding)
        {
            Filename = filename;
            Magic = magic;
            Encoding = encoding;
            MaxAssets = MAX_ASSETS_TO_START_AT_1000;
        }

        protected void RewriteFile(bool empty)
        {
            lock (threadLock)
            {
                if (!empty && !allLoaded)
                    Load();

                // Calculate fields
                dataOffset = HEADER_SIZE + maxAssets * INDEX_ENTRY_SIZE;
                usedAssets = Assets.Count;

                writer.Seek(0, SeekOrigin.Begin);
                // Write header
                writer.Write(Magic);
                writer.Write(dataOffset);
                writer.Write(maxAssets);
                writer.Write(usedAssets);
                // Write index placeholder
                int indexPos = indexOffset = (int)stream.Position;
                byte[] index = new byte[maxAssets * INDEX_ENTRY_SIZE];
                writer.Write(index);
                // Write data
                if (indexCache == null)
                    indexCache = new Dictionary<int, IndexEntry>();
                else
                    indexCache.Clear();
                foreach (KeyValuePair<int, T> pair in Assets)
                {
                    IndexEntry entry;
                    int id = pair.Key;
                    T asset = pair.Value;

                    // Write asset data
                    byte[] data = Serialize(asset);
                    entry.ID = id;
                    entry.IndexOffset = indexPos;
                    entry.DataOffset = (int)stream.Position - dataOffset;
                    entry.DataLength = data.Length;
                    writer.Write(data);
                    int dataPosAfter = (int)stream.Position;

                    // Write data index
                    writer.Seek(indexPos, SeekOrigin.Begin);
                    writer.Write(entry.ID);
                    writer.Write(entry.DataOffset);
                    writer.Write(entry.DataLength);
                    indexPos = (int)stream.Position;
                    writer.Seek(dataPosAfter, SeekOrigin.Begin);
                }
                writer.Seek(0, SeekOrigin.Begin);
            }
        }
        protected void ReadIndexCache()
        {
            lock (threadLock)
            {
                stream.Seek(0, SeekOrigin.Begin);
                reader.ReadChars(Magic.Length);
                dataOffset = reader.ReadInt32();
                maxAssets = reader.ReadInt32();
                usedAssets = reader.ReadInt32();
                NextID = usedAssets + 1;
                if (indexCache == null)
                    indexCache = new Dictionary<int, IndexEntry>();
                else
                    indexCache.Clear();
                indexOffset = (int)stream.Position;
                for (int i = 0; i < usedAssets; i++)
                {
                    IndexEntry entry;
                    entry.IndexOffset = (int)stream.Position;
                    entry.ID = reader.ReadInt32();
                    entry.DataOffset = reader.ReadInt32();
                    entry.DataLength = reader.ReadInt32();
                    indexCache.Add(entry.ID, entry);
                }
                stream.Seek(0, SeekOrigin.Begin);
            }
        }
        protected byte[] Serialize(object obj)
        {
            return Serialization.Serialize(obj, Encoding);
        }
        protected T Deserialize(byte[] data, T obj)
        {
            return Serialization.Deserialize(data, obj, Encoding);
        }
        protected T MakeBaseObject(int id)
        {
            Type type = typeof(T);
            if (type.GetConstructor(new Type[0]) != null)
                return (T)Activator.CreateInstance(type);
            MethodInfo method;
            if ((method = type.GetMethod("CreateByID", BindingFlags.Static)) != null)
                return (T)method.Invoke(null, new object[] { id });
            return default(T);
        }
        protected void WriteAsset(int id, T asset, bool overwrite)
        {
            byte[] data;
            int oIndex = indexOffset + usedAssets * INDEX_ENTRY_SIZE;
            int oData = (int)stream.Length;

            IndexEntry entry;
            if (indexCache.TryGetValue(id, out entry))
            {
                if (!overwrite)
                    return;

                oIndex = entry.IndexOffset;
                data = Serialize(asset);
                if (data.Length <= entry.DataLength)
                    oData = entry.DataOffset + dataOffset;
            }
            else
            {
                data = Serialize(asset);
                indexCache.Add(id, new IndexEntry { ID = id, IndexOffset = oIndex, DataOffset = oData, DataLength = data.Length });
            }

            // Write asset index
            writer.Seek(oIndex, SeekOrigin.Begin);
            writer.Write(id);
            writer.Write(oData - dataOffset);
            writer.Write(data.Length);

            // Write asset data
            writer.Seek(oData, SeekOrigin.Begin);
            writer.Write(data);

            writer.Seek(0, SeekOrigin.Begin);
        }
        protected void WriteUsedAssets()
        {
            writer.Seek(HEADER_SIZE - 4, SeekOrigin.Begin);
            writer.Write(usedAssets);
            writer.Seek(0, SeekOrigin.Begin);
        }

        public void WriteAll()
        {
            RewriteFile(false);
        }

        private bool connected = false;
        public override bool Connect()
        {
            /*try
            {*/
            if (connected)
                return true;

            bool needsCreate = !File.Exists(Filename);
            try
            {
                stream = new FileStream(Filename, FileMode.OpenOrCreate, UploadChanges ? FileAccess.ReadWrite : FileAccess.Read);
            }
            catch
            {
                string newFilename = Filename + "-" + Process.GetCurrentProcess().Id;
                needsCreate = !File.Exists(newFilename);
                stream = new FileStream(newFilename, FileMode.OpenOrCreate, UploadChanges ? FileAccess.ReadWrite : FileAccess.Read);
            }
            reader = new BinaryReader(stream, Encoding);
            writer = new BinaryWriter(stream, Encoding);
            Connected = true;
            allLoaded = false;
            if (needsCreate)
                RewriteFile(true);
            else
                ReadIndexCache();
            /*}
            catch
            {
                try { writer.Close(); } catch { }
                try { reader.Close(); } catch { }
                try { stream.Close(); } catch { }
                Connected = false;
                return false;
            }*/
            connected = true;
            return true;
        }
        public override bool Close()
        {
            if (!connected)
                return true;

            WriteAll();
            reader.Close();
            writer.Close();
            stream.Close();
            connected = false;
            return true;
        }

        public override T Find(Predicate<T> condition, T defaultReturn)
        {
            lock (threadLock)
            {
                foreach (T asset in Assets.Values)
                    if (condition(asset))
                        return asset;

                foreach (int id in indexCache.Keys)
                {
                    if (Assets.ContainsKey(id) || !Load(id))
                        continue;
                    
                    if (condition(Assets[id]))
                        return Assets[id];
                    
                    AssetIndex.Remove(Assets[id]);
                    Assets.Remove(id);
                }
            }
            return defaultReturn;
        }
        public override IEnumerable<T> FindAll(Predicate<T> condition)
        {
            lock (threadLock)
            {
                foreach (T asset in Assets.Values)
                    if (condition(asset))
                        yield return asset;

                foreach (int id in indexCache.Keys)
                {
                    if (Assets.ContainsKey(id) || !Load(id))
                        continue;

                    if (condition(Assets[id]))
                        yield return Assets[id];
                    else
                    {
                        AssetIndex.Remove(Assets[id]);
                        Assets.Remove(id);
                    }
                }
            }
        }

        public override int Add(int id, T asset)
        {
            lock (threadLock)
            {
                if (usedAssets >= maxAssets)
                    throw new Exception();
                if (indexCache.ContainsKey(id))
                    return id;
                base.Add(id, asset);

                if (UploadChanges && UploadChangesImmediately)
                {
                    if (!Connected)
                        throw new Exception();
                    WriteAsset(id, asset, false);
                    ++usedAssets;
                    WriteUsedAssets();
                }
                else
                    ++usedAssets;

                return id;
            }
        }
        public override int Add(T asset)
        {
            lock (threadLock)
            {
                if (usedAssets >= maxAssets)
                    throw new Exception();
                int id = base.Add(asset);

                if (UploadChanges && UploadChangesImmediately)
                {
                    if (!Connected)
                        throw new Exception();
                    WriteAsset(id, asset, false);
                    ++usedAssets;
                    WriteUsedAssets();
                }
                else
                    ++usedAssets;

                return id;
            }
        }
        public override void Clear()
        {
            base.Clear();
            lock (threadLock)
            {
                usedAssets = 0;
                indexCache.Clear();
                allLoaded = true;
            }
        }

        public override bool Load()
        {
            lock (threadLock)
            {
                if (allLoaded)
                    return true;
                if (!Connected)
                    throw new Exception();
                if (indexCache == null)
                    ReadIndexCache();
                foreach (IndexEntry entry in indexCache.Values)
                {
                    if (Assets.ContainsKey(entry.ID))
                        continue;
                    stream.Seek(dataOffset + entry.DataOffset, SeekOrigin.Begin);
                    base.Add(entry.ID, Deserialize(reader.ReadBytes(entry.DataLength), MakeBaseObject(entry.ID)));
                }
                allLoaded = true;
                return true;
            }
        }
        public override bool Load(int id)
        {
            lock (threadLock)
            {
                if (!Connected)
                    throw new Exception();
                if (indexCache == null)
                    ReadIndexCache();

                IndexEntry entry;
                if (indexCache.TryGetValue(id, out entry))
                {
                    stream.Seek(dataOffset + entry.DataOffset, SeekOrigin.Begin);
                    base.Add(entry.ID, Deserialize(reader.ReadBytes(entry.DataLength), MakeBaseObject(entry.ID)));
                    return true;
                }
                return false;
            }
        }
        public override bool Load(int idMin, int idMax)
        {
            lock (threadLock)
            {
                if (!Connected)
                    throw new Exception();
                if (indexCache == null)
                    ReadIndexCache();

                bool success = true;
                for (int id = idMin; id <= idMax; id++)
                {
                    IndexEntry entry;
                    if (indexCache.TryGetValue(id, out entry))
                    {
                        stream.Seek(dataOffset + entry.DataOffset, SeekOrigin.Begin);
                        base.Add(entry.ID, Deserialize(reader.ReadBytes(entry.DataLength), MakeBaseObject(entry.ID)));
                    }
                    else
                        success = false;
                }
                return success;
            }
        }
        public override void Save(int id)
        {
            WriteAsset(id, Assets[id], true);
        }
    }
}