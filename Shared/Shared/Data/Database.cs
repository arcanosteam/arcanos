﻿using System;
using System.Collections;
using System.Collections.Generic;
using Arcanos.Shared.Objects;

namespace Arcanos.Shared.Data
{
    public class DatabaseAssetMissingException<T> : Exception
    {
        public DatabaseAssetMissingException(int id) : base(string.Format("Asset ID:{0} is missing in database", id)) { }
        public DatabaseAssetMissingException(T asset) : base(string.Format("Asset {0} is missing in database", asset)) { }
        public DatabaseAssetMissingException(int id, T asset) : base(string.Format("Asset {1} ID:{0} is missing in database", id, asset)) { }
    }
    public abstract class Database<T> : IDatabase, IEnumerable<KeyValuePair<int, T>>
    {
        protected int NextID = 1;
        protected readonly Dictionary<int, T> Assets = new Dictionary<int, T>();
        protected readonly Dictionary<T, int> AssetIndex = new Dictionary<T, int>();

        public bool LoadIfMissing = true;
        public bool UploadChanges = false;
        public bool UploadChangesImmediately = false;
        public bool Asynchronous = false;
        public T AsyncPlaceholder = default(T);

        protected readonly object threadLock = new object();

        public virtual T this[int id]
        {
            get
            {
                lock (threadLock)
                {
                    T asset;
                    if (Assets.TryGetValue(id, out asset))
                        return asset;
                    if (LoadIfMissing)
                        Load(id);
                    if (Assets.TryGetValue(id, out asset))
                        return asset;
                    throw new DatabaseAssetMissingException<T>(id);
                }
            }
        }

        public virtual int Add(T asset)
        {
            lock (threadLock)
            {
                Assets.Add(NextID, asset);
                if (asset != null)
                {
                    AssetIndex.Add(asset, NextID);
                    if (asset is IReferencedByID)
                        (asset as IReferencedByID).ID = NextID;
                    if (asset is IReferencedByGUID)
                        (asset as IReferencedByGUID).GUID = (ulong)NextID;
                }
                return NextID++;
            }
        }
        public T AddAndReturn(T asset)
        {
            Add(asset);
            return asset;
        }
        public virtual int Add(int id, T asset)
        {
            lock (threadLock)
            {
                T oldAsset;
                if (Assets.TryGetValue(id, out oldAsset))
                {
                    Assets.Remove(id);
                    AssetIndex.Remove(oldAsset);
                }
                Assets.Add(id, asset);
                if (asset != null)
                {
                    AssetIndex.Add(asset, id);
                    if (asset is IReferencedByID)
                        (asset as IReferencedByID).ID = id;
                    if (asset is IReferencedByGUID)
                        (asset as IReferencedByGUID).GUID = (ulong)id;
                }
                if (id >= NextID)
                    NextID = id + 1;
            }
            return id;
        }
        public virtual void Skip(int ids)
        {
            NextID += ids;
        }
        public virtual void Clear()
        {
            lock (threadLock)
            {
                Assets.Clear();
                AssetIndex.Clear();
                NextID = 1;
            }
        }
        public virtual T GetOrDefault(int id)
        {
            T asset;
            if (Assets.TryGetValue(id, out asset))
                return asset;
            if (LoadIfMissing)
                Load(id);
            if (Assets.TryGetValue(id, out asset))
                return asset;
            return default(T);
        }
        public virtual int GetID(T asset)
        {
            int id;
            lock (threadLock)
                if (AssetIndex.TryGetValue(asset, out id))
                    return id;
            //throw new DatabaseAssetMissingException<T>(asset);
            return -1;
        }
        public virtual T Find(Predicate<T> condition)
        {
            return Find(condition, default(T));
        }
        public virtual T Find(Predicate<T> condition, T defaultReturn)
        {
            Load();
            lock (threadLock)
                foreach (T asset in Assets.Values)
                    if (condition(asset))
                        return asset;
            return defaultReturn;
        }
        public virtual IEnumerable<T> FindAll(Predicate<T> condition)
        {
            Load();
            lock (threadLock)
                foreach (T asset in Assets.Values)
                    if (condition(asset))
                        yield return asset;
        }

        public abstract bool Connect();
        public abstract bool Close();

        public abstract bool Load();
        public abstract bool Load(int id);
        public abstract bool Load(int idMin, int idMax);
        public abstract void Save(int id);

        public IEnumerator<KeyValuePair<int, T>> GetEnumerator()
        {
            Load();
            return Assets.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}