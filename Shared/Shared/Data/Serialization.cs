﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Objects;
using Arcanos.Utilities;
using Microsoft.Xna.Framework;

namespace Arcanos.Shared.Data
{
    public enum SerializationMethod
    {
        ByValue,
        //ByFlag,
        RefByID,
        RefByGUID,
        Manual,
    }

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Field | AttributeTargets.Property, Inherited = true, AllowMultiple = false)]
    public sealed class SerializationSettingsAttribute : Attribute
    {
        private Type referenceResolverType;
        private string referenceResolverMethod;
        public SerializationReferenceResolver ReferenceResolver;

        public SerializationMethod Method { get; set; }
        public string FlagName { get; set; }
        public Type ReferenceResolverType
        {
            get { return referenceResolverType; }
            set
            {
                referenceResolverType = value;
                MakeResolver();
            }
        }
        public string ReferenceResolverMethod
        {
            get { return referenceResolverMethod; }
            set
            {
                referenceResolverMethod = value;
                MakeResolver();
            }
        }
        public bool SkipSerialization { get; set; }
        public bool SkipDeserialization { get; set; }

        public SerializationSettingsAttribute()
        {
            Method = SerializationMethod.ByValue;
        }

        private void MakeResolver()
        {
            if (referenceResolverType == null || referenceResolverMethod == null)
                return;
            ReferenceResolver = (SerializationReferenceResolver)Delegate.CreateDelegate(typeof(SerializationReferenceResolver), referenceResolverType.GetMethod(referenceResolverMethod, BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic));
        }
    }

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Field | AttributeTargets.Property, Inherited = true, AllowMultiple = false)]
    public sealed class SerializationIgnoreAttribute : Attribute { }

    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, Inherited = true, AllowMultiple = false)]
    public sealed class SerializeAsAttribute : Attribute
    {
        public Type Type { get; set; }

        public SerializeAsAttribute(Type type)
        {
            Type = type;
        }
    }

    public interface IPostDeserialization
    {
        void MemberDeserialized(string member, object value);
        void Deserialized(object parent);
    }
    public interface IPreSerialization
    {
        void StartedSerialization();
    }
    public interface IPreDeserialization
    {
        void StartedDeserialization();
    }
    public interface IManualSerialization
    {
        byte[] Serialize(string member);
    }
    public interface IManualDeserialization
    {
        void Deserialize(string member, byte[] data);
    }

    public delegate void Serializer(BinaryWriter writer, object obj);
    public delegate object Deserializer(BinaryReader reader);
    public delegate object SerializationReferenceResolver(int refID);
    public static class Serialization
    {
        public const BindingFlags MEMBER_FLAGS = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
        private const int DEFAULT_BUFFER_SIZE = 1024;

        private readonly static Dictionary<Type, int> registeredTypes = new Dictionary<Type, int>();
        private readonly static Dictionary<int, Type> registeredIdentifiers = new Dictionary<int, Type>();
        private static int nextIdentifier = 1;

        public readonly static Dictionary<Type, Serializer> Serializers = new Dictionary<Type, Serializer>();
        public readonly static Dictionary<Type, Deserializer> Deserializers = new Dictionary<Type, Deserializer>();

        public static bool DebugOutput;

        static Serialization()
        {
            foreach (Type type in new[]
            {
                typeof(int),
                typeof(uint),
                typeof(ulong),
                typeof(ushort),
                typeof(byte),
                typeof(float),
                typeof(bool),
                typeof(string),
                typeof(Vector3),
                typeof(Vector4),
                typeof(Rotation),
                typeof(Text),
                typeof(ByteBounds),
                typeof(IntBounds),
                typeof(FloatBounds),
                typeof(Damage),
                typeof(DamageBounds),
                typeof(int[]),
            })
                Register(type);

            Serializers.Add(typeof(Vector3), (writer, obj) =>
            {
                Vector3 v = (Vector3)obj;
                writer.Write(v.X);
                writer.Write(v.Y);
                writer.Write(v.Z);
            });
            Deserializers.Add(typeof(Vector3), reader => new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle()));
            Serializers.Add(typeof(Vector4), (writer, obj) =>
            {
                Vector4 v = (Vector4)obj;
                writer.Write(v.X);
                writer.Write(v.Y);
                writer.Write(v.Z);
                writer.Write(v.W);
            });
            Deserializers.Add(typeof(Vector4), reader => new Vector4(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle()));
            Serializers.Add(typeof(Rotation), (writer, obj) =>
            {
                Rotation v = (Rotation)obj;
                writer.Write(v.Yaw);
                writer.Write(v.Pitch);
                writer.Write(v.Roll);
            });
            Deserializers.Add(typeof(Rotation), reader => new Rotation(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle()));
            Serializers.Add(typeof(Text), (writer, obj) =>
            {
                Text t = (Text)obj;
                int localesLength = Enum.GetValues(typeof(LocaleIndex)).Length;
                for (int locale = 0; locale < localesLength; locale++)
                    writer.Write(t.LocalizedStrings[locale] ?? string.Empty);
            });
            Deserializers.Add(typeof(Text), reader =>
            {
                Text t = new Text();
                int localesLength = Enum.GetValues(typeof(LocaleIndex)).Length;
                for (int locale = 0; locale < localesLength; locale++)
                {
                    string str = reader.ReadString();
                    t.LocalizedStrings[locale] = str == string.Empty ? null : str;
                }
                return t;
            });
            Serializers.Add(typeof(ByteBounds), (writer, obj) =>
            {
                ByteBounds b = (ByteBounds)obj;
                writer.Write(b.Min);
                writer.Write(b.Max);
            });
            Deserializers.Add(typeof(ByteBounds), reader => new ByteBounds(reader.ReadByte(), reader.ReadByte()));
            Serializers.Add(typeof(IntBounds), (writer, obj) =>
            {
                IntBounds b = (IntBounds)obj;
                writer.Write(b.Min);
                writer.Write(b.Max);
            });
            Deserializers.Add(typeof(IntBounds), reader => new IntBounds(reader.ReadInt32(), reader.ReadInt32()));
            Serializers.Add(typeof(FloatBounds), (writer, obj) =>
            {
                FloatBounds b = (FloatBounds)obj;
                writer.Write(b.Min);
                writer.Write(b.Max);
            });
            Deserializers.Add(typeof(FloatBounds), reader => new FloatBounds(reader.ReadSingle(), reader.ReadSingle()));
            Serializers.Add(typeof(Damage), (writer, obj) =>
            {
                Damage damage = (Damage)obj;
                writer.Write(damage.Generic);
                writer.Write(damage.Physical);
                writer.Write(damage.Fire);
                writer.Write(damage.Water);
                writer.Write(damage.Ground);
                writer.Write(damage.Air);
                writer.Write(damage.Light);
                writer.Write(damage.Dark);
            });
            Deserializers.Add(typeof(Damage), reader => new Damage()
            {
                Generic = reader.ReadInt32(),
                Physical = reader.ReadInt32(),
                Fire = reader.ReadInt32(),
                Water = reader.ReadInt32(),
                Ground = reader.ReadInt32(),
                Air = reader.ReadInt32(),
                Light = reader.ReadInt32(),
                Dark = reader.ReadInt32(),
            });
            Serializers.Add(typeof(DamageBounds), (writer, obj) =>
            {
                DamageBounds damage = (DamageBounds)obj;
                writer.Write(damage.Generic.Min);
                writer.Write(damage.Generic.Max);
                writer.Write(damage.Physical.Min);
                writer.Write(damage.Physical.Max);
                writer.Write(damage.Fire.Min);
                writer.Write(damage.Fire.Max);
                writer.Write(damage.Water.Min);
                writer.Write(damage.Water.Max);
                writer.Write(damage.Ground.Min);
                writer.Write(damage.Ground.Max);
                writer.Write(damage.Air.Min);
                writer.Write(damage.Air.Max);
                writer.Write(damage.Light.Min);
                writer.Write(damage.Light.Max);
                writer.Write(damage.Dark.Min);
                writer.Write(damage.Dark.Max);
            });
            Deserializers.Add(typeof(DamageBounds), reader => new DamageBounds()
            {
                Generic = new IntBounds(reader.ReadInt32(), reader.ReadInt32()),
                Physical = new IntBounds(reader.ReadInt32(), reader.ReadInt32()),
                Fire = new IntBounds(reader.ReadInt32(), reader.ReadInt32()),
                Water = new IntBounds(reader.ReadInt32(), reader.ReadInt32()),
                Ground = new IntBounds(reader.ReadInt32(), reader.ReadInt32()),
                Air = new IntBounds(reader.ReadInt32(), reader.ReadInt32()),
                Light = new IntBounds(reader.ReadInt32(), reader.ReadInt32()),
                Dark = new IntBounds(reader.ReadInt32(), reader.ReadInt32()),
            });
            Serializers.Add(typeof(int[]), (writer, obj) =>
            {
                int[] array = (int[])obj;
                Write7BitEncodedInt(writer, array.Length);
                for (int i = 0; i < array.Length; ++i)
                    writer.Write(array[i]);
            });
            Deserializers.Add(typeof(int[]), reader =>
            {
                int[] array = new int[Read7BitEncodedInt(reader)];
                for (int i = 0; i < array.Length; ++i)
                    array[i] = reader.ReadInt32();
                return array;
            });

            SetRegisterID(0x40);
        }

        public static int Register(Type type)
        {
            int identifier;
            if (registeredTypes.TryGetValue(type, out identifier))
                return identifier;
            identifier = nextIdentifier++;
            registeredTypes.Add(type, identifier);
            registeredIdentifiers.Add(identifier, type);
            return identifier;
        }
        public static int Register(int identifier, Type type)
        {
            registeredTypes[type] = identifier;
            registeredIdentifiers[identifier] = type;
            return identifier;
        }
        public static void Register()
        {
            nextIdentifier++;
        }
        public static void SetRegisterID(int id)
        {
            nextIdentifier = id;
        }
        public static IEnumerable<Type> GetRegisteredTypes()
        {
            return registeredTypes.Keys;
        }

        public static byte[] Serialize(object obj)
        {
            return Serialize(obj, null, Encoding.UTF8);
        }
        public static byte[] Serialize(object obj, Type asType)
        {
            return Serialize(obj, asType, Encoding.UTF8);
        }
        public static byte[] Serialize(object obj, Encoding encoding)
        {
            return Serialize(obj, null, encoding);
        }
        public static byte[] Serialize(object obj, Type asType, Encoding encoding)
        {
            MemoryStream stream = new MemoryStream(DEFAULT_BUFFER_SIZE);
            BinaryWriter writer = new BinaryWriter(stream, encoding);

            DebugStart();
            InnerSerialize(writer, obj, asType, encoding);
            DebugEnd();

            writer.Flush();
            byte[] buffer = stream.ToArray();
            stream.Close();
            return buffer;
        }
        private static void InnerSerialize(BinaryWriter writer, object obj, Type asType, Encoding encoding)
        {
            Type type = obj.GetType();

            int identifier;
            Write7BitEncodedInt(writer, registeredTypes.TryGetValue(type, out identifier) ? identifier : 0);
            if (identifier == 0)
                throw new Exception("Unregistered serializable type " + type.Name);

            Serializer serializer;
            if (type == typeof(int))
            {
                writer.Write((int)obj);
                DebugPrintPart("int");
            }
            else if (type == typeof(uint))
            {
                writer.Write((uint)obj);
                DebugPrintPart("uint");
            }
            else if (type == typeof(ulong))
            {
                writer.Write((ulong)obj);
                DebugPrintPart("ulong");
            }
            else if (type == typeof(ushort))
            {
                writer.Write((ushort)obj);
                DebugPrintPart("ushort");
            }
            else if (type == typeof(byte))
            {
                writer.Write((byte)obj);
                DebugPrintPart("byte");
            }
            else if (type == typeof(float))
            {
                writer.Write((float)obj);
                DebugPrintPart("float");
            }
            else if (type == typeof(bool))
            {
                writer.Write((bool)obj);
                DebugPrintPart("bool");
            }
            else if (type == typeof(string))
            {
                string s = (string)obj;
                Write7BitEncodedInt(writer, encoding.GetByteCount(s));
                writer.Write(encoding.GetBytes(s));
                DebugPrintPart("string");
            }
            else if (Serializers.TryGetValue(asType ?? type, out serializer))
            {
                serializer(writer, obj);
                DebugPrintPart("internal serializer");
            }
            else
            {
                DebugPrint("object " + type.Name + " {{");
                DebugOpen();

                if (typeof(IPreSerialization).IsAssignableFrom(asType ?? type))
                    ((IPreSerialization)obj).StartedSerialization();

                Type attrIgnore = typeof(SerializationIgnoreAttribute);
                Type attrSettings = typeof(SerializationSettingsAttribute);
                Type typeList = typeof(IList);
                Type typeDict = typeof(IDictionary);
                foreach (MemberInfo member in GetSerializationMembers(type, asType))
                {
                    if (member.IsDefined(attrIgnore, false))
                        continue;
                    if (member.Name.Contains("<")) // Compiler-generated fields
                        continue;
                    SerializationMethod method = SerializationMethod.ByValue;
                    bool skip = false;
                    foreach (SerializationSettingsAttribute attr in member.GetCustomAttributes(attrSettings, true))
                    {
                        method = attr.Method;
                        if (attr.SkipSerialization)
                            skip = true;
                    }
                    if (skip)
                        continue;
                    if (method == SerializationMethod.Manual)
                    {
                        byte[] data = ((IManualSerialization)obj).Serialize(member.Name);
                        Write7BitEncodedInt(writer, data.Length);
                        writer.Write(data);
                        DebugPrint("{0} manual", member.Name);
                    }
                    else
                    {
                        object value;
                        Type valueType;
                        switch (member.MemberType)
                        {
                            case MemberTypes.Field:
                                FieldInfo fi = member as FieldInfo;
                                value = fi.GetValue(obj);
                                valueType = fi.FieldType;
                                DebugPrintPart("{0} field: ", member.Name);
                                break;
                            case MemberTypes.Property:
                                PropertyInfo pi = member as PropertyInfo;
                                if (!pi.CanWrite)
                                    continue;
                                value = pi.GetValue(obj, null);
                                valueType = pi.PropertyType;
                                DebugPrintPart("{0} property: ", member.Name);
                                break;
                            default:
                                continue;
                        }
                        if (valueType.IsArray)
                        {
                            DebugPrint("as array [");
                            if (value == null)
                            {
                                writer.Write(0);
                                continue;
                            }
                            int rank = valueType.GetArrayRank();
                            Array array = (Array)value;
                            valueType = valueType.GetElementType();
                            int[] lengths = new int[rank];
                            for (int r = 0; r < rank; r++)
                            {
                                lengths[r] = array.GetLength(r);
                                writer.Write(lengths[r]);
                            }

                            Func<int[], int[]> nextIndex = index =>
                            {
                                for (int i = index.Length - 1; i >= 0; --i)
                                {
                                    index[i]++;
                                    if (index[i] < lengths[i])
                                        return index;
                                    index[i] = 0;
                                }
                                return null;
                            };
                            DebugOpen();
                            if (lengths[0] > 0)
                                for (int[] index = new int[rank]; index != null; index = nextIndex(index))
                                    InnerSerializeByType(writer, array.GetValue(index), valueType, method, encoding);
                            DebugClose();
                            DebugPrint("]");
                        }
                        else if (typeList.IsAssignableFrom(valueType))
                        {
                            DebugPrint("as list [");
                            IList list = value as IList;
                            Type listValueType = valueType.GetGenericArguments()[0];
                            Write7BitEncodedInt(writer, list.Count);
                            DebugOpen();
                            foreach (object item in list)
                                InnerSerializeByType(writer, item, listValueType, method, encoding);
                            DebugClose();
                            DebugPrint("]");
                        }
                        else if (typeDict.IsAssignableFrom(valueType))
                        {
                            DebugPrint("as dictionary [");
                            IDictionary dict = value as IDictionary;
                            Type dictKeyType = valueType.GetGenericArguments()[0];
                            Type dictValueType = valueType.GetGenericArguments()[1];
                            Write7BitEncodedInt(writer, dict.Count);
                            DebugOpen();
                            foreach (DictionaryEntry item in dict)
                            {
                                InnerSerializeByType(writer, item.Key, dictKeyType, method, encoding);
                                InnerSerializeByType(writer, item.Value, dictValueType, method, encoding);
                            }
                            DebugClose();
                            DebugPrint("]");
                        }
                        else
                            InnerSerializeByType(writer, value, valueType, method, encoding);
                    }
                }
                DebugClose();
                DebugPrint("}}");
            }
        }
        private static void InnerSerializeByType(BinaryWriter writer, object value, Type valueType, SerializationMethod method, Encoding encoding)
        {
            Type type = value == null ? valueType : value.GetType();
            foreach (SerializationSettingsAttribute attr in type.GetCustomAttributes(typeof(SerializationSettingsAttribute), true))
                method = attr.Method;
            switch (method)
            {
                case SerializationMethod.ByValue:
                    DebugPrintPart("as value ");
                    if (valueType.IsEnum)
                        InnerSerialize(writer, Convert.ChangeType(value, Enum.GetUnderlyingType(valueType)), null, encoding);
                    else
                    {
                        if (valueType.IsClass || valueType == typeof(String))
                            writer.Write(value != null);
                        if (value != null)
                            InnerSerialize(writer, value, null, encoding);
                    }
                    DebugPrint("");
                    return;
                case SerializationMethod.RefByID:
                    DebugPrint("as RefByID");
                    writer.Write(value == null ? 0 : ((IReferencedByID)value).ID);
                    return;
                case SerializationMethod.RefByGUID:
                    DebugPrint("as RefByGUID");
                    writer.Write(value == null ? 0 : ((IReferencedByGUID)value).GUID);
                    return;
                case SerializationMethod.Manual:
                    return;
            }
        }
        public static T Deserialize<T>(byte[] data, T obj)
        {
            return Deserialize(data, obj, null, Encoding.UTF8);
        }
        public static T Deserialize<T>(byte[] data, T obj, Type asType)
        {
            return Deserialize(data, obj, asType, Encoding.UTF8);
        }
        public static T Deserialize<T>(byte[] data, T obj, Encoding encoding)
        {
            return Deserialize(data, obj, null, encoding);
        }
        public static T Deserialize<T>(byte[] data, T obj, Type asType, Encoding encoding)
        {
            MemoryStream stream = new MemoryStream(data);
            BinaryReader reader = new BinaryReader(stream, encoding);

            DebugStart();
            obj = (T)InnerDeserialize(reader, obj, asType, null, encoding);
            DebugEnd();

            return obj;
        }
        private static object InnerDeserialize(BinaryReader reader, object obj, Type asType, object parent, Encoding encoding)
        {
            Type type;

            int identifier = Read7BitEncodedInt(reader);
            if (!registeredIdentifiers.TryGetValue(identifier, out type))
                throw new Exception("Unknown serialized type with identifier " + identifier);

            Deserializer deserializer;
            if (type == typeof(int))
                return reader.ReadInt32();
            if (type == typeof(uint))
                return reader.ReadUInt32();
            if (type == typeof(ulong))
                return reader.ReadUInt64();
            if (type == typeof(ushort))
                return reader.ReadUInt16();
            if (type == typeof(byte))
                return reader.ReadByte();
            if (type == typeof(float))
                return reader.ReadSingle();
            if (type == typeof(bool))
                return reader.ReadBoolean();
            if (type == typeof(string))
            {
                int l = Read7BitEncodedInt(reader);
                return encoding.GetString(reader.ReadBytes(l));
            }
            if (type != null || asType != null)
            {
                if (Deserializers.TryGetValue(asType ?? type, out deserializer))
                    return deserializer(reader);

                bool invokePreDeserialization = typeof(IPreDeserialization).IsAssignableFrom(type ?? asType);
                bool invokePostDeserialization = typeof(IPostDeserialization).IsAssignableFrom(type ?? asType);
                if (obj == null)
                    obj = Activator.CreateInstance(type, MEMBER_FLAGS | BindingFlags.CreateInstance, null, null, null);

                if (invokePreDeserialization)
                    ((IPreDeserialization)obj).StartedDeserialization();

                Type attrIgnore = typeof(SerializationIgnoreAttribute);
                Type attrSettings = typeof(SerializationSettingsAttribute);
                Type typeList = typeof(IList);
                Type typeDict = typeof(IDictionary);
                foreach (MemberInfo member in GetSerializationMembers(type, asType))
                {
                    if (member.IsDefined(attrIgnore, false))
                        continue;
                    if (member.Name.Contains("<")) // Compiler-generated fields
                        continue;
                    SerializationMethod method = SerializationMethod.ByValue;
                    SerializationReferenceResolver refResolver = null;
                    bool skip = false;
                    foreach (SerializationSettingsAttribute attr in member.GetCustomAttributes(attrSettings, true))
                    {
                        method = attr.Method;
                        if (method == SerializationMethod.RefByID ||
                            method == SerializationMethod.RefByGUID)
                            refResolver = attr.ReferenceResolver;
                        if (attr.SkipDeserialization)
                            skip = true;
                    }
                    if (skip)
                        continue;
                    Type asValueType = null;
                    foreach (SerializeAsAttribute attr in member.GetCustomAttributes(typeof(SerializeAsAttribute), true))
                        asValueType = attr.Type;
                    if (method == SerializationMethod.Manual)
                    {
                        int l = Read7BitEncodedInt(reader);
                        ((IManualDeserialization)obj).Deserialize(member.Name, reader.ReadBytes(l));
                    }
                    else
                    {
                        object value;
                        Type valueType;
                        switch (member.MemberType)
                        {
                            case MemberTypes.Field:
                                FieldInfo fi = member as FieldInfo;
                                value = fi.GetValue(obj);
                                valueType = fi.FieldType;
                                if (valueType.IsArray)
                                {
                                    int rank = valueType.GetArrayRank();
                                    if (rank == 0)
                                        value = null;
                                    else
                                    {
                                        Type arrayValueType = valueType.GetElementType();
                                        int[] lengths = new int[rank];
                                        for (int r = 0; r < rank; r++)
                                            lengths[r] = reader.ReadInt32();
                                        if (lengths[0] == 0)
                                            value = Array.CreateInstance(arrayValueType, 0);
                                        else
                                        {
                                            Array array = Array.CreateInstance(arrayValueType, lengths);

                                            Func<int[], int[]> nextIndex = index =>
                                            {
                                                for (int i = index.Length - 1; i >= 0; --i)
                                                {
                                                    index[i]++;
                                                    if (index[i] < lengths[i])
                                                        return index;
                                                    index[i] = 0;
                                                }
                                                return null;
                                            };
                                            for (int[] index = new int[rank]; index != null; index = nextIndex(index))
                                            {
                                                object element = InnerDeserializeByType(reader, array.GetValue(index), asValueType ?? arrayValueType, obj, method, refResolver, encoding);
                                                array.SetValue(element, index);
                                            }
                                            value = array;
                                        }
                                    }
                                    fi.SetValue(obj, value);
                                }
                                else if (typeList.IsAssignableFrom(valueType))
                                {
                                    IList list = value as IList;
                                    Type listValueType = valueType.GetGenericArguments()[0];
                                    int count = Read7BitEncodedInt(reader);
                                    for (int i = 0; i < count; i++)
                                    {
                                        object element = InnerDeserializeByType(reader, null, asValueType ?? listValueType, obj, method, refResolver, encoding);
                                        list.Add(element);
                                    }
                                }
                                else if (typeDict.IsAssignableFrom(valueType))
                                {
                                    IDictionary dict = value as IDictionary;
                                    Type dictKeyType = valueType.GetGenericArguments()[0];
                                    Type dictValueType = valueType.GetGenericArguments()[1];
                                    int count = Read7BitEncodedInt(reader);
                                    for (int i = 0; i < count; i++)
                                    {
                                        object elementKey = InnerDeserializeByType(reader, null, dictKeyType, obj, method, refResolver, encoding);
                                        object elementValue = InnerDeserializeByType(reader, null, asValueType ?? dictValueType, obj, method, refResolver, encoding);
                                        dict.Add(elementKey, elementValue);
                                    }
                                }
                                else
                                {
                                    object element = InnerDeserializeByType(reader, value, asValueType ?? valueType, obj, method, refResolver, encoding);
                                    fi.SetValue(obj, element);
                                }
                                break;
                            case MemberTypes.Property:
                                PropertyInfo pi = member as PropertyInfo;
                                if (!pi.CanWrite)
                                    continue;
                                value = pi.GetValue(obj, null);
                                valueType = pi.PropertyType;
                                if (typeList.IsAssignableFrom(valueType))
                                {
                                    IList list = value as IList;
                                    Type listValueType = valueType.GetGenericArguments()[0];
                                    int count = Read7BitEncodedInt(reader);
                                    for (int i = 0; i < count; i++)
                                    {
                                        object element = InnerDeserializeByType(reader, null, asValueType ?? listValueType, obj, method, refResolver, encoding);
                                        list.Add(element);
                                    }
                                }
                                else if (typeDict.IsAssignableFrom(valueType))
                                {
                                    IDictionary dict = value as IDictionary;
                                    Type dictKeyType = valueType.GetGenericArguments()[0];
                                    Type dictValueType = valueType.GetGenericArguments()[1];
                                    int count = Read7BitEncodedInt(reader);
                                    for (int i = 0; i < count; i++)
                                    {
                                        object elementKey = InnerDeserializeByType(reader, null, dictKeyType, obj, method, refResolver, encoding);
                                        object elementValue = InnerDeserializeByType(reader, null, asValueType ?? dictValueType, obj, method, refResolver, encoding);
                                        dict.Add(elementKey, elementValue);
                                    }
                                }
                                else
                                {
                                    object element = InnerDeserializeByType(reader, value, asValueType ?? valueType, obj, method, refResolver, encoding);
                                    pi.SetValue(obj, element, null);
                                }
                                break;
                            default:
                                continue;
                        }
                        if (invokePostDeserialization)
                            ((IPostDeserialization)obj).MemberDeserialized(member.Name, value);
                    }
                }
                if (invokePostDeserialization)
                    ((IPostDeserialization)obj).Deserialized(parent);
            }
            if (obj == null)
                throw new Exception();
            return obj;
        }
        private static object InnerDeserializeByType(BinaryReader reader, object value, Type valueType, object parent, SerializationMethod method, SerializationReferenceResolver refResolver, Encoding encoding)
        {
            Type type = value == null ? valueType : value.GetType();
            foreach (SerializationSettingsAttribute attr in type.GetCustomAttributes(typeof(SerializationSettingsAttribute), true))
            {
                method = attr.Method;
                if (method == SerializationMethod.RefByID || method == SerializationMethod.RefByGUID)
                    refResolver = attr.ReferenceResolver;
            }
            switch (method)
            {
                case SerializationMethod.ByValue:
                    if (valueType.IsEnum)
                        return Enum.ToObject(valueType, InnerDeserialize(reader, value, type, parent, encoding));
                    if ((valueType.IsClass || valueType == typeof(string)) && reader.ReadBoolean() == false)
                        return null;
                    return InnerDeserialize(reader, value, null, parent, encoding);
                case SerializationMethod.RefByID:
                    int id = reader.ReadInt32();
                    return id == 0 ? null : refResolver(id);
                case SerializationMethod.RefByGUID:
                    ulong guid = reader.ReadUInt64();
                    return guid == 0 ? null : refResolver((int)guid);
                case SerializationMethod.Manual:
                    break;
            }
            throw new Exception();
        }

        private static int debugPrintIndent = 0;
        private static bool debugPrintPart = false;
        public static object outputLock = new object();
        [Conditional("DEBUG")]
        private static void DebugPrint(string format, params object[] args)
        {
            if (!DebugOutput)
                return;
            lock (outputLock)
                if (debugPrintPart)
                    Logger.RawLine(format, args);
                else
                    Logger.RawLine(new string(' ', debugPrintIndent * 4) + format, args);
            debugPrintPart = false;
        }
        [Conditional("DEBUG")]
        private static void DebugPrintPart(string format, params object[] args)
        {
            if (!DebugOutput)
                return;
            lock (outputLock)
                if (debugPrintPart)
                    Logger.Raw(format, args);
                else
                    Logger.Raw(new string(' ', debugPrintIndent * 4) + format, args);
            debugPrintPart = true;
        }
        [Conditional("DEBUG")]
        private static void DebugOpen()
        {
            if (!DebugOutput)
                return;
            debugPrintIndent++;
        }
        [Conditional("DEBUG")]
        private static void DebugClose()
        {
            if (!DebugOutput)
                return;
            debugPrintIndent--;
        }
        [Conditional("DEBUG")]
        private static void DebugStart()
        {
            if (!DebugOutput)
                return;
            Monitor.Enter(outputLock);
        }
        [Conditional("DEBUG")]
        private static void DebugEnd()
        {
            if (!DebugOutput)
                return;
            Monitor.Exit(outputLock);
        }

        public static IEnumerable<MemberInfo> GetSerializationMembers(Type type, Type asType)
        {
            return GetSerializationMembers(type, asType, true);
        }
        public static IEnumerable<MemberInfo> GetSerializationMembers(Type type, Type asType, bool alphabeticOrder)
        {
            MemberInfo[] members = (asType ?? type).GetMembers(MEMBER_FLAGS);
            if (alphabeticOrder)
                Array.Sort(members, (a, b) => String.Compare(a.Name, b.Name, StringComparison.Ordinal));
            return members;
        }

        private static void Write7BitEncodedInt(BinaryWriter writer, int value)
        {
            uint num = (uint)value;
            while (num >= 128U)
            {
                writer.Write((byte)(num | 128U));
                num >>= 7;
            }
            writer.Write((byte)num);
        }
        private static int Read7BitEncodedInt(BinaryReader reader)
        {
            int num1 = 0;
            int num2 = 0;
            while (num2 != 35)
            {
                byte num3 = reader.ReadByte();
                num1 |= (num3 & sbyte.MaxValue) << num2;
                num2 += 7;
                if ((num3 & 128) == 0)
                    return num1;
            }
            throw new Exception();
        }
    }
}