using System;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Timing;

namespace Arcanos.Shared.Graphics
{
    public class SpriteModelInstance : ModelInstance
    {
        public SpriteModelAnimation CurrentAnimation;
        private int cachedFrame;

        public SpriteModelInstance(SpriteModel model, IWorldEntity owner) : base(model, owner)
        {
            if (model.Animations != null && model.Animations.Length > 0)
                PlayAnimation(model.Animations[0].Name);
        }

        public override void Update()
        {
            base.Update();

            if (CurrentAnimation == null)
                return;

            TimePassed += Time.PerSecond;

            if (CurrentAnimation.Length == 0)
            {
                cachedFrame = CurrentAnimation.Frames[0];
                return;
            }

            if (TimePassed >= CurrentAnimation.Length)
            {
                TimePassed -= CurrentAnimation.Length;
                if (NextAnimationName != null)
                {
                    PlayAnimation(NextAnimationName);
                    if (CurrentAnimation.Length == 0)
                    {
                        cachedFrame = CurrentAnimation.Frames[0];
                        return;
                    }
                }
            }

            float percent = TimePassed / CurrentAnimation.Length;
            int frame = (int)Math.Floor(CurrentAnimation.Frames.Length * percent);
            cachedFrame = frame >= CurrentAnimation.Frames.Length ? CurrentAnimation.Frames[CurrentAnimation.Frames.Length - 1] : CurrentAnimation.Frames[frame];
        }

        public int GetAnimationIndex(string name)
        {
            SpriteModel model = Model as SpriteModel;
            if (model.Animations == null)
                return -1;

            int length = model.Animations.Length;
            for (int i = 0; i < length; ++i)
                if (model.Animations[i].Name == name)
                    return i;

            return -1;
        }
        public override bool HasAnimation(string name)
        {
            return GetAnimationIndex(name) != -1;
        }
        public override bool IsAnimationOneshot(string name)
        {
            int index = GetAnimationIndex(name);
            if (index == -1)
                return false;
            return (Model as SpriteModel).Animations[index].OneShot;
        }
        public override void PlayAnimation(string name)
        {
            if (CurrentAnimation != null && CurrentAnimation.Name == name)
                return;
            if (name == null)
            {
                if ((Model as SpriteModel).Animations != null)
                    PlayAnimation((Model as SpriteModel).Animations[0].Name);
                return;
            }
            string currentAnimationName = CurrentAnimationName;
            base.PlayAnimation(name);

            int index = GetAnimationIndex(name);
            if (index == -1)
                return;

            SpriteModel model = Model as SpriteModel;
            if (model.Animations[index].OneShot)
                NextAnimationName = currentAnimationName;
            CurrentAnimation = model.Animations[index];
            TimePassed = 0;
            cachedFrame = CurrentAnimation.Frames[0];
        }

        public int GetFrameTexture()
        {
            return CurrentAnimation == null ? (Model as SpriteModel).DefaultTextureID : cachedFrame;
        }
    }
}