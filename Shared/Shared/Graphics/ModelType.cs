﻿namespace Arcanos.Shared.Graphics
{
    public enum ModelType
    {
        EmptyModel,
        SpriteModel,
        MultiAngleSpriteModel,
        WorldModel,
        DecalModel,
    }
}