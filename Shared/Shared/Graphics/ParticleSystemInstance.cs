﻿using System.Collections.Generic;

namespace Arcanos.Shared.Graphics
{
    public class ParticleSystemInstance
    {
        public readonly ParticleSystem Template;
        public readonly List<ParticleEmitterInstance> Emitters = new List<ParticleEmitterInstance>();

        public bool IsActive
        {
            get { return activeEmitters > 0; }
        }
        private int activeEmitters;

        public ParticleSystemInstance(ParticleSystem template)
        {
            Template = template;
            foreach (ParticleEmitter emitter in Template.Emitters)
                Emitters.Add(new ParticleEmitterInstance(emitter));
        }

        public void Update(ModelInstance model)
        {
            activeEmitters = 0;
            foreach (ParticleEmitterInstance emitter in Emitters)
            {
                emitter.Position = model.Owner.GetPosition() + emitter.Template.Position;
                emitter.Direction = model.Owner.GetRotation();
                emitter.Template.UpdateEmitter(emitter);
                if (emitter.IsActive)
                    ++activeEmitters;
            }
        }

        public void Stop()
        {
            foreach (ParticleEmitterInstance emitter in Emitters)
                emitter.Stop();
        }
    }
}