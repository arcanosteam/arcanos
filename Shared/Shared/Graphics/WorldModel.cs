﻿using System;
using Arcanos.Shared.Objects;
using Microsoft.Xna.Framework;

namespace Arcanos.Shared.Graphics
{
    [Flags]
    public enum WorldModelFlags : byte
    {
        SwapVertexYZ = 0x01,
        FlipTextureV = 0x02,
        InvertNormals = 0x04,
        DoubleFaced = 0x08,
    }
    public class WorldModel : Model
    {
        public override EditorEntry EditorEntry
        {
            get { return new EditorEntry(ID, string.Format("{0}{1}{2}{3}", Filename, RelativeSize ? " [REL]" : "", ParticleSystem == null ? "" : " [PS]", WindInfluenceZ == 0 ? "" : " [WIND]")); }
        }

        public Vector3 Size;
        public string Filename;
        public int[] Materials;
        public WorldModelFlags Flags;
        public float WindInfluenceZ;

        public WorldModel() : base(ModelType.WorldModel) { }

        public override ModelInstance CreateInstance(IWorldEntity owner)
        {
            return new WorldModelInstance(this, owner);
        }
    }
}