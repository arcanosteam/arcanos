﻿using Arcanos.Shared.Objects;

namespace Arcanos.Shared.Graphics
{
    public class WorldModelInstance : ModelInstance
    {
        public WorldModelInstance(WorldModel model, IWorldEntity owner) : base(model, owner) { }

        public override bool HasAnimation(string name)
        {
            return false;
        }
        public override bool IsAnimationOneshot(string name)
        {
            return false;
        }
    }
}