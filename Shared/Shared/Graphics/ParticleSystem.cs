﻿using System;
using System.Collections.Generic;
using Arcanos.Shared.Data;
using Arcanos.Shared.Objects;

namespace Arcanos.Shared.Graphics
{
    [SerializationSettings(Method = SerializationMethod.RefByID, ReferenceResolverType = typeof(ParticleSystem), ReferenceResolverMethod = "ReferenceResolver")]
    public class ParticleSystem : IReferencedByID, IEditableEntry
    {
        public static Func<int, ParticleSystem> LoadHandler;

        [SerializationIgnore]
        public int ID { get; set; }
        [SerializationIgnore]
        public EditorEntry EditorEntry
        {
            get { return new EditorEntry(ID, string.Format("{0} emitters", Emitters.Count)); }
        }

        public List<ParticleEmitter> Emitters = new List<ParticleEmitter>();

        internal static object ReferenceResolver(int id)
        {
            ParticleSystem data = LoadHandler(id);
            data.ID = id;
            return data;
        }
    }
}