﻿using Arcanos.Shared.Objects;

namespace Arcanos.Shared.Graphics
{
    public class EmptyModel : Model
    {
        public EmptyModel() : base(ModelType.EmptyModel) { }

        public override ModelInstance CreateInstance(IWorldEntity owner)
        {
            return new EmptyModelInstance(this, owner);
        }
    }
}