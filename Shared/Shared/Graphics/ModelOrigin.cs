﻿namespace Arcanos.Shared.Graphics
{
    public enum ModelOrigin : byte
    {
        Center,
        Bottom,
        Top,

        Max,
    }
}