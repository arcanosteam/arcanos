﻿using System;
using Arcanos.Shared.Timing;
using Microsoft.Xna.Framework;

namespace Arcanos.Shared.Graphics
{
    public class ParticleEmitter
    {
        public Vector3 Position;

        // Visual
        public int[] ParticleTextures;
        public FloatBounds ParticleSize;
        public float SizeMultiplier;
        // Spawn
        public ParticleEmitterSourcePosition SourcePosition;
        public FloatBounds SourcePositionX, SourcePositionY, SourcePositionZ;
        public FloatBounds SpawnDelay;
        // Motion
        public FloatBounds DirectionYaw, DirectionPitch;
        public FloatBounds Velocity;
        public float VelocityMultiplier;
        public float GravityInfluence;
        // Lifetime
        public IntBounds TotalParticleCount;
        public FloatBounds TimeToLive;
        public int ParticleLimit;

        private Vector3 GetParticleSpawnPosition(ParticleEmitterInstance emitter)
        {
            switch (SourcePosition)
            {
                case ParticleEmitterSourcePosition.EmitterPosition:
                    return emitter.Position + new Vector3(SourcePositionX.Random, SourcePositionY.Random, SourcePositionZ.Random);
                case ParticleEmitterSourcePosition.RandomInRadius:
                {
                    float radius = SourcePositionX.Random;
                    float direction = emitter.Direction.Yaw + SourcePositionY.Random;
                    return emitter.Position + new Vector3((float)Math.Cos(direction) * radius, (float)Math.Sin(direction) * radius, SourcePositionZ.Random);
                }
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void UpdateEmitter(ParticleEmitterInstance emitter)
        {
            if (!emitter.Stopped && (TotalParticleCount.Empty || !TotalParticleCount.Empty && emitter.ParticleSpawnsLeft > 0))
            {
                emitter.NextSpawnDelay -= Time.PerSecond;
                while (emitter.NextSpawnDelay <= 0)
                {
                    emitter.NextSpawnDelay += SpawnDelay.Random;

                    int index = emitter.GetUnusedIndex();
                    if (index == -1)
                        break;

                    float vel = Velocity.Random;
                    float dy = DirectionYaw.Random;
                    float dp = DirectionPitch.Random;
                    Vector3 dir = new Vector3((float)Math.Cos(dy) * (float)Math.Cos(dp) * vel, (float)Math.Sin(dy) * (float)Math.Cos(dp) * vel, (float)Math.Sin(dp) * vel);
                    emitter.Particles[index] = new ParticleInstance(GetParticleSpawnPosition(emitter), dir, ParticleSize.Random, R.Array(ParticleTextures), TimeToLive.Random);
                    --emitter.ParticleSpawnsLeft;
                    ++emitter.ActiveParticles;
                }
            }

            bool updated = false;
            for (int i = 0; i < ParticleLimit; ++i)
            {
                if (emitter.Particles[i].TimeToLive <= 0)
                    continue;
                UpdateParticle(ref emitter.Particles[i]);
                if (!updated)
                    updated = true;
                if (emitter.Particles[i].TimeToLive <= 0)
                {
                    emitter.UnuseIndex(i);
                    --emitter.ActiveParticles;
                }
            }
            if (!updated)
                emitter.ActiveParticles = 0;
        }

        private void UpdateParticle(ref ParticleInstance particle)
        {
            particle.Position += particle.Velocity * Time.PerSecond;
            particle.Velocity *= 1 - (1 - VelocityMultiplier) * Time.PerSecond;
            particle.Velocity.Z += GravityInfluence * Time.PerSecond;
            particle.Size *= 1 - (1 - SizeMultiplier) * Time.PerSecond;
            particle.TimeToLive -= Time.PerSecond;
        }
    }
}