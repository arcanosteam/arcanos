﻿using Microsoft.Xna.Framework;

namespace Arcanos.Shared.Graphics
{
    public interface IRenderGeometry<T>
    {
        void Add(T texture, Vector3 aPos, Vector3 bPos, Vector3 cPos, Vector3 aNorm, Vector3 bNorm, Vector3 cNorm, Vector2 aTex, Vector2 bTex, Vector2 cTex);
    }
}