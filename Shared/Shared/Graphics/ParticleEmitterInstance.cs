﻿using Microsoft.Xna.Framework;

namespace Arcanos.Shared.Graphics
{
    public class ParticleEmitterInstance
    {
        private const int FREED_INDEXES_COUNT = 5;

        public readonly ParticleEmitter Template;

        public Vector3 Position;
        public Rotation Direction;

        public bool IsActive
        {
            get { return ActiveParticles > 0; }
        }
        public bool Stopped;
        public int ActiveParticles;
        public float NextSpawnDelay;
        public int ParticleSpawnsLeft;
        public readonly ParticleInstance[] Particles;
        private readonly int[] lastFreedIndexes = new int[FREED_INDEXES_COUNT];

        public ParticleEmitterInstance(ParticleEmitter template)
        {
            Template = template;
            Particles = new ParticleInstance[Template.ParticleLimit];
            ParticleSpawnsLeft = template.TotalParticleCount.Random;
            for (int i = 0; i < FREED_INDEXES_COUNT; ++i)
                lastFreedIndexes[i] = -1;
        }

        public int GetUnusedIndex()
        {
            for (int i = 0; i < FREED_INDEXES_COUNT; ++i)
                if (lastFreedIndexes[i] != -1)
                {
                    int index = lastFreedIndexes[i];
                    lastFreedIndexes[i] = -1;
                    return index;
                }
            for (int i = 0; i < Template.ParticleLimit; ++i)
                if (Particles[i].TimeToLive <= 0)
                    return i;
            return -1;
        }
        public void UnuseIndex(int index)
        {
            for (int i = 0; i < FREED_INDEXES_COUNT; ++i)
                if (lastFreedIndexes[i] == index)
                    return;
            for (int i = 0; i < FREED_INDEXES_COUNT; ++i)
                if (lastFreedIndexes[i] == -1)
                    lastFreedIndexes[i] = index;
        }

        public void Stop()
        {
            Stopped = true;
        }
    }
}