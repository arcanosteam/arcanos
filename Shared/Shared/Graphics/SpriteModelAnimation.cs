﻿namespace Arcanos.Shared.Graphics
{
    public class SpriteModelAnimation : Animation
    {
        public int[] Frames;

        internal SpriteModelAnimation() { }
        public SpriteModelAnimation(string name, float length, params int[] frames)
        {
            Name = name;
            Length = length;
            Frames = frames;
        }
    }
}