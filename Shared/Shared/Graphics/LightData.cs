﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Arcanos.Shared.Graphics
{
    public struct LightData
    {
        public readonly bool Enabled;
        public readonly Vector4 Color;
        public readonly float Radius;

        public LightData(Vector4 color, float radius)
        {
            Enabled = true;
            Color = color;
            Radius = radius;
        }
        public LightData(Vector3 color, float intensity, float radius)
        {
            Enabled = true;
            Color = new Vector4(color, intensity);
            Radius = radius;
        }
        public LightData(Color color, float radius)
        {
            Enabled = true;
            Color = color.ToVector4();
            Radius = radius;
        }
        public LightData(Color color, float intensity, float radius)
        {
            Enabled = true;
            Color = color.ToVector4();
            Color.W = intensity;
            Radius = radius;
        }
    }
}