﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;

namespace Arcanos.Shared.Graphics
{
    public interface IRenderable<T, TContext> where TContext : IRenderGeometry<T>
    {
        IEnumerable<TContext> GetRenderContext(GraphicsDevice device);
    }
}