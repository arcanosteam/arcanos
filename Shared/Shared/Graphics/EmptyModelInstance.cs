﻿using Arcanos.Shared.Objects;

namespace Arcanos.Shared.Graphics
{
    public class EmptyModelInstance : ModelInstance
    {
        public EmptyModelInstance(EmptyModel model, IWorldEntity owner) : base(model, owner) { }

        public override bool HasAnimation(string name)
        {
            return false;
        }
        public override bool IsAnimationOneshot(string name)
        {
            return false;
        }
    }
}