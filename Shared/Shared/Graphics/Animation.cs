﻿namespace Arcanos.Shared.Graphics
{
    public abstract class Animation
    {
        public string Name;
        public float Length;
        public bool OneShot;
    }
}