﻿using Arcanos.Shared.Objects;
using Microsoft.Xna.Framework;

namespace Arcanos.Shared.Graphics
{
    public class SpriteModel : Model
    {
        public int DefaultTextureID;
        public SpriteModelAnimation[] Animations;
        public Vector3 Size;

        public SpriteModel() : base(ModelType.SpriteModel) { }

        public override ModelInstance CreateInstance(IWorldEntity owner)
        {
            return new SpriteModelInstance(this, owner);
        }
    }
}