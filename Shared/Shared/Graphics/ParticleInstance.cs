﻿using Microsoft.Xna.Framework;

namespace Arcanos.Shared.Graphics
{
    public struct ParticleInstance
    {
        public Vector3 Position;
        public Vector3 Velocity;
        public float Size;
        public int Texture;
        public float TimeToLive;

        public ParticleInstance(Vector3 pos, Vector3 vel, float size, int texture, float ttl)
        {
            Position = pos;
            Velocity = vel;
            Size = size;
            Texture = texture;
            TimeToLive = ttl;
        }
    }
}