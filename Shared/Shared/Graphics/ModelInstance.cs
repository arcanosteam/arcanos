﻿using System;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Timing;

namespace Arcanos.Shared.Graphics
{
    public abstract class ModelInstance
    {
        public static event Action<ModelInstance> RegisterLight;
        public static event Action<ModelInstance> UnregisterLight;

        public readonly Model Model;
        public readonly IWorldEntity Owner;
        public readonly ParticleSystemInstance ParticleSystem;
        public ModelOrigin Origin;
        public string CurrentAnimationName;
        protected float TimePassed;
        protected string CurrentLoopedAnimationName;
        protected string NextAnimationName;
        protected float TimeAlive;

        public bool IsActive
        {
            get
            {
                if (Model.TimeAlive != 0 && TimeAlive < Model.TimeAlive)
                    return true;
                return ParticleSystem != null && ParticleSystem.IsActive;
            }
        }

        protected ModelInstance(Model model, IWorldEntity owner)
        {
            Model = model;
            Owner = owner;
            Origin = Model.Origin;
            if (Model.ParticleSystem != null)
                ParticleSystem = new ParticleSystemInstance(Model.ParticleSystem);
            if (Model.Light.Enabled)
                RegisterLight(this);
        }

        public virtual void Update()
        {
            TimeAlive += Time.PerSecond;
            UpdateParticleSystem();
        }
        public virtual void Destroy()
        {
            if (Model.Light.Enabled)
                UnregisterLight(this);
        }

        public void RequestStop()
        {
            if (ParticleSystem != null)
                ParticleSystem.Stop();
        }

        protected void UpdateParticleSystem()
        {
            if (ParticleSystem == null)
                return;

            ParticleSystem.Update(this);
        }

        public abstract bool HasAnimation(string name);
        public abstract bool IsAnimationOneshot(string name);
        public virtual void PlayAnimation(string name)
        {
            if (!HasAnimation(name))
                return;
            CurrentAnimationName = name;
            if (!IsAnimationOneshot(name))
                CurrentLoopedAnimationName = name;
            NextAnimationName = null;
        }
        public void PlayAnimationOnce(string name)
        {
            PlayAnimation(name, CurrentLoopedAnimationName);
        }
        public void PlayAnimation(string name, string nextName)
        {
            string temp = CurrentLoopedAnimationName;
            PlayAnimation(name);
            CurrentLoopedAnimationName = temp;
            NextAnimationName = nextName;
        }
        public void StopAnimation(string name)
        {
            if (CurrentAnimationName == name)
                PlayAnimation(NextAnimationName);
        }
    }
}