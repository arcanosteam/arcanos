﻿using Arcanos.Shared.Objects;
using Microsoft.Xna.Framework;

namespace Arcanos.Shared.Graphics
{
    public class MultiAngleSpriteModel : Model
    {
        public int DefaultTextureID;
        public MultiAngleSpriteModelAnimation[] Animations;
        public Vector3 Size;

        public MultiAngleSpriteModel() : base(ModelType.MultiAngleSpriteModel) { }

        public override ModelInstance CreateInstance(IWorldEntity owner)
        {
            return new MultiAngleSpriteModelInstance(this, owner);
        }
    }
}