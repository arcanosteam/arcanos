﻿using System;
using Arcanos.Shared.Data;
using Arcanos.Shared.Objects;

namespace Arcanos.Shared.Graphics
{
    [SerializationSettings(Method = SerializationMethod.RefByID, ReferenceResolverType = typeof(TextureInfo), ReferenceResolverMethod = "ReferenceResolver")]
    public class TextureInfo : IReferencedByID, IEditableEntry
    {
        public static Func<int, TextureInfo> LoadHandler;

        [SerializationIgnore]
        public int ID { get; set; }
        [SerializationIgnore]
        public EditorEntry EditorEntry
        {
            get { return new EditorEntry(ID, Filename); }
        }

        public string Filename;

        internal TextureInfo() { }
        public TextureInfo(string filename)
        {
            Filename = filename;
        }

        internal static object ReferenceResolver(int id)
        {
            TextureInfo data = LoadHandler(id);
            data.ID = id;
            return data;
        }
    }
}