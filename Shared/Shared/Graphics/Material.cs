﻿using System;
using Arcanos.Shared.Data;
using Arcanos.Shared.Objects;

namespace Arcanos.Shared.Graphics
{
    [SerializationSettings(Method = SerializationMethod.RefByID, ReferenceResolverType = typeof(Material), ReferenceResolverMethod = "ReferenceResolver")]
    public class Material : IReferencedByID, IEditableEntry
    {
        public static Func<int, Material> LoadHandler;

        [SerializationIgnore]
        public int ID { get; set; }
        [SerializationIgnore]
        public EditorEntry EditorEntry
        {
            get { return new EditorEntry(ID, string.Format("Texture: {0}{1}", TextureID, NormalMapID == 0 ? "" : string.Format(" NormalMap: {0}", NormalMapID))); }
        }

        public int TextureID;
        public int NormalMapID;

        protected Material() { }
        public Material(int textureID)
        {
            TextureID = textureID;
        }
        public Material(int textureID, int normalMapID) : this(textureID)
        {
            NormalMapID = normalMapID;
        }

        internal static object ReferenceResolver(int id)
        {
            Material data = LoadHandler(id);
            data.ID = id;
            return data;
        }
    }
}