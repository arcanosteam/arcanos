﻿using System;
using Arcanos.Shared.Data;
using Arcanos.Shared.Objects;

namespace Arcanos.Shared.Graphics
{
    [SerializationSettings(Method = SerializationMethod.RefByID, ReferenceResolverType = typeof(Model), ReferenceResolverMethod = "ReferenceResolver")]
    public abstract class Model : IReferencedByID, IEditableEntry
    {
        public static Func<int, Model> LoadHandler;

        [SerializationIgnore]
        public int ID { get; set; }
        [SerializationIgnore]
        public virtual EditorEntry EditorEntry
        {
            get { return new EditorEntry(ID, string.Format("{0}{1}{2}", Type, RelativeSize ? " [REL]" : "", ParticleSystem == null ? "" : " [PS]")); }
        }

        public readonly ModelType Type;
        public ParticleSystem ParticleSystem;
        public bool RelativeSize;
        public ModelOrigin Origin;
        public LightData Light;
        public float TimeAlive;

        protected Model(ModelType type)
        {
            Type = type;
        }

        internal static object ReferenceResolver(int id)
        {
            Model data = LoadHandler(id);
            data.ID = id;
            return data;
        }

        public abstract ModelInstance CreateInstance(IWorldEntity owner);
    }
}