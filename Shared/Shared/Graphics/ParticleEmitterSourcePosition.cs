﻿namespace Arcanos.Shared.Graphics
{
    public enum ParticleEmitterSourcePosition
    {
        /// <summary>
        /// X,Y,Z - Offset from emitter position
        /// </summary>
        EmitterPosition,
        /// <summary>
        /// X - Radius from emitter position
        /// Y - Pie from emitter direction
        /// Z - Z offset from emitter position
        /// </summary>
        RandomInRadius,
    }
}