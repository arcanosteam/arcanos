using System;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Timing;
using Microsoft.Xna.Framework;

namespace Arcanos.Shared.Graphics
{
    public class MultiAngleSpriteModelInstance : ModelInstance
    {
        public MultiAngleSpriteModelAnimation CurrentAnimation;
        private int[] cachedFrame;

        public MultiAngleSpriteModelInstance(MultiAngleSpriteModel model, IWorldEntity owner) : base(model, owner)
        {
            if (model.Animations != null && model.Animations.Length > 0)
                PlayAnimation(model.Animations[0].Name);
        }

        public override void Update()
        {
            base.Update();

            if (CurrentAnimation == null)
                return;

            TimePassed += Time.PerSecond;

            if (CurrentAnimation.Length == 0)
            {
                cachedFrame = CurrentAnimation.Frames[0];
                return;
            }

            if (TimePassed >= CurrentAnimation.Length)
            {
                TimePassed -= CurrentAnimation.Length;
                if (NextAnimationName != null)
                {
                    PlayAnimation(NextAnimationName);
                    if (CurrentAnimation.Length == 0)
                    {
                        cachedFrame = CurrentAnimation.Frames[0];
                        return;
                    }
                }
            }

            float percent = TimePassed / CurrentAnimation.Length;
            int frame = (int)Math.Floor(CurrentAnimation.Frames.Length * percent);
            cachedFrame = frame >= CurrentAnimation.Frames.Length ? CurrentAnimation.Frames[CurrentAnimation.Frames.Length - 1] : CurrentAnimation.Frames[frame];
        }

        public int GetAnimationIndex(string name)
        {
            MultiAngleSpriteModel model = Model as MultiAngleSpriteModel;
            if (model.Animations == null)
                return -1;

            int length = model.Animations.Length;
            for (int i = 0; i < length; ++i)
                if (model.Animations[i].Name == name)
                    return i;

            return -1;
        }
        public override bool HasAnimation(string name)
        {
            return GetAnimationIndex(name) != -1;
        }
        public override bool IsAnimationOneshot(string name)
        {
            int index = GetAnimationIndex(name);
            if (index == -1)
                return false;
            return (Model as MultiAngleSpriteModel).Animations[index].OneShot;
        }
        public override void PlayAnimation(string name)
        {
            if (CurrentAnimation != null && CurrentAnimation.Name == name)
                return;
            if (name == null)
            {
                if ((Model as MultiAngleSpriteModel).Animations != null)
                    PlayAnimation((Model as MultiAngleSpriteModel).Animations[0].Name);
                return;
            }
            string currentAnimationName = CurrentAnimationName;
            base.PlayAnimation(name);

            int index = GetAnimationIndex(name);
            if (index == -1)
                return;

            MultiAngleSpriteModel model = Model as MultiAngleSpriteModel;
            if (model.Animations[index].OneShot)
                NextAnimationName = currentAnimationName;
            CurrentAnimation = model.Animations[index];
            TimePassed = 0;
            cachedFrame = CurrentAnimation.Frames[0];
        }

        public int GetFrameTexture(float angle, out bool flipX)
        {
            if (CurrentAnimation == null)
            {
                flipX = false;
                return (Model as MultiAngleSpriteModel).DefaultTextureID;
            }
            int angles = cachedFrame.Length - 1;
            angle = 180 * (angle / MathHelper.Pi);
            if (angle > 0)
                angle = ((angle + 180) % 360) - 180;
            else
                angle = ((angle - 180) % 360) + 180;
            flipX = angle < 0;
            angle = Math.Abs(angle);
            int frame = (int)Math.Round((angle / 180) * angles);
            if (frame >= cachedFrame.Length)
                frame -= cachedFrame.Length;
            if (frame == 0 || frame == cachedFrame.Length - 1)
                flipX = false;
            return cachedFrame[frame];
        }
    }
}