﻿namespace Arcanos.Shared.Graphics
{
    public class MultiAngleSpriteModelAnimation : Animation
    {
        public readonly int[][] Frames;

        internal MultiAngleSpriteModelAnimation() { }
        public MultiAngleSpriteModelAnimation(string name, float length, params int[][] frames)
        {
            Name = name;
            Length = length;
            Frames = frames;
        }
    }
}