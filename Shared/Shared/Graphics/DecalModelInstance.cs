﻿using System;
using Arcanos.Shared.Objects;
using Microsoft.Xna.Framework;

namespace Arcanos.Shared.Graphics
{
    public class DecalModelInstance : ModelInstance
    {
        public static event Func<DecalModelInstance, int> RegisterDecal;
        public static event Action<int, Vector3> MoveDecal;
        public static event Action<int> UnregisterDecal;

        private readonly int decalIndex = -1;
        private Vector3 prevPosition;

        public DecalModelInstance(DecalModel model, IWorldEntity owner) : base(model, owner)
        {
            decalIndex = RegisterDecal(this);
            prevPosition = owner.GetPosition();
        }

        public override void Update()
        {
            base.Update();
            Vector3 position = Owner.GetPosition();
            if (position != prevPosition)
                MoveDecal(decalIndex, prevPosition = position);
        }
        public override void Destroy()
        {
            base.Destroy();
            if (decalIndex != -1)
                UnregisterDecal(decalIndex);
        }

        public override bool HasAnimation(string name)
        {
            return false;
        }
        public override bool IsAnimationOneshot(string name)
        {
            return false;
        }
    }
}