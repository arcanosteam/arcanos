﻿using Arcanos.Shared.Objects;

namespace Arcanos.Shared.Graphics
{
    public class DecalModel : Model
    {
        public override EditorEntry EditorEntry
        {
            get { return new EditorEntry(ID, string.Format("Decal: {0} Size: {1}{2}{3}", TextureID, Size, RelativeSize ? " [REL]" : "", ParticleSystem == null ? "" : " [PS]")); }
        }

        public float Size;
        public int TextureID;

        public DecalModel() : base(ModelType.DecalModel) { }

        public override ModelInstance CreateInstance(IWorldEntity owner)
        {
            return new DecalModelInstance(this, owner);
        }
    }
}