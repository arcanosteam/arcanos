﻿namespace Arcanos.Shared.Spells
{
    public enum SpellCategory
    {
        None,
        Melee,
        Ability,
        /// <summary>
        /// Affected by silence.
        /// </summary>
        Magic,
        Mind,
    }
}