﻿namespace Arcanos.Shared.Spells
{
    public enum SpellEffectTarget : byte
    {
        None,
        Caster,
        CasterTarget,
        SpellTarget,
        AllAOE,
        HostilesAOE,
        NonFriendsAOE,
        FriendsAOE,
        ByID,
        ByGUID,
        Coordinates,
        MapCoordinates,
        GroundTarget,

        Max,
    }
}