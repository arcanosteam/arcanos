﻿namespace Arcanos.Shared.Spells
{
    public enum AuraType : ushort
    {
        None,

        Dummy,
        /// <summary>
        /// IV1: Spell ID<br/>
        /// TSP: Period
        /// </summary>
        PeriodicTriggerSpell,
        /// <summary>
        /// IV1: Spell ID<br/>
        /// TSP: Period
        /// </summary>
        PeriodicTriggerSpellFromCaster,

        /// <summary>
        /// FV1: Speed factor
        /// </summary>
        ModWalkSpeed,
        /// <summary>
        /// FV1: Speed factor
        /// </summary>
        ModRunSpeed,
        /// <summary>
        /// FV1: Speed factor
        /// </summary>
        ModSwimSpeed,
        /// <summary>
        /// FV1: Speed factor
        /// </summary>
        ModFlySpeed,
        /// <summary>
        /// IV1: Stat<br/>
        /// IV2: Modifier
        /// </summary>
        ModStat,
        /// <summary>
        /// IV1: AttackType Mask (0: Full)<br/>
        /// IV2: Modifier
        /// </summary>
        ModWeaponDamage,
        /// <summary>
        /// IV1: AttackType Mask (0: Full)<br/>
        /// FV1: Percent Modifier
        /// </summary>
        ModWeaponDamagePercent,
        /// <summary>
        /// IV1: AttackType Mask (0: Full)<br/>
        /// FV1: Percent Modifier
        /// </summary>
        ModWeaponSpeedPercent,
        /// <summary>
        /// IV1: Regen Min<br/>
        /// IV2: Regen Max (0: =IV1)
        /// </summary>
        ModHealthRegenRaw,
        /// <summary>
        /// FV1: Percent Modifier
        /// </summary>
        ModHealthRegenPct,
        /// <summary>
        /// IV1: PowerType Type<br/>
        /// IV2: Regen Min<br/>
        /// IV3: Regen Max (0: =IV2)
        /// </summary>
        ModPowerRegenRaw,
        /// <summary>
        /// IV1: PowerType Type<br/>
        /// FV1: Percent Modifier
        /// </summary>
        ModPowerRegenPct,
        /// <summary>
        /// IV1: DamageSchool Mask (0: Full)<br/>
        /// IV2: Bonus Damage Min<br/>
        /// IV3: Bonus Damage Max (0: =IV2)
        /// </summary>
        ModDamageDoneRaw,
        /// <summary>
        /// IV1: DamageSchool Mask (0: Full)<br/>
        /// FV1: Percent Modifier
        /// </summary>
        ModDamageDonePct,
        /// <summary>
        /// IV1: DamageSchool Mask (0: Full)<br/>
        /// IV2: Bonus Damage Min<br/>
        /// IV3: Bonus Damage Max (0: =IV2)
        /// </summary>
        ModDamageTakenRaw,
        /// <summary>
        /// IV1: DamageSchool Mask (0: Full)<br/>
        /// FV1: Percent Modifier
        /// </summary>
        ModDamageTakenPct,
        /// <summary>
        /// IV1: Bonus Heal Min<br/>
        /// IV2: Bonus Heal Max (0: =IV1)
        /// </summary>
        ModHealingDoneRaw,
        /// <summary>
        /// FV1: Percent Modifier
        /// </summary>
        ModHealingDonePct,
        /// <summary>
        /// IV1: Bonus Heal Min<br/>
        /// IV2: Bonus Heal Max (0: =IV1)
        /// </summary>
        ModHealingTakenRaw,
        /// <summary>
        /// FV1: Percent Modifier
        /// </summary>
        ModHealingTakenPct,

        /// <summary>
        /// IV1: Spell ID<br/>
        /// FV1: Proc Chance<br/>
        /// </summary>
        MeleeHitProc,
        /// <summary>
        /// IV1: Spell ID<br/>
        /// FV1: Proc Chance<br/>
        /// </summary>
        MeleeMissProc,

        Stun,
        Root,
        Silence,
        /// <summary>
        /// FV1: Miss chance
        /// </summary>
        Evasion,
        /// <summary>
        /// FV1: Chance not to be detected
        /// </summary>
        Invisibility,

        Max,
    }
}