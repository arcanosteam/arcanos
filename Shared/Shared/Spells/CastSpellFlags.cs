﻿using System;

namespace Arcanos.Shared.Spells
{
    [Flags]
    public enum CastSpellFlags : ushort
    {
        None                    = 0x0000,

        IgnorePowerCheck        = 0x0001,
        IgnoreTargetCheck       = 0x0002,
        IgnoreCastingTime       = 0x0004,
        IgnoreCooldown          = 0x0008,
        IgnoreAlreadyCasting    = 0x0010,
        IgnoreUnknownSpell      = 0x0020,
        IgnoreCombatCheck       = 0x0040,
        IgnoreOwnerState        = 0x0080,

        IgnoreAllChecks         = 0xFFFF,

        ItemSpellIgnores        = IgnoreUnknownSpell | IgnorePowerCheck,
        TriggerSpellIgnores     = IgnoreAllChecks,
        ProcSpellIgnores        = IgnoreAlreadyCasting | IgnoreUnknownSpell | IgnoreOwnerState,
    }
}