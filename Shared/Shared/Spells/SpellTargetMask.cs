﻿using System;

namespace Arcanos.Shared.Spells
{
    [Flags]
    public enum SpellTargetMask : byte
    {
        Self        = 0x01,
        Hostile     = 0x02,
        Neutral     = 0x04,
        Friendly    = 0x08,

        Dead        = 0x10,

        All         = 0xFF,
        AllAlive    = All & ~Dead,
        Attackable  = Neutral | Hostile,
    }
}