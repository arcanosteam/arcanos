﻿using System;
using Arcanos.Shared.Data;
using Arcanos.Shared.Graphics;
using Arcanos.Shared.Objects;

namespace Arcanos.Shared.Spells
{
    public struct SpellVisualSet
    {
        public Model Model;
        public string ModelAnimation;
        public string CasterAnimation;
        public string TargetAnimation;
    }
    [SerializationSettings(Method = SerializationMethod.RefByID, ReferenceResolverType = typeof(SpellVisual), ReferenceResolverMethod = "ReferenceResolver")]
    public class SpellVisual : IReferencedByID, IEditableEntry
    {
        public static Func<int, SpellVisual> LoadHandler;

        [SerializationIgnore]
        public int ID { get; set; }
        [SerializationIgnore]
        public EditorEntry EditorEntry
        {
            get { return new EditorEntry(ID, ""); }
        }

        public SpellVisualSet Casting = new SpellVisualSet { CasterAnimation = "SpellCast" };
        public SpellVisualSet CastSuccess = new SpellVisualSet { CasterAnimation = "SpellCastSuccess" };
        public SpellVisualSet CastFail = new SpellVisualSet { CasterAnimation = "SpellCastFail" };
        public SpellVisualSet Channel = new SpellVisualSet { CasterAnimation = "SpellChannel" };
        public SpellVisualSet Flying;
        public SpellVisualSet Hit;
        public SpellVisualSet Miss;
        public SpellVisualSet GroundStart;
        public SpellVisualSet GroundIdle;
        public SpellVisualSet GroundTrigger;
        public SpellVisualSet GroundEnd;

        internal static object ReferenceResolver(int id)
        {
            return LoadHandler(id);
        }
    }
}