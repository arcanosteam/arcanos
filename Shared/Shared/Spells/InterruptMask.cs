﻿using System;

namespace Arcanos.Shared.Spells
{
    [Flags]
    public enum InterruptMask : uint
    {
        None                    = 0x00000000,

        Moving                  = 0x00000001,
        Rotating                = 0x00000002,
        Jumping                 = 0x00000004,
        TakingDamage            = 0x00000008,
        InflictingDamage        = 0x00000010,
        BeingHitInMelee         = 0x00000020,
        BeingMissedInMelee      = 0x00000040,
        HittingInMelee          = 0x00000080,
        MissingInMelee          = 0x00000100,
        StartingCastingSpell    = 0x00000200,
        FailingCastingSpell     = 0x00000400,
        CastingSpell            = 0x00000800,
        BeingHitBySpell         = 0x00001000,
        BeingMissedBySpell      = 0x00002000,
        HittingWithSpell        = 0x00004000,
        MissingWithSpell        = 0x00008000,
        EnteringCombat          = 0x00010000,
        ExitingCombat           = 0x00020000,
        Dying                   = 0x00040000,
        Manual                  = 0x00080000,

        DefaultCasting          = Manual | Moving,
        DefaultAura             = Manual,
        DefaultChannel          = Manual,
        All                     = 0xFFFFFFFF,
    }
}