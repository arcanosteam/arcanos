﻿using Arcanos.Shared.Objects;
using Arcanos.Shared.Timing;
using Arcanos.Shared.World;
using Microsoft.Xna.Framework;

namespace Arcanos.Shared.Spells
{
    public abstract class SpellBase : IWorldEntity
    {
        protected readonly SpellTemplateBase template;
        public SpellTemplateBase Template { get { return template; } }

        public readonly CreatureBase Caster;
        public readonly CreatureBase ExplicitTarget;
        public readonly Vector3 GroundTarget;
        public readonly MapBase Map;
        public Vector3 Position;

        protected SpellBase(SpellTemplateBase template, MapBase map, Vector3 pos)
        {
            this.template = template;
            Map = map;
            Position = pos;
            Map.Add(this);
        }
        protected SpellBase(SpellTemplateBase template, CreatureBase caster, CreatureBase target)
        {
            this.template = template;
            Caster = caster;
            ExplicitTarget = target;
            Map = Caster.Map;
            Position = Caster.Position + Caster.GetSpellCastOffset();

            if (Map != null)
                Map.Add(this);

            if (caster == target || Template.Speed == 0)
                Hit();
        }
        protected SpellBase(SpellTemplateBase template, CreatureBase caster, Vector3 target)
        {
            this.template = template;
            Caster = caster;
            GroundTarget = target;
            Map = Caster.Map;
            Position = Caster.Position;

            Map.Add(this);

            if (Template.Speed == 0)
                Hit();
        }

        public virtual void Update()
        {
            Vector3 destination = ExplicitTarget == null ? GroundTarget : ExplicitTarget.Position + ExplicitTarget.GetSpellHitOffset();

            float dist;
            Vector3.Distance(ref Position, ref destination, out dist);

            float speed = template.Speed * Time.PerSecond;

            if (speed >= dist)
            {
                Hit();
                return;
            }

            Position += Vector3.Normalize(Vector3.Subtract(destination, Position)) * speed;
        }
        public virtual void Destroy() { }

        public virtual void Hit()
        {
            Remove();
        }

        public virtual void Remove()
        {
            if (Map != null)
                Map.Remove(this);
        }

        public Vector3 GetPosition()
        {
            if (Caster != null)
            {
                Vector3 offset, size;
                Caster.GetRenderInfo(out offset, out size);
                return Position + offset;
            }
            return Position;
        }
        public Rotation GetRotation()
        {
            return Position.GetRotationTo(ExplicitTarget == null ? GroundTarget : ExplicitTarget.Position + ExplicitTarget.GetSpellHitOffset());
        }
        public Vector3 GetSize()
        {
            if (Caster != null)
            {
                Vector3 offset, size;
                Caster.GetRenderInfo(out offset, out size);
                return size;
            }
            return Template.Radius == 0 ? Vector3.One : new Vector3(Template.Radius);
        }
    }
}