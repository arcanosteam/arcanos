﻿using System;

namespace Arcanos.Shared.Spells
{
    [Flags]
    public enum SpellFlags : ushort
    {
        GroundTargeted          = 0x0001,
        HiddenInLog             = 0x0002,
        HiddenAura              = 0x0004,
        NotInCombat             = 0x0008,
        OnlyInCombat            = 0x0010,
        IgnoreFacing            = 0x0020,
        AutoTargetSelf          = 0x0040,
        UsableWhileStunned      = 0x0080,
        UsableWhileSilenced     = 0x0100,
    }
}