﻿using System.Collections.Generic;
using Arcanos.Shared.Objects;
using Microsoft.Xna.Framework;

namespace Arcanos.Shared.Spells
{
    public abstract class SpellBookBase
    {
        public readonly CreatureBase Owner;

        public readonly List<SpellTemplateBase> Spells = new List<SpellTemplateBase>();
        public readonly Dictionary<int, CooldownInstance> Cooldowns = new Dictionary<int, CooldownInstance>();
        protected readonly List<AuraBase> AppliedAuras = new List<AuraBase>();
        public float GlobalCooldownTotal;
        public float GlobalCooldown;

        public bool IsCasting
        {
            get { return CastingSpell != null; }
        }
        public SpellTemplateBase CastingSpell;
        public CreatureBase CastingTarget;
        public Vector3 CastingGroundTarget;
        public CastSpellFlags CastingFlags;
        public float CastingTime;

        protected SpellBookBase(CreatureBase owner)
        {
            Owner = owner;
        }

        public abstract void Update();

        public abstract CastSpellResult CastSpell(SpellTemplateBase spell, CreatureBase target, CastSpellFlags flags);
        public abstract CastSpellResult CastSpell(SpellTemplateBase spell, Vector3 target, CastSpellFlags flags);
        public abstract CastSpellResult CanCastSpell(SpellTemplateBase spell, CreatureBase target, CastSpellFlags flags);
        public abstract CastSpellResult CanCastSpell(SpellTemplateBase spell, Vector3 target, CastSpellFlags flags);
        public abstract void Interrupt(InterruptMask interrupt);
        public abstract void InterruptCasting(InterruptMask interrupt);
        public abstract void InterruptAuras(InterruptMask interrupt);
        public abstract CastSpellResult FinishCast(SpellTemplateBase spell, CreatureBase target, CastSpellFlags flags);
        public abstract CastSpellResult FinishCast(SpellTemplateBase spell, Vector3 target, CastSpellFlags flags);
        public abstract CastSpellResult StartCasting(SpellTemplateBase spell, CreatureBase target, CastSpellFlags flags);
        public abstract CastSpellResult StartCasting(SpellTemplateBase spell, Vector3 target, CastSpellFlags flags);
        public abstract void FailCasting(CastSpellResult result);
        public abstract void StopCasting();

        public virtual CastSpellResult CanCastSpell(SpellTemplateBase spell, CastSpellFlags flags)
        {
            if ((flags & CastSpellFlags.IgnoreUnknownSpell) == 0 && !KnowsSpell(spell))
                return CastSpellResult.UnknownSpell;
            if ((flags & CastSpellFlags.IgnoreCombatCheck) == 0 && spell.HasFlag(SpellFlags.NotInCombat) && Owner.IsInCombat)
                return CastSpellResult.NotInCombat;
            if ((flags & CastSpellFlags.IgnoreCombatCheck) == 0 && spell.HasFlag(SpellFlags.OnlyInCombat) && !Owner.IsInCombat)
                return CastSpellResult.OnlyInCombat;
            if ((flags & CastSpellFlags.IgnoreOwnerState) == 0)
            {
                if (Owner.HasFlag(ObjectFlags.Stunned) && !spell.HasFlag(SpellFlags.UsableWhileStunned))
                    return CastSpellResult.Stunned;
                if (Owner.HasFlag(ObjectFlags.Silenced) && !spell.HasFlag(SpellFlags.UsableWhileSilenced))
                    return CastSpellResult.Silenced;
            }
            if ((flags & CastSpellFlags.IgnoreAlreadyCasting) == 0 && IsCasting)
                return CastSpellResult.AlreadyCasting;
            if ((flags & CastSpellFlags.IgnoreCooldown) == 0 && IsOnCooldown(spell))
                return CastSpellResult.OnCooldown;
            if ((flags & CastSpellFlags.IgnorePowerCheck) == 0 && spell.RequiredPowerAmount != 0)
            {
                if (!Owner.UsesPowerType[(byte)spell.RequiredPowerType])
                    return CastSpellResult.CantUsePowerType;
                if (Owner.CurrentPower[(byte)spell.RequiredPowerType] < spell.RequiredPowerAmount)
                    return CastSpellResult.NotEnoughPower;
            }
            return CastSpellResult.Ok;
        }

        public virtual void AddSpell(SpellTemplateBase spell)
        {
            if (KnowsSpell(spell))
                return;

            Spells.Add(spell);
        }
        public virtual void RemoveSpell(SpellTemplateBase spell)
        {
            Spells.Remove(spell);
        }
        public virtual void RemoveAllSpells()
        {
            Spells.Clear();
        }
        public bool KnowsSpell(SpellTemplateBase spell)
        {
            return Spells.Contains(spell);
        }
        public void PutOnCooldown(SpellTemplateBase spell)
        {
            if (spell.GlobalCooldown > 0)
                GlobalCooldownTotal = GlobalCooldown = spell.GlobalCooldown;

            if (spell.CooldownGroup == 0 || spell.CooldownDuration == 0)
                return;

            Cooldowns[spell.CooldownGroup] = new CooldownInstance(spell.CooldownDuration);
        }
        public bool IsOnCooldown(SpellTemplateBase spell)
        {
            if (GlobalCooldown > 0)
                return true;

            if (spell.CooldownGroup != 0)
            {
                CooldownInstance cd;
                if (Cooldowns.TryGetValue(spell.CooldownGroup, out cd))
                    return cd.Left > 0;
            }

            return false;
        }
        public bool IsOnCooldown(SpellTemplateBase spell, out float left, out float total)
        {
            if (spell.CooldownGroup != 0)
            {
                CooldownInstance cd;
                if (Cooldowns.TryGetValue(spell.CooldownGroup, out cd))
                {
                    left = cd.Left;
                    total = cd.Total;
                    return true;
                }
            }

            if (GlobalCooldown > 0)
            {
                left = GlobalCooldown;
                total = spell.GlobalCooldown;
                return true;
            }

            left = 0;
            total = 0;
            return false;
        }

        public void ApplyAura(ulong casterGUID, SpellTemplateBase spell)
        {
            for (int i = 0; i < spell.Effects.Length; i++)
                if (spell.Effects[i].IsAuraEffect)
                    ApplyAura(casterGUID, spell, i);
        }
        public abstract void ApplyAura(ulong casterGUID, SpellTemplateBase spell, int effectID);
        public abstract void ApplyAura(ulong casterGUID, SpellTemplateBase spell, int effectID, float duration);
        public abstract void RemoveAura(AuraBase aura);
        public void RemoveAura(SpellTemplateBase spell)
        {
            foreach (AuraBase aura in AppliedAuras)
                if (aura.Spell == spell && !aura.ToBeRemoved)
                    RemoveAura(aura);
        }
        public void RemoveAura(CreatureBase caster)
        {
            foreach (AuraBase aura in AppliedAuras)
                if (aura.CasterGUID == caster.GUID && !aura.ToBeRemoved)
                    RemoveAura(aura);
        }
        public void RemoveAura(AuraType type)
        {
            foreach (AuraBase aura in AppliedAuras)
                if (aura.Type == type && !aura.ToBeRemoved)
                    RemoveAura(aura);
        }
        public void RemoveAllAuras()
        {
            foreach (AuraBase aura in AppliedAuras)
                if (!aura.ToBeRemoved)
                    RemoveAura(aura);
        }
        public bool HasAura(SpellTemplateBase spell)
        {
            foreach (AuraBase aura in AppliedAuras)
                if (aura.Spell == spell && !aura.ToBeRemoved)
                    return true;
            return false;
        }
        public bool HasAura(CreatureBase caster)
        {
            foreach (AuraBase aura in AppliedAuras)
                if (aura.CasterGUID == caster.GUID && !aura.ToBeRemoved)
                    return true;
            return false;
        }
        public bool HasAura(AuraType type)
        {
            foreach (AuraBase aura in AppliedAuras)
                if (aura.Type == type && !aura.ToBeRemoved)
                    return true;
            return false;
        }
        public IEnumerable<AuraBase> GetAuras(SpellTemplateBase spell)
        {
            foreach (AuraBase aura in AppliedAuras)
                if (aura.Spell == spell && !aura.ToBeRemoved)
                    yield return aura;
        }
        public IEnumerable<AuraBase> GetAuras(CreatureBase caster)
        {
            foreach (AuraBase aura in AppliedAuras)
                if (aura.CasterGUID == caster.GUID && !aura.ToBeRemoved)
                    yield return aura;
        }
        public IEnumerable<AuraBase> GetAuras(AuraType type)
        {
            foreach (AuraBase aura in AppliedAuras)
                if (aura.Type == type && !aura.ToBeRemoved)
                    yield return aura;
        }
        public IEnumerable<AuraBase> GetAuras()
        {
            foreach (AuraBase aura in AppliedAuras)
                if (!aura.ToBeRemoved)
                    yield return aura;
        }
    }
}