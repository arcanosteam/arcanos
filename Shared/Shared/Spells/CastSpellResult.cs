﻿namespace Arcanos.Shared.Spells
{
    public enum CastSpellResult : byte
    {
        Ok,

        UnknownSpell,
        AlreadyCasting,
        OnCooldown,
        CantUsePowerType,
        NotEnoughPower,
        NoTarget,
        WrongTarget,
        TargetIsDead,
        TargetOutOfSight,
        TargetTooClose,
        TargetTooFar,
        WrongFacing,
        Interrupted,
        NotInCombat,
        OnlyInCombat,
        Stunned,
        Silenced,
    }
}