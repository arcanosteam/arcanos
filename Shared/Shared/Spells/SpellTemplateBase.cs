﻿using Arcanos.Shared.Graphics;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Objects;
using Arcanos.Shared.World;
using Microsoft.Xna.Framework;

namespace Arcanos.Shared.Spells
{
    public abstract class SpellTemplateBase : Template
    {
        public override EditorEntry EditorEntry
        {
            get { return new EditorEntry(ID, Name); }
        }

        public Text Name;
        public Text Description;
        public int Icon;
        public int AuraIcon;
        public int ActiveIcon;
        public int GroundTargetTexture;

        public SpellCategory Category;
        public SpellFlags Flags;
        public SpellTargetMask AcceptableTargets = SpellTargetMask.All;
        public FloatBounds Range;
        public float Radius;
        public float Speed;
        public SpellEffect[] Effects;

        public PowerType RequiredPowerType;
        public int RequiredPowerAmount;

        public float CastingTime;
        public float AuraDuration;
        public float ChannelingTime;

        public int CooldownGroup;
        public float CooldownDuration;
        public float GlobalCooldown;

        public float AreaEffectDuration;

        public InterruptMask CastingInterrupt = InterruptMask.DefaultCasting;
        public InterruptMask AuraInterrupt = InterruptMask.DefaultAura;
        public InterruptMask ChannelInterrupt = InterruptMask.DefaultChannel;

        public SpellVisual Visual;
        public LightData LightData;

        public CastSpellResult CanBeCastOn(CreatureBase caster, CreatureBase target)
        {
            if (target == null)
                return CastSpellResult.NoTarget;
            if (target.Map != caster.Map)
                return CastSpellResult.NoTarget;
            if (!caster.CanSee(target))
                return CastSpellResult.NoTarget;
            if (!caster.IsInLineOfSight(target))
                return CastSpellResult.TargetOutOfSight;
            float dist = caster.DistanceTo(target);
            if (dist > Range.Max)
                return CastSpellResult.TargetTooFar;
            if (dist < Range.Min)
                return CastSpellResult.TargetTooClose;
            if (!HasFlag(SpellFlags.IgnoreFacing) && !caster.IsInArc(target, MathHelper.Pi))
                return CastSpellResult.WrongFacing;

            if (caster == target && (AcceptableTargets & SpellTargetMask.Self) == 0)
                return CastSpellResult.WrongTarget;
            if (!target.IsAlive && (AcceptableTargets & SpellTargetMask.Dead) == 0)
                return CastSpellResult.TargetIsDead;
            if (caster != target)
            {
                if (caster.GetReactionFrom(target) == Reaction.Hostile && (AcceptableTargets & SpellTargetMask.Hostile) == 0)
                    return CastSpellResult.WrongTarget;
                if (caster.GetReactionFrom(target) == Reaction.Neutral && (AcceptableTargets & SpellTargetMask.Neutral) == 0)
                    return CastSpellResult.WrongTarget;
                if (caster.GetReactionFrom(target) == Reaction.Friendly && (AcceptableTargets & SpellTargetMask.Friendly) == 0)
                    return CastSpellResult.WrongTarget;
            }

            return CastSpellResult.Ok;
        }
        public CastSpellResult CanBeCastAt(CreatureBase caster, Vector3 pos)
        {
            if (!caster.IsInLineOfSight(pos))
                return CastSpellResult.TargetOutOfSight;
            float dist = caster.DistanceTo(pos);
            if (dist > Range.Max)
                return CastSpellResult.TargetTooFar;
            if (dist < Range.Min)
                return CastSpellResult.TargetTooClose;
            if (!HasFlag(SpellFlags.IgnoreFacing) && !caster.IsInArc(pos, MathHelper.Pi))
                return CastSpellResult.WrongFacing;

            return CastSpellResult.Ok;
        }

        public bool HasEffectOfType(SpellEffectType type)
        {
            if (Effects == null)
                return false;
            int count = Effects.Length;
            for (int i = 0; i < count; ++i)
                if (Effects[i].Type == type)
                    return true;
            return false;
        }
        public bool IsCastingInterruptedBy(InterruptMask interrupt)
        {
            return (CastingInterrupt & interrupt) != 0;
        }
        public bool IsAuraInterruptedBy(InterruptMask interrupt)
        {
            return (AuraInterrupt & interrupt) != 0;
        }
        public bool IsInstantCast()
        {
            return CastingTime == 0;
        }
        public bool IsAffectedBySilence()
        {
            return Category == SpellCategory.Magic;
        }

        public bool HasFlag(SpellFlags flag)
        {
            return (Flags & flag) == flag;
        }
        public void SetFlag(SpellFlags flag)
        {
            SetFlag(flag, true);
        }
        public void UnsetFlag(SpellFlags flag)
        {
            SetFlag(flag, false);
        }
        public void ToggleFlag(SpellFlags flag)
        {
            SetFlag(flag, !HasFlag(flag));
        }
        public void SetFlag(SpellFlags flag, bool set)
        {
            if (set)
                Flags |= flag;
            else
                Flags &= ~flag;
        }

        public abstract SpellBase Create(MapBase map, Vector3 pos);
        public abstract SpellBase Create(CreatureBase caster, CreatureBase target);
        public abstract SpellBase Create(CreatureBase caster, Vector3 target);
    }
}