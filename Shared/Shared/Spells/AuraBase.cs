﻿using Arcanos.Shared.Data;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Timing;

namespace Arcanos.Shared.Spells
{
    public abstract class AuraBase : IManualSerialization, IManualDeserialization
    {
        [SerializationIgnore]
        public CreatureBase Owner;
        public readonly ulong CasterGUID;
        [SerializationSettings(Method = SerializationMethod.Manual)]
        public SpellTemplateBase Spell;
        public readonly int EffectID;

        public readonly AuraType Type;
        public float DurationTotal;
        public float DurationLeft;

        public SpellEffect Effect
        {
            get { return Spell.Effects[EffectID]; }
        }

        [SerializationIgnore]
        public bool ToBeRemoved;

        protected AuraBase() { }
        protected AuraBase(CreatureBase owner, ulong casterGUID, SpellTemplateBase spell, int effectID)
        {
            Owner = owner;
            CasterGUID = casterGUID;
            Spell = spell;
            EffectID = effectID;

            SpellEffect effect = spell.Effects[effectID];
            Type = effect.AuraType;
            DurationTotal = DurationLeft = spell.AuraDuration;
        }

        public virtual void Update()
        {
            if (DurationLeft > 0)
            {
                DurationLeft -= Time.PerSecond;
                if (DurationLeft <= 0)
                {
                    DurationLeft = 0;
                    MarkForRemoval();
                }
            }
        }

        protected virtual void MarkForRemoval()
        {
            ToBeRemoved = true;
        }

        public abstract byte[] Serialize(string member);
        public abstract void Deserialize(string member, byte[] data);
    }
}