﻿using Arcanos.Shared.Objects;

namespace Arcanos.Shared.Spells
{
    public abstract class AreaEffectBase : GameObject
    {
        protected AreaEffectBase(ulong guid)
        {
            RegisterGUID(guid);
        }

        public virtual void Remove()
        {
            ObjectManager.Unregister(GUID);
        }
    }
}