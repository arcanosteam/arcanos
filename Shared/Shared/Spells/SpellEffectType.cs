﻿namespace Arcanos.Shared.Spells
{
    public enum SpellEffectType : ushort
    {
        None,

        InstantKill,
        Damage,
        /// <summary>
        /// IV1: Min Damage<br/>
        /// IV2: Max Damage (0: =IV1)<br/>
        /// FV1: Level scaling<br/>
        /// FV2: Stat scaling
        /// </summary>
        PhysicalDamage,
        /// <summary>
        /// IV1: Min Damage<br/>
        /// IV2: Max Damage (0: =IV1)<br/>
        /// IV3: Damage School<br/>
        /// FV1: Level scaling<br/>
        /// FV2: Stat scaling
        /// </summary>
        SchoolDamage,
        /// <summary>
        /// IV1: Damage School<br/>
        /// IV2: Attack Type Mask (0: All)<br/>
        /// FV1: Weapon damage percent (0: 100%)
        /// </summary>
        WeaponDamage,
        /// <summary>
        /// IV1: Min Heal<br/>
        /// IV2: Max Heal (0: =IV1)<br/>
        /// FV1: Level scaling<br/>
        /// FV2: Stat scaling
        /// </summary>
        Heal,
        /// <summary>
        /// IV1: Min Power<br/>
        /// IV2: Max Power (0: =IV1)<br/>
        /// IV3: Power Type<br/>
        /// </summary>
        RestorePower,
        /// <summary>
        /// IV1: Min Damage<br/>
        /// IV2: Max Damage (0: =IV1)<br/>
        /// IV3: Damage School<br/>
        /// FV1: Level scaling<br/>
        /// FV2: Stat scaling<br/>
        /// FV3: Steal percent
        /// </summary>
        StealHealth,
        /// <summary>
        /// IV1: Min percent of max health<br/>
        /// IV2: Max percent of max health (0: =IV1)<br/>
        /// IV3: Damage School<br/>
        /// FV1: Level scaling<br/>
        /// FV2: Stat scaling
        /// </summary>
        MaxHealthPercentSchoolDamage,

        Teleport,
        TriggerSpell,
        TriggerSpellFromTarget,

        ApplyAura,
        /// <summary>
        /// IV1: Spell ID<br/>
        /// IV2: Spell Effect Mask<br/>
        /// FV1: Custom Duration<br/>
        /// </summary>
        ApplyAuraFromSpell,
        RemoveAuraBySpell,
        RemoveAuraByType,
        RemoveAuraByCaster,

        AuraAreaEffect,
        ApplyAuraAreaEffect,
        RemoveAuraBySpellAreaEffect,
        PeriodicAreaEffect,
        NullAreaEffect,

        Max,
    }
}