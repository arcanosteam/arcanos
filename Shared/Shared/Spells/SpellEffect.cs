﻿namespace Arcanos.Shared.Spells
{
    public struct SpellEffect
    {
        public float Chance;
        public SpellEffectTarget TargetA;
        public SpellEffectTarget TargetB;
        public SpellEffectType Type;
        public int IntValue1;
        public int IntValue2;
        public int IntValue3;
        public float FloatValue1;
        public float FloatValue2;
        public float FloatValue3;
        public AuraType AuraType;
        public float TriggerSpellPeriodic;
        public float AreaEffectPeriodic;

        public SpellEffect(SpellEffectType type) : this(1, type) { }
        public SpellEffect(float chance, SpellEffectType type) : this(chance, type, 0, 0) { }
        public SpellEffect(SpellEffectType type, SpellEffectTarget targetA, SpellEffectTarget targetB) : this(1, type, targetA, targetB) { }
        public SpellEffect(float chance, SpellEffectType type, SpellEffectTarget targetA, SpellEffectTarget targetB)
        {
            Type = type;
            Chance = chance;
            TargetA = targetA;
            TargetB = targetB;
            IntValue1 = 0;
            IntValue2 = 0;
            IntValue3 = 0;
            FloatValue1 = 0;
            FloatValue2 = 0;
            FloatValue3 = 0;
            AuraType = AuraType.None;
            TriggerSpellPeriodic = 0;
            AreaEffectPeriodic = 0;
        }

        public bool IsAuraEffect
        {
            get
            {
                switch (Type)
                {
                    case SpellEffectType.ApplyAura:
                        return true;
                    default:
                        return false;
                }
            }
        }
        public bool IsAreaEffect
        {
            get
            {
                switch (Type)
                {
                    case SpellEffectType.AuraAreaEffect:
                    case SpellEffectType.ApplyAuraAreaEffect:
                    case SpellEffectType.RemoveAuraBySpellAreaEffect:
                    case SpellEffectType.PeriodicAreaEffect:
                    case SpellEffectType.NullAreaEffect:
                        return true;
                    default:
                        return false;
                }
            }
        }
    }
}