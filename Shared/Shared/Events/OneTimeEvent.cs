﻿using Arcanos.Shared.Timing;

namespace Arcanos.Shared.Events
{
    public class OneTimeEvent : Event
    {
        public float Delay;
        
        public float Remaining;

        public OneTimeEvent(float delay, EventActionHandler action) : this(null, delay, action) { }
        public OneTimeEvent(string name, float delay, EventActionHandler action) : base(name)
        {
            Name = name;
            Delay = delay;
            Remaining = delay;
            Action = action;
        }

        public override bool Update()
        {
            Remaining -= Time.PerSecond;
            if (Remaining <= 0)
            {
                if (Action != null)
                    Action();
                return true;
            }
            return false;
        }
    }
}