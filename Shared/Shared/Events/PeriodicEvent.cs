﻿using Arcanos.Shared.Timing;

namespace Arcanos.Shared.Events
{
    public class PeriodicEvent : Event
    {
        public float RepeatDelayMin;
        public float RepeatDelayMax;
        public int Repeats;

        public float Remaining;
        public int Repeated;

        public PeriodicEvent(float initialMin, float initialMax, float repeatDelayMin, float repeatDelayMax, int repeats, EventActionHandler action) : this(null, initialMin, initialMax, repeatDelayMin, repeatDelayMax, repeats, action) { }
        public PeriodicEvent(string name, float initialMin, float initialMax, float repeatDelayMin, float repeatDelayMax, int repeats, EventActionHandler action) : base(name)
        {
            Name = name;
            RepeatDelayMin = repeatDelayMin;
            RepeatDelayMax = repeatDelayMax;
            Repeats = repeats;
            Remaining = R.Float(initialMin, initialMax);
            Repeated = 0;
            Action = action;
        }

        public override bool Update()
        {
            Remaining -= Time.PerSecond;
            while (Remaining <= 0)
            {
                if (Action != null)
                    Action();
                if (++Repeated < Repeats || Repeats < 0)
                    Remaining += R.Float(RepeatDelayMin, RepeatDelayMax);
                else
                {
                    Repeated--;
                    return true;
                }
            }
            return false;
        }
    }
}