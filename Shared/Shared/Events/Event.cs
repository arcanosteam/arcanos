﻿namespace Arcanos.Shared.Events
{
    public delegate void EventActionHandler();
    public abstract class Event
    {
        public string Name;

        public EventActionHandler Action;

        protected Event(string name)
        {
            Name = name;
        }

        public abstract bool Update();
    }
}