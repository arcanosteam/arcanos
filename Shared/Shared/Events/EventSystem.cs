﻿using System.Collections.Generic;

namespace Arcanos.Shared.Events
{
    public class EventSystem
    {
        protected class EventCollection : List<Event> { }

        public int Count
        {
            get { return Events.Count + Events.Count; }
        }

        protected readonly EventCollection Events = new EventCollection();

        public EventSystem() { }

        public virtual void Update()
        {
            int cachedCount = Events.Count;
            for (int i = 0; i < cachedCount; i++)
                if (Events[i].Update())
                {
                    Events.RemoveAt(i--);
                    cachedCount--;
                }
        }

        public virtual void Clear()
        {
            Events.Clear();
            Events.Clear();
        }
        public virtual OneTimeEvent Schedule(float delay, EventActionHandler action)
        {
            OneTimeEvent e = new OneTimeEvent(delay, action);
            Events.Add(e);
            return e;
        }
        public virtual OneTimeEvent Schedule(float delayMin, float delayMax, EventActionHandler action)
        {
            OneTimeEvent e = new OneTimeEvent(R.Float(delayMin, delayMax), action);
            Events.Add(e);
            return e;
        }
        public virtual OneTimeEvent Schedule(string name, float delay, EventActionHandler action)
        {
            OneTimeEvent e = new OneTimeEvent(name, delay, action);
            Events.Add(e);
            return e;
        }
        public virtual OneTimeEvent Schedule(string name, float delayMin, float delayMax, EventActionHandler action)
        {
            OneTimeEvent e = new OneTimeEvent(name, R.Float(delayMin, delayMax), action);
            Events.Add(e);
            return e;
        }
        public virtual void Schedule(OneTimeEvent e)
        {
            Events.Add(e);
        }
        public virtual PeriodicEvent SchedulePeriodic(float repeatDelay, EventActionHandler action)
        {
            PeriodicEvent e = new PeriodicEvent(repeatDelay, repeatDelay, repeatDelay, repeatDelay, -1, action);
            Events.Add(e);
            return e;
        }
        public virtual PeriodicEvent SchedulePeriodic(float repeatDelay, int repeats, EventActionHandler action)
        {
            PeriodicEvent e = new PeriodicEvent(repeatDelay, repeatDelay, repeatDelay, repeatDelay, repeats, action);
            Events.Add(e);
            return e;
        }
        public virtual PeriodicEvent SchedulePeriodic(float repeatDelayMin, float repeatDelayMax, EventActionHandler action)
        {
            PeriodicEvent e = new PeriodicEvent(repeatDelayMin, repeatDelayMax, repeatDelayMin, repeatDelayMax, -1, action);
            Events.Add(e);
            return e;
        }
        public virtual PeriodicEvent SchedulePeriodic(float repeatDelayMin, float repeatDelayMax, int repeats, EventActionHandler action)
        {
            PeriodicEvent e = new PeriodicEvent(repeatDelayMin, repeatDelayMax, repeatDelayMin, repeatDelayMax, repeats, action);
            Events.Add(e);
            return e;
        }
        public virtual PeriodicEvent SchedulePeriodic(float initialDelayMin, float initialDelayMax, float repeatDelayMin, float repeatDelayMax, EventActionHandler action)
        {
            PeriodicEvent e = new PeriodicEvent(initialDelayMin, initialDelayMax, repeatDelayMin, repeatDelayMax, -1, action);
            Events.Add(e);
            return e;
        }
        public virtual PeriodicEvent SchedulePeriodic(float initialDelayMin, float initialDelayMax, float repeatDelayMin, float repeatDelayMax, int repeats, EventActionHandler action)
        {
            PeriodicEvent e = new PeriodicEvent(initialDelayMin, initialDelayMax, repeatDelayMin, repeatDelayMax, repeats, action);
            Events.Add(e);
            return e;
        }
        public virtual PeriodicEvent SchedulePeriodic(string name, float repeatDelay, EventActionHandler action)
        {
            PeriodicEvent e = new PeriodicEvent(name, repeatDelay, repeatDelay, repeatDelay, repeatDelay, -1, action);
            Events.Add(e);
            return e;
        }
        public virtual PeriodicEvent SchedulePeriodic(string name, float repeatDelay, int repeats, EventActionHandler action)
        {
            PeriodicEvent e = new PeriodicEvent(name, repeatDelay, repeatDelay, repeatDelay, repeatDelay, repeats, action);
            Events.Add(e);
            return e;
        }
        public virtual PeriodicEvent SchedulePeriodic(string name, float repeatDelayMin, float repeatDelayMax, EventActionHandler action)
        {
            PeriodicEvent e = new PeriodicEvent(name, repeatDelayMin, repeatDelayMax, repeatDelayMin, repeatDelayMax, -1, action);
            Events.Add(e);
            return e;
        }
        public virtual PeriodicEvent SchedulePeriodic(string name, float repeatDelayMin, float repeatDelayMax, int repeats, EventActionHandler action)
        {
            PeriodicEvent e = new PeriodicEvent(name, repeatDelayMin, repeatDelayMax, repeatDelayMin, repeatDelayMax, repeats, action);
            Events.Add(e);
            return e;
        }
        public virtual PeriodicEvent SchedulePeriodic(string name, float initialDelayMin, float initialDelayMax, float repeatDelayMin, float repeatDelayMax, EventActionHandler action)
        {
            PeriodicEvent e = new PeriodicEvent(name, initialDelayMin, initialDelayMax, repeatDelayMin, repeatDelayMax, -1, action);
            Events.Add(e);
            return e;
        }
        public virtual PeriodicEvent SchedulePeriodic(string name, float initialDelayMin, float initialDelayMax, float repeatDelayMin, float repeatDelayMax, int repeats, EventActionHandler action)
        {
            PeriodicEvent e = new PeriodicEvent(name, initialDelayMin, initialDelayMax, repeatDelayMin, repeatDelayMax, repeats, action);
            Events.Add(e);
            return e;
        }
        public virtual void SchedulePeriodic(PeriodicEvent e)
        {
            Events.Add(e);
        }
        public virtual void Unschedule(string name)
        {
            int cachedCount = Events.Count;
            for (int i = 0; i < cachedCount; i++)
                if (Events[i].Name == name)
                {
                    Events.RemoveAt(i--);
                    cachedCount--;
                }
        }
        public virtual void Unschedule(OneTimeEvent e)
        {
            Events.Remove(e);
        }
        public virtual void Unschedule(PeriodicEvent e)
        {
            Events.Remove(e);
        }
    }
}