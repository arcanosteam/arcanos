﻿using System.Collections.Generic;
using Arcanos.Shared.Data;
using Arcanos.Shared.Items;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Players;
using Arcanos.Shared.Spells;

namespace Arcanos.Shared.Quests
{
    public abstract class QuestLogBase
    {
        [SerializationIgnore]
        public CharacterBase Owner;
        public List<QuestInstanceBase> Quests = new List<QuestInstanceBase>();

        protected QuestLogBase() { }
        protected QuestLogBase(CharacterBase owner)
        {
            Owner = owner;
        }

        public bool HasQuest(QuestBase quest)
        {
            return GetInstanceForQuest(quest) != null;
        }
        public bool HasQuestForItem(ItemTemplateBase item)
        {
            foreach (QuestInstanceBase instance in Quests)
                foreach (QuestObjective objective in instance.Quest.Objectives)
                    if (objective.Type == QuestObjectiveType.Item && objective.ID == item.ID)
                        return true;
            return false;
        }
        public QuestInstanceBase GetInstanceForQuest(QuestBase quest)
        {
            int count = Quests.Count;
            for (int i = 0; i < count; i++)
                if (Quests[i].Quest == quest)
                    return Quests[i];
            return null;
        }

        public virtual QuestInstanceBase AddQuest(QuestBase quest)
        {
            QuestInstanceBase instance = quest.Create();
            Quests.Add(instance);
            return instance;
        }
        public virtual void RemoveQuest(QuestBase quest)
        {
            Quests.Remove(GetInstanceForQuest(quest));
        }

        public virtual void CreditedCallback(QuestInstanceBase instance, byte objectiveIndex) { }

        public void CreditKill(CreatureBase victim)
        {
            Credit(QuestObjectiveType.Kill, victim.Template.ID, 0, 1);
        }
        public void CreditUse(WorldObjectBase wo)
        {
            Credit(QuestObjectiveType.Use, wo.Template.ID, 0, 1);
        }
        public void CreditItem(ItemTemplateBase item, int count)
        {
            Credit(QuestObjectiveType.Item, item.ID, 0, count);
        }
        public void CreditCast(SpellTemplateBase spell, CreatureBase target)
        {
            Credit(QuestObjectiveType.Cast, spell.ID, target == null ? 0 : target.Template.ID, 1);
        }
        protected virtual bool Credit(QuestObjectiveType type, int id, int misc, int credit)
        {
            bool credited = false;
            for (int i = 0; i < Quests.Count; i++)
                if (Quests[i].Credit(this, type, id, misc, credit))
                    credited = true;
            return credited;
        }
    }
}