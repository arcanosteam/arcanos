﻿using Arcanos.Shared.Localization;

namespace Arcanos.Shared.Quests
{
    public enum QuestObjectiveType : byte
    {
        Kill,
        Use,
        Item,
        Cast,
    }
    public class QuestObjective
    {
        public QuestObjectiveType Type;
        public int ID;
        public int Misc;
        public int Required;
        public Text OverrideText;

        internal QuestObjective() { }
        public QuestObjective(QuestObjectiveType type, int id, int required) : this(type, id, 0, required) { }
        public QuestObjective(QuestObjectiveType type, int id, int misc, int required)
        {
            Type = type;
            ID = id;
            Misc = misc;
            Required = required;
        }
    }
}