﻿using System;
using Arcanos.Shared.Data;

namespace Arcanos.Shared.Quests
{
    public abstract class QuestInstanceBase : IManualSerialization, IManualDeserialization
    {
        [SerializationSettings(Method = SerializationMethod.Manual)]
        public QuestBase Quest;

        public int[] ObjectiveProgress = new int[0];

        protected QuestInstanceBase() { }
        protected QuestInstanceBase(QuestBase quest)
        {
            Quest = quest;
            ObjectiveProgress = new int[quest.Objectives.Count];
        }

        public virtual bool IsCompleted()
        {
            for (int i = 0; i < ObjectiveProgress.Length; i++)
                if (GetObjectiveRemainder(i) != 0)
                    return false;
            return true;
        }
        public virtual int GetObjectiveRemainder(int index)
        {
            int remainder = Quest.Objectives[index].Required - ObjectiveProgress[index];
            return remainder < 0 ? 0 : remainder;
        }

        public virtual bool Credit(QuestLogBase log, QuestObjectiveType type, int id, int misc, int credit)
        {
            bool credited = false;
            int count = Quest.Objectives.Count;
            for (byte i = 0; i < count; i++)
            {
                if (Quest.Objectives[i].Type == type)
                {
                    switch (type)
                    {
                        case QuestObjectiveType.Kill:
                        case QuestObjectiveType.Use:
                        case QuestObjectiveType.Item:
                            if (Quest.Objectives[i].ID != id)
                                continue;
                            break;
                        case QuestObjectiveType.Cast:
                            if (Quest.Objectives[i].ID != id || Quest.Objectives[i].Misc != misc)
                                continue;
                            break;
                    }

                    ObjectiveProgress[i] += credit;
                    credited = true;
                    log.CreditedCallback(this, i);
                }
            }
            return credited;
        }

        public byte[] Serialize(string member)
        {
            if (member == "Quest")
                return BitConverter.GetBytes(Quest.ID);
            return new byte[0];
        }
        public abstract void Deserialize(string member, byte[] data);
    }
}