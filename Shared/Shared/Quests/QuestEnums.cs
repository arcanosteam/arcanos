﻿namespace Arcanos.Shared.Quests
{
    public enum QuestAcceptResult : byte
    {
        Ok,

        UnknownQuest,
        AlreadyHaveQuest,
    }
    public enum QuestRemoveResult : byte
    {
        Ok,

        UnknownQuest,
        QuestNotPresent,
    }
    public enum QuestCompleteResult : byte
    {
        Ok,
    }
}