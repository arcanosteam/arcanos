﻿using System.Collections.Generic;
using Arcanos.Shared.Items;
using Arcanos.Shared.Localization;

namespace Arcanos.Shared.Quests
{
    public abstract class QuestBase : Template
    {
        public override EditorEntry EditorEntry
        {
            get { return new EditorEntry(ID, Title); }
        }

        public Text Title;
        public Text AcceptText;
        public Text StatusQueryText;
        public Text CompletedText;

        public int RewardMoney;
        public int RewardExperience;
        public List<ItemDefinitionBase> RewardItems = new List<ItemDefinitionBase>();

        public List<QuestObjective> Objectives = new List<QuestObjective>();

        public abstract QuestInstanceBase Create();
    }
}