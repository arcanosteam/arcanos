﻿using System;

namespace Arcanos.Shared.Timing
{
    public static class Time
    {
        // Needs to be changed manually
        public static float PerSecond;
        public static long Timestamp;

        public static DateTime TimestampToDateTime(long timestamp)
        {
            return new DateTime(timestamp * 10000);
        }
        public static string FormatTimestamp(string format, long timestamp)
        {
            return TimestampToDateTime(timestamp).ToString(format);
        }
    }
}