﻿using Arcanos.Shared.Data;
using Arcanos.Shared.Players;

namespace Arcanos.Shared.Guilds
{
    public abstract class GuildMemberBase
    {
        /// <summary>
        /// Represents a player character that is a member in a guild. Should be hidden by a new property, that defines by-reference serialization settings.
        /// </summary>
        [SerializationIgnore]
        public CharacterBase Character;
        public int Rank;

        protected GuildMemberBase(CharacterBase character, int rank)
        {
            Character = character;
            Rank = rank;
        }
    }
}