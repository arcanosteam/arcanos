﻿using System.Collections.Generic;
using Arcanos.Shared.Players;

namespace Arcanos.Shared.Guilds
{
    public abstract class GuildBase : Template
    {
        public override EditorEntry EditorEntry
        {
            get { return new EditorEntry(ID, Title); }
        }

        public enum AddMemberResult
        {
            Ok,
            MemberAlready,
        }

        public string Title;
        public string Tag;
        public readonly List<GuildMemberBase> Members = new List<GuildMemberBase>();

        public int StartingRank;

        public AddMemberResult AddMember(CharacterBase character)
        {
            return AddMember(character, StartingRank);
        }
        public AddMemberResult AddMember(CharacterBase character, int rank)
        {
            if (IsMember(character))
                return AddMemberResult.MemberAlready;

            AddMemberImpl(character, rank);

            return AddMemberResult.Ok;
        }
        protected abstract void AddMemberImpl(CharacterBase character, int rank);

        public bool IsMember(CharacterBase character)
        {
            int count = Members.Count;
            for (int i = 0; i < count; i++)
                if (Members[i].Character == character)
                    return true;
            return false;
        }
    }
}