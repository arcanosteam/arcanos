﻿using System;
using Arcanos.Shared.Collisions;
using Arcanos.Shared.Data;
using Arcanos.Shared.Graphics;
using Microsoft.Xna.Framework;

namespace Arcanos.Shared.Objects
{
    [Flags]
    public enum StaticModelObjectFlags : byte
    {
        Solid = 0x01,
        GroundedOnTerrain = 0x02,
        CellOcclusion = 0x04,
        ReducedVisibilityDistance = 0x08,
    }
    public class StaticModelObjectBase
    {
        public WorldModel Model;
        public StaticModelObjectFlags Flags;
        public Shape Shape;
        public Vector3 Position;
        public Rotation Rotation;
        public Vector3 Size;

        protected StaticModelObjectBase() { }
        public StaticModelObjectBase(WorldModel model, StaticModelObjectFlags flags, Shape shape, Vector3 position, Rotation rotation, Vector3 size)
        {
            Model = model;
            Flags = flags;
            Shape = shape;
            Position = position;
            Rotation = rotation;
            Size = size;
            Build();
        }

        [SerializationIgnore]
        public Matrix Matrix;
        [SerializationIgnore]
        public BoundingBox BoundingBox;

        protected void Build()
        {
            Vector3 size;
            Vector3 offset;
            GetRenderInfo(out offset, out size);
            Matrix = Matrix.CreateRotationX(Rotation.Roll) * 
                     Matrix.CreateRotationY(Rotation.Pitch) * 
                     Matrix.CreateRotationZ(Rotation.Yaw) *
                     Matrix.CreateScale(Model.RelativeSize ? size * Model.Size : Model.Size) * 
                     Matrix.CreateTranslation(Position);
            GetVolume(out BoundingBox.Min, out BoundingBox.Max);
        }

        public void MoveTo(Vector3 pos)
        {
            Position = pos;
            Build();
        }
        public void RotateTo(Rotation rot)
        {
            Rotation = rot;
            Build();
        }
        public void ResizeTo(Vector3 size)
        {
            Size = size;
            Build();
        }

        public float GetApproachRadius()
        {
            switch (Shape)
            {
                case Shape.None:
                case Shape.Cylinder:
                    return Size.X;
                case Shape.Box:
                    return Size.X / 2;
                case Shape.Sphere:
                    return Size.Z / 2;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        public void GetVolume(out Vector3 min, out Vector3 max)
        {
            switch (Shape)
            {
                case Shape.None:
                    min = Position - Size / 2;
                    max = Position + Size / 2;
                    break;
                case Shape.Box:
                    min = Position + new Vector3(-Size.X / 2, -Size.Y / 2, 0);
                    max = Position + new Vector3(Size.X / 2, Size.Y / 2, Size.Z);
                    break;
                case Shape.Sphere:
                    min = Position + new Vector3(-Size.Z / 2, -Size.Z / 2, -Size.Z / 2);
                    max = Position + new Vector3(Size.Z / 2, Size.Z / 2, Size.Z / 2);
                    break;
                case Shape.Cylinder:
                    min = Position + new Vector3(-Size.X / 2, -Size.X / 2, 0);
                    max = Position + new Vector3(Size.X / 2, Size.X / 2, Size.Z);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        public void GetRenderInfo(out Vector3 offset, out Vector3 size)
        {
            switch (Shape)
            {
                case Shape.None:
                    offset = Vector3.Zero;
                    size = Vector3.One;
                    break;
                case Shape.Box:
                    offset = new Vector3(0, 0, Size.Z / 2);
                    size = new Vector3(Math.Max(Math.Max(Size.X, Size.Y), Size.Z));
                    break;
                case Shape.Sphere:
                    offset = Vector3.Zero;
                    size = new Vector3(Size.Z);
                    break;
                case Shape.Cylinder:
                    offset = Vector3.Zero;
                    size = new Vector3(Math.Max(Size.X, Size.Z));
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}