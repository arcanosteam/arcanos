﻿using System;
using Arcanos.Shared.Collisions;
using Arcanos.Shared.Grids;
using Arcanos.Shared.Movement;
using Arcanos.Shared.Players;
using Microsoft.Xna.Framework;

namespace Arcanos.Shared.Objects
{
    public abstract class PlayerCreatureBase : CreatureBase
    {
        private const float MOVEMENT_DISTANCE_TOLERANCE_PERCENT = 1.1f;
        private const float STATIC_COLLISION_CHECK_DISTANCE = 10.0f;

        public static int FriendlyTargetDecalTextureID;
        public static int NeutralTargetDecalTextureID;
        public static int HostileTargetDecalTextureID;

        protected readonly CharacterBase character;
        public CharacterBase Character
        {
            get { return character; }
        }

        protected PlayerCreatureBase(ulong guid, CreatureTemplateBase template, CharacterBase character, Vector3 pos, Rotation rot) : base(guid, template, pos, rot)
        {
            Type = ObjectType.PlayerCreature;
            this.character = character;
        }

        public MovementValidity IsMovementValid(Vector3 source, Vector3 destination, bool clientside)
        {
            if (HasFlag(ObjectFlags.Stunned))
                return MovementValidity.Incapable;

            float speed = GetMovementSpeed();
            if (Vector3.Distance(source, destination) > speed * MOVEMENT_DISTANCE_TOLERANCE_PERCENT)
                return MovementValidity.MovedTooFar;

            if (clientside)
            {
                // Terrain collisions check
                if (destination.Z - source.Z >= 0.5f)
                    return MovementValidity.CollisionWithTerrain;
                if (Map.Template.GetNormal(destination.X, destination.Y).Z <= 0.75f && destination.Z >= source.Z)
                    return MovementValidity.SlopeTooSteep;
            }

            // SMO collisions check
            GridBounds bounds = Map.Grid.GetGridBounds(Position, STATIC_COLLISION_CHECK_DISTANCE);
            for (int x = bounds.MinX; x <= bounds.MaxX; ++x)
                for (int y = bounds.MinY; y <= bounds.MaxY; ++y)
                    foreach (StaticModelObjectBase smo in Map.Grid.Cells[x][y].SolidStatics)
                        switch (smo.Shape)
                        {
                            case Shape.Cylinder:
                                if (Math.Sqrt((destination.X - smo.Position.X) * (destination.X - smo.Position.X) + (destination.Y - smo.Position.Y) * (destination.Y - smo.Position.Y)) <= GetApproachRadius() + smo.GetApproachRadius())
                                    return MovementValidity.CollisionWithSolidStatic;
                                break;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }

            return MovementValidity.Ok;
        }
    }
}