﻿using System;

namespace Arcanos.Shared.Objects
{
    [Flags]
    public enum ObjectFlags : uint
    {
        Lootable        = 0x00000001,
        Looting         = 0x00000002,
        BeingLooted     = 0x00000004,
        HasDialog       = 0x00000008,
        Stunned         = 0x00000010,
        Rooted          = 0x00000020,
        Silenced        = 0x00000040,
    }
}