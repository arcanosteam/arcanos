﻿using System;

namespace Arcanos.Shared.Objects
{
    [Flags]
    public enum ObjectType : ushort
    {
        WorldObject    = 0x0001,
        Creature       = 0x0002 | WorldObject,
        PlayerCreature = 0x0004 | Creature,

        All            = 0xFFFF,
    }
}