﻿using System;
using Arcanos.Shared.Collisions;
using Arcanos.Shared.Graphics;
using Arcanos.Shared.Grids;
using Arcanos.Shared.Items;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Timing;
using Arcanos.Shared.World;
using Microsoft.Xna.Framework;

namespace Arcanos.Shared.Objects
{
    public abstract class WorldObjectBase : GameObject, IWorldEntity
    {
        protected readonly WorldObjectTemplateBase template;
        public WorldObjectTemplateBase Template { get { return template; } }

        public ObjectType Type;

        public abstract string Name { get; }
        public abstract Text NameRef { get; }

        public bool IsInWorld
        {
            get { return Map != null && !isDespawned; }
        }

        public MapBase Map;
        public GridCell Cell;
        public ulong PhaseMask;
        protected bool isDespawned;
        private long despawnTime;
        private long respawnTime;

        public Vector3 Position;
        public Rotation Rotation;
        public Vector3 HomePosition;
        public Rotation HomeRotation;
        public Vector3 Size;
        public Shape Shape;
        public BoundingBox BoundingBox;

        public ObjectFlags Flags;

        public LootBase Loot;

        public ModelInstance Model;

        public bool Finalized;

        protected WorldObjectBase(ulong guid, WorldObjectTemplateBase template, Vector3 pos, Rotation rot)
        {
            RegisterGUID(guid);
            Type = ObjectType.WorldObject;
            this.template = template;

            HomePosition = pos;
            HomeRotation = rot;
        }

        protected virtual void ResetFromTemplate()
        {
            PhaseMask = 0;
            Position = HomePosition;
            Rotation = HomeRotation;
            Size = template.Size;
            Shape = template.Shape;
            GetVolume(out BoundingBox.Min, out BoundingBox.Max);

            Flags = 0;

            Loot = null;

            Model = template.Model.CreateInstance(this);

            Finalized = false;
        }

        private ulong lastUpdateCycle;
        public virtual bool Update()
        {
            if (Map.UpdateCycle == lastUpdateCycle)
                return false;
            lastUpdateCycle = Map.UpdateCycle;

            if (respawnTime > 0 && Time.Timestamp >= respawnTime)
                Respawn();
            if (despawnTime > 0 && Time.Timestamp >= despawnTime)
                Despawn();
            if (isDespawned)
                return false;

            Model.Update();

            return true;
        }
        public virtual void Destroy()
        {
            if (Model != null)
                Model.Destroy();
        }

        public Vector3 GetPosition()
        {
            return Position;
        }
        public Rotation GetRotation()
        {
            return Rotation;
        }
        public Vector3 GetSize()
        {
            return Size;
        }
        public virtual void MoveTo(Vector3 pos)
        {
            if (Position == pos)
                return;
            Position = pos;
            GetVolume(out BoundingBox.Min, out BoundingBox.Max);
            if (Map != null)
                Map.MoveObject(this);
            OnPositionChanged();
        }
        public virtual void RotateTo(Rotation rot)
        {
            if (Rotation == rot)
                return;
            Rotation = rot;
            OnRotationChanged();
        }

        public bool HasFlag(ObjectFlags flag)
        {
            return (Flags & flag) != 0;
        }
        public void SetFlag(ObjectFlags flag)
        {
            SetFlag(flag, true);
        }
        public void UnsetFlag(ObjectFlags flag)
        {
            SetFlag(flag, false);
        }
        public void ToggleFlag(ObjectFlags flag)
        {
            SetFlag(flag, !HasFlag(flag));
        }
        public virtual void SetFlag(ObjectFlags flag, bool set)
        {
            if (set)
                Flags |= flag;
            else
                Flags &= ~flag;
        }

        public virtual void Respawn()
        {
            respawnTime = 0;
            isDespawned = false;
            ResetFromTemplate();
            OnAddedToMap();
        }
        public void Respawn(long afterMsec)
        {
            respawnTime = Time.Timestamp + afterMsec;
        }
        public virtual void Despawn()
        {
            despawnTime = 0;
            isDespawned = true;
            OnRemovedFromMap();
        }
        public void Despawn(long afterMsec)
        {
            despawnTime = Time.Timestamp + afterMsec;
        }

        public abstract void OnPositionChanged();
        public abstract void OnRotationChanged();
        public abstract void OnAddedToMap();
        public abstract void OnRemovedFromMap();

        public float DistanceTo(WorldObjectBase wo)
        {
            if (Map != wo.Map)
                return float.PositiveInfinity;
            return Vector3.Distance(Position, wo.Position);
        }
        public float DistanceTo(Vector3 pos)
        {
            return Vector3.Distance(Position, pos);
        }
        public float DistanceTo2D(WorldObjectBase wo)
        {
            if (Map != wo.Map)
                return float.PositiveInfinity;
            return (float)Math.Sqrt((Position.X - wo.Position.X) * (Position.X - wo.Position.X) + (Position.Y - wo.Position.Y) * (Position.Y - wo.Position.Y));
        }
        public float DistanceTo2D(Vector3 pos)
        {
            return (float)Math.Sqrt((Position.X - pos.X) * (Position.X - pos.X) + (Position.Y - pos.Y) * (Position.Y - pos.Y));
        }
        public bool IsInArc(WorldObjectBase wo, float arc)
        {
            if (wo == this)
                return true;
            return IsInArc(wo.Position, arc);
        }
        public bool IsInArc(Vector3 pos, float arc)
        {
            arc = Rotation.NormalizeYaw(arc);
            float angle = Rotation.NormalizePitch(Position.GetRotationTo(pos).Yaw - Rotation.Yaw);
            return angle >= -arc / 2 && angle <= arc / 2;
        }

        public void GetVolume(out Vector3 min, out Vector3 max)
        {
            switch (Shape)
            {
                case Shape.None:
                    min = Position - Size / 2;
                    max = Position + Size / 2;
                    break;
                case Shape.Box:
                    min = Position + new Vector3(-Size.X / 2, -Size.Y / 2, 0);
                    max = Position + new Vector3(Size.X / 2, Size.Y / 2, Size.Z);
                    break;
                case Shape.Sphere:
                    min = Position + new Vector3(-Size.Z / 2, -Size.Z / 2, -Size.Z / 2);
                    max = Position + new Vector3(Size.Z / 2, Size.Z / 2, Size.Z / 2);
                    break;
                case Shape.Cylinder:
                    min = Position + new Vector3(-Size.X / 2, -Size.X / 2, 0);
                    max = Position + new Vector3(Size.X / 2, Size.X / 2, Size.Z);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        public float GetApproachRadius()
        {
            switch (Shape)
            {
                case Shape.None:
                case Shape.Cylinder:
                    return Size.X;
                case Shape.Box:
                    return Size.X / 2;
                case Shape.Sphere:
                    return Size.Z / 2;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        public void GetRenderInfo(out Vector3 offset, out Vector3 size)
        {
            switch (Shape)
            {
                case Shape.None:
                    offset = Vector3.Zero;
                    size = Vector3.One;
                    break;
                case Shape.Box:
                    offset = new Vector3(0, 0, Size.Z / 2);
                    size = new Vector3(Math.Max(Math.Max(Size.X, Size.Y), Size.Z));
                    break;
                case Shape.Sphere:
                    offset = Vector3.Zero;
                    size = new Vector3(Size.Z);
                    break;
                case Shape.Cylinder:
                    offset = Vector3.Zero;
                    size = new Vector3(Math.Max(Size.X, Size.Z));
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        public float GetHeight()
        {
            switch (Shape)
            {
                case Shape.None:
                case Shape.Sphere:
                    return Size.Z / 2;
                case Shape.Box:
                case Shape.Cylinder:
                    return Size.Z;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}