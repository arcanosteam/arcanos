﻿using Arcanos.Shared.Collisions;
using Arcanos.Shared.Graphics;
using Arcanos.Shared.Localization;
using Microsoft.Xna.Framework;

namespace Arcanos.Shared.Objects
{
    public abstract class WorldObjectTemplateBase : Template
    {
        public override EditorEntry EditorEntry
        {
            get { return new EditorEntry(ID, Name); }
        }

        public Text Name;

        public Vector3 Size;
        public float CameraHeightPercent;
        public Shape Shape = Shape.Cylinder;

        public Model Model;
        public LightData LightData;
    }
}