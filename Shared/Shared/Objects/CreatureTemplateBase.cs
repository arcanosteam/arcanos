﻿namespace Arcanos.Shared.Objects
{
    public abstract class CreatureTemplateBase : WorldObjectTemplateBase
    {
        public IntBounds Level = new IntBounds(1);
        public FactionBase Faction;
        public CreatureFamily Family;
        public CreatureClassification Classification;

        public IntBounds CurrentHealth;
        public IntBounds MaxHealth = new IntBounds(1);
        public IntBounds[] CurrentPower = new IntBounds[(byte)PowerType.Max];
        public IntBounds[] MaxPower = new IntBounds[(byte)PowerType.Max];
        public bool[] UsesPowerType = new bool[(byte)PowerType.Max];

        public float[] MovementSpeed = new float[(byte)MovementType.Max];
        public MovementType CurrentMovementType = MovementType.Run;
        public MovementTypeMask AllowedMovementTypes = MovementTypeMask.Ground;

        protected CreatureTemplateBase()
        {
            CameraHeightPercent = 0.75f;
        }

        public bool IsMovementTypeAllowed(MovementType type)
        {
            return (AllowedMovementTypes & (MovementTypeMask)(1 << (byte)type)) != 0;
        }
        public bool CanWalk()
        {
            return IsMovementTypeAllowed(MovementType.Walk);
        }
        public bool CanRun()
        {
            return IsMovementTypeAllowed(MovementType.Run);
        }
        public bool CanSwim()
        {
            return IsMovementTypeAllowed(MovementType.Swim);
        }
        public bool CanFly()
        {
            return IsMovementTypeAllowed(MovementType.Fly);
        }
        public float GetMovementSpeed()
        {
            return MovementSpeed[(byte)CurrentMovementType];
        }
    }
}