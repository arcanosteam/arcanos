﻿using Microsoft.Xna.Framework;

namespace Arcanos.Shared.Objects
{
    public interface IWorldEntity
    {
        Vector3 GetPosition();
        Rotation GetRotation();
        Vector3 GetSize();
    }
}