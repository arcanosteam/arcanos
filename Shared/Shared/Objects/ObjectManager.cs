﻿using System;
using System.Collections.Generic;
using Arcanos.Shared.Net;
using Arcanos.Shared.Spells;
using Arcanos.Shared.World;
using Arcanos.Utilities;

namespace Arcanos.Shared.Objects
{
    public class InvalidGUIDException : Exception
    {
        public InvalidGUIDException(int guid) : base(string.Format("GUID:{0} is invalid. GUID should be larger then zero.", guid)) { }
    }
    public interface IReferencedByGUID
    {
        ulong GUID { get; set; }
    }
    public interface IReferencedByID
    {
        int ID { get; set; }
    }
    public static class ObjectManager
    {
        private struct PacketQueueEntry
        {
            public WorldPackets Type;
            public IWorldPacket Packet;
            public byte[] RawData;
        }
        private static readonly Dictionary<ulong, IReferencedByGUID> guidPool = new Dictionary<ulong, IReferencedByGUID>();
        private static readonly Dictionary<ulong, Queue<PacketQueueEntry>> packetQueue = new Dictionary<ulong, Queue<PacketQueueEntry>>();
        private static readonly List<ulong> finalized = new List<ulong>();
        private static ulong nextGUID = 1;

        public static ulong Register(ulong guid, IReferencedByGUID obj)
        {
            if (guid <= 0)
                return Register(obj);
                //throw new InvalidGUIDException(guid);
            guidPool.Add(guid, obj);
            if (guid >= nextGUID)
                nextGUID = guid + 1;
            return guid;
        }
        public static ulong Register(IReferencedByGUID obj)
        {
            guidPool.Add(nextGUID, obj);
            return nextGUID++;
        }
        public static void Unregister(ulong guid)
        {
            guidPool.Remove(guid);
        }

        public static IReferencedByGUID GetReference(ulong guid)
        {
            IReferencedByGUID obj;
            if (guidPool.TryGetValue(guid, out obj))
                return obj;
            return null;
        }
        public static GameObject GetObject(ulong guid)
        {
            IReferencedByGUID obj;
            if (guidPool.TryGetValue(guid, out obj))
                return obj as GameObject;
            return null;
        }
        public static WorldObjectBase GetWorldObject(ulong guid)
        {
            IReferencedByGUID obj;
            if (guidPool.TryGetValue(guid, out obj))
                return obj as WorldObjectBase;
            return null;
        }
        public static CreatureBase GetCreature(ulong guid)
        {
            IReferencedByGUID obj;
            if (guidPool.TryGetValue(guid, out obj))
                return obj as CreatureBase;
            return null;
        }
        public static PlayerCreatureBase GetPlayer(ulong guid)
        {
            IReferencedByGUID obj;
            if (guidPool.TryGetValue(guid, out obj))
                return obj as PlayerCreatureBase;
            return null;
        }
        public static MapBase GetMap(ulong guid)
        {
            IReferencedByGUID obj;
            if (guidPool.TryGetValue(guid, out obj))
                return obj as MapBase;
            return null;
        }
        public static AreaEffectBase GetAreaEffect(ulong guid)
        {
            IReferencedByGUID obj;
            if (guidPool.TryGetValue(guid, out obj))
                return obj as AreaEffectBase;
            return null;
        }

        public static void EnqueuePacket(ulong guid, WorldPackets type, IWorldPacket packet)
        {
            EnqueuePacket(guid, type, packet, null);
        }
        public static void EnqueuePacket(ulong guid, WorldPackets type, IWorldPacket packet, byte[] rawData)
        {
            Queue<PacketQueueEntry> queue;
            if (!packetQueue.TryGetValue(guid, out queue))
            {
                Logger.Warning(LogCategory.Packets, "ObjectManager::EnqueuePacket: Trying to enqueue packet of type {1} for object with GUID {0} that is not marked for loading", guid, type);
                return;
            }
            /*{
                queue = new Queue<PacketQueueEntry>();
                packetQueue.Add(guid, queue);
            }*/
            queue.Enqueue(new PacketQueueEntry { Type = type, Packet = packet, RawData = rawData });
        }
        public static void ProcessPackets(PlayerSessionBase session, ulong guid)
        {
            Queue<PacketQueueEntry> queue;
            if (!packetQueue.TryGetValue(guid, out queue))
                return;
            while (queue.Count != 0)
            {
                PacketQueueEntry packet = queue.Dequeue();
                session.ProcessPacket(packet.Type, packet.Packet, packet.RawData);
            }
            packetQueue.Remove(guid);
            WorldObjectBase wo = GetWorldObject(guid);
            if (wo != null)
                wo.Finalized = true;
        }
        public static void MarkAsLoading(ulong guid)
        {
            if (!packetQueue.ContainsKey(guid))
                packetQueue.Add(guid, new Queue<PacketQueueEntry>());
        }
        public static void FinalizeLoading(ulong guid)
        {
            finalized.Add(guid);
        }
        public static bool IsFinalized(WorldObjectBase wo)
        {
            return wo.Finalized || finalized.Contains(wo.GUID);
        }
    }
}