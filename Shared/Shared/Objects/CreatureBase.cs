﻿using System;
using Arcanos.Shared.Collisions;
using Arcanos.Shared.Graphics;
using Arcanos.Shared.Movement;
using Arcanos.Shared.Spells;
using Microsoft.Xna.Framework;

namespace Arcanos.Shared.Objects
{
    public abstract class CreatureBase : WorldObjectBase
    {
        public new CreatureTemplateBase Template { get { return (CreatureTemplateBase)template; } }

        public int Level;
        public CreatureState State = CreatureState.JustSpawned;
        public bool IsAlive
        {
            get { return State < CreatureState.JustDied; }
        }
        public bool IsInCombat;

        public FactionBase Faction;

        public int CurrentHealth;
        public int MaxHealth;
        public virtual float CurrentHealthPercent
        {
            get { return (float)CurrentHealth / MaxHealth; }
            set { SetHealth((int)(value * MaxHealth)); }
        }
        public readonly int[] CurrentPower = new int[(byte)PowerType.Max];
        public readonly int[] MaxPower = new int[(byte)PowerType.Max];
        public readonly bool[] UsesPowerType = new bool[(byte)PowerType.Max];

        public SpellBookBase SpellBook;

        public readonly float[] MovementSpeed = new float[(byte)MovementType.Max];
        public readonly float[] MovementSpeedFactor = new float[(byte)MovementType.Max];
        public MovementType CurrentMovementType = MovementType.Run;
        public MovementTypeMask AllowedMovementTypes = MovementTypeMask.Ground;
        public MoverBase Mover { get; protected set; }

        public CreatureBase Target;

        protected CreatureBase(ulong guid, CreatureTemplateBase template, Vector3 pos, Rotation rot) : base(guid, template, pos, rot)
        {
            Type = ObjectType.Creature;
        }

        protected override void ResetFromTemplate()
        {
            base.ResetFromTemplate();

            Level = Template.Level.Random;
            State = CreatureState.JustSpawned;
            IsInCombat = false;

            Faction = Template.Faction;

            MaxHealth = Template.MaxHealth.Random;
            CurrentHealth = Template.CurrentHealth.Empty ? MaxHealth : Template.CurrentHealth.Random;
            for (byte type = 0; type < (byte)PowerType.Max; type++)
            {
                UsesPowerType[type] = Template.UsesPowerType[type];
                MaxPower[type] = Template.MaxPower[type].Random;
                CurrentPower[type] = Template.CurrentPower[type].Empty ? MaxPower[type] : Template.CurrentPower[type].Random;
            }

            for (byte type = 0; type < (byte)MovementType.Max; type++)
            {
                MovementSpeed[type] = Template.MovementSpeed[type];
                MovementSpeedFactor[type] = 1;
                CurrentMovementType = Template.CurrentMovementType;
                AllowedMovementTypes = Template.AllowedMovementTypes;
            }

            Target = null;
        }

        public override bool Update()
        {
            if (!base.Update())
                return false;

            if (IsAlive)
                Mover.Update();

            return true;
        }

        public virtual void InflictDamage(CreatureBase attacker, Damage damage)
        {
            ModityHealth(-damage.Sum);
        }
        public virtual void Heal(CreatureBase healer, int amount)
        {
            ModityHealth(amount);
        }
        public virtual void KilledCreature(CreatureBase victim)
        {
        }
        public virtual void Died(CreatureBase killer)
        {
            if (Model != null)
                Model.Origin = ModelOrigin.Bottom;
        }
        public virtual void MeleeHit(CreatureBase attacker, AttackType type)
        {
        }
        public virtual void MeleeMiss(CreatureBase attacker, AttackType type)
        {
        }
        public virtual void SpellHit(CreatureBase caster, SpellBase spell)
        {
        }
        public virtual void SpellMiss(CreatureBase caster, SpellBase spell)
        {
        }
        public virtual void SpellCasted(SpellTemplateBase spell, CreatureBase target)
        {
        }
        public virtual void SpellCasted(SpellTemplateBase spell, Vector3 target)
        {
        }
        public virtual void SpellCastingStarted(SpellTemplateBase spell, CreatureBase target)
        {
        }
        public virtual void SpellCastingStarted(SpellTemplateBase spell, Vector3 target)
        {
        }
        public virtual void SpellCastingFailed(SpellTemplateBase spell, CreatureBase target, CastSpellResult result)
        {
        }
        public virtual void SpellCastingFailed(SpellTemplateBase spell, Vector3 target, CastSpellResult result)
        {
        }
        public virtual void AuraApplied(AuraBase aura)
        {
        }
        public virtual void AuraRemoved(AuraBase aura)
        {
        }

        public virtual void SetHealth(int health)
        {
            CurrentHealth = health;
            if (CurrentHealth < 0)
                CurrentHealth = 0;
            if (CurrentHealth > MaxHealth)
                CurrentHealth = MaxHealth;
        }
        public virtual void SetHealth(int currentHealth, int maxHealth)
        {
            MaxHealth = maxHealth;
            CurrentHealth = currentHealth;
            if (MaxHealth < 1)
                MaxHealth = 1;
            if (CurrentHealth < 0)
                CurrentHealth = 0;
            if (CurrentHealth > MaxHealth)
                CurrentHealth = MaxHealth;
        }
        public void ModityHealth(int amount)
        {
            SetHealth(CurrentHealth + amount);
        }
        public virtual void SetUsesPower(PowerType type, bool uses)
        {
            UsesPowerType[(byte)type] = uses;
        }
        public virtual void SetPower(PowerType type, int power)
        {
            CurrentPower[(byte)type] = power;
            if (CurrentPower[(byte)type] < 0)
                CurrentPower[(byte)type] = 0;
            if (CurrentPower[(byte)type] > MaxPower[(byte)type])
                CurrentPower[(byte)type] = MaxPower[(byte)type];
        }
        public virtual void SetPower(PowerType type, int currentPower, int maxPower)
        {
            CurrentPower[(byte)type] = currentPower;
            MaxPower[(byte)type] = maxPower;
            if (MaxPower[(byte)type] < 0)
                MaxPower[(byte)type] = 0;
            if (CurrentPower[(byte)type] < 0)
                CurrentPower[(byte)type] = 0;
            if (CurrentPower[(byte)type] > MaxPower[(byte)type])
                CurrentPower[(byte)type] = MaxPower[(byte)type];
        }
        public void ModityPower(PowerType type, int amount)
        {
            SetPower(type, CurrentPower[(byte)type] + amount);
        }
        public virtual void SetState(CreatureState state)
        {
            State = state;
        }
        public virtual void SetTarget(CreatureBase target)
        {
            Target = target;
        }
        public virtual void SetMovementType(MovementType type)
        {
            if (!IsMovementTypeAllowed(type))
                return;

            CurrentMovementType = type;
        }
        public virtual void SetMovementSpeedFactor(MovementType type, float factor)
        {
            MovementSpeedFactor[(byte)type] = factor;
            Mover.UpdateSpeed();
        }

        public bool IsMovementTypeAllowed(MovementType type)
        {
            return (AllowedMovementTypes & (MovementTypeMask)(1 << (byte)type)) != 0;
        }
        public bool CanWalk()
        {
            return IsMovementTypeAllowed(MovementType.Walk);
        }
        public bool CanRun()
        {
            return IsMovementTypeAllowed(MovementType.Run);
        }
        public bool CanSwim()
        {
            return IsMovementTypeAllowed(MovementType.Swim);
        }
        public bool CanFly()
        {
            return IsMovementTypeAllowed(MovementType.Fly);
        }
        public float GetDefaultMovementSpeed()
        {
            return MovementSpeed[(byte)CurrentMovementType];
        }
        public float GetMovementSpeed()
        {
            return GetDefaultMovementSpeed() * MovementSpeedFactor[(byte)CurrentMovementType];
        }

        public virtual Reaction GetReactionTo(CreatureBase creature)
        {
            if (creature == this)
                return Reaction.Friendly;
            return GetReactionTo(creature.Faction);
        }
        public virtual Reaction GetReactionTo(FactionBase faction)
        {
            if (Faction == null)
                return faction != null ? faction.GetReactionFrom(Faction) : Reaction.Neutral;

            return Faction.GetReactionTo(faction);
        }
        public virtual Reaction GetReactionFrom(CreatureBase creature)
        {
            if (creature == this)
                return Reaction.Friendly;
            return GetReactionFrom(creature.Faction);
        }
        public virtual Reaction GetReactionFrom(FactionBase faction)
        {
            if (Faction == null)
                return faction != null ? faction.GetReactionTo(Faction) : Reaction.Neutral;

            return faction.GetReactionTo(Faction);
        }
        public virtual bool CanSee(WorldObjectBase target)
        {
            if (target == this)
                return true;
            if (target == null)
                return false;
            if (target.Map != Map)
                return false;
            if (PhaseMask != 0 && target.PhaseMask != 0 && (target.PhaseMask & PhaseMask) == 0)
                return false;
            if (target.Type.IsCreature())
                foreach (AuraBase aura in (target as CreatureBase).SpellBook.GetAuras(AuraType.Invisibility))
                    if (aura.Effect.FloatValue1 == 1 || R.Float() >= aura.Effect.FloatValue1)
                        return false;

            return true;
        }
        public bool IsInLineOfSight(WorldObjectBase target)
        {
            if (Map != target.Map)
                return false;
            return IsInLineOfSight(target.Position);
        }
        public bool IsInLineOfSight(Vector3 pos)
        {
            // TODO: LOS check
            return true;
        }

        public override void Respawn()
        {
            base.Respawn();
            SetState(CreatureState.JustSpawned);
        }
        public override void Despawn()
        {
            SetState(CreatureState.Despawned);
            base.Despawn();
        }

        public Vector3 GetSpellCastOffset()
        {
            switch (Shape)
            {
                case Shape.None:
                case Shape.Box:
                case Shape.Sphere:
                    return Vector3.Zero;
                case Shape.Cylinder:
                    return new Vector3(0, 0, GetHeight() / 2);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        public Vector3 GetSpellHitOffset()
        {
            switch (Shape)
            {
                case Shape.None:
                case Shape.Box:
                case Shape.Sphere:
                    return Vector3.Zero;
                case Shape.Cylinder:
                    return new Vector3(0, 0, GetHeight() / 2);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}