﻿using Arcanos.Shared.Timing;
using Microsoft.Xna.Framework;

namespace Arcanos.Shared.Movement
{
    public struct PointWithSpeedMovement : IMovementType, ISpeedBasedMovement
    {
        public Vector3 EndPosition;
        public float Speed;

        public PointWithSpeedMovement(Vector3 endPos, float speed)
        {
            EndPosition = endPos;
            Speed = speed;
        }

        public void SetSpeed(float speed)
        {
            Speed = speed;
        }

        public bool DoMovement(ref Vector3 currentPos, MoverBase mover)
        {
            if (currentPos.AlmostEquals(EndPosition))
                return false;

            float moveDist = Speed * Time.PerSecond;

            // Check if arrives during this update
            float dist;
            Vector3.Distance(ref currentPos, ref EndPosition, out dist);
            if (moveDist >= dist)
            {
                currentPos = EndPosition;
                return true;
            }

            // Calculate next position
            currentPos += Vector3.Normalize(Vector3.Subtract(EndPosition, currentPos)) * moveDist;
            return true;
        }
        public bool DoRotation(ref Rotation currentRot, MoverBase mover)
        {
            if (!mover.HasMoved)
                return false;

            Rotation rot = mover.Owner.Position.GetRotationTo(EndPosition);

            if (currentRot.AlmostEquals(rot))
                return false;

            currentRot = rot;
            return true;
        }
    }
}