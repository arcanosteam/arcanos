﻿namespace Arcanos.Shared.Movement
{
    public enum MovementValidity : byte
    {
        Ok,

        MovedTooFar,
        CollisionWithTerrain,
        CollisionWithSolidStatic,
        SlopeTooSteep,
        Incapable,
    }
}