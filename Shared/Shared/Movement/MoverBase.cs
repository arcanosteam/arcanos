﻿using System;
using System.Runtime.InteropServices;
using Arcanos.Shared.Data;
using Arcanos.Shared.Net;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Timing;
using Microsoft.Xna.Framework;

namespace Arcanos.Shared.Movement
{
    public interface IMovementType
    {
        bool DoMovement(ref Vector3 currentPos, MoverBase mover);
        bool DoRotation(ref Rotation currentRot, MoverBase mover);
    }
    public interface ISpeedBasedMovement
    {
        void SetSpeed(float speed);
    }
    public abstract class MoverBase
    {
        private const float GROUND_WALK_THRESHOLD = 1;

        private static readonly Type[] MovementTypes =
        {
            null,
            typeof(PointOverTimeMovement),
            typeof(PointWithSpeedMovement),
            typeof(ClosestPointWithSpeedMovement),
        };

        public IMovementType MovementType;
        [SerializationIgnore]
        public readonly CreatureBase Owner;
        public float TimePassed;

        [SerializationIgnore]
        public bool Finished { get; private set; }
        [SerializationIgnore]
        public bool HasMoved { get; private set; }
        [SerializationIgnore]
        public bool HasRotated { get; private set; }

        protected MoverBase(CreatureBase owner)
        {
            Owner = owner;
            Finished = true;
        }

        public virtual void Update()
        {
            if (MovementType == null || Owner.HasFlag(ObjectFlags.Stunned))
                return;

            TimePassed += Time.PerSecond;

            Vector3 pos = Owner.Position;
            Rotation rot = Owner.Rotation;
            if (!Owner.HasFlag(ObjectFlags.Rooted) && MovementType.DoMovement(ref pos, this))
            {
                ValidatePosition(ref pos);
                HasMoved = true;
                Owner.MoveTo(pos);
            }
            else
                HasMoved = false;
            if (MovementType.DoRotation(ref rot, this))
            {
                HasRotated = true;
                Owner.RotateTo(rot);
            }
            else
                HasRotated = false;
            Finished = !HasMoved && !HasRotated;
        }

        private void ValidatePosition(ref Vector3 pos)
        {
            float groundZ = Owner.Map.Template.GetHeight(pos.X, pos.Y);
            if (pos.X < 0)
                pos.X = 0;
            if (pos.Y < 0)
                pos.Y = 0;
            if (pos.Z < groundZ)
                pos.Z = groundZ;

            if (pos.X >= Owner.Map.Template.SizeX)
                pos.X = Owner.Map.Template.SizeX;
            if (pos.Y >= Owner.Map.Template.SizeY)
                pos.Y = Owner.Map.Template.SizeY;
            if (!Owner.CanFly())
            {
                if (pos.Z < groundZ + GROUND_WALK_THRESHOLD)
                    pos.Z = groundZ;
            }
        }

        public virtual void Reset()
        {
            TimePassed = 0;
            Finished = true;
        }
        public virtual void SetType(IMovementType type)
        {
            MovementType = type;
            Reset();
        }
        public void Clear()
        {
            SetType(null);
        }

        public virtual void UpdateSpeed()
        {
            ISpeedBasedMovement type = MovementType as ISpeedBasedMovement;
            if (type != null)
                type.SetSpeed(Owner.GetMovementSpeed());
        }

        public void Stop()
        {
            SetType(null);
        }
        public void MoveToPointOverTime(Vector3 pos, float time)
        {
            SetType(new PointOverTimeMovement(Owner.Position, pos, time));
        }
        public void MoveToPointWithSpeed(Vector3 pos)
        {
            if (MovementType is PointWithSpeedMovement &&
                ((PointWithSpeedMovement)MovementType).EndPosition == pos)
                return;
            SetType(new PointWithSpeedMovement(pos, Owner.GetMovementSpeed()));
        }
        public void MoveClosestToPointWithSpeed(Vector3 pos, float minRadius)
        {
            if (MovementType is ClosestPointWithSpeedMovement &&
                ((ClosestPointWithSpeedMovement)MovementType).EndPosition == pos &&
                ((ClosestPointWithSpeedMovement)MovementType).Radius == minRadius)
                return;
            SetType(new ClosestPointWithSpeedMovement(pos, minRadius, Owner.GetMovementSpeed()));
        }

        public byte GetTypeID()
        {
            if (MovementType == null)
                return 0;

            Type type = MovementType.GetType();
            int length = MovementTypes.Length;
            for (byte i = 0; i < length; ++i)
                if (MovementTypes[i] == type)
                    return i;

            return 0xFF;
        }
        public void FillPacket(ref SCreatureMovementUpdatePacket packet)
        {
            if ((packet.Type = GetTypeID()) == 0)
                return;

            int length = Marshal.SizeOf(MovementType);
            if (length > packet.Data.Length)
                throw new Exception();

            IntPtr ptr = Marshal.AllocHGlobal(length);
            Marshal.StructureToPtr(MovementType, ptr, false);
            Marshal.Copy(ptr, packet.Data, 0, length);
            Marshal.FreeHGlobal(ptr);
        }
        public void FillFromPacket(ref SCreatureMovementUpdatePacket packet)
        {
            if (packet.Type == 0)
            {
                MovementType = null;
                Finished = true;
                return;
            }
            Type type = MovementTypes[packet.Type];

            GCHandle dataHandle = GCHandle.Alloc(packet.Data, GCHandleType.Pinned);
            object data = Marshal.PtrToStructure(dataHandle.AddrOfPinnedObject(), type);
            dataHandle.Free();

            MovementType = (IMovementType)data;
        }
    }
}