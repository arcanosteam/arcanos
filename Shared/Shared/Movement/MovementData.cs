﻿using System.Runtime.InteropServices;
using Microsoft.Xna.Framework;

namespace Arcanos.Shared.Movement
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct MovementData
    {
        public Vector3 Position;
        public Rotation Rotation;
        public MovementFlags Flags;
        public Vector3 Velocity;
        public float FallDuration,
                     FallDistance;
    }
}