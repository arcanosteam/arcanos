﻿using Microsoft.Xna.Framework;

namespace Arcanos.Shared.Movement
{
    public struct PointOverTimeMovement : IMovementType
    {
        public Vector3 StartPosition;
        public Vector3 EndPosition;
        public float Time;

        public PointOverTimeMovement(Vector3 startPos, Vector3 endPos, float time)
        {
            StartPosition = startPos;
            EndPosition = endPos;
            Time = time;
        }

        public bool DoMovement(ref Vector3 currentPos, MoverBase mover)
        {
            if (mover.TimePassed > Time)
                return false;
            Vector3.Lerp(ref StartPosition, ref EndPosition, mover.TimePassed / Time, out currentPos);
            return true;
        }
        public bool DoRotation(ref Rotation currentRot, MoverBase mover)
        {
            return false;
        }
    }
}