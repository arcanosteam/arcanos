﻿using System;

namespace Arcanos.Shared.Movement
{
    [Flags]
    public enum MovementFlags : ushort
    {
        Forward         = 0x0001,
        Backward        = 0x0002,
        Left            = 0x0004,
        Right           = 0x0008,
        Up              = 0x0010,
        Down            = 0x0020,
        Walk            = 0x0100,
        Jump            = 0x0200,
        Fly             = 0x0400,
        Swim            = 0x0800,
        RotationLeft    = 0x1000,
        RotationRight   = 0x2000,
        RotationUp      = 0x4000,
        RotationDown    = 0x8000,

        HorizontalDirections    = Forward | Backward | Left | Right,
        VerticalDirections      = Up | Down,
        Directions              = HorizontalDirections | VerticalDirections,

        Movement                = Walk | Jump | Fly | Swim,
        FreeVerticalMovement    = Fly | Swim,
        Rotation                = RotationLeft | RotationRight | RotationUp | RotationDown,
    }
}