﻿using Arcanos.Shared.Timing;
using Microsoft.Xna.Framework;

namespace Arcanos.Shared.Movement
{
    public struct ClosestPointWithSpeedMovement : IMovementType, ISpeedBasedMovement
    {
        public Vector3 EndPosition;
        public float Radius;
        public float Speed;

        public ClosestPointWithSpeedMovement(Vector3 endPos, float radius, float speed)
        {
            EndPosition = endPos;
            Radius = radius;
            Speed = speed;
        }

        public void SetSpeed(float speed)
        {
            Speed = speed;
        }

        public bool DoMovement(ref Vector3 currentPos, MoverBase mover)
        {
            const float EPSILON = 0.0001f;
            float moveDist = Speed * Time.PerSecond;

            // Check if arrives during this update
            float dist;
            Vector3.Distance(ref currentPos, ref EndPosition, out dist);
            dist -= Radius;
            if (dist <= EPSILON)
                return false;
            if (moveDist - dist >= EPSILON)
            {
                currentPos += Vector3.Normalize(Vector3.Subtract(EndPosition, currentPos)) * dist;
                return true;
            }

            // Calculate next position
            currentPos += Vector3.Normalize(Vector3.Subtract(EndPosition, currentPos)) * moveDist;
            return true;
        }
        public bool DoRotation(ref Rotation currentRot, MoverBase mover)
        {
            if (!mover.HasMoved)
                return false;

            Rotation rot = mover.Owner.Position.GetRotationTo(EndPosition);

            if (currentRot.AlmostEquals(rot))
                return false;

            currentRot = rot;
            return true;
        }
    }
}