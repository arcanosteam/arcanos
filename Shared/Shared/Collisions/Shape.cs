﻿namespace Arcanos.Shared.Collisions
{
    public enum Shape
    {
        None,
        /// <summary>
        /// Axis-aligned bounding box. Uses all 3 units of Size vector. Center is assumed on the bottom of the box.
        /// </summary>
        Box,
        /// <summary>
        /// Uses z-axis as a radius. Center is assumed in the center of the sphere.
        /// </summary>
        Sphere,
        /// <summary>
        /// Uses x-axis as a radius and z-axis as a height. Center is assumed on the bottom of the cylinder.
        /// </summary>
        Cylinder,
    }
}