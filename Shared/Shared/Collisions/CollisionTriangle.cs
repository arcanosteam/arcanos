﻿using Microsoft.Xna.Framework;

namespace Arcanos.Shared.Collisions
{
    public struct CollisionTriangle
    {
        public Vector3 A, B, C;

        public CollisionTriangle(Vector3 a, Vector3 b, Vector3 c)
        {
            A = a;
            B = b;
            C = c;
        }
    }
}