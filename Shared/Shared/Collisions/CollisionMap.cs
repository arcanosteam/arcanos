using System;
using System.Collections.Generic;
using Arcanos.Shared.Objects;
using Arcanos.Shared.World;
using Microsoft.Xna.Framework;

namespace Arcanos.Shared.Collisions
{
    public class CollisionMap
    {
        public const int DEFAULT_STATIC_GEOMETRY_CAPACITY = 1024;
        public const float CONTACT_CHECK_ITERATION_DISTANCE = 0.1f;

        // TODO: Divide StaticGeometry into grid
        public CollisionTriangle[] StaticGeometry = new CollisionTriangle[DEFAULT_STATIC_GEOMETRY_CAPACITY];
        public int StaticGeometryCount = 0;
        public readonly Dictionary<object, CollisionTriangle[]> DynamicGeometry = new Dictionary<object, CollisionTriangle[]>();

        public CollisionMap(MapTemplateBase map)
        {
            //map.HeightMap
        }

        public void AddStatic(params CollisionTriangle[] geometry)
        {
            int addedLen = geometry.Length;
            if (StaticGeometryCount + addedLen > StaticGeometry.Length)
                Array.Resize(ref StaticGeometry, StaticGeometryCount + addedLen);
            Array.Copy(geometry, 0, StaticGeometry, StaticGeometryCount, addedLen);
            StaticGeometryCount += addedLen;
        }
        public void AddDynamic(object owner, params CollisionTriangle[] geometry)
        {
            int addedLen = geometry.Length;
            CollisionTriangle[] storedGeometry;
            if (DynamicGeometry.TryGetValue(owner, out storedGeometry))
            {
                int origLen = storedGeometry.Length;
                Array.Resize(ref storedGeometry, origLen + addedLen);
                Array.Copy(geometry, 0, storedGeometry, origLen, addedLen);
            }
            else
            {
                storedGeometry = new CollisionTriangle[addedLen];
                Array.Copy(geometry, storedGeometry, addedLen);
                DynamicGeometry.Add(owner, storedGeometry);
            }
        }

        public Vector3 GetContactPoint(WorldObjectBase wo, Vector3 from, Vector3 to)
        {
            Vector3 d = to - from;
            Vector3 direction = Vector3.Normalize(d);
            Vector3 step = direction * CONTACT_CHECK_ITERATION_DISTANCE;
            int steps = (int)Math.Ceiling(Vector3.Distance(d, step));
            for (int i = 0; i < steps; i++)
            {
                Vector3 check = from + step * i;
                if (WillObjectIntersectWorld(wo, check))
                    return from + step * (i - 1);
            }
            return from;
        }

        public bool IsBoxIntersectingTriangle(Vector3 center, Vector3 size, CollisionTriangle triangle)
        {
            return triBoxOverlap(center, size / 2, new[]
            {
                triangle.A,
                triangle.B,
                triangle.C,
            });
        }

        private static bool planeBoxOverlap(Vector3 normal, Vector3 vertex, Vector3 halfSize)
        {
            Vector3 vMin, vMax;

            {
                float v = vertex.X;
                if (normal.X > 0.0f)
                {
                    vMin.X = -halfSize.X - v;
                    vMax.X = halfSize.X - v;
                }
                else
                {
                    vMin.X = halfSize.X - v;
                    vMax.X = -halfSize.X - v;
                }
            }
            {
                float v = vertex.Y;
                if (normal.Y > 0.0f)
                {
                    vMin.Y = -halfSize.Y - v;
                    vMax.Y = halfSize.Y - v;
                }
                else
                {
                    vMin.Y = halfSize.Y - v;
                    vMax.Y = -halfSize.Y - v;
                }
            }
            {
                float v = vertex.Z;
                if (normal.X > 0.0f)
                {
                    vMin.Z = -halfSize.Z - v;
                    vMax.Z = halfSize.Z - v;
                }
                else
                {
                    vMin.Z = halfSize.Z - v;
                    vMax.Z = -halfSize.Z - v;
                }
            }

            if (Vector3.Dot(normal, vMin) > 0.0f) return false;
            if (Vector3.Dot(normal, vMax) >= 0.0f) return true;
            return false;
        }
        private bool triBoxOverlap(Vector3 center, Vector3 halfSize, Vector3[] geometry)
        {
            float min, max;

            Vector3 v0 = geometry[0] - center;
            Vector3 v1 = geometry[1] - center;
            Vector3 v2 = geometry[2] - center;

            Vector3 e0 = v1 - v0;
            Vector3 e1 = v2 - v1;
            Vector3 e2 = v0 - v2;

            float fex = e0.X > 0 ? e0.X : -e0.X;
            float fey = e0.Y > 0 ? e0.Y : -e0.Y;
            float fez = e0.Z > 0 ? e0.Z : -e0.Z;

            float p0 = e0.Z * v0.Y - e0.Y * v0.Z;
            float p2 = e0.Z * v2.Y - e0.Y * v2.Z;
            if (p0 < p2) { min=p0; max=p2; } else { min=p2; max=p0; }
            float rad = fez * halfSize.Y + fey * halfSize.Z;
            if (min > rad || max < -rad) return false;

            p0 = -e0.Z * v0.X + e0.X * v0.Z;
            p2 = -e0.Z * v2.X + e0.X * v2.Z;
            if(p0 < p2) { min=p0; max=p2; } else { min=p2; max=p0; }
            rad = fez * halfSize.X + fex * halfSize.Z;
            if(min > rad || max < -rad) return false;

            float p1 = e0.Y * v1.X - e0.X * v1.Y;
            p2 = e0.Y * v2.X - e0.X * v2.Y;
            if (p2 < p1) { min = p2; max = p1; } else { min = p1; max = p2; }
            rad = fey * halfSize.X + fex * halfSize.Y;
            if (min > rad || max < -rad) return false;

            fex = e1.X > 0 ? e1.X : -e1.X;
            fey = e1.Y > 0 ? e1.Y : -e1.Y;
            fez = e1.Z > 0 ? e1.Z : -e1.Z;

            p0 = e1.Z * v0.Y - e1.Y * v0.Z;
            p2 = e1.Z * v2.Y - e1.Y * v2.Z;
            if (p0 < p2) { min = p0; max = p2; } else { min = p2; max = p0; }
            rad = fez * halfSize.Y + fey * halfSize.Z;
            if (min > rad || max < -rad) return false;

            p0 = -e1.Z * v0.X + e1.X * v0.Z;
            p2 = -e1.Z * v2.X + e1.X * v2.Z;
            if (p0 < p2) { min = p0; max = p2; } else { min = p2; max = p0; }
            rad = fez * halfSize.X + fex * halfSize.Z;
            if (min > rad || max < -rad) return false;

            p0 = e1.Y * v0.X - e1.X * v0.Y;
            p1 = e1.Y * v1.X - e1.X * v1.Y;
            if(p0 < p1) { min=p0; max=p1; } else { min=p1; max=p0; }
            rad = fey * halfSize.X + fex * halfSize.Y;
            if (min > rad || max < -rad) return false;

            fex = e2.X > 0 ? e2.X : -e2.X;
            fey = e2.Y > 0 ? e2.Y : -e2.Y;
            fez = e2.Z > 0 ? e2.Z : -e2.Z;

            p0 = e2.Z * v0.Y - e2.Y * v0.Z;
            p1 = e2.Z * v1.Y - e2.Y * v1.Z;
            if(p0 < p1) { min=p0; max=p1; } else { min=p1; max=p0; }
            rad = fez * halfSize.Y + fey * halfSize.Z;
            if(min > rad || max < -rad) return false;

            p0 = -e2.Z * v0.X + e2.X * v0.Z;
            p1 = -e2.Z * v1.X + e2.X * v1.Z;
            if(p0 < p1) { min=p0; max=p1; } else { min=p1; max=p0; }
            rad = fez * halfSize.X + fex * halfSize.Z;
            if(min > rad || max < -rad) return false;

            p1 = e2.Y * v1.X - e2.X * v1.Y;
            p2 = e2.Y * v2.X - e2.X * v2.Y;
            if(p2 < p1) { min=p2; max=p1; } else { min=p1; max=p2; }
            rad = fey * halfSize.X + fex * halfSize.Y;
            if(min > rad || max < -rad) return false;

            min = max = v0.X;
            if (v1.X < min) min = v1.X;
            if (v1.X > max) max = v1.X;
            if (v2.X < min) min = v2.X;
            if (v2.X > max) max = v2.X;
            if (min > halfSize.X || max < -halfSize.X) return false;

            min = max = v0.Y;
            if (v1.Y < min) min = v1.Y;
            if (v1.Y > max) max = v1.Y;
            if (v2.Y < min) min = v2.Y;
            if (v2.Y > max) max = v2.Y;
            if (min > halfSize.Y || max < -halfSize.Y) return false;

            min = max = v0.Z;
            if(v1.Z<min) min=v1.Z;
            if(v1.Z>max) max=v1.Z;
            if(v2.Z<min) min=v2.Z;
            if(v2.Z>max) max=v2.Z;
            if (min > halfSize.Z || max < -halfSize.Z) return false;

            Vector3 normal = Vector3.Cross(e0, e1);

            if (!planeBoxOverlap(normal, v0, halfSize)) return false;

            return true;

        }

        public bool IsBoxIntersectingStatic(Vector3 center, Vector3 size)
        {
            for (int i = 0; i < StaticGeometryCount; i++)
                if (IsBoxIntersectingTriangle(center, size, StaticGeometry[i]))
                    return true;
            return false;
        }
        public bool IsBoxIntersectingDynamic(Vector3 center, Vector3 size)
        {
            foreach (KeyValuePair<object, CollisionTriangle[]> dynamicObjGeometry in DynamicGeometry)
            {
                CollisionTriangle[] geometry = dynamicObjGeometry.Value;
                int len = geometry.Length;
                for (int i = 0; i < len; i++)
                    if (IsBoxIntersectingTriangle(center, size, geometry[i]))
                        return true;
            }
            return false;
        }
        public bool IsBoxIntersectingWorld(Vector3 center, Vector3 size)
        {
            return IsBoxIntersectingStatic(center, size) || IsBoxIntersectingDynamic(center, size);
        }

        public bool IsObjectIntersectingWorld(WorldObjectBase wo)
        {
            switch (wo.Shape)
            {
                case Shape.None:
                    return false; // TODO
                case Shape.Box:
                    return IsBoxIntersectingWorld(wo.Position, wo.Size);
                case Shape.Sphere:
                    return false; // TODO
                case Shape.Cylinder:
                    return false; // TODO
                default:
                    return false;
            }
        }
        public bool WillObjectIntersectWorld(WorldObjectBase wo, Vector3 position)
        {
            switch (wo.Shape)
            {
                case Shape.None:
                    return false; // TODO
                case Shape.Box:
                    return IsBoxIntersectingWorld(position, wo.Size);
                case Shape.Sphere:
                    return false; // TODO
                case Shape.Cylinder:
                    return false; // TODO
                default:
                    return false;
            }
        }
    }
}