﻿using System.Collections.Generic;
using Arcanos.Shared.World;

namespace Arcanos.Shared.Collisions
{
    public class CollisionManager
    {
        public Dictionary<ulong, CollisionMap> Maps = new Dictionary<ulong, CollisionMap>();

        public CollisionMap AddMap(MapBase map)
        {
            CollisionMap cmap = new CollisionMap(map.Template);
            Maps.Add(map.GUID, cmap);
            return cmap;
        }
    }
}