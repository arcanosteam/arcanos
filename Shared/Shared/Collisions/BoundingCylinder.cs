﻿using System;
using Microsoft.Xna.Framework;

namespace Arcanos.Shared.Collisions
{
    public struct BoundingCylinder
    {
        /// <summary>
        /// Specifies the bottom center of the BoundingCylinder.
        /// </summary>
        public Vector3 Position;

        /// <summary>
        /// The height of the BoundingCylinder.
        /// </summary>
        public float Height;

        /// <summary>
        /// The radius of the BoundingCylinder.
        /// </summary>
        public float Radius;

        /// <summary>
        /// Creates an instance of BoundingCylinder.
        /// </summary>
        /// <param name="position">Specifies the bottom center of the BoundingCylinder.</param>
        /// <param name="height">The height of the BoundingCylinder.</param>
        /// <param name="radius">The radius of the BoundingCylinder.</param>
        public BoundingCylinder(Vector3 position, float height, float radius)
        {
            Height = height;
            Radius = radius;
            Position = position;
        }

        /// <summary>
        /// Checks whether the current BoundingCylinder intersects a BoundingSphere.
        /// </summary>
        /// <param name="sphere">The BoundingSphere to check for intersection with</param>
        public bool Intersect(BoundingSphere sphere)
        {
            float cylinderMinY = Position.Y;
            float cylinderMaxY = Position.Y + Height;

            if (sphere.Center.Y > cylinderMaxY)
            {
                Vector3 direction = new Vector3(sphere.Center.X - Position.X, 0, sphere.Center.Z - Position.Z);
                direction.Normalize();

                Vector3 closestPoint = new Vector3(Position.X + direction.X * Radius, cylinderMaxY, Position.Z + direction.Z * Radius);
                if (Vector3.Distance(sphere.Center, closestPoint) <= Radius)
                    return true;
            }
            else if (sphere.Center.Y < cylinderMinY)
            {
                Vector3 direction = new Vector3(sphere.Center.X - Position.X, 0, sphere.Center.Z - Position.Z);
                direction.Normalize();

                Vector3 closestPoint = new Vector3(Position.X + direction.X * Radius, cylinderMinY, Position.Z + direction.Z * Radius);
                if (Vector3.Distance(sphere.Center, closestPoint) <= Radius)
                    return true;

            }
            else
            {
                if (Vector3.Distance(new Vector3(sphere.Center.X, 0, sphere.Center.Z), new Vector3(Position.X, 0, Position.Z)) <= Radius + sphere.Radius)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Checks whether the current BoundingCylinder intersects a Ray.
        /// </summary>
        /// <param name="ray">The Ray to check for intersection with.</param>
        public bool Intersect(Ray ray)
        {
            float dX = ray.Position.X - Position.X;
            float dZ = ray.Position.Z - Position.Z;

            float a = ray.Direction.X * ray.Direction.X + ray.Direction.Z * ray.Direction.Z;
            float b = 2 * (ray.Direction.X * dX + ray.Direction.Z * dZ);
            float c = dX * dX + dZ * dZ - (Radius * Radius);

            float t1 = (-b + (float)Math.Sqrt(b * b - 4 * a * c)) / (2 * a);
            float t2 = (-b - (float)Math.Sqrt(b * b - 4 * a * c)) / (2 * a);

            float t = (t1 < t2) ? t1 : t2;

            return t >= 0;
        }

        /// <summary>
        /// Checks whether the current BoundingCylinder intersects a Ray.
        /// </summary>
        /// <param name="ray">The Ray to check for intersection with.</param>
        /// <param name="result">[OutAttribute] Distance at which the ray intersects the BoundingBox, or null if there is no intersection.</param>
        public bool Intersect(Ray ray, ref float? result)
        {
            float dX = ray.Position.X - Position.X;
            float dZ = ray.Position.Z - Position.Z;

            float a = ray.Direction.X * ray.Direction.X + ray.Direction.Z * ray.Direction.Z;
            float b = 2 * (ray.Direction.X * dX + ray.Direction.Z * dZ);
            float c = dX * dX + dZ * dZ - (Radius * Radius);

            float t1 = (-b + (float)Math.Sqrt(b * b - 4 * a * c)) / (2 * a);
            float t2 = (-b - (float)Math.Sqrt(b * b - 4 * a * c)) / (2 * a);

            float t = (t1 < t2) ? t1 : t2;

            if (t >= 0)
            {
                result = t;
                return true;
            }
            result = null;
            return false;
        }

    }
}