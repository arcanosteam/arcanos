﻿using System.Collections.Generic;
using Arcanos.Shared.Objects;

namespace Arcanos.Shared.PvP
{
    public abstract class DuelBase
    {
        public static int MaxParticipants;

        public DuelStatus Status;

        public readonly Dictionary<PlayerCreatureBase, DuelParticipantStatus> Participants = new Dictionary<PlayerCreatureBase, DuelParticipantStatus>();

        public virtual bool AddParticipant(PlayerCreatureBase pc)
        {
            if (Participants.Count >= MaxParticipants)
                return false;

            if (Participants.ContainsKey(pc))
                return true;

            SendFactionUpdates(pc);
            Participants.Add(pc, DuelParticipantStatus.None);
            return true;
        }
        public virtual bool RemoveParticipant(PlayerCreatureBase pc)
        {
            if (!Participants.ContainsKey(pc))
                return true;

            Participants.Remove(pc);
            SendRestoreFaction(pc);
            return true;
        }

        public virtual void SetStatus(DuelStatus status)
        {
            Status = status;
        }
        public virtual void SetStatus(PlayerCreatureBase participant, DuelParticipantStatus status)
        {
            if (!Participants.ContainsKey(participant))
                return;

            Participants[participant] = status;

            if (status == DuelParticipantStatus.Lose)
            {
                bool winnerFound = false;
                PlayerCreatureBase winner = null;
                foreach (KeyValuePair<PlayerCreatureBase, DuelParticipantStatus> pair in Participants)
                {
                    if (pair.Value == DuelParticipantStatus.None)
                    {
                        if (!winnerFound)
                        {
                            winnerFound = true;
                            winner = pair.Key;
                        }
                        else
                            winner = null;
                    }
                }

                if (winnerFound && winner != null)
                {
                    SetStatus(winner, DuelParticipantStatus.Win);
                    SetStatus(DuelStatus.Finished);
                    RemoveParticipant(winner);
                }
            }
        }

        protected abstract void SendFactionUpdates(PlayerCreatureBase participant);
        protected abstract void SendRestoreFaction(PlayerCreatureBase participant);

        public virtual Reaction? GetReactionTo(CreatureBase creature)
        {
            if (!creature.Type.IsPlayer())
                return null;

            if (Participants.ContainsKey(creature as PlayerCreatureBase))
                return Reaction.Hostile;

            // TODO: Pet reaction here if they are implemented

            return null;
        }
        public virtual Reaction? GetReactionFrom(CreatureBase creature)
        {
            return GetReactionTo(creature);
        }
    }
}