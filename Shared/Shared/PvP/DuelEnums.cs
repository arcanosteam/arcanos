﻿namespace Arcanos.Shared.PvP
{
    public enum DuelResponse
    {
        Accept,
        Decline,
    }
    public enum DuelStatus
    {
        None,
        Started,
        Finished,
    }
    public enum DuelParticipantStatus
    {
        None,
        Win,
        Lose,
    }
}