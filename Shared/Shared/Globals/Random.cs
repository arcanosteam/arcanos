﻿using System;
using Microsoft.Xna.Framework;

namespace Arcanos.Shared
{
    public static class R
    {
        private static readonly Random random = new Random();

        public static T Object<T>(params T[] objects)
        {
            return objects[random.Next(objects.Length)];
        }
        public static bool Bool()
        {
            return random.Next() % 2 == 0;
        }
        public static float Float()
        {
            return (float)random.NextDouble();
        }
        public static float Float(float max)
        {
            return max == 0 ? 0 : (float)random.NextDouble() * max;
        }
        public static float Float(float min, float max)
        {
            return max == min ? min : min + (float)random.NextDouble() * (max - min);
        }
        public static double Double()
        {
            return random.NextDouble();
        }
        public static double Double(double max)
        {
            return max == 0 ? 0 : random.NextDouble() * max;
        }
        public static double Double(double min, double max)
        {
            return max == min ? min : min + random.NextDouble() * (max - min);
        }
        public static int Int(int max)
        {
            return random.Next(max);
        }
        public static int Int(int min, int max)
        {
            return random.Next(min, max);
        }
        public static int IntInclusive(int max)
        {
            return max == 0 ? 0 : random.Next(max + 1);
        }
        public static int IntInclusive(int min, int max)
        {
            return max == min ? min : random.Next(min, max + 1);
        }
        public static Vector3 Radius2D(Vector3 center, float radius)
        {
            float d = (float)random.NextDouble() * MathHelper.TwoPi;
            return Vector3.Add(center, new Vector3((float)Math.Cos(d) * radius, (float)Math.Sin(d) * radius, 0));
        }
        public static Vector3 Radius3D(Vector3 center, float radius)
        {
            float d = (float)random.NextDouble() * MathHelper.TwoPi;
            float dz = (float)random.NextDouble() * MathHelper.TwoPi;
            return Vector3.Add(center, new Vector3((float)Math.Cos(d) * radius, (float)Math.Sin(d) * radius, (float)Math.Sin(dz) * radius));
        }
        public static Vector2 Random2D()
        {
            return new Vector2(Float(), Float());
        }
        public static Vector3 Random3D()
        {
            return new Vector3(Float(), Float(), Float());
        }
        public static Vector4 Random4D()
        {
            return new Vector4(Float(), Float(), Float(), Float());
        }
        public static Rotation Yaw()
        {
            return new Rotation(Float() * MathHelper.TwoPi);
        }
        public static T Array<T>(T[] array)
        {
            return array[random.Next(array.Length)];
        }
    }
}