﻿using System;
using System.Collections.Generic;
using Arcanos.Shared.Data;

namespace Arcanos.Shared
{
    [SerializationSettings(Method = SerializationMethod.RefByID, ReferenceResolverType = typeof(FactionBase), ReferenceResolverMethod = "ReferenceResolver")]
    public abstract class FactionBase : Template
    {
        public static Func<int, FactionBase> LoadHandler;

        public override EditorEntry EditorEntry
        {
            get { return new EditorEntry(ID, Name); }
        }

        public string Name;
        public readonly Dictionary<int, Reaction> Reactions = new Dictionary<int, Reaction>();
        public Reaction ReactionToOthers = Reaction.Hostile;
        public Reaction ReactionInsideFaction = Reaction.Friendly;
        public bool ForceSameReactionFromOthers = false;

        protected FactionBase() { }
        protected FactionBase(string name)
        {
            Name = name;
        }
        protected FactionBase(string name, Reaction reactionToOthers)
        {
            Name = name;
            ReactionToOthers = reactionToOthers;
        }

        public Reaction GetReactionTo(FactionBase faction)
        {
            if (faction == null)
                return ReactionToOthers;

            if (faction == this)
                return ReactionInsideFaction;

            if (faction.ForceSameReactionFromOthers)
                return faction.ReactionToOthers;

            Reaction result;
            if (Reactions.TryGetValue(faction.ID, out result))
                return result;

            return ReactionToOthers;
        }
        public Reaction GetReactionFrom(FactionBase faction)
        {
            if (faction == null)
            {
                if (ForceSameReactionFromOthers)
                    return ReactionToOthers;

                return Reaction.Neutral;
            }

            return faction.GetReactionTo(this);
        }

        internal static object ReferenceResolver(int id)
        {
            return LoadHandler(id);
        }
    }
}