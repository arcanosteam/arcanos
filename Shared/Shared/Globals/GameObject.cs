﻿using Arcanos.Shared.Data;
using Arcanos.Shared.Objects;

namespace Arcanos.Shared
{
    [SerializationSettings(Method = SerializationMethod.RefByGUID)]
    public abstract class GameObject : IReferencedByGUID
    {
        [SerializationIgnore]
        private ulong cachedGUID;
        [SerializationIgnore]
        public ulong GUID
        {
            get { return cachedGUID; }
            set { cachedGUID = value; }
        }

        protected void RegisterGUID(ulong guid)
        {
            GUID = ObjectManager.Register(guid, this);
        }
    }
}