﻿namespace Arcanos.Shared
{
    public enum CreatureFamily
    {
        Unknown,
        Humanoid,
        Animal,
        Elemental,
    }
}