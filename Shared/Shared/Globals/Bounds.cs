﻿using System;

namespace Arcanos.Shared
{
    public struct FloatBounds
    {
        public static readonly FloatBounds Infinite = new FloatBounds(0, float.MaxValue);

        public float Min, Max;
        
        public float Random
        {
            get { return R.Float(Min, Max); }
        }
        public int RandomInt
        {
            get { return R.IntInclusive((int)Math.Floor(Min), (int)Math.Ceiling(Max)); }
        }
        public bool Empty
        {
            get { return Max == 0 && Min == 0; }
        }

        public FloatBounds(float value)
        {
            Min = Max = value;
        }
        public FloatBounds(float min, float max)
        {
            Min = min;
            Max = max;
        }

        public float PercentOfValue(float value)
        {
            return (value - Min) / (Max - Min);
        }
        public float ValueOfPercent(float percent)
        {
            return Min + percent * (Max - Min);
        }

        public float InFloatBounds(float value, FloatBounds bounds)
        {
            return bounds.ValueOfPercent(PercentOfValue(value));
        }
        public int InIntBounds(float value, IntBounds bounds)
        {
            return bounds.ValueOfPercent(PercentOfValue(value));
        }
        public byte InByteBounds(float value, ByteBounds bounds)
        {
            return bounds.ValueOfPercent(PercentOfValue(value));
        }

        public static FloatBounds operator +(FloatBounds a, FloatBounds b)
        {
            return new FloatBounds(a.Min + b.Min, a.Max + b.Max);
        }
        public static FloatBounds operator +(FloatBounds a, float b)
        {
            return new FloatBounds(a.Min + b, a.Max + b);
        }
        public static FloatBounds operator *(FloatBounds a, float b)
        {
            return new FloatBounds(a.Min * b, a.Max * b);
        }
    }
    public struct IntBounds
    {
        public int Min, Max;

        public int Random
        {
            get { return R.IntInclusive(Min, Max); }
        }
        public bool Empty
        {
            get { return Max == 0 && Min == 0; }
        }

        public IntBounds(int value)
        {
            Min = Max = value;
        }
        public IntBounds(int min, int max)
        {
            Min = min;
            Max = max;
        }

        public float PercentOfValue(int value)
        {
            return (float)(value - Min) / (Max - Min);
        }
        public int ValueOfPercent(float percent)
        {
            return (int)(Min + percent * (Max - Min));
        }

        public float InFloatBounds(int value, FloatBounds bounds)
        {
            return bounds.ValueOfPercent(PercentOfValue(value));
        }
        public int InIntBounds(int value, IntBounds bounds)
        {
            return bounds.ValueOfPercent(PercentOfValue(value));
        }
        public byte InByteBounds(int value, ByteBounds bounds)
        {
            return bounds.ValueOfPercent(PercentOfValue(value));
        }

        public static IntBounds operator +(IntBounds a, IntBounds b)
        {
            return new IntBounds(a.Min + b.Min, a.Max + b.Max);
        }
        public static IntBounds operator +(IntBounds a, int b)
        {
            return new IntBounds(a.Min + b, a.Max + b);
        }
        public static IntBounds operator -(IntBounds a, int b)
        {
            return new IntBounds(a.Min - b, a.Max - b);
        }
        public static IntBounds operator *(IntBounds a, int b)
        {
            return new IntBounds(a.Min * b, a.Max * b);
        }
        public static IntBounds operator *(IntBounds a, float b)
        {
            return new IntBounds((int)(a.Min * b), (int)(a.Max * b));
        }
    }
    public struct ByteBounds
    {
        public byte Min, Max;

        public byte Random
        {
            get { return (byte)R.IntInclusive(Min, Max); }
        }
        public bool Empty
        {
            get { return Max == 0 && Min == 0; }
        }

        public ByteBounds(byte value)
        {
            Min = Max = value;
        }
        public ByteBounds(byte min, byte max)
        {
            Min = min;
            Max = max;
        }

        public float PercentOfValue(byte value)
        {
            return (float)(value - Min) / (Max - Min);
        }
        public byte ValueOfPercent(float percent)
        {
            return (byte)(Min + percent * (Max - Min));
        }

        public float InFloatBounds(byte value, FloatBounds bounds)
        {
            return bounds.ValueOfPercent(PercentOfValue(value));
        }
        public int InIntBounds(byte value, IntBounds bounds)
        {
            return bounds.ValueOfPercent(PercentOfValue(value));
        }
        public byte InByteBounds(byte value, ByteBounds bounds)
        {
            return bounds.ValueOfPercent(PercentOfValue(value));
        }
    }
}