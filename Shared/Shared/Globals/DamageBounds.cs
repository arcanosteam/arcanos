﻿using System;

namespace Arcanos.Shared
{
    public struct DamageBounds
    {
        public IntBounds Generic;
        public IntBounds Physical;
        public IntBounds Fire;
        public IntBounds Water;
        public IntBounds Ground;
        public IntBounds Air;
        public IntBounds Light;
        public IntBounds Dark;

        public Damage Random
        {
            get { return new Damage(this); }
        }
        public IntBounds Sum
        {
            get { return Generic + Physical + Fire + Water + Ground + Air + Light + Dark; }
        }
        public bool Empty
        {
            get { return Generic.Empty && Physical.Empty && Fire.Empty && Water.Empty && Ground.Empty && Air.Empty && Light.Empty && Dark.Empty; }
        }

        public bool IsGeneric
        {
            get { return !Generic.Empty; }
        }
        public bool IsPhysical
        {
            get { return !Physical.Empty; }
        }
        public bool IsMagic
        {
            get { return !Fire.Empty || !Water.Empty || !Ground.Empty || !Air.Empty || !Light.Empty || !Dark.Empty; }
        }

        public DamageBounds GenericDamage
        {
            get { return new DamageBounds { Generic = Generic }; }
        }
        public DamageBounds PhysicalDamage
        {
            get { return new DamageBounds { Physical = Physical }; }
        }
        public DamageBounds MagicDamage
        {
            get { return new DamageBounds { Fire = Fire, Water = Water, Ground = Ground, Air = Air, Light = Light, Dark = Dark }; }
        }

        public DamageBounds(DamageSchool school, IntBounds amount) : this()
        {
            switch (school)
            {
                case DamageSchool.Generic:
                    Generic = amount;
                    break;
                case DamageSchool.Physical:
                    Physical = amount;
                    break;
                case DamageSchool.Fire:
                    Fire = amount;
                    break;
                case DamageSchool.Water:
                    Water = amount;
                    break;
                case DamageSchool.Ground:
                    Ground = amount;
                    break;
                case DamageSchool.Air:
                    Air = amount;
                    break;
                case DamageSchool.Light:
                    Light = amount;
                    break;
                case DamageSchool.Dark:
                    Dark = amount;
                    break;
            }
        }

        public void AddRaw(IntBounds mod)
        {
            if (!Generic.Empty)
                Generic += mod;
            if (!Physical.Empty)
                Physical += mod;
            if (!Fire.Empty)
                Fire += mod;
            if (!Water.Empty)
                Water += mod;
            if (!Ground.Empty)
                Ground += mod;
            if (!Air.Empty)
                Air += mod;
            if (!Light.Empty)
                Light += mod;
            if (!Dark.Empty)
                Dark += mod;
        }
        public void AddRaw(int schoolMask, IntBounds mod)
        {
            if ((schoolMask & (1 << (byte)DamageSchool.Generic)) > 0 && !Generic.Empty)
                Generic += mod;
            if ((schoolMask & (1 << (byte)DamageSchool.Physical)) > 0 && !Physical.Empty)
                Physical += mod;
            if ((schoolMask & (1 << (byte)DamageSchool.Fire)) > 0 && !Fire.Empty)
                Fire += mod;
            if ((schoolMask & (1 << (byte)DamageSchool.Water)) > 0 && !Water.Empty)
                Water += mod;
            if ((schoolMask & (1 << (byte)DamageSchool.Ground)) > 0 && !Ground.Empty)
                Ground += mod;
            if ((schoolMask & (1 << (byte)DamageSchool.Air)) > 0 && !Air.Empty)
                Air += mod;
            if ((schoolMask & (1 << (byte)DamageSchool.Light)) > 0 && !Light.Empty)
                Light += mod;
            if ((schoolMask & (1 << (byte)DamageSchool.Dark)) > 0 && !Dark.Empty)
                Dark += mod;
        }
        public void AddPct(float pct)
        {
            pct += 1;
            if (!Generic.Empty)
                Generic *= pct;
            if (!Physical.Empty)
                Physical *= pct;
            if (!Fire.Empty)
                Fire *= pct;
            if (!Water.Empty)
                Water *= pct;
            if (!Ground.Empty)
                Ground *= pct;
            if (!Air.Empty)
                Air *= pct;
            if (!Light.Empty)
                Light *= pct;
            if (!Dark.Empty)
                Dark *= pct;
        }
        public void AddPct(int schoolMask, float pct)
        {
            pct += 1;
            if ((schoolMask & (1 << (byte)DamageSchool.Generic)) > 0 && !Generic.Empty)
                Generic *= pct;
            if ((schoolMask & (1 << (byte)DamageSchool.Physical)) > 0 && !Physical.Empty)
                Physical *= pct;
            if ((schoolMask & (1 << (byte)DamageSchool.Fire)) > 0 && !Fire.Empty)
                Fire *= pct;
            if ((schoolMask & (1 << (byte)DamageSchool.Water)) > 0 && !Water.Empty)
                Water *= pct;
            if ((schoolMask & (1 << (byte)DamageSchool.Ground)) > 0 && !Ground.Empty)
                Ground *= pct;
            if ((schoolMask & (1 << (byte)DamageSchool.Air)) > 0 && !Air.Empty)
                Air *= pct;
            if ((schoolMask & (1 << (byte)DamageSchool.Light)) > 0 && !Light.Empty)
                Light *= pct;
            if ((schoolMask & (1 << (byte)DamageSchool.Dark)) > 0 && !Dark.Empty)
                Dark *= pct;
        }
        public void SubRaw(DamageSchool school, int mod)
        {
            switch (school)
            {
                case DamageSchool.Generic:
                    Generic.Min = Math.Max(0, Generic.Min - mod);
                    Generic.Max = Math.Max(0, Generic.Max - mod);
                    break;
                case DamageSchool.Physical:
                    Physical.Min = Math.Max(0, Physical.Min - mod);
                    Physical.Max = Math.Max(0, Physical.Max - mod);
                    break;
                case DamageSchool.Fire:
                    Fire.Min = Math.Max(0, Fire.Min - mod);
                    Fire.Max = Math.Max(0, Fire.Max - mod);
                    break;
                case DamageSchool.Water:
                    Water.Min = Math.Max(0, Water.Min - mod);
                    Water.Max = Math.Max(0, Water.Max - mod);
                    break;
                case DamageSchool.Ground:
                    Ground.Min = Math.Max(0, Ground.Min - mod);
                    Ground.Max = Math.Max(0, Ground.Max - mod);
                    break;
                case DamageSchool.Air:
                    Air.Min = Math.Max(0, Air.Min - mod);
                    Air.Max = Math.Max(0, Air.Max - mod);
                    break;
                case DamageSchool.Light:
                    Light.Min = Math.Max(0, Light.Min - mod);
                    Light.Max = Math.Max(0, Light.Max - mod);
                    break;
                case DamageSchool.Dark:
                    Dark.Min = Math.Max(0, Dark.Min - mod);
                    Dark.Max = Math.Max(0, Dark.Max - mod);
                    break;
            }
        }
        public void SubPct(float pct)
        {
            AddPct(1 / pct - 1);
        }
        public void SubPct(int schoolMask, float pct)
        {
            AddPct(schoolMask, 1 / pct - 1);
        }

        public static DamageBounds operator +(DamageBounds a, DamageBounds b)
        {
            return new DamageBounds
            {
                Generic = a.Generic + b.Generic,
                Physical = a.Physical + b.Physical,
                Fire = a.Fire + b.Fire,
                Water = a.Water + b.Water,
                Ground = a.Ground + b.Ground,
                Air = a.Air + b.Air,
                Light = a.Light + b.Light,
                Dark = a.Dark + b.Dark,
            };
        }
        public static DamageBounds operator +(DamageBounds a, int b)
        {
            return new DamageBounds
            {
                Generic = a.Generic + b,
                Physical = a.Physical + b,
                Fire = a.Fire + b,
                Water = a.Water + b,
                Ground = a.Ground + b,
                Air = a.Air + b,
                Light = a.Light + b,
                Dark = a.Dark + b,
            };
        }
        public static DamageBounds operator -(DamageBounds a, int b)
        {
            return new DamageBounds
            {
                Generic = a.Generic - b,
                Physical = a.Physical - b,
                Fire = a.Fire - b,
                Water = a.Water - b,
                Ground = a.Ground - b,
                Air = a.Air - b,
                Light = a.Light - b,
                Dark = a.Dark - b,
            };
        }
        public static DamageBounds operator *(DamageBounds a, float mul)
        {
            return new DamageBounds
            {
                Generic = a.Generic * mul,
                Physical = a.Physical * mul,
                Fire = a.Fire * mul,
                Water = a.Water * mul,
                Ground = a.Ground * mul,
                Air = a.Air * mul,
                Light = a.Light * mul,
                Dark = a.Dark * mul,
            };
        }
    }
}