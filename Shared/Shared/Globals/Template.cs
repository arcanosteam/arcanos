﻿using Arcanos.Shared.Data;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Objects;

namespace Arcanos.Shared
{
    [SerializationSettings(Method = SerializationMethod.RefByID)]
    public abstract class Template : IReferencedByID, IEditableEntry
    {
        [SerializationIgnore]
        private int cachedID;
        [SerializationIgnore]
        public int ID
        {
            get
            {
                if (cachedID == 0)
                {
                    cachedID = GetInitialID();
                    if (cachedID == -1)
                    {
                        cachedID = 0;
                        return -1;
                    }
                }
                return cachedID;
            }
            set { cachedID = value; }
        }

        [SerializationIgnore]
        public abstract EditorEntry EditorEntry { get; }

        protected abstract int GetInitialID();

        public override string ToString()
        {
            return EditorEntry.Name;
        }
    }
    public interface IEditableEntry
    {
        [SerializationIgnore]
        EditorEntry EditorEntry { get; }
    }
    public struct EditorEntry
    {
        [SerializationIgnore]
        public int ID;
        [SerializationIgnore]
        public string Name;

        public EditorEntry(int id, Text name) : this(id, name == null ? "" : name.ToString()) { }
        public EditorEntry(int id, string name)
        {
            ID = id;
            Name = name;
        }

        public override string ToString()
        {
            return string.Format("{{{0}: {1}}}", ID, Name);
        }
    }
}