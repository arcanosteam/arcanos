﻿namespace Arcanos.Shared
{
    public enum Reaction
    {
        Friendly,
        Neutral,
        Hostile,
    }
}