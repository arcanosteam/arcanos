﻿namespace Arcanos.Shared
{
    public enum DamageSchool
    {
        Generic,
        Physical,
        Fire,
        Water,
        Ground,
        Air,
        Light,
        Dark,

        Max,
    }
}