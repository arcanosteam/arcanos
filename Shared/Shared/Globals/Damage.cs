﻿namespace Arcanos.Shared
{
    public struct Damage
    {
        public int Generic;
        public int Physical;
        public int Fire;
        public int Water;
        public int Ground;
        public int Air;
        public int Light;
        public int Dark;

        public Damage(DamageSchool school, int amount) : this()
        {
            switch (school)
            {
                case DamageSchool.Generic:
                    Generic = amount;
                    break;
                case DamageSchool.Physical:
                    Physical = amount;
                    break;
                case DamageSchool.Fire:
                    Fire = amount;
                    break;
                case DamageSchool.Water:
                    Water = amount;
                    break;
                case DamageSchool.Ground:
                    Ground = amount;
                    break;
                case DamageSchool.Air:
                    Air = amount;
                    break;
                case DamageSchool.Light:
                    Light = amount;
                    break;
                case DamageSchool.Dark:
                    Dark = amount;
                    break;
            }
        }
        public Damage(Damage damage)
        {
            Generic = damage.Generic;
            Physical = damage.Physical;
            Fire = damage.Fire;
            Water = damage.Water;
            Ground = damage.Ground;
            Air = damage.Air;
            Light = damage.Light;
            Dark = damage.Dark;
        }
        public Damage(DamageBounds bounds)
        {
            Generic = bounds.Generic.Random;
            Physical = bounds.Physical.Random;
            Fire = bounds.Fire.Random;
            Water = bounds.Water.Random;
            Ground = bounds.Ground.Random;
            Air = bounds.Air.Random;
            Light = bounds.Light.Random;
            Dark = bounds.Dark.Random;
        }

        public int Sum
        {
            get { return Generic + Physical + Fire + Water + Ground + Air + Light + Dark; }
        }

        public bool Empty
        {
            get { return Generic == 0 && Physical == 0 && Fire == 0 && Water == 0 && Ground == 0 && Air == 0 && Light == 0 && Dark == 0; }
        }

        public bool IsGeneric
        {
            get { return Generic != 0; }
        }
        public bool IsPhysical
        {
            get { return Physical != 0; }
        }
        public bool IsMagic
        {
            get { return Fire != 0 || Water != 0 || Ground != 0 || Air != 0 || Light != 0 || Dark != 0; }
        }

        public Damage GenericDamage
        {
            get { return new Damage { Generic = Generic }; }
        }
        public Damage PhysicalDamage
        {
            get { return new Damage { Physical = Physical }; }
        }
        public Damage MagicDamage
        {
            get { return new Damage { Fire = Fire, Water = Water, Ground = Ground, Air = Air, Light = Light, Dark = Dark }; }
        }

        public static Damage operator +(Damage a, Damage b)
        {
            return new Damage
            {
                Generic = a.Generic + b.Generic,
                Physical = a.Physical + b.Physical,
                Fire = a.Fire + b.Fire,
                Water = a.Water + b.Water,
                Ground = a.Ground + b.Ground,
                Air = a.Air + b.Air,
                Light = a.Light + b.Light,
                Dark = a.Dark + b.Dark,
            };
        }
        public static Damage operator *(Damage a, float mul)
        {
            return new Damage
            {
                Generic = (int)(a.Generic * mul),
                Physical = (int)(a.Physical * mul),
                Fire = (int)(a.Fire * mul),
                Water = (int)(a.Water * mul),
                Ground = (int)(a.Ground * mul),
                Air = (int)(a.Air * mul),
                Light = (int)(a.Light * mul),
                Dark = (int)(a.Dark * mul),
            };
        }
    }
}