﻿namespace Arcanos.Shared
{
    public enum PlayerRace : byte
    {
        Human,
        WoodElf,
        Minotaur,
        Orc,
        Lizardman,

        Max,
    }
}