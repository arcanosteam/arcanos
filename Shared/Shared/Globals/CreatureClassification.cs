﻿using System;

namespace Arcanos.Shared
{
    [Flags]
    public enum CreatureClassification : byte
    {
        Normal = 0,

        Elite = 0x01,
        Rare = 0x02,
        Boss = 0x04,
    }
}