﻿namespace Arcanos.Shared
{
    public enum AttackResult : byte
    {
        Hit,
        Miss,
    }
}