﻿namespace Arcanos.Shared
{
    public enum CreatureState
    {
        JustSpawned,
        Alive,
        JustDied,
        Corpse,
        Despawned,
    }
}