﻿using Microsoft.Xna.Framework;

namespace Arcanos.Shared
{
    public static class SizeFactory
    {
        public static Vector3 Box(float width, float length, float height)
        {
            return new Vector3(width * 2, length * 2, height * 2);
        }
        public static Vector3 Sphere(float radius)
        {
            return new Vector3(0, 0, radius);
        }
        public static Vector3 Cylinder(float radius, float height)
        {
            return new Vector3(radius, 0, height);
        }
    }
}