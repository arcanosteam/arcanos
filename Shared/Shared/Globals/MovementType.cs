﻿using System;

namespace Arcanos.Shared
{
    public enum MovementType : byte
    {
        Walk,
        Run,
        Swim,
        Fly,

        Max,
    }
    [Flags]
    public enum MovementTypeMask : byte
    {
        Walk    = 0x01,
        Run     = 0x02,
        Swim    = 0x04,
        Fly     = 0x08,

        Ground = Walk | Run,
        All = Walk | Run | Swim | Fly,
    }
}