﻿namespace Arcanos.Shared
{
    public enum AttackType : byte
    {
        MeleeMainHand,
        MeleeOffHand,

        Max,
    }
}