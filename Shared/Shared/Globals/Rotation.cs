﻿using System;
using Microsoft.Xna.Framework;

namespace Arcanos.Shared
{
    public struct Rotation : IEquatable<Rotation>
    {
        private static readonly Rotation zero = new Rotation();
        public static Rotation Zero
        {
            get { return zero; }
        }

        public float Pitch, Yaw, Roll;

        public Vector3 Forward
        {
            get
            {
                return new Vector3(
                    (float)(Math.Cos(Yaw) * Math.Cos(Pitch)),
                    (float)(Math.Sin(Yaw) * Math.Cos(Pitch)),
                    (float)Math.Sin(Pitch));
            }
            set
            {
                Pitch = (float)Math.Atan2(value.Y, value.X);
                Yaw = (float)Math.Atan(value.Z);
                Roll = 0;
            }
        }
        public Vector3 Backward
        {
            get
            {
                return new Vector3(
                    (float)(Math.Cos(Yaw + MathHelper.Pi) * Math.Cos(Pitch)),
                    (float)(Math.Sin(Yaw + MathHelper.Pi) * Math.Cos(Pitch)),
                    (float)Math.Sin(-Pitch));
            }
            set
            {
                Pitch = (float)Math.Atan2(value.Y, value.X);
                Yaw = (float)Math.Atan(value.Z) - MathHelper.Pi;
                Roll = 0;
            }
        }
        public Vector3 Left
        {
            get
            {
                return new Vector3(
                    (float)(Math.Cos(Yaw + MathHelper.PiOver2)),
                    (float)(Math.Sin(Yaw + MathHelper.PiOver2)),
                    0);
            }
            set
            {
                Pitch = (float)Math.Atan2(value.Y, value.X);
                Yaw = (float)Math.Atan(value.Z) - MathHelper.PiOver2;
                Roll = 0;
            }
        }
        public Vector3 Right
        {
            get
            {
                return new Vector3(
                    (float)(Math.Cos(Yaw - MathHelper.PiOver2)),
                    (float)(Math.Sin(Yaw - MathHelper.PiOver2)),
                    0);
            }
            set
            {
                Pitch = (float)Math.Atan2(value.Y, value.X);
                Yaw = (float)Math.Atan(value.Z) + MathHelper.PiOver2;
                Roll = 0;
            }
        }
        public Vector3 Up
        {
            get
            {
                Vector3 right = Right;
                Vector3 forward = Forward;
                return new Vector3((right.Y * forward.Z) - (forward.Y * right.Z),
                                   (right.Z * forward.X) - (forward.Z * right.X),
                                   (right.X * forward.Y) - (forward.X * right.Y));
            }
        }
        public Vector3 Down
        {
            get
            {
                Vector3 right = Right;
                Vector3 forward = Forward;
                return new Vector3((forward.Y * right.Z) - (right.Y * forward.Z),
                                   (forward.Z * right.X) - (right.Z * forward.X),
                                   (forward.X * right.Y) - (right.X * forward.Y));
            }
        }

        public float NormalizedPitch
        {
            get { return NormalizePitch(Pitch); }
        }
        public float NormalizedYaw
        {
            get { return NormalizeYaw(Yaw); }
        }
        public float NormalizedRoll
        {
            get { return NormalizePitch(Roll); }
        }

        public Rotation(float yaw)
        {
            Pitch = 0;
            Yaw = yaw;
            Roll = 0;
        }
        public Rotation(float yaw, float pitch)
        {
            Pitch = pitch;
            Yaw = yaw;
            Roll = 0;
        }
        public Rotation(float yaw, float pitch, float roll)
        {
            Pitch = pitch;
            Yaw = yaw;
            Roll = roll;
        }

        public override string ToString()
        {
            if (Roll != 0)
                return string.Format("{{Yaw:{0} Pitch:{1} Roll:{2}}}", Yaw, Pitch, Roll);
            if (Pitch != 0)
                return string.Format("{{Yaw:{0} Pitch:{1}}}", Yaw, Pitch);
            return string.Format("{{Yaw:{0}}}", Yaw);
        }
        public bool Equals(Rotation other)
        {
            return Pitch.Equals(other.Pitch) && Yaw.Equals(other.Yaw) && Roll.Equals(other.Roll);
        }
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is Rotation && Equals((Rotation)obj);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = Pitch.GetHashCode();
                hashCode = (hashCode * 397) ^ Yaw.GetHashCode();
                hashCode = (hashCode * 397) ^ Roll.GetHashCode();
                return hashCode;
            }
        }
        public static Rotation operator +(Rotation a, Rotation b)
        {
            return new Rotation(a.Yaw + b.Yaw, a.Pitch + b.Pitch, a.Roll + b.Roll);
        }
        public static Rotation operator -(Rotation a, Rotation b)
        {
            return new Rotation(a.Yaw - b.Yaw, a.Pitch - b.Pitch, a.Roll - b.Roll);
        }
        public static bool operator ==(Rotation a, Rotation b)
        {
            return a.Yaw == b.Yaw && a.Pitch == b.Pitch && a.Roll == b.Roll;
        }
        public static bool operator !=(Rotation a, Rotation b)
        {
            return a.Yaw != b.Yaw || a.Pitch != b.Pitch || a.Roll != b.Roll;
        }

        public static float NormalizeYaw(float yaw)
        {
            return yaw >= 0 && yaw <= MathHelper.TwoPi
                ? yaw
                : yaw < 0
                    ? -(-yaw % MathHelper.TwoPi) + MathHelper.TwoPi
                    : yaw % MathHelper.TwoPi;
        }
        public static float NormalizePitch(float pitch)
        {
            return pitch > -MathHelper.Pi && pitch < MathHelper.Pi
                ? pitch
                : NormalizeYaw(pitch + MathHelper.Pi) - MathHelper.Pi;
        }
    }
}