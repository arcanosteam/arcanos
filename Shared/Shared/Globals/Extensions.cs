﻿using System;
using Arcanos.Shared.Objects;
using Microsoft.Xna.Framework;

namespace Arcanos.Shared
{
    public static class Extensions
    {
        public static bool IsWorldObject(this ObjectType type)
        {
            return (type & ObjectType.WorldObject) == ObjectType.WorldObject;
        }
        public static bool IsCreature(this ObjectType type)
        {
            return (type & ObjectType.Creature) == ObjectType.Creature;
        }
        public static bool IsPlayer(this ObjectType type)
        {
            return (type & ObjectType.PlayerCreature) == ObjectType.PlayerCreature;
        }

        public static Rotation GetRotationTo(this Vector3 from, Vector3 to)
        {
            Vector3 d = to - from;
            float xyDistance = (float)Math.Sqrt(d.X * d.X + d.Y * d.Y);
            return new Rotation(
                (float)Math.Atan2(d.Y, d.X),
                (float)Math.Atan2(d.Z, xyDistance));
        }
        public static bool AlmostEquals(this Vector3 a, Vector3 b)
        {
            const float EPSILON = 1e-10f;

            float dist;
            Vector3.Distance(ref a, ref b, out dist);
            return dist < EPSILON;
        }
        public static bool AlmostEquals(this Rotation a, Rotation b)
        {
            const float EPSILON = 1e-10f;

            if ((a.Yaw > b.Yaw ? a.Yaw - b.Yaw : b.Yaw - a.Yaw) > EPSILON)
                return false;
            if ((a.Pitch > b.Pitch ? a.Pitch - b.Pitch : b.Pitch - a.Pitch) > EPSILON)
                return false;
            if ((a.Roll > b.Roll ? a.Roll - b.Roll : b.Roll - a.Roll) > EPSILON)
                return false;
            return true;
        }

        public static void Lerp(float from, float to, float amount, out float result)
        {
            result = from + (to - from) * amount;
        }
    }
}