﻿namespace Arcanos.Shared
{
    public enum PlayerClass : byte
    {
        Warrior,
        Rogue,
        Mage,
        Cultist,

        Max,
    }
}