﻿namespace Arcanos.Shared.Dialog
{
    public enum DialogStartResult : byte
    {
        Ok,
        
        TalkerNotFound,
        NoDialog,
        TargetIsPlayer,
        TooFar,
        TalkerIncapable,
    }
    public enum DialogSelectOptionResult : byte
    {
        Ok,

        NotTalking,
        WrongPage,
    }
    public enum DialogEndReason : byte
    {
        UserRequest,
        DialogEnded,
        TooFar,
        TalkerIncapable,
        ChangeDialog,
    }
    public enum DialogQuestOptionState : byte
    {
        NotAcceptable,
        Acceptable,
        NotCompleted,
        Completed,
    }
}