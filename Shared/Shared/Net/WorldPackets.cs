﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.InteropServices;
using Arcanos.Net.Packets;
using Arcanos.Shared.Chat;
using Arcanos.Shared.Collisions;
using Arcanos.Shared.Dialog;
using Arcanos.Shared.Items;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Movement;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Players;
using Arcanos.Shared.PvP;
using Arcanos.Shared.Quests;
using Arcanos.Shared.Server;
using Arcanos.Shared.Spells;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Arcanos.Shared.Net
{
    public static class WorldPacketsTable
    {
        public static void Fill(Dictionary<WorldPackets, Type> dictionary)
        {
            foreach (object type in Enum.GetValues(typeof(WorldPackets)))
                dictionary.Add((WorldPackets)type, Assembly.GetExecutingAssembly().GetType("Arcanos.Shared.Net." + type + "Packet"));
        }
    }

    public enum WorldPackets : byte
    {
        // Protocol-related
        CWorldProtocolQuery,
        SWorldProtocolResponse,
        CWorldServerVersionQuery,
        SWorldServerVersionResponse,
        CWorldGameVersionQuery,
        SWorldGameVersionResponse,
        // Login-related
        CWorldLoginRequest,
        SWorldLoginResult,
        CWorldEnterRequest,
        SWorldEnterResponse,
        // Account-related
        CPlayerQuery,
        SPlayerResponse,
        CCharacterListStatusQuery,
        SCharacterListStatusResponse,
        CCharacterStatusQuery,
        SCharacterStatusResponse,
        CCharacterQuery,
        SCharacterResponse,
        CCharacterTemplateQuery,
        SCharacterTemplateResponse,
        CCharacterCreateTemplateQuery,
        SCharacterCreateTemplateResponse,
        CCharacterCreate,
        SCharacterCreateResult,
        SKnowsSpell,
        CForceLogout,
        SExperienceChanged,
        SLevelUp,
        SStatsData,
        SBaseStatUpdate,
        SModifiedStatUpdate,
        // Groups
        SCharacterChangedGroup,
        CGroupInvite,
        SGroupInviteResult,
        CGroupKick,
        SGroupKickResult,
        SGroupInvitation,
        CGroupInvitationResponse,
        CLeaveGroup,
        SGroupMemberJoined,
        SGroupMemberLeft,
        SGroupLeaderChanged,
        // Chat
        CChatMessage,
        CChatWhisper,
        SChatMessageResult,
        SChatMessage,
        CChatChannelJoin,
        SChatChannelJoinResult,
        CChatChannelLeave,
        SChatChannelLeaveResult,
        // Items
        SMoneyChanged,
        SBagChanged,
        SBagSlotUpdated,
        SItemCountUpdated,
        CItemDataQuery,
        SItemDataResponse,
        CMoveItem,
        SMoveItemResult,
        CDivideItem,
        SDivideItemResult,
        CDestroyItem,
        SDestroyItemResult,
        CUseItem,
        SUseItemResult,
        CEquipItem,
        SEquipItemResult,
        CUnequipItem,
        SUnequipItemResult,
        SItemsAdded,
        SItemsRemoved,
        CLootCreature,
        CCancelLooting,
        SCancelLooting,
        SLootResult,
        SLootResultItem,
        CLootItem,
        SLootItemResult,
        SLootItemRemoved,
        // Quests
        SQuestAdded,
        SQuestRemoved,
        SQuestCompleted,
        SQuestObjectiveUpdate,
        SAddQuestResult,
        SRemoveQuestResult,
        SCompleteQuestResult,
        CAbandonQuest,
        // Data
        CTextQuery,
        STextResponse,
        CGameStringsQuery,
        SGameStringsResponse,
        CMapTemplateQuery,
        SMapTemplateResponse,
        CWorldObjectTemplateQuery,
        SWorldObjectTemplateResponse,
        CCreatureTemplateQuery,
        SCreatureTemplateResponse,
        CSpellTemplateQuery,
        SSpellTemplateResponse,
        CSpellVisualQuery,
        SSpellVisualResponse,
        CItemTemplateQuery,
        SItemTemplateResponse,
        CQuestQuery,
        SQuestResponse,
        CFactionQuery,
        SFactionResponse,
        CEnvironmentQuery,
        SEnvironmentResponse,
        CGuildInfoQuery,
        SGuildInfoResponse,
        // Creatures
        CTargetUpdate,
        SCreatureEnteredCombat,
        SCreatureEvaded,
        SCreatureHealthUpdate,
        SCreatureUsesPowerUpdate,
        SCreaturePowerUpdate,
        SCreatureStateUpdate,
        SCreatureTargetUpdate,
        SCreatureMovementTypeUpdate,
        SCreatureFactionUpdate,
        SCreatureModelUpdate,
        // Movement
        CMovementUpdate,
        SCreatureMovementUpdate,
        // Dialog
        CStartDialog,
        SStartDialogResult,
        CEndDialog,
        SEndDialog,
        SChangedPage,
        SPageOption,
        SPageQuestOption,
        CSelectOption,
        SSelectOptionResult,
        SDialogQuestAcceptPage,
        SDialogQuestCompletePage,
        // Combat
        SCombatInflictedDamage,
        SCombatHealed,
        SCombatPerformedMeleeAttack,
        SCombatPerformedSpellAttack,
        SCombatSpellCasted,
        SCombatSpellCastingStarted,
        SCombatSpellCastingFailed,
        CCombatStartMeleeAttack,
        CCombatStopMeleeAttack,
        CCombatCastSpell,
        CCombatCastGroundSpell,
        CCombatInterruptSpellCasting,
        SHasAura,
        SAppliedAura,
        SRemovedAura,
        SAreaEffectPlaced,
        SAreaEffectRemoved,
        CDuelRequest,
        SDuelRequest,
        CDuelRequestResponse,
        SDuelStatus,
        SDuelParticipantStatus,
        // World
        SChangeMap,
        SChangeEnvironment,
        CTeleportAck,
        SObjectEnteredVisibility,
        SObjectLeftVisibility,
        SObjectUpdateLocation,
        SObjectLootRecipientUpdate,
        SObjectFlagsUpdate,
        SObjectLoadingFinalized,
        // Graphics
        CMaterialQuery,
        SMaterialResponse,
        CTextureInfoQuery,
        STextureInfoResponse,
        CModelQuery,
        SModelResponse,
        CParticleSystemQuery,
        SParticleSystemResponse,
        // Misc
        SWorldSystemMessage,
    }
    public interface IWorldPacket { }

    #region Protocol-related
    [PacketType(WorldPackets.CWorldProtocolQuery)]
    public struct CWorldProtocolQueryPacket : IWorldPacket { }
    [PacketType(WorldPackets.SWorldProtocolResponse)]
    public struct SWorldProtocolResponsePacket : IWorldPacket
    {
        public int ProtocolVersion;
    }
    [PacketType(WorldPackets.CWorldServerVersionQuery)]
    public struct CWorldServerVersionQueryPacket : IWorldPacket { }
    [PacketType(WorldPackets.SWorldServerVersionResponse)]
    public struct SWorldServerVersionResponsePacket : IWorldPacket
    {
        public byte Major;
        public int Minor;
        public int Build;
        public LocaleIndex Locale;
    }
    [PacketType(WorldPackets.CWorldGameVersionQuery)]
    public struct CWorldGameVersionQueryPacket : IWorldPacket { }
    [PacketType(WorldPackets.SWorldGameVersionResponse)]
    public struct SWorldGameVersionResponsePacket : IWorldPacket
    {
        public byte Major;
        public byte Minor;
        public int Build;
        public LocaleIndex Locale;
    }
    #endregion
    #region Login-related
    [PacketType(WorldPackets.CWorldLoginRequest)]
    public struct CWorldLoginRequestPacket : IWorldPacket
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public byte[] LoginToken;
        public LocaleIndex Locale;
    }
    [PacketType(WorldPackets.SWorldLoginResult)]
    public struct SWorldLoginResultPacket : IWorldPacket
    {
        public WorldLoginResult Result;
        public int PlayerID;
    }
    [PacketType(WorldPackets.CWorldEnterRequest)]
    public struct CWorldEnterRequestPacket : IWorldPacket
    {
        public byte CharacterIndex;
    }
    [PacketType(WorldPackets.SWorldEnterResponse)]
    public struct SWorldEnterResponsePacket : IWorldPacket
    {
        public WorldEnterResult Result;
        public int CharacterID;
        public ulong PlayerCreatureGUID;
        public ulong MapGUID;
        public int MapTemplateID;
        public int MapEnvironmentID;
        public Vector3 Position;
        public Rotation Rotation;
    }
    #endregion
    #region Account-related
    [PacketType(WorldPackets.CPlayerQuery)]
    public struct CPlayerQueryPacket : IWorldPacket
    {
        public int ID;
    }
    [PacketType(WorldPackets.SPlayerResponse, true)]
    public struct SPlayerResponsePacket : IWorldPacket
    {
        public int ID;
    }
    [PacketType(WorldPackets.CCharacterListStatusQuery)]
    public struct CCharacterListStatusQueryPacket : IWorldPacket { }
    [PacketType(WorldPackets.SCharacterListStatusResponse)]
    public struct SCharacterListStatusResponsePacket : IWorldPacket
    {
        public byte MaxCount;
        public byte MaxAvailable;
        public byte CurrentCount;
    }
    [PacketType(WorldPackets.CCharacterStatusQuery)]
    public struct CCharacterStatusQueryPacket : IWorldPacket
    {
        public byte Index;
    }
    [PacketType(WorldPackets.SCharacterStatusResponse)]
    public struct SCharacterStatusResponsePacket : IWorldPacket
    {
        public byte Index;
        public int ID;
    }
    [PacketType(WorldPackets.CCharacterQuery)]
    public struct CCharacterQueryPacket : IWorldPacket
    {
        public int ID;
    }
    [PacketType(WorldPackets.SCharacterResponse, true)]
    public struct SCharacterResponsePacket : IWorldPacket
    {
        public int ID;
    }
    [PacketType(WorldPackets.CCharacterTemplateQuery)]
    public struct CCharacterTemplateQueryPacket : IWorldPacket
    {
        public int ID;
    }
    [PacketType(WorldPackets.SCharacterTemplateResponse, true)]
    public struct SCharacterTemplateResponsePacket : IWorldPacket
    {
        public int ID;
        public int FriendlyTargetDecalTextureID;
        public int NeutralTargetDecalTextureID;
        public int HostileTargetDecalTextureID;
    }
    [PacketType(WorldPackets.CCharacterCreateTemplateQuery)]
    public struct CCharacterCreateTemplateQueryPacket : IWorldPacket
    {
        public PlayerRace Race;
        public PlayerClass Class;
        public byte ModelIndex;
    }
    [PacketType(WorldPackets.SCharacterCreateTemplateResponse, true)]
    public struct SCharacterCreateTemplateResponsePacket : IWorldPacket
    {
        public PlayerRace Race;
        public PlayerClass Class;
        public byte ModelIndex;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
        public int[] Spells;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public int[] Items;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public int[] ItemCounts;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = (byte)EquipSlot.Max)]
        public int[] Equipment;
    }
    [PacketType(WorldPackets.CCharacterCreate)]
    public struct CCharacterCreatePacket : IWorldPacket
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
        public string Name;
        public PlayerRace Race;
        public PlayerClass Class;
        public byte ModelIndex;
    }
    [PacketType(WorldPackets.SCharacterCreateResult)]
    public struct SCharacterCreateResultPacket : IWorldPacket
    {
        public CharacterCreateResult Result;
        public byte ModelIndexCount;
    }
    [PacketType(WorldPackets.SKnowsSpell)]
    public struct SKnowsSpellPacket : IWorldPacket
    {
        public int SpellID;
    }
    [PacketType(WorldPackets.CForceLogout)]
    public struct CForceLogoutPacket : IWorldPacket { }
    [PacketType(WorldPackets.SExperienceChanged)]
    public struct SExperienceChangedPacket : IWorldPacket
    {
        public int XP;
        public int XPToLevel;
    }
    [PacketType(WorldPackets.SLevelUp)]
    public struct SLevelUpPacket : IWorldPacket
    {
        public ulong GUID;
        public int Level;
    }
    [PacketType(WorldPackets.SStatsData)]
    public struct SStatsDataPacket : IWorldPacket
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = (byte)Stat.Max)]
        public int[] BaseValues;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = (byte)Stat.Max)]
        public int[] ModifiedValues;
    }
    [PacketType(WorldPackets.SBaseStatUpdate)]
    public struct SBaseStatUpdatePacket : IWorldPacket
    {
        public Stat Stat;
        public int Value;
    }
    [PacketType(WorldPackets.SModifiedStatUpdate)]
    public struct SModifiedStatUpdatePacket : IWorldPacket
    {
        public Stat Stat;
        public int Value;
    }
    #endregion
    #region Group
    [PacketType(WorldPackets.SCharacterChangedGroup)]
    public struct SCharacterChangedGroupPacket : IWorldPacket
    {
        public ulong GUID;
        public int GroupID;
        public byte Index;
    }
    [PacketType(WorldPackets.CGroupInvite)]
    public struct CGroupInvitePacket : IWorldPacket
    {
        public ulong InviteeGUID;
    }
    [PacketType(WorldPackets.SGroupInviteResult)]
    public struct SGroupInviteResultPacket : IWorldPacket
    {
        public ulong InviteeGUID;
        public GroupInviteResult Result;
    }
    [PacketType(WorldPackets.CGroupKick)]
    public struct CGroupKickPacket : IWorldPacket
    {
        public ulong TargetGUID;
    }
    [PacketType(WorldPackets.SGroupKickResult)]
    public struct SGroupKickResultPacket : IWorldPacket
    {
        public ulong TargetGUID;
        public GroupKickResult Result;
    }
    [PacketType(WorldPackets.SGroupInvitation), StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    public struct SGroupInvitationPacket : IWorldPacket
    {
        public ulong InviterGUID;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
        public string InviterName;
    }
    [PacketType(WorldPackets.CGroupInvitationResponse)]
    public struct CGroupInvitationResponsePacket : IWorldPacket
    {
        public ulong InviterGUID;
        public GroupInvitationResponse Response;
    }
    [PacketType(WorldPackets.CLeaveGroup)]
    public struct CLeaveGroupPacket : IWorldPacket
    {
    }
    [PacketType(WorldPackets.SGroupMemberJoined)]
    public struct SGroupMemberJoinedPacket : IWorldPacket
    {
        public bool Initial;
        public byte Index;
        public ulong MemberGUID;
    }
    [PacketType(WorldPackets.SGroupMemberLeft)]
    public struct SGroupMemberLeftPacket : IWorldPacket
    {
        public byte Index;
        public ulong MemberGUID;
    }
    [PacketType(WorldPackets.SGroupLeaderChanged)]
    public struct SGroupLeaderChangedPacket : IWorldPacket
    {
        public bool Initial;
        public ulong LeaderGUID;
    }
    #endregion
    #region Chat
    [PacketType(WorldPackets.CChatMessage, true)]
    public struct CChatMessagePacket : IWorldPacket
    {
        public int ChannelID;
        public long Timestamp;
        public ChatMessageFlags Flags;
    }
    [PacketType(WorldPackets.CChatWhisper, true)]
    public struct CChatWhisperPacket : IWorldPacket
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
        public string TargetName;
        public long Timestamp;
        public ChatMessageFlags Flags;
    }
    [PacketType(WorldPackets.SChatMessageResult)]
    public struct SChatMessageResultPacket : IWorldPacket
    {
        public ChatMessageResult Result;
    }
    [PacketType(WorldPackets.SChatMessage, true)]
    public struct SChatMessagePacket : IWorldPacket
    {
        public ulong SenderGUID;
        public int ChannelID;
        public long Timestamp;
        public ChatMessageFlags Flags;
    }
    [PacketType(WorldPackets.CChatChannelJoin)]
    public struct CChatChannelJoinPacket : IWorldPacket
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
        public string ChannelName;
    }
    [PacketType(WorldPackets.SChatChannelJoinResult)]
    public struct SChatChannelJoinResultPacket : IWorldPacket
    {
        public int ChannelID;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
        public string CustomChannelName;
        public int ChannelNameID;
        public int ChannelAliasID;
        public Color ChannelColor;
        public ChatChannelJoinResult Result;
    }
    [PacketType(WorldPackets.CChatChannelLeave)]
    public struct CChatChannelLeavePacket : IWorldPacket
    {
        public int ChannelID;
    }
    [PacketType(WorldPackets.SChatChannelLeaveResult)]
    public struct SChatChannelLeaveResultPacket : IWorldPacket
    {
        public int ChannelID;
        public ChatChannelLeaveResult Result;
    }
    #endregion
    #region Items
    [PacketType(WorldPackets.SMoneyChanged)]
    public struct SMoneyChangedPacket : IWorldPacket
    {
        public int Count;
        public bool Initial;
    }
    [PacketType(WorldPackets.SBagChanged)]
    public struct SBagChangedPacket : IWorldPacket
    {
        public byte BagIndex;
        public ulong BagItemGUID;
        public byte BagSize;
    }
    [PacketType(WorldPackets.SBagSlotUpdated)]
    public struct SBagSlotUpdatedPacket : IWorldPacket
    {
        public sbyte BagIndex;
        public byte BagSlot;
        public ulong ItemGUID;
    }
    [PacketType(WorldPackets.SItemCountUpdated)]
    public struct SItemCountUpdatedPacket : IWorldPacket
    {
        public ulong GUID;
        public int Count;
    }
    [PacketType(WorldPackets.CItemDataQuery)]
    public struct CItemDataQueryPacket : IWorldPacket
    {
        public ulong GUID;
    }
    [PacketType(WorldPackets.SItemDataResponse)]
    public struct SItemDataResponsePacket : IWorldPacket
    {
        public ulong GUID;
        public int ID;
        public int Count;
    }
    [PacketType(WorldPackets.CMoveItem)]
    public struct CMoveItemPacket : IWorldPacket
    {
        public byte OldBagIndex;
        public byte OldSlot;
        public byte NewBagIndex;
        public byte NewSlot;
    }
    [PacketType(WorldPackets.SMoveItemResult)]
    public struct SMoveItemResultPacket : IWorldPacket
    {
        public byte OldBagIndex;
        public byte OldSlot;
        public byte NewBagIndex;
        public byte NewSlot;
        public ItemMoveResult Result;
    }
    [PacketType(WorldPackets.CDivideItem)]
    public struct CDivideItemPacket : IWorldPacket
    {
        public byte OldBagIndex;
        public byte OldSlot;
        public byte NewBagIndex;
        public byte NewSlot;
        public int NewCount;
    }
    [PacketType(WorldPackets.SDivideItemResult)]
    public struct SDivideItemResultPacket : IWorldPacket
    {
        public byte OldBagIndex;
        public byte OldSlot;
        public byte NewBagIndex;
        public byte NewSlot;
        public int NewCount;
        public ItemDivideResult Result;
    }
    [PacketType(WorldPackets.CDestroyItem)]
    public struct CDestroyItemPacket : IWorldPacket
    {
        public ulong GUID;
    }
    [PacketType(WorldPackets.SDestroyItemResult)]
    public struct SDestroyItemResultPacket : IWorldPacket
    {
        public ulong GUID;
        public ItemDestroyResult Result;
    }
    [PacketType(WorldPackets.CUseItem)]
    public struct CUseItemPacket : IWorldPacket
    {
        public ulong ItemGUID;
        public ulong TargetGUID;
    }
    [PacketType(WorldPackets.SUseItemResult)]
    public struct SUseItemResultPacket : IWorldPacket
    {
        public ulong GUID;
        public ItemUseResult Result;
    }
    [PacketType(WorldPackets.CEquipItem)]
    public struct CEquipItemPacket : IWorldPacket
    {
        public ulong GUID;
        public EquipSlot Slot;
    }
    [PacketType(WorldPackets.SEquipItemResult)]
    public struct SEquipItemResultPacket : IWorldPacket
    {
        public ulong GUID;
        public ItemEquipResult Result;
    }
    [PacketType(WorldPackets.CUnequipItem)]
    public struct CUnequipItemPacket : IWorldPacket
    {
        public ulong GUID;
        public byte NewBagIndex;
        public byte NewSlot;
    }
    [PacketType(WorldPackets.SUnequipItemResult)]
    public struct SUnequipItemResultPacket : IWorldPacket
    {
        public ulong GUID;
        public byte NewBagIndex;
        public byte NewSlot;
        public ItemUnequipResult Result;
    }
    [PacketType(WorldPackets.SItemsAdded)]
    public struct SItemsAddedPacket : IWorldPacket
    {
        public int ID;
        public int Count;
    }
    [PacketType(WorldPackets.SItemsRemoved)]
    public struct SItemsRemovedPacket : IWorldPacket
    {
        public int ID;
        public int Count;
    }
    [PacketType(WorldPackets.CLootCreature)]
    public struct CLootCreaturePacket : IWorldPacket
    {
        public ulong GUID;
    }
    [PacketType(WorldPackets.CCancelLooting)]
    public struct CCancelLootingPacket : IWorldPacket
    {
        public ulong GUID;
    }
    [PacketType(WorldPackets.SCancelLooting)]
    public struct SCancelLootingPacket : IWorldPacket
    {
        public ulong GUID;
    }
    [PacketType(WorldPackets.SLootResult)]
    public struct SLootResultPacket : IWorldPacket
    {
        public ulong SourceGUID;
        public LootingResult Result;
        public int ItemCount;
        public int Money;
    }
    [PacketType(WorldPackets.SLootResultItem)]
    public struct SLootResultItemPacket : IWorldPacket
    {
        public ulong SourceGUID;
        public LootType LootType;
        public byte ItemIndex;
        public int ItemID;
        public int ItemCount;
    }
    [PacketType(WorldPackets.CLootItem)]
    public struct CLootItemPacket : IWorldPacket
    {
        public ulong SourceGUID;
        public byte ItemIndex;
    }
    [PacketType(WorldPackets.SLootItemResult)]
    public struct SLootItemResultPacket : IWorldPacket
    {
        public ulong SourceGUID;
        public byte ItemIndex;
        public ItemTakeResult Result;
    }
    [PacketType(WorldPackets.SLootItemRemoved)]
    public struct SLootItemRemovedPacket : IWorldPacket
    {
        public ulong SourceGUID;
        public byte ItemIndex;
    }
    #endregion
    #region Quests
    [PacketType(WorldPackets.SQuestAdded)]
    public struct SQuestAddedPacket : IWorldPacket
    {
        public bool Initial;
        public int ID;
        public bool Completed;
    }
    [PacketType(WorldPackets.SQuestRemoved)]
    public struct SQuestRemovedPacket : IWorldPacket
    {
        public int ID;
    }
    [PacketType(WorldPackets.SQuestCompleted)]
    public struct SQuestCompletedPacket : IWorldPacket
    {
        public bool Initial;
        public int ID;
    }
    [PacketType(WorldPackets.SQuestObjectiveUpdate)]
    public struct SQuestObjectiveUpdatePacket : IWorldPacket
    {
        public bool Initial;
        public int QuestID;
        public byte ObjectiveIndex;
        public int CurrentProgress;
        public bool Completed;
    }
    [PacketType(WorldPackets.SAddQuestResult)]
    public struct SAddQuestResultPacket : IWorldPacket
    {
        public int ID;
        public QuestAcceptResult Result;
    }
    [PacketType(WorldPackets.SRemoveQuestResult)]
    public struct SRemoveQuestResultPacket : IWorldPacket
    {
        public int ID;
        public QuestRemoveResult Result;
    }
    [PacketType(WorldPackets.SCompleteQuestResult)]
    public struct SCompleteQuestResultPacket : IWorldPacket
    {
        public int ID;
        public QuestCompleteResult Result;
    }
    [PacketType(WorldPackets.CAbandonQuest)]
    public struct CAbandonQuestPacket : IWorldPacket
    {
        public int ID;
    }
    #endregion
    #region Data
    [PacketType(WorldPackets.CTextQuery)]
    public struct CTextQueryPacket : IWorldPacket
    {
        public int ID;
    }
    [PacketType(WorldPackets.STextResponse, true)]
    public struct STextResponsePacket : IWorldPacket
    {
        public int ID;
    }
    [PacketType(WorldPackets.CGameStringsQuery)]
    public struct CGameStringsQueryPacket : IWorldPacket { }
    [PacketType(WorldPackets.SGameStringsResponse, true)]
    public struct SGameStringsResponsePacket : IWorldPacket { }
    [PacketType(WorldPackets.CMapTemplateQuery)]
    public struct CMapTemplateQueryPacket : IWorldPacket
    {
        public int ID;
    }
    [PacketType(WorldPackets.SMapTemplateResponse, true)]
    public struct SMapTemplateResponsePacket : IWorldPacket
    {
        public int ID;
    }
    [PacketType(WorldPackets.CWorldObjectTemplateQuery)]
    public struct CWorldObjectTemplateQueryPacket : IWorldPacket
    {
        public int ID;
    }
    [PacketType(WorldPackets.SWorldObjectTemplateResponse, true)]
    public struct SWorldObjectTemplateResponsePacket : IWorldPacket
    {
        public int ID;
    }
    [PacketType(WorldPackets.CCreatureTemplateQuery)]
    public struct CCreatureTemplateQueryPacket : IWorldPacket
    {
        public int ID;
    }
    [PacketType(WorldPackets.SCreatureTemplateResponse, true)]
    public struct SCreatureTemplateResponsePacket : IWorldPacket
    {
        public int ID;
    }
    [PacketType(WorldPackets.CSpellTemplateQuery)]
    public struct CSpellTemplateQueryPacket : IWorldPacket
    {
        public int ID;
    }
    [PacketType(WorldPackets.SSpellTemplateResponse, true)]
    public struct SSpellTemplateResponsePacket : IWorldPacket
    {
        public int ID;
    }
    [PacketType(WorldPackets.CSpellVisualQuery)]
    public struct CSpellVisualQueryPacket : IWorldPacket
    {
        public int ID;
    }
    [PacketType(WorldPackets.SSpellVisualResponse, true)]
    public struct SSpellVisualResponsePacket : IWorldPacket
    {
        public int ID;
    }
    [PacketType(WorldPackets.CItemTemplateQuery)]
    public struct CItemTemplateQueryPacket : IWorldPacket
    {
        public int ID;
    }
    [PacketType(WorldPackets.SItemTemplateResponse, true)]
    public struct SItemTemplateResponsePacket : IWorldPacket
    {
        public int ID;
    }
    [PacketType(WorldPackets.CQuestQuery)]
    public struct CQuestQueryPacket : IWorldPacket
    {
        public int ID;
    }
    [PacketType(WorldPackets.SQuestResponse, true)]
    public struct SQuestResponsePacket : IWorldPacket
    {
        public int ID;
    }
    [PacketType(WorldPackets.CFactionQuery)]
    public struct CFactionQueryPacket : IWorldPacket
    {
        public int ID;
    }
    [PacketType(WorldPackets.SFactionResponse, true)]
    public struct SFactionResponsePacket : IWorldPacket
    {
        public int ID;
    }
    [PacketType(WorldPackets.CEnvironmentQuery)]
    public struct CEnvironmentQueryPacket : IWorldPacket
    {
        public int ID;
    }
    [PacketType(WorldPackets.SEnvironmentResponse, true)]
    public struct SEnvironmentResponsePacket : IWorldPacket
    {
        public int ID;
    }
    [PacketType(WorldPackets.CGuildInfoQuery)]
    public struct CGuildInfoQueryPacket : IWorldPacket
    {
        public int UID;
    }
    [PacketType(WorldPackets.SGuildInfoResponse), StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    public struct SGuildInfoResponsePacket : IWorldPacket
    {
        public int UID;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
        public string Title;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 8)]
        public string Tag;
    }
    #endregion
    #region Creatures
    [PacketType(WorldPackets.CTargetUpdate)]
    public struct CTargetUpdatePacket : IWorldPacket
    {
        public ulong TargetGUID;
    }
    [PacketType(WorldPackets.SCreatureEnteredCombat)]
    public struct SCreatureEnteredCombatPacket : IWorldPacket
    {
        public ulong GUID;
    }
    [PacketType(WorldPackets.SCreatureEvaded)]
    public struct SCreatureEvadedPacket : IWorldPacket
    {
        public ulong GUID;
    }
    [PacketType(WorldPackets.SCreatureHealthUpdate)]
    public struct SCreatureHealthUpdatePacket : IWorldPacket
    {
        public ulong GUID;
        public int CurrentHealth;
        public int MaxHealth;
    }
    [PacketType(WorldPackets.SCreatureUsesPowerUpdate)]
    public struct SCreatureUsesPowerUpdatePacket : IWorldPacket
    {
        public ulong GUID;
        public PowerType PowerType;
        public bool Uses;
    }
    [PacketType(WorldPackets.SCreaturePowerUpdate)]
    public struct SCreaturePowerUpdatePacket : IWorldPacket
    {
        public ulong GUID;
        public PowerType PowerType;
        public int CurrentPower;
        public int MaxPower;
    }
    [PacketType(WorldPackets.SCreatureStateUpdate)]
    public struct SCreatureStateUpdatePacket : IWorldPacket
    {
        public ulong GUID;
        public CreatureState State;
    }
    [PacketType(WorldPackets.SCreatureTargetUpdate)]
    public struct SCreatureTargetUpdatePacket : IWorldPacket
    {
        public ulong GUID;
        public ulong TargetGUID;
    }
    [PacketType(WorldPackets.SCreatureMovementTypeUpdate)]
    public struct SCreatureMovementTypeUpdatePacket : IWorldPacket
    {
        public ulong GUID;
        public MovementType MovementType;
        public float MovementSpeedFactor;
    }
    [PacketType(WorldPackets.SCreatureFactionUpdate)]
    public struct SCreatureFactionUpdatePacket : IWorldPacket
    {
        public ulong GUID;
        public int FactionID;
    }
    [PacketType(WorldPackets.SCreatureModelUpdate)]
    public struct SCreatureModelUpdatePacket : IWorldPacket
    {
        public ulong GUID;
        public int ModelID;
    }
    #endregion
    #region Movement
    [PacketType(WorldPackets.CMovementUpdate)]
    public struct CMovementUpdatePacket : IWorldPacket
    {
        public ulong GUID;
        public MovementData Data;
    }
    [PacketType(WorldPackets.SCreatureMovementUpdate)]
    public struct SCreatureMovementUpdatePacket : IWorldPacket
    {
        public ulong GUID;
        public Vector3 Position;
        public byte Type;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 48)]
        public byte[] Data;
    }
    #endregion
    #region Dialog
    [PacketType(WorldPackets.CStartDialog)]
    public struct CStartDialogPacket : IWorldPacket
    {
        public ulong TalkerGUID;
    }
    [PacketType(WorldPackets.SStartDialogResult)]
    public struct SStartDialogResultPacket : IWorldPacket
    {
        public ulong TalkerGUID;
        public DialogStartResult Result;
    }
    [PacketType(WorldPackets.CEndDialog)]
    public struct CEndDialogPacket : IWorldPacket
    {
        public ulong TalkerGUID;
        public DialogEndReason Reason;
    }
    [PacketType(WorldPackets.SEndDialog)]
    public struct SEndDialogPacket : IWorldPacket
    {
        public ulong TalkerGUID;
        public DialogEndReason Reason;
    }
    [PacketType(WorldPackets.SChangedPage)]
    public struct SChangedPagePacket : IWorldPacket
    {
        public int TextID;
        public byte OptionCount;
        public byte QuestOptionCount;
    }
    [PacketType(WorldPackets.SPageOption)]
    public struct SPageOptionPacket : IWorldPacket
    {
        public byte Index;
        public int IconID;
        public int TextID;
    }
    [PacketType(WorldPackets.SPageQuestOption)]
    public struct SPageQuestOptionPacket : IWorldPacket
    {
        public byte Index;
        public DialogQuestOptionState State;
        public int QuestID;
        public int TextID;
    }
    [PacketType(WorldPackets.CSelectOption)]
    public struct CSelectOptionPacket : IWorldPacket
    {
        public bool IsQuestOption;
        public byte Index;
    }
    [PacketType(WorldPackets.SSelectOptionResult)]
    public struct SSelectOptionResultPacket : IWorldPacket
    {
        public bool IsQuestOption;
        public byte Index;
        public DialogSelectOptionResult Result;
    }
    [PacketType(WorldPackets.SDialogQuestAcceptPage)]
    public struct SDialogQuestAcceptPagePacket : IWorldPacket
    {
        public int QuestID;
        public int TextID;
        public bool CanAccept;
    }
    [PacketType(WorldPackets.SDialogQuestCompletePage)]
    public struct SDialogQuestCompletePagePacket : IWorldPacket
    {
        public int QuestID;
        public int TextID;
        public bool CanComplete;
    }
    #endregion
    #region Combat
    [PacketType(WorldPackets.SCombatInflictedDamage)]
    public struct SCombatInflictedDamagePacket : IWorldPacket
    {
        public ulong AttackerGUID;
        public ulong VictimGUID;
        public Damage Damage;
    }
    [PacketType(WorldPackets.SCombatHealed)]
    public struct SCombatHealedPacket : IWorldPacket
    {
        public ulong HealerGUID;
        public ulong TargetGUID;
        public int Heal;
    }
    [PacketType(WorldPackets.SCombatPerformedMeleeAttack)]
    public struct SCombatPerformedMeleeAttackPacket : IWorldPacket
    {
        public ulong AttackerGUID;
        public ulong VictimGUID;
        public AttackType AttackType;
        public AttackResult Result;
    }
    [PacketType(WorldPackets.SCombatPerformedSpellAttack)]
    public struct SCombatPerformedSpellAttackPacket : IWorldPacket
    {
        public ulong CasterGUID;
        public ulong TargetGUID;
        public int SpellID;
        public AttackResult Result;
    }
    [PacketType(WorldPackets.SCombatSpellCasted)]
    public struct SCombatSpellCastedPacket : IWorldPacket
    {
        public ulong CasterGUID;
        public ulong TargetGUID;
        public Vector3 GroundTarget;
        public int SpellID;
    }
    [PacketType(WorldPackets.SCombatSpellCastingStarted)]
    public struct SCombatSpellCastingStartedPacket : IWorldPacket
    {
        public ulong CasterGUID;
        public ulong TargetGUID;
        public Vector3 GroundTarget;
        public int SpellID;
    }
    [PacketType(WorldPackets.SCombatSpellCastingFailed)]
    public struct SCombatSpellCastingFailedPacket : IWorldPacket
    {
        public ulong CasterGUID;
        public ulong TargetGUID;
        public Vector3 GroundTarget;
        public int SpellID;
        public CastSpellResult Result;
    }
    [PacketType(WorldPackets.CCombatStartMeleeAttack)]
    public struct CCombatStartMeleeAttackPacket : IWorldPacket
    {
        public ulong TargetGUID;
    }
    [PacketType(WorldPackets.CCombatStopMeleeAttack)]
    public struct CCombatStopMeleeAttackPacket : IWorldPacket
    {
    }
    [PacketType(WorldPackets.CCombatCastSpell)]
    public struct CCombatCastSpellPacket : IWorldPacket
    {
        public int SpellID;
        public ulong TargetGUID;
    }
    [PacketType(WorldPackets.CCombatCastGroundSpell)]
    public struct CCombatCastGroundSpellPacket : IWorldPacket
    {
        public int SpellID;
        public Vector3 Target;
    }
    [PacketType(WorldPackets.CCombatInterruptSpellCasting)]
    public struct CCombatInterruptSpellCastingPacket : IWorldPacket
    {
    }
    [PacketType(WorldPackets.SHasAura)]
    public struct SHasAuraPacket : IWorldPacket
    {
        public ulong GUID;
        public ulong CasterGUID;
        public int SpellID;
        public int EffectID;
        public float DurationLeft;

        public SHasAuraPacket(AuraBase aura)
        {
            GUID = aura.Owner.GUID;
            CasterGUID = aura.CasterGUID;
            SpellID = aura.Spell.ID;
            EffectID = aura.EffectID;
            DurationLeft = aura.DurationLeft;
        }
    }
    [PacketType(WorldPackets.SAppliedAura)]
    public struct SAppliedAuraPacket : IWorldPacket
    {
        public ulong GUID;
        public ulong CasterGUID;
        public int SpellID;
        public int EffectID;

        public SAppliedAuraPacket(AuraBase aura)
        {
            GUID = aura.Owner.GUID;
            CasterGUID = aura.CasterGUID;
            SpellID = aura.Spell.ID;
            EffectID = aura.EffectID;
        }
    }
    [PacketType(WorldPackets.SRemovedAura)]
    public struct SRemovedAuraPacket : IWorldPacket
    {
        public ulong GUID;
        public ulong CasterGUID;
        public int SpellID;
        public int EffectID;

        public SRemovedAuraPacket(AuraBase aura)
        {
            GUID = aura.Owner.GUID;
            CasterGUID = aura.CasterGUID;
            SpellID = aura.Spell.ID;
            EffectID = aura.EffectID;
        }
    }
    [PacketType(WorldPackets.SAreaEffectPlaced)]
    public struct SAreaEffectPlacedPacket : IWorldPacket
    {
        public ulong GUID;
        public ulong CasterGUID;
        public int SpellID;
        public Vector3 Position;
    }
    [PacketType(WorldPackets.SAreaEffectRemoved)]
    public struct SAreaEffectRemovedPacket : IWorldPacket
    {
        public ulong GUID;
    }
    [PacketType(WorldPackets.CDuelRequest)]
    public struct CDuelRequestPacket : IWorldPacket
    {
        public ulong OpponentGUID;
    }
    [PacketType(WorldPackets.SDuelRequest)]
    public struct SDuelRequestPacket : IWorldPacket
    {
        public ulong ChallengerGUID;
    }
    [PacketType(WorldPackets.CDuelRequestResponse)]
    public struct CDuelRequestResponsePacket : IWorldPacket
    {
        public ulong ChallengerGUID;
        public DuelResponse Response;
    }
    [PacketType(WorldPackets.SDuelStatus)]
    public struct SDuelStatusPacket : IWorldPacket
    {
        public DuelStatus Status;
    }
    [PacketType(WorldPackets.SDuelParticipantStatus)]
    public struct SDuelParticipantStatusPacket : IWorldPacket
    {
        public ulong ParticipantGUID;
        public DuelParticipantStatus Status;
    }
    #endregion
    #region World
    [PacketType(WorldPackets.SChangeMap)]
    public struct SChangeMapPacket : IWorldPacket
    {
        public ulong MapGUID;
        public int MapTemplateID;
        public int MapEnvironmentID;
        public Vector3 Position;
        public Rotation Rotation;
    }
    [PacketType(WorldPackets.SChangeEnvironment)]
    public struct SChangeEnvironmentPacket : IWorldPacket
    {
        public int ID;
    }
    [PacketType(WorldPackets.CTeleportAck)]
    public struct CTeleportAckPacket : IWorldPacket
    {
    }
    [PacketType(WorldPackets.SObjectEnteredVisibility)]
    public struct SObjectEnteredVisibilityPacket : IWorldPacket
    {
        public ulong GUID;
        public int ID;
        public ObjectType Type;
        public Vector3 Position;
        public Rotation Rotation;
        public Vector3 Size;
        public Shape Shape;
    }
    [PacketType(WorldPackets.SObjectLeftVisibility)]
    public struct SObjectLeftVisibilityPacket : IWorldPacket
    {
        public ulong GUID;
    }
    [PacketType(WorldPackets.SObjectUpdateLocation)]
    public struct SObjectUpdateLocationPacket : IWorldPacket
    {
        public ulong GUID;
        public Vector3 Position;
        public Rotation Rotation;
    }
    [PacketType(WorldPackets.SObjectLootRecipientUpdate)]
    public struct SObjectLootRecipientUpdatePacket : IWorldPacket
    {
        public ulong GUID;
        public Looter LootRecipient;
    }
    [PacketType(WorldPackets.SObjectFlagsUpdate)]
    public struct SObjectFlagsUpdatePacket : IWorldPacket
    {
        public ulong GUID;
        public ObjectFlags Flags;
    }
    [PacketType(WorldPackets.SObjectLoadingFinalized)]
    public struct SObjectLoadingFinalizedPacket : IWorldPacket
    {
        public ulong GUID;
    }
    #endregion
    #region Graphics
    [PacketType(WorldPackets.CMaterialQuery)]
    public struct CMaterialQueryPacket : IWorldPacket
    {
        public int ID;
    }
    [PacketType(WorldPackets.SMaterialResponse, true)]
    public struct SMaterialResponsePacket : IWorldPacket
    {
        public int ID;
    }
    [PacketType(WorldPackets.CTextureInfoQuery)]
    public struct CTextureInfoQueryPacket : IWorldPacket
    {
        public int ID;
    }
    [PacketType(WorldPackets.STextureInfoResponse, true)]
    public struct STextureInfoResponsePacket : IWorldPacket
    {
        public int ID;
    }
    [PacketType(WorldPackets.CModelQuery)]
    public struct CModelQueryPacket : IWorldPacket
    {
        public int ID;
    }
    [PacketType(WorldPackets.SModelResponse, true)]
    public struct SModelResponsePacket : IWorldPacket
    {
        public int ID;
    }
    [PacketType(WorldPackets.CParticleSystemQuery)]
    public struct CParticleSystemQueryPacket : IWorldPacket
    {
        public int ID;
    }
    [PacketType(WorldPackets.SParticleSystemResponse, true)]
    public struct SParticleSystemResponsePacket : IWorldPacket
    {
        public int ID;
    }
    #endregion
    #region Misc
    [PacketType(WorldPackets.SWorldSystemMessage), StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    public struct SWorldSystemMessagePacket : IWorldPacket
    {
        public DateTime Time;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
        public string Text;
    }
    #endregion
}