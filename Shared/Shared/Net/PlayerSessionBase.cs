﻿using Arcanos.Shared.Players;

namespace Arcanos.Shared.Net
{
    public abstract class PlayerSessionBase
    {
        protected PlayerBase Player { get; set; }

        public abstract void ProcessPackets();
        public abstract void ProcessPacket(WorldPackets type, IWorldPacket packet, byte[] rawData);
    }
}