﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.InteropServices;
using Arcanos.Net.Packets;
using Arcanos.Shared.Server;

namespace Arcanos.Shared.Net
{
    public static class LoginPacketsTable
    {
        public static void Fill(Dictionary<LoginPackets, Type> dictionary)
        {
            foreach (object type in Enum.GetValues(typeof(LoginPackets)))
                dictionary.Add((LoginPackets)type, Assembly.GetExecutingAssembly().GetType("Arcanos.Shared.Net." + type + "Packet"));
        }
    }

    public enum LoginPackets : byte
    {
        // Protocol-related
        CLoginProtocolQuery,
        SLoginProtocolResponse,
        CLoginServerVersionQuery,
        SLoginServerVersionResponse,
        CLoginGameVersionQuery,
        SLoginGameVersionResponse,
        // Login-related
        CLoginLoginRequest,
        SLoginLoginResult,
        // World server-related queries
        SWorldServerEntry,
        // Misc
        SLoginSystemMessage,
    }
    public interface ILoginPacket { }

    [PacketType(LoginPackets.CLoginProtocolQuery)]
    public struct CLoginProtocolQueryPacket : ILoginPacket { }
    [PacketType(LoginPackets.SLoginProtocolResponse)]
    public struct SLoginProtocolResponsePacket : ILoginPacket
    {
        public int ProtocolVersion;
    }
    [PacketType(LoginPackets.CLoginServerVersionQuery)]
    public struct CLoginServerVersionQueryPacket : ILoginPacket { }
    [PacketType(LoginPackets.SLoginServerVersionResponse)]
    public struct SLoginServerVersionResponsePacket : ILoginPacket
    {
        public byte Major;
        public byte Minor;
        public int Build;
        public Localization.LocaleIndex Locale;
    }
    [PacketType(LoginPackets.CLoginGameVersionQuery)]
    public struct CLoginGameVersionQueryPacket : ILoginPacket { }
    [PacketType(LoginPackets.SLoginGameVersionResponse)]
    public struct SLoginGameVersionResponsePacket : ILoginPacket
    {
        public byte Major;
        public int Minor;
        public int Build;
        public Localization.LocaleIndex Locale;
    }
    [PacketType(LoginPackets.CLoginLoginRequest), StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    public struct CLoginLoginRequestPacket : ILoginPacket
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
        public string UsernameOrEmail;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
        public string Password;
    }
    [PacketType(LoginPackets.SLoginLoginResult)]
    public struct SLoginLoginResultPacket : ILoginPacket
    {
        public LoginResult Result;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public byte[] LoginToken;
    }

    [PacketType(LoginPackets.SWorldServerEntry), StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    public struct SWorldServerEntryPacket : ILoginPacket
    {
        public int ID;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
        public string ServerName;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
        public string URL;
    }

    [PacketType(LoginPackets.SLoginSystemMessage), StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    public struct SLoginSystemMessagePacket : ILoginPacket
    {
        public DateTime Time;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
        public string Text;
    }
}