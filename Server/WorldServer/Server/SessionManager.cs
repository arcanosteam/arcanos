﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Arcanos.Server.WorldServer;
using Arcanos.Server.WorldServer.Data;
using Arcanos.Server.WorldServer.Net;
using Arcanos.Server.WorldServer.Players;
using Arcanos.Shared.Net;
using Arcanos.Shared.Server;
using Arcanos.Utilities;

namespace Arcanos.Server
{
    public static class SessionManager
    {
        public static TcpListener ConnectionListener;
        public static readonly List<PlayerSession> ConnectedSessions = new List<PlayerSession>();
        private static readonly object sessionsLock = new object();

        public static void StartListening()
        {
            ConnectionListener = new TcpListener(IPAddress.Any, WorldConstants.PORT);
            ConnectionListener.Start();
            ConnectionListener.BeginAcceptTcpClient(ConnectionAccepted, null);
        }
        public static void StopListening()
        {
            ConnectionListener.Stop();
        }
        public static void DropAllConnections()
        {
            lock (sessionsLock)
                foreach (PlayerSession session in ConnectedSessions)
                {
                    session.Connection.Stop();
                    session.Connection.Client.Close();
                }
        }

        public static void SendSystemMessage(string text)
        {
            foreach (PlayerSession session in ConnectedSessions)
                session.SendSystemMessage(text);
        }

        private static void ConnectionAccepted(IAsyncResult iar)
        {
            TcpClient client;
            try
            {
                client = ConnectionListener.EndAcceptTcpClient(iar);
            }
            catch
            {
                return;
            }
            ConnectionListener.BeginAcceptTcpClient(ConnectionAccepted, null);

            PlayerSession session = new PlayerSession();
            session.StartConnect(client);
            session.Connection.PacketReceived += (type, packet, rawData) => Logger.Trace(LogCategory.Packets, "[PCGUID:{2}] Recv {0}: {1}", type, Logger.Unwrap(packet), session.Player == null || session.Player.CurrentCharacter == null || session.Creature == null ? -1 : (int)session.Creature.GUID);
            session.Connection.PacketSent += (type, packet, rawData) => Logger.Trace(LogCategory.Packets, "[PCGUID:{2}] Sent {0}: {1}", type, Logger.Unwrap(packet), session.Player == null || session.Player.CurrentCharacter == null || session.Creature == null ? -1 : (int)session.Creature.GUID);
            session.EndConnect();

            lock (sessionsLock)
                ConnectedSessions.Add(session);
        }

        public static void HandleCLoginRequestPacket(PlayerSession session, IWorldPacket sPacket)
        {
            CWorldLoginRequestPacket packet = (CWorldLoginRequestPacket)sPacket;
            byte[] token = packet.LoginToken;
            StringBuilder sb = new StringBuilder(16 * 2);
            for (int i = 0; i < 16; i++)
                sb.Append(token[i].ToString("X2"));
            string filename = Path.Combine(ArcanosWorldServer.TokenPath, sb.ToString());
            if (!File.Exists(filename))
            {
                session.Connection.Write(new SWorldLoginResultPacket
                {
                    Result = WorldLoginResult.NotLoggedIn,
                });
                return;
            }
            StreamReader sr = new StreamReader(filename, Encoding.UTF8);
            string account = sr.ReadToEnd();
            sr.Close();
            Logger.Info(LogCategory.Connection, "Login attempt via token {0} (account: \"{1}\")", sb.ToString(), account);
            Player player = DatabaseManager.Players.Find(p => p.Name == account);
            if (player == null)
            {
                session.Connection.Write(new SWorldLoginResultPacket
                {
                    Result = WorldLoginResult.AccountNotFound,
                });
                return;
            }

            session.Locale = packet.Locale;
            session.SetPlayer(player);
            session.Connection.Write(new SWorldLoginResultPacket
            {
                PlayerID = player.ID,
                Result = WorldLoginResult.Ok,
            });
            Logger.Info(LogCategory.Connection, "Successful");
        }
        public static void ProcessPackets()
        {
            lock (sessionsLock)
                foreach (PlayerSession session in ConnectedSessions)
                    session.ProcessPackets();
        }
    }
}