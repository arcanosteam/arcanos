﻿using Arcanos.Shared.Localization;

namespace Arcanos.Server.WorldServer.Chat
{
    public class CustomChatChannel : ChatChannel
    {
        public string CustomName;

        public override string GetNameForLocale(LocaleIndex locale)
        {
            return CustomName;
        }
    }
}