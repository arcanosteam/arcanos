﻿using System.Collections.Generic;
using System.Text;
using Arcanos.Server.WorldServer.Objects;
using Arcanos.Server.WorldServer.Players;
using Arcanos.Shared;
using Arcanos.Shared.Chat;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Net;
using Arcanos.Shared.Objects;
using Microsoft.Xna.Framework.Graphics;

namespace Arcanos.Server.WorldServer.Chat
{
    public static class ChatManager
    {
        public static Dictionary<int, Text> GlobalChannelNames;
        public static Dictionary<int, Text> GlobalChannelAliases;

        public const int CHANNEL_WHISPER = -1;
        public const int CHANNEL_LOCAL_SAY = -2;
        public const int CHANNEL_LOCAL_YELL = -3;
        public const int CHANNEL_LOCAL_EMOTE = -4;
        public const int CHANNEL_MAP = -10;
        public const int CHANNEL_GROUP = -20;
        public const int CHANNEL_GUILD = -30; // TODO

        public const float LOCAL_SAY_DISTANCE = 10;
        public const float LOCAL_YELL_DISTANCE = 25;
        public const float LOCAL_EMOTE_DISTANCE = 15;

        public static readonly Dictionary<int, ChatChannel> Channels = new Dictionary<int, ChatChannel>();
        public static readonly List<int> BannedUsers = new List<int>();
        public static readonly List<int> MutedUsers = new List<int>();

        private static int nextChannelID = 1;

        public static void CreateGlobalChannels()
        {
            Channels.Add(CHANNEL_WHISPER, null);
            Channels.Add(CHANNEL_LOCAL_SAY, new LocalChatChannel { ID = CHANNEL_LOCAL_SAY, Name = GlobalChannelNames[CHANNEL_LOCAL_SAY], Alias = GlobalChannelAliases[CHANNEL_LOCAL_SAY], Distance = LOCAL_SAY_DISTANCE, Color = Color.White });
            Channels.Add(CHANNEL_LOCAL_YELL, new LocalChatChannel { ID = CHANNEL_LOCAL_YELL, Name = GlobalChannelNames[CHANNEL_LOCAL_YELL], Alias = GlobalChannelAliases[CHANNEL_LOCAL_YELL], Distance = LOCAL_YELL_DISTANCE, Color = Color.IndianRed });
            Channels.Add(CHANNEL_LOCAL_EMOTE, new LocalChatChannel { ID = CHANNEL_LOCAL_EMOTE, Name = GlobalChannelNames[CHANNEL_LOCAL_EMOTE], Alias = GlobalChannelAliases[CHANNEL_LOCAL_EMOTE], Distance = LOCAL_EMOTE_DISTANCE, Color = Color.Orange });
            Channels.Add(CHANNEL_MAP, new MapChatChannel { ID = CHANNEL_MAP, Name = GlobalChannelNames[CHANNEL_MAP], Alias = GlobalChannelAliases[CHANNEL_MAP], Color = Color.PaleVioletRed });
            Channels.Add(CHANNEL_GROUP, new GroupChatChannel { ID = CHANNEL_GROUP, Name = GlobalChannelNames[CHANNEL_GROUP], Alias = GlobalChannelAliases[CHANNEL_GROUP], Color = Color.LightBlue });
            //Channels.Add(CHANNEL_GUILD, new GuildChatChannel { ID = CHANNEL_GUILD, Name = "Guild" });
        }

        private static ChatChannelJoinResult InnerJoin(Player player, string channelName, out ChatChannel joinedChannel)
        {
            foreach (KeyValuePair<int, ChatChannel> channel in Channels)
                if (channel.Value is MapRestrictedChatChannel)
                {
                    if (channel.Value.Name != null)
                        foreach (string localizedChannelName in channel.Value.Name.LocalizedStrings)
                            if (string.Compare(localizedChannelName, channelName, System.StringComparison.CurrentCultureIgnoreCase) == 0)
                                return InnerJoin(player, channel.Key, out joinedChannel);
                }
                else if (channel.Value is CustomChatChannel)
                    if (string.IsNullOrEmpty((channel.Value as CustomChatChannel).CustomName))
                        if (string.Compare((channel.Value as CustomChatChannel).CustomName, channelName, System.StringComparison.CurrentCultureIgnoreCase) == 0)
                            return InnerJoin(player, channel.Key, out joinedChannel);

            int channelID = nextChannelID++;
            Channels.Add(channelID, new CustomChatChannel { ID = channelID, CustomName = channelName, Color = Color.GhostWhite });
            return InnerJoin(player, channelID, out joinedChannel);
        }
        private static ChatChannelJoinResult InnerJoin(Player player, int channelID, out ChatChannel joinedChannel)
        {
            if (!Channels.TryGetValue(channelID, out joinedChannel))
                return ChatChannelJoinResult.NotFound;

            return joinedChannel.Join(player);
        }
        private static ChatChannelLeaveResult InnerLeave(Player player, int channelID, out ChatChannel leftChannel)
        {
            if (!Channels.TryGetValue(channelID, out leftChannel))
                return ChatChannelLeaveResult.NotFound;

            return leftChannel.Leave(player);
        }

        public static void Join(Player player, string channelName)
        {
            ChatChannel channel;
            ChatChannelJoinResult result = InnerJoin(player, channelName, out channel);
            if (result == ChatChannelJoinResult.Ok)
                player.Session.SendPacket(new SChatChannelJoinResultPacket
                {
                    Result = result,
                    ChannelID = channel.ID,
                    ChannelNameID = channel.Name == null ? 0 : channel.Name.ID,
                    ChannelAliasID = channel.Name == null ? 0 : channel.Alias.ID,
                    CustomChannelName = channel.GetNameForLocale(player.Session.Locale),
                    ChannelColor = channel.Color,
                });
            else
                player.Session.SendPacket(new SChatChannelJoinResultPacket { Result = result });
        }
        public static void Join(Player player, int channelID)
        {
            ChatChannel channel;
            ChatChannelJoinResult result = InnerJoin(player, channelID, out channel);
            if (result == ChatChannelJoinResult.Ok)
                player.Session.SendPacket(new SChatChannelJoinResultPacket
                {
                    Result = result,
                    ChannelID = channel.ID,
                    ChannelNameID = channel.Name.ID,
                    ChannelAliasID = channel.Alias.ID,
                    CustomChannelName = channel.GetNameForLocale(player.Session.Locale),
                    ChannelColor = channel.Color,
                });
            else
                player.Session.SendPacket(new SChatChannelJoinResultPacket { Result = result });
        }
        public static void Leave(Player player, int channelID)
        {
            ChatChannel channel;
            ChatChannelLeaveResult result = InnerLeave(player, channelID, out channel);
            player.Session.SendPacket(new SChatChannelLeaveResultPacket { Result = result, ChannelID = channel.ID });
        }
        public static void Leave(Player player)
        {
            foreach (ChatChannel channel in GetJoinedChannels(player))
                Leave(player, channel.ID);
        }
        public static IEnumerable<ChatChannel> GetJoinedChannels(Player player)
        {
            foreach (KeyValuePair<int, ChatChannel> channel in Channels)
                if (channel.Value != null && channel.Value.Listeners.Contains(player))
                    yield return channel.Value;
        }

        public static ChatMessageResult Whisper(WorldObjectBase sender, PlayerCreature listener, ref ChatMessage message)
        {
            if (listener == null)
                return ChatMessageResult.UnknownTarget;

            return Whisper(sender, listener.Player, ref message);
        }
        public static ChatMessageResult Whisper(WorldObjectBase sender, Player listener, ref ChatMessage message)
        {
            if (listener == null)
                return ChatMessageResult.UnknownTarget;

            if (sender.Type.IsPlayer())
                (sender as PlayerCreature).Player.Session.SendPacket(new SChatMessagePacket
                {
                    SenderGUID = listener.CurrentCharacter.Creature.GUID,
                    ChannelID = CHANNEL_WHISPER,
                    Timestamp = message.Timestamp,
                    Flags = message.Flags | ChatMessageFlags.WhisperFromYou,
                }, Encoding.UTF8.GetBytes(message.Message));
            SayTo(listener, sender, null, ref message);
            return ChatMessageResult.Ok;
        }
        public static ChatMessageResult Say(WorldObjectBase sender, ref ChatMessage message)
        {
            return Broadcast(sender, CHANNEL_LOCAL_SAY, ref message);
        }
        public static ChatMessageResult Yell(WorldObjectBase sender, ref ChatMessage message)
        {
            return Broadcast(sender, CHANNEL_LOCAL_YELL, ref message);
        }
        public static ChatMessageResult Emote(WorldObjectBase sender, ref ChatMessage message)
        {
            return Broadcast(sender, CHANNEL_LOCAL_EMOTE, ref message);
        }
        public static ChatMessageResult Map(WorldObjectBase sender, ref ChatMessage message)
        {
            return Broadcast(sender, CHANNEL_MAP, ref message);
        }
        public static ChatMessageResult Group(WorldObjectBase sender, ref ChatMessage message)
        {
            return Broadcast(sender, CHANNEL_GROUP, ref message);
        }
        public static ChatMessageResult Guild(WorldObjectBase sender, ref ChatMessage message)
        {
            return Broadcast(sender, CHANNEL_GUILD, ref message);
        }

        public static ChatMessageResult Broadcast(WorldObjectBase sender, int channelID, ref ChatMessage message)
        {
            if (sender == null)
                return ChatMessageResult.UnknownSender;

            if (sender.Type.IsPlayer() && BannedUsers.Contains((sender as PlayerCreature).Player.ID))
                return ChatMessageResult.GloballyBanned;

            if (sender.Type.IsPlayer() && MutedUsers.Contains((sender as PlayerCreature).Player.ID))
                return ChatMessageResult.GloballyMuted;

            ChatChannel channel;
            if (!Channels.TryGetValue(channelID, out channel))
                return ChatMessageResult.UnknownChannel;

            return channel.Broadcast(sender, ref message);
        }

        public static void SayTo(Player listener, WorldObjectBase sender, ChatChannel channel, ref ChatMessage message)
        {
            listener.Session.SendPacket(new SChatMessagePacket
            {
                SenderGUID = sender.GUID,
                ChannelID = channel == null ? CHANNEL_WHISPER : channel.ID,
                Timestamp = message.Timestamp,
                Flags = message.Flags,
            }, Encoding.UTF8.GetBytes(message.Message));
        }
    }
}