﻿using Arcanos.Server.WorldServer.Objects;
using Arcanos.Server.WorldServer.Players;
using Arcanos.Shared;
using Arcanos.Shared.Chat;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Players;

namespace Arcanos.Server.WorldServer.Chat
{
    public class GroupChatChannel : ChatChannel
    {
        public override ChatMessageResult Broadcast(WorldObjectBase sender, ref ChatMessage message)
        {
            if (!sender.Type.IsPlayer())
                return ChatMessageResult.UnknownSender;

            Character character = (sender as PlayerCreature).Character;
            if (character.Group == null)
                return ChatMessageResult.NoListeners;

            int listened = 0;
            for (byte i = 0; i < GroupBase.MAX_GROUP_MEMBERS; ++i)
                if (character.Group.Members[i] != null)
                {
                    ChatManager.SayTo(character.Group.Members[i].Player as Player, sender, this, ref message);
                    ++listened;
                }

            return listened == 0 ? ChatMessageResult.NoListeners : ChatMessageResult.Ok;
        }
    }
}