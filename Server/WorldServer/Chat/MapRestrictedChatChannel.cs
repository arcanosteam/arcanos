﻿using Arcanos.Server.WorldServer.Objects;
using Arcanos.Server.WorldServer.World;
using Arcanos.Shared;
using Arcanos.Shared.Chat;
using Arcanos.Shared.Objects;

namespace Arcanos.Server.WorldServer.Chat
{
    public class MapRestrictedChatChannel : MapChatChannel
    {
        public Map RestrictedMap;

        public override ChatMessageResult Broadcast(WorldObjectBase sender, ref ChatMessage message)
        {
            if (sender.Type.IsPlayer() && BannedUsers.Contains((sender as PlayerCreature).Player.ID))
                return ChatMessageResult.BannedInChannel;

            if (sender.Type.IsPlayer() && MutedUsers.Contains((sender as PlayerCreature).Player.ID))
                return ChatMessageResult.MutedInChannel;

            if (RestrictedMap != null && sender.Map != RestrictedMap)
                return ChatMessageResult.WrongMap;

            return base.Broadcast(sender, ref message);
        }
    }
}