﻿using Arcanos.Server.WorldServer.Objects;
using Arcanos.Shared;
using Arcanos.Shared.Chat;
using Arcanos.Shared.Objects;

namespace Arcanos.Server.WorldServer.Chat
{
    public class LocalChatChannel : ChatChannel
    {
        public float Distance;

        public override ChatMessageResult Broadcast(WorldObjectBase sender, ref ChatMessage message)
        {
            if (sender.Type.IsPlayer() && BannedUsers.Contains((sender as PlayerCreature).Player.ID))
                return ChatMessageResult.BannedInChannel;

            if (sender.Type.IsPlayer() && MutedUsers.Contains((sender as PlayerCreature).Player.ID))
                return ChatMessageResult.MutedInChannel;

            int listened = 0;
            foreach (WorldObjectBase wo in sender.Map.GetNearbyObjects(sender.Position, Distance))
            {
                if (!wo.Type.IsPlayer())
                    continue;

                ChatManager.SayTo((wo as PlayerCreature).Player, sender, this, ref message);
                ++listened;
            }
            
            return listened == 0 ? ChatMessageResult.NoListeners : ChatMessageResult.Ok;
        }
    }
}