﻿using Arcanos.Server.WorldServer.Objects;
using Arcanos.Server.WorldServer.World;
using Arcanos.Shared;
using Arcanos.Shared.Chat;
using Arcanos.Shared.Objects;

namespace Arcanos.Server.WorldServer.Chat
{
    public class MapChatChannel : ChatChannel
    {
        public override ChatMessageResult Broadcast(WorldObjectBase sender, ref ChatMessage message)
        {
            if (sender.Type.IsPlayer() && BannedUsers.Contains((sender as PlayerCreature).Player.ID))
                return ChatMessageResult.BannedInChannel;

            if (sender.Type.IsPlayer() && MutedUsers.Contains((sender as PlayerCreature).Player.ID))
                return ChatMessageResult.MutedInChannel;

            int listened = 0;
            foreach (PlayerCreature listener in (sender.Map as Map).Players)
            {
                ChatManager.SayTo(listener.Player, sender, this, ref message);
                ++listened;
            }

            return listened == 0 ? ChatMessageResult.NoListeners : ChatMessageResult.Ok;
        }
    }
}