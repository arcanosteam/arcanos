﻿using System.Collections.Generic;
using Arcanos.Server.WorldServer.Objects;
using Arcanos.Server.WorldServer.Players;
using Arcanos.Shared;
using Arcanos.Shared.Chat;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Objects;
using Microsoft.Xna.Framework.Graphics;

namespace Arcanos.Server.WorldServer.Chat
{
    public abstract class ChatChannel
    {
        public int ID;
        public Text Name;
        public Text Alias;
        public Color Color;

        public readonly List<Player> Listeners = new List<Player>();
        public readonly List<int> BannedUsers = new List<int>();
        public readonly List<int> MutedUsers = new List<int>();

        public virtual ChatMessageResult Broadcast(WorldObjectBase sender, ref ChatMessage message)
        {
            if (sender.Type.IsPlayer() && BannedUsers.Contains((sender as PlayerCreature).Player.ID))
                return ChatMessageResult.BannedInChannel;

            if (sender.Type.IsPlayer() && MutedUsers.Contains((sender as PlayerCreature).Player.ID))
                return ChatMessageResult.MutedInChannel;

            foreach (Player listener in Listeners)
                ChatManager.SayTo(listener, sender, this, ref message);

            return ChatMessageResult.Ok;
        }

        public virtual ChatChannelJoinResult Join(Player player)
        {
            if (BannedUsers.Contains(player.ID))
                return ChatChannelJoinResult.Banned;
            if (Listeners.Contains(player))
                return ChatChannelJoinResult.AlreadyJoined;

            Listeners.Add(player);
            return ChatChannelJoinResult.Ok;
        }
        public virtual ChatChannelLeaveResult Leave(Player player)
        {
            if (!Listeners.Remove(player))
                return ChatChannelLeaveResult.NotJoined;

            return ChatChannelLeaveResult.Ok;
        }
        public virtual string GetNameForLocale(LocaleIndex locale)
        {
            return Name.LocalizedStrings[(byte)locale];
        }
    }
}