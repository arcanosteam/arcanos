using System.Collections.Generic;
using Arcanos.Server.WorldServer.Objects;
using Arcanos.Server.WorldServer.Quests;
using Arcanos.Shared.Dialog;
using Arcanos.Shared.Objects;

namespace Arcanos.Server.WorldServer.Dialog
{
    public interface IDialogTalker : IReferencedByGUID
    {
        DialogStart GetDialogStart(PlayerCreature player, out DialogPage startingPage);
        bool CanEndDialog(PlayerCreature player, DialogPage page);

        void StartedDialog(PlayerCreature player, DialogStart dialog, DialogPage page);
        void EndedDialog(PlayerCreature player, DialogPage page, DialogEndReason reason);
        bool SelectOption(PlayerCreature player, DialogPage page, DialogOption option, int index);
        void ChangedPage(PlayerCreature player, DialogPage currentPage, ref DialogPage nextPage);
        
        IList<Quest> GetQuestsFor(PlayerCreature player);
    }
}