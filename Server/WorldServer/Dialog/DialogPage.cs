﻿using System.Collections.Generic;
using Arcanos.Server.WorldServer.Data;
using Arcanos.Server.WorldServer.Objects;
using Arcanos.Shared;
using Arcanos.Shared.Data;
using Arcanos.Shared.Localization;

namespace Arcanos.Server.WorldServer.Dialog
{
    [SerializationSettings(Method = SerializationMethod.RefByID, ReferenceResolverType = typeof(DatabaseManager), ReferenceResolverMethod = "GetDialogPage")]
    public class DialogPage : Template
    {
        public override EditorEntry EditorEntry
        {
            get { return new EditorEntry(ID, Text); }
        }

        public Text Text;
        public List<DialogOption> Options = new List<DialogOption>();

        protected DialogPage() { }
        public DialogPage(Text text, params DialogOption[] options)
        {
            Text = text;
            Options.AddRange(options);
        }

        public IList<DialogOption> GetOptionsFor(PlayerCreature player)
        {
            // TODO: Options depending on conditions
            return Options;
        }

        protected override int GetInitialID()
        {
            return DatabaseManager.DialogPages.GetID(this);
        }
    }
}