﻿using System.Collections.Generic;
using Arcanos.Server.WorldServer.Data;
using Arcanos.Server.WorldServer.Objects;

namespace Arcanos.Server.WorldServer.Dialog
{
    public class DialogStart
    {
        public List<DialogPage> Pages = new List<DialogPage>();

        public DialogStart() { }
        public DialogStart(params DialogPage[] pages)
        {
            Pages.AddRange(pages);
        }
        public DialogStart(params int[] pages)
        {
            foreach (int page in pages)
                Pages.Add(DatabaseManager.DialogPages[page]);
        }

        public DialogPage GetStartingPageFor(PlayerCreature player)
        {
            // TODO: Conditions for pages and randomness
            return Pages[0];
        }
    }
}