﻿using Arcanos.Server.WorldServer.Data;
using Arcanos.Server.WorldServer.Objects;
using Arcanos.Shared;
using Arcanos.Shared.Data;
using Arcanos.Shared.Dialog;
using Arcanos.Shared.Localization;

namespace Arcanos.Server.WorldServer.Dialog
{
    [SerializationSettings(Method = SerializationMethod.RefByID, ReferenceResolverType = typeof(DatabaseManager), ReferenceResolverMethod = "GetDialogOption")]
    public class DialogOption : Template
    {
        public override EditorEntry EditorEntry
        {
            get { return new EditorEntry(ID, Text); }
        }

        public Text Text;
        public int Icon;
        public int NextPageID;

        protected DialogOption() { }
        public DialogOption(Text text, int icon, int nextPage)
        {
            Text = text;
            Icon = icon;
            NextPageID = nextPage;
        }
        public DialogOption(Text text, int icon) : this(text, icon, 0) { }

        public bool DoAction(PlayerCreature player, IDialogTalker talker)
        {
            if (NextPageID != 0)
                player.ChangePage(DatabaseManager.DialogPages[NextPageID]);
            else
                player.EndDialog(DialogEndReason.DialogEnded);
            return true;
        }

        protected override int GetInitialID()
        {
            return DatabaseManager.DialogOptions.GetID(this);
        }
    }
}