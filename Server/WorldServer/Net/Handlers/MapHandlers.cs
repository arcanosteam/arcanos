﻿using Arcanos.Shared.Net;

namespace Arcanos.Server.WorldServer.Net.Handlers
{
    public static class MapHandlers
    {
        public static void HandleCTeleportAckPacket(PlayerSession session, IWorldPacket sPacket)
        {
            //CTeleportAckPacket packet = (CTeleportAckPacket)sPacket;

            session.Creature.MarkAsSpawned(true);
            if (session.Creature.Character.JustCreated)
                session.Creature.Character.ProcessEnterMap();
        }
    }
}