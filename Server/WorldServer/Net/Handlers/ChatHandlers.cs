﻿using System.Text;
using Arcanos.Server.WorldServer.Chat;
using Arcanos.Server.WorldServer.Players;
using Arcanos.Server.WorldServer.World;
using Arcanos.Shared.Chat;
using Arcanos.Shared.Net;

namespace Arcanos.Server.WorldServer.Net.Handlers
{
    public static class ChatHandlers
    {
        public static void HandleCChatMessagePacket(PlayerSession session, IWorldPacket sPacket, byte[] rawData)
        {
            CChatMessagePacket packet = (CChatMessagePacket)sPacket;

            ChatMessage message = new ChatMessage
            {
                Message = Encoding.UTF8.GetString(rawData).Trim(),
                Timestamp = packet.Timestamp,
                Flags = packet.Flags,
            };
            if (message.Message.Length == 0)
                return;
            if (message.Message.StartsWith("."))
            {
                Console.Commands.Execute(session, message.Message.Substring(1));
                return;
            }
            ChatMessageResult result = ChatManager.Broadcast(session.Creature, packet.ChannelID, ref message);

            session.SendPacket(new SChatMessageResultPacket { Result = result });
        }
        public static void HandleCChatWhisperPacket(PlayerSession session, IWorldPacket sPacket, byte[] rawData)
        {
            CChatWhisperPacket packet = (CChatWhisperPacket)sPacket;

            ChatMessage message = new ChatMessage
            {
                Message = Encoding.UTF8.GetString(rawData).Trim(),
                Timestamp = packet.Timestamp,
                Flags = packet.Flags,
            };
            if (message.Message.Length == 0)
                return;
            if (message.Message.StartsWith("."))
            {
                Console.Commands.Execute(session, message.Message.Substring(1));
                return;
            }
            Character listener = WorldManager.GetOnlineCharacterByName(packet.TargetName);
            if (listener == null)
            {
                Player listenerPlayer = WorldManager.GetOnlinePlayerByName(packet.TargetName);
                if (listenerPlayer != null)
                    listener = listenerPlayer.CurrentCharacter;
            }
            if (listener == null)
            {
                session.SendPacket(new SChatMessageResultPacket { Result = ChatMessageResult.NoListeners });
                return;
            }

            ChatMessageResult result = ChatManager.Whisper(session.Creature, listener.Player, ref message);

            session.SendPacket(new SChatMessageResultPacket { Result = result });
        }
        public static void HandleCChatChannelJoinPacket(PlayerSession session, IWorldPacket sPacket)
        {
            CChatChannelJoinPacket packet = (CChatChannelJoinPacket)sPacket;

            ChatManager.Join(session.Player, packet.ChannelName);
        }
        public static void HandleCChatChannelLeavePacket(PlayerSession session, IWorldPacket sPacket)
        {
            CChatChannelLeavePacket packet = (CChatChannelLeavePacket)sPacket;

            ChatManager.Leave(session.Player, packet.ChannelID);
        }
    }
}