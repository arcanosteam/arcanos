﻿using System;
using System.Reflection;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Net;
using Arcanos.Shared.Server;

namespace Arcanos.Server.WorldServer.Net.Handlers
{
    public static class ProtocolHandlers
    {
        public static void HandleCProtocolQueryPacket(PlayerSession session, IWorldPacket sPacket)
        {
            //CProtocolQueryPacket packet = (CProtocolQueryPacket)sPacket;
            session.SendPacket(new SWorldProtocolResponsePacket { ProtocolVersion = WorldConstants.PROTOCOL_VERSION });
        }
        public static void HandleCServerVersionQueryPacket(PlayerSession session, IWorldPacket sPacket)
        {
            //CServerVersionQueryPacket packet = (CServerVersionQueryPacket)sPacket;
            Version version = Assembly.GetExecutingAssembly().GetName().Version;
            session.SendPacket(new SWorldServerVersionResponsePacket { Major = (byte)version.Major, Minor = (byte)version.Minor, Build = version.Build, Locale = Localization.CurrentLocale });
        }
        public static void HandleCGameVersionQueryPacket(PlayerSession session, IWorldPacket sPacket)
        {
            //CGameVersionQueryPacket packet = (CGameVersionQueryPacket)sPacket;
            Version version = Assembly.GetExecutingAssembly().GetName().Version;
            session.SendPacket(new SWorldGameVersionResponsePacket { Major = (byte)version.Major, Minor = (byte)version.Minor, Build = version.Build, Locale = Localization.CurrentLocale });
        }
    }
}