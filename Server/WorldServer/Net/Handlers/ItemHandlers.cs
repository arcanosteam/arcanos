﻿using System.Collections.Generic;
using Arcanos.Server.WorldServer.Data;
using Arcanos.Server.WorldServer.Items;
using Arcanos.Server.WorldServer.Players;
using Arcanos.Server.WorldServer.Spells;
using Arcanos.Shared.Items;
using Arcanos.Shared.Net;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Spells;
using Arcanos.Shared.World;

namespace Arcanos.Server.WorldServer.Net.Handlers
{
    public static class ItemHandlers
    {
        public static void HandleCItemDataQueryPacket(PlayerSession session, IWorldPacket sPacket)
        {
            CItemDataQueryPacket packet = (CItemDataQueryPacket)sPacket;

            ItemBase item = DatabaseManager.GameItems[(int)packet.GUID];
            if (item == null)
                return;

            session.SendPacket(new SItemDataResponsePacket
            {
                GUID = item.GUID,
                ID = item.Template.ID,
                Count = item.Count,
            });
        }
        public static void HandleCMoveItemPacket(PlayerSession session, IWorldPacket sPacket)
        {
            CMoveItemPacket packet = (CMoveItemPacket)sPacket;

            if (packet.OldBagIndex == packet.NewBagIndex && packet.OldSlot == packet.NewSlot)
                return;

            Bag oldBag = session.Player.CurrentCharacter.Inventory.Bags[packet.OldBagIndex] as Bag;
            Item item = oldBag.Items[packet.OldSlot] as Item;

            ItemMoveResult result = session.Player.CurrentCharacter.Inventory.CanMoveItem(item, packet.NewBagIndex, packet.NewSlot);
            session.SendPacket(new SMoveItemResultPacket { OldBagIndex = packet.OldBagIndex, OldSlot = packet.OldSlot, NewBagIndex = packet.NewBagIndex, NewSlot = packet.NewSlot, Result = result });
            if (result == ItemMoveResult.Ok)
                session.Player.CurrentCharacter.Inventory.MoveItem(item, packet.NewBagIndex, packet.NewSlot);
        }
        public static void HandleCDivideItemPacket(PlayerSession session, IWorldPacket sPacket)
        {
            CDivideItemPacket packet = (CDivideItemPacket)sPacket;

            if (packet.OldBagIndex == packet.NewBagIndex && packet.OldSlot == packet.NewSlot)
                return;

            Bag oldBag = session.Player.CurrentCharacter.Inventory.Bags[packet.OldBagIndex] as Bag;
            Item item = oldBag.Items[packet.OldSlot] as Item;

            ItemDivideResult result = session.Player.CurrentCharacter.Inventory.CanDivideItem(item, packet.NewBagIndex, packet.NewSlot, packet.NewCount);
            session.SendPacket(new SDivideItemResultPacket { OldBagIndex = packet.OldBagIndex, OldSlot = packet.OldSlot, NewBagIndex = packet.NewBagIndex, NewSlot = packet.NewSlot, NewCount = packet.NewCount, Result = result });
            if (result == ItemDivideResult.Ok)
                session.Player.CurrentCharacter.Inventory.DivideItem(item, packet.NewBagIndex, packet.NewSlot, packet.NewCount);
        }
        public static void HandleCDestroyItemPacket(PlayerSession session, IWorldPacket sPacket)
        {
            CDestroyItemPacket packet = (CDestroyItemPacket)sPacket;

            Item item = DatabaseManager.GameItems[(int)packet.GUID];
            if (item == null)
            {
                session.SendPacket(new SDestroyItemResultPacket { GUID = packet.GUID, Result = ItemDestroyResult.ItemNotFound });
                return;
            }

            if (item.OwnerCreature != session.Creature)
            {
                session.SendPacket(new SDestroyItemResultPacket { GUID = packet.GUID, Result = ItemDestroyResult.NotOwner });
                return;
            }

            ItemDestroyResult result = session.Player.CurrentCharacter.Inventory.CanDestroyItem(item);
            if (result != ItemDestroyResult.Ok)
            {
                session.SendPacket(new SDestroyItemResultPacket { GUID = packet.GUID, Result = result });
                return;
            }

            if (!session.Player.CurrentCharacter.Inventory.RemoveItem(item))
            {
                session.SendPacket(new SDestroyItemResultPacket { GUID = packet.GUID, Result = ItemDestroyResult.NotOwner });
                return;
            }

            session.SendPacket(new SDestroyItemResultPacket { GUID = packet.GUID, Result = ItemDestroyResult.Ok });
        }
        public static void HandleCUseItemPacket(PlayerSession session, IWorldPacket sPacket)
        {
            CUseItemPacket packet = (CUseItemPacket)sPacket;

            Item item = DatabaseManager.GameItems[(int)packet.ItemGUID];
            if (item == null)
            {
                session.SendPacket(new SUseItemResultPacket { GUID = packet.ItemGUID, Result = ItemUseResult.ItemNotFound });
                return;
            }

            CreatureBase target = ObjectManager.GetCreature(packet.TargetGUID);
            if (target == null)
            {
                session.SendPacket(new SUseItemResultPacket { GUID = packet.ItemGUID, Result = ItemUseResult.TargetNotFound });
                return;
            }

            if (item.OwnerCreature != session.Creature)
            {
                session.SendPacket(new SUseItemResultPacket { GUID = packet.ItemGUID, Result = ItemUseResult.NotOwner });
                return;
            }

            if (item.Template.RequiredLevel.Min != 0 && session.Creature.Level < item.Template.RequiredLevel.Min)
            {
                session.SendPacket(new SUseItemResultPacket { GUID = packet.ItemGUID, Result = ItemUseResult.LevelTooLow });
                return;
            }
            if (item.Template.RequiredLevel.Max != 0 && session.Creature.Level > item.Template.RequiredLevel.Max)
            {
                session.SendPacket(new SUseItemResultPacket { GUID = packet.ItemGUID, Result = ItemUseResult.LevelTooHigh });
                return;
            }

            SpellTemplate spell = item.Template.SpellTriggers[(byte)ItemSpellUsage.OnUse] as SpellTemplate;
            if (spell == null)
            {
                session.SendPacket(new SUseItemResultPacket { GUID = packet.ItemGUID, Result = ItemUseResult.NoOnUseTrigger });
                return;
            }

            CastSpellResult result = item.Use(target);
            if (result != CastSpellResult.Ok)
            {
                session.SendPacket(new SUseItemResultPacket { GUID = packet.ItemGUID, Result = ItemUseResult.SpellCastError });
                return;
            }

            session.SendPacket(new SUseItemResultPacket { GUID = packet.ItemGUID, Result = ItemUseResult.Ok });
        }
        public static void HandleCEquipItemPacket(PlayerSession session, IWorldPacket sPacket)
        {
            CEquipItemPacket packet = (CEquipItemPacket)sPacket;

            Item item = DatabaseManager.GameItems[(int)packet.GUID];
            if (item == null)
            {
                session.SendPacket(new SEquipItemResultPacket { GUID = packet.GUID, Result = ItemEquipResult.ItemNotFound });
                return;
            }

            ItemEquipResult result = session.Player.CurrentCharacter.Equip(item, packet.Slot);
            session.SendPacket(new SEquipItemResultPacket { GUID = packet.GUID, Result = result });
        }
        public static void HandleCUnequipItemPacket(PlayerSession session, IWorldPacket sPacket)
        {
            CUnequipItemPacket packet = (CUnequipItemPacket)sPacket;

            Item item = DatabaseManager.GameItems[(int)packet.GUID];
            if (item == null)
            {
                session.SendPacket(new SUnequipItemResultPacket { GUID = packet.GUID, Result = ItemUnequipResult.ItemNotFound });
                return;
            }

            ItemUnequipResult result = session.Player.CurrentCharacter.Unequip(item, packet.NewBagIndex, packet.NewSlot);
            session.SendPacket(new SUnequipItemResultPacket { GUID = packet.GUID, NewBagIndex = packet.NewBagIndex, NewSlot = packet.NewSlot, Result = result });
        }
        public static void HandleCLootCreaturePacket(PlayerSession session, IWorldPacket sPacket)
        {
            CLootCreaturePacket packet = (CLootCreaturePacket)sPacket;

            CreatureBase creature = ObjectManager.GetCreature(packet.GUID);
            if (creature == null)
            {
                session.SendPacket(new SLootResultPacket { SourceGUID = packet.GUID, Result = LootingResult.CreatureNotFound });
                return;
            }
            if (creature.IsAlive)
            {
                session.SendPacket(new SLootResultPacket { SourceGUID = packet.GUID, Result = LootingResult.CreatureNotDead });
                return;
            }
            if (creature.Loot == null)
            {
                session.SendPacket(new SLootResultPacket { SourceGUID = packet.GUID, Result = LootingResult.AlreadyLooted });
                return;
            }
            if (creature.HasFlag(ObjectFlags.BeingLooted))
            {
                session.SendPacket(new SLootResultPacket { SourceGUID = packet.GUID, Result = LootingResult.AlreadyBeingLooted });
                return;
            }
            if (session.Creature.HasFlag(ObjectFlags.Looting) || session.Creature.LootingObject != null)
            {
                session.SendPacket(new SLootResultPacket { SourceGUID = packet.GUID, Result = LootingResult.AlreadyLooting });
                return;
            }
            if (session.Creature.DistanceTo(creature) > MapTemplateBase.INTERACT_DISTANCE)
            {
                session.SendPacket(new SLootResultPacket { SourceGUID = packet.GUID, Result = LootingResult.OutOfRange });
                return;
            }
            if (creature.Loot.Looter.LootingGroup == 0 && creature.Loot.Looter.LootingCharacter != 0 && creature.Loot.Looter.LootingCharacter != session.Creature.Character.ID)
            {
                session.SendPacket(new SLootResultPacket { SourceGUID = packet.GUID, Result = LootingResult.NotLootOwner });
                return;
            }
            if (creature.Loot.Looter.LootingGroup != 0 && (session.Creature.Character.Group == null || session.Creature.Character.Group != null && creature.Loot.Looter.LootingGroup != session.Creature.Character.Group.ID))
            {
                session.SendPacket(new SLootResultPacket { SourceGUID = packet.GUID, Result = LootingResult.NotLootOwner });
                return;
            }

            session.Creature.LootingObject = creature;
            session.Creature.SetFlag(ObjectFlags.Looting);
            Loot loot = creature.Loot as Loot;
            if (loot.GetItemCount() == 0 && loot.Money == 0)
            {
                session.SendPacket(new SLootResultPacket { SourceGUID = packet.GUID, Result = LootingResult.AlreadyLooted });
                return;
            }

            loot.AddLooter(session.Creature);
            /*if (loot.GeneratedItems == null)
            {
                loot.GeneratedItems = new List<ItemDefinition>((creature.Template as CreatureTemplate).Loot.Get(creature, loot.Looter));
                loot.Money = (creature.Template as CreatureTemplate).Money.Random;
            }*/
            int count = loot.GeneratedItems.Count;
            if (loot.HadMoney)
                ++count;
            session.SendPacket(new SLootResultPacket
            {
                SourceGUID = packet.GUID,
                Result = LootingResult.Ok,
                ItemCount = count,
                Money = loot.Money,
            });
            byte i = 0;
            if (loot.HadMoney)
                session.SendPacket(new SLootResultItemPacket
                {
                    SourceGUID = packet.GUID,
                    LootType = LootType.Money,
                    ItemIndex = i++,
                    ItemCount = loot.Money,
                });
            foreach (ItemDefinition item in loot.GeneratedItems)
                if (item == null)
                    session.SendPacket(new SLootResultItemPacket
                    {
                        SourceGUID = packet.GUID,
                        LootType = LootType.None,
                        ItemIndex = i++,
                    });
                else
                    session.SendPacket(new SLootResultItemPacket
                    {
                        SourceGUID = packet.GUID,
                        LootType = LootType.Item,
                        ItemIndex = i++,
                        ItemID = item.Template.ID,
                        ItemCount = item.Count,
                    });
        }
        public static void HandleCCancelLootingPacket(PlayerSession session, IWorldPacket sPacket)
        {
            CCancelLootingPacket packet = (CCancelLootingPacket)sPacket;

            WorldObjectBase wo = ObjectManager.GetWorldObject(packet.GUID);
            if (wo == null)
                return;

            Loot loot = wo.Loot as Loot;
            loot.RemoveLooter(session.Creature);
            session.Creature.LootingObject = null;
            if (session.Creature.HasFlag(ObjectFlags.Looting))
                session.Creature.UnsetFlag(ObjectFlags.Looting);
            if (wo.HasFlag(ObjectFlags.BeingLooted))
                wo.UnsetFlag(ObjectFlags.BeingLooted);
        }
        public static void HandleCLootItemPacket(PlayerSession session, IWorldPacket sPacket)
        {
            CLootItemPacket packet = (CLootItemPacket)sPacket;

            WorldObjectBase wo = ObjectManager.GetWorldObject(packet.SourceGUID);
            if (wo == null)
                return;

            if (session.Creature.LootingObject != wo || !session.Creature.HasFlag(ObjectFlags.Looting))
            {
                session.SendPacket(new SLootItemResultPacket { SourceGUID = wo.GUID, ItemIndex = packet.ItemIndex, Result = ItemTakeResult.MissingSource });
                return;
            }
            Loot loot = wo.Loot as Loot;
            if (loot == null)
            {
                session.SendPacket(new SLootItemResultPacket { SourceGUID = wo.GUID, ItemIndex = packet.ItemIndex, Result = ItemTakeResult.MissingSource });
                return;
            }
            int index = packet.ItemIndex;
            if (loot.HadMoney)
            {
                // Looting money
                if (index == 0)
                {
                    Group group = session.Player.CurrentCharacter.Group;
                    if (group != null)
                    {
                        int membersLeft = group.MembersCount;
                        int commonShare = loot.Money / group.MembersCount;
                        foreach (Character character in group.GetMembers())
                        {
                            int share = membersLeft == 1 ? loot.Money : commonShare;
                            character.AddMoney(share);
                            loot.Money -= share;
                            --membersLeft;
                        }
                    }
                    else
                    {
                        session.Player.CurrentCharacter.AddMoney(loot.Money);
                        loot.Money = 0;
                    }
                }
                --index;
            }
            if (index >= 0)
            {
                // Looting item
                List<ItemDefinition> items = loot.GeneratedItems;
                if (index >= items.Count)
                {
                    session.SendPacket(new SLootItemResultPacket { SourceGUID = wo.GUID, ItemIndex = packet.ItemIndex, Result = ItemTakeResult.UnknownItem });
                    return;
                }
                ItemDefinition item = items[index];
                if (item == null)
                {
                    session.SendPacket(new SLootItemResultPacket { SourceGUID = wo.GUID, ItemIndex = packet.ItemIndex, Result = ItemTakeResult.AlreadyLooted });
                    return;
                }
                Character looter = session.Creature.Character;
                Inventory looterInventory = looter.Inventory;
                ItemTakeResult result = looterInventory.CanStoreItem(item.Template, item.Count);
                if (result != ItemTakeResult.Ok)
                {
                    session.SendPacket(new SLootItemResultPacket { SourceGUID = wo.GUID, ItemIndex = packet.ItemIndex, Result = result });
                    return;
                }
                items[index] = null;
                looterInventory.AddItem(item.Template, item.Count);
            }
            loot.SendPacketToLooters(new SLootItemRemovedPacket { SourceGUID = wo.GUID, ItemIndex = packet.ItemIndex });
            session.SendPacket(new SLootItemResultPacket { SourceGUID = wo.GUID, ItemIndex = packet.ItemIndex, Result = ItemTakeResult.Ok });
            if (loot.GetItemCount() == 0 && loot.Money == 0)
            {
                loot.SendPacketToLooters(new SCancelLootingPacket { GUID = wo.GUID });
                loot.CloseLootForLooters();
                wo.UnsetFlag(ObjectFlags.Lootable);
                wo.Loot = null;
            }
        }
    }
}