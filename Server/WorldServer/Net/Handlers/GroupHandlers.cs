﻿using Arcanos.Server.WorldServer.Objects;
using Arcanos.Server.WorldServer.Players;
using Arcanos.Shared.Net;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Players;

namespace Arcanos.Server.WorldServer.Net.Handlers
{
    public static class GroupHandlers
    {
        public static void HandleCGroupInvitePacket(PlayerSession session, IWorldPacket sPacket)
        {
            CGroupInvitePacket packet = (CGroupInvitePacket)sPacket;

            PlayerCreature invitee = ObjectManager.GetPlayer(packet.InviteeGUID) as PlayerCreature;
            if (invitee == null)
            {
                session.SendPacket(new SGroupInviteResultPacket { InviteeGUID = packet.InviteeGUID, Result = GroupInviteResult.PlayerNotFound });
                return;
            }
            if (invitee == session.Creature)
            {
                session.SendPacket(new SGroupInviteResultPacket { InviteeGUID = packet.InviteeGUID, Result = GroupInviteResult.CannotInviteSelf });
                return;
            }
            if (invitee.Character.Group != null)
            {
                session.SendPacket(new SGroupInviteResultPacket { InviteeGUID = packet.InviteeGUID, Result = GroupInviteResult.PlayerIsInGroup });
                return;
            }

            Group group = session.Player.CurrentCharacter.Group;
            if (group == null)
            {
                invitee.Character.PendingGroupInviteToCharacter = session.Player.CurrentCharacter.ID;
                invitee.Player.Session.SendPacket(new SGroupInvitationPacket { InviterGUID = session.Creature.GUID, InviterName = session.Creature.Name });
            }
            else
            {
                int index = group.FindAvailableIndex();
                if (index == -1)
                {
                    session.SendPacket(new SGroupInviteResultPacket { InviteeGUID = packet.InviteeGUID, Result = GroupInviteResult.NoSpaceInGroup });
                    return;
                }
                invitee.Character.PendingGroupInviteToCharacter = session.Player.CurrentCharacter.ID;
                invitee.Player.Session.SendPacket(new SGroupInvitationPacket { InviterGUID = session.Creature.GUID, InviterName = session.Creature.Name });
            }
        }
        public static void HandleCGroupKickPacket(PlayerSession session, IWorldPacket sPacket)
        {
            CGroupKickPacket packet = (CGroupKickPacket)sPacket;

            PlayerCreature target = ObjectManager.GetPlayer(packet.TargetGUID) as PlayerCreature;
            if (target == null)
            {
                session.SendPacket(new SGroupKickResultPacket { TargetGUID = packet.TargetGUID, Result = GroupKickResult.PlayerNotFound });
                return;
            }
            if (target.Character.Group != session.Player.CurrentCharacter.Group)
            {
                session.SendPacket(new SGroupKickResultPacket { TargetGUID = packet.TargetGUID, Result = GroupKickResult.PlayerNotInYourGroup });
                return;
            }
            if (session.Player.CurrentCharacter.Group != null && session.Player.CurrentCharacter.Group.Leader != session.Player.CurrentCharacter)
            {
                session.SendPacket(new SGroupKickResultPacket { TargetGUID = packet.TargetGUID, Result = GroupKickResult.NotLeader });
                return;
            }

            if (target.Character.Group.RemoveMember(target.Character))
                session.SendPacket(new SGroupKickResultPacket { TargetGUID = packet.TargetGUID, Result = GroupKickResult.Ok });
        }
        public static void HandleCGroupInvitationResponsePacket(PlayerSession session, IWorldPacket sPacket)
        {
            CGroupInvitationResponsePacket packet = (CGroupInvitationResponsePacket)sPacket;

            int pendingInvite = session.Player.CurrentCharacter.PendingGroupInviteToCharacter;
            session.Player.CurrentCharacter.PendingGroupInviteToCharacter = 0;

            if (packet.Response != GroupInvitationResponse.Accept)
                return;

            PlayerCreature inviter = ObjectManager.GetPlayer(packet.InviterGUID) as PlayerCreature;
            if (inviter == null)
                return;
            if (session.Player.CurrentCharacter.Group != null)
                return;
            if (pendingInvite != inviter.Character.ID)
                return;
            
            Group group = inviter.Character.Group;
            if (group == null)
                Group.MakeGroup(inviter.Character, session.Player.CurrentCharacter);
            else
                group.AddMember(inviter.Character);
        }
        public static void HandleCLeaveGroupPacket(PlayerSession session, IWorldPacket sPacket)
        {
            //CLeaveGroupPacket packet = (CLeaveGroupPacket)sPacket;

            if (session.Player.CurrentCharacter.Group == null)
                return;

            session.Player.CurrentCharacter.Group.RemoveMember(session.Player.CurrentCharacter);
        }
    }
}