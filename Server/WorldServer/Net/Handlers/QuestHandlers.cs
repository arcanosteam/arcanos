﻿using Arcanos.Server.WorldServer.Data;
using Arcanos.Shared.Net;

namespace Arcanos.Server.WorldServer.Net.Handlers
{
    public static class QuestHandlers
    {
        public static void HandleCAbandonQuestPacket(PlayerSession session, IWorldPacket sPacket)
        {
            CAbandonQuestPacket packet = (CAbandonQuestPacket)sPacket;

            session.Player.CurrentCharacter.QuestLog.RemoveQuest(DatabaseManager.Quests[packet.ID]);
        }
    }
}