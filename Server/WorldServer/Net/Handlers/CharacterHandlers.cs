﻿using System;
using System.Collections.Generic;
using Arcanos.Server.WorldServer.Data;
using Arcanos.Server.WorldServer.Items;
using Arcanos.Server.WorldServer.Objects;
using Arcanos.Server.WorldServer.Players;
using Arcanos.Server.WorldServer.Spells;
using Arcanos.Shared;
using Arcanos.Shared.Data;
using Arcanos.Shared.Items;
using Arcanos.Shared.Net;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Players;

namespace Arcanos.Server.WorldServer.Net.Handlers
{
    public static class CharacterHandlers
    {
        public static void HandleCCharacterQueryPacket(PlayerSession session, IWorldPacket sPacket)
        {
            CCharacterQueryPacket packet = (CCharacterQueryPacket)sPacket;
            session.SendPacket(new SCharacterResponsePacket { ID = packet.ID }, Serialization.Serialize(DatabaseManager.Characters[packet.ID], typeof(CharacterBase)));
        }

        public static void HandleCCharacterTemplateQueryPacket(PlayerSession session, IWorldPacket sPacket)
        {
            CCharacterTemplateQueryPacket packet = (CCharacterTemplateQueryPacket)sPacket;

            CreatureTemplate template;
            Character character = DatabaseManager.Characters[packet.ID];
            if (character == null || character.Creature == null)
                template = null;
            else
                template = character.Creature.Template;

            session.SendPacket(new SCharacterTemplateResponsePacket
            {
                ID = packet.ID,
                FriendlyTargetDecalTextureID = PlayerCreatureBase.FriendlyTargetDecalTextureID,
                NeutralTargetDecalTextureID = PlayerCreatureBase.NeutralTargetDecalTextureID,
                HostileTargetDecalTextureID = PlayerCreatureBase.HostileTargetDecalTextureID,
            }, Serialization.Serialize(template, typeof(CreatureTemplateBase)));
        }

        private static bool CheckCharacterCreatePacket(PlayerSession session, bool report, PlayerRace playerRace, PlayerClass playerClass, byte modelIndex)
        {
            if (!Enum.IsDefined(typeof(PlayerRace), playerRace))
            {
                if (report)
                    session.SendPacket(new SCharacterCreateResultPacket { Result = CharacterCreateResult.RaceUnavailable });
                return false;
            }
            if (!Enum.IsDefined(typeof(PlayerClass), playerClass))
            {
                if (report)
                    session.SendPacket(new SCharacterCreateResultPacket { Result = CharacterCreateResult.ClassUnavailable });
                return false;
            }

            RaceInfo race = DatabaseManager.RaceInfos[(byte)playerRace];
            if (modelIndex >= race.Models.Length)
            {
                if (report)
                    session.SendPacket(new SCharacterCreateResultPacket { Result = CharacterCreateResult.ModelIndexOutOfBounds, ModelIndexCount = (byte)race.Models.Length });
                return false;
            }

            return true;
        }

        public static void HandleCCharacterCreateTemplateQueryPacket(PlayerSession session, IWorldPacket sPacket)
        {
            CCharacterCreateTemplateQueryPacket packet = (CCharacterCreateTemplateQueryPacket)sPacket;

            CreatureTemplate template = null;
            List<ItemDefinition> items = null;
            Dictionary<EquipSlot, ItemTemplate> equipment = null;

            if (CheckCharacterCreatePacket(session, false, packet.Race, packet.Class, packet.ModelIndex))
            {
                template = PlayerCreature.MakeTemplateFor(new Character(null, null, packet.Race, packet.Class, packet.ModelIndex, true));
                items = new List<ItemDefinition>(Character.GetStartingItems(packet.Race, packet.Class));
                equipment = new Dictionary<EquipSlot, ItemTemplate>();
                foreach (KeyValuePair<EquipSlot, ItemTemplate> pair in Character.GetStartingEquipment(packet.Race, packet.Class))
                    equipment.Add(pair.Key, pair.Value);
            }

            SCharacterCreateTemplateResponsePacket response = new SCharacterCreateTemplateResponsePacket
            {
                Race = packet.Race,
                Class = packet.Class,
                ModelIndex = packet.ModelIndex,
                Spells = new int[32],
                Items = new int[16],
                ItemCounts = new int[16],
                Equipment = new int[(byte)EquipSlot.Max],
            };
            if (template != null)
            {
                int i = 0;
                foreach (SpellTemplate spell in template.Spells)
                    response.Spells[i++] = spell.ID;
                i = 0;
                foreach (ItemDefinition item in items)
                {
                    response.Items[i] = item.Template.ID;
                    response.ItemCounts[i++] = item.Count;
                }
                foreach (KeyValuePair<EquipSlot, ItemTemplate> pair in equipment)
                    response.Equipment[(byte)pair.Key] = pair.Value.ID;
            }

            session.SendPacket(response, template == null ? new byte[0] : Serialization.Serialize(template, typeof(CreatureTemplateBase)));
        }

        public static void HandleCCharacterCreatePacket(PlayerSession session, IWorldPacket sPacket)
        {
            CCharacterCreatePacket packet = (CCharacterCreatePacket)sPacket;

            if (!CheckCharacterCreatePacket(session, true, packet.Race, packet.Class, packet.ModelIndex))
                return;

            packet.Name = packet.Name.Trim();

            if (packet.Name.Length < 2 || packet.Name.Length > 32)
            {
                session.SendPacket(new SCharacterCreateResultPacket { Result = CharacterCreateResult.NameInappropriate });
                return;
            }
            if (DatabaseManager.Characters.Find(c => string.Equals(c.Name, packet.Name, StringComparison.CurrentCultureIgnoreCase)) != null)
            {
                session.SendPacket(new SCharacterCreateResultPacket { Result = CharacterCreateResult.NameUnavailable });
                return;
            }

            Character character = new Character(session.Player, packet.Name, packet.Race, packet.Class, packet.ModelIndex, false);
            DatabaseManager.Characters.Add(character);
            DatabaseManager.Characters.Save(character.ID);

            session.Player.Characters.Add(character);
            DatabaseManager.Players.Save(session.Player.ID);

            session.SendPacket(new SCharacterCreateResultPacket { Result = CharacterCreateResult.Ok });
        }
    }
}