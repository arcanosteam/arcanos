﻿using Arcanos.Server.WorldServer.Data;
using Arcanos.Server.WorldServer.Objects;
using Arcanos.Server.WorldServer.Players;
using Arcanos.Server.WorldServer.World;
using Arcanos.Shared;
using Arcanos.Shared.Data;
using Arcanos.Shared.Net;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Players;
using Arcanos.Shared.Server;
using Arcanos.Utilities;
using Microsoft.Xna.Framework;

namespace Arcanos.Server.WorldServer.Net.Handlers
{
    public static class PlayerHandlers
    {
        public static void HandleCPlayerQueryPacket(PlayerSession session, IWorldPacket sPacket)
        {
            CPlayerQueryPacket packet = (CPlayerQueryPacket)sPacket;
            session.SendPacket(new SPlayerResponsePacket { ID = packet.ID }, Serialization.Serialize(DatabaseManager.Players[packet.ID], typeof(PlayerBase)));
        }

        public static void HandleCCharacterListStatusQueryPacket(PlayerSession session, IWorldPacket sPacket)
        {
            //CCharacterListStatusQueryPacket packet = (CCharacterListStatusQueryPacket)sPacket;
            session.SendPacket(new SCharacterListStatusResponsePacket
            {
                CurrentCount = (byte)session.Player.Characters.Count,
                MaxAvailable = (byte)session.Player.Characters.Count,
                MaxCount = (byte)session.Player.Characters.Count,
            });
        }
        public static void HandleCCharacterStatusQueryPacket(PlayerSession session, IWorldPacket sPacket)
        {
            CCharacterStatusQueryPacket packet = (CCharacterStatusQueryPacket)sPacket;
            if (packet.Index >= session.Player.Characters.Count)
                return;

            Character character = session.Player.Characters[packet.Index] as Character;
            if (character == null)
                return;

            session.SendPacket(new SCharacterStatusResponsePacket { Index = packet.Index, ID = character.ID });
        }

        public static void HandleCWorldEnterRequestPacket(PlayerSession session, IWorldPacket sPacket)
        {
            CWorldEnterRequestPacket packet = (CWorldEnterRequestPacket)sPacket;
            if (packet.CharacterIndex >= session.Player.Characters.Count)
            {
                session.SendPacket(new SWorldEnterResponsePacket
                {
                    Result = WorldEnterResult.InvalidCharacter,
                });
                return;
            }

            Character character = session.Player.Characters[packet.CharacterIndex] as Character;
            if (character == null)
            {
                session.SendPacket(new SWorldEnterResponsePacket
                {
                    Result = WorldEnterResult.InvalidCharacter,
                });
                return;
            }

            // Setting the character as currently active
            session.Player.CurrentCharacter = character;
            session.Player.CurrentCharacter.ProcessCharacterSelect();

            // Finding a location to spawn the character
            Map map;
            Vector3 position = character.LocationPosition;
            Rotation rotation = character.LocationRotation;
            if (!WorldManager.PublicMaps.TryGetValue(character.LocationMapGUID, out map))
                if (!WorldManager.InstanceMaps.TryGetValue(character.LocationMapGUID, out map))
                {
                    Logger.Warning(LogCategory.Player, "Character \"{0}\" (ID:{1}) has an invalid location MapGUID:{2} Position:{3} Rotation:{4}", character.Name, character.ID, character.LocationMapGUID, character.LocationPosition, character.LocationRotation);
                    WorldManager.GetSpawnForInvalidLocation(out map, out position, out rotation);
                }
            if (map == null)
            {
                session.SendPacket(new SWorldEnterResponsePacket
                {
                    Result = WorldEnterResult.InvalidLocation,
                });
                return;
            }

            // Spawning the character in the world
            PlayerCreature pc = new PlayerCreature(0, character, position, rotation);
            session.Player.CurrentCharacter.Creature = pc;
            CharacterHandlers.HandleCCharacterTemplateQueryPacket(session, new CCharacterTemplateQueryPacket { ID = character.ID });
            session.SendPacket(new SWorldEnterResponsePacket
            {
                CharacterID = character.ID,
                MapGUID = map.GUID,
                MapTemplateID = map.Template.ID,
                MapEnvironmentID = (map.Environment as Environment).CurrentID,
                PlayerCreatureGUID = pc.GUID,
                Position = pc.Position,
                Rotation = pc.Rotation,
                Result = WorldEnterResult.Ok,
            });
            session.SendPacket(new SObjectLoadingFinalizedPacket { GUID = pc.GUID });
            session.Player.CurrentCharacter.SendInitialData();
            session.SendPacket(new SObjectLoadingFinalizedPacket { GUID = pc.GUID });
            session.Player.CurrentCharacter.JoinGlobalChatChannels();
            pc.MarkAsSpawned(false);
            map.Add(pc);
        }
        public static void HandleCForceLogoutPacket(PlayerSession session, IWorldPacket sPacket)
        {
            //CForceLogoutPacket packet = (CForceLogoutPacket)sPacket;
            session.ProcessForceLogout();
        }

        public static void HandleCTargetUpdatePacket(PlayerSession session, IWorldPacket sPacket)
        {
            CTargetUpdatePacket packet = (CTargetUpdatePacket)sPacket;

            CreatureBase target = packet.TargetGUID == 0 ? null : ObjectManager.GetCreature(packet.TargetGUID);

            if (!session.Creature.CanSee(target))
            {
                session.Creature.SendPacketToSelf(new SCreatureTargetUpdatePacket { GUID = session.Creature.GUID, TargetGUID = session.Creature.Target == null ? 0 : session.Creature.Target.GUID });
                return;
            }

            session.Creature.SetTarget(target);
        }
    }
}