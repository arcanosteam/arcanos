﻿using Arcanos.Server.WorldServer.Data;
using Arcanos.Shared;
using Arcanos.Shared.Data;
using Arcanos.Shared.Items;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Net;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Quests;
using Arcanos.Shared.Spells;
using Arcanos.Shared.World;

namespace Arcanos.Server.WorldServer.Net.Handlers
{
    public static class DataHandlers
    {
        public static void HandleCWorldObjectTemplateQueryPacket(PlayerSession session, IWorldPacket sPacket)
        {
            CWorldObjectTemplateQueryPacket packet = (CWorldObjectTemplateQueryPacket)sPacket;
            session.SendPacket(new SWorldObjectTemplateResponsePacket { ID = packet.ID }, Serialization.Serialize(DatabaseManager.WorldObjects[packet.ID], typeof(WorldObjectTemplateBase)));
        }
        public static void HandleCCreatureTemplateQueryPacket(PlayerSession session, IWorldPacket sPacket)
        {
            CCreatureTemplateQueryPacket packet = (CCreatureTemplateQueryPacket)sPacket;
            session.SendPacket(new SCreatureTemplateResponsePacket { ID = packet.ID }, Serialization.Serialize(DatabaseManager.Creatures[packet.ID], typeof(CreatureTemplateBase)));
        }
        public static void HandleCSpellTemplateQueryPacket(PlayerSession session, IWorldPacket sPacket)
        {
            CSpellTemplateQueryPacket packet = (CSpellTemplateQueryPacket)sPacket;
            session.SendPacket(new SSpellTemplateResponsePacket { ID = packet.ID }, Serialization.Serialize(DatabaseManager.Spells[packet.ID], typeof(SpellTemplateBase)));
        }
        public static void HandleCSpellVisualQueryPacket(PlayerSession session, IWorldPacket sPacket)
        {
            CSpellVisualQueryPacket packet = (CSpellVisualQueryPacket)sPacket;
            session.SendPacket(new SSpellVisualResponsePacket { ID = packet.ID }, Serialization.Serialize(DatabaseManager.SpellVisuals[packet.ID]));
        }
        public static void HandleCItemTemplateQueryPacket(PlayerSession session, IWorldPacket sPacket)
        {
            CItemTemplateQueryPacket packet = (CItemTemplateQueryPacket)sPacket;
            session.SendPacket(new SItemTemplateResponsePacket { ID = packet.ID }, Serialization.Serialize(DatabaseManager.Items[packet.ID], typeof(ItemTemplateBase)));
        }
        public static void HandleCQuestQueryPacket(PlayerSession session, IWorldPacket sPacket)
        {
            CQuestQueryPacket packet = (CQuestQueryPacket)sPacket;
            session.SendPacket(new SQuestResponsePacket { ID = packet.ID }, Serialization.Serialize(DatabaseManager.Quests[packet.ID], typeof(QuestBase)));
        }
        public static void HandleCMapTemplateQueryPacket(PlayerSession session, IWorldPacket sPacket, byte[] rawData)
        {
            CMapTemplateQueryPacket packet = (CMapTemplateQueryPacket)sPacket;
            session.SendPacket(new SMapTemplateResponsePacket { ID = packet.ID }, Serialization.Serialize(DatabaseManager.Maps[packet.ID], typeof(MapTemplateBase)));
        }
        public static void HandleCFactionQueryPacket(PlayerSession session, IWorldPacket sPacket)
        {
            CFactionQueryPacket packet = (CFactionQueryPacket)sPacket;
            session.SendPacket(new SFactionResponsePacket { ID = packet.ID }, Serialization.Serialize(DatabaseManager.Factions[packet.ID], typeof(FactionBase)));
        }
        public static void HandleCEnvironmentQueryPacket(PlayerSession session, IWorldPacket sPacket)
        {
            CEnvironmentQueryPacket packet = (CEnvironmentQueryPacket)sPacket;
            session.SendPacket(new SEnvironmentResponsePacket { ID = packet.ID }, Serialization.Serialize(DatabaseManager.Environments[packet.ID], typeof(EnvironmentBase)));
        }
        public static void HandleCTextQueryPacket(PlayerSession session, IWorldPacket sPacket)
        {
            CTextQueryPacket packet = (CTextQueryPacket)sPacket;
            session.SendPacket(new STextResponsePacket { ID = packet.ID }, Serialization.Serialize(DatabaseManager.Text[packet.ID].LocalizedStrings[(byte)session.Locale]));
        }
        public static void HandleCGameStringsQueryPacket(PlayerSession session, IWorldPacket sPacket)
        {
            //CGameStringsQueryPacket packet = (CGameStringsQueryPacket)sPacket;
            session.SendPacket(new SGameStringsResponsePacket(), Serialization.Serialize(Localization.Game));
        }
        public static void HandleCMaterialQueryPacket(PlayerSession session, IWorldPacket sPacket)
        {
            CMaterialQueryPacket packet = (CMaterialQueryPacket)sPacket;
            session.SendPacket(new SMaterialResponsePacket { ID = packet.ID }, Serialization.Serialize(DatabaseManager.Materials[packet.ID]));
        }
        public static void HandleCTextureInfoQueryPacket(PlayerSession session, IWorldPacket sPacket)
        {
            CTextureInfoQueryPacket packet = (CTextureInfoQueryPacket)sPacket;
            session.SendPacket(new STextureInfoResponsePacket { ID = packet.ID }, Serialization.Serialize(DatabaseManager.Textures[packet.ID]));
        }
        public static void HandleCModelQueryPacket(PlayerSession session, IWorldPacket sPacket)
        {
            CModelQueryPacket packet = (CModelQueryPacket)sPacket;
            session.SendPacket(new SModelResponsePacket { ID = packet.ID }, Serialization.Serialize(DatabaseManager.Models[packet.ID]));
        }
        public static void HandleCParticleSystemQueryPacket(PlayerSession session, IWorldPacket sPacket)
        {
            CParticleSystemQueryPacket packet = (CParticleSystemQueryPacket)sPacket;
            session.SendPacket(new SParticleSystemResponsePacket { ID = packet.ID }, Serialization.Serialize(DatabaseManager.ParticleSystems[packet.ID]));
        }
    }
}