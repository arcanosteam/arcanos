﻿using Arcanos.Server.WorldServer.Objects;
using Arcanos.Shared.Movement;
using Arcanos.Shared.Net;
using Arcanos.Shared.Objects;
using Microsoft.Xna.Framework;

namespace Arcanos.Server.WorldServer.Net.Handlers
{
    public static class MovementHandlers
    {
        public static void HandleCMovementUpdatePacket(PlayerSession session, IWorldPacket sPacket)
        {
            CMovementUpdatePacket packet = (CMovementUpdatePacket)sPacket;
            CreatureBase mover = ObjectManager.GetCreature(packet.GUID);
            if (mover == null)
                return;
            if (mover.Type == ObjectType.PlayerCreature)
            {
                PlayerCreature pc = mover as PlayerCreature;
                Vector3 source = pc.Position;
                Vector3 destination = packet.Data.Position;
                if (pc.IsMovementValid(source, destination, false) == MovementValidity.Ok)
                {
                    pc.MoveTo(packet.Data.Position);
                }
                else
                {
                    packet.Data.Position = pc.Position;
                    pc.SendPacketToSelf(packet);
                }
                pc.RotateTo(packet.Data.Rotation);
                pc.SendPacketToVisibleExceptSelf(packet);
            }
            else
            {
                mover.MoveTo(packet.Data.Position);
                mover.RotateTo(packet.Data.Rotation);
            }
        }
    }
}