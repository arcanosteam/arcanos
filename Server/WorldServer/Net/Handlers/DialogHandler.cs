﻿using Arcanos.Shared;
using Arcanos.Shared.Dialog;
using Arcanos.Shared.Net;
using Arcanos.Shared.Objects;
using Arcanos.Shared.World;

namespace Arcanos.Server.WorldServer.Net.Handlers
{
    public static class DialogHandler
    {
        public static void HandleCStartDialogPacket(PlayerSession session, IWorldPacket sPacket)
        {
            CStartDialogPacket packet = (CStartDialogPacket)sPacket;

            WorldObjectBase talker = ObjectManager.GetWorldObject(packet.TalkerGUID);
            if (talker == null)
            {
                session.SendPacket(new SStartDialogResultPacket { TalkerGUID = packet.TalkerGUID, Result = DialogStartResult.TalkerNotFound });
                return;
            }

            if (talker.Type.IsCreature() && !(talker as CreatureBase).IsAlive)
            {
                session.SendPacket(new SStartDialogResultPacket { TalkerGUID = packet.TalkerGUID, Result = DialogStartResult.TalkerIncapable });
                return;
            }
            if (session.Creature.DistanceTo2D(talker) > MapTemplateBase.INTERACT_DISTANCE)
            {
                session.SendPacket(new SStartDialogResultPacket { TalkerGUID = packet.TalkerGUID, Result = DialogStartResult.TooFar });
                return;
            }

            session.Creature.StartDialog(talker);
        }
        public static void HandleCEndDialogPacket(PlayerSession session, IWorldPacket sPacket)
        {
            CEndDialogPacket packet = (CEndDialogPacket)sPacket;

            if (!session.Creature.IsTalkingTo(packet.TalkerGUID))
                return;

            session.Creature.EndDialog(DialogEndReason.UserRequest);
        }
        public static void HandleCSelectOptionPacket(PlayerSession session, IWorldPacket sPacket)
        {
            CSelectOptionPacket packet = (CSelectOptionPacket)sPacket;

            session.Creature.SelectOption(packet.IsQuestOption, packet.Index);
        }
    }
}