﻿using Arcanos.Server.WorldServer.Objects;
using Arcanos.Shared.Net;
using Arcanos.Shared.Objects;
using Arcanos.Shared.PvP;
using Arcanos.Shared.Spells;

namespace Arcanos.Server.WorldServer.Net.Handlers
{
    public static class CombatHandlers
    {
        public static void HandleCCombatStartMeleeAttackPacket(PlayerSession session, IWorldPacket sPacket)
        {
            CCombatStartMeleeAttackPacket packet = (CCombatStartMeleeAttackPacket)sPacket;
            
            CreatureBase target = ObjectManager.GetCreature(packet.TargetGUID);
            if (target == null)
                return;

            session.Creature.StartAttacking(target);
        }
        public static void HandleCCombatStopMeleeAttackPacket(PlayerSession session, IWorldPacket sPacket)
        {
            //CCombatStopMeleeAttackPacket packet = (CCombatStopMeleeAttackPacket)sPacket;
            session.Creature.StopAttacking();
        }
        public static void HandleCCombatCastSpellPacket(PlayerSession session, IWorldPacket sPacket)
        {
            CCombatCastSpellPacket packet = (CCombatCastSpellPacket)sPacket;
            
            CreatureBase target = ObjectManager.GetCreature(packet.TargetGUID);
            if (target == null)
                return;

            session.Creature.CastSpell(target, packet.SpellID, CastSpellFlags.None);
        }
        public static void HandleCCombatCastGroundSpellPacket(PlayerSession session, IWorldPacket sPacket)
        {
            CCombatCastGroundSpellPacket packet = (CCombatCastGroundSpellPacket)sPacket;

            session.Creature.CastSpell(packet.Target, packet.SpellID, CastSpellFlags.None);
        }
        public static void HandleCCombatInterruptSpellCastingPacket(PlayerSession session, IWorldPacket sPacket)
        {
            //CCombatInterruptSpellCastingPacket packet = (CCombatInterruptSpellCastingPacket)sPacket;
            session.Creature.SpellBook.InterruptCasting(InterruptMask.Manual);
        }
        public static void HandleCDuelRequestPacket(PlayerSession session, IWorldPacket sPacket)
        {
            CDuelRequestPacket packet = (CDuelRequestPacket)sPacket;

            PlayerCreature opponent = ObjectManager.GetPlayer(packet.OpponentGUID) as PlayerCreature;
            if (opponent == null)
                return;

            if (session.Creature.Duel != null || opponent.Duel != null)
                return;

            opponent.DuelRequestSender = session.Creature;
            opponent.SendPacketToSelf(new SDuelRequestPacket { ChallengerGUID = session.Creature.GUID });
        }
        public static void HandleCDuelRequestResponsePacket(PlayerSession session, IWorldPacket sPacket)
        {
            CDuelRequestResponsePacket packet = (CDuelRequestResponsePacket)sPacket;

            PlayerCreature challenger = ObjectManager.GetPlayer(packet.ChallengerGUID) as PlayerCreature;
            if (challenger == null)
                return;

            if (session.Creature.DuelRequestSender != challenger)
                return;

            session.Creature.DuelRequestSender = null;

            if (packet.Response == DuelResponse.Decline)
                return;

            challenger.StartDuel(session.Creature);
            session.Creature.StartDuel(challenger);
        }
    }
}