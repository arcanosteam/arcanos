﻿using System.Collections.Generic;
using Arcanos.Server.WorldServer.Net.Handlers;
using Arcanos.Shared.Net;

namespace Arcanos.Server.WorldServer.Net
{
    public delegate void PacketHandler(PlayerSession session, IWorldPacket packet);
    public delegate void PacketHandlerWithRawData(PlayerSession session, IWorldPacket packet, byte[] rawData);
    public static class PacketHandling
    {
        private struct HandlerDef
        {
            public readonly WorldPackets Type;
            public readonly PacketHandler Handler;
            public readonly PacketHandlerWithRawData HandlerWithRawData;

            public HandlerDef(WorldPackets type, PacketHandler handler)
            {
                Type = type;
                Handler = handler;
                HandlerWithRawData = null;
            }
            public HandlerDef(WorldPackets type, PacketHandlerWithRawData handler)
            {
                Type = type;
                Handler = null;
                HandlerWithRawData = handler;
            }
        }

        private static readonly List<HandlerDef> handlers = new List<HandlerDef>
        {
            new HandlerDef(WorldPackets.CWorldLoginRequest,             SessionManager.HandleCLoginRequestPacket),

            new HandlerDef(WorldPackets.CCharacterQuery,                CharacterHandlers.HandleCCharacterQueryPacket),
            new HandlerDef(WorldPackets.CCharacterTemplateQuery,        CharacterHandlers.HandleCCharacterTemplateQueryPacket),
            new HandlerDef(WorldPackets.CCharacterCreateTemplateQuery,  CharacterHandlers.HandleCCharacterCreateTemplateQueryPacket),
            new HandlerDef(WorldPackets.CCharacterCreate,               CharacterHandlers.HandleCCharacterCreatePacket),

            new HandlerDef(WorldPackets.CChatMessage,                   ChatHandlers.HandleCChatMessagePacket),
            new HandlerDef(WorldPackets.CChatWhisper,                   ChatHandlers.HandleCChatWhisperPacket),
            new HandlerDef(WorldPackets.CChatChannelJoin,               ChatHandlers.HandleCChatChannelJoinPacket),
            new HandlerDef(WorldPackets.CChatChannelLeave,              ChatHandlers.HandleCChatChannelLeavePacket),

            new HandlerDef(WorldPackets.CCombatStartMeleeAttack,        CombatHandlers.HandleCCombatStartMeleeAttackPacket),
            new HandlerDef(WorldPackets.CCombatStopMeleeAttack,         CombatHandlers.HandleCCombatStopMeleeAttackPacket),
            new HandlerDef(WorldPackets.CCombatCastSpell,               CombatHandlers.HandleCCombatCastSpellPacket),
            new HandlerDef(WorldPackets.CCombatCastGroundSpell,         CombatHandlers.HandleCCombatCastGroundSpellPacket),
            new HandlerDef(WorldPackets.CCombatInterruptSpellCasting,   CombatHandlers.HandleCCombatInterruptSpellCastingPacket),
            new HandlerDef(WorldPackets.CDuelRequest,                   CombatHandlers.HandleCDuelRequestPacket),
            new HandlerDef(WorldPackets.CDuelRequestResponse,           CombatHandlers.HandleCDuelRequestResponsePacket),
            
            new HandlerDef(WorldPackets.CWorldObjectTemplateQuery,      DataHandlers.HandleCWorldObjectTemplateQueryPacket),
            new HandlerDef(WorldPackets.CCreatureTemplateQuery,         DataHandlers.HandleCCreatureTemplateQueryPacket),
            new HandlerDef(WorldPackets.CSpellTemplateQuery,            DataHandlers.HandleCSpellTemplateQueryPacket),
            new HandlerDef(WorldPackets.CSpellVisualQuery,              DataHandlers.HandleCSpellVisualQueryPacket),
            new HandlerDef(WorldPackets.CItemTemplateQuery,             DataHandlers.HandleCItemTemplateQueryPacket),
            new HandlerDef(WorldPackets.CQuestQuery,                    DataHandlers.HandleCQuestQueryPacket),
            new HandlerDef(WorldPackets.CMapTemplateQuery,              DataHandlers.HandleCMapTemplateQueryPacket),
            new HandlerDef(WorldPackets.CFactionQuery,                  DataHandlers.HandleCFactionQueryPacket),
            new HandlerDef(WorldPackets.CEnvironmentQuery,              DataHandlers.HandleCEnvironmentQueryPacket),
            new HandlerDef(WorldPackets.CTextQuery,                     DataHandlers.HandleCTextQueryPacket),
            new HandlerDef(WorldPackets.CGameStringsQuery,              DataHandlers.HandleCGameStringsQueryPacket),
            new HandlerDef(WorldPackets.CMaterialQuery,                 DataHandlers.HandleCMaterialQueryPacket),
            new HandlerDef(WorldPackets.CTextureInfoQuery,              DataHandlers.HandleCTextureInfoQueryPacket),
            new HandlerDef(WorldPackets.CModelQuery,                    DataHandlers.HandleCModelQueryPacket),
            new HandlerDef(WorldPackets.CParticleSystemQuery,           DataHandlers.HandleCParticleSystemQueryPacket),

            new HandlerDef(WorldPackets.CStartDialog,                   DialogHandler.HandleCStartDialogPacket),
            new HandlerDef(WorldPackets.CEndDialog,                     DialogHandler.HandleCEndDialogPacket),
            new HandlerDef(WorldPackets.CSelectOption,                  DialogHandler.HandleCSelectOptionPacket),

            new HandlerDef(WorldPackets.CGroupInvite,                   GroupHandlers.HandleCGroupInvitePacket),
            new HandlerDef(WorldPackets.CGroupKick,                     GroupHandlers.HandleCGroupKickPacket),
            new HandlerDef(WorldPackets.CGroupInvitationResponse,       GroupHandlers.HandleCGroupInvitationResponsePacket),
            new HandlerDef(WorldPackets.CLeaveGroup,                    GroupHandlers.HandleCLeaveGroupPacket),

            new HandlerDef(WorldPackets.CItemDataQuery,                 ItemHandlers.HandleCItemDataQueryPacket),
            new HandlerDef(WorldPackets.CMoveItem,                      ItemHandlers.HandleCMoveItemPacket),
            new HandlerDef(WorldPackets.CDivideItem,                    ItemHandlers.HandleCDivideItemPacket),
            new HandlerDef(WorldPackets.CDestroyItem,                   ItemHandlers.HandleCDestroyItemPacket),
            new HandlerDef(WorldPackets.CUseItem,                       ItemHandlers.HandleCUseItemPacket),
            new HandlerDef(WorldPackets.CEquipItem,                     ItemHandlers.HandleCEquipItemPacket),
            new HandlerDef(WorldPackets.CUnequipItem,                   ItemHandlers.HandleCUnequipItemPacket),
            new HandlerDef(WorldPackets.CLootCreature,                  ItemHandlers.HandleCLootCreaturePacket),
            new HandlerDef(WorldPackets.CCancelLooting,                 ItemHandlers.HandleCCancelLootingPacket),
            new HandlerDef(WorldPackets.CLootItem,                      ItemHandlers.HandleCLootItemPacket),

            new HandlerDef(WorldPackets.CTeleportAck,                   MapHandlers.HandleCTeleportAckPacket),

            new HandlerDef(WorldPackets.CMovementUpdate,                MovementHandlers.HandleCMovementUpdatePacket),

            new HandlerDef(WorldPackets.CPlayerQuery,                   PlayerHandlers.HandleCPlayerQueryPacket),
            new HandlerDef(WorldPackets.CCharacterListStatusQuery,      PlayerHandlers.HandleCCharacterListStatusQueryPacket),
            new HandlerDef(WorldPackets.CCharacterStatusQuery,          PlayerHandlers.HandleCCharacterStatusQueryPacket),
            new HandlerDef(WorldPackets.CWorldEnterRequest,             PlayerHandlers.HandleCWorldEnterRequestPacket),
            new HandlerDef(WorldPackets.CForceLogout,                   PlayerHandlers.HandleCForceLogoutPacket),
            new HandlerDef(WorldPackets.CTargetUpdate,                  PlayerHandlers.HandleCTargetUpdatePacket),

            new HandlerDef(WorldPackets.CWorldProtocolQuery,            ProtocolHandlers.HandleCProtocolQueryPacket),
            new HandlerDef(WorldPackets.CWorldServerVersionQuery,       ProtocolHandlers.HandleCServerVersionQueryPacket),
            new HandlerDef(WorldPackets.CWorldGameVersionQuery,         ProtocolHandlers.HandleCGameVersionQueryPacket),

            new HandlerDef(WorldPackets.CAbandonQuest,                  QuestHandlers.HandleCAbandonQuestPacket),
        };
        private static readonly int handlersCount = handlers.Count;

        public static bool Handle(PlayerSession session, WorldPackets type, IWorldPacket packet, byte[] rawData)
        {
            for (int i = 0; i < handlersCount; i++)
                if (handlers[i].Type == type)
                {
                    HandlerDef handler = handlers[i];
                    if (handler.Handler != null)
                        handler.Handler(session, packet);
                    else if (handler.HandlerWithRawData != null)
                        handler.HandlerWithRawData(session, packet, rawData);
                    return true;
                }
            return false;
        }
    }
}