﻿using System;
using System.Net.Sockets;
using System.Threading;
using Arcanos.Net;
using Arcanos.Server.WorldServer.Objects;
using Arcanos.Server.WorldServer.Players;
using Arcanos.Server.WorldServer.World;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Net;
using Arcanos.Utilities;

namespace Arcanos.Server.WorldServer.Net
{
    public class PlayerSession : PlayerSessionBase
    {
        public new Player Player
        {
            get { return (Player)base.Player; }
            set { base.Player = value; }
        }
        public PlayerCreature Creature
        {
            get { return Player.CurrentCharacter.Creature; }
        }

        public ConnectionManager<WorldPackets, IWorldPacket> Connection;
        public bool Connected { get; protected set; }
        public LocaleIndex Locale;

        public void SetPlayer(Player player)
        {
            Player = player;
            player.Session = this;
        }
        public void ProcessForceLogout()
        {
            if (Player != null && Player.CurrentCharacter != null)
                Player.CurrentCharacter.ProcessLogout();
        }

        public bool StartConnect(TcpClient client)
        {
            if (!client.Connected)
                return false;
            Connection = new ConnectionManager<WorldPackets, IWorldPacket>(client);
            WorldPacketsTable.Fill(Connection.Reader.PacketTypes);
            WorldPacketsTable.Fill(Connection.Writer.PacketTypes);
            Connection.Reader.ImmediateProcessing = false;
            Connection.ConnectionClosed += ProcessForceLogout;
            return true;
        }
        public bool EndConnect()
        {
            if (Connected)
                return false;
            Connection.PacketReceived += ProcessPacket;
            Connection.Start();
            Connected = true;
            return true;
        }
        public override void ProcessPackets()
        {
            Connection.Reader.ProcessQueue();
        }

        public override void ProcessPacket(WorldPackets type, IWorldPacket packet, byte[] rawData)
        {
            if (!PacketHandling.Handle(this, type, packet, rawData))
                Logger.Error(LogCategory.Packets, "Received packet of type {0} was unhandled", type);
        }

        public void SendPacket(IWorldPacket packet)
        {
            Connection.Write(packet);
            if (WorldManager.PacketDelay != 0)
                Thread.Sleep(WorldManager.PacketDelay);
        }
        public void SendPacket(IWorldPacket packet, byte[] rawData)
        {
            Connection.Write(packet, rawData);
            if (WorldManager.PacketDelay != 0)
                Thread.Sleep(WorldManager.PacketDelay);
        }
        public void SendSystemMessage(string format, params object[] args)
        {
            SendPacket(new SWorldSystemMessagePacket { Time = DateTime.Now, Text = string.Format(format, args) });
        }
    }
}