﻿using System;
using System.Collections.Generic;
using Arcanos.Shared.Objects;

namespace Arcanos.Server.WorldServer.Objects
{
    public struct ThreatEntry : IComparable<ThreatEntry>, IEquatable<ThreatEntry>
    {
        public readonly CreatureBase Attacker;
        public readonly float Threat;

        public ThreatEntry(CreatureBase attacker, float threat)
        {
            Attacker = attacker;
            Threat = threat;
        }

        public int CompareTo(ThreatEntry other)
        {
            return Threat.CompareTo(other.Threat);
        }
        public bool Equals(ThreatEntry other)
        {
            return Attacker.Equals(other.Attacker);
        }
    }
    public class ThreatManager
    {
        private readonly SortedList<ThreatEntry> attackers = new SortedList<ThreatEntry>();
        public bool Empty
        {
            get { return attackers.Count == 0; }
        }

        public void AddThreat(CreatureBase attacker, float threat)
        {
            for (int i = 0; i < attackers.Count; ++i)
            {
                ThreatEntry entry = attackers[i];
                if (entry.Attacker == attacker)
                {
                    threat = entry.Threat + threat;
                    attackers.RemoveAt(i);
                    break;
                }
            }
            attackers.Add(new ThreatEntry(attacker, threat));
        }
        public void AddThreatPercent(CreatureBase attacker, float percent)
        {
            float threat = 0;
            for (int i = 0; i < attackers.Count; ++i)
            {
                ThreatEntry entry = attackers[i];
                if (entry.Attacker == attacker)
                {
                    threat = entry.Threat + entry.Threat * percent;
                    attackers.RemoveAt(i);
                    break;
                }
            }
            attackers.Add(new ThreatEntry(attacker, threat));
        }
        public void SubtractThreat(CreatureBase attacker, float threat)
        {
            for (int i = 0; i < attackers.Count; ++i)
            {
                ThreatEntry entry = attackers[i];
                if (entry.Attacker == attacker)
                {
                    threat = entry.Threat - threat;
                    attackers.RemoveAt(i);
                    break;
                }
            }
            attackers.Add(new ThreatEntry(attacker, threat));
        }
        public void SubtractThreatPercent(CreatureBase attacker, float percent)
        {
            float threat = 0;
            for (int i = 0; i < attackers.Count; ++i)
            {
                ThreatEntry entry = attackers[i];
                if (entry.Attacker == attacker)
                {
                    threat = entry.Threat - entry.Threat * percent;
                    attackers.RemoveAt(i);
                    break;
                }
            }
            attackers.Add(new ThreatEntry(attacker, threat));
        }
        public void SetThreat(CreatureBase attacker, float threat)
        {
            for (int i = 0; i < attackers.Count; ++i)
            {
                ThreatEntry entry = attackers[i];
                if (entry.Attacker == attacker)
                {
                    attackers.RemoveAt(i);
                    break;
                }
            }
            attackers.Add(new ThreatEntry(attacker, threat));
        }
        public void SetThreatPercent(CreatureBase attacker, float percent)
        {
            float threat = 0;
            for (int i = 0; i < attackers.Count; ++i)
            {
                ThreatEntry entry = attackers[i];
                if (entry.Attacker == attacker)
                {
                    threat = entry.Threat * percent;
                    attackers.RemoveAt(i);
                    break;
                }
            }
            attackers.Add(new ThreatEntry(attacker, threat));
        }
        public void ZeroThreat(CreatureBase attacker)
        {
            SetThreat(attacker, 0);
        }
        public void Remove(CreatureBase attacker)
        {
            for (int i = 0; i < attackers.Count; ++i)
            {
                ThreatEntry entry = attackers[i];
                if (entry.Attacker == attacker)
                {
                    attackers.RemoveAt(i);
                    return;
                }
            }
        }
        public void Clear()
        {
            attackers.Clear();
        }

        public CreatureBase GetTopThreat()
        {
            if (attackers.Count == 0)
                return null;

            return attackers[0].Attacker;
        }
        public CreatureBase GetTopThreat(out float threat)
        {
            if (attackers.Count == 0)
            {
                threat = 0;
                return null;
            }

            threat = attackers[0].Threat;
            return attackers[0].Attacker;
        }
        public float GetThreat(CreatureBase attacker)
        {
            for (int i = 0; i < attackers.Count; ++i)
                if (attackers[i].Attacker == attacker)
                    return attackers[i].Threat;

            return 0;
        }

        public IEnumerable<ThreatEntry> GetAttackers()
        {
            return attackers;
        }
        public List<ThreatEntry> GetAttackers(ObjectType type)
        {
            List<ThreatEntry> filteredAttackers = new List<ThreatEntry>(attackers.Count);
            for (int i = 0; i < attackers.Count; ++i)
                if ((attackers[i].Attacker.Type & type) != 0)
                    filteredAttackers.Add(attackers[i]);
            return filteredAttackers;
        }
    }
}