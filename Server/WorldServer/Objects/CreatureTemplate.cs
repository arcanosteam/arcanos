﻿using System.Collections.Generic;
using Arcanos.Server.WorldServer.Data;
using Arcanos.Server.WorldServer.Dialog;
using Arcanos.Server.WorldServer.Items;
using Arcanos.Server.WorldServer.Quests;
using Arcanos.Server.WorldServer.Scripting;
using Arcanos.Server.WorldServer.Spells;
using Arcanos.Shared;
using Arcanos.Shared.Data;
using Arcanos.Shared.Objects;

namespace Arcanos.Server.WorldServer.Objects
{
    public enum CreatureCombatType
    {
        Melee,
        MeleeEnergy,
        CasterMana,
    }
    public class CreatureTemplate : CreatureTemplateBase, IPostDeserialization
    {
        public CreatureCombatType CombatType;
        public DamageBounds[] AttackPower = new DamageBounds[(byte)AttackType.Max];
        public FloatBounds[] AttackInterval = new FloatBounds[(byte)AttackType.Max];
        public float MeleeReach;

        public SpellTemplate[] Spells;
        public SpellTemplate[] Auras;

        public DialogStart Dialog;
        public IntBounds Money;
        public LootTemplate Loot;
        public List<Quest> Quests = new List<Quest>();

        public CreatureScriptLoader Script;

        public CreatureTemplate()
        {
            Init();
        }

        private void Init()
        {
            AttackPower[(byte)AttackType.MeleeMainHand] = new DamageBounds { Physical = Formulas.GetDefaultDamageForLevel(Level) };
            AttackInterval[(byte)AttackType.MeleeMainHand] = new FloatBounds(2.5f);
            MeleeReach = 1.0f;

            MaxHealth = Formulas.GetDefaultHealthForLevel(Level);
            Formulas.GetDefaultPowerForLevel(Level, CombatType, ref MaxPower, ref UsesPowerType);

            MovementSpeed[(byte)MovementType.Walk] = 0.75f;
            MovementSpeed[(byte)MovementType.Run] = 2.0f;
        }

        protected override int GetInitialID()
        {
            return DatabaseManager.Creatures.GetID(this);
        }

        public void MemberDeserialized(string member, object value) { }
        public void Deserialized(object parent)
        {
            Init();
        }
    }
}