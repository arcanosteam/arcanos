﻿using System;
using System.Collections.Generic;
using Arcanos.Server.WorldServer.Data;
using Arcanos.Server.WorldServer.Dialog;
using Arcanos.Server.WorldServer.Items;
using Arcanos.Server.WorldServer.Movement;
using Arcanos.Server.WorldServer.Quests;
using Arcanos.Server.WorldServer.Scripting;
using Arcanos.Server.WorldServer.Spells;
using Arcanos.Shared;
using Arcanos.Shared.Dialog;
using Arcanos.Shared.Items;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Net;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Spells;
using Arcanos.Shared.Timing;
using Microsoft.Xna.Framework;

namespace Arcanos.Server.WorldServer.Objects
{
    public class Creature : CreatureBase, IVisibleToPlayers, IDialogTalker
    {
        private const int TARGET_SEARCH_INTERVAL = 100;

        public new CreatureTemplate Template { get { return (CreatureTemplate)template; } }

        public override string Name
        {
            get { return template.Name == null ? Text.MissingString : template.Name.ToString(); }
        }
        public override Text NameRef
        {
            get { return null; }
        }

        public readonly DamageBounds[] AttackPower = new DamageBounds[(byte)AttackType.Max];
        public readonly FloatBounds[] AttackInterval = new FloatBounds[(byte)AttackType.Max];
        private readonly float[] attackIntervalLeft = new float[(byte)AttackType.Max];
        private readonly float[] regenIntervalLeft = new float[(byte)PowerType.Max + 1];
        public float MeleeReach;

        public ThreatManager ThreatManager;
        public Vector3 AggroPosition;
        public float AggroRadius;
        public float ChaseDistance;

        public Looter LootRecipient;

        public readonly CreatureScript Script;

        public readonly List<PlayerCreatureBase> VisibleTo = new List<PlayerCreatureBase>();
        private readonly List<PlayerCreatureBase> talksTo = new List<PlayerCreatureBase>();

        public Creature(ulong guid, CreatureTemplate template, Vector3 pos, Rotation rot) : base(guid, template, pos, rot)
        {
            Mover = new Mover(this);

            if (Template.Script != null)
                Script = Template.Script.Make(this);

            ResetFromTemplate();

            AggroPosition = Position;

            if (Script != null)
                Script.OnSpawn();
        }

        protected override void ResetFromTemplate()
        {
            base.ResetFromTemplate();

            for (byte type = 0; type < (byte)AttackType.Max; type++)
            {
                AttackPower[type] = Template.AttackPower[type];
                AttackInterval[type] = Template.AttackInterval[type];
                attackIntervalLeft[type] = 0;
            }
            for (byte type = 0; type < (byte)PowerType.Max + 1; type++)
                regenIntervalLeft[type] = 0;
            MeleeReach = Template.MeleeReach;

            ThreatManager = new ThreatManager();
            AggroRadius = 10;
            ChaseDistance = 25;

            SpellBook = new SpellBook(this);
            if (Template.Spells != null)
                foreach (SpellTemplate spell in Template.Spells)
                    SpellBook.AddSpell(spell);
            if (Template.Auras != null)
                foreach (SpellTemplate spell in Template.Auras)
                    for (int i = 0; i < spell.Effects.Length; ++i)
                        if (spell.Effects[i].IsAuraEffect)
                            SpellBook.ApplyAura(GUID, spell, i);

            if (Template.Dialog != null)
                SetFlag(ObjectFlags.HasDialog, true);

            LootRecipient = new Looter();

            if (Script != null)
                Script.OnReset();
        }

        // Updates
        public override bool Update()
        {
            if (!base.Update())
                return false;

            UpdateState();
            UpdateAttackCooldowns();
            UpdateRegen();
            UpdateOrientation();
            SpellBook.Update();
            if (Script != null)
                Script.OnUpdate();

            return true;
        }

        private void UpdateState()
        {
            switch (State)
            {
                case CreatureState.JustSpawned:
                    SetState(CreatureState.Alive);
                    break;
                case CreatureState.Alive:
                    break;
                case CreatureState.JustDied:
                    SetState(CreatureState.Corpse);
                    if (Template.Loot != null)
                    {
                        Loot = new Loot(Template.Loot, Template.Money.Random, this, LootRecipient);
                        SetFlag(ObjectFlags.Lootable);
                    }
                    break;
                case CreatureState.Corpse:
                case CreatureState.Despawned:
                    break;
            }
        }
        private void UpdateAttackCooldowns()
        {
            for (AttackType type = 0; type < AttackType.Max; ++type)
                if (attackIntervalLeft[(byte)type] > 0)
                    attackIntervalLeft[(byte)type] -= Time.PerSecond;
        }
        private void UpdateRegen()
        {
            if (!IsAlive)
                return;
            for (byte type = 0; type < (byte)PowerType.Max + 1; ++type)
            {
                if (regenIntervalLeft[type] > 0)
                    regenIntervalLeft[type] -= Time.PerSecond;
                while (regenIntervalLeft[type] <= 0)
                {
                    float interval;
                    if (type == 0)
                    {
                        int regen = Formulas.CalcHealthRegen(this, IsInCombat);
                        if (regen != 0 && CurrentHealth < MaxHealth)
                            ModityHealth(regen);
                        interval = Formulas.CalcHealthRegenInterval(this, IsInCombat);
                    }
                    else
                    {
                        int regen = Formulas.CalcPowerRegen((PowerType)(type - 1), this, IsInCombat);
                        if (regen != 0 && CurrentPower[type - 1] < MaxPower[type - 1])
                            ModityPower((PowerType)(type - 1), regen);
                        interval = Formulas.CalcPowerRegenInterval((PowerType)(type - 1), this, IsInCombat);
                    }
                    if (interval == 0)
                        break;
                    regenIntervalLeft[type] += interval;
                }
            }
        }
        private void UpdateOrientation()
        {
            if (Target != null)
                RotateTo(Position.GetRotationTo(Target.Position));
        }
        private long nextTargetSearchTimestamp;
        public bool UpdateTarget()
        {
            if (!IsAlive)
                return false;

            if (Target != null && !Target.IsAlive)
                SetTarget(null);

            CreatureBase topThreat;
            while ((topThreat = ThreatManager.GetTopThreat()) != null)
            {
                if (topThreat == Target)
                    return true;
                if (topThreat.IsAlive && topThreat.Map == Map)
                {
                    EnterCombatWith(topThreat);
                    return true;
                }
                ThreatManager.Remove(topThreat);
            }

            if (Time.Timestamp >= nextTargetSearchTimestamp)
            {
                nextTargetSearchTimestamp = Time.Timestamp + TARGET_SEARCH_INTERVAL;
                CustomSortedList<WorldObjectBase> nearbyHostiles = null;
                foreach (WorldObjectBase wo in Map.GetNearbyObjects(Position, AggroRadius))
                {
                    if (wo == this)
                        continue;
                    if (!wo.Type.IsCreature())
                        continue;

                    CreatureBase creature = wo as CreatureBase;
                    if (!creature.IsAlive)
                        continue;
                    if (GetReactionTo(creature) != Reaction.Hostile)
                        continue;
                    if (!CanSee(creature))
                        continue;

                    if (nearbyHostiles == null)
                        nearbyHostiles = new CustomSortedList<WorldObjectBase>(new ObjectDistanceComparer(Position));

                    nearbyHostiles.Add(creature);
                }

                if (nearbyHostiles != null && nearbyHostiles.Count != 0)
                {
                    EnterCombatWith(nearbyHostiles[0] as CreatureBase);
                    return true;
                }
            }

            return false;
        }

        // Dialog related
        public DialogStart GetDialogStart(PlayerCreature player, out DialogPage startingPage)
        {
            DialogStart dialog = Script == null ? Template.Dialog : Script.GetDialogStart(player);
            startingPage = dialog == null ? null : dialog.GetStartingPageFor(player);
            return dialog;
        }
        public bool CanEndDialog(PlayerCreature player, DialogPage page)
        {
            return Script == null || Script.CanEndDialog(player, page);
        }
        public void StartedDialog(PlayerCreature player, DialogStart dialog, DialogPage page)
        {
            talksTo.Add(player);
            if (Script != null)
                Script.OnDialogStart(player, dialog, page);
        }
        public void EndedDialog(PlayerCreature player, DialogPage page, DialogEndReason reason)
        {
            talksTo.Remove(player);
            if (Script != null)
                Script.OnDialogEnd(player, page, reason);
        }
        public bool SelectOption(PlayerCreature player, DialogPage page, DialogOption option, int index)
        {
            return Script != null && Script.OnDialogOption(player, page, option, index);
        }
        public void ChangedPage(PlayerCreature player, DialogPage currentPage, ref DialogPage nextPage)
        {
            if (Script != null)
                Script.OnDialogPage(player, currentPage, ref nextPage);
        }
        public IList<Quest> GetQuestsFor(PlayerCreature player)
        {
            return player.Character.QuestLog.FilterQuests(Template.Quests);
        }

        // Combat related
        public void EnterCombatWith(CreatureBase target)
        {
            if (target == this)
                return;
            StartAttacking(target);
            if (!SetInCombatWith(target))
                return;
            AggroPosition = Position;

            if (target.Type.IsPlayer())
            {
                PlayerCreature pc = target as PlayerCreature;
                pc.SetInCombatWith(this);
                if (pc.Character.Group != null)
                    SetLootRecipient(new Looter(pc.Character.Group));
                else
                    SetLootRecipient(new Looter(pc));
            }
        }
        public bool SetInCombatWith(CreatureBase target)
        {
            if (target == this)
                return false;
            ThreatManager.AddThreat(target, 0);
            if (IsInCombat)
                return false;
            IsInCombat = true;
            SendPacketToVisible(new SCreatureEnteredCombatPacket { GUID = GUID });
            SpellBook.Interrupt(InterruptMask.EnteringCombat);
            return true;
        }
        public void Evade()
        {
            StopAttacking();
            SetTarget(null);
            if (!IsInCombat)
                return;
            IsInCombat = false;
            Mover.MoveToPointWithSpeed(AggroPosition);
            foreach (ThreatEntry threat in ThreatManager.GetAttackers())
            {
                if (threat.Attacker.Type.IsPlayer())
                {
                    (threat.Attacker as PlayerCreature).ThreatManager.Remove(this);
                    if ((threat.Attacker as PlayerCreature).ThreatManager.Empty)
                        (threat.Attacker as PlayerCreature).Evade();
                }
                else if (threat.Attacker.Type.IsCreature())
                {
                    (threat.Attacker as Creature).ThreatManager.Remove(this);
                    if ((threat.Attacker as Creature).ThreatManager.Empty)
                        (threat.Attacker as Creature).Evade();
                }
            }
            ThreatManager.Clear();
            SendPacketToVisible(new SCreatureEvadedPacket { GUID = GUID });
            SpellBook.Interrupt(InterruptMask.ExitingCombat);

            ResetLootRecipient();
        }
        public bool CanAttack(AttackType type)
        {
            return !AttackPower[(byte)type].Empty;
        }
        public bool CanAttack(CreatureBase target)
        {
            return !HasFlag(ObjectFlags.Stunned) && SpellBook.CastingSpell == null && IsInMeleeDistance(target) && GetReactionTo(target) != Reaction.Friendly && IsInArc(target, MathHelper.Pi);
        }
        public bool IsAttackReady(AttackType type)
        {
            if (!CanAttack(type))
                return false;

            return attackIntervalLeft[(byte)type] <= 0;
        }
        public bool IsInMeleeDistance(CreatureBase target)
        {
            float dist = GetApproachRadius() + target.GetApproachRadius() + GetMeleeReach();
            return DistanceTo(target) <= dist;
        }
        public float GetMeleeReach()
        {
            return MeleeReach;
        }
        public void StartAttacking(CreatureBase target)
        {
            if (target == this)
                return;
            SetTarget(target);
        }
        public void StopAttacking() { }
        public void DoAttacksIfReady()
        {
            if (Target == null)
                return;
            if (Target == this)
                return;
            if (SpellBook.IsCasting)
                return;
            if (!CanAttack(Target))
                return;

            for (AttackType type = 0; type < AttackType.Max; ++type)
                if (IsAttackReady(type))
                    DoAttack(Target, type);
        }
        public void DoAttack(CreatureBase target, AttackType type)
        {
            if (!target.IsAlive)
                return;
            attackIntervalLeft[(byte)type] = AttackInterval[(byte)type].Random;
            AttackResult result = Formulas.CalculateAttackResult(this, target, type);
            switch (result)
            {
                case AttackResult.Hit:
                    target.MeleeHit(this, type);
                    SpellBook.Interrupt(InterruptMask.HittingInMelee);
                    target.InflictDamage(this, Formulas.CalculateDamage(this, target, AttackPower[(byte)type]));
                    (SpellBook as SpellBook).ProcessProc(AuraType.MeleeHitProc, target);
                    break;
                case AttackResult.Miss:
                    target.MeleeMiss(this, type);
                    SpellBook.Interrupt(InterruptMask.MissingInMelee);
                    (SpellBook as SpellBook).ProcessProc(AuraType.MeleeMissProc, target);
                    break;
            }
        }
        public CastSpellResult CastSpell(CreatureBase target, int spellID, CastSpellFlags flags)
        {
            return CastSpell(target, DatabaseManager.Spells[spellID], flags);
        }
        public CastSpellResult CastSpell(CreatureBase target, SpellTemplate spell, CastSpellFlags flags)
        {
            CastSpellResult result = SpellBook.CastSpell(spell, target, flags);
            return result;
        }
        public void ProcessTargetSpellHit(CreatureBase target, Spell spell, AttackResult result)
        {
            switch (result)
            {
                case AttackResult.Hit:
                    SpellBook.Interrupt(InterruptMask.HittingWithSpell);
                    break;
                case AttackResult.Miss:
                    SpellBook.Interrupt(InterruptMask.MissingWithSpell);
                    break;
            }
        }
        public void Die(CreatureBase killer)
        {
            killer.KilledCreature(this);
            SpellBook.Interrupt(InterruptMask.Dying);
            foreach (PlayerCreature player in talksTo)
                player.EndDialog(DialogEndReason.TalkerIncapable);
            SetTarget(null);
            SetHealth(0);
            SetState(CreatureState.JustDied);
            if (Template.Loot != null)
                SetFlag(ObjectFlags.Lootable);
            Mover.Clear();

            long despawnSecs, respawnSecs;
            switch (Template.Classification)
            {
                case CreatureClassification.Normal:
                    respawnSecs = (despawnSecs = 1 * 60) + 5 * 60;
                    break;
                case CreatureClassification.Elite:
                    respawnSecs = (despawnSecs = 5 * 60) + 10 * 60;
                    break;
                case CreatureClassification.Rare:
                    respawnSecs = (despawnSecs = 10 * 60) + 12 * 60 * 60;
                    break;
                case CreatureClassification.Boss:
                    respawnSecs = (despawnSecs = 60 * 60) + 60 * 60;
                    break;
                case CreatureClassification.Elite | CreatureClassification.Rare:
                    respawnSecs = (despawnSecs = 15 * 60) + 24 * 60 * 60;
                    break;
                case CreatureClassification.Elite | CreatureClassification.Boss:
                    respawnSecs = (despawnSecs = 60 * 60) + 120 * 60;
                    break;
                case CreatureClassification.Rare | CreatureClassification.Boss:
                    respawnSecs = (despawnSecs = 60 * 60) + 7 * 24 * 60 * 60;
                    break;
                case CreatureClassification.Elite | CreatureClassification.Rare | CreatureClassification.Boss:
                    respawnSecs = (despawnSecs = 24 * 60 * 60) + 30 * 24 * 60 * 60;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            Despawn(despawnSecs * 1000);
            Respawn(respawnSecs * 1000);
        }

        public override void InflictDamage(CreatureBase attacker, Damage damage)
        {
            if (Script != null)
                Script.OnDamageTaken(attacker, ref damage);
            base.InflictDamage(attacker, damage);
            SetInCombatWith(attacker);
            ThreatManager.AddThreat(attacker, damage.Sum);
            SendPacketToVisible(new SCombatInflictedDamagePacket { AttackerGUID = attacker.GUID, VictimGUID = GUID, Damage = damage });
            attacker.SpellBook.Interrupt(InterruptMask.InflictingDamage);
            SpellBook.Interrupt(InterruptMask.TakingDamage);
            if (CurrentHealth <= 0)
                Die(attacker);
        }
        public override void Heal(CreatureBase healer, int amount)
        {
            if (Script != null)
                Script.OnHealed(healer, ref amount);
            base.Heal(healer, amount);
            if (IsInCombat)
            {
                foreach (ThreatEntry entry in ThreatManager.GetAttackers())
                {
                    if (entry.Attacker.Type.IsPlayer())
                    {
                        PlayerCreature attacker = entry.Attacker as PlayerCreature;
                        attacker.SetInCombatWith(healer);
                        attacker.ThreatManager.AddThreat(healer, amount);
                    }
                    else if (entry.Attacker.Type.IsCreature())
                    {
                        Creature attacker = entry.Attacker as Creature;
                        attacker.SetInCombatWith(healer);
                        attacker.ThreatManager.AddThreat(healer, amount);
                    }
                }
            }
            SendPacketToVisible(new SCombatHealedPacket { HealerGUID = healer.GUID, TargetGUID = GUID, Heal = amount });
        }
        public override void KilledCreature(CreatureBase victim)
        {
            base.KilledCreature(victim);
            if (victim == Target)
                StopAttacking();
            ThreatManager.Remove(victim);
        }
        public override void Died(CreatureBase killer)
        {
            base.Died(killer);
            StopAttacking();
            if (Script != null)
                Script.OnDeath(killer);
        }
        public override void MeleeHit(CreatureBase attacker, AttackType type)
        {
            base.MeleeHit(attacker, type);
            if (Script != null)
                Script.OnMeleeHit(attacker);
            SendPacketToVisible(new SCombatPerformedMeleeAttackPacket { AttackerGUID = attacker.GUID, VictimGUID = GUID, AttackType = type, Result = AttackResult.Hit });
            SpellBook.Interrupt(InterruptMask.BeingHitInMelee);
        }
        public override void MeleeMiss(CreatureBase attacker, AttackType type)
        {
            base.MeleeMiss(attacker, type);
            if (Script != null)
                Script.OnMeleeMiss(attacker);
            SendPacketToVisible(new SCombatPerformedMeleeAttackPacket { AttackerGUID = attacker.GUID, VictimGUID = GUID, AttackType = type, Result = AttackResult.Miss });
            SpellBook.Interrupt(InterruptMask.BeingMissedInMelee);
        }
        public override void SpellHit(CreatureBase caster, SpellBase spell)
        {
            base.SpellHit(caster, spell);
            if (Script != null)
                Script.OnSpellHit(caster, spell.Template as SpellTemplate);
            if (caster.Type.IsPlayer())
                (caster as PlayerCreature).ProcessTargetSpellHit(this, spell as Spell, AttackResult.Hit);
            else
                (caster as Creature).ProcessTargetSpellHit(this, spell as Spell, AttackResult.Hit);
            SendPacketToVisible(new SCombatPerformedSpellAttackPacket { CasterGUID = caster.GUID, TargetGUID = GUID, SpellID = spell.Template.ID, Result = AttackResult.Hit });
            SpellBook.Interrupt(InterruptMask.BeingHitBySpell);
        }
        public override void SpellMiss(CreatureBase caster, SpellBase spell)
        {
            base.SpellMiss(caster, spell);
            if (Script != null)
                Script.OnSpellMiss(caster, spell.Template as SpellTemplate);
            if (caster.Type.IsPlayer())
                (caster as PlayerCreature).ProcessTargetSpellHit(this, spell as Spell, AttackResult.Miss);
            else
                (caster as Creature).ProcessTargetSpellHit(this, spell as Spell, AttackResult.Miss);
            SendPacketToVisible(new SCombatPerformedSpellAttackPacket { CasterGUID = caster.GUID, TargetGUID = GUID, SpellID = spell.Template.ID, Result = AttackResult.Miss });
            SpellBook.Interrupt(InterruptMask.BeingMissedBySpell);
        }
        public override void SpellCasted(SpellTemplateBase spell, CreatureBase target)
        {
            base.SpellCasted(spell, target);
            if (Script != null)
                Script.OnSpellCasted(spell as SpellTemplate, target);
            SendPacketToVisible(new SCombatSpellCastedPacket { CasterGUID = GUID, TargetGUID = target == null ? 0 : target.GUID, SpellID = spell.ID });
            SpellBook.Interrupt(InterruptMask.CastingSpell);
        }
        public override void SpellCasted(SpellTemplateBase spell, Vector3 target)
        {
            base.SpellCasted(spell, target);
            if (Script != null)
                Script.OnSpellCasted(spell as SpellTemplate, target);
            SendPacketToVisible(new SCombatSpellCastedPacket { CasterGUID = GUID, GroundTarget = target, SpellID = spell.ID });
            SpellBook.Interrupt(InterruptMask.CastingSpell);
        }
        public override void SpellCastingStarted(SpellTemplateBase spell, CreatureBase target)
        {
            base.SpellCastingStarted(spell, target);
            if (Script != null)
                Script.OnSpellCastingStarted(spell as SpellTemplate, target);
            SendPacketToVisible(new SCombatSpellCastingStartedPacket { CasterGUID = GUID, TargetGUID = target == null ? 0 : target.GUID, SpellID = spell.ID });
            SpellBook.Interrupt(InterruptMask.StartingCastingSpell);
        }
        public override void SpellCastingStarted(SpellTemplateBase spell, Vector3 target)
        {
            base.SpellCastingStarted(spell, target);
            if (Script != null)
                Script.OnSpellCastingStarted(spell as SpellTemplate, target);
            SendPacketToVisible(new SCombatSpellCastingStartedPacket { CasterGUID = GUID, GroundTarget = target, SpellID = spell.ID });
            SpellBook.Interrupt(InterruptMask.StartingCastingSpell);
        }
        public override void SpellCastingFailed(SpellTemplateBase spell, CreatureBase target, CastSpellResult result)
        {
            base.SpellCastingFailed(spell, target, result);
            if (Script != null)
                Script.OnSpellCastingFailed(spell as SpellTemplate, target, result);
            SendPacketToVisible(new SCombatSpellCastingFailedPacket { CasterGUID = GUID, TargetGUID = target == null ? 0 : target.GUID, SpellID = spell.ID, Result = result });
            SpellBook.Interrupt(InterruptMask.FailingCastingSpell);
        }
        public override void SpellCastingFailed(SpellTemplateBase spell, Vector3 target, CastSpellResult result)
        {
            base.SpellCastingFailed(spell, target, result);
            if (Script != null)
                Script.OnSpellCastingFailed(spell as SpellTemplate, target, result);
            SendPacketToVisible(new SCombatSpellCastingFailedPacket { CasterGUID = GUID, GroundTarget = target, SpellID = spell.ID, Result = result });
            SpellBook.Interrupt(InterruptMask.FailingCastingSpell);
        }
        public override void AuraApplied(AuraBase aura)
        {
            base.AuraApplied(aura);
            if (Script != null)
                Script.OnAuraApplied(aura as Aura);
            SendPacketToVisible(new SAppliedAuraPacket(aura));
        }
        public override void AuraRemoved(AuraBase aura)
        {
            base.AuraRemoved(aura);
            if (Script != null)
                Script.OnAuraRemoved(aura as Aura);
            SendPacketToVisible(new SRemovedAuraPacket(aura));
        }

        // Setters
        public override void SetHealth(int health)
        {
            base.SetHealth(health);
            SendPacketToVisible(new SCreatureHealthUpdatePacket { GUID = GUID, CurrentHealth = CurrentHealth, MaxHealth = MaxHealth });
        }
        public override void SetHealth(int currentHealth, int maxHealth)
        {
            base.SetHealth(currentHealth, maxHealth);
            SendPacketToVisible(new SCreatureHealthUpdatePacket { GUID = GUID, CurrentHealth = CurrentHealth, MaxHealth = MaxHealth });
        }
        public override void SetUsesPower(PowerType type, bool uses)
        {
            base.SetUsesPower(type, uses);
            SendPacketToVisible(new SCreatureUsesPowerUpdatePacket { GUID = GUID, PowerType = type, Uses = UsesPowerType[(byte)type] });
        }
        public override void SetPower(PowerType type, int power)
        {
            base.SetPower(type, power);
            SendPacketToVisible(new SCreaturePowerUpdatePacket { GUID = GUID, PowerType = type, CurrentPower = CurrentPower[(byte)type], MaxPower = MaxPower[(byte)type] });
        }
        public override void SetPower(PowerType type, int currentPower, int maxPower)
        {
            base.SetPower(type, currentPower, maxPower);
            SendPacketToVisible(new SCreaturePowerUpdatePacket { GUID = GUID, PowerType = type, CurrentPower = CurrentPower[(byte)type], MaxPower = MaxPower[(byte)type] });
        }
        public override void SetState(CreatureState state)
        {
            base.SetState(state);
            SendPacketToVisible(new SObjectUpdateLocationPacket() { GUID = GUID, Position = Position, Rotation = Rotation });
            SendPacketToVisible(new SCreatureStateUpdatePacket { GUID = GUID, State = State });
        }
        public override void SetTarget(CreatureBase target)
        {
            if (Target == target)
                return;

            base.SetTarget(target);
            SendPacketToVisible(new SCreatureTargetUpdatePacket { GUID = GUID, TargetGUID = Target == null ? 0 : Target.GUID });
        }
        public override void SetMovementType(MovementType type)
        {
            base.SetMovementType(type);
            SendPacketToVisible(new SCreatureMovementTypeUpdatePacket { GUID = GUID, MovementType = CurrentMovementType, MovementSpeedFactor = MovementSpeedFactor[(byte)CurrentMovementType] });
        }
        public override void SetMovementSpeedFactor(MovementType type, float factor)
        {
            base.SetMovementSpeedFactor(type, factor);
            SendPacketToVisible(new SCreatureMovementTypeUpdatePacket { GUID = GUID, MovementType = CurrentMovementType, MovementSpeedFactor = MovementSpeedFactor[(byte)CurrentMovementType] });
        }
        public void ResetLootRecipient()
        {
            SetLootRecipient(new Looter());
        }
        public void SetLootRecipient(Looter looter)
        {
            LootRecipient = looter;
            SendPacketToVisible(new SObjectLootRecipientUpdatePacket { GUID = GUID, LootRecipient = LootRecipient });
        }

        // Overrides, implementations and utility
        public override void Respawn()
        {
            base.Respawn();
            if (Script != null)
                Script.OnRespawn();
        }
        public override void Despawn()
        {
            foreach (PlayerCreature player in talksTo)
                player.EndDialog(DialogEndReason.TalkerIncapable);
            if (Script != null)
                Script.OnDespawn();
            base.Despawn();
        }

        public override void MoveTo(Vector3 pos)
        {
            if (Position == pos)
                return;
            base.MoveTo(pos);
            SpellBook.Interrupt(InterruptMask.Moving);
        }
        public override void RotateTo(Rotation rot)
        {
            if (Rotation == rot)
                return;
            base.RotateTo(rot);
            SpellBook.Interrupt(InterruptMask.Rotating);
        }

        public override void SetFlag(ObjectFlags flag, bool set)
        {
            base.SetFlag(flag, set);
            SendPacketToVisible(new SObjectFlagsUpdatePacket { GUID = GUID, Flags = Flags });
        }

        public override void OnPositionChanged()
        {
            this.UpdateVisibility(VisibleTo, VisibilityUpdateReason.Move);
        }
        public override void OnRotationChanged()
        {
        }
        public override void OnAddedToMap()
        {
            this.UpdateVisibility(VisibleTo, VisibilityUpdateReason.AddToMap);
        }
        public override void OnRemovedFromMap()
        {
            this.UpdateVisibility(VisibleTo, VisibilityUpdateReason.RemoveFromMap);
        }

        public void AddVisibleTo(PlayerCreatureBase pc)
        {
            if (VisibleTo.Contains(pc))
                return;
            VisibleTo.Add(pc);
        }
        public void RemoveVisibleTo(PlayerCreatureBase pc)
        {
            VisibleTo.Remove(pc);
        }
        public void SendPacketToVisible(IWorldPacket packet)
        {
            int count = VisibleTo.Count;
            for (int i = 0; i < count; i++)
                (VisibleTo[i] as PlayerCreature).Player.Session.SendPacket(packet);
        }
        public void SendPacketToVisible(IWorldPacket packet, byte[] rawData)
        {
            int count = VisibleTo.Count;
            for (int i = 0; i < count; i++)
                (VisibleTo[i] as PlayerCreature).Player.Session.SendPacket(packet, rawData);
        }
    }
}