﻿using System;
using System.Collections.Generic;
using Arcanos.Server.WorldServer.World;
using Arcanos.Shared;
using Arcanos.Shared.Objects;
using Arcanos.Utilities.Debug;

namespace Arcanos.Server.WorldServer.Objects
{
    public interface IVisibleToPlayers
    {
        void AddVisibleTo(PlayerCreatureBase pc);
        void RemoveVisibleTo(PlayerCreatureBase pc);
    }
    public enum VisibilityUpdateReason
    {
        Move,
        AddToMap,
        RemoveFromMap,
        AppearForOthers,
        DisappearForOthers,
    }
    public static class VisibilityExtensions
    {
        public static void UpdateVisibility(this WorldObjectBase self, VisibilityUpdateReason reason)
        {
            if (self.Type.IsPlayer())
                UpdateVisibility(self, (self as PlayerCreature).VisibleTo, reason);
            else if (self.Type.IsCreature())
                UpdateVisibility(self, (self as Creature).VisibleTo, reason);
            else if (self.Type.IsWorldObject())
                UpdateVisibility(self, (self as WorldObject).VisibleTo, reason);
        }
        public static void UpdateVisibility(this WorldObjectBase self, List<PlayerCreatureBase> visibleTo, VisibilityUpdateReason reason)
        {
            D.Assert(self.Map != null, "UpdateVisibility called for a WorldObject that is not present in any map");

            switch (reason)
            {
                case VisibilityUpdateReason.Move:
                    {
                        int count = visibleTo.Count;
                        for (int i = 0; i < count; i++)
                            if (self.DistanceTo2D(visibleTo[i]) > (self.Map as Map).VisibilityDistance)
                            {
                                (visibleTo[i] as PlayerCreature).RemoveVisibleObject(self);
                                --i;
                                --count;
                            }

                        foreach (WorldObjectBase wo in self.Map.GetVisibleObjects(self.Position))
                        {
                            if (wo.Type == ObjectType.PlayerCreature)
                                (wo as PlayerCreature).AddVisibleObject(self);
                        }
                        break;
                    }
                case VisibilityUpdateReason.AddToMap:
                case VisibilityUpdateReason.AppearForOthers:
                    foreach (WorldObjectBase wo in self.Map.GetVisibleObjects(self.Position))
                    {
                        if (wo.Type == ObjectType.PlayerCreature)
                            (wo as PlayerCreature).AddVisibleObject(self);
                        if (self.Type == ObjectType.PlayerCreature)
                            (self as PlayerCreature).AddVisibleObject(wo);
                    }
                    break;
                case VisibilityUpdateReason.RemoveFromMap:
                case VisibilityUpdateReason.DisappearForOthers:
                    {
                        int count = visibleTo.Count;
                        for (int i = 0; i < count; i++)
                        {
                            (visibleTo[i] as PlayerCreature).RemoveVisibleObject(self);
                            --i;
                            --count;
                        }

                        if (reason != VisibilityUpdateReason.DisappearForOthers && self.Type == ObjectType.PlayerCreature)
                            while ((self as PlayerCreature).VisibleObjects.Count > 0)
                                (self as PlayerCreature).RemoveVisibleObject(0);
                        break;
                    }
                default:
                    throw new ArgumentOutOfRangeException("reason");
            }
        }
    }
}