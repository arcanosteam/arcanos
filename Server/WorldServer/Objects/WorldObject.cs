﻿using System.Collections.Generic;
using Arcanos.Server.WorldServer.Dialog;
using Arcanos.Server.WorldServer.Quests;
using Arcanos.Server.WorldServer.Scripting;
using Arcanos.Shared;
using Arcanos.Shared.Dialog;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Net;
using Arcanos.Shared.Objects;
using Microsoft.Xna.Framework;

namespace Arcanos.Server.WorldServer.Objects
{
    public class WorldObject : WorldObjectBase, IVisibleToPlayers, IDialogTalker
    {
        public new WorldObjectTemplate Template { get { return (WorldObjectTemplate)template; } }

        public override string Name
        {
            get { return template.Name == null ? Text.MissingString : template.Name.ToString(); }
        }
        public override Text NameRef
        {
            get { return template.Name; }
        }

        public readonly WorldObjectScript Script;

        public readonly List<PlayerCreatureBase> VisibleTo = new List<PlayerCreatureBase>();
        private readonly List<PlayerCreatureBase> talksTo = new List<PlayerCreatureBase>();

        public WorldObject(ulong guid, WorldObjectTemplate template, Vector3 pos, Rotation rot) : base(guid, template, pos, rot)
        {
            if (Template.Script != null)
                Script = Template.Script.Make(this);

            ResetFromTemplate();

            if (Script != null)
                Script.OnSpawn();
        }

        protected override void ResetFromTemplate()
        {
            base.ResetFromTemplate();

            if (Template.Dialog != null)
                SetFlag(ObjectFlags.HasDialog, true);

            if (Script != null)
                Script.OnReset();
        }

        public override bool Update()
        {
            if (!base.Update())
                return false;

            if (Script != null)
                Script.OnUpdate();

            return true;
        }

        public override void SetFlag(ObjectFlags flag, bool set)
        {
            base.SetFlag(flag, set);
            SendPacketToVisible(new SObjectFlagsUpdatePacket { GUID = GUID, Flags = Flags });
        }

        // Dialog related
        public DialogStart GetDialogStart(PlayerCreature player, out DialogPage startingPage)
        {
            DialogStart dialog = Script == null ? Template.Dialog : Script.GetDialogStart(player);
            startingPage = dialog == null ? null : dialog.GetStartingPageFor(player);
            return dialog;
        }
        public bool CanEndDialog(PlayerCreature player, DialogPage page)
        {
            return Script == null || Script.CanEndDialog(player, page);
        }
        public void StartedDialog(PlayerCreature player, DialogStart dialog, DialogPage page)
        {
            talksTo.Add(player);
            if (Script != null)
                Script.OnDialogStart(player, dialog, page);
        }
        public void EndedDialog(PlayerCreature player, DialogPage page, DialogEndReason reason)
        {
            talksTo.Remove(player);
            if (Script != null)
                Script.OnDialogEnd(player, page, reason);
        }
        public bool SelectOption(PlayerCreature player, DialogPage page, DialogOption option, int index)
        {
            return Script != null && Script.OnDialogOption(player, page, option, index);
        }
        public void ChangedPage(PlayerCreature player, DialogPage currentPage, ref DialogPage nextPage)
        {
            if (Script != null)
                Script.OnDialogPage(player, currentPage, ref nextPage);
        }
        public IList<Quest> GetQuestsFor(PlayerCreature player)
        {
            return player.Character.QuestLog.FilterQuests(Template.Quests);
        }

        // Overrides, implementations and utility
        public override void Respawn()
        {
            base.Respawn();
            if (Script != null)
                Script.OnRespawn();
        }
        public override void Despawn()
        {
            foreach (PlayerCreature player in talksTo)
                player.EndDialog(DialogEndReason.TalkerIncapable);
            if (Script != null)
                Script.OnDespawn();
            base.Despawn();
        }

        public override void OnPositionChanged()
        {
            this.UpdateVisibility(VisibleTo, VisibilityUpdateReason.Move);
        }
        public override void OnRotationChanged()
        {
        }
        public override void OnAddedToMap()
        {
            this.UpdateVisibility(VisibleTo, VisibilityUpdateReason.AddToMap);
        }
        public override void OnRemovedFromMap()
        {
            this.UpdateVisibility(VisibleTo, VisibilityUpdateReason.RemoveFromMap);
        }

        public void AddVisibleTo(PlayerCreatureBase pc)
        {
            if (VisibleTo.Contains(pc))
                return;
            VisibleTo.Add(pc);
        }
        public void RemoveVisibleTo(PlayerCreatureBase pc)
        {
            VisibleTo.Remove(pc);
        }
        public void SendPacketToVisible(IWorldPacket packet)
        {
            int count = VisibleTo.Count;
            for (int i = 0; i < count; i++)
                (VisibleTo[i] as PlayerCreature).Player.Session.SendPacket(packet);
        }
        public void SendPacketToVisible(IWorldPacket packet, byte[] rawData)
        {
            int count = VisibleTo.Count;
            for (int i = 0; i < count; i++)
                (VisibleTo[i] as PlayerCreature).Player.Session.SendPacket(packet, rawData);
        }
    }
}