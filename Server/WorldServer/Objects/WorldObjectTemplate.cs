﻿using System.Collections.Generic;
using Arcanos.Server.WorldServer.Data;
using Arcanos.Server.WorldServer.Dialog;
using Arcanos.Server.WorldServer.Items;
using Arcanos.Server.WorldServer.Quests;
using Arcanos.Server.WorldServer.Scripting;
using Arcanos.Shared;
using Arcanos.Shared.Objects;

namespace Arcanos.Server.WorldServer.Objects
{
    public class WorldObjectTemplate : WorldObjectTemplateBase
    {
        public DialogStart Dialog;
        public IntBounds Money;
        public LootTemplate Loot;
        public List<Quest> Quests = new List<Quest>();

        public WorldObjectScriptLoader Script;

        protected override int GetInitialID()
        {
            return DatabaseManager.WorldObjects.GetID(this);
        }
    }
}