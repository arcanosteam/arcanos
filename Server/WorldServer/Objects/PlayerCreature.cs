﻿using System.Collections.Generic;
using Arcanos.Server.WorldServer.Data;
using Arcanos.Server.WorldServer.Dialog;
using Arcanos.Server.WorldServer.Items;
using Arcanos.Server.WorldServer.Movement;
using Arcanos.Server.WorldServer.Players;
using Arcanos.Server.WorldServer.PvP;
using Arcanos.Server.WorldServer.Quests;
using Arcanos.Server.WorldServer.Scripting;
using Arcanos.Server.WorldServer.Scripting.Game;
using Arcanos.Server.WorldServer.Spells;
using Arcanos.Server.WorldServer.World;
using Arcanos.Shared;
using Arcanos.Shared.Dialog;
using Arcanos.Shared.Graphics;
using Arcanos.Shared.Items;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Net;
using Arcanos.Shared.Objects;
using Arcanos.Shared.PvP;
using Arcanos.Shared.Spells;
using Arcanos.Shared.Timing;
using Arcanos.Shared.World;
using Arcanos.Utilities;
using Microsoft.Xna.Framework;

namespace Arcanos.Server.WorldServer.Objects
{
    public class PlayerCreature : PlayerCreatureBase, IVisibleToPlayers
    {
        private const int COMBAT_CHECK_INTERVAL = 2000;

        public new CreatureTemplate Template { get { return (CreatureTemplate)template; } }

        public readonly Player Player;
        public new Character Character
        {
            get { return (Character)character; }
        }

        public override string Name
        {
            get { return character.Name ?? Text.MissingString; }
        }
        public override Text NameRef
        {
            get { return template.Name; }
        }

        public readonly DamageBounds[] AttackPower = new DamageBounds[(byte)AttackType.Max];
        public readonly FloatBounds[] AttackInterval = new FloatBounds[(byte)AttackType.Max];
        private readonly float[] attackIntervalLeft = new float[(byte)AttackType.Max];
        private readonly float[] regenIntervalLeft = new float[(byte)PowerType.Max + 1];
        public float MeleeReach;

        public ThreatManager ThreatManager;
        private long nextCombatCheckTime;

        public WorldObjectBase LootingObject;

        public Duel Duel;
        public PlayerCreatureBase DuelRequestSender;

        public readonly PlayerCreatureScript Script;

        public readonly List<WorldObjectBase> VisibleObjects = new List<WorldObjectBase>();
        public readonly List<PlayerCreatureBase> VisibleTo = new List<PlayerCreatureBase>();

        public PlayerCreature(ulong guid, Character character, Vector3 pos, Rotation rot) : base(guid, MakeTemplateFor(character), character, pos, rot)
        {
            Player = character.Player;
            Mover = new Mover(this);

            SpellBook = new SpellBook(this);

            if (ScriptLoader.GetPlayerCreatureScriptLoader() != null)
                Script = ScriptLoader.GetPlayerCreatureScriptLoader().Make(this);

            ResetFromTemplate();

            Level = character.Level;

            foreach (SpellTemplate spell in character.SpellBook.Spells)
                SpellBook.AddSpell(spell);
            foreach (Aura aura in character.SpellBook.Auras)
                (SpellBook as SpellBook).ApplyAura(aura);
            foreach (CharacterSpellBookData.CooldownInstance cooldown in character.SpellBook.Cooldowns)
                (SpellBook as SpellBook).AddCooldown(cooldown.CategoryID, cooldown.Left, cooldown.Total);

            UpdatePlayerStats(true);

            if (Script != null)
                Script.OnSpawn();
        }

        public static CreatureTemplate MakeTemplateFor(Character character)
        {
            CreatureTemplate template = new CreatureTemplate()
            {
                ID = -1,
                Level = new IntBounds(1),
                Faction = DatabaseManager.Factions[1],
                Name = null,
                Shape = Shared.Collisions.Shape.Cylinder,
                Size = new Vector3(0.5f, 0, 2.0f),
                Model = DatabaseManager.RaceInfos[(byte)character.Race].Models[character.ModelIndex],
                LightData = new LightData(new Vector3(R.Float(0.9f, 1.0f), R.Float(0.9f, 1.0f), R.Float(0.9f, 1.0f)), R.Float(0.5f, 1.0f), R.Float(10, 50)),
                MovementSpeed = new[] { 2.5f, 5f, 0, 0 },
            };
            List<SpellTemplate> spells = new List<SpellTemplate>();
            spells.AddRange(DatabaseManager.ClassInfos[(byte)character.Class].Spells);
            spells.AddRange(DatabaseManager.RaceInfos[(byte)character.Race].Spells);
            template.Spells = spells.ToArray();
            return template;
        }
        protected override void ResetFromTemplate()
        {
            base.ResetFromTemplate();

            MeleeReach = Template.MeleeReach;

            ThreatManager = new ThreatManager();

            LootingObject = null;

            SpellBook.RemoveAllSpells();
            SpellBook.RemoveAllAuras();

            if (Template.Spells != null)
                foreach (SpellTemplate spell in Template.Spells)
                    SpellBook.AddSpell(spell);
            if (Template.Auras != null)
                foreach (SpellTemplate spell in Template.Auras)
                    for (int i = 0; i < spell.Effects.Length; ++i)
                        if (spell.Effects[i].IsAuraEffect)
                            SpellBook.ApplyAura(GUID, spell, i);

            if (Script != null)
                Script.OnReset();

            autoAttack = false;
        }

        public override bool Update()
        {
            if (!base.Update())
                return false;

            UpdateState();
            UpdateAttackCooldowns();
            UpdateRegen();
            UpdateCombatState(false);
            SpellBook.Update();
            if (autoAttack)
                DoAttacksIfReady();
            if (Script != null)
                Script.OnUpdate();

            return true;
        }

        private void UpdateState()
        {
            switch (State)
            {
                case CreatureState.JustSpawned:
                    SetState(CreatureState.Alive);
                    break;
                case CreatureState.Alive:
                    break;
                case CreatureState.JustDied:
                    SetState(CreatureState.Corpse);
                    break;
                case CreatureState.Corpse:
                case CreatureState.Despawned:
                    break;
            }
        }
        private void UpdateAttackCooldowns()
        {
            for (AttackType type = 0; type < AttackType.Max; ++type)
                if (attackIntervalLeft[(byte)type] > 0)
                    attackIntervalLeft[(byte)type] -= Time.PerSecond;
        }
        private void UpdateRegen()
        {
            if (!IsAlive)
                return;
            for (byte type = 0; type < (byte)PowerType.Max + 1; ++type)
            {
                if (regenIntervalLeft[type] > 0)
                    regenIntervalLeft[type] -= Time.PerSecond;
                while (regenIntervalLeft[type] <= 0)
                {
                    float interval;
                    if (type == 0)
                    {
                        int regen = Formulas.CalcHealthRegen(this, IsInCombat);
                        if (regen != 0 && CurrentHealth < MaxHealth)
                            ModityHealth(regen);
                        interval = Formulas.CalcHealthRegenInterval(this, IsInCombat);
                    }
                    else
                    {
                        int regen = Formulas.CalcPowerRegen((PowerType)(type - 1), this, IsInCombat);
                        if (regen != 0 && CurrentPower[type - 1] < MaxPower[type - 1])
                            ModityPower((PowerType)(type - 1), regen);
                        interval = Formulas.CalcPowerRegenInterval((PowerType)(type - 1), this, IsInCombat);
                    }
                    if (interval == 0)
                        break;
                    regenIntervalLeft[type] += interval;
                }
            }
        }
        private void UpdateCombatState(bool now)
        {
            if (now || nextCombatCheckTime >= Time.Timestamp)
            {
                nextCombatCheckTime = Time.Timestamp + COMBAT_CHECK_INTERVAL;

                CreatureBase topThreat;
                while ((topThreat = ThreatManager.GetTopThreat()) != null)
                {
                    if (topThreat.IsAlive && topThreat.Map == Map)
                    {
                        if (!IsInCombat)
                        {
                            SetInCombatWith(topThreat);
                            Logger.Error(LogCategory.Player, "Player {2} (GUID={1} ID={0}) entered combat with creature {5} (GUID={4} ID={3}) because it was present in the threat list and the player was out of combat.", Character.ID, GUID, Name, topThreat.Template.ID, topThreat.GUID, topThreat.Name);
                        }
                        return;
                    }

                    ThreatManager.Remove(topThreat);
                }

                if (IsInCombat)
                    Evade();
            }
        }

        public void UpdatePlayerStats(bool loading)
        {
            int health = Formulas.GetHealthForPlayer(Character);
            SetHealth(loading ? Character.Health : health, health);
            for (byte type = 0; type < (byte)PowerType.Max; type++)
            {
                int power = Formulas.GetPowerForPlayer(Character, (PowerType)type);
                SetUsesPower((PowerType)type, power > 0);
                SetPower((PowerType)type, loading ? Character.Power[type] : power, power);
            }

            UpdateAttackPowerStat();
            UpdateAttackIntervalStat();
        }
        public void UpdateHealthStat()
        {
            int max = Formulas.GetHealthForPlayer(Character);
            int cur = CurrentHealth + max - MaxHealth;
            SetHealth(cur <= 0 ? 1 : cur, max);
        }
        public void UpdatePowerStat(PowerType power)
        {
            int max = Formulas.GetPowerForPlayer(Character, power);
            int d = max - MaxPower[(byte)power];
            if (d < 0)
                d = 0;
            int cur = CurrentPower[(byte)power] + d;
            bool usesChanged = CurrentPower[(byte)power] > 0 ^ cur > 0;
            SetPower(power, cur, max);
            if (usesChanged)
                SetUsesPower(power, power > 0);
        }
        public void UpdateAttackPowerStat()
        {
            UpdateAttackPowerStat(null);
        }
        public void UpdateAttackPowerStat(Item exceptItem)
        {
            ItemBase mainHand = Character.Equipment.Items[(byte)EquipSlot.MainHand];
            ItemBase offHand = Character.Equipment.Items[(byte)EquipSlot.OffHand];
            if (mainHand == exceptItem)
                mainHand = null;
            if (offHand == exceptItem)
                offHand = null;
            for (byte type = 0; type < (byte)AttackType.Max; type++)
                AttackPower[type] = Formulas.CalculatePlayerAttackPower(this, (AttackType)type, mainHand, offHand);
        }
        public void UpdateAttackIntervalStat()
        {
            UpdateAttackIntervalStat(null);
        }
        public void UpdateAttackIntervalStat(Item exceptItem)
        {
            ItemBase mainHand = Character.Equipment.Items[(byte)EquipSlot.MainHand];
            ItemBase offHand = Character.Equipment.Items[(byte)EquipSlot.OffHand];
            if (mainHand == exceptItem)
                mainHand = null;
            if (offHand == exceptItem)
                offHand = null;
            for (byte type = 0; type < (byte)AttackType.Max; type++)
                attackIntervalLeft[type] = (AttackInterval[type] = Formulas.CalculatePlayerAttackInterval(this, (AttackType)type, mainHand, offHand)).Random;
        }

        // Movement
        public void ChangeMap(int mapID, float x, float y, float z, float yaw)
        {
            if (Map != null)
                Map.Remove(this);

            Position = new Vector3(x, y, z);
            GetVolume(out BoundingBox.Min, out BoundingBox.Max);
            Rotation.Yaw = yaw;

            Map map = WorldManager.GetOrCreateMapFor(Character, mapID);

            SendPacketToSelf(new SChangeMapPacket
            {
                MapGUID = map.GUID,
                MapTemplateID = map.Template.ID,
                MapEnvironmentID = (map.Environment as Environment).CurrentID,
                Position = Position,
                Rotation = Rotation,
            });

            MarkAsSpawned(false);
            map.Add(this);
        }
        public void MarkAsSpawned(bool spawned)
        {
            isDespawned = !spawned;
            if (spawned)
                OnAddedToMap();
        }

        // Dialog related
        private IDialogTalker currentDialogTalker;
        private DialogPage currentDialogPage;
        private Quest currentDialogQuest;
        public bool StartDialog(WorldObjectBase talker)
        {
            currentDialogQuest = null;
            if (currentDialogTalker != null || currentDialogPage != null)
                if (!EndDialog(DialogEndReason.ChangeDialog))
                    return false;

            if (talker.Type.IsPlayer())
            {
                SendPacketToSelf(new SStartDialogResultPacket { TalkerGUID = talker.GUID, Result = DialogStartResult.TargetIsPlayer });
                return false;
            }

            DialogPage startingPage;
            DialogStart dialog = (talker as IDialogTalker).GetDialogStart(this, out startingPage);
            if (dialog == null || startingPage == null)
            {
                SendPacketToSelf(new SStartDialogResultPacket { TalkerGUID = talker.GUID, Result = DialogStartResult.NoDialog });
                return false;
            }

            currentDialogTalker = talker as IDialogTalker;
            SendPacketToSelf(new SStartDialogResultPacket { TalkerGUID = talker.GUID, Result = DialogStartResult.Ok });

            ChangePage(startingPage);

            currentDialogTalker.StartedDialog(this, dialog, startingPage);

            return true;
        }
        public bool EndDialog(DialogEndReason reason)
        {
            if (!currentDialogTalker.CanEndDialog(this, currentDialogPage))
                return false;

            IDialogTalker tempTalker = currentDialogTalker;
            DialogPage tempPage = currentDialogPage;
            ulong guid = currentDialogTalker.GUID;
            currentDialogTalker = null;
            currentDialogPage = null;
            SendPacketToSelf(new SEndDialogPacket { TalkerGUID = guid, Reason = reason });

            tempTalker.EndedDialog(this, tempPage, reason);

            return true;
        }
        public void SelectOption(bool isQuestOption, byte index)
        {
            if (currentDialogQuest != null && !isQuestOption)
            {
                Quest quest = currentDialogQuest;
                DialogQuestOptionState state = Character.QuestLog.GetDialogQuestOptionStateForQuest(quest);

                if (index == 0)
                {
                    if (state == DialogQuestOptionState.Acceptable)
                        Character.QuestLog.AddQuest(currentDialogQuest);
                    else if (state == DialogQuestOptionState.Completed)
                    {
                        if (!Character.QuestLog.CanBeRewardedForQuest(currentDialogQuest))
                            return;

                        Character.QuestLog.RemoveQuest(currentDialogQuest);
                        Character.QuestLog.RewardQuest(currentDialogQuest);
                    }
                }
                EndDialog(DialogEndReason.DialogEnded);
                currentDialogQuest = null;
                return;
            }
            if (currentDialogPage == null)
            {
                SendPacketToSelf(new SSelectOptionResultPacket { IsQuestOption = isQuestOption, Index = index, Result = DialogSelectOptionResult.NotTalking });
                return;
            }
            if (isQuestOption)
            {
                IList<Quest> quests = currentDialogTalker.GetQuestsFor(this);

                if (index >= quests.Count)
                {
                    SendPacketToSelf(new SSelectOptionResultPacket { IsQuestOption = true, Index = index, Result = DialogSelectOptionResult.WrongPage });
                    return;
                }

                Quest quest = quests[index];
                currentDialogQuest = quest;
                DialogQuestOptionState state = Character.QuestLog.GetDialogQuestOptionStateForQuest(quest);
                Text optionText = null;
                switch (state)
                {
                    case DialogQuestOptionState.Acceptable:
                        optionText = quest.AcceptText;
                        break;
                    case DialogQuestOptionState.NotCompleted:
                        optionText = quest.StatusQueryText;
                        break;
                    case DialogQuestOptionState.Completed:
                        optionText = quest.CompletedText;
                        break;
                }
                if (state == DialogQuestOptionState.Acceptable)
                {
                    SendPacketToSelf(new SDialogQuestAcceptPagePacket { QuestID = quest.ID, TextID = optionText == null ? 0 : optionText.ID, CanAccept = true });
                    byte i = 0;
                    SendPacketToSelf(new SPageOptionPacket { Index = i++, TextID = Localization.Game.DialogAcceptQuestOption.ID });
                    SendPacketToSelf(new SPageOptionPacket { Index = i, TextID = Localization.Game.DialogDeclineQuestOption.ID });
                }
                else if (state == DialogQuestOptionState.Completed || state == DialogQuestOptionState.NotCompleted)
                {
                    SendPacketToSelf(new SDialogQuestCompletePagePacket { QuestID = quest.ID, TextID = optionText == null ? 0 : optionText.ID, CanComplete = state == DialogQuestOptionState.Completed });
                    byte i = 0;
                    if (state == DialogQuestOptionState.Completed)
                        SendPacketToSelf(new SPageOptionPacket { Index = i++, TextID = Localization.Game.DialogCompleteQuestOption.ID });
                    SendPacketToSelf(new SPageOptionPacket { Index = i, TextID = Localization.Game.DialogDeclineQuestOption.ID });
                }
            }
            else
            {
                IList<DialogOption> options = currentDialogPage.GetOptionsFor(this);
                if (index >= options.Count)
                {
                    SendPacketToSelf(new SSelectOptionResultPacket { IsQuestOption = false, Index = index, Result = DialogSelectOptionResult.WrongPage });
                    return;
                }

                SendPacketToSelf(new SSelectOptionResultPacket { IsQuestOption = false, Index = index, Result = DialogSelectOptionResult.Ok });
                DialogOption option = options[index];

                if (!currentDialogTalker.SelectOption(this, currentDialogPage, option, index))
                    option.DoAction(this, currentDialogTalker);
            }
        }
        public bool ChangePage(DialogPage nextPage)
        {
            currentDialogTalker.ChangedPage(this, currentDialogPage, ref nextPage);

            currentDialogPage = nextPage;
            IList<DialogOption> options = currentDialogPage.GetOptionsFor(this);

            IList<Quest> quests = currentDialogTalker.GetQuestsFor(this);

            SendPacketToSelf(new SChangedPagePacket { TextID = currentDialogPage.Text.ID, OptionCount = (byte)options.Count, QuestOptionCount = (byte)quests.Count });

            byte index = 0;
            foreach (DialogOption option in options)
                SendPacketToSelf(new SPageOptionPacket { Index = index++, TextID = option.Text.ID, IconID = option.Icon });

            index = 0;
            foreach (Quest quest in quests)
                SendPacketToSelf(new SPageQuestOptionPacket { Index = index++, QuestID = quest.ID, TextID = quest.Title.ID, State = Character.QuestLog.GetDialogQuestOptionStateForQuest(quest) });

            return true;
        }
        public bool IsTalking(ulong guid)
        {
            return currentDialogPage != null;
        }
        public bool IsTalkingTo(ulong guid)
        {
            if (currentDialogTalker == null)
                return false;

            return currentDialogTalker.GUID == guid;
        }

        // Combat related
        private bool autoAttack;
        public bool SetInCombatWith(CreatureBase target)
        {
            if (target == this)
                return false;
            ThreatManager.AddThreat(target, 0);
            if (!target.IsInCombat)
            {
                bool stop = IsInCombat;
                IsInCombat = true;
                if (target.Type.IsPlayer())
                    (target as PlayerCreature).SetInCombatWith(this);
                else if (target.Type.IsCreature())
                    (target as Creature).SetInCombatWith(this);
                if (stop)
                    return false;
            }
            if (IsInCombat)
                return false;
            IsInCombat = true;
            SendPacketToVisible(new SCreatureEnteredCombatPacket { GUID = GUID });
            SpellBook.Interrupt(InterruptMask.EnteringCombat);
            return true;
        }
        public void Evade()
        {
            if (!IsInCombat)
                return;
            IsInCombat = false;
            foreach (ThreatEntry threat in ThreatManager.GetAttackers())
            {
                if (threat.Attacker.Type.IsPlayer())
                {
                    (threat.Attacker as PlayerCreature).ThreatManager.Remove(this);
                    if ((threat.Attacker as PlayerCreature).ThreatManager.Empty)
                        (threat.Attacker as PlayerCreature).Evade();
                }
                else if (threat.Attacker.Type.IsCreature())
                {
                    (threat.Attacker as Creature).ThreatManager.Remove(this);
                    if ((threat.Attacker as Creature).ThreatManager.Empty)
                        (threat.Attacker as Creature).Evade();
                }
            }
            ThreatManager.Clear();
            SendPacketToVisible(new SCreatureEvadedPacket { GUID = GUID });
            SpellBook.Interrupt(InterruptMask.ExitingCombat);
        }
        public bool CanAttack(AttackType type)
        {
            return !AttackPower[(byte)type].Empty;
        }
        public bool CanAttack(CreatureBase target)
        {
            return !HasFlag(ObjectFlags.Stunned) && SpellBook.CastingSpell == null && IsInMeleeDistance(target) && GetReactionTo(target) != Reaction.Friendly && IsInArc(target, MathHelper.Pi);
        }
        public bool IsAttackReady(AttackType type)
        {
            if (!CanAttack(type))
                return false;

            return attackIntervalLeft[(byte)type] <= 0;
        }
        public bool IsInMeleeDistance(CreatureBase target)
        {
            float dist = GetApproachRadius() + target.GetApproachRadius() + GetMeleeReach();
            return DistanceTo(target) <= dist;
        }
        public float GetMeleeReach()
        {
            return MeleeReach;
        }
        public void StartAttacking(CreatureBase target)
        {
            if (target == this)
                return;
            SetTarget(target);
            autoAttack = true;
        }
        public void StopAttacking()
        {
            autoAttack = false;
        }
        public void DoAttacksIfReady()
        {
            if (Target == null)
                return;
            if (Target == this)
                return;
            if (SpellBook.IsCasting)
                return;
            if (!CanAttack(Target))
                return;

            for (AttackType type = 0; type < AttackType.Max; ++type)
                if (IsAttackReady(type))
                    DoAttack(Target, type);
        }
        public void DoAttack(CreatureBase target, AttackType type)
        {
            if (!target.IsAlive)
                return;
            attackIntervalLeft[(byte)type] = AttackInterval[(byte)type].Random;
            AttackResult result = Formulas.CalculateAttackResult(this, target, type);
            switch (result)
            {
                case AttackResult.Hit:
                    target.MeleeHit(this, type);
                    SpellBook.Interrupt(InterruptMask.HittingInMelee);
                    target.InflictDamage(this, Formulas.CalculateDamage(this, target, AttackPower[(byte)type]));
                    (SpellBook as SpellBook).ProcessProc(AuraType.MeleeHitProc, target);
                    break;
                case AttackResult.Miss:
                    target.MeleeMiss(this, type);
                    SpellBook.Interrupt(InterruptMask.MissingInMelee);
                    (SpellBook as SpellBook).ProcessProc(AuraType.MeleeMissProc, target);
                    break;
            }
        }
        public CastSpellResult CastSpell(CreatureBase target, int spellID, CastSpellFlags flags)
        {
            return CastSpell(target, DatabaseManager.Spells[spellID], flags);
        }
        public CastSpellResult CastSpell(CreatureBase target, SpellTemplate spell, CastSpellFlags flags)
        {
            CastSpellResult result = SpellBook.CastSpell(spell, target, flags);
            return result;
        }
        public CastSpellResult CastSpell(Vector3 target, int spellID, CastSpellFlags flags)
        {
            return CastSpell(target, DatabaseManager.Spells[spellID], flags);
        }
        public CastSpellResult CastSpell(Vector3 target, SpellTemplate spell, CastSpellFlags flags)
        {
            CastSpellResult result = SpellBook.CastSpell(spell, target, flags);
            return result;
        }
        public void ProcessTargetSpellHit(CreatureBase target, Spell spell, AttackResult result)
        {
            switch (result)
            {
                case AttackResult.Hit:
                    SpellBook.Interrupt(InterruptMask.HittingWithSpell);
                    break;
                case AttackResult.Miss:
                    SpellBook.Interrupt(InterruptMask.MissingWithSpell);
                    break;
            }
        }
        public void Die(CreatureBase killer)
        {
            if (LoseDuel())
            {
                SetHealth(1);
                Evade();
                return;
            }
            killer.KilledCreature(this);
            SpellBook.Interrupt(InterruptMask.Dying);
            SetTarget(null);
            SetHealth(0);
            SetState(CreatureState.JustDied);
            if (Template.Loot != null)
                SetFlag(ObjectFlags.Lootable);
            Mover.Clear();
        }

        public void Revive(float healthPct, float powerPct)
        {
            SetHealth((int)(MaxHealth * healthPct));
            for (byte power = 0; power < (byte)PowerType.Max; power++)
                if (UsesPowerType[power])
                    SetPower((PowerType)power, (int)(MaxPower[power] * powerPct));
            SetState(CreatureState.Alive);
        }

        public void StartDuel(PlayerCreature opponent)
        {
            if (opponent.Duel != null)
            {
                Duel = opponent.Duel;
                opponent.Duel.AddParticipant(this);
                return;
            }

            Duel = new Duel();
            Duel.AddParticipant(this);
            Duel.AddParticipant(opponent);
            Duel.SetStatus(DuelStatus.Started);
        }
        public bool LoseDuel()
        {
            if (Duel == null)
                return false;

            Duel.SetStatus(this, DuelParticipantStatus.Lose);
            Duel.RemoveParticipant(this);
            return true;
        }

        public override void InflictDamage(CreatureBase attacker, Damage damage)
        {
            if (Script != null)
                Script.OnDamageTaken(attacker, ref damage);
            base.InflictDamage(attacker, damage);
            SetInCombatWith(attacker);
            if (attacker != this)
                ThreatManager.AddThreat(attacker, damage.Sum);
            nextCombatCheckTime = Time.Timestamp + COMBAT_CHECK_INTERVAL;
            SendPacketToVisible(new SCombatInflictedDamagePacket { AttackerGUID = attacker.GUID, VictimGUID = GUID, Damage = damage });
            attacker.SpellBook.Interrupt(InterruptMask.InflictingDamage);
            SpellBook.Interrupt(InterruptMask.TakingDamage);
            if (CurrentHealth <= 0)
                Die(attacker);
        }
        public override void Heal(CreatureBase healer, int amount)
        {
            if (Script != null)
                Script.OnHealed(healer, ref amount);
            base.Heal(healer, amount);
            if (IsInCombat)
            {
                foreach (ThreatEntry entry in ThreatManager.GetAttackers())
                {
                    if (entry.Attacker.Type.IsPlayer())
                    {
                        PlayerCreature attacker = entry.Attacker as PlayerCreature;
                        attacker.SetInCombatWith(healer);
                        attacker.ThreatManager.AddThreat(healer, amount);
                    }
                    else if (entry.Attacker.Type.IsCreature())
                    {
                        Creature attacker = entry.Attacker as Creature;
                        attacker.SetInCombatWith(healer);
                        attacker.ThreatManager.AddThreat(healer, amount);
                    }
                }
            }
            SendPacketToVisible(new SCombatHealedPacket { HealerGUID = healer.GUID, TargetGUID = GUID, Heal = amount });
        }
        public override void KilledCreature(CreatureBase victim)
        {
            base.KilledCreature(victim);
            if (victim == Target)
                StopAttacking();
            ThreatManager.Remove(victim);
            UpdateCombatState(true);
            Character.AddExperience(Formulas.CalculateExperienceForKill(this, victim));
            Character.QuestLog.CreditKill(victim);
        }
        public override void Died(CreatureBase killer)
        {
            base.Died(killer);
            StopAttacking();
            if (Script != null)
                Script.OnDeath(killer);
        }
        public override void MeleeHit(CreatureBase attacker, AttackType type)
        {
            base.MeleeHit(attacker, type);
            if (Script != null)
                Script.OnMeleeHit(attacker);
            SendPacketToVisible(new SCombatPerformedMeleeAttackPacket { AttackerGUID = attacker.GUID, VictimGUID = GUID, AttackType = type, Result = AttackResult.Hit });
            SpellBook.Interrupt(InterruptMask.BeingHitInMelee);
        }
        public override void MeleeMiss(CreatureBase attacker, AttackType type)
        {
            base.MeleeMiss(attacker, type);
            if (Script != null)
                Script.OnMeleeMiss(attacker);
            SendPacketToVisible(new SCombatPerformedMeleeAttackPacket { AttackerGUID = attacker.GUID, VictimGUID = GUID, AttackType = type, Result = AttackResult.Miss });
            SpellBook.Interrupt(InterruptMask.BeingMissedInMelee);
        }
        public override void SpellHit(CreatureBase caster, SpellBase spell)
        {
            base.SpellHit(caster, spell);
            if (Script != null)
                Script.OnSpellHit(caster, spell.Template as SpellTemplate);
            if (caster.Type.IsPlayer())
                (caster as PlayerCreature).ProcessTargetSpellHit(this, spell as Spell, AttackResult.Hit);
            else
                (caster as Creature).ProcessTargetSpellHit(this, spell as Spell, AttackResult.Hit);
            SendPacketToVisible(new SCombatPerformedSpellAttackPacket { CasterGUID = caster.GUID, TargetGUID = GUID, SpellID = spell.Template.ID, Result = AttackResult.Hit });
            SpellBook.Interrupt(InterruptMask.BeingHitBySpell);
        }
        public override void SpellMiss(CreatureBase caster, SpellBase spell)
        {
            base.SpellMiss(caster, spell);
            if (Script != null)
                Script.OnSpellMiss(caster, spell.Template as SpellTemplate);
            if (caster.Type.IsPlayer())
                (caster as PlayerCreature).ProcessTargetSpellHit(this, spell as Spell, AttackResult.Miss);
            else
                (caster as Creature).ProcessTargetSpellHit(this, spell as Spell, AttackResult.Miss);
            SendPacketToVisible(new SCombatPerformedSpellAttackPacket { CasterGUID = caster.GUID, TargetGUID = GUID, SpellID = spell.Template.ID, Result = AttackResult.Miss });
            SpellBook.Interrupt(InterruptMask.BeingMissedBySpell);
        }
        public override void SpellCasted(SpellTemplateBase spell, CreatureBase target)
        {
            base.SpellCasted(spell, target);
            if (Script != null)
                Script.OnSpellCasted(spell as SpellTemplate, target);
            SendPacketToVisible(new SCombatSpellCastedPacket { CasterGUID = GUID, TargetGUID = target.GUID, SpellID = spell.ID });
            SpellBook.Interrupt(InterruptMask.CastingSpell);
            Character.QuestLog.CreditCast(spell, target);
        }
        public override void SpellCasted(SpellTemplateBase spell, Vector3 target)
        {
            base.SpellCasted(spell, target);
            if (Script != null)
                Script.OnSpellCasted(spell as SpellTemplate, target);
            SendPacketToVisible(new SCombatSpellCastedPacket { CasterGUID = GUID, GroundTarget = target, SpellID = spell.ID });
            SpellBook.Interrupt(InterruptMask.CastingSpell);
            Character.QuestLog.CreditCast(spell, null);
        }
        public override void SpellCastingStarted(SpellTemplateBase spell, CreatureBase target)
        {
            base.SpellCastingStarted(spell, target);
            if (Script != null)
                Script.OnSpellCastingStarted(spell as SpellTemplate, target);
            SendPacketToVisible(new SCombatSpellCastingStartedPacket { CasterGUID = GUID, TargetGUID = target.GUID, SpellID = spell.ID });
            SpellBook.Interrupt(InterruptMask.StartingCastingSpell);
        }
        public override void SpellCastingStarted(SpellTemplateBase spell, Vector3 target)
        {
            base.SpellCastingStarted(spell, target);
            if (Script != null)
                Script.OnSpellCastingStarted(spell as SpellTemplate, target);
            SendPacketToVisible(new SCombatSpellCastingStartedPacket { CasterGUID = GUID, GroundTarget = target, SpellID = spell.ID });
            SpellBook.Interrupt(InterruptMask.StartingCastingSpell);
        }
        public override void SpellCastingFailed(SpellTemplateBase spell, CreatureBase target, CastSpellResult result)
        {
            base.SpellCastingFailed(spell, target, result);
            if (Script != null)
                Script.OnSpellCastingFailed(spell as SpellTemplate, target, result);
            SendPacketToVisible(new SCombatSpellCastingFailedPacket { CasterGUID = GUID, TargetGUID = target.GUID, SpellID = spell.ID, Result = result });
            SpellBook.Interrupt(InterruptMask.FailingCastingSpell);
        }
        public override void SpellCastingFailed(SpellTemplateBase spell, Vector3 target, CastSpellResult result)
        {
            base.SpellCastingFailed(spell, target, result);
            if (Script != null)
                Script.OnSpellCastingFailed(spell as SpellTemplate, target, result);
            SendPacketToVisible(new SCombatSpellCastingFailedPacket { CasterGUID = GUID, GroundTarget = target, SpellID = spell.ID, Result = result });
            SpellBook.Interrupt(InterruptMask.FailingCastingSpell);
        }
        public override void AuraApplied(AuraBase aura)
        {
            base.AuraApplied(aura);
            if (Script != null)
                Script.OnAuraApplied(aura as Aura);
            SendPacketToVisible(new SAppliedAuraPacket(aura));
        }
        public override void AuraRemoved(AuraBase aura)
        {
            base.AuraRemoved(aura);
            if (Script != null)
                Script.OnAuraRemoved(aura as Aura);
            SendPacketToVisible(new SRemovedAuraPacket(aura));
        }

        // Setters
        public override void SetHealth(int health)
        {
            base.SetHealth(health);
            SendPacketToVisible(new SCreatureHealthUpdatePacket { GUID = GUID, CurrentHealth = CurrentHealth, MaxHealth = MaxHealth });
        }
        public override void SetHealth(int currentHealth, int maxHealth)
        {
            base.SetHealth(currentHealth, maxHealth);
            SendPacketToVisible(new SCreatureHealthUpdatePacket { GUID = GUID, CurrentHealth = CurrentHealth, MaxHealth = MaxHealth });
        }
        public override void SetUsesPower(PowerType type, bool uses)
        {
            base.SetUsesPower(type, uses);
            SendPacketToVisible(new SCreatureUsesPowerUpdatePacket { GUID = GUID, PowerType = type, Uses = UsesPowerType[(byte)type] });
        }
        public override void SetPower(PowerType type, int power)
        {
            base.SetPower(type, power);
            SendPacketToVisible(new SCreaturePowerUpdatePacket { GUID = GUID, PowerType = type, CurrentPower = CurrentPower[(byte)type], MaxPower = MaxPower[(byte)type] });
        }
        public override void SetPower(PowerType type, int currentPower, int maxPower)
        {
            base.SetPower(type, currentPower, maxPower);
            SendPacketToVisible(new SCreaturePowerUpdatePacket { GUID = GUID, PowerType = type, CurrentPower = CurrentPower[(byte)type], MaxPower = MaxPower[(byte)type] });
        }
        public override void SetState(CreatureState state)
        {
            base.SetState(state);
            SendPacketToVisible(new SObjectUpdateLocationPacket() { GUID = GUID, Position = Position, Rotation = Rotation });
            SendPacketToVisible(new SCreatureStateUpdatePacket { GUID = GUID, State = State });
        }
        public override void SetTarget(CreatureBase target)
        {
            if (Target == target)
                return;

            base.SetTarget(target);
            SendPacketToVisible(new SCreatureTargetUpdatePacket { GUID = GUID, TargetGUID = Target == null ? 0 : Target.GUID });
        }
        public override void SetMovementType(MovementType type)
        {
            base.SetMovementType(type);
            SendPacketToVisible(new SCreatureMovementTypeUpdatePacket { GUID = GUID, MovementType = CurrentMovementType, MovementSpeedFactor = MovementSpeedFactor[(byte)CurrentMovementType] });
        }
        public override void SetMovementSpeedFactor(MovementType type, float factor)
        {
            base.SetMovementSpeedFactor(type, factor);
            SendPacketToVisible(new SCreatureMovementTypeUpdatePacket { GUID = GUID, MovementType = CurrentMovementType, MovementSpeedFactor = MovementSpeedFactor[(byte)CurrentMovementType] });
        }

        // Overrides, implementations and utility
        public override void Respawn()
        {
            base.Respawn();
            if (Script != null)
                Script.OnRespawn();
        }
        public override void Despawn()
        {
            if (Script != null)
                Script.OnDespawn();
            base.Despawn();
        }

        public override void MoveTo(Vector3 pos)
        {
            if (Position == pos)
                return;
            base.MoveTo(pos);

            Map map = Map as Map;
            if (map != null)
            {
                float distance = map.VisibilityDistance;

                // Remove objects that exited the visibility range
                int count = VisibleObjects.Count;
                for (int i = 0; i < count; i++)
                {
                    WorldObjectBase wo = VisibleObjects[i];
                    if (DistanceTo2D(wo) > distance)
                    {
                        RemoveVisibleObject(i--);
                        --count;
                    }
                }

                // Add objects that entered the visibility range
                foreach (WorldObjectBase wo in map.GetVisibleObjects(Position))
                {
                    if (wo == this)
                        continue;
                    AddVisibleObject(wo);
                }
            }

            SpellBook.Interrupt(InterruptMask.Moving);
        }
        public override void RotateTo(Rotation rot)
        {
            if (Rotation == rot)
                return;
            base.RotateTo(rot);
            SpellBook.Interrupt(InterruptMask.Rotating);
        }

        public override void SetFlag(ObjectFlags flag, bool set)
        {
            base.SetFlag(flag, set);
            SendPacketToVisible(new SObjectFlagsUpdatePacket { GUID = GUID, Flags = Flags });
        }

        public override void OnPositionChanged()
        {
            this.UpdateVisibility(VisibleTo, VisibilityUpdateReason.Move);

            if (LootingObject != null && DistanceTo(LootingObject) > MapTemplateBase.INTERACT_DISTANCE)
            {
                SendPacketToSelf(new SCancelLootingPacket { GUID = LootingObject.GUID });
                Loot loot = LootingObject.Loot as Loot;
                loot.RemoveLooter(this);
                UnsetFlag(ObjectFlags.Looting);
                LootingObject = null;
            }

            if (currentDialogTalker != null && DistanceTo(currentDialogTalker as WorldObjectBase) > MapTemplateBase.INTERACT_DISTANCE)
                EndDialog(DialogEndReason.TooFar);
        }
        public override void OnRotationChanged()
        {
        }
        public override void OnAddedToMap()
        {
            if (!isDespawned)
                this.UpdateVisibility(VisibleTo, VisibilityUpdateReason.AddToMap);
        }
        public override void OnRemovedFromMap()
        {
            this.UpdateVisibility(VisibleTo, VisibilityUpdateReason.RemoveFromMap);
        }

        public override Reaction GetReactionTo(CreatureBase creature)
        {
            if (creature == this)
                return Reaction.Friendly;

            if (Duel != null)
            {
                Reaction? duelReaction = Duel.GetReactionTo(creature);
                if (duelReaction.HasValue)
                    return duelReaction.Value;
            }

            if (ThreatManager.GetThreat(creature) > 0)
                return Reaction.Hostile;

            return base.GetReactionTo(creature);
        }
        public override Reaction GetReactionFrom(CreatureBase creature)
        {
            if (creature == this)
                return Reaction.Friendly;

            if (Duel != null)
            {
                Reaction? duelReaction = Duel.GetReactionFrom(creature);
                if (duelReaction.HasValue)
                    return duelReaction.Value;
            }

            if (creature.Type.IsPlayer())
            {
                if ((creature as PlayerCreature).ThreatManager.GetThreat(this) > 0)
                    return Reaction.Hostile;
            }
            else if (creature.Type.IsCreature())
            {
                if ((creature as Creature).ThreatManager.GetThreat(this) > 0)
                    return Reaction.Hostile;
            }

            return base.GetReactionFrom(creature);
        }

        public void AddVisibleTo(PlayerCreatureBase pc)
        {
            if (pc == this)
                return;
            if (VisibleTo.Contains(pc))
                return;
            VisibleTo.Add(pc);
        }
        public void RemoveVisibleTo(PlayerCreatureBase pc)
        {
            VisibleTo.Remove(pc);
        }

        public void AddVisibleObject(WorldObjectBase wo)
        {
            if (wo == this)
                return;
            if (VisibleObjects.Contains(wo))
                return;
            VisibleObjects.Add(wo);
            (wo as IVisibleToPlayers).AddVisibleTo(this);

            SendPacketToSelf(new SObjectEnteredVisibilityPacket
            {
                GUID = wo.GUID,
                ID = wo.Type.IsPlayer() ? (wo as PlayerCreatureBase).Character.ID : wo.Template.ID,
                Type = wo.Type,
                Position = wo.Position,
                Rotation = wo.Rotation,
                Size = wo.Size,
                Shape = wo.Shape,
            });
            SendInitialDataAbout(wo);
            if (wo.Type.IsCreature())
                SendInitialDataAbout(wo as CreatureBase);
            if (wo.Type.IsPlayer())
                SendInitialDataAbout(wo as PlayerCreatureBase);
            SendPacketToSelf(new SObjectLoadingFinalizedPacket { GUID = wo.GUID });
        }
        public void RemoveVisibleObject(int index)
        {
            WorldObjectBase wo = VisibleObjects[index];
            if (Character.Group != null && wo.Type.IsPlayer())
                if (Character.Group.IsInGroup(wo as PlayerCreature))
                    return;
            VisibleObjects.RemoveAt(index);
            if (wo.Type.IsPlayer())
                RemoveVisibleTo(wo as PlayerCreature);
            (wo as IVisibleToPlayers).RemoveVisibleTo(this);

            Player.Session.SendPacket(new SObjectLeftVisibilityPacket { GUID = wo.GUID });
        }
        public void RemoveVisibleObject(WorldObjectBase wo)
        {
            if (Character.Group != null && wo.Type.IsPlayer())
                if (Character.Group.IsInGroup(wo as PlayerCreature))
                    return;
            if (!VisibleObjects.Remove(wo))
                return;
            if (wo.Type.IsPlayer())
                RemoveVisibleTo(wo as PlayerCreature);
            (wo as IVisibleToPlayers).RemoveVisibleTo(this);

            Player.Session.SendPacket(new SObjectLeftVisibilityPacket { GUID = wo.GUID });
        }
        public void SendPacketToVisible(IWorldPacket packet)
        {
            SendPacketToSelf(packet);
            SendPacketToVisibleExceptSelf(packet);
        }
        public void SendPacketToVisible(IWorldPacket packet, byte[] rawData)
        {
            SendPacketToSelf(packet, rawData);
            SendPacketToVisibleExceptSelf(packet, rawData);
        }
        public void SendPacketToSelf(IWorldPacket packet)
        {
            Player.Session.SendPacket(packet);
        }
        public void SendPacketToSelf(IWorldPacket packet, byte[] rawData)
        {
            Player.Session.SendPacket(packet, rawData);
        }
        public void SendPacketToVisibleExceptSelf(IWorldPacket packet)
        {
            int count = VisibleTo.Count;
            for (int i = 0; i < count; i++)
                (VisibleTo[i] as PlayerCreature).Player.Session.SendPacket(packet);
        }
        public void SendPacketToVisibleExceptSelf(IWorldPacket packet, byte[] rawData)
        {
            int count = VisibleTo.Count;
            for (int i = 0; i < count; i++)
                (VisibleTo[i] as PlayerCreature).Player.Session.SendPacket(packet, rawData);
        }

        public void SendInitialDataAbout(PlayerCreatureBase pc)
        {
            
        }
        public void SendInitialDataAbout(CreatureBase creature)
        {
            SendPacketToSelf(new SLevelUpPacket { GUID = creature.GUID, Level = creature.Level });
            SendPacketToSelf(new SCreatureStateUpdatePacket { GUID = creature.GUID, State = creature.State });
            SendPacketToSelf(new SCreatureHealthUpdatePacket { GUID = creature.GUID, CurrentHealth = creature.CurrentHealth, MaxHealth = creature.MaxHealth });
            for (PowerType type = 0; type < PowerType.Max; type++)
                if (creature.UsesPowerType[(byte)type])
                {
                    SendPacketToSelf(new SCreatureUsesPowerUpdatePacket { GUID = creature.GUID, PowerType = type, Uses = true });
                    SendPacketToSelf(new SCreaturePowerUpdatePacket { GUID = creature.GUID, PowerType = type, CurrentPower = creature.CurrentPower[(byte)type], MaxPower = creature.MaxPower[(byte)type] });
                }
            if (!creature.Mover.Finished)
                SendPacketToSelf((creature.Mover as Mover).BuildPacket());
            foreach (Aura aura in creature.SpellBook.GetAuras())
                SendPacketToSelf(new SHasAuraPacket(aura));
        }
        public void SendInitialDataAbout(WorldObjectBase wo)
        {
            SendPacketToSelf(new SObjectFlagsUpdatePacket { GUID = wo.GUID, Flags = wo.Flags });
        }
    }
}