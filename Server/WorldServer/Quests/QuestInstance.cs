﻿using System;
using Arcanos.Server.WorldServer.Data;
using Arcanos.Shared.Quests;

namespace Arcanos.Server.WorldServer.Quests
{
    public class QuestInstance : QuestInstanceBase
    {
        protected QuestInstance() { }
        public QuestInstance(Quest quest) : base(quest) { }

        public override void Deserialize(string member, byte[] data)
        {
            if (member == "Quest")
                Quest = DatabaseManager.Quests[BitConverter.ToInt32(data, 0)];
        }
    }
}