﻿using System.Collections.Generic;
using Arcanos.Server.WorldServer.Data;
using Arcanos.Shared.Data;
using Arcanos.Shared.Quests;

namespace Arcanos.Server.WorldServer.Quests
{
    [SerializationSettings(Method = SerializationMethod.RefByID, ReferenceResolverType = typeof(DatabaseManager), ReferenceResolverMethod = "GetQuest")]
    public class Quest : QuestBase
    {
        public List<Quest> RequiresQuestsCompleted = new List<Quest>();

        protected override int GetInitialID()
        {
            return DatabaseManager.Quests.GetID(this);
        }

        public override QuestInstanceBase Create()
        {
            return new QuestInstance(this);
        }
    }
}