﻿using System.Collections.Generic;
using Arcanos.Server.WorldServer.Data;
using Arcanos.Server.WorldServer.Players;
using Arcanos.Shared.Data;
using Arcanos.Shared.Dialog;
using Arcanos.Shared.Items;
using Arcanos.Shared.Net;
using Arcanos.Shared.Players;
using Arcanos.Shared.Quests;

namespace Arcanos.Server.WorldServer.Quests
{
    public class QuestLog : QuestLogBase
    {
        [SerializationIgnore]
        public new Character Owner
        {
            get { return base.Owner as Character; }
            set { base.Owner = value; }
        }

        public readonly List<Quest> RewardedQuests = new List<Quest>();
        
        [SerializationIgnore]
        public readonly List<Quest> ReportedAsRewarded = new List<Quest>();

        [SerializationIgnore]
        private bool initialCredit = false;

        protected QuestLog() { }
        public QuestLog(CharacterBase owner) : base(owner) { }

        public bool IsQuestRewarded(QuestBase quest)
        {
            return RewardedQuests.Contains(quest as Quest);
        }
        public bool CanAcceptQuest(Quest quest)
        {
            return !HasQuest(quest) && !RewardedQuests.Contains(quest);
        }
        public bool CanCompleteQuest(Quest quest)
        {
            QuestInstanceBase instance = GetInstanceForQuest(quest);
            return instance != null && instance.IsCompleted();
        }
        public bool CanBeRewardedForQuest(Quest quest)
        {
            return !IsQuestRewarded(quest) && Owner.Inventory.CanStoreItem(quest.RewardItems) == ItemTakeResult.Ok;
        }

        public override QuestInstanceBase AddQuest(QuestBase quest)
        {
            if (HasQuest(quest))
            {
                SendPacketToOwner(new SAddQuestResultPacket { ID = quest.ID, Result = QuestAcceptResult.AlreadyHaveQuest });
                return null;
            }

            QuestInstanceBase instance = base.AddQuest(quest);
            SendPacketToOwner(new SAddQuestResultPacket { ID = quest.ID, Result = QuestAcceptResult.Ok });
            SendPacketToOwner(new SQuestAddedPacket { ID = quest.ID, Completed = instance.IsCompleted() });

            initialCredit = true;
            foreach (QuestObjective objective in quest.Objectives)
            {
                switch (objective.Type)
                {
                    case QuestObjectiveType.Item:
                        instance.Credit(this, QuestObjectiveType.Item, objective.ID, 0, Owner.Inventory.GetItemCount(DatabaseManager.Items[objective.ID]));
                        break;
                    case QuestObjectiveType.Kill:
                    case QuestObjectiveType.Use:
                    case QuestObjectiveType.Cast:
                        break;
                }
            }
            initialCredit = false;

            return instance;
        }
        public override void RemoveQuest(QuestBase quest)
        {
            if (!HasQuest(quest))
            {
                SendPacketToOwner(new SRemoveQuestResultPacket { ID = quest.ID, Result = QuestRemoveResult.QuestNotPresent });
                return;
            }

            base.RemoveQuest(quest);
            SendPacketToOwner(new SRemoveQuestResultPacket { ID = quest.ID, Result = QuestRemoveResult.Ok });
            SendPacketToOwner(new SQuestRemovedPacket { ID = quest.ID });
        }
        public void RewardQuest(Quest quest)
        {
            foreach (QuestObjective objective in quest.Objectives)
                if (objective.Type == QuestObjectiveType.Item)
                    Owner.Inventory.RemoveItem(DatabaseManager.Items[objective.ID], objective.Required);
            Owner.AddMoney(quest.RewardMoney);
            Owner.AddExperience(quest.RewardExperience);
            Owner.Inventory.AddItem(quest.RewardItems);
            RewardedQuests.Add(quest);
        }

        public override void CreditedCallback(QuestInstanceBase instance, byte objectiveIndex)
        {
            base.CreditedCallback(instance, objectiveIndex);
            SendPacketToOwner(new SQuestObjectiveUpdatePacket
            {
                QuestID = instance.Quest.ID,
                ObjectiveIndex = objectiveIndex,
                CurrentProgress = instance.ObjectiveProgress[objectiveIndex],
                Completed = instance.GetObjectiveRemainder(objectiveIndex) == 0,
                Initial = initialCredit,
            });
            if (instance.IsCompleted())
            {
                if (!ReportedAsRewarded.Contains(instance.Quest as Quest))
                {
                    ReportedAsRewarded.Add(instance.Quest as Quest);
                    SendPacketToOwner(new SQuestCompletedPacket
                    {
                        ID = instance.Quest.ID,
                        Initial = initialCredit,
                    });
                }
            }
            else
                ReportedAsRewarded.Remove(instance.Quest as Quest);
        }

        public IList<Quest> FilterQuests(IEnumerable<Quest> quests)
        {
            List<Quest> result = new List<Quest>(quests);
            for (int i = 0; i < result.Count; i++)
            {
                bool remove = false;
                Quest quest = result[i];
                if (RewardedQuests.Contains(quest))
                    remove = true;
                if (!remove)
                    foreach (Quest preQuest in quest.RequiresQuestsCompleted)
                    {
                        if (!RewardedQuests.Contains(preQuest))
                        {
                            remove = true;
                            break;
                        }
                    }
                
                if (remove)
                    result.RemoveAt(i--);
            }
            return result;
        }

        public void SendInitialData()
        {
            for (int i = 0; i < Quests.Count; i++)
            {
                QuestInstance instance = Quests[i] as QuestInstance;
                SendPacketToOwner(new SQuestAddedPacket { Initial = true, ID = instance.Quest.ID, Completed = instance.IsCompleted() });
                for (byte j = 0; j < instance.ObjectiveProgress.Length; j++)
                {
                    SendPacketToOwner(new SQuestObjectiveUpdatePacket
                    {
                        Initial = true,
                        QuestID = instance.Quest.ID,
                        ObjectiveIndex = j,
                        CurrentProgress = instance.ObjectiveProgress[j],
                        Completed = instance.GetObjectiveRemainder(j) == 0,
                    });
                }
                if (instance.IsCompleted())
                    SendPacketToOwner(new SQuestCompletedPacket { Initial = true, ID = instance.Quest.ID });
            }
        }

        private void SendPacketToOwner(IWorldPacket packet)
        {
            Owner.Player.Session.SendPacket(packet);
        }
        public DialogQuestOptionState GetDialogQuestOptionStateForQuest(Quest quest)
        {
            if (!HasQuest(quest))
                return CanAcceptQuest(quest) ? DialogQuestOptionState.Acceptable : DialogQuestOptionState.NotAcceptable;
            if (CanCompleteQuest(quest))
                return DialogQuestOptionState.Completed;
            return DialogQuestOptionState.NotCompleted;
        }
    }
}