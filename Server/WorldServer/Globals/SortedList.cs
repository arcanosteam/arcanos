﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Arcanos.Server.WorldServer
{
    public class SortedList<T> : ICollection<T> where T : IComparable<T>
    {
        private readonly List<T> items;
        private int count = 0;

        public int Count
        {
            get { return count; }
        }
        public bool IsReadOnly { get { return false; } }

        public T this[int i]
        {
            get { return items[i]; }
            set { items[i] = value; }
        }

        public SortedList()
        {
            items = new List<T>();
        }
        public SortedList(int capacity)
        {
            items = new List<T>(capacity);
        }
        public SortedList(IEnumerable<T> collection)
        {
            items = new List<T>(collection);
            count = items.Count;
        }

        public void Add(T item)
        {
            for (int i = 0; i < count; ++i)
                if (items[i].CompareTo(item) < 0)
                {
                    items.Insert(i, item);
                    ++count;
                    return;
                }
            items.Add(item);
            ++count;
        }
        public bool Remove(T item)
        {
            if (items.Remove(item))
            {
                --count;
                return true;
            }
            return false;
        }
        public void RemoveAt(int index)
        {
            if (index < 0 || index >= count)
                throw new IndexOutOfRangeException("index");

            items.RemoveAt(index);
            --count;
        }

        public void Clear()
        {
            items.Clear();
            count = 0;
        }
        public void CopyTo(T[] array, int arrayIndex)
        {
            items.CopyTo(array, arrayIndex);
        }

        public bool Contains(T item)
        {
            return items.Contains(item);
        }
        public int IndexOf(T item)
        {
            return items.IndexOf(item);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return items.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return items.GetEnumerator();
        }
    }
    public class CustomSortedList<T> : ICollection<T>
    {
        public readonly IComparer<T> Comparer;
        private readonly List<T> items;
        private int count = 0;

        public int Count
        {
            get { return count; }
        }
        public bool IsReadOnly { get { return false; } }

        public T this[int i]
        {
            get { return items[i]; }
        }

        public CustomSortedList(IComparer<T> comparer)
        {
            Comparer = comparer;
            items = new List<T>();
        }
        public CustomSortedList(IComparer<T> comparer, int capacity)
        {
            Comparer = comparer;
            items = new List<T>(capacity);
        }
        public CustomSortedList(IComparer<T> comparer, IEnumerable<T> collection)
        {
            Comparer = comparer;
            items = new List<T>(collection);
            count = items.Count;
        }

        public void Add(T item)
        {
            for (int i = 0; i < count; ++i)
                if (Comparer.Compare(items[i], item) < 0)
                {
                    items.Insert(i, item);
                    ++count;
                    return;
                }
            items.Add(item);
            ++count;
        }
        public bool Remove(T item)
        {
            if (items.Remove(item))
            {
                --count;
                return true;
            }
            return false;
        }
        public void RemoveAt(int index)
        {
            if (index < 0 || index >= count)
                throw new IndexOutOfRangeException("index");

            items.RemoveAt(index);
            --count;
        }

        public void Clear()
        {
            items.Clear();
            count = 0;
        }
        public void CopyTo(T[] array, int arrayIndex)
        {
            items.CopyTo(array, arrayIndex);
        }

        public bool Contains(T item)
        {
            return items.Contains(item);
        }
        public int IndexOf(T item)
        {
            return items.IndexOf(item);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return items.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return items.GetEnumerator();
        }
    }
}