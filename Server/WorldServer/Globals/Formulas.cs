﻿using System;
using System.Collections.Generic;
using Arcanos.Server.WorldServer.Objects;
using Arcanos.Server.WorldServer.Players;
using Arcanos.Server.WorldServer.Spells;
using Arcanos.Shared;
using Arcanos.Shared.Items;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Players;
using Arcanos.Shared.Spells;

namespace Arcanos.Server.WorldServer
{
    public static class Formulas
    {
        // Combat
        public static Damage CalculateDamage(CreatureBase attacker, CreatureBase victim, DamageBounds damage)
        {
            // Resistance reduction
            for (DamageSchool school = DamageSchool.Generic + 1; school < DamageSchool.Max; school++)
                damage.SubRaw(school, GetSchoolResistance(victim, school));

            // Auras modification
            foreach (Aura aura in attacker.SpellBook.GetAuras(AuraType.ModDamageDoneRaw))
                if (aura.Effect.IntValue1 == 0)
                    damage.AddRaw(new IntBounds(aura.Effect.IntValue2, aura.Effect.IntValue3 == 0 ? aura.Effect.IntValue2 : aura.Effect.IntValue3));
                else
                    damage.AddRaw(aura.Effect.IntValue1, new IntBounds(aura.Effect.IntValue2, aura.Effect.IntValue3 == 0 ? aura.Effect.IntValue2 : aura.Effect.IntValue3));
            foreach (Aura aura in victim.SpellBook.GetAuras(AuraType.ModDamageTakenRaw))
                if (aura.Effect.IntValue1 == 0)
                    damage.AddRaw(new IntBounds(aura.Effect.IntValue2, aura.Effect.IntValue3 == 0 ? aura.Effect.IntValue2 : aura.Effect.IntValue3));
                else
                    damage.AddRaw(aura.Effect.IntValue1, new IntBounds(aura.Effect.IntValue2, aura.Effect.IntValue3 == 0 ? aura.Effect.IntValue2 : aura.Effect.IntValue3));
            foreach (Aura aura in attacker.SpellBook.GetAuras(AuraType.ModDamageDonePct))
                if (aura.Effect.IntValue1 == 0)
                    damage.AddPct(aura.Effect.FloatValue1);
                else
                    damage.AddPct(aura.Effect.IntValue1, aura.Effect.FloatValue1);
            foreach (Aura aura in victim.SpellBook.GetAuras(AuraType.ModDamageTakenPct))
                if (aura.Effect.IntValue1 == 0)
                    damage.AddPct(aura.Effect.FloatValue1);
                else
                    damage.AddPct(aura.Effect.IntValue1, aura.Effect.FloatValue1);

            return damage.Random;
        }
        public static int GetSchoolResistance(CreatureBase creature, DamageSchool school)
        {
            int resistance;

            // Only players have resistance from stats
            if (creature.Type.IsPlayer())
            {
                PlayerCreature player = creature as PlayerCreature;
                switch (school)
                {
                    case DamageSchool.Generic:
                        resistance = 0;
                        break;
                    case DamageSchool.Physical:
                        resistance = player.Character.Stats[Stat.PhysicalResistance] + R.IntInclusive(player.Character.Stats[Stat.Armor]);
                        break;
                    case DamageSchool.Fire:
                        resistance = player.Character.Stats[Stat.MagicResistance] + player.Character.Stats[Stat.FireResistance];
                        break;
                    case DamageSchool.Water:
                        resistance = player.Character.Stats[Stat.MagicResistance] + player.Character.Stats[Stat.WaterResistance];
                        break;
                    case DamageSchool.Ground:
                        resistance = player.Character.Stats[Stat.MagicResistance] + player.Character.Stats[Stat.GroundResistance];
                        break;
                    case DamageSchool.Air:
                        resistance = player.Character.Stats[Stat.MagicResistance] + player.Character.Stats[Stat.AirResistance];
                        break;
                    case DamageSchool.Light:
                        resistance = player.Character.Stats[Stat.MagicResistance] + player.Character.Stats[Stat.LightResistance];
                        break;
                    case DamageSchool.Dark:
                        resistance = player.Character.Stats[Stat.MagicResistance] + player.Character.Stats[Stat.DarkResistance];
                        break;
                    default:
                        throw new ArgumentOutOfRangeException("school");
                }
            }
            else
                resistance = 0;

            // TODO: Modify resistance by auras

            // TODO: Modify resistance by creature family

            return resistance;
        }
        public static AttackResult CalculateAttackResult(CreatureBase attacker, CreatureBase victim, AttackType type)
        {
            foreach (AuraBase aura in victim.SpellBook.GetAuras(AuraType.Evasion))
                if (aura.Effect.FloatValue1 == 1 || R.Float() >= aura.Effect.FloatValue1)
                    return AttackResult.Miss;

            return AttackResult.Hit;
        }
        public static void ScaleDamage(ref DamageBounds damage, CreatureBase caster, CreatureBase target, ref SpellEffect effect)
        {
            // Level scaling
            if (effect.FloatValue1 != 0)
                damage.AddRaw(new IntBounds((int)(effect.FloatValue1 * caster.Level)));

            // Stat scaling
            if (effect.FloatValue2 != 0 && caster.Type.IsPlayer())
            {
                Character character = (caster as PlayerCreature).Character;
                switch (character.Class)
                {
                    case PlayerClass.Warrior:
                    case PlayerClass.Rogue:
                        damage.AddRaw(new IntBounds((int)(effect.FloatValue2 * character.Stats[Stat.MeleePower])));
                        break;
                    case PlayerClass.Mage:
                    case PlayerClass.Cultist:
                        damage.AddRaw(new IntBounds((int)(effect.FloatValue2 * character.Stats[Stat.MagicPower])));
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
        public static void ScaleHeal(ref FloatBounds heal, CreatureBase caster, CreatureBase target, ref SpellEffect effect)
        {
            // Level scaling
            if (effect.FloatValue1 != 0)
                heal += effect.FloatValue1 * caster.Level;
            
            // Stat scaling
            if (effect.FloatValue2 != 0 && caster.Type.IsPlayer())
            {
                Character character = (caster as PlayerCreature).Character;
                switch (character.Class)
                {
                    case PlayerClass.Mage:
                    case PlayerClass.Cultist:
                        heal += effect.FloatValue2 * character.Stats[Stat.MagicPower];
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            // Aura scaling
            foreach (Aura aura in caster.SpellBook.GetAuras(AuraType.ModHealingDoneRaw))
                heal += aura.Effect.IntValue2 == 0 ? aura.Effect.IntValue1 : R.IntInclusive(aura.Effect.IntValue1, aura.Effect.IntValue2);
            foreach (Aura aura in target.SpellBook.GetAuras(AuraType.ModHealingTakenRaw))
                heal += aura.Effect.IntValue2 == 0 ? aura.Effect.IntValue1 : R.IntInclusive(aura.Effect.IntValue1, aura.Effect.IntValue2);
            foreach (Aura aura in caster.SpellBook.GetAuras(AuraType.ModHealingDonePct))
                heal *= 1 + aura.Effect.FloatValue1;
            foreach (Aura aura in target.SpellBook.GetAuras(AuraType.ModHealingTakenPct))
                heal *= 1 + aura.Effect.FloatValue1;
        }

        // Regeneration
        public static float CalcHealthRegenInterval(CreatureBase creature, bool inCombat)
        {
            if (creature.Type.IsPlayer() || !inCombat)
                return 1;
            return 0;
        }
        public static int CalcHealthRegen(CreatureBase creature, bool inCombat)
        {
            float regen;
            if (creature.Type.IsPlayer())
            {
                if (inCombat)
                    regen = (creature as PlayerCreature).Character.Stats[Stat.Spirit];
                else
                    regen = creature.MaxHealth / 10;
            }
            else if (inCombat)
                regen = 0;
            else
                regen = creature.MaxHealth / 10;

            foreach (Aura aura in creature.SpellBook.GetAuras(AuraType.ModHealthRegenRaw))
                regen += aura.Effect.IntValue2 == 0 ? aura.Effect.IntValue1 : R.IntInclusive(aura.Effect.IntValue1, aura.Effect.IntValue2);

            foreach (Aura aura in creature.SpellBook.GetAuras(AuraType.ModHealthRegenPct))
                regen *= 1 + aura.Effect.FloatValue1;

            return (int)regen;
        }
        public static float CalcPowerRegenInterval(PowerType power, CreatureBase creature, bool inCombat)
        {
            if (creature.Type.IsPlayer())
            {
                if (!CanUsePower((creature as PlayerCreature).Character, power))
                    return 0;
                return 1;
            }
            if (!inCombat)
                return 1;
            return 0;
        }
        public static int CalcPowerRegen(PowerType power, CreatureBase creature, bool inCombat)
        {
            float regen;
            if (creature.Type.IsPlayer())
            {
                if (!CanUsePower((creature as PlayerCreature).Character, power))
                    return 0;
                if (inCombat)
                    regen = (creature as PlayerCreature).Character.Stats[Stat.Spirit];
                else
                    regen = creature.MaxPower[(byte)power] / 10;
            }
            else if (inCombat)
                regen = 0;
            else
                regen = creature.MaxPower[(byte)power] / 10;

            foreach (Aura aura in creature.SpellBook.GetAuras(AuraType.ModPowerRegenRaw))
                if (aura.Effect.IntValue1 == (int)power)
                    regen += aura.Effect.IntValue3 == 0 ? aura.Effect.IntValue2 : R.IntInclusive(aura.Effect.IntValue2, aura.Effect.IntValue3);

            foreach (Aura aura in creature.SpellBook.GetAuras(AuraType.ModPowerRegenPct))
                if (aura.Effect.IntValue1 == (int)power)
                    regen *= 1 + aura.Effect.FloatValue1;

            return (int)regen;
        }

        // Experience
        public static int CalculateExperienceForKill(PlayerCreature killer, CreatureBase victim)
        {
            const int BASE_XP_PER_LEVEL = 100;

            float xp = victim.Level * BASE_XP_PER_LEVEL;

            // Level diminishing
            int levelDifference = victim.Level - killer.Level;
            if (levelDifference < 0)
            {
                if (levelDifference <= -5)
                    return 0;
                float mul = 1 - levelDifference / 5.0f;
                xp *= mul;
            }
            if (levelDifference > 0)
            {
                float mul = 1 + levelDifference / 5.0f;
                if (mul > 5)
                    mul = 5;
                xp *= mul;
            }

            // Classification multipliers
            ThreatManager threatManager = null;
            if (victim.Type.IsPlayer())
            {
                threatManager = (victim as PlayerCreature).ThreatManager;
            }
            else if (victim.Type.IsCreature())
            {
                if ((victim.Template.Classification & CreatureClassification.Elite) != 0)
                    xp *= 2;
                if ((victim.Template.Classification & CreatureClassification.Rare) != 0)
                    xp *= 1.5f;
                if ((victim.Template.Classification & CreatureClassification.Boss) != 0)
                    xp *= 3;

                threatManager = (victim as Creature).ThreatManager;
            }

            // Split between attackers
            int players = threatManager.GetAttackers(ObjectType.PlayerCreature).Count;
            if (players != 0)
                xp /= players;

            return (int)Math.Ceiling(xp);
        }
        public static int CalculateExperienceToNextLevel(int fromLevel)
        {
            return PowMul(fromLevel, 1.2, 1000) - (fromLevel - 1) * 1000;
        }

        // Default Creature Stats
        public static IntBounds GetDefaultDamageForLevel(IntBounds level)
        {
            const float LEVEL_MULTIPLIER = 25;

            return new IntBounds((int)(level.Min * LEVEL_MULTIPLIER), (int)(level.Max * LEVEL_MULTIPLIER));
        }
        public static IntBounds GetDefaultHealthForLevel(IntBounds level)
        {
            const float LEVEL_MULTIPLIER = 500;

            return new IntBounds((int)(level.Min * LEVEL_MULTIPLIER), (int)(level.Max * LEVEL_MULTIPLIER));
        }
        public static void GetDefaultPowerForLevel(IntBounds level, CreatureCombatType type, ref IntBounds[] power, ref bool[] usesPowerType)
        {
            PowerType powerType;
            float mul;
            switch (type)
            {
                case CreatureCombatType.Melee:
                    return;
                case CreatureCombatType.MeleeEnergy:
                    powerType = PowerType.Energy;
                    mul = 50;
                    break;
                case CreatureCombatType.CasterMana:
                    powerType = PowerType.Mana;
                    mul = 100;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("type");
            }

            usesPowerType[(byte)powerType] = true;
            power[(byte)powerType] = new IntBounds((int)(level.Min * mul), (int)(level.Max * mul));
        }
        public static int GetAnswerToTheUltimateQuestionOfLifeTheUniverseAndEverything()
        {
            return 42;
        }

        // Player Creature Stats
        public static DamageBounds CalculatePlayerAttackPower(PlayerCreature pc, AttackType type)
        {
            return CalculatePlayerAttackPower(pc, type, pc.Character.Equipment.Items[(byte)EquipSlot.MainHand], pc.Character.Equipment.Items[(byte)EquipSlot.OffHand]);
        }
        public static FloatBounds CalculatePlayerAttackInterval(PlayerCreature pc, AttackType type)
        {
            return CalculatePlayerAttackInterval(pc, type, pc.Character.Equipment.Items[(byte)EquipSlot.MainHand], pc.Character.Equipment.Items[(byte)EquipSlot.OffHand]);
        }
        public static DamageBounds CalculatePlayerAttackPower(PlayerCreature pc, AttackType type, ItemBase mainHandWeapon, ItemBase offHandWeapon)
        {
            ItemBase weapon = type == AttackType.MeleeMainHand ? mainHandWeapon : offHandWeapon;
            if (type == AttackType.MeleeOffHand && weapon == null)
                return new DamageBounds();

            DamageBounds damage = weapon == null ? new DamageBounds() : new DamageBounds() { Physical = weapon.Template.WeaponDamage };
            if (weapon != null && damage.Empty)
                return new DamageBounds();

            damage.Physical += pc.Character.Stats[Stat.MeleePower];

            foreach (Aura aura in pc.SpellBook.GetAuras(AuraType.ModWeaponDamage))
                if (aura.Effect.IntValue1 == 0 || ((1 << (byte)type) & aura.Effect.IntValue1) != 0)
                    damage.AddRaw(new IntBounds(aura.Effect.IntValue2));

            foreach (Aura aura in pc.SpellBook.GetAuras(AuraType.ModWeaponDamagePercent))
                if (aura.Effect.IntValue1 == 0 || ((1 << (byte)type) & aura.Effect.IntValue1) != 0)
                    damage.AddPct(aura.Effect.FloatValue1);

            return damage;
        }
        public static FloatBounds CalculatePlayerAttackInterval(PlayerCreature pc, AttackType type, ItemBase mainHandWeapon, ItemBase offHandWeapon)
        {
            ItemBase weapon = type == AttackType.MeleeMainHand ? mainHandWeapon : offHandWeapon;
            if (type == AttackType.MeleeOffHand && weapon == null)
                return new FloatBounds();

            FloatBounds interval = weapon == null ? new FloatBounds(2) : weapon.Template.WeaponSpeed;
            interval *= 1 / (1 + pc.Character.Stats[Stat.Swiftness] / 500f);

            foreach (Aura aura in pc.SpellBook.GetAuras(AuraType.ModWeaponSpeedPercent))
                if (aura.Effect.IntValue1 == 0 || ((1 << (byte)type) & aura.Effect.IntValue1) != 0)
                    interval *= 1 + aura.Effect.FloatValue1;

            return interval;
        }
        public static int GetHealthForPlayer(Character character)
        {
            return character.Stats[Stat.Stamina] * 10;
        }
        public static int GetPowerForPlayer(Character character, PowerType power)
        {
            if (!CanUsePower(character, power))
                return 0;
            switch (power)
            {
                case PowerType.Mana:
                    if (character.Class == PlayerClass.Mage || character.Class == PlayerClass.Cultist)
                        return character.Stats[Stat.Intellect] * 10;
                    return 0;
                case PowerType.Energy:
                    if (character.Class == PlayerClass.Warrior || character.Class == PlayerClass.Rogue)
                        return character.Stats[Stat.Swiftness] * 10;
                    return 0;
                default:
                    throw new ArgumentOutOfRangeException("power");
            }
        }
        public static bool CanUsePower(Character character, PowerType power)
        {
            switch (power)
            {
                case PowerType.Mana:
                    return character.Class == PlayerClass.Mage ||
                           character.Class == PlayerClass.Cultist;
                case PowerType.Energy:
                    return character.Class == PlayerClass.Warrior ||
                           character.Class == PlayerClass.Rogue;
                default:
                    throw new ArgumentOutOfRangeException("power");
            }
        }

        // Character Stats
        private class PlaceholderMap<TKey, TValue> : Dictionary<TKey, TValue>
        {
            public new TValue this[TKey key]
            {
                get
                {
                    TValue value;
                    if (TryGetValue(key, out value))
                        return value;
                    return default(TValue);
                }
            }
        }
        private static readonly Dictionary<PlayerRace, PlaceholderMap<Stat, float>> raceMultipliers = new Dictionary<PlayerRace, PlaceholderMap<Stat, float>>
        {
            {
                PlayerRace.Human,
                new PlaceholderMap<Stat, float>
                {
                    { Stat.MeleePower, +0.05f },
                    { Stat.MagicPower, -0.10f },
                    { Stat.Swiftness, -0.05f },
                    { Stat.Spirit, +0.05f },
                    { Stat.PhysicalResistance, +0.05f },
                }
            },
            {
                PlayerRace.WoodElf,
                new PlaceholderMap<Stat, float>
                {
                    { Stat.MeleePower, -0.05f },
                    { Stat.MagicPower, +0.05f },
                    { Stat.Swiftness, +0.05f },
                    { Stat.Stamina, -0.1f },
                    { Stat.Spirit, +0.05f },
                    { Stat.GroundResistance, +0.1f },
                    { Stat.FireResistance, -0.1f },
                }
            },
            {
                PlayerRace.Minotaur,
                new PlaceholderMap<Stat, float>
                {
                    { Stat.MeleePower, +0.1f },
                    { Stat.MagicPower, -0.2f },
                    { Stat.Swiftness, -0.1f },
                    { Stat.Stamina, +0.1f },
                    { Stat.Intellect, -0.1f },
                    { Stat.PhysicalResistance, +0.2f },
                }
            },
            {
                PlayerRace.Orc,
                new PlaceholderMap<Stat, float>
                {
                    { Stat.MeleePower, +0.2f },
                    { Stat.MagicPower, -0.15f },
                    { Stat.Stamina, +0.05f },
                    { Stat.Intellect, -0.1f },
                    { Stat.Spirit, -0.05f },
                    { Stat.FireResistance, +0.05f },
                }
            },
            {
                PlayerRace.Lizardman,
                new PlaceholderMap<Stat, float>
                {
                    { Stat.MeleePower, -0.05f },
                    { Stat.MagicPower, +0.05f },
                    { Stat.Swiftness, +0.1f },
                    { Stat.Stamina, -0.1f },
                    { Stat.AirResistance, -0.1f },
                    { Stat.WaterResistance, +0.1f },
                }
            },
        };
        private static int PowMul(double x, double pow, double mul)
        {
            return (int)(Math.Pow(x, pow) * mul);
        }
        public static void FillBaseStatsForLevel(Stats stats, PlayerRace playerRace, PlayerClass playerClass, int level)
        {
            for (byte i = 0; i < (byte)Stat.Max; i++)
                stats.BaseValues[i] = 0;

            switch (playerClass)
            {
                case PlayerClass.Warrior:
                    stats.BaseValues[(byte)Stat.MeleePower] = 10 + PowMul(level, 1.4, 5);
                    stats.BaseValues[(byte)Stat.Swiftness] = 5 + PowMul(level, 1.2, 5);
                    stats.BaseValues[(byte)Stat.Stamina] = 10 + PowMul(level, 1.5, 15);
                    stats.BaseValues[(byte)Stat.PhysicalResistance] = 8 + PowMul(level, 2, 0.6);
                    stats.BaseValues[(byte)Stat.Spirit] = 1 + PowMul(level, 1.35, 1);
                    break;
                case PlayerClass.Rogue:
                    stats.BaseValues[(byte)Stat.MeleePower] = 5 + PowMul(level, 1.3, 2);
                    stats.BaseValues[(byte)Stat.Swiftness] = 25 + PowMul(level, 1.4, 9);
                    stats.BaseValues[(byte)Stat.Stamina] = 7 + PowMul(level, 1.4, 12);
                    stats.BaseValues[(byte)Stat.PhysicalResistance] = 3 + PowMul(level, 2, 0.4);
                    stats.BaseValues[(byte)Stat.Spirit] = 1 + PowMul(level, 1.4, 1.05);
                    break;
                case PlayerClass.Mage:
                    stats.BaseValues[(byte)Stat.MeleePower] = 2 + PowMul(level, 1.1, 3);
                    stats.BaseValues[(byte)Stat.MagicPower] = 15 + PowMul(level, 1.5, 20);
                    stats.BaseValues[(byte)Stat.Stamina] = 5 + PowMul(level, 1.4, 10);
                    stats.BaseValues[(byte)Stat.Intellect] = 25 + PowMul(level, 1.6, 15);
                    stats.BaseValues[(byte)Stat.Spirit] = 1 + PowMul(level, 1.5, 1.25);
                    break;
                case PlayerClass.Cultist:
                    stats.BaseValues[(byte)Stat.MeleePower] = 2 + PowMul(level, 1.1, 4);
                    stats.BaseValues[(byte)Stat.MagicPower] = 10 + PowMul(level, 2.5, 1);
                    stats.BaseValues[(byte)Stat.Stamina] = 5 + PowMul(level, 1.4, 8);
                    stats.BaseValues[(byte)Stat.Intellect] = 35 + PowMul(level, 1.5, 13);
                    stats.BaseValues[(byte)Stat.MagicResistance] = 1 + PowMul(level, 2, 0.5);
                    stats.BaseValues[(byte)Stat.Spirit] = 1 + PowMul(level, 1.45, 1.05);
                    break;
            }

            // Applying race modifiers
            for (byte i = 0; i < (byte)Stat.Max; i++)
                stats.BaseValues[i] += (int)(stats.BaseValues[i] * raceMultipliers[playerRace][(Stat)i]);

            stats.RecalcModifiedValues();
        }
    }
}