﻿using Arcanos.Server.WorldServer.Data;
using Arcanos.Shared;

namespace Arcanos.Server.WorldServer
{
    public class Faction : FactionBase
    {
        protected Faction() { }
        public Faction(string name) : base(name) { }
        public Faction(string name, Reaction reactionToOthers) : base(name, reactionToOthers) { }

        protected override int GetInitialID()
        {
            return DatabaseManager.Factions.GetID(this);
        }

        public static void SetReaction(Faction a, Faction b, Reaction reaction)
        {
            a.Reactions[b.ID] = reaction;
            b.Reactions[a.ID] = reaction;
        }
        public static void SetReaction(int aID, int bID, Reaction reaction)
        {
            Faction a = DatabaseManager.Factions[aID];
            Faction b = DatabaseManager.Factions[bID];
            a.Reactions[b.ID] = reaction;
            b.Reactions[a.ID] = reaction;
        }
        public static void SetReactionTo(Faction a, Faction b, Reaction reaction)
        {
            a.Reactions[b.ID] = reaction;
        }
        public static void SetReactionTo(int aID, int bID, Reaction reaction)
        {
            Faction a = DatabaseManager.Factions[aID];
            Faction b = DatabaseManager.Factions[bID];
            a.Reactions[b.ID] = reaction;
        }
    }
}