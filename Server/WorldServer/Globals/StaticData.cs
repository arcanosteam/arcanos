﻿using System.Collections.Generic;
using Arcanos.Server.WorldServer.Data;
using Arcanos.Shared;
using Arcanos.Shared.Localization;
using Microsoft.Xna.Framework;

namespace Arcanos.Server.WorldServer
{
    public class StaticData : Template
    {
        public override EditorEntry EditorEntry
        {
            get { return new EditorEntry(ID, "StaticData"); }
        }

        public int FriendlyTargetDecalTextureID;
        public int NeutralTargetDecalTextureID;
        public int HostileTargetDecalTextureID;

        public int MaxDuelParticipants;
        public int[] DuelFactions;
        
        public GameStrings LocalizationGame;

        public Dictionary<int, Text> GlobalChannelNames = new Dictionary<int, Text>();
        public Dictionary<int, Text> GlobalChannelAliases = new Dictionary<int, Text>();

        public ulong InvalidLocationMapGUID;
        public Vector3 InvalidLocationPosition;
        public Rotation InvalidLocationRotation;

        protected override int GetInitialID()
        {
            return DatabaseManager.StaticData.GetID(this);
        }
    }
}