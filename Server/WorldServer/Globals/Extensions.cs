﻿using System.Collections.Generic;
using Arcanos.Server.WorldServer.Data;
using Arcanos.Server.WorldServer.Players;
using Arcanos.Shared.Items;
using Arcanos.Shared.Players;

namespace Arcanos.Server.WorldServer
{
    public static class Extensions
    {
        public static IEnumerable<Character> GetCharacters(this Looter looter)
        {
            if (looter.LootingGroup != 0)
            {
                foreach (CharacterBase member in DatabaseManager.Groups[looter.LootingGroup].Members)
                    if (member != null)
                        yield return member as Character;
            }
            else if (looter.LootingCharacter != 0)
                yield return DatabaseManager.Characters[looter.LootingCharacter];
        }
        public static bool ArePlayersInvolved(this Looter looter)
        {
            return looter.LootingCharacter != 0 || looter.LootingGroup != 0;
        }
    }
}