﻿using System.Collections.Generic;
using Arcanos.Shared.Objects;
using Microsoft.Xna.Framework;

namespace Arcanos.Server.WorldServer
{
    public struct ObjectDistanceComparer : IComparer<WorldObjectBase>
    {
        public Vector3 CompareTo;

        public ObjectDistanceComparer(Vector3 compareTo)
        {
            CompareTo = compareTo;
        }

        public int Compare(WorldObjectBase x, WorldObjectBase y)
        {
            float dx, dy;
            Vector3.Distance(ref CompareTo, ref x.Position, out dx);
            Vector3.Distance(ref CompareTo, ref y.Position, out dy);
            return dx.CompareTo(dy);
        }
    }
}