﻿#define REBUILD_DB

using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Arcanos.Server.WorldServer.Console;
using Arcanos.Shared;
using Arcanos.Shared.Data;
using Arcanos.Shared.Graphics;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Spells;
using Arcanos.Utilities;
using Arcanos.Server.WorldServer.Data;
using Arcanos.Server.WorldServer.World;
using Arcanos.Utilities.Console;

namespace Arcanos.Server.WorldServer
{
    public static class ArcanosWorldServer
    {
#if DEBUG
        public static string TokenPath = "..\\..\\..\\..\\LoginServer\\bin\\x86\\Debug\\Tokens";
#else
        public static string TokenPath = "..\\..\\..\\..\\LoginServer\\bin\\x86\\Release\\Tokens";
#endif
        public static bool NoDatabaseRebuild = false;
        public static bool PreloadDatabase = false;

        static void Main(string[] args)
        {
            for (int i = 0; i < args.Length; i++)
            {
                switch (args[i])
                {
                    case "-t":
                        TokenPath = args[++i];
                        break;
#if REBUILD_DB
                    case "-norebuild":
                        NoDatabaseRebuild = true;
                        break;
#endif
                    case "-preload":
                        PreloadDatabase = true;
                        break;
                    default:
                        Logger.Error(LogCategory.General, "Unknown argument: {0}", args[i]);
                        break;
                }
            }
            InitDatabases();
            Logger.Settings.SetEnabled(LogCategory.World, false);
            Logger.Settings.SetEnabled(LogCategory.Packets, false);
            Logger.Info(LogCategory.Status, "WorldServer started. Build {0}", Assembly.GetExecutingAssembly().GetName().Version.Build);
            Logger.Info(LogCategory.Status, new string('=', 80));
            {
                List<string> typeNames = new List<string>();
                foreach (Type type in Serialization.GetRegisteredTypes())
                    typeNames.Add(type.Name);
                Logger.Debug(LogCategory.General, "Serializable types: {0}", typeNames.Count);
                Logger.Debug(LogCategory.General, "{0}", string.Join(", ", typeNames.ToArray()));
                Logger.Debug(LogCategory.General, new string('=', 80));
            }

            // Connecting to databases
#if REBUILD_DB
            if (!NoDatabaseRebuild)
                if (!File.Exists("maps.serverdb")) // Only rebuild if DB files were removed
                    Tests.MainTest.RebuildDatabase();
#endif
            Tests.MainTest.LoadDatabase(PreloadDatabase);
            // Setting up logging events
            Tests.LoggingTest.HookLoggers();
            // Populating world for testing
            WorldManager.Create();
            Tests.MainTest.PopulateWorld();

            // Main loop
            WorldManager.StartUpdateThread();
            SessionManager.StartListening();
            System.Console.Beep();
            while (true)
            {
                string cmd = System.Console.ReadLine();
                if (cmd.ToLowerInvariant() == "exit")
                    break;
                TargetedCommand.TargetGUID = 0;
                Commands.Execute(cmd);
            }
            SessionManager.StopListening();
            SessionManager.DropAllConnections();
            WorldManager.StopUpdateThread();
            DatabaseManager.CloseAccount();
            DatabaseManager.CloseWorld();
            System.Console.Write("Press any key to continue...");
            System.Console.ReadKey(true);
        }
        private static void InitDatabases()
        {
            Localization.CurrentLocale = LocaleIndex.EnUs;
            Text.LoadHandler = id => DatabaseManager.Text[id];
            Text.MakeHandler = text => DatabaseManager.Text.Add(text);
            SpellVisual.LoadHandler = id => DatabaseManager.SpellVisuals[id];
            FactionBase.LoadHandler = id => DatabaseManager.Factions[id];
            Material.LoadHandler = id => DatabaseManager.Materials[id];
            TextureInfo.LoadHandler = id => DatabaseManager.Textures[id];
            Model.LoadHandler = id => DatabaseManager.Models[id];
            ParticleSystem.LoadHandler = id => DatabaseManager.ParticleSystems[id];
            Serialization.DebugOutput = false;
            SerializationManager.Initialize();
        }
    }
}
