﻿using System;
using System.Collections.Generic;
using Arcanos.Server.WorldServer.Data;
using Arcanos.Server.WorldServer.Objects;
using Arcanos.Server.WorldServer.World;
using Arcanos.Shared;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Spells;
using Arcanos.Shared.World;
using Microsoft.Xna.Framework;

namespace Arcanos.Server.WorldServer.Spells
{
    public class Spell : SpellBase
    {
        protected struct SpellEffectTargetEntry
        {
            public readonly int EffectID;
            public readonly CreatureBase Target;

            public SpellEffectTargetEntry(int effectID, CreatureBase target)
            {
                EffectID = effectID;
                Target = target;
            }
        }

        public new SpellTemplate Template { get { return (SpellTemplate)template; } }

        public Spell(SpellTemplateBase template, MapBase map, Vector3 pos) : base(template, map, pos) { }
        public Spell(SpellTemplateBase template, CreatureBase caster, CreatureBase target) : base(template, caster, target) { }
        public Spell(SpellTemplateBase template, CreatureBase caster, Vector3 target) : base(template, caster, target) { }

        public override void Hit()
        {
            base.Hit();
            ProcessHit();
        }

        private bool hitProcessed;
        private void ProcessHit()
        {
            if (hitProcessed)
                return;
            hitProcessed = true;

            if (ExplicitTarget == null)
            {
                for (int effectID = 0; effectID < template.Effects.Length; ++effectID)
                {
                    float chance = template.Effects[effectID].Chance;
                    if (chance > 0 && chance < 1 && R.Float() > chance)
                        continue;

                    if (template.Effects[effectID].IsAreaEffect)
                        SpellEffectHandlers.Handle(this, GroundTarget, effectID);
                }
            }
            List<CreatureBase> processedTargets = new List<CreatureBase>();
            foreach (SpellEffectTargetEntry target in FindTargets())
            {
                if (!processedTargets.Contains(target.Target))
                {
                    target.Target.SpellHit(Caster, this);
                    processedTargets.Add(target.Target);
                }
                SpellEffectHandlers.Handle(this, target.Target, target.EffectID);
            }
        }

        protected IEnumerable<SpellEffectTargetEntry> FindTargets()
        {
            if (template.Effects == null)
                yield break;

            float radius = template.Radius;
            int count = template.Effects.Length;
            for (int effectID = 0; effectID < count; ++effectID)
            {
                float chance = template.Effects[effectID].Chance;
                if (chance > 0 && chance < 1 && R.Float() > chance)
                    continue;

                SpellEffectTargetData targetData;
                switch (template.Effects[effectID].TargetA)
                {
                    case SpellEffectTarget.Caster:
                        yield return new SpellEffectTargetEntry(effectID, Caster);
                        break;
                    case SpellEffectTarget.CasterTarget:
                        if (Caster.Target != null)
                            yield return new SpellEffectTargetEntry(effectID, Caster.Target);
                        break;
                    case SpellEffectTarget.SpellTarget:
                        yield return new SpellEffectTargetEntry(effectID, ExplicitTarget);
                        break;
                    case SpellEffectTarget.AllAOE:
                    case SpellEffectTarget.HostilesAOE:
                    case SpellEffectTarget.NonFriendsAOE:
                    case SpellEffectTarget.FriendsAOE:
                    {
                        SpellEffect effect = template.Effects[effectID];
                        MapBase sourceMap;
                        Vector3 sourcePos;
                        GetSource(effectID, out sourceMap, out sourcePos);
                        foreach (WorldObjectBase wo in sourceMap.GetNearbyObjects(sourcePos, radius))
                        {
                            if (!wo.Type.IsCreature())
                                continue;

                            switch (effect.TargetA)
                            {
                                case SpellEffectTarget.AllAOE:
                                    break;
                                case SpellEffectTarget.HostilesAOE:
                                    if (Caster.GetReactionFrom(wo as CreatureBase) != Reaction.Hostile)
                                        continue;
                                    break;
                                case SpellEffectTarget.NonFriendsAOE:
                                    if (Caster.GetReactionFrom(wo as CreatureBase) == Reaction.Friendly)
                                        continue;
                                    break;
                                case SpellEffectTarget.FriendsAOE:
                                    if (Caster.GetReactionFrom(wo as CreatureBase) != Reaction.Friendly)
                                        continue;
                                    break;
                                case SpellEffectTarget.ByID:
                                    if (!DatabaseManager.SpellTargetData[template.ID].GetForEffect(effectID, out targetData))
                                        throw new Exception();
                                    if (wo.Template.ID != targetData.CreatureTemplateID)
                                        continue;
                                    break;
                                case SpellEffectTarget.ByGUID:
                                    if (!DatabaseManager.SpellTargetData[template.ID].GetForEffect(effectID, out targetData))
                                        throw new Exception();
                                    if (wo.GUID != targetData.CreatureGUID)
                                        continue;
                                    break;
                            }

                            yield return new SpellEffectTargetEntry(effectID, wo as CreatureBase);
                        }
                        break;
                    }
                    case SpellEffectTarget.ByID:
                    {
                        if (!DatabaseManager.SpellTargetData[template.ID].GetForEffect(effectID, out targetData))
                            throw new Exception();
                        WorldObjectBase target = Caster.Map.GetNearestObjectByID(Caster.Position, targetData.CreatureTemplateID, radius);
                        if (target != null && target.Type.IsCreature())
                            yield return new SpellEffectTargetEntry(effectID, target as CreatureBase);
                        break;
                    }
                    case SpellEffectTarget.ByGUID:
                    {
                        if (!DatabaseManager.SpellTargetData[template.ID].GetForEffect(effectID, out targetData))
                            throw new Exception();
                        CreatureBase target = ObjectManager.GetCreature(targetData.CreatureGUID);
                        if (target != null)
                            yield return new SpellEffectTargetEntry(effectID, target);
                        break;
                    }
                }
            }
        }
        private void GetSource(int effectID, out MapBase sourceMap, out Vector3 sourcePos)
        {
            sourceMap = Caster.Map;
            sourcePos = Caster.Position;

            SpellEffect effect = template.Effects[effectID];
            SpellEffectTargetData targetData;
            switch (effect.TargetB)
            {
                case SpellEffectTarget.Caster:
                    sourceMap = Caster.Map;
                    sourcePos = Caster.Position;
                    break;
                case SpellEffectTarget.CasterTarget:
                    sourceMap = Caster.Target.Map;
                    sourcePos = Caster.Target.Position;
                    break;
                case SpellEffectTarget.SpellTarget:
                    sourceMap = ExplicitTarget.Map;
                    sourcePos = ExplicitTarget.Position;
                    break;
                case SpellEffectTarget.ByID:
                {
                    if (!DatabaseManager.SpellTargetData[template.ID].GetForEffect(effectID, out targetData))
                        throw new Exception();
                    WorldObjectBase wo = Caster.Map.GetNearestObjectByID(Caster.Position, targetData.CreatureTemplateID, template.Range.Max);
                    sourceMap = Caster.Map;
                    sourcePos = wo == null ? Caster.Position : wo.Position;
                    break;
                }
                case SpellEffectTarget.ByGUID:
                {
                    if (!DatabaseManager.SpellTargetData[template.ID].GetForEffect(effectID, out targetData))
                        throw new Exception();
                    WorldObjectBase wo = ObjectManager.GetWorldObject(targetData.CreatureGUID);
                    sourceMap = (wo ?? Caster).Map;
                    sourcePos = wo == null ? Caster.Position : wo.Position;
                    break;
                }
                case SpellEffectTarget.Coordinates:
                    if (!DatabaseManager.SpellTargetData[template.ID].GetForEffect(effectID, out targetData))
                        throw new Exception();
                    sourceMap = Caster.Map;
                    sourcePos = targetData.Coordinates;
                    break;
                case SpellEffectTarget.MapCoordinates:
                    if (!DatabaseManager.SpellTargetData[template.ID].GetForEffect(effectID, out targetData))
                        throw new Exception();
                    if (Caster.Type.IsPlayer())
                        sourceMap = WorldManager.GetOrCreateMapFor((Caster as PlayerCreature).Character, targetData.MapID);
                    else
                        sourceMap = WorldManager.GetPublicMap(targetData.MapID);
                    sourcePos = targetData.Coordinates;
                    break;
                case SpellEffectTarget.GroundTarget:
                    sourceMap = Map;
                    sourcePos = GroundTarget;
                    break;
            }
        }
    }
}