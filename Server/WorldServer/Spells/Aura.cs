﻿using System;
using Arcanos.Server.WorldServer.Data;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Spells;
using Arcanos.Shared.Timing;

namespace Arcanos.Server.WorldServer.Spells
{
    public class Aura : AuraBase
    {
        private Action<Aura> periodicHandler;
        private float periodicInterval;
        private float timeToTick;

        protected Aura() { }
        public Aura(CreatureBase owner, ulong casterGUID, SpellTemplateBase spell, int effectID) : base(owner, casterGUID, spell, effectID) { }

        public override void Update()
        {
            base.Update();
            if (Spell.Effects[EffectID].Type == SpellEffectType.AuraAreaEffect)
                ToBeRemoved = false;
            if (periodicInterval > 0)
            {
                timeToTick -= Time.PerSecond;
                while (timeToTick <= 0)
                {
                    periodicHandler(this);
                    timeToTick += periodicInterval;
                }
            }
        }

        public void SetupPeriodic(float interval, Action<Aura> handler)
        {
            periodicHandler = handler;
            periodicInterval = interval;
            timeToTick = periodicInterval;
            if (timeToTick == 0)
                periodicHandler(this);
        }

        public override byte[] Serialize(string member)
        {
            switch (member)
            {
                case "Spell":
                    return BitConverter.GetBytes(Spell.ID);
            }
            return new byte[0];
        }
        public override void Deserialize(string member, byte[] data)
        {
            Spell = DatabaseManager.Spells[BitConverter.ToInt32(data, 0)];
        }
    }
}