﻿using Arcanos.Server.WorldServer.Data;
using Arcanos.Shared.Data;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Spells;
using Arcanos.Shared.World;
using Microsoft.Xna.Framework;

namespace Arcanos.Server.WorldServer.Spells
{
    [SerializationSettings(Method = SerializationMethod.RefByID, ReferenceResolverType = typeof(DatabaseManager), ReferenceResolverMethod = "GetSpellTemplate")]
    public class SpellTemplate : SpellTemplateBase
    {
        protected SpellTemplate() { }
        public SpellTemplate(Text name, Text description)
        {
            Name = name;
            Description = description;
        }

        public bool HasOffensiveEffects()
        {
            if (Effects == null)
                return false;

            int count = Effects.Length;
            for (int i = 0; i < count; ++i)
            {
                switch (Effects[i].Type)
                {
                    case SpellEffectType.InstantKill:
                    case SpellEffectType.Damage:
                    case SpellEffectType.PhysicalDamage:
                    case SpellEffectType.SchoolDamage:
                        return true;
                    case SpellEffectType.TriggerSpell:
                    case SpellEffectType.TriggerSpellFromTarget:
                        return DatabaseManager.Spells[Effects[i].IntValue1].HasOffensiveEffects();
                    default:
                        continue;
                }
            }
            return false;
        }
        public bool HasDefensiveEffects()
        {
            if (Effects == null)
                return false;

            int count = Effects.Length;
            for (int i = 0; i < count; ++i)
            {
                switch (Effects[i].Type)
                {
                    case SpellEffectType.ApplyAura:
                    case SpellEffectType.RemoveAuraBySpell:
                    case SpellEffectType.RemoveAuraByType:
                    case SpellEffectType.RemoveAuraByCaster:
                    case SpellEffectType.AuraAreaEffect:
                    case SpellEffectType.ApplyAuraAreaEffect:
                    case SpellEffectType.RemoveAuraBySpellAreaEffect:
                    case SpellEffectType.PeriodicAreaEffect:
                        return true;
                    default:
                        continue;
                }
            }
            return false;
        }
        public bool HasHealingEffects()
        {
            if (Effects == null)
                return false;

            int count = Effects.Length;
            for (int i = 0; i < count; ++i)
            {
                switch (Effects[i].Type)
                {
                    case SpellEffectType.Heal:
                    case SpellEffectType.RestorePower:
                        return true;
                    default:
                        continue;
                }
            }
            return false;
        }
        public bool HasCasterEffects()
        {
            if (Effects == null)
                return false;

            int count = Effects.Length;
            for (int i = 0; i < count; ++i)
            {
                switch (Effects[i].TargetA)
                {
                    case SpellEffectTarget.Caster:
                        return true;
                }
            }
            return false;
        }
        public bool HasCasterTargetEffects()
        {
            if (Effects == null)
                return false;

            int count = Effects.Length;
            for (int i = 0; i < count; ++i)
            {
                switch (Effects[i].TargetA)
                {
                    case SpellEffectTarget.CasterTarget:
                        return true;
                }
            }
            return false;
        }
        public bool HasTargetEffects()
        {
            if (Effects == null)
                return false;

            int count = Effects.Length;
            for (int i = 0; i < count; ++i)
            {
                switch (Effects[i].TargetA)
                {
                    case SpellEffectTarget.SpellTarget:
                        return true;
                }
            }
            return false;
        }
        public bool HasAOEEffects()
        {
            if (Effects == null)
                return false;

            int count = Effects.Length;
            for (int i = 0; i < count; ++i)
            {
                switch (Effects[i].TargetA)
                {
                    case SpellEffectTarget.AllAOE:
                    case SpellEffectTarget.HostilesAOE:
                    case SpellEffectTarget.NonFriendsAOE:
                    case SpellEffectTarget.FriendsAOE:
                        return true;
                }
            }
            return false;
        }

        public override SpellBase Create(MapBase map, Vector3 pos)
        {
            return new Spell(this, map, pos);
        }
        public override SpellBase Create(CreatureBase caster, CreatureBase target)
        {
            return new Spell(this, caster, target);
        }
        public override SpellBase Create(CreatureBase caster, Vector3 target)
        {
            return new Spell(this, caster, target);
        }

        protected override int GetInitialID()
        {
            return DatabaseManager.Spells.GetID(this);
        }
    }
}