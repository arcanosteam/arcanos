﻿using Arcanos.Server.WorldServer.Data;
using Arcanos.Shared;
using Microsoft.Xna.Framework;

namespace Arcanos.Server.WorldServer.Spells
{
    public class SpellTargetData : Template
    {
        public override EditorEntry EditorEntry
        {
            get { return new EditorEntry(ID, string.Format("{0} effect targets", Effects.Length)); }
        }

        public SpellEffectTargetData[] Effects;

        public bool GetForEffect(int effectID, out SpellEffectTargetData data)
        {
            int count = Effects.Length;
            for (int i = 0; i < count; ++i)
                if (Effects[i].EffectID == effectID)
                {
                    data = Effects[i];
                    return true;
                }
            data = new SpellEffectTargetData();
            return false;
        }

        protected override int GetInitialID()
        {
            return DatabaseManager.SpellTargetData.GetID(this);
        }
    }
    public struct SpellEffectTargetData
    {
        public int EffectID;
        public int CreatureTemplateID;
        public ulong CreatureGUID;
        public int MapID;
        public Vector3 Coordinates;
    }
}