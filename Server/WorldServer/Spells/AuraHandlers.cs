﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Arcanos.Server.WorldServer.Data;
using Arcanos.Server.WorldServer.Objects;
using Arcanos.Shared;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Players;
using Arcanos.Shared.Spells;
using Arcanos.Utilities;

namespace Arcanos.Server.WorldServer.Spells
{
    public delegate bool AuraHandler(Aura aura, ref SpellEffect effect, bool apply);
    public static class AuraHandlers
    {
        private struct HandlerDef
        {
            public readonly AuraType Type;
            public readonly AuraHandler Handler;

            public HandlerDef(AuraType type, AuraHandler handler)
            {
                Type = type;
                Handler = handler;
            }
        }

        private static readonly HandlerDef[] handlers =
        {
            new HandlerDef(AuraType.None,                       NullAura),

            new HandlerDef(AuraType.Dummy,                      DummyAura),
            new HandlerDef(AuraType.PeriodicTriggerSpell,       PeriodicTriggerSpellAura),
            new HandlerDef(AuraType.PeriodicTriggerSpellFromCaster, PeriodicTriggerSpellFromCasterAura),

            new HandlerDef(AuraType.ModWalkSpeed,               ModWalkSpeedAura),
            new HandlerDef(AuraType.ModRunSpeed,                ModRunSpeedAura),
            new HandlerDef(AuraType.ModSwimSpeed,               ModSwimSpeedAura),
            new HandlerDef(AuraType.ModFlySpeed,                ModFlySpeedAura),
            new HandlerDef(AuraType.ModStat,                    ModStatAura),
            new HandlerDef(AuraType.ModWeaponDamage,            ModWeaponDamageAura),
            new HandlerDef(AuraType.ModWeaponDamagePercent,     ModWeaponDamagePercentAura),
            new HandlerDef(AuraType.ModWeaponSpeedPercent,      ModWeaponSpeedPercentAura),
            new HandlerDef(AuraType.ModHealthRegenRaw,          NullAura),
            new HandlerDef(AuraType.ModHealthRegenPct,          NullAura),
            new HandlerDef(AuraType.ModPowerRegenRaw,           NullAura),
            new HandlerDef(AuraType.ModPowerRegenPct,           NullAura),
            new HandlerDef(AuraType.ModDamageDoneRaw,           NullAura),
            new HandlerDef(AuraType.ModDamageDonePct,           NullAura),
            new HandlerDef(AuraType.ModDamageTakenRaw,          NullAura),
            new HandlerDef(AuraType.ModDamageTakenPct,          NullAura),
            new HandlerDef(AuraType.ModHealingDoneRaw,          NullAura),
            new HandlerDef(AuraType.ModHealingDonePct,          NullAura),
            new HandlerDef(AuraType.ModHealingTakenRaw,         NullAura),
            new HandlerDef(AuraType.ModHealingTakenPct,         NullAura),

            new HandlerDef(AuraType.Stun,                       StunAura),
            new HandlerDef(AuraType.Root,                       RootAura), 
            new HandlerDef(AuraType.Silence,                    SilenceAura),
            new HandlerDef(AuraType.Evasion,                    NullAura),
            new HandlerDef(AuraType.Invisibility,               InvisibilityAura),
            
            new HandlerDef(AuraType.MeleeHitProc,               NullAura), 
        };
        private static readonly int handlersCount = handlers.Length;

        public static void Handle(Aura aura, int effectID, bool apply)
        {
            currentAura = aura;
            AuraType type = aura.Type;
            for (int i = 0; i < handlersCount; ++i)
                if (handlers[i].Type == type)
                {
                    handlers[i].Handler(aura, ref aura.Spell.Effects[effectID], apply);
                    currentAura = null;
                    return;
                }
            Logger.Error(LogCategory.Auras, "AuraHandlers::Handle: No handler defined for AuraType.{0}", type);
            currentAura = null;
        }
        private static Aura currentAura;
        private static bool Failed(Aura aura, bool condition, string errorFormat, params object[] args)
        {
            if (!condition)
            {
                StackFrame stackFrame = new StackTrace().GetFrame(1);
                Logger.Error(LogCategory.Auras, new StringBuilder().Append("AuraHandlers::").Append(stackFrame.GetMethod().Name).Append(":").Append(stackFrame.GetFileLineNumber()).AppendFormat(" (Spell:{0}:{1}): ", aura.Spell.ID, aura.EffectID).AppendFormat(errorFormat, args).ToString());
                return true;
            }
            return false;
        }
        private static bool Failed(bool condition, string errorFormat, params object[] args)
        {
            return Failed(currentAura, condition, errorFormat, args);
        }

        private static bool NullAura(Aura aura, ref SpellEffect effect, bool apply)
        {
            return true;
        }

        private static bool DummyAura(Aura aura, ref SpellEffect effect, bool apply)
        {
            return true;
        }
        private static bool PeriodicTriggerSpellAura(Aura aura, ref SpellEffect effect, bool apply)
        {
            if (!apply)
                return true;

            SpellTemplate spell = DatabaseManager.Spells.GetOrDefault(effect.IntValue1);
            if (Failed(spell != null, "Spell ID={0} not found", effect.IntValue1))
                return false;

            aura.SetupPeriodic(effect.TriggerSpellPeriodic, a =>
            {
                SpellTemplate s = DatabaseManager.Spells.GetOrDefault(a.Effect.IntValue1);
                if (Failed(a, s != null, "Spell ID={0} not found", a.Effect.IntValue1))
                    return;
                a.Owner.SpellBook.CastSpell(s, a.Owner, CastSpellFlags.TriggerSpellIgnores);
            });
            return true;
        }
        private static bool PeriodicTriggerSpellFromCasterAura(Aura aura, ref SpellEffect effect, bool apply)
        {
            if (!apply)
                return true;

            SpellTemplate spell = DatabaseManager.Spells.GetOrDefault(effect.IntValue1);
            if (Failed(spell != null, "Spell ID={0} not found", effect.IntValue1))
                return false;

            aura.SetupPeriodic(effect.TriggerSpellPeriodic, a =>
            {
                CreatureBase caster = ObjectManager.GetCreature(a.CasterGUID);
                if (caster == null)
                    return;

                SpellTemplate s = DatabaseManager.Spells.GetOrDefault(a.Effect.IntValue1);
                if (Failed(a, s != null, "Spell ID={0} not found", a.Effect.IntValue1))
                    return;
                caster.SpellBook.CastSpell(s, a.Owner, CastSpellFlags.TriggerSpellIgnores);
            });
            return true;
        }

        private static bool ModWalkSpeedAura(Aura aura, ref SpellEffect effect, bool apply)
        {
            float maxFactor = 1, minFactor = 1;
            foreach (Aura appliedAura in aura.Owner.SpellBook.GetAuras(AuraType.ModWalkSpeed))
            {
                if (appliedAura.Effect.FloatValue1 > maxFactor)
                    maxFactor = appliedAura.Effect.FloatValue1;
                if (appliedAura.Effect.FloatValue1 < minFactor)
                    minFactor = appliedAura.Effect.FloatValue1;
            }
            aura.Owner.SetMovementSpeedFactor(MovementType.Walk, minFactor < 1 ? minFactor : maxFactor);
            return true;
        }
        private static bool ModRunSpeedAura(Aura aura, ref SpellEffect effect, bool apply)
        {
            float maxFactor = 1, minFactor = 1;
            foreach (Aura appliedAura in aura.Owner.SpellBook.GetAuras(AuraType.ModRunSpeed))
            {
                if (appliedAura.Effect.FloatValue1 > maxFactor)
                    maxFactor = appliedAura.Effect.FloatValue1;
                if (appliedAura.Effect.FloatValue1 < minFactor)
                    minFactor = appliedAura.Effect.FloatValue1;
            }
            aura.Owner.SetMovementSpeedFactor(MovementType.Run, minFactor < 1 ? minFactor : maxFactor);
            return true;
        }
        private static bool ModSwimSpeedAura(Aura aura, ref SpellEffect effect, bool apply)
        {
            float maxFactor = 1, minFactor = 1;
            foreach (Aura appliedAura in aura.Owner.SpellBook.GetAuras(AuraType.ModSwimSpeed))
            {
                if (appliedAura.Effect.FloatValue1 > maxFactor)
                    maxFactor = appliedAura.Effect.FloatValue1;
                if (appliedAura.Effect.FloatValue1 < minFactor)
                    minFactor = appliedAura.Effect.FloatValue1;
            }
            aura.Owner.SetMovementSpeedFactor(MovementType.Swim, minFactor < 1 ? minFactor : maxFactor);
            return true;
        }
        private static bool ModFlySpeedAura(Aura aura, ref SpellEffect effect, bool apply)
        {
            float maxFactor = 1, minFactor = 1;
            foreach (Aura appliedAura in aura.Owner.SpellBook.GetAuras(AuraType.ModFlySpeed))
            {
                if (appliedAura.Effect.FloatValue1 > maxFactor)
                    maxFactor = appliedAura.Effect.FloatValue1;
                if (appliedAura.Effect.FloatValue1 < minFactor)
                    minFactor = appliedAura.Effect.FloatValue1;
            }
            aura.Owner.SetMovementSpeedFactor(MovementType.Fly, minFactor < 1 ? minFactor : maxFactor);
            return true;
        }
        private static bool ModStatAura(Aura aura, ref SpellEffect effect, bool apply)
        {
            if (!aura.Owner.Type.IsPlayer())
                return true;

            if (Failed(effect.IntValue1 >= 0 && effect.IntValue1 < (byte)Stat.Max, "Wrong stat value: {0}", effect.IntValue1))
                return false;

            PlayerCreature pc = aura.Owner as PlayerCreature;
            pc.Character.Stats.Modify((Stat)effect.IntValue1, apply ? effect.IntValue2 : -effect.IntValue2);

            return true;
        }
        private static bool ModWeaponDamageAura(Aura aura, ref SpellEffect effect, bool apply)
        {
            if (aura.Owner.Type.IsPlayer())
                (aura.Owner as PlayerCreature).UpdateAttackPowerStat();
            else if (aura.Owner.Type.IsCreature())
            {
                IntBounds mod = new IntBounds(apply ? effect.IntValue2 : -effect.IntValue2);
                Creature creature = aura.Owner as Creature;
                for (AttackType type = 0; type < AttackType.Max; type++)
                    if (effect.IntValue1 == 0 || ((1 << (byte)type) & effect.IntValue1) != 0)
                        creature.AttackPower[(byte)type].AddRaw(mod);
            }
            return true;
        }
        private static bool ModWeaponDamagePercentAura(Aura aura, ref SpellEffect effect, bool apply)
        {
            if (aura.Owner.Type.IsPlayer())
                (aura.Owner as PlayerCreature).UpdateAttackPowerStat();
            else if (aura.Owner.Type.IsCreature())
            {
                Creature creature = aura.Owner as Creature;
                for (AttackType type = 0; type < AttackType.Max; type++)
                    if (effect.IntValue1 == 0 || ((1 << (byte)type) & effect.IntValue1) != 0)
                        if (apply)
                            creature.AttackPower[(byte)type].AddPct(effect.FloatValue1);
                        else
                            creature.AttackPower[(byte)type].SubPct(effect.FloatValue1);
            }
            return true;
        }
        private static bool ModWeaponSpeedPercentAura(Aura aura, ref SpellEffect effect, bool apply)
        {
            if (aura.Owner.Type.IsPlayer())
                (aura.Owner as PlayerCreature).UpdateAttackIntervalStat();
            else if (aura.Owner.Type.IsCreature())
            {
                Creature creature = aura.Owner as Creature;
                for (AttackType type = 0; type < AttackType.Max; type++)
                    if (effect.IntValue1 == 0 || ((1 << (byte)type) & effect.IntValue1) != 0)
                        if (apply)
                            creature.AttackInterval[(byte)type] *= 1 + effect.FloatValue1;
                        else
                            creature.AttackInterval[(byte)type] *= 1 / (1 + effect.FloatValue1);
            }
            return true;
        }

        private static bool StunAura(Aura aura, ref SpellEffect effect, bool apply)
        {
            aura.Owner.SetFlag(ObjectFlags.Stunned, apply);
            if (apply)
            {
                if (aura.Owner.SpellBook.IsCasting)
                    aura.Owner.SpellBook.InterruptCasting(InterruptMask.All);
            }
            return true;
        }
        private static bool RootAura(Aura aura, ref SpellEffect effect, bool apply)
        {
            aura.Owner.SetFlag(ObjectFlags.Rooted, apply);
            return true;
        }
        private static bool SilenceAura(Aura aura, ref SpellEffect effect, bool apply)
        {
            aura.Owner.SetFlag(ObjectFlags.Silenced, apply);
            if (apply)
            {
                if (aura.Owner.SpellBook.IsCasting && aura.Owner.SpellBook.CastingSpell.IsAffectedBySilence())
                    aura.Owner.SpellBook.InterruptCasting(InterruptMask.All);
            }
            return true;
        }
        private static bool InvisibilityAura(Aura aura, ref SpellEffect effect, bool apply)
        {
            List<PlayerCreatureBase> visibleTo = null;
            if (aura.Owner.Type.IsPlayer())
                visibleTo = (aura.Owner as PlayerCreature).VisibleTo;
            else if (aura.Owner.Type.IsCreature())
                visibleTo = (aura.Owner as Creature).VisibleTo;

            if (visibleTo != null)
                foreach (PlayerCreatureBase pc in visibleTo)
                    if (pc.Target == aura.Owner)
                        pc.SetTarget(null);

            return true;
        }
    }
}