﻿using System.Diagnostics;
using System.Text;
using Arcanos.Server.WorldServer.Data;
using Arcanos.Server.WorldServer.Objects;
using Arcanos.Server.WorldServer.World;
using Arcanos.Shared;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Spells;
using Arcanos.Utilities;
using Microsoft.Xna.Framework;

namespace Arcanos.Server.WorldServer.Spells
{
    public delegate bool SpellEffectHandler(Spell spell, CreatureBase target, int effectID, ref SpellEffect effect);
    public delegate bool SpellEffectGroundHandler(Spell spell, Vector3 target, int effectID, ref SpellEffect effect);
    public static class SpellEffectHandlers
    {
        private struct HandlerDef
        {
            public readonly SpellEffectType Type;
            public readonly SpellEffectHandler Handler;
            public readonly SpellEffectGroundHandler GroundHandler;

            public HandlerDef(SpellEffectType type, SpellEffectHandler handler)
            {
                Type = type;
                Handler = handler;
                GroundHandler = null;
            }
            public HandlerDef(SpellEffectType type, SpellEffectGroundHandler handler)
            {
                Type = type;
                Handler = null;
                GroundHandler = handler;
            }
        }

        private static readonly HandlerDef[] handlers =
        {
            new HandlerDef(SpellEffectType.None,                            NullEffect),

            new HandlerDef(SpellEffectType.InstantKill,                     InstantKillEffect),
            new HandlerDef(SpellEffectType.Damage,                          DamageEffect),
            new HandlerDef(SpellEffectType.PhysicalDamage,                  PhysicalDamageEffect),
            new HandlerDef(SpellEffectType.SchoolDamage,                    SchoolDamageEffect),
            new HandlerDef(SpellEffectType.WeaponDamage,                    WeaponDamageEffect),
            new HandlerDef(SpellEffectType.Heal,                            HealEffect), 
            new HandlerDef(SpellEffectType.RestorePower,                    RestorePowerEffect), 
            new HandlerDef(SpellEffectType.StealHealth,                     StealHealthEffect), 
            new HandlerDef(SpellEffectType.MaxHealthPercentSchoolDamage,    MaxHealthPercentSchoolDamageEffect), 

            new HandlerDef(SpellEffectType.Teleport,                        TeleportEffect),
            new HandlerDef(SpellEffectType.TriggerSpell,                    TriggerSpellEffect),
            new HandlerDef(SpellEffectType.TriggerSpellFromTarget,          TriggerSpellFromTargetEffect),

            new HandlerDef(SpellEffectType.ApplyAura,                       ApplyAuraEffect),
            new HandlerDef(SpellEffectType.ApplyAuraFromSpell,              ApplyAuraEffectFromSpell),
            new HandlerDef(SpellEffectType.RemoveAuraBySpell,               RemoveAuraBySpellEffect),
            new HandlerDef(SpellEffectType.RemoveAuraByType,                RemoveAuraByTypeEffect),
            new HandlerDef(SpellEffectType.RemoveAuraByCaster,              RemoveAuraByCasterEffect),

            new HandlerDef(SpellEffectType.AuraAreaEffect,                  AreaEffect),
            new HandlerDef(SpellEffectType.ApplyAuraAreaEffect,             AreaEffect),
            new HandlerDef(SpellEffectType.RemoveAuraBySpellAreaEffect,     AreaEffect),
            new HandlerDef(SpellEffectType.PeriodicAreaEffect,              AreaEffect),
            new HandlerDef(SpellEffectType.NullAreaEffect,                  AreaEffect),
        };
        private static readonly int handlersCount = handlers.Length;

        public static void Handle(Spell spell, CreatureBase target, int effectID)
        {
            currentSpell = spell;
            currentEffect = effectID;
            SpellEffect effect = spell.Template.Effects[effectID];
            SpellEffectType type = effect.Type;
            for (int i = 0; i < handlersCount; ++i)
                if (handlers[i].Type == type)
                {
                    if (handlers[i].Handler != null)
                        handlers[i].Handler(spell, target, effectID, ref effect);
                    else if (handlers[i].GroundHandler != null)
                        handlers[i].GroundHandler(spell, target.Position, effectID, ref effect);
                    currentSpell = null;
                    currentEffect = 0;
                    return;
                }
            Logger.Error(LogCategory.Spells, "SpellEffectHandlers::Handle: No handler defined for SpellEffectType.{0}", type);
            currentSpell = null;
            currentEffect = 0;
        }
        public static void Handle(Spell spell, Vector3 target, int effectID)
        {
            currentSpell = spell;
            currentEffect = effectID;
            SpellEffect effect = spell.Template.Effects[effectID];
            SpellEffectType type = effect.Type;
            for (int i = 0; i < handlersCount; ++i)
                if (handlers[i].Type == type)
                {
                    handlers[i].GroundHandler(spell, target, effectID, ref effect);
                    currentSpell = null;
                    currentEffect = 0;
                    return;
                }
            Logger.Error(LogCategory.Spells, "SpellEffectHandlers::Handle: No handler defined for SpellEffectType.{0}", type);
            currentSpell = null;
            currentEffect = 0;
        }
        private static Spell currentSpell;
        private static int currentEffect;
        private static bool Failed(bool condition, string errorFormat, params object[] args)
        {
            if (!condition)
            {
                StackFrame stackFrame = new StackTrace().GetFrame(1);
                Logger.Error(LogCategory.Spells, new StringBuilder().Append("SpellEffectHandlers::").Append(stackFrame.GetMethod().Name).Append(":").Append(stackFrame.GetFileLineNumber()).AppendFormat(" (Spell:{0}:{1}): ", currentSpell.Template.ID, currentEffect).AppendFormat(errorFormat, args).ToString());
                return true;
            }
            return false;
        }

        private static bool NullEffect(Spell spell, CreatureBase target, int effectID, ref SpellEffect effect)
        {
            return true;
        }

        private static bool InstantKillEffect(Spell spell, CreatureBase target, int effectID, ref SpellEffect effect)
        {
            if (target.Type.IsPlayer())
                (target as PlayerCreature).Die(spell.Caster);
            else if (spell.Caster.Type.IsCreature())
                (target as Creature).Die(spell.Caster);
            return true;
        }
        private static bool DamageEffect(Spell spell, CreatureBase target, int effectID, ref SpellEffect effect)
        {
            if (!target.IsAlive)
                return true;
            target.InflictDamage(spell.Caster, new Damage { Generic = R.IntInclusive(effect.IntValue1, effect.IntValue2) });
            return true;
        }
        private static bool PhysicalDamageEffect(Spell spell, CreatureBase target, int effectID, ref SpellEffect effect)
        {
            if (!target.IsAlive)
                return true;
            DamageBounds damage = new DamageBounds { Physical = new IntBounds(effect.IntValue1, effect.IntValue2 == 0 ? effect.IntValue1 : effect.IntValue2) };
            Formulas.ScaleDamage(ref damage, spell.Caster, target, ref effect);
            target.InflictDamage(spell.Caster, damage.Random);
            return true;
        }
        private static bool SchoolDamageEffect(Spell spell, CreatureBase target, int effectID, ref SpellEffect effect)
        {
            if (!target.IsAlive)
                return true;
            DamageBounds damage = new DamageBounds((DamageSchool)effect.IntValue3, new IntBounds(effect.IntValue1, effect.IntValue2 == 0 ? effect.IntValue1 : effect.IntValue2));
            Formulas.ScaleDamage(ref damage, spell.Caster, target, ref effect);
            target.InflictDamage(spell.Caster, damage.Random);
            return true;
        }
        private static bool WeaponDamageEffect(Spell spell, CreatureBase target, int effectID, ref SpellEffect effect)
        {
            if (!target.IsAlive)
                return true;
            DamageSchool school = (DamageSchool)effect.IntValue1;
            int attackTypeMask = effect.IntValue2;
            Damage damage = new Damage();
            for (AttackType type = 0; type < AttackType.Max; type++)
                if (attackTypeMask == 0 || ((1 << (byte)type) & attackTypeMask) != 0)
                    if (spell.Caster.Type.IsPlayer())
                        damage += Formulas.CalculateDamage(spell.Caster, target, (spell.Caster as PlayerCreature).AttackPower[(byte)type]);
                    else if (spell.Caster.Type.IsCreature())
                        damage += Formulas.CalculateDamage(spell.Caster, target, (spell.Caster as Creature).AttackPower[(byte)type]);
            if (effect.FloatValue1 != 0)
                damage *= effect.FloatValue1;
            if (school != 0)
                damage = new Damage(school, damage.Sum);
            target.InflictDamage(spell.Caster, damage);
            return true;
        }
        private static bool HealEffect(Spell spell, CreatureBase target, int effectID, ref SpellEffect effect)
        {
            if (!target.IsAlive)
                return true;
            FloatBounds amount = new FloatBounds(effect.IntValue1, effect.IntValue2 == 0 ? effect.IntValue1 : effect.IntValue2);
            Formulas.ScaleHeal(ref amount, spell.Caster, target, ref effect);
            target.Heal(spell.Caster, amount.RandomInt);
            return true;
        }
        private static bool RestorePowerEffect(Spell spell, CreatureBase target, int effectID, ref SpellEffect effect)
        {
            if (!target.IsAlive)
                return true;
            IntBounds amount = new IntBounds(effect.IntValue1, effect.IntValue2 == 0 ? effect.IntValue1 : effect.IntValue2);
            PowerType power = (PowerType)effect.IntValue3;
            target.ModityPower(power, amount.Random);
            return true;
        }
        private static bool StealHealthEffect(Spell spell, CreatureBase target, int effectID, ref SpellEffect effect)
        {
            if (!target.IsAlive)
                return true;
            DamageBounds damage = new DamageBounds((DamageSchool)effect.IntValue3, new IntBounds(effect.IntValue1, effect.IntValue2 == 0 ? effect.IntValue1 : effect.IntValue2));
            Formulas.ScaleDamage(ref damage, spell.Caster, target, ref effect);
            int oldHealth = target.CurrentHealth;
            target.InflictDamage(spell.Caster, damage.Random);
            int heal = oldHealth - target.CurrentHealth;
            spell.Caster.Heal(spell.Caster, (int)(heal * effect.FloatValue3));
            return true;
        }
        private static bool MaxHealthPercentSchoolDamageEffect(Spell spell, CreatureBase target, int effectID, ref SpellEffect effect)
        {
            if (!target.IsAlive)
                return true;
            float percent = (effect.IntValue2 == 0 ? effect.IntValue1 : effect.IntValue2) / 100f;
            DamageBounds damage = new DamageBounds((DamageSchool)effect.IntValue3, new IntBounds(effect.IntValue1, (int)(target.MaxHealth * percent)));
            Formulas.ScaleDamage(ref damage, spell.Caster, target, ref effect);
            target.InflictDamage(spell.Caster, damage.Random);
            return true;
        }

        private static bool TeleportEffect(Spell spell, CreatureBase target, int effectID, ref SpellEffect effect)
        {
            if (effect.IntValue1 != 0)
            {
                if (target is PlayerCreature)
                {
                    (target as PlayerCreature).ChangeMap(effect.IntValue1, effect.FloatValue1, effect.FloatValue2, effect.FloatValue3, target.Rotation.Yaw);
                    return true;
                }

                Map map = null;
                if (spell.Caster.Type.IsPlayer())
                    map = WorldManager.GetOrCreateMapFor((spell.Caster as PlayerCreature).Character, effect.IntValue1);
                else if (spell.Caster.Type.IsCreature())
                    map = WorldManager.GetMapByID(effect.IntValue1);
                target.Map.Remove(target);
                if (map != null)
                    map.Add(target);
            }
            target.MoveTo(new Vector3(effect.FloatValue1, effect.FloatValue2, effect.FloatValue3));
            return true;
        }
        private static bool TriggerSpellEffect(Spell spell, CreatureBase target, int effectID, ref SpellEffect effect)
        {
            SpellTemplate triggeredSpell = DatabaseManager.Spells.GetOrDefault(effect.IntValue1);
            if (Failed(triggeredSpell != null, "Spell ID={0} not found", effect.IntValue1))
                return true;

            spell.Caster.SpellBook.CastSpell(triggeredSpell, target, CastSpellFlags.TriggerSpellIgnores);
            return true;
        }
        private static bool TriggerSpellFromTargetEffect(Spell spell, CreatureBase target, int effectID, ref SpellEffect effect)
        {
            SpellTemplate triggeredSpell = DatabaseManager.Spells.GetOrDefault(effect.IntValue1);
            if (Failed(triggeredSpell != null, "Spell ID={0} not found", effect.IntValue1))
                return true;

            target.SpellBook.CastSpell(triggeredSpell, spell.Caster, CastSpellFlags.TriggerSpellIgnores);
            return true;
        }

        private static bool ApplyAuraEffect(Spell spell, CreatureBase target, int effectID, ref SpellEffect effect)
        {
            target.SpellBook.ApplyAura(spell.Caster.GUID, spell.Template, effectID);
            return true;
        }
        private static bool ApplyAuraEffectFromSpell(Spell spell, CreatureBase target, int effectID, ref SpellEffect effect)
        {
            SpellTemplate auraSpell = DatabaseManager.Spells.GetOrDefault(effect.IntValue1);
            if (Failed(auraSpell != null, "Spell ID={0} not found", effect.IntValue1))
                return true;

            for (int e = 0; e < auraSpell.Effects.Length; e++)
                if (effect.IntValue2 == 0 || ((1 << e) & effect.IntValue2) != 0)
                    if (effect.FloatValue1 == 0)
                        target.SpellBook.ApplyAura(spell.Caster.GUID, auraSpell, e);
                    else
                        target.SpellBook.ApplyAura(spell.Caster.GUID, auraSpell, e, effect.FloatValue1);

            return true;
        }
        private static bool RemoveAuraBySpellEffect(Spell spell, CreatureBase target, int effectID, ref SpellEffect effect)
        {
            target.SpellBook.RemoveAura(DatabaseManager.Spells[effect.IntValue1]);
            return true;
        }
        private static bool RemoveAuraByTypeEffect(Spell spell, CreatureBase target, int effectID, ref SpellEffect effect)
        {
            target.SpellBook.RemoveAura(effect.AuraType);
            return true;
        }
        private static bool RemoveAuraByCasterEffect(Spell spell, CreatureBase target, int effectID, ref SpellEffect effect)
        {
            target.SpellBook.RemoveAura(spell.Caster);
            return true;
        }

        private static bool AreaEffect(Spell spell, Vector3 target, int effectID, ref SpellEffect effect)
        {
            (spell.Map as Map).Add(new AreaEffect(spell, effectID, target));
            return true;
        }
    }
}