﻿using System.Collections.Generic;
using Arcanos.Server.WorldServer.Data;
using Arcanos.Server.WorldServer.Objects;
using Arcanos.Server.WorldServer.World;
using Arcanos.Shared;
using Arcanos.Shared.Net;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Spells;
using Arcanos.Shared.Timing;
using Arcanos.Utilities;
using Microsoft.Xna.Framework;

namespace Arcanos.Server.WorldServer.Spells
{
    public class AreaEffect : AreaEffectBase
    {
        public readonly Vector3 Position;
        public readonly Map Map;
        public readonly ulong CasterGUID;
        public readonly Faction CasterFaction;
        public readonly SpellTemplate Spell;
        public readonly int EffectID;

        public float Duration;
        public readonly List<CreatureBase> AffectedTargets = new List<CreatureBase>();
        public readonly List<PlayerCreature> VisibleTo = new List<PlayerCreature>();

        private float timeToTick;

        public AreaEffect(Spell spell, int effectID, Vector3 pos) : base(0)
        {
            CasterGUID = spell.Caster.GUID;
            CasterFaction = spell.Caster.Faction as Faction;
            Spell = spell.Template;
            EffectID = effectID;
            Map = spell.Map as Map;
            Position = pos;

            Duration = Spell.AreaEffectDuration;

            foreach (WorldObjectBase wo in Map.GetVisibleObjects(Position))
            {
                if (!wo.Type.IsPlayer())
                    continue;

                (wo as PlayerCreature).SendPacketToSelf(new SAreaEffectPlacedPacket { GUID = GUID, CasterGUID = CasterGUID, Position = Position, SpellID = Spell.ID });
                VisibleTo.Add(wo as PlayerCreature);
            }

            if (Spell.Effects[EffectID].Type == SpellEffectType.NullAreaEffect)
                return;

            foreach (WorldObjectBase wo in Map.GetNearbyObjects(Position, Spell.Radius))
                if (wo.Type.IsCreature())
                    CreatureEnteredArea(wo as CreatureBase);
        }

        public void Update()
        {
            if (Spell.AreaEffectDuration > 0)
            {
                Duration -= Time.PerSecond;
                if (Duration <= 0)
                    Remove();
            }

            if (Spell.Effects[EffectID].Type == SpellEffectType.NullAreaEffect)
                return;

            int count = AffectedTargets.Count;
            for (int i = 0; i < count; i++)
            {
                CreatureBase creature = AffectedTargets[i];
                if (!creature.IsInWorld || creature.Map != Map || creature.DistanceTo(Position) > Spell.Radius)
                    if (CreatureLeftArea(creature))
                    {
                        --i;
                        --count;
                    }
            }

            if (Spell.Effects[EffectID].Type == SpellEffectType.PeriodicAreaEffect)
            {
                timeToTick -= Time.PerSecond; 
                while (timeToTick <= 0)
                {
                    foreach (CreatureBase target in AffectedTargets)
                    {
                        CreatureBase caster = ObjectManager.GetCreature(CasterGUID) ?? target;
                        new Spell(DatabaseManager.Spells[Spell.Effects[EffectID].IntValue1], caster, target).Hit();
                    }
                    timeToTick += Spell.Effects[EffectID].AreaEffectPeriodic;
                }
            }
        }

        public void CreatureEnteredArea(CreatureBase creature)
        {
            CreatureBase caster = ObjectManager.GetCreature(CasterGUID);
            switch (Spell.Effects[EffectID].TargetB)
            {
                case SpellEffectTarget.AllAOE:
                    break;
                case SpellEffectTarget.HostilesAOE:
                    if (caster == null && creature.GetReactionTo(CasterFaction) != Reaction.Hostile ||
                        caster != null && creature.GetReactionTo(caster) != Reaction.Hostile)
                        return;
                    break;
                case SpellEffectTarget.NonFriendsAOE:
                    if (caster == null && creature.GetReactionTo(CasterFaction) == Reaction.Friendly ||
                        caster != null && creature.GetReactionTo(caster) == Reaction.Friendly)
                        return;
                    break;
                case SpellEffectTarget.FriendsAOE:
                    if (caster == null && creature.GetReactionTo(CasterFaction) != Reaction.Friendly ||
                        caster != null && creature.GetReactionTo(caster) != Reaction.Friendly)
                        return;
                    break;
                default:
                    Logger.Error(LogCategory.Spells, "AreaEffect::CreatureEnteredArea: Wrong TargetB ({0}) for spell {1}", Spell.Effects[EffectID].TargetB, Spell.ID);
                    return;
            }

            if (AffectedTargets.Contains(creature))
                return;
            AffectedTargets.Add(creature);

            SpellEffect effect = Spell.Effects[EffectID];
            switch (effect.Type)
            {
                case SpellEffectType.AuraAreaEffect:
                case SpellEffectType.ApplyAuraAreaEffect:
                    if (Map.IsAffectedByAreaEffectsOfSpellExcept(creature, Spell, this))
                        return;
                    if (!creature.SpellBook.HasAura(Spell))
                        creature.SpellBook.ApplyAura(CasterGUID, Spell, EffectID);
                    break;
                case SpellEffectType.RemoveAuraBySpellAreaEffect:
                {
                    SpellTemplate spell = DatabaseManager.Spells[effect.IntValue1];
                    if (creature.SpellBook.HasAura(spell))
                        creature.SpellBook.RemoveAura(spell);
                    break;
                }
                default:
                    return;
            }

            Logger.Debug(LogCategory.Spells, "Applied areaeffect of type {0} for {1}", effect.Type, creature);
        }
        public bool CreatureLeftArea(CreatureBase creature)
        {
            if (!AffectedTargets.Remove(creature))
                return false;

            SpellEffect effect = Spell.Effects[EffectID];
            switch (effect.Type)
            {
                case SpellEffectType.AuraAreaEffect:
                    if (Map.IsAffectedByAreaEffectsOfSpellExcept(creature, Spell, this))
                        return true;
                    if (creature.SpellBook.HasAura(Spell))
                        creature.SpellBook.RemoveAura(Spell);
                    break;
                default:
                    return true;
            }

            Logger.Debug(LogCategory.Spells, "Unapplied areaeffect of type {0} for {1}", effect.Type, creature);
            return true;
        }

        public override void Remove()
        {
            foreach (CreatureBase target in new List<CreatureBase>(AffectedTargets))
                CreatureLeftArea(target);
            foreach (PlayerCreature pc in VisibleTo)
                pc.SendPacketToSelf(new SAreaEffectRemovedPacket { GUID = GUID });
            Map.Remove(this);
            base.Remove();
        }
    }
}