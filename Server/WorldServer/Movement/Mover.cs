﻿using Arcanos.Server.WorldServer.Objects;
using Arcanos.Shared;
using Arcanos.Shared.Movement;
using Arcanos.Shared.Net;
using Arcanos.Shared.Objects;

namespace Arcanos.Server.WorldServer.Movement
{
    public class Mover : MoverBase
    {
        public Mover(CreatureBase owner) : base(owner) { }

        public override void SetType(IMovementType type)
        {
            base.SetType(type);
            if (Owner.Type.IsPlayer())
                (Owner as PlayerCreature).SendPacketToVisible(BuildPacket());
            else if (Owner.Type.IsCreature())
                (Owner as Creature).SendPacketToVisible(BuildPacket());
        }

        public SCreatureMovementUpdatePacket BuildPacket()
        {
            SCreatureMovementUpdatePacket packet = new SCreatureMovementUpdatePacket
            {
                GUID = Owner.GUID,
                Position = Owner.Position,
                Type = 0,
                Data = new byte[48],
            };
            FillPacket(ref packet);
            return packet;
        }
    }
}