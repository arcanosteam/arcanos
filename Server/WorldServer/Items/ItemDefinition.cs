﻿using Arcanos.Server.WorldServer.Data;
using Arcanos.Shared.Data;
using Arcanos.Shared.Items;

namespace Arcanos.Server.WorldServer.Items
{
    public class ItemDefinition : ItemDefinitionBase
    {
        [SerializationSettings(Method = SerializationMethod.Manual)]
        [SerializeAs(typeof(ItemTemplate))]
        public override ItemTemplateBase Template { get; set; }

        protected ItemDefinition() { }
        public ItemDefinition(ItemTemplateBase template, int count) : base(template, count) { }

        protected override ItemTemplateBase GetTemplateForID(int id)
        {
            return DatabaseManager.Items[id];
        }
    }
}