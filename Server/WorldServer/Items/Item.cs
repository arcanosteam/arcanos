﻿using System;
using System.Collections.Generic;
using Arcanos.Server.WorldServer.Data;
using Arcanos.Server.WorldServer.Objects;
using Arcanos.Server.WorldServer.Players;
using Arcanos.Server.WorldServer.Scripting;
using Arcanos.Shared.Data;
using Arcanos.Shared.Items;
using Arcanos.Shared.Net;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Players;
using Arcanos.Shared.Spells;

namespace Arcanos.Server.WorldServer.Items
{
    [SerializationSettings(Method = SerializationMethod.RefByGUID, ReferenceResolverType = typeof(DatabaseManager), ReferenceResolverMethod = "GetItem")]
    public class Item : ItemBase, IManualSerialization, IManualDeserialization
    {
        public new ItemTemplate Template
        {
            get { return template as ItemTemplate; }
        }

        public readonly ItemScript Script;

        protected Item() { }
        public Item(ulong guid, ItemTemplate template, int count) : base(guid, template, count)
        {
            if (guid == 0)
                DatabaseManager.GameItems.Add(this);
            else
                DatabaseManager.GameItems.Add((int)GUID, this);
            if (template.Script != null)
                Script = template.Script.Make(this);
        }

        public override void SetCount(int count)
        {
            base.SetCount(count);
            (OwnerCreature as PlayerCreature).SendPacketToSelf(new SItemCountUpdatedPacket { GUID = GUID, Count = Count });
            if (Script != null)
                Script.OnCountChanged();
        }
        public override void Destroy()
        {
            //TODO: DatabaseManager.GameItems.Remove(this);
        }

        public void Pickup()
        {
            if (Template.SpellTriggers[(byte)ItemSpellUsage.OnPickup] != null)
                OwnerCreature.SpellBook.CastSpell(Template.SpellTriggers[(byte)ItemSpellUsage.OnPickup], OwnerCreature, CastSpellFlags.ItemSpellIgnores);

            if (Script != null)
                Script.OnPickup();
        }
        public void Lose()
        {
            if (Template.SpellTriggers[(byte)ItemSpellUsage.OnPickup] != null)
                OwnerCreature.SpellBook.RemoveAura(Template.SpellTriggers[(byte)ItemSpellUsage.OnPickup]);
            if (Template.SpellTriggers[(byte)ItemSpellUsage.OnLose] != null)
                OwnerCreature.SpellBook.CastSpell(Template.SpellTriggers[(byte)ItemSpellUsage.OnLose], OwnerCreature, CastSpellFlags.ItemSpellIgnores);

            if (Script != null)
                Script.OnLose();
        }
        public CastSpellResult Use(CreatureBase target)
        {
            if (Template.SpellTriggers[(byte)ItemSpellUsage.OnUse] != null)
            {
                CastSpellResult result = OwnerCreature.SpellBook.CastSpell(Template.SpellTriggers[(byte)ItemSpellUsage.OnUse], target, CastSpellFlags.ItemSpellIgnores);
                if (result != CastSpellResult.Ok)
                    return result;
            }

            bool toBeRemoved = false;
            if (Template.HasFlag(ItemFlags.Consumable))
                if (Count > 1)
                    SetCount(Count - 1);
                else
                    toBeRemoved = true;

            if (Script != null)
                Script.OnUsed();

            if (toBeRemoved)
                (Inventory as Inventory).RemoveItem(this);

            return CastSpellResult.Ok;
        }
        public void Equip()
        {
            ApplyStatMods(true);

            if (Template.Type == ItemType.Weapon)
            {
                (OwnerCreature as PlayerCreature).UpdateAttackPowerStat();
                (OwnerCreature as PlayerCreature).UpdateAttackIntervalStat();
            }

            if (Template.SpellTriggers[(byte)ItemSpellUsage.OnEquip] != null)
                OwnerCreature.SpellBook.CastSpell(Template.SpellTriggers[(byte)ItemSpellUsage.OnEquip], OwnerCreature, CastSpellFlags.ItemSpellIgnores);

            if (Script != null)
                Script.OnEquipped();
        }
        public void Unequip()
        {
            ApplyStatMods(false);

            if (Template.SpellTriggers[(byte)ItemSpellUsage.OnEquip] != null)
                OwnerCreature.SpellBook.RemoveAura(Template.SpellTriggers[(byte)ItemSpellUsage.OnEquip]);
            if (Template.SpellTriggers[(byte)ItemSpellUsage.OnUnequip] != null)
                OwnerCreature.SpellBook.CastSpell(Template.SpellTriggers[(byte)ItemSpellUsage.OnUnequip], OwnerCreature, CastSpellFlags.ItemSpellIgnores);

            if (Template.Type == ItemType.Weapon)
            {
                (OwnerCreature as PlayerCreature).UpdateAttackPowerStat(this);
                (OwnerCreature as PlayerCreature).UpdateAttackIntervalStat(this);
            }

            if (Script != null)
                Script.OnUneqipped();
        }
        private void ApplyStatMods(bool apply)
        {
            foreach (KeyValuePair<Stat, int> mod in Template.Mods)
                ApplyStatMod(mod.Key, mod.Value, apply);
        }
        private void ApplyStatMod(Stat stat, int mod, bool apply)
        {
            if (!apply)
                mod = -mod;

            (Owner as Character).Stats.Modify(stat, mod);
        }

        public override string ToString()
        {
            return string.Format("{{{2} x {1} [{0}]}}", GUID, Template.Name, Count);
        }

        protected override int GetInitialID()
        {
            return DatabaseManager.GameItems.GetID(this);
        }
        public byte[] Serialize(string member)
        {
            switch (member)
            {
                case "template":
                    return BitConverter.GetBytes(template.ID);
            }
            return new byte[0];
        }
        public void Deserialize(string member, byte[] data)
        {
            switch (member)
            {
                case "template":
                    template = DatabaseManager.Items[BitConverter.ToInt32(data, 0)];
                    break;
            }
        }
    }
}