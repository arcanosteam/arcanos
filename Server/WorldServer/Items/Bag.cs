﻿using System.IO;
using Arcanos.Server.WorldServer.Data;
using Arcanos.Server.WorldServer.Objects;
using Arcanos.Shared.Data;
using Arcanos.Shared.Items;
using Arcanos.Shared.Net;
using Arcanos.Utilities.Debug;

namespace Arcanos.Server.WorldServer.Items
{
    public class Bag : BagBase, IManualSerialization, IManualDeserialization, IPostDeserialization
    {
        protected Bag() { }
        public Bag(Inventory inventory, Item bagItem, byte size) : base(inventory, bagItem, size) { }

        public ItemStoreResult CanStoreItem(ItemBase item, byte slot)
        {
            // TODO: Bag type check
            return ItemStoreResult.Ok;
        }

        public bool AddItem(ItemTemplateBase item, ref int count)
        {
            int quickSlot, remainder;
            if (!CanStoreItem(item, count, out quickSlot, out remainder))
                return false;

            if (quickSlot >= 0)
            {
                D.Assert(remainder == 0, "Remainder should be 0");
                AddItem((byte)quickSlot, item.Create(count));
                count = 0;
                return true;
            }

            for (byte i = 0; i < Size; ++i)
            {
                ItemBase storedItem = Items[i];
                if (storedItem == null)
                {
                    ItemBase createdItem = item.Create(count > item.Stack ? item.Stack : count);
                    if (AddItem(i, createdItem))
                        count -= createdItem.Count;
                }
                else if (storedItem.Template == item && !storedItem.StackFull)
                {
                    int toAdd = count > storedItem.StackRemainder ? storedItem.StackRemainder : count;
                    storedItem.SetCount(storedItem.Count + toAdd);
                    count -= toAdd;
                }

                D.Assert(count >= 0, "Count went below 0");
                if (count == 0)
                    return true;
            }

            return false;
        }
        public bool AddItem(ItemBase item)
        {
            D.Assert(item != null, "Item is null");

            for (byte i = 0; i < Size; ++i)
                if (Items[i] == null)
                    return AddItem(i, item);

            return false;
        }
        public bool RemoveItem(ItemTemplateBase item, ref int count)
        {
            for (byte i = 0; i < Size; ++i)
            {
                ItemBase storedItem = Items[i];
                if (storedItem == null)
                    continue;
                if (storedItem.Template != item)
                    continue;

                if (storedItem.Count > count)
                {
                    storedItem.SetCount(storedItem.Count - count);
                    count = 0;
                }
                else
                {
                    count -= storedItem.Count;
                    RemoveItem(i);
                }

                D.Assert(count >= 0, "Count went below 0");

                if (count == 0)
                    return true;
            }

            return false;
        }
        public bool RemoveItem(ItemBase item)
        {
            D.Assert(item != null, "Item is null");

            for (byte i = 0; i < Size; ++i)
                if (Items[i] == item)
                    return RemoveItem(i);

            return true;
        }

        public override bool AddItem(byte slot, ItemBase item)
        {
            bool result = base.AddItem(slot, item);
            (item.OwnerCreature as PlayerCreature).SendPacketToSelf(new SBagSlotUpdatedPacket { BagIndex = Inventory.IndexOf(this), BagSlot = slot, ItemGUID = item.GUID });
            (item as Item).Pickup();
            return result;
        }
        public override bool RemoveItem(byte slot)
        {
            ItemBase item = Items[slot];
            (item as Item).Lose();
            PlayerCreature owner = item.OwnerCreature as PlayerCreature;
            bool result = base.RemoveItem(slot);
            owner.SendPacketToSelf(new SBagSlotUpdatedPacket { BagIndex = Inventory.IndexOf(this), BagSlot = slot, ItemGUID = 0 });
            return result;
        }

        public void MemberDeserialized(string member, object value)
        {
        }
        public void Deserialized(object parent)
        {
            for (byte i = 0; i < Size; ++i)
                if (Items[i] != null)
                    Items[i].Bag = this;
        }
        public byte[] Serialize(string member)
        {
            MemoryStream stream = null;
            switch (member)
            {
                case "Items":
                {
                    stream = new MemoryStream(1 + Size * 8);
                    BinaryWriter writer = new BinaryWriter(stream);
                    writer.Write(Size);
                    for (byte i = 0; i < Size; ++i)
                        writer.Write(Items[i] == null ? 0 : Items[i].GUID);
                    break;
                }
            }
            if (stream == null)
                return new byte[0];
            byte[] result = stream.GetBuffer();
            stream.Close();
            return result;
        }
        public void Deserialize(string member, byte[] data)
        {
            MemoryStream stream = new MemoryStream(data);
            BinaryReader reader = new BinaryReader(stream);
            switch (member)
            {
                case "Items":
                {
                    byte size = reader.ReadByte();
                    Items = new ItemBase[size];
                    for (byte i = 0; i < size; ++i)
                    {
                        ulong guid = reader.ReadUInt64();
                        Items[i] = guid == 0 ? null : DatabaseManager.GameItems[(int)guid];
                    }
                    break;
                }
            }
            stream.Close();
        }
    }
}