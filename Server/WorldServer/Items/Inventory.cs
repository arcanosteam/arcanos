﻿using System;
using System.Collections.Generic;
using Arcanos.Server.WorldServer.Players;
using Arcanos.Shared.Data;
using Arcanos.Shared.Items;
using Arcanos.Shared.Net;
using Arcanos.Utilities.Debug;

namespace Arcanos.Server.WorldServer.Items
{
    public class Inventory : InventoryBase, IPostDeserialization
    {
        protected Inventory() { }
        public Inventory(Character owner) : base(owner)
        {
        }

        public void ChangeBag(byte index, Item bagItem, byte size)
        {
            Bags[index] = new Bag(this, bagItem, size);
            (Owner as Character).Creature.SendPacketToSelf(new SBagChangedPacket { BagIndex = index, BagItemGUID = bagItem == null ? 0 : bagItem.GUID, BagSize = size });
        }

        public ItemTakeResult CanStoreItem(ItemTemplateBase item, int count)
        {
            if (item.HasFlag(ItemFlags.Unique) && GetItemCount(item) > 0)
                return ItemTakeResult.AlreadyHavingUnique;

            for (int i = 0; i < BAG_LIMIT; ++i)
            {
                BagBase bag = Bags[i];
                if (bag == null)
                    continue;

                int slot;
                bag.CanStoreItem(item, count, out slot, out count);

                D.Assert(count >= 0, "Count went below 0");

                if (count == 0)
                    return ItemTakeResult.Ok;
            }
            return ItemTakeResult.NoSpace;
        }
        public ItemTakeResult CanStoreItem(IEnumerable<ItemDefinitionBase> items)
        {
            // TODO: Simultaneous multiple items check
            //Queue<ItemDefinitionBase> itemQueue = new Queue<ItemDefinitionBase>();
            foreach (ItemDefinition item in items)
            {
                ItemTakeResult result = CanStoreItem(item.Template, item.Count);
                if (result != ItemTakeResult.Ok)
                {
                    //itemQueue.Clear();
                    return result;
                }
                //itemQueue.Enqueue(item);
            }
            return ItemTakeResult.Ok;
        }
        public ItemMoveResult CanMoveItem(ItemBase item, byte bagIndex, byte slot)
        {
            if (item == null)
                return ItemMoveResult.ItemDoesNotExist;
            if (!HasItem(item))
                return ItemMoveResult.ItemIsNotInInventory;
            if (bagIndex >= BAG_LIMIT)
                return ItemMoveResult.InvalidBagIndex;
            Bag bag = Bags[bagIndex] as Bag;
            if (bag == null)
                return ItemMoveResult.BagDoesNotExist;
            if (slot >= bag.Size)
                return ItemMoveResult.InvalidSlot;
            Item containedItem = bag.Items[slot] as Item;
            if (containedItem != null)
            {
                if (containedItem.Template == item.Template && containedItem.StackRemainder > 0)
                    return ItemMoveResult.Ok;
                if ((item.Bag as Bag).CanStoreItem(containedItem, (byte)item.Bag.SlotOf(item)) != ItemStoreResult.Ok)
                    return ItemMoveResult.CantSwapItems;
            }
            switch (bag.CanStoreItem(item, slot))
            {
                case ItemStoreResult.Ok:
                    return ItemMoveResult.Ok;
                case ItemStoreResult.WrongBagType:
                    return ItemMoveResult.WrongBagType;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        public ItemDivideResult CanDivideItem(ItemBase item, byte bagIndex, byte slot, int count)
        {
            if (item == null)
                return ItemDivideResult.ItemDoesNotExist;
            if (!HasItem(item))
                return ItemDivideResult.ItemIsNotInInventory;
            if (count < 1 || count >= item.Count)
                return ItemDivideResult.WrongStackSize;
            if (bagIndex >= BAG_LIMIT)
                return ItemDivideResult.InvalidBagIndex;
            Bag bag = Bags[bagIndex] as Bag;
            if (bag == null)
                return ItemDivideResult.BagDoesNotExist;
            if (slot >= bag.Size)
                return ItemDivideResult.InvalidSlot;
            Item containedItem = bag.Items[slot] as Item;
            if (containedItem != null)
                if (containedItem.Template == item.Template && containedItem.StackRemainder < count)
                    return ItemDivideResult.DestinationNotEmpty;
            switch (bag.CanStoreItem(item, slot))
            {
                case ItemStoreResult.Ok:
                    return ItemDivideResult.Ok;
                case ItemStoreResult.WrongBagType:
                    return ItemDivideResult.WrongBagType;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        public ItemDestroyResult CanDestroyItem(ItemBase item)
        {
            if (item.Template.HasFlag(ItemFlags.Indestructible))
                return ItemDestroyResult.Indestructible;

            return ItemDestroyResult.Ok;
        }
        public ItemMoveResult MoveItem(ItemBase item, byte bagIndex, byte slot)
        {
            ItemMoveResult result = CanMoveItem(item, bagIndex, slot);
            if (result != ItemMoveResult.Ok)
                return result;

            BagBase newBag = Bags[bagIndex];
            Item containedItem = newBag.Items[slot] as Item;
            if (containedItem != null)
            {
                if (containedItem.Template == item.Template && containedItem.StackRemainder > 0)
                {
                    if (containedItem.StackRemainder >= item.Count)
                    {
                        containedItem.SetCount(containedItem.Count += item.Count);
                        (item.Bag as Bag).RemoveItem(item);
                        item.Destroy();
                    }
                    else
                    {
                        item.SetCount(item.Count - containedItem.StackRemainder);
                        containedItem.SetCount(containedItem.Template.Stack);
                    }
                    return ItemMoveResult.Ok;
                }
                (containedItem.Bag as Bag).RemoveItem(containedItem);
            }

            BagBase oldBag = item.Bag;
            int oldSlot = oldBag.SlotOf(item);
            oldBag.RemoveItem((byte)oldSlot);

            if (containedItem != null)
                oldBag.AddItem((byte)oldSlot, containedItem);
            newBag.AddItem(slot, item);

            return ItemMoveResult.Ok;
        }
        public ItemDivideResult DivideItem(ItemBase item, byte bagIndex, byte slot, int count)
        {
            ItemDivideResult result = CanDivideItem(item, bagIndex, slot, count);
            if (result != ItemDivideResult.Ok)
                return result;

            item.SetCount(item.Count - count);

            BagBase newBag = Bags[bagIndex];
            Item containedItem = newBag.Items[slot] as Item;
            if (containedItem == null)
                newBag.AddItem(slot, new Item(0, item.Template as ItemTemplate, count));
            else
                containedItem.SetCount(containedItem.Count + count);

            return ItemDivideResult.Ok;
        }
        public ItemDestroyResult DestoryItem(ItemBase item)
        {
            ItemDestroyResult result = CanDestroyItem(item);
            if (result != ItemDestroyResult.Ok)
                return result;

            Bag bag = item.Bag as Bag;
            if (bag != null)
                bag.RemoveItem(item);
            item.Destroy();

            return ItemDestroyResult.Ok;
        }

        public bool AddItem(ItemBase item)
        {
            for (int i = 0; i < BAG_LIMIT; ++i)
            {
                Bag bag = Bags[i] as Bag;
                if (bag == null)
                    continue;

                if (bag.CanStoreItem(item))
                {
                    bag.AddItem(item);
                    (Owner as Character).Creature.SendPacketToSelf(new SItemsAddedPacket { ID = item.Template.ID, Count = item.Count });
                    (Owner as Character).QuestLog.CreditItem(item.Template, item.Count);
                    return true;
                }
            }
            return false;
        }
        public bool AddItem(ItemTemplateBase item, int count)
        {
            if (CanStoreItem(item, count) != ItemTakeResult.Ok)
                return false;

            for (int i = 0; i < BAG_LIMIT; ++i)
            {
                Bag bag = Bags[i] as Bag;
                if (bag == null)
                    continue;

                int originalCount = count;
                bag.AddItem(item, ref count);

                if (count == 0)
                {
                    (Owner as Character).Creature.SendPacketToSelf(new SItemsAddedPacket { ID = item.ID, Count = originalCount });
                    (Owner as Character).QuestLog.CreditItem(item, originalCount);
                    return true;
                }
            }

            D.Fail("AddItem failed after CanStoreItem check");
            return false;
        }
        public bool AddItem(List<ItemDefinitionBase> items)
        {
            if (CanStoreItem(items) != ItemTakeResult.Ok)
                return false;

            foreach (ItemDefinitionBase item in items)
                AddItem(item.Template, item.Count);

            return true;
        }
        public bool RemoveItem(ItemBase item)
        {
            for (int i = 0; i < BAG_LIMIT; ++i)
            {
                Bag bag = Bags[i] as Bag;
                if (bag == null)
                    continue;

                if (bag.HasItem(item))
                {
                    bag.RemoveItem(item);
                    (Owner as Character).Creature.SendPacketToSelf(new SItemsRemovedPacket { ID = item.Template.ID, Count = item.Count });
                    (Owner as Character).QuestLog.CreditItem(item.Template, -item.Count);
                    return true;
                }
            }
            return false;
        }
        public bool RemoveItem(ItemTemplateBase item, int count)
        {
            if (GetItemCount(item) < count)
                return false;

            for (int i = 0; i < BAG_LIMIT; ++i)
            {
                Bag bag = Bags[i] as Bag;
                if (bag == null)
                    continue;

                bag.RemoveItem(item, ref count);

                if (count == 0)
                {
                    (Owner as Character).Creature.SendPacketToSelf(new SItemsRemovedPacket { ID = item.ID, Count = count });
                    (Owner as Character).QuestLog.CreditItem(item, -count);
                    return true;
                }
            }

            D.Fail("RemoveItem failed after GetItemCount check");
            return false;
        }

        public void MemberDeserialized(string member, object value)
        {
        }
        public void Deserialized(object parent)
        {
            for (byte i = 0; i < BAG_LIMIT; ++i)
                if (Bags[i] != null)
                    Bags[i].Inventory = this;
        }
    }
}