﻿using System.Collections.Generic;
using Arcanos.Server.WorldServer.Data;
using Arcanos.Server.WorldServer.Players;
using Arcanos.Shared;
using Arcanos.Shared.Data;
using Arcanos.Shared.Items;
using Arcanos.Shared.Objects;

namespace Arcanos.Server.WorldServer.Items
{
    public interface ILootItem
    {
        IEnumerable<ItemDefinition> Get(WorldObjectBase source, Looter looter);
    }
    public struct LootElement
    {
        public float Chance;
        [SerializationIgnore]
        public float NormalizedChance;
        public ILootItem Item;
        public IntBounds Count;

        public LootElement(ILootItem item) : this(0, item) { }
        public LootElement(float chance, ILootItem item) : this(chance, new IntBounds(1), item) { }
        public LootElement(float chance, IntBounds count, ILootItem item)
        {
            Chance = chance;
            NormalizedChance = 0;
            Item = item;
            Count = count;
        }

        public IEnumerable<ItemDefinition> Get(WorldObjectBase source, Looter looter)
        {
            int count = Count.Random;
            for (int i = 0; i < count; ++i)
                foreach (ItemDefinition item in Item.Get(source, looter))
                    if (item != null)
                        yield return item;
        }
    }
    public struct LootGroup : ILootItem
    {
        public LootElement[] Elements;

        public LootGroup(params LootElement[] elements)
        {
            Elements = elements;
            normalized = false;
            Normalize();
        }

        [SerializationIgnore]
        private bool normalized;
        private void Normalize()
        {
            if (normalized)
                return;

            int length = Elements.Length;
            float totalChance = 0, minChance = float.MaxValue;
            bool overOne = false;
            for (int i = 0; i < length; ++i)
            {
                float chance = Elements[i].Chance;
                if (chance > 1)
                    overOne = true;
                totalChance += chance;
                if (chance < minChance)
                    minChance = chance == 0 ? 1f / length : chance;
            }
            if (totalChance == 0 || totalChance > 1 || overOne)
            {
                for (int i = 0; i < length; ++i)
                {
                    if (Elements[i].Chance == 0)
                    {
                        Elements[i].NormalizedChance = minChance;
                        totalChance += minChance;
                    }
                }
                for (int i = 0; i < length; ++i)
                    Elements[i].NormalizedChance /= totalChance;
            }
            else
            {
                for (int i = 0; i < length; ++i)
                    Elements[i].NormalizedChance = Elements[i].Chance / 1;
            }

            normalized = true;
        }
        public void Renormalize()
        {
            normalized = false;
        }

        public IEnumerable<ItemDefinition> Get(WorldObjectBase source, Looter looter)
        {
            if (!normalized)
                Normalize();

            if (Elements.Length == 0)
                return new ItemDefinition[0];
            if (Elements.Length == 1)
                return Elements[0].Get(source, looter);

            float roll = R.Float(1);
            int length = Elements.Length;
            float sum = 0;
            for (int i = 0; i < length; ++i)
            {
                sum += Elements[i].NormalizedChance;
                if (roll <= sum)
                    return Elements[i].Get(source, looter);
            }
            return new ItemDefinition[0];
        }
    }
    public struct LootItem : ILootItem
    {
        public ItemTemplate Item;
        public IntBounds Count;

        public LootItem(ItemTemplate item) : this(item, new IntBounds(1)) { }
        public LootItem(ItemTemplate item, IntBounds count)
        {
            Item = item;
            Count = count;
        }

        public IEnumerable<ItemDefinition> Get(WorldObjectBase source, Looter looter)
        {
            if (Item.Type == ItemType.Quest)
            {
                bool found = false;
                foreach (Character character in looter.GetCharacters())
                    if (character.QuestLog.HasQuestForItem(Item) && (found = true))
                        break;
                if (!found)
                    yield break;
            }

            int count = Count.Random;
            int fullStacks = count / Item.Stack;
            int remainder = count % Item.Stack;
            for (int i = 0; i < fullStacks; i++)
                yield return new ItemDefinition(Item, Item.Stack);
            if (remainder > 0)
                yield return new ItemDefinition(Item, remainder);
        }
    }
    public struct LootTemplateReference : ILootItem
    {
        public LootTemplate Template;

        public LootTemplateReference(int id) : this(DatabaseManager.Loot[id]) { }
        public LootTemplateReference(LootTemplate template)
        {
            Template = template;
        }

        public IEnumerable<ItemDefinition> Get(WorldObjectBase source, Looter looter)
        {
            return Template.Get(source, looter);
        }
    }
}