using System.Collections.Generic;
using Arcanos.Server.WorldServer.Data;
using Arcanos.Shared;
using Arcanos.Shared.Data;
using Arcanos.Shared.Items;
using Arcanos.Shared.Objects;

namespace Arcanos.Server.WorldServer.Items
{
    [SerializationSettings(Method = SerializationMethod.RefByID, ReferenceResolverType = typeof(DatabaseManager), ReferenceResolverMethod = "GetLoot")]
    public class LootTemplate : Template
    {
        public override EditorEntry EditorEntry
        {
            get { return new EditorEntry(ID, Name); }
        }

        public string Name;
        public readonly LootElement[] Elements;

        protected LootTemplate() { }
        public LootTemplate(string name, params LootElement[] elements)
        {
            Name = name;
            Elements = elements;
        }

        protected override int GetInitialID()
        {
            return DatabaseManager.Loot.GetID(this);
        }

        public IEnumerable<ItemDefinition> Get(WorldObjectBase source, Looter looter)
        {
            float chanceMul = looter.ArePlayersInvolved() ? 1 : 0.25f;
            foreach (LootElement element in Elements)
                if (R.Float() <= (element.Chance == 0 ? 1 : element.Chance) * chanceMul)
                    foreach (ItemDefinition item in element.Get(source, looter))
                        if (item != null)
                            yield return item;
        }
    }
}