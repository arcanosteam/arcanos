﻿using System.Collections.Generic;
using Arcanos.Server.WorldServer.Objects;
using Arcanos.Shared.Items;
using Arcanos.Shared.Net;
using Arcanos.Shared.Objects;
using Arcanos.Utilities;

namespace Arcanos.Server.WorldServer.Items
{
    public class Loot : LootBase
    {
        public int Money;
        public readonly bool HadMoney;
        public readonly List<ItemDefinition> GeneratedItems;
        private List<PlayerCreature> currentLooters;

        public Loot(LootTemplate template, int money, WorldObjectBase source, Looter looter) : base(source, looter)
        {
            Money = money;
            if (Money != 0)
                HadMoney = true;
            GeneratedItems = new List<ItemDefinition>(template.Get(source, looter));
        }

        public int GetItemCount()
        {
            int count = 0;
            foreach (ItemDefinition item in GeneratedItems)
                if (item != null)
                    ++count;
            return count;
        }

        public void AddLooter(PlayerCreature pc)
        {
            if (currentLooters == null)
                currentLooters = new List<PlayerCreature>();
            if (currentLooters.Contains(pc))
                Logger.Error(LogCategory.Player, "Tried to add player [GUID:{0}] to looters of source [GUID:{1}] but player was already present in the list", pc.GUID, Source.GUID);
            currentLooters.Add(pc);
        }
        public void RemoveLooter(PlayerCreature pc)
        {
            if (currentLooters == null)
                return;
            if (!currentLooters.Remove(pc))
                Logger.Error(LogCategory.Player, "Tried to remove player [GUID:{0}] from looters of source [GUID:{1}] but player wasn't present in the list", pc.GUID, Source.GUID);
        }

        public void SendPacketToLooters(IWorldPacket packet)
        {
            if (currentLooters != null)
                foreach (PlayerCreature pc in currentLooters)
                    pc.SendPacketToSelf(packet);
        }
        public void CloseLootForLooters()
        {
            if (currentLooters != null)
                foreach (PlayerCreature pc in currentLooters)
                    if (pc.LootingObject == Source)
                    {
                        pc.SendPacketToSelf(new SCancelLootingPacket { GUID = Source.GUID });
                        pc.UnsetFlag(ObjectFlags.Looting);
                        pc.LootingObject = null;
                    }
            currentLooters = null;
        }
    }
}