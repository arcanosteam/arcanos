﻿using Arcanos.Server.WorldServer.Objects;
using Arcanos.Shared.Net;
using Arcanos.Shared.Objects;
using Arcanos.Shared.PvP;
using Arcanos.Shared.Spells;

namespace Arcanos.Server.WorldServer.PvP
{
    public class Duel : DuelBase
    {
        public static int[] DuelFactions;

        public override void SetStatus(DuelStatus status)
        {
            base.SetStatus(status);
            foreach (PlayerCreature pc in Participants.Keys)
                pc.SendPacketToSelf(new SDuelStatusPacket { Status = status });
        }
        public override void SetStatus(PlayerCreatureBase participant, DuelParticipantStatus status)
        {
            base.SetStatus(participant, status);
            foreach (PlayerCreature pc in Participants.Keys)
                pc.SendPacketToSelf(new SDuelParticipantStatusPacket { ParticipantGUID = participant.GUID, Status = status });
            if (status == DuelParticipantStatus.Win)
                (participant as PlayerCreature).Evade();
        }

        protected override void SendFactionUpdates(PlayerCreatureBase participant)
        {
            int i = 0;
            foreach (PlayerCreature pc in Participants.Keys)
            {
                pc.SendPacketToSelf(new SCreatureFactionUpdatePacket { GUID = participant.GUID, FactionID = DuelFactions[Participants.Count] });
                (participant as PlayerCreature).SendPacketToSelf(new SCreatureFactionUpdatePacket { GUID = pc.GUID, FactionID = DuelFactions[i++] });
            }
        }
        protected override void SendRestoreFaction(PlayerCreatureBase participant)
        {
            (participant as PlayerCreature).Duel = null;
            foreach (PlayerCreature pc in Participants.Keys)
            {
                pc.SendPacketToSelf(new SCreatureFactionUpdatePacket { GUID = participant.GUID, FactionID = participant.Faction == null ? 0 : participant.Faction.ID });
                (participant as PlayerCreature).SendPacketToSelf(new SCreatureFactionUpdatePacket { GUID = pc.GUID, FactionID = pc.Faction == null ? 0 : pc.Faction.ID });
                if (pc.Target == participant)
                    pc.StopAttacking();
                if (pc.SpellBook.CastingTarget == participant)
                    pc.SpellBook.FailCasting(CastSpellResult.WrongTarget);
                if (participant.Target == pc)
                    (participant as PlayerCreature).StopAttacking();
                if (participant.SpellBook.CastingTarget == pc)
                    participant.SpellBook.FailCasting(CastSpellResult.WrongTarget);
            }
        }
    }
}