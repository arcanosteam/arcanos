﻿using System.Text;
using Arcanos.Server.WorldServer.Dialog;
using Arcanos.Server.WorldServer.Guilds;
using Arcanos.Server.WorldServer.Items;
using Arcanos.Server.WorldServer.Quests;
using Arcanos.Server.WorldServer.Scripting;
using Arcanos.Server.WorldServer.Spells;
using Arcanos.Shared.Data;
using Arcanos.Server.WorldServer.Objects;
using Arcanos.Server.WorldServer.World;
using Arcanos.Server.WorldServer.Players;
using Arcanos.Shared.Graphics;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Spells;
using Arcanos.Utilities;
using Model = Arcanos.Shared.Graphics.Model;

namespace Arcanos.Server.WorldServer.Data
{
    public class WorldDatabase<T> : FileDatabase<T>
    {
        public readonly string ResourceName;

        public WorldDatabase(string filename, string resourceName, string magic) : this(filename, resourceName, magic, Encoding.UTF8) { }
        public WorldDatabase(string filename, string resourceName, string magic, Encoding encoding) : base(filename, magic.ToCharArray(0, 4), encoding)
        {
            ResourceName = resourceName;
            UploadChanges = true;
            UploadChangesImmediately = true;
        }

        public override bool Load()
        {
            bool success = base.Load();
            if (success)
                Logger.Info(LogCategory.Database, "Successfully loaded {0} {1}", Assets.Count, ResourceName);
            else
                Logger.Error(LogCategory.Database, "Failed to load {0}", ResourceName);
            return success;
        }
    }
    public static class DatabaseManager
    {
        // Account-related databases
        public static readonly Database<Player> Players = new WorldDatabase<Player>("players.serverdb", "players", "PLRS");
        public static readonly Database<Character> Characters = new WorldDatabase<Character>("characters.serverdb", "characters", "CHRS");
        public static readonly Database<Item> GameItems = new WorldDatabase<Item>("gameitems.serverdb", "items", "GITM");
        public static readonly Database<Group> Groups = new WorldDatabase<Group>("groups.serverdb", "groups", "GRPS");
        public static readonly Database<Guild> Guilds = new WorldDatabase<Guild>("guilds.serverdb", "guilds", "GLDS");

        // Game-related databases
        public static readonly Database<Text> Text = new WorldDatabase<Text>("text.serverdb", "texts", "TEXT") { MaxAssets = 4096 };
        public static readonly Database<MapTemplate> Maps = new WorldDatabase<MapTemplate>("maps.serverdb", "maps", "MAPS");
        public static readonly Database<WorldObjectTemplate> WorldObjects = new WorldDatabase<WorldObjectTemplate>("wo.serverdb", "world objects", "WOBJ");
        public static readonly Database<CreatureTemplate> Creatures = new WorldDatabase<CreatureTemplate>("creatures.serverdb", "creatures", "CRTS");
        public static readonly Database<SpellTemplate> Spells = new WorldDatabase<SpellTemplate>("spells.serverdb", "spells", "SPLS");
        public static readonly Database<SpellVisual> SpellVisuals = new WorldDatabase<SpellVisual>("spellvisuals.serverdb", "spell visuals", "SVLS");
        public static readonly Database<SpellTargetData> SpellTargetData = new WorldDatabase<SpellTargetData>("spelltargetdata.serverdb", "spell targets", "STAR");
        public static readonly Database<ItemTemplate> Items = new WorldDatabase<ItemTemplate>("items.serverdb", "item templates", "ITMS");
        public static readonly Database<LootTemplate> Loot = new WorldDatabase<LootTemplate>("loot.serverdb", "loot definitions", "LOOT");
        public static readonly Database<DialogPage> DialogPages = new WorldDatabase<DialogPage>("dialogpages.serverdb", "dialog pages", "DLGP");
        public static readonly Database<DialogOption> DialogOptions = new WorldDatabase<DialogOption>("dialogoptions.serverdb", "dialog options", "DLGO");
        public static readonly Database<Quest> Quests = new WorldDatabase<Quest>("quests.serverdb", "quests", "QEST");
        public static readonly Database<Faction> Factions = new WorldDatabase<Faction>("factions.serverdb", "factions", "FCTS");
        public static readonly Database<Environment> Environments = new WorldDatabase<Environment>("environments.serverdb", "environments", "ENVS");

        public static readonly Database<WorldObjectScriptLoader> WorldObjectScripts = new MemoryDatabase<WorldObjectScriptLoader>();
        public static readonly Database<CreatureScriptLoader> CreatureScripts = new MemoryDatabase<CreatureScriptLoader>();
        public static readonly Database<PlayerCreatureScriptLoader> PlayerCreatureScripts = new MemoryDatabase<PlayerCreatureScriptLoader>();
        public static readonly Database<PlayerScriptLoader> PlayerScripts = new MemoryDatabase<PlayerScriptLoader>();
        public static readonly Database<ItemScriptLoader> ItemScripts = new MemoryDatabase<ItemScriptLoader>();

        // Graphics-related databases
        public static readonly Database<Material> Materials = new WorldDatabase<Material>("materials.serverdb", "materials", "MTLS");
        public static readonly Database<TextureInfo> Textures = new WorldDatabase<TextureInfo>("textures.serverdb", "textures", "TXTR") { MaxAssets = 65536 };
        public static readonly Database<Model> Models = new WorldDatabase<Model>("models.serverdb", "models", "MDLS");
        public static readonly Database<ParticleSystem> ParticleSystems = new WorldDatabase<ParticleSystem>("particlesystems.serverdb", "particle systems", "PRTS");

        // These must be loaded last
        public static readonly Database<RaceInfo> RaceInfos = new WorldDatabase<RaceInfo>("raceinfos.serverdb", "races", "RINF");
        public static readonly Database<ClassInfo> ClassInfos = new WorldDatabase<ClassInfo>("classinfos.serverdb", "classes", "CINF");
        public static readonly Database<StaticData> StaticData = new WorldDatabase<StaticData>("staticdata.serverdb", "static data", "STIC");

        private static readonly IDatabase[] dbAccount =
        {
            Players, Characters, GameItems, Groups, Guilds,
        };
        private static readonly IDatabase[] dbWorld =
        {
            Text, Maps, WorldObjects, Creatures, Spells, SpellVisuals, SpellTargetData, Items, Loot, DialogPages, DialogOptions, Quests, Factions, Environments,
            WorldObjectScripts, CreatureScripts, PlayerCreatureScripts, PlayerScripts,
            Materials, Textures, Models, ParticleSystems,
            RaceInfos, ClassInfos, StaticData,
        };

        public static void ConnectAccount()
        {
            foreach (IDatabase database in dbAccount)
                database.Connect();
        }
        public static void CloseAccount()
        {
            foreach (IDatabase database in dbAccount)
            {
                database.Close();
                database.Clear();
            }
        }
        public static void PreloadAccount()
        {
            foreach (IDatabase database in dbAccount)
                database.Load();
        }

        public static void ConnectWorld()
        {
            foreach (IDatabase database in dbWorld)
                database.Connect();
        }
        public static void CloseWorld()
        {
            foreach (IDatabase database in dbWorld)
                database.Close();
            foreach (IDatabase database in dbWorld)
                database.Clear();
        }
        public static void PreloadWorld()
        {
            foreach (IDatabase database in dbWorld)
                database.Load();
        }

        public static Character GetCharacter(int id)
        {
            return Characters[id];
        }
        public static Item GetItem(int guid)
        {
            return GameItems[guid];
        }
        public static Group GetGroup(int id)
        {
            return Groups[id];
        }
        public static SpellTemplate GetSpellTemplate(int id)
        {
            return Spells[id];
        }
        public static ItemTemplate GetItemTemplate(int id)
        {
            return Items[id];
        }
        public static LootTemplate GetLoot(int id)
        {
            return Loot[id];
        }
        public static DialogPage GetDialogPage(int id)
        {
            return DialogPages[id];
        }
        public static DialogOption GetDialogOption(int id)
        {
            return DialogOptions[id];
        }
        public static Quest GetQuest(int id)
        {
            return Quests[id];
        }
        public static Faction GetFaction(int id)
        {
            return Factions[id];
        }
        public static Environment GetEnvironment(int id)
        {
            return Environments[id];
        }
        public static WorldObjectScriptLoader GetWorldObjectScriptLoader(int id)
        {
            return WorldObjectScripts[id];
        }
        public static WorldObjectScriptLoader GetWorldObjectScript(string scriptName)
        {
            return WorldObjectScripts.Find(script => script.ScriptName == scriptName);
        }
        public static CreatureScriptLoader GetCreatureScriptLoader(int id)
        {
            return CreatureScripts[id];
        }
        public static CreatureScriptLoader GetCreatureScript(string scriptName)
        {
            return CreatureScripts.Find(script => script.ScriptName == scriptName);
        }
        public static PlayerCreatureScriptLoader GetPlayerCreatureScriptLoader(int id)
        {
            return PlayerCreatureScripts[id];
        }
        public static PlayerCreatureScriptLoader GetPlayerCreatureScript(string scriptName)
        {
            return PlayerCreatureScripts.Find(script => script.ScriptName == scriptName);
        }
        public static PlayerScriptLoader GetPlayerScriptLoader(int id)
        {
            return PlayerScripts[id];
        }
        public static PlayerScriptLoader GetPlayerScript(string scriptName)
        {
            return PlayerScripts.Find(script => script.ScriptName == scriptName);
        }
        public static ItemScriptLoader GetItemScriptLoader(int id)
        {
            return ItemScripts[id];
        }
        public static ItemScriptLoader GetItemScript(string scriptName)
        {
            return ItemScripts.Find(script => script.ScriptName == scriptName);
        }
    }
}