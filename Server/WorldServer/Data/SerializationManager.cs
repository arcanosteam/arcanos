﻿using Arcanos.Server.WorldServer.Dialog;
using Arcanos.Server.WorldServer.Guilds;
using Arcanos.Server.WorldServer.Items;
using Arcanos.Server.WorldServer.Movement;
using Arcanos.Server.WorldServer.Objects;
using Arcanos.Server.WorldServer.Players;
using Arcanos.Server.WorldServer.Quests;
using Arcanos.Server.WorldServer.Spells;
using Arcanos.Server.WorldServer.World;
using Arcanos.Server.WorldServer.World.Spawns;
using Arcanos.Shared.Data;
using Arcanos.Shared.Graphics;
using Arcanos.Shared.Items;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Movement;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Quests;
using Arcanos.Shared.Spells;
using Arcanos.Shared.World;
using Arcanos.Utilities;

namespace Arcanos.Server.WorldServer.Data
{
    public static class SerializationManager
    {
        public static void Initialize()
        {
            Serialization.Register(typeof(Player));
            Serialization.Register(typeof(Character));
            Serialization.Register(typeof(Stats));
            Serialization.Register(typeof(Guild));
            Serialization.Register(typeof(GuildMember));

            Serialization.Register(typeof(Text));
            Serialization.Register(typeof(GameStrings));

            Serialization.Register(typeof(WorldObjectTemplate));
            Serialization.Register(typeof(CreatureTemplate));

            Serialization.Register(typeof(SpellTemplate));
            Serialization.Register(typeof(SpellEffect));
            Serialization.Register(typeof(SpellVisual));
            Serialization.Register(typeof(SpellVisualSet));

            Serialization.Register(typeof(ItemDefinition));
            Serialization.Register(typeof(ItemTemplate));
            Serialization.Register(typeof(Loot));
            Serialization.Register(typeof(Looter));

            Serialization.Register(typeof(Quest));
            Serialization.Register(typeof(QuestObjective));

            Serialization.Register(typeof(Mover));
            Serialization.Register(typeof(PointOverTimeMovement));
            Serialization.Register(typeof(PointWithSpeedMovement));
            Serialization.Register(typeof(ClosestPointWithSpeedMovement));

            Serialization.Register(typeof(MapTemplate));
            Serialization.Register(typeof(HeightMapData));
            Serialization.Register(typeof(TerrainData));
            Serialization.Register(typeof(TerrainData.TerrainPatch));
            Serialization.Register(typeof(StaticModelObjectBase));

            Serialization.Register(typeof(Faction));

            Serialization.Register(typeof(Material));
            Serialization.Register(typeof(TextureInfo));
            Serialization.Register(typeof(LightData));
            Serialization.Register(typeof(EmptyModel));
            Serialization.Register(typeof(SpriteModel));
            Serialization.Register(typeof(SpriteModelAnimation));
            Serialization.Register(typeof(MultiAngleSpriteModel));
            Serialization.Register(typeof(MultiAngleSpriteModelAnimation));
            Serialization.Register(typeof(WorldModel));
            Serialization.Register(typeof(DecalModel));
            Serialization.Register(typeof(ParticleSystem));
            Serialization.Register(typeof(ParticleEmitter));

            Serialization.Register(typeof(Environment));

            // Server-side only types
            Serialization.SetRegisterID(0x10000000);

            Serialization.Register(typeof(SpellTargetData));
            Serialization.Register(typeof(CharacterSpellBookData));
            Serialization.Register(typeof(CharacterSpellBookData.CooldownInstance));
            Serialization.Register(typeof(Aura));

            Serialization.Register(typeof(Item));
            Serialization.Register(typeof(Inventory));
            Serialization.Register(typeof(Bag));
            Serialization.Register(typeof(LootTemplate));
            Serialization.Register(typeof(LootElement));
            Serialization.Register(typeof(LootGroup));
            Serialization.Register(typeof(LootItem));
            Serialization.Register(typeof(LootTemplateReference));

            Serialization.Register(typeof(QuestInstance));
            Serialization.Register(typeof(QuestLog));

            Serialization.Register(typeof(DialogStart));
            Serialization.Register(typeof(DialogPage));
            Serialization.Register(typeof(DialogOption));

            Serialization.Register(typeof(MapSpawn));
            Serialization.Register(typeof(MapObjectSpawn));
            Serialization.Register(typeof(MapCreatureSpawn));

            Serialization.Register(typeof(Group));

            Serialization.Register(typeof(RaceInfo));
            Serialization.Register(typeof(ClassInfo));
            Serialization.Register(typeof(StaticData));

            Serialization.Register(typeof(EnvironmentChangeEntry));

            Serialization.outputLock = Logger.outputLock;
        }
    }
}