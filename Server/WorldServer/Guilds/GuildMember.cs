﻿using Arcanos.Server.WorldServer.Data;
using Arcanos.Server.WorldServer.Players;
using Arcanos.Shared.Data;
using Arcanos.Shared.Guilds;
using Arcanos.Shared.Players;

namespace Arcanos.Server.WorldServer.Guilds
{
    public class GuildMember : GuildMemberBase
    {
        [SerializationSettings(Method = SerializationMethod.RefByID, ReferenceResolverType = typeof(DatabaseManager), ReferenceResolverMethod = "GetCharacter")]
        public new Character Character
        {
            get { return (Character)base.Character; }
            set { base.Character = value; }
        }

        public GuildMember() : base(null, 0) { }
        public GuildMember(CharacterBase character, int rank) : base(character, rank) { }
    }
}