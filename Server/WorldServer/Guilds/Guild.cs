﻿using Arcanos.Server.WorldServer.Data;
using Arcanos.Shared;
using Arcanos.Shared.Guilds;
using Arcanos.Shared.Players;

namespace Arcanos.Server.WorldServer.Guilds
{
    public class Guild : GuildBase
    {
        protected override void AddMemberImpl(CharacterBase character, int rank)
        {
            Members.Add(new GuildMember(character, rank));
        }

        protected override int GetInitialID()
        {
            return DatabaseManager.Guilds.GetID(this);
        }
    }
}