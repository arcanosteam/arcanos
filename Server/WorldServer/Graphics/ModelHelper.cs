﻿using System.IO;
using Arcanos.Server.WorldServer.Data;
using Arcanos.Shared.Graphics;
using ObjLoader.Loader.Loaders;
using Material = Arcanos.Shared.Graphics.Material;

namespace Arcanos.Server.WorldServer.Graphics
{
    public static class ModelHelper
    {
        private class MatrialStreamRedirection : IMaterialStreamProvider
        {
            private readonly string filename;
            
            public MatrialStreamRedirection(string modelFilePath)
            {
                filename = modelFilePath;
            }

            public Stream Open(string materialFilePath)
            {
                return File.OpenRead(Path.Combine(Path.GetDirectoryName(filename), materialFilePath));
            }
        }
        private static readonly ObjLoaderFactory factory = new ObjLoaderFactory();
        public static void CollectMaterials(WorldModel model, string texturePath)
        {
            LoadResult result;
            using (FileStream stream = new FileStream(model.Filename, FileMode.Open, FileAccess.Read))
                result = factory.Create(new MatrialStreamRedirection(model.Filename)).Load(stream);

            model.Materials = new int[result.Materials.Count];
            int i = 0;
            foreach (ObjLoader.Loader.Data.Material material in result.Materials)
                model.Materials[i++] = DatabaseManager.Materials.Add(new Material(DatabaseManager.Textures.Add(new TextureInfo(Path.Combine(texturePath, material.DiffuseTextureMap)))));
        }
    }
}