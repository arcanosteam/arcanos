using Arcanos.Server.WorldServer.Data;
using Arcanos.Server.WorldServer.Dialog;
using Arcanos.Server.WorldServer.Objects;
using Arcanos.Shared.Data;
using Arcanos.Shared.Dialog;

namespace Arcanos.Server.WorldServer.Scripting
{
    [SerializationSettings(Method = SerializationMethod.RefByID, ReferenceResolverType = typeof(DatabaseManager), ReferenceResolverMethod = "GetWorldObjectScriptLoader")]
    public abstract class WorldObjectScriptLoader : ScriptLoader<WorldObject, WorldObjectScript>
    {
        protected WorldObjectScriptLoader(string scriptName) : base(scriptName)
        {
            DatabaseManager.WorldObjectScripts.Add(this);
        }

        protected override int GetInitialID()
        {
            return DatabaseManager.WorldObjectScripts.GetID(this);
        }
    }
    public abstract class WorldObjectScript : ScriptBase
    {
        protected readonly WorldObject Me;

        protected WorldObjectScript(WorldObject me)
        {
            Me = me;
            OnLoad();
        }

        public virtual void OnReset() { }
        public virtual void OnSpawn() { }
        public virtual void OnRespawn() { }
        public virtual void OnDespawn() { }
        public abstract void OnUpdate();

        public virtual DialogStart GetDialogStart(PlayerCreature player) { return Me.Template.Dialog; }
        public virtual bool CanEndDialog(PlayerCreature player, DialogPage page) { return true; }
        public virtual void OnDialogStart(PlayerCreature player, DialogStart dialog, DialogPage page) { }
        public virtual void OnDialogEnd(PlayerCreature player, DialogPage page, DialogEndReason reason) { }
        public virtual void OnDialogPage(PlayerCreature player, DialogPage currentPage, ref DialogPage nextPage) { }
        public virtual bool OnDialogOption(PlayerCreature player, DialogPage page, DialogOption option, int index) { return false; }
    }
}