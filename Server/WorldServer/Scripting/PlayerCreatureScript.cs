using Arcanos.Server.WorldServer.Data;
using Arcanos.Server.WorldServer.Objects;
using Arcanos.Server.WorldServer.Spells;
using Arcanos.Shared;
using Arcanos.Shared.Data;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Spells;
using Microsoft.Xna.Framework;

namespace Arcanos.Server.WorldServer.Scripting
{
    [SerializationSettings(Method = SerializationMethod.RefByID, ReferenceResolverType = typeof(DatabaseManager), ReferenceResolverMethod = "GetPlayerCreatureScriptLoader")]
    public abstract class PlayerCreatureScriptLoader : ScriptLoader<PlayerCreature, PlayerCreatureScript>
    {
        protected PlayerCreatureScriptLoader(string scriptName) : base(scriptName)
        {
            DatabaseManager.PlayerCreatureScripts.Add(this);
        }

        protected override int GetInitialID()
        {
            return DatabaseManager.PlayerCreatureScripts.GetID(this);
        }
    }
    public abstract class PlayerCreatureScript : ScriptBase
    {
        protected readonly PlayerCreature Me;

        protected PlayerCreatureScript(PlayerCreature me)
        {
            Me = me;
            OnLoad();
        }

        public virtual void OnReset() { }
        public virtual void OnSpawn() { }
        public virtual void OnRespawn() { }
        public virtual void OnDespawn() { }
        public abstract void OnUpdate();

        public virtual void OnDeath(CreatureBase killer) { }
        public virtual void OnMeleeHit(CreatureBase attacker) { }
        public virtual void OnMeleeMiss(CreatureBase attacker) { }
        public virtual void OnSpellHit(CreatureBase caster, SpellTemplate spell) { }
        public virtual void OnSpellMiss(CreatureBase caster, SpellTemplate spell) { }
        public virtual void OnSpellCasted(SpellTemplate spell, CreatureBase target) { }
        public virtual void OnSpellCasted(SpellTemplate spell, Vector3 target) { }
        public virtual void OnSpellCastingStarted(SpellTemplate spell, CreatureBase target) { }
        public virtual void OnSpellCastingStarted(SpellTemplate spell, Vector3 target) { }
        public virtual void OnSpellCastingFailed(SpellTemplate spell, CreatureBase target, CastSpellResult result) { }
        public virtual void OnSpellCastingFailed(SpellTemplate spell, Vector3 target, CastSpellResult result) { }
        public virtual void OnDamageTaken(CreatureBase attacker, ref Damage damage) { }
        public virtual void OnHealed(CreatureBase healer, ref int amount) { }
        public virtual void OnAuraApplied(Aura aura) { }
        public virtual void OnAuraRemoved(Aura aura) { }

        public virtual void OnHealthChanged(CreatureBase invoker) { }
    }
}