using Arcanos.Server.WorldServer.Data;
using Arcanos.Server.WorldServer.Players;
using Arcanos.Shared.Data;

namespace Arcanos.Server.WorldServer.Scripting
{
    [SerializationSettings(Method = SerializationMethod.RefByID, ReferenceResolverType = typeof(DatabaseManager), ReferenceResolverMethod = "GetPlayerScriptLoader")]
    public abstract class PlayerScriptLoader : ScriptLoader<Player, PlayerScript>
    {
        protected PlayerScriptLoader(string scriptName) : base(scriptName)
        {
            DatabaseManager.PlayerScripts.Add(this);
        }

        protected override int GetInitialID()
        {
            return DatabaseManager.PlayerScripts.GetID(this);
        }
    }
    public class PlayerScript : ScriptBase
    {
        protected readonly Player Player;
        protected readonly Character Character;

        protected PlayerScript(Character me)
        {
            Character = me;
            Player = me.Player;
            OnLoad();
        }
    }
}