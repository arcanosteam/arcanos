using Arcanos.Server.WorldServer.Data;
using Arcanos.Server.WorldServer.Dialog;
using Arcanos.Server.WorldServer.Objects;

namespace Arcanos.Server.WorldServer.Scripting.Game
{
    public class NPCTestDialogTeleport : CreatureScriptLoader
    {
        public NPCTestDialogTeleport() : base("npc_test_dialog_teleport") { }

        private class Script : CreatureScript
        {
            public Script(Creature me) : base(me) { }

            public override void OnUpdate() { }

            public override bool OnDialogOption(PlayerCreature player, DialogPage page, DialogOption option, int index)
            {
                if (page.ID == 1 && option.ID == 2)
                {
                    int map = player.Map.Template.ID == 1 ? 2 : 1;
                    float x = 0.5f * DatabaseManager.Maps[2].SizeX;
                    float y = 0.5f * DatabaseManager.Maps[2].SizeY;
                    float z = DatabaseManager.Maps[2].GetHeight(x, y);
                    player.ChangeMap(map, x, y, z, player.Rotation.Yaw);
                    return true;
                }
                return false;
            }
        }

        public override CreatureScript Make(Creature scriptedObject)
        {
            return new Script(scriptedObject);
        }
    }
}