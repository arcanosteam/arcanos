using Arcanos.Server.WorldServer.Objects;
using Arcanos.Server.WorldServer.Spells;
using Arcanos.Shared.Spells;

namespace Arcanos.Server.WorldServer.Scripting.Game
{
    public class GenericCreatureScript : CreatureScriptLoader
    {
        public GenericCreatureScript() : base("generic_creature") { }

        private class Script : CreatureScript
        {
            public Script(Creature me) : base(me) { }

            public override void OnUpdate()
            {
                if (!Me.IsAlive)
                {
                    StopChase();
                    return;
                }
                if (!Me.UpdateTarget())
                {
                    if (Me.IsInCombat)
                        Me.Evade();
                    return;
                }

                if (Me.CurrentHealthPercent <= 50)
                    foreach (SpellTemplate spell in (Me.SpellBook as SpellBook).FindDefensiveSpells())
                        if (Me.SpellBook.CanCastSpell(spell, Me, CastSpellFlags.None) == CastSpellResult.Ok)
                            Me.CastSpell(Me, spell, CastSpellFlags.None);
                if (Me.CurrentHealthPercent <= 25)
                    foreach (SpellTemplate spell in (Me.SpellBook as SpellBook).FindHealingSpells())
                        if (Me.SpellBook.CanCastSpell(spell, Me, CastSpellFlags.None) == CastSpellResult.Ok)
                            Me.CastSpell(Me, spell, CastSpellFlags.None);
                foreach (SpellTemplate spell in (Me.SpellBook as SpellBook).FindOffensiveSpells())
                    if (Me.SpellBook.CanCastSpell(spell, Me.Target, CastSpellFlags.None) == CastSpellResult.Ok)
                        Me.CastSpell(Me.Target, spell, CastSpellFlags.None);

                if (Me.SpellBook.IsCasting && Me.SpellBook.CastingSpell.IsCastingInterruptedBy(InterruptMask.Moving | InterruptMask.Rotating))
                    StopChase();
                else
                    ChaseTarget();

                Me.DoAttacksIfReady();
            }
        }

        public override CreatureScript Make(Creature scriptedObject)
        {
            return new Script(scriptedObject);
        }
    }
}