﻿namespace Arcanos.Server.WorldServer.Scripting.Game
{
    public static class ScriptLoader
    {
        public static PlayerScriptLoader GetPlayerScriptLoader()
        {
            return null;
        }
        public static PlayerCreatureScriptLoader GetPlayerCreatureScriptLoader()
        {
            return null;
        }
        public static void LoadScripts()
        {
            new GenericCreatureScript();
            new NPCTestDialogTeleport();
        }
    }
}