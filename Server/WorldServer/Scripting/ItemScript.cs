using Arcanos.Server.WorldServer.Data;
using Arcanos.Server.WorldServer.Items;
using Arcanos.Shared.Data;

namespace Arcanos.Server.WorldServer.Scripting
{
    [SerializationSettings(Method = SerializationMethod.RefByID, ReferenceResolverType = typeof(DatabaseManager), ReferenceResolverMethod = "GetItemScriptLoader")]
    public abstract class ItemScriptLoader : ScriptLoader<Item, ItemScript>
    {
        protected ItemScriptLoader(string scriptName) : base(scriptName)
        {
            DatabaseManager.ItemScripts.Add(this);
        }

        protected override int GetInitialID()
        {
            return DatabaseManager.ItemScripts.GetID(this);
        }
    }
    public class ItemScript : ScriptBase
    {
        protected readonly Item Me;

        public ItemScript(Item me)
        {
            Me = me;
            OnLoad();
        }

        public virtual void OnPickup() { }
        public virtual void OnLose() { }
        public virtual void OnCountChanged() { }
        public virtual void OnEquipped() { }
        public virtual void OnUneqipped() { }
        public virtual void OnUsed() { }
    }
}