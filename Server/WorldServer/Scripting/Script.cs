﻿using Arcanos.Shared;
using Arcanos.Utilities;
using Arcanos.Shared.Data;

namespace Arcanos.Server.WorldServer.Scripting
{
    public abstract class ScriptLoader<T, TScript> : Template where TScript : ScriptBase
    {
        public override EditorEntry EditorEntry
        {
            get { return new EditorEntry(ID, ScriptName); }
        }

        public readonly string ScriptName;

        protected ScriptLoader(string scriptName)
        {
            ScriptName = scriptName;
            Logger.Debug(LogCategory.Scripting, "Script loaded: '{0}'", ScriptName);
        }

        public abstract TScript Make(T scriptedObject);
    }
    [SerializationIgnore]
    public abstract class ScriptBase
    {
        public virtual void OnLoad()
        {
            Logger.Debug(LogCategory.Scripting, "Script instantiated");
        }
    }
}