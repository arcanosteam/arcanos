using Arcanos.Server.WorldServer.Data;
using Arcanos.Server.WorldServer.Dialog;
using Arcanos.Server.WorldServer.Objects;
using Arcanos.Server.WorldServer.Spells;
using Arcanos.Shared;
using Arcanos.Shared.Data;
using Arcanos.Shared.Dialog;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Spells;
using Arcanos.Shared.Timing;
using Microsoft.Xna.Framework;

namespace Arcanos.Server.WorldServer.Scripting
{
    [SerializationSettings(Method = SerializationMethod.RefByID, ReferenceResolverType = typeof(DatabaseManager), ReferenceResolverMethod = "GetCreatureScriptLoader")]
    public abstract class CreatureScriptLoader : ScriptLoader<Creature, CreatureScript>
    {
        protected CreatureScriptLoader(string scriptName) : base(scriptName)
        {
            DatabaseManager.CreatureScripts.Add(this);
        }

        protected override int GetInitialID()
        {
            return DatabaseManager.CreatureScripts.GetID(this);
        }
    }
    public abstract class CreatureScript : ScriptBase
    {
        protected readonly Creature Me;

        protected CreatureScript(Creature me)
        {
            Me = me;
            OnLoad();
        }

        public virtual void OnReset() { }
        public virtual void OnSpawn() { }
        public virtual void OnRespawn() { }
        public virtual void OnDespawn() { }
        public abstract void OnUpdate();

        public virtual void OnDeath(CreatureBase killer) { }
        public virtual void OnMeleeHit(CreatureBase attacker) { }
        public virtual void OnMeleeMiss(CreatureBase attacker) { }
        public virtual void OnSpellHit(CreatureBase caster, SpellTemplate spell) { }
        public virtual void OnSpellMiss(CreatureBase caster, SpellTemplate spell) { }
        public virtual void OnSpellCasted(SpellTemplate spell, CreatureBase target) { }
        public virtual void OnSpellCasted(SpellTemplate spell, Vector3 target) { }
        public virtual void OnSpellCastingStarted(SpellTemplate spell, CreatureBase target) { }
        public virtual void OnSpellCastingStarted(SpellTemplate spell, Vector3 target) { }
        public virtual void OnSpellCastingFailed(SpellTemplate spell, CreatureBase target, CastSpellResult result) { }
        public virtual void OnSpellCastingFailed(SpellTemplate spell, Vector3 target, CastSpellResult result) { }
        public virtual void OnDamageTaken(CreatureBase attacker, ref Damage damage) { }
        public virtual void OnHealed(CreatureBase healer, ref int amount) { }
        public virtual void OnAuraApplied(Aura aura) { }
        public virtual void OnAuraRemoved(Aura aura) { }

        public virtual void OnHealthChanged(CreatureBase invoker) { }

        public virtual DialogStart GetDialogStart(PlayerCreature player) { return Me.Template.Dialog; }
        public virtual bool CanEndDialog(PlayerCreature player, DialogPage page) { return true; }
        public virtual void OnDialogStart(PlayerCreature player, DialogStart dialog, DialogPage page) { }
        public virtual void OnDialogEnd(PlayerCreature player, DialogPage page, DialogEndReason reason) { }
        public virtual void OnDialogPage(PlayerCreature player, DialogPage currentPage, ref DialogPage nextPage) { }
        public virtual bool OnDialogOption(PlayerCreature player, DialogPage page, DialogOption option, int index) { return false; }

        // Utility functions
        protected CreatureBase chasingTarget;
        protected long nextTargetChasePositionUpdateTimestamp;
        public void ChaseTarget()
        {
            if (Me.Target == null)
                return;

            Chase(Me.Target);
        }
        public void Chase(CreatureBase target)
        {
            if (target == null)
                return;

            if (Vector3.Distance(Me.IsInCombat ? Me.AggroPosition : Me.Position, target.Position) > Me.ChaseDistance)
            {
                Me.Evade();
                return;
            }

            float approachRadius = target.GetApproachRadius() + Me.GetApproachRadius();
            float maxApproachRadius = approachRadius + Me.GetMeleeReach();

            if (chasingTarget == target)
            {
                if (Time.Timestamp >= nextTargetChasePositionUpdateTimestamp)
                {
                    nextTargetChasePositionUpdateTimestamp = Time.Timestamp + 100;
                    if (Me.DistanceTo(chasingTarget) > maxApproachRadius)
                        Me.Mover.MoveClosestToPointWithSpeed(target.Position, (approachRadius + maxApproachRadius) / 2);
                }
                return;
            }

            chasingTarget = target;
            Me.Mover.MoveClosestToPointWithSpeed(target.Position, (approachRadius + maxApproachRadius) / 2);
        }
        public void StopChase()
        {
            if (chasingTarget == null)
                return;

            chasingTarget = null;
            Me.Mover.Stop();
        }
    }
}