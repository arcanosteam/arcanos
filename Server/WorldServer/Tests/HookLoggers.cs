﻿using Arcanos.Shared.Grids;
using Arcanos.Utilities;

namespace Arcanos.Server.WorldServer.Tests
{
    public static class LoggingTest
    {
        public static void HookLoggers()
        {
            GridCell.ObjectAdded += (cell, wo) => Logger.Debug(LogCategory.Grid,
                "Object GUID:{0} ID:{1} entered grid cell [{2},{3}] for map GUID:{4} ID:{5}",
                wo.GUID,
                wo.Template.ID,
                cell.X,
                cell.Y,
                cell.Map.GUID,
                cell.Map.Template.ID);
            GridCell.ObjectRemoved += (cell, wo) => Logger.Debug(LogCategory.Grid,
                "Object GUID:{0} ID:{1} left grid cell [{2},{3}] for map GUID:{4} ID:{5}",
                wo.GUID,
                wo.Template.ID,
                cell.X,
                cell.Y,
                cell.Map.GUID,
                cell.Map.Template.ID);
        }
    }
}