﻿using System;
using System.Collections.Generic;
using Arcanos.Server.WorldServer.Chat;
using Arcanos.Server.WorldServer.Data;
using Arcanos.Server.WorldServer.Dialog;
using Arcanos.Server.WorldServer.Graphics;
using Arcanos.Server.WorldServer.Guilds;
using Arcanos.Server.WorldServer.Items;
using Arcanos.Server.WorldServer.Objects;
using Arcanos.Server.WorldServer.Players;
using Arcanos.Server.WorldServer.PvP;
using Arcanos.Server.WorldServer.Quests;
using Arcanos.Server.WorldServer.Scripting.Game;
using Arcanos.Server.WorldServer.Spells;
using Arcanos.Server.WorldServer.World;
using Arcanos.Shared;
using Arcanos.Shared.Graphics;
using Arcanos.Shared.Items;
using Arcanos.Shared.Localization;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Players;
using Arcanos.Shared.PvP;
using Arcanos.Shared.Quests;
using Arcanos.Shared.Spells;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Environment = Arcanos.Server.WorldServer.World.Environment;
using Model = Arcanos.Shared.Graphics.Model;

namespace Arcanos.Server.WorldServer.Tests
{
    public static partial class MainTest
    {
        private delegate int[] AnimIndexesGenerator(ref int from, int count);
        private delegate int[][] MultiAngleAnimIndexesGenerator(ref int from, int count, int angles);
        private enum CreatureRace : byte
        {
            Goblin = PlayerRace.Max,
            Rat,
            Wolf,
            HumanNPC,

            Max,
        }
        private struct ModelLoadAnimation
        {
            public readonly string Name;
            public readonly string FilenameSuffix;
            public readonly int Frames;
            public readonly int Angles;
            public readonly float Length;
            public readonly bool Oneshot;

            public ModelLoadAnimation(string name, string filenameSuffix, int frames, int angles, float length, bool oneshot)
            {
                Name = name;
                FilenameSuffix = filenameSuffix;
                Frames = frames;
                Angles = angles;
                Length = length;
                Oneshot = oneshot;
            }
        }
        public static void RebuildDatabase()
        {
            DatabaseManager.ConnectWorld();
            ScriptLoader.LoadScripts();
            StaticData staticData = new StaticData();

            #region Helper Methods
            Func<string, int, int> loadAngles = (format, angles) =>
            {
                int result = 0;
                for (int i = 0; i < angles; i++)
                {
                    int curID = DatabaseManager.Textures.Add(new TextureInfo(string.Format(format, i)));
                    if (result == 0)
                        result = curID;
                }
                return result;
            };
            AnimIndexesGenerator animIndexes = delegate(ref int from, int count)
            {
                int[] result = new int[count];
                for (int i = 0; i < count; i++)
                    result[i] = from++;
                return result;
            };
            MultiAngleAnimIndexesGenerator multiAngleAnimIndexes = delegate(ref int from, int count, int angles)
            {
                int[][] result = new int[count][];
                for (int i = 0; i < count; i++)
                {
                    result[i] = new int[angles];
                    for (int j = 0; j < angles; j++)
                        result[i][j] = from++;
                }
                return result;
            };
            Func<string, float, string, ModelLoadAnimation, MultiAngleSpriteModelAnimation> loadAnimAngles = (name, length, format, set) =>
            {
                const string frameChars = "abcdefghijklmnopqrstuvwxyz";
                int fromID = 0;
                if (set.Frames < 0)
                {
                    for (int i = -set.Frames - 1; i >= 0; i--)
                    {
                        int curID = loadAngles(string.Format(format, frameChars[i]), set.Angles);
                        if (fromID == 0)
                            fromID = curID;
                    }
                }
                else
                {
                    for (int i = 0; i < set.Frames; i++)
                    {
                        int curID = loadAngles(string.Format(format, frameChars[i]), set.Angles);
                        if (fromID == 0)
                            fromID = curID;
                    }
                }

                return new MultiAngleSpriteModelAnimation(name, length, multiAngleAnimIndexes(ref fromID, set.Frames < 0 ? -set.Frames : set.Frames, set.Angles)) { OneShot = set.Oneshot };
            };
            ModelLoadAnimation[][] loadModelAnimationSets =
            {
                new[] // 0 - Unanimated
                {
                    new ModelLoadAnimation("Stand", "S", 1, 1, 0, false),
                },
                new[] // 1 - Default Creature
                {
                    new ModelLoadAnimation("Stand", "S", 1, 5, 0, false),
                    new ModelLoadAnimation("Walk", "W", 8, 5, 0.5f, false),
                    new ModelLoadAnimation("WalkBackwards", "W", -8, 5, 0.5f, false),
                    new ModelLoadAnimation("MeleeAttackHit", "A", 4, 5, 0.5f, true),
                    new ModelLoadAnimation("MeleeAttackMiss", "A", 6, 5, 0.75f, true),
                    new ModelLoadAnimation("Death", "D", 5, 1, 0.5f, true),
                    new ModelLoadAnimation("Dead", "X", 1, 1, 0, false),
                    new ModelLoadAnimation("SpellCast", "F", 5, 5, 0.5f, false),
                    new ModelLoadAnimation("SpellCastSuccess", "F", 5, 5, 0.5f, true),
                    new ModelLoadAnimation("SpellHit", "N", 6, 5, 0.5f, true),
                    new ModelLoadAnimation("MeleeHit", "N", 6, 5, 0.5f, true),
                },
                new[] // 2 - Passive Creature
                {
                    new ModelLoadAnimation("Stand", "S", 1, 5, 0, false),
                    new ModelLoadAnimation("Walk", "W", 8, 5, 0.5f, false),
                    new ModelLoadAnimation("WalkBackwards", "W", -8, 5, 0.5f, false),
                    new ModelLoadAnimation("Death", "D", 3, 1, 0.5f, true),
                    new ModelLoadAnimation("Dead", "X", 1, 1, 0, false),
                    new ModelLoadAnimation("SpellHit", "N", 6, 5, 0.5f, true),
                    new ModelLoadAnimation("MeleeHit", "N", 6, 5, 0.5f, true),
                },
                new[] // 3 - Default Creature (4 frames)
                {
                    new ModelLoadAnimation("Stand", "S", 1, 5, 0, false),
                    new ModelLoadAnimation("Walk", "W", 8, 5, 0.5f, false),
                    new ModelLoadAnimation("WalkBackwards", "W", -8, 5, 0.5f, false),
                    new ModelLoadAnimation("MeleeAttackHit", "A", 4, 5, 0.5f, true),
                    new ModelLoadAnimation("MeleeAttackMiss", "A", 6, 5, 0.75f, true),
                    new ModelLoadAnimation("Death", "D", 4, 1, 0.5f, true),
                    new ModelLoadAnimation("Dead", "X", 1, 1, 0, false),
                    new ModelLoadAnimation("SpellCast", "F", 5, 5, 0.5f, false),
                    new ModelLoadAnimation("SpellCastSuccess", "F", 5, 5, 0.5f, true),
                    new ModelLoadAnimation("SpellHit", "N", 5, 5, 0.5f, true),
                    new ModelLoadAnimation("MeleeHit", "N", 5, 5, 0.5f, true),
                },
                new[] // 4 - Default Creature (2 frames)
                {
                    new ModelLoadAnimation("Stand", "S", 1, 5, 0, false),
                    new ModelLoadAnimation("Walk", "W", 8, 5, 0.5f, false),
                    new ModelLoadAnimation("WalkBackwards", "W", -8, 5, 0.5f, false),
                    new ModelLoadAnimation("MeleeAttackHit", "A", 4, 5, 0.5f, true),
                    new ModelLoadAnimation("MeleeAttackMiss", "A", 6, 5, 0.75f, true),
                    new ModelLoadAnimation("Death", "D", 2, 1, 0.5f, true),
                    new ModelLoadAnimation("Dead", "X", 1, 1, 0, false),
                    new ModelLoadAnimation("SpellCast", "F", 5, 5, 0.5f, false),
                    new ModelLoadAnimation("SpellCastSuccess", "F", 5, 5, 0.5f, true),
                    new ModelLoadAnimation("SpellHit", "N", 5, 5, 0.5f, true),
                    new ModelLoadAnimation("MeleeHit", "N", 5, 5, 0.5f, true),
                },
            };
            Func<string, string, int, MultiAngleSpriteModelAnimation[]> loadModel = (prefix, palette, animSet) =>
            {
                ModelLoadAnimation[] set = loadModelAnimationSets[animSet];
                MultiAngleSpriteModelAnimation[] anims = new MultiAngleSpriteModelAnimation[set.Length];
                for (int i = 0; i < set.Length; i++)
                    anims[i] = loadAnimAngles(set[i].Name, set[i].Length, string.Format("Textures\\{0}{1}{{0}}{{{{0}}}}_pal{2}.png", prefix, set[i].FilenameSuffix, palette), set[i]);
                return anims;
            };
            #endregion

            #region Textures
            DatabaseManager.Textures.Add(1, new TextureInfo("Textures\\195688.png"));
            DatabaseManager.Textures.Add(2, new TextureInfo("Textures\\S_Fire03.png"));
            DatabaseManager.Textures.Add(3, new TextureInfo("Textures\\SP103B01.png"));
            DatabaseManager.Textures.Add(4, new TextureInfo("Textures\\S_Buff04.png"));

            MultiAngleSpriteModelAnimation[][][] raceModelAnimations = new MultiAngleSpriteModelAnimation[(byte)CreatureRace.Max][][];

            raceModelAnimations[(byte)PlayerRace.Human] = new[]
            {
                loadModel("Models\\656\\M656", "657", 4),
                loadModel("Models\\656\\M656", "658", 4),
                loadModel("Models\\656\\M656", "659", 4),

                loadModel("Models\\456\\M456", "456", 1),
                loadModel("Models\\456\\M456", "457", 1),
                loadModel("Models\\456\\M456", "458", 1),
                loadModel("Models\\456\\M456", "459", 1),

                loadModel("Models\\460\\M460", "461", 1),
                loadModel("Models\\460\\M460", "462", 1),
                loadModel("Models\\460\\M460", "463", 1),

                loadModel("Models\\362\\M362", "363", 1),
                loadModel("Models\\362\\M362", "364", 1),
                loadModel("Models\\362\\M362", "365", 1),
                loadModel("Models\\362\\M362", "366", 1),
                loadModel("Models\\362\\M362", "367", 1),
                loadModel("Models\\362\\M362", "368", 1),
                loadModel("Models\\362\\M362", "369", 1),
                loadModel("Models\\362\\M362", "370", 1),
                loadModel("Models\\362\\M362", "371", 1),

                loadModel("Models\\620\\M620", "621", 1),
                loadModel("Models\\620\\M620", "622", 1),
                loadModel("Models\\620\\M620", "623", 1),
            };
            raceModelAnimations[(byte)PlayerRace.WoodElf] = new[]
            {
                loadModel("Models\\424\\M424", "425", 1),
                loadModel("Models\\424\\M424", "426", 1),
                loadModel("Models\\424\\M424", "427", 1),

                loadModel("Models\\428\\M428", "429", 1),
                loadModel("Models\\428\\M428", "430", 1),
                loadModel("Models\\428\\M428", "431", 1),

                loadModel("Models\\432\\M432", "433", 1),
                loadModel("Models\\432\\M432", "434", 1),
                loadModel("Models\\432\\M432", "435", 1),
            };
            raceModelAnimations[(byte)PlayerRace.Minotaur] = new[]
            {
                loadModel("Models\\286\\M286", "286", 1),
                loadModel("Models\\286\\M286", "287", 1),
                loadModel("Models\\286\\M286", "288", 1),
                loadModel("Models\\286\\M286", "289", 1),

                loadModel("Models\\484\\M484", "484", 1),
                loadModel("Models\\484\\M484", "485", 1),
                loadModel("Models\\484\\M484", "486", 1),
                loadModel("Models\\484\\M484", "487", 1),
            };
            raceModelAnimations[(byte)PlayerRace.Orc] = new[]
            {
                loadModel("Models\\440\\M440", "441", 3),
                loadModel("Models\\440\\M440", "442", 3),
                loadModel("Models\\440\\M440", "443", 3),

                loadModel("Models\\436\\M436", "437", 3),
                loadModel("Models\\436\\M436", "438", 3),
                loadModel("Models\\436\\M436", "439", 3),

                loadModel("Models\\540\\M540", "541", 3),
                loadModel("Models\\540\\M540", "543", 3),
            };
            raceModelAnimations[(byte)PlayerRace.Lizardman] = new[]
            {
                loadModel("Models\\400\\M400", "401", 3),
                loadModel("Models\\400\\M400", "402", 3),
                loadModel("Models\\400\\M400", "403", 3),

                loadModel("Models\\404\\M404", "405", 3),
                loadModel("Models\\404\\M404", "406", 3),
                loadModel("Models\\404\\M404", "407", 3),
            };
            raceModelAnimations[(byte)CreatureRace.Goblin] = new[]
            {
                loadModel("Models\\462\\M462", "463", 3),
                loadModel("Models\\462\\M462", "464", 3),
                loadModel("Models\\462\\M462", "465", 3),
                loadModel("Models\\462\\M462", "465", 3),

                loadModel("Models\\480\\M480", "481", 3),
                loadModel("Models\\480\\M480", "482", 3),
                loadModel("Models\\480\\M480", "483", 3),

                loadModel("Models\\476\\M476", "477", 3),
                loadModel("Models\\476\\M476", "478", 3),
                loadModel("Models\\476\\M476", "479", 3),
            };
            raceModelAnimations[(byte)CreatureRace.Wolf] = new[]
            {
                loadModel("Models\\512\\M512", "512", 3),
                loadModel("Models\\512\\M512", "513", 3),
            };
            raceModelAnimations[(byte)CreatureRace.Rat] = new[]
            {
                loadModel("Models\\452\\M452", "452", 3),
            };
            raceModelAnimations[(byte)CreatureRace.HumanNPC] = new[]
            {
                loadModel("Models\\352\\M352", "354", 2),
                loadModel("Models\\352\\M352", "356", 2),
                loadModel("Models\\352\\M352", "358", 2),
                loadModel("Models\\352\\M352", "359", 2),
            };

            DatabaseManager.Textures.Skip(187);

            int texGroundBase = DatabaseManager.Textures.Add(new TextureInfo("Textures\\189483.png"));
            int texGroundLayer0 = DatabaseManager.Textures.Add(new TextureInfo("Textures\\184887.png"));
            int texGroundLayer1 = DatabaseManager.Textures.Add(new TextureInfo("Textures\\195688.png"));
            int texGroundBaseN = DatabaseManager.Textures.Add(new TextureInfo("Textures\\287315.png"));
            int texGroundLayer0N = DatabaseManager.Textures.Add(new TextureInfo("Textures\\290190.png"));
            int texGroundLayer1N = DatabaseManager.Textures.Add(new TextureInfo("Textures\\281561.png"));

            /*int tf = texGroundLayer1N + 1;
            for (int mapID = 0; mapID <= 0; ++mapID)
                for (int y = 0; y < 4; ++y)
                    for (int x = 0; x < 4; ++x)
                        for (int layer = 0; layer < 2; ++layer)
                            DatabaseManager.Textures.Add(new TextureInfo(string.Format("Textures\\m{0}a{1}_{2}x{3}.png", mapID, layer, x, y)));*/

            int texGroundTarget = DatabaseManager.Textures.Add(new TextureInfo("Textures\\groundtarget.png"));
            int texParticleStar = DatabaseManager.Textures.Add(new TextureInfo("Textures\\particlestar.png"));
            staticData.FriendlyTargetDecalTextureID = DatabaseManager.Textures.Add(new TextureInfo("Textures\\playertargetdecalf.png"));
            staticData.NeutralTargetDecalTextureID = DatabaseManager.Textures.Add(new TextureInfo("Textures\\playertargetdecaln.png"));
            staticData.HostileTargetDecalTextureID = DatabaseManager.Textures.Add(new TextureInfo("Textures\\playertargetdecalh.png"));
            int[] texToxicFumes =
            {
                DatabaseManager.Textures.Add(new TextureInfo("Textures\\toxicfumes01.png")),
                DatabaseManager.Textures.Add(new TextureInfo("Textures\\toxicfumes02.png")),
                DatabaseManager.Textures.Add(new TextureInfo("Textures\\toxicfumes03.png")),
                DatabaseManager.Textures.Add(new TextureInfo("Textures\\toxicfumes04.png")),
            };
            int[] texLavaSpark =
            {
                DatabaseManager.Textures.Add(new TextureInfo("Textures\\lavaspark01.png")),
                DatabaseManager.Textures.Add(new TextureInfo("Textures\\lavaspark02.png")),
                DatabaseManager.Textures.Add(new TextureInfo("Textures\\lavaspark03.png")),
                DatabaseManager.Textures.Add(new TextureInfo("Textures\\lavaspark04.png")),
            };
            int texLavaPool = DatabaseManager.Textures.Add(new TextureInfo("Textures\\lavapool.png"));

            int texIcons = DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\A_Armor04.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\A_Armor05.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\A_Armour01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\A_Armour02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\A_Armour03.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\A_Clothing01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\A_Clothing02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\A_Shoes01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\A_Shoes02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\A_Shoes03.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\A_Shoes04.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\A_Shoes05.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\A_Shoes06.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\A_Shoes07.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\Ac_Earing01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\Ac_Earing02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\Ac_Gloves01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\Ac_Gloves02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\Ac_Gloves03.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\Ac_Gloves04.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\Ac_Gloves05.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\Ac_Gloves06.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\Ac_Gloves07.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\Ac_Medal01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\Ac_Medal02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\Ac_Medal03.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\Ac_Medal04.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\Ac_Necklace01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\Ac_Necklace02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\Ac_Necklace03.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\Ac_Necklace04.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\Ac_Ring01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\Ac_Ring02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\Ac_Ring03.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\Ac_Ring04.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\C_Elm01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\C_Elm02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\C_Elm03.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\C_Elm04.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\C_Hat01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\C_Hat02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\C_Hat03.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\E_Bones01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\E_Bones02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\E_Bones03.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\E_Gold01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\E_Gold02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\E_Metal01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\E_Metal02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\E_Metal03.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\E_Metal04.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\E_Metal05.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\E_Wood01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\E_Wood02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\E_Wood03.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\E_Wood04.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Agate.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Amethist.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Antidote.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_BatWing.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_BirdsBeak.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Bone.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Book.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Bottle01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Bottle02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Bottle03.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Bottle04.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_BronzeBar.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_BronzeCoin.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_C_Apple.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_C_Banana.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_C_Bread.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_C_Carrot.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_C_Cheese.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_C_Cherry.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_C_Egg.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_C_Fish.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_C_Grapes.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_C_GreenGrapes.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_C_GreenPepper.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_C_Lemon.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_C_Meat.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_C_Mulberry.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_C_Mushroom.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_C_Nut.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_C_Orange.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_C_Pear.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_C_Pie.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_C_Pineapple.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_C_Radish.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_C_RawFish.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_C_RawMeat.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_C_RedPepper.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_C_Strawberry.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_C_UnripeApple.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_C_Watermellon.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_C_YellowPepper.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Cannon01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Cannon02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Cannon03.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Cannon04.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Cannon05.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Chest01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Chest02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Coal.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Crystal01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Crystal02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Crystal03.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Diamond.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Eye.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Fabric.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Fang.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Feather01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Feather02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_FishTail.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_FoxTail.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_FrogLeg.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_GoldBar.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_GoldCoin.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Ink.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_IronBall.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Jade.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Key01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Key02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Key03.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Key04.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Key05.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Key06.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Key07.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Leaf.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Map.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Mirror.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Opal.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Rock01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Rock02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Rock03.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Rock04.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Rock05.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Rubi.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Saphire.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_ScorpionClaw.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Scroll.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Scroll02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_SilverBar.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_SilverCoin.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_SnailShell.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_SolidShell.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Telescope.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Tentacle.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Torch01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Torch02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_Water.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\I_WolfFur.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\P_Blue01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\P_Blue02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\P_Blue03.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\P_Blue04.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\P_Green01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\P_Green02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\P_Green03.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\P_Green04.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\P_Medicine01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\P_Medicine02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\P_Medicine03.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\P_Medicine04.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\P_Medicine05.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\P_Medicine06.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\P_Medicine07.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\P_Medicine08.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\P_Medicine09.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\P_Orange01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\P_Orange02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\P_Orange03.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\P_Orange04.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\P_Pink01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\P_Pink02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\P_Pink03.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\P_Pink04.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\P_Red01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\P_Red02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\P_Red03.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\P_Red04.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\P_White01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\P_White02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\P_White03.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\P_White04.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\P_Yellow01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\P_Yellow02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\P_Yellow03.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\P_Yellow04.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Bow01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Bow02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Bow03.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Bow04.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Bow05.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Bow06.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Bow07.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Bow08.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Bow09.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Bow10.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Bow11.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Bow12.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Bow13.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Bow14.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Buff01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Buff02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Buff03.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Buff04.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Buff05.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Buff06.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Buff07.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Buff08.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Buff09.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Buff10.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Buff11.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Buff12.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Buff13.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Buff14.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Death01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Death02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Earth01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Earth02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Earth03.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Earth04.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Earth05.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Earth06.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Earth07.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Fire01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Fire02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Fire03.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Fire04.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Fire05.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Fire06.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Fire07.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Holy01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Holy02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Holy03.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Holy04.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Holy05.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Holy06.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Holy07.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Ice01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Ice02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Ice03.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Ice04.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Ice05.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Ice06.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Ice07.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Light01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Light02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Light03.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Magic01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Magic02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Magic03.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Magic04.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Physic01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Physic02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Poison01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Poison02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Poison03.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Poison04.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Poison05.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Poison06.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Poison07.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Shadow01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Shadow02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Shadow03.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Shadow04.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Shadow05.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Shadow06.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Shadow07.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Sword01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Sword02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Sword03.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Sword04.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Sword05.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Sword06.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Sword07.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Sword08.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Sword09.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Sword10.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Thunder01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Thunder02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Thunder03.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Thunder04.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Thunder05.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Thunder06.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Thunder07.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Water01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Water02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Water03.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Water04.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Water05.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Water06.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Water07.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Wind01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Wind02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Wind03.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Wind04.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Wind05.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Wind06.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\S_Wind07.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Axe001.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Axe002.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Axe003.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Axe004.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Axe005.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Axe006.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Axe007.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Axe008.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Axe009.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Axe010.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Axe011.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Axe012.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Axe013.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Axe014.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Book01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Book02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Book03.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Book04.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Book05.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Book06.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Book07.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Bow01.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Bow02.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Bow03.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Bow04.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Bow05.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Bow06.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Bow07.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Bow08.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Bow09.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Bow10.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Bow11.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Bow12.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Bow13.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Bow14.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Dagger001.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Dagger002.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Dagger003.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Dagger004.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Dagger005.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Dagger006.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Dagger007.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Dagger008.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Dagger009.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Dagger010.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Dagger011.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Dagger012.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Dagger013.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Dagger014.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Dagger015.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Dagger016.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Dagger017.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Dagger018.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Dagger019.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Dagger020.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Dagger021.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Fist001.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Fist002.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Fist003.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Fist004.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Fist005.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Gun001.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Gun002.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Gun003.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Mace001.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Mace002.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Mace003.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Mace004.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Mace005.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Mace006.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Mace007.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Mace008.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Mace009.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Mace010.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Mace011.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Mace012.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Mace013.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Mace014.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Spear001.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Spear002.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Spear003.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Spear004.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Spear005.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Spear006.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Spear007.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Spear008.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Spear009.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Spear010.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Spear011.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Spear012.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Spear013.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Spear014.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Sword001.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Sword002.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Sword003.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Sword004.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Sword005.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Sword006.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Sword007.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Sword008.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Sword009.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Sword010.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Sword011.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Sword012.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Sword013.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Sword014.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Sword015.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Sword016.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Sword017.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Sword018.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Sword019.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Sword020.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Sword021.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Throw001.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Throw002.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Throw003.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Throw004.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\Icons\\W_Throw05.png"));

            int[] texMageFireball =
            {
                DatabaseManager.Textures.Add(new TextureInfo("Textures\\SP103B01.png")),
                DatabaseManager.Textures.Add(new TextureInfo("Textures\\SP103B02.png")),
                DatabaseManager.Textures.Add(new TextureInfo("Textures\\SP103B03.png")),
                DatabaseManager.Textures.Add(new TextureInfo("Textures\\SP103B04.png")),
                DatabaseManager.Textures.Add(new TextureInfo("Textures\\SP103B05.png")),
                DatabaseManager.Textures.Add(new TextureInfo("Textures\\SP103B06.png")),
            };
            int[] texMageIce =
            {
                DatabaseManager.Textures.Add(new TextureInfo("Textures\\sp07h10.png")),
                DatabaseManager.Textures.Add(new TextureInfo("Textures\\sp07h11.png")),
                DatabaseManager.Textures.Add(new TextureInfo("Textures\\sp07h12.png")),
                DatabaseManager.Textures.Add(new TextureInfo("Textures\\sp07h13.png")),
                DatabaseManager.Textures.Add(new TextureInfo("Textures\\sp07h14.png")),
            };
            int texMageCircle = DatabaseManager.Textures.Add(new TextureInfo("Textures\\flamecircle.png"));
            int[] texMageCircleParticles =
            {
                DatabaseManager.Textures.Add(new TextureInfo("Textures\\flamecirclept1.png")),
                DatabaseManager.Textures.Add(new TextureInfo("Textures\\flamecirclept2.png")),
                DatabaseManager.Textures.Add(new TextureInfo("Textures\\flamecirclept3.png")),
                DatabaseManager.Textures.Add(new TextureInfo("Textures\\flamecirclept4.png")),
                DatabaseManager.Textures.Add(new TextureInfo("Textures\\flamecirclept5.png")),
            };
            int[] texMageEarthquake =
            {
                DatabaseManager.Textures.Add(new TextureInfo("Textures\\SP102B01.png")),
                DatabaseManager.Textures.Add(new TextureInfo("Textures\\SP102B02.png")),
                DatabaseManager.Textures.Add(new TextureInfo("Textures\\SP102B03.png")),
                DatabaseManager.Textures.Add(new TextureInfo("Textures\\SP102B04.png")),
                DatabaseManager.Textures.Add(new TextureInfo("Textures\\SP102B05.png")),
                DatabaseManager.Textures.Add(new TextureInfo("Textures\\SP102B06.png")),
            };
            int texMageEarthquakeGround = DatabaseManager.Textures.Add(new TextureInfo("Textures\\earthquake.png"));
            #endregion

            #region Materials
            int matGroundBase = DatabaseManager.Materials.Add(new Material(texGroundBase, texGroundBaseN));
            int matGroundLayer0 = DatabaseManager.Materials.Add(new Material(texGroundLayer0, texGroundLayer0N));
            int matGroundLayer1 = DatabaseManager.Materials.Add(new Material(texGroundLayer1, texGroundLayer1N));
            #endregion

            #region Particle Systems
            int psFireball = DatabaseManager.ParticleSystems.Add(new ParticleSystem()
            {
                Emitters = new List<ParticleEmitter>
                {
                    new ParticleEmitter()
                    {
                        ParticleTextures = new[] { 3 },
                        ParticleSize = new FloatBounds(0.5f, 0.66f),
                        SizeMultiplier = 0.75f,

                        SourcePosition = ParticleEmitterSourcePosition.EmitterPosition,
                        SourcePositionX = new FloatBounds(-0.01f, 0.01f),
                        SourcePositionY = new FloatBounds(-0.01f, 0.01f),
                        SourcePositionZ = new FloatBounds(-0.01f, 0.01f),
                        SpawnDelay = new FloatBounds(1.0f / 35, 1.0f / 50),

                        DirectionYaw = new FloatBounds(-MathHelper.Pi, MathHelper.Pi),
                        DirectionPitch = new FloatBounds(-MathHelper.PiOver4, MathHelper.PiOver2),
                        Velocity = new FloatBounds(0.4f, 0.6f),
                        VelocityMultiplier = 0.75f,

                        TotalParticleCount = new IntBounds(0),
                        TimeToLive = new FloatBounds(1, 2),
                        ParticleLimit = 100,
                    },
                },
            });
            int psBlazingFire = DatabaseManager.ParticleSystems.Add(new ParticleSystem()
            {
                Emitters = new List<ParticleEmitter>
                {
                    new ParticleEmitter()
                    {
                        ParticleTextures = new[] { texParticleStar },
                        ParticleSize = new FloatBounds(0.5f, 1.5f),
                        SizeMultiplier = 0.5f,

                        SourcePosition = ParticleEmitterSourcePosition.RandomInRadius,
                        SourcePositionX = new FloatBounds(4.75f, 5),
                        SourcePositionY = new FloatBounds(-MathHelper.Pi, MathHelper.Pi),
                        SourcePositionZ = new FloatBounds(0, 0.25f),
                        SpawnDelay = new FloatBounds(1.0f / 150, 1.0f / 200),

                        DirectionYaw = new FloatBounds(-MathHelper.Pi, MathHelper.Pi),
                        DirectionPitch = new FloatBounds(MathHelper.PiOver2, MathHelper.PiOver2),
                        Velocity = new FloatBounds(2.0f, 2.5f),
                        VelocityMultiplier = 0.5f,

                        TotalParticleCount = new IntBounds(0),
                        TimeToLive = new FloatBounds(2, 3),
                        ParticleLimit = 250,
                    },
                },
            });
            int psToxicCloud = DatabaseManager.ParticleSystems.Add(new ParticleSystem()
            {
                Emitters = new List<ParticleEmitter>
                {
                    new ParticleEmitter()
                    {
                        ParticleTextures = texToxicFumes,
                        ParticleSize = new FloatBounds(4, 6),
                        SizeMultiplier = 0.9f,

                        SourcePosition = ParticleEmitterSourcePosition.RandomInRadius,
                        SourcePositionX = new FloatBounds(0, 10),
                        SourcePositionY = new FloatBounds(-MathHelper.Pi, MathHelper.Pi),
                        SourcePositionZ = new FloatBounds(2),
                        SpawnDelay = new FloatBounds(1.0f / 10, 1.0f / 20),

                        DirectionYaw = new FloatBounds(-MathHelper.Pi, MathHelper.Pi),
                        Velocity = new FloatBounds(0.1f, 0.3f),
                        VelocityMultiplier = 0.9f,

                        TotalParticleCount = new IntBounds(0),
                        TimeToLive = new FloatBounds(3, 4),
                        ParticleLimit = 100,
                    },
                },
            });
            int psMoltenDaggerAoE = DatabaseManager.ParticleSystems.Add(new ParticleSystem()
            {
                Emitters = new List<ParticleEmitter>
                {
                    new ParticleEmitter()
                    {
                        ParticleTextures = texLavaSpark,
                        ParticleSize = new FloatBounds(0.1f, 0.3f),
                        SizeMultiplier = 0.5f,

                        SourcePosition = ParticleEmitterSourcePosition.RandomInRadius,
                        SourcePositionX = new FloatBounds(0, 4),
                        SourcePositionY = new FloatBounds(-MathHelper.Pi, MathHelper.Pi),
                        SourcePositionZ = new FloatBounds(0, 0.25f),
                        SpawnDelay = new FloatBounds(1.0f / 10, 1.0f / 25),

                        DirectionYaw = new FloatBounds(-MathHelper.Pi, MathHelper.Pi),
                        DirectionPitch = new FloatBounds(MathHelper.PiOver2 * 0.75f, MathHelper.PiOver2),
                        Velocity = new FloatBounds(1, 3),
                        VelocityMultiplier = 0.5f,
                        GravityInfluence = -2,

                        TotalParticleCount = new IntBounds(0),
                        TimeToLive = new FloatBounds(2, 3),
                        ParticleLimit = 100,
                    },
                },
            });
            int psMageFireball = DatabaseManager.ParticleSystems.Add(new ParticleSystem()
            {
                Emitters = new List<ParticleEmitter>
                {
                    new ParticleEmitter()
                    {
                        ParticleTextures = texMageFireball,
                        ParticleSize = new FloatBounds(0.2f, 0.4f),
                        SizeMultiplier = 0.75f,

                        SourcePosition = ParticleEmitterSourcePosition.EmitterPosition,
                        SourcePositionX = new FloatBounds(-0.01f, 0.01f),
                        SourcePositionY = new FloatBounds(-0.01f, 0.01f),
                        SourcePositionZ = new FloatBounds(-0.01f, 0.01f),
                        SpawnDelay = new FloatBounds(1.0f / 35, 1.0f / 50),

                        DirectionYaw = new FloatBounds(-MathHelper.Pi, MathHelper.Pi),
                        DirectionPitch = new FloatBounds(-MathHelper.PiOver4, MathHelper.PiOver2),
                        Velocity = new FloatBounds(0.1f, 0.25f),
                        VelocityMultiplier = 0.75f,

                        TotalParticleCount = new IntBounds(0),
                        TimeToLive = new FloatBounds(1, 2),
                        ParticleLimit = 100,
                    },
                },
            });
            int psMageFireballHit = DatabaseManager.ParticleSystems.Add(new ParticleSystem()
            {
                Emitters = new List<ParticleEmitter>
                {
                    new ParticleEmitter()
                    {
                        ParticleTextures = texMageFireball,
                        ParticleSize = new FloatBounds(0.2f, 0.4f),
                        SizeMultiplier = 0.75f,

                        SourcePosition = ParticleEmitterSourcePosition.EmitterPosition,
                        SourcePositionX = new FloatBounds(-0.01f, 0.01f),
                        SourcePositionY = new FloatBounds(-0.01f, 0.01f),
                        SourcePositionZ = new FloatBounds(-0.01f, 0.01f),
                        SpawnDelay = new FloatBounds(0),

                        DirectionYaw = new FloatBounds(-MathHelper.Pi, MathHelper.Pi),
                        DirectionPitch = new FloatBounds(-MathHelper.PiOver4, MathHelper.PiOver2),
                        Velocity = new FloatBounds(0.3f, 0.75f),
                        VelocityMultiplier = 0.5f,

                        TotalParticleCount = new IntBounds(50),
                        TimeToLive = new FloatBounds(1, 2),
                        ParticleLimit = 50,
                    },
                },
            });
            int psMageIce = DatabaseManager.ParticleSystems.Add(new ParticleSystem()
            {
                Emitters = new List<ParticleEmitter>
                {
                    new ParticleEmitter()
                    {
                        ParticleTextures = new[] { texParticleStar },
                        ParticleSize = new FloatBounds(0.075f, 0.125f),
                        SizeMultiplier = 0.5f,

                        SourcePosition = ParticleEmitterSourcePosition.EmitterPosition,
                        SourcePositionX = new FloatBounds(-0.01f, 0.01f),
                        SourcePositionY = new FloatBounds(-0.01f, 0.01f),
                        SourcePositionZ = new FloatBounds(-0.01f, 0.01f),
                        SpawnDelay = new FloatBounds(1.0f / 35, 1.0f / 50),

                        DirectionYaw = new FloatBounds(-MathHelper.Pi, MathHelper.Pi),
                        DirectionPitch = new FloatBounds(-MathHelper.PiOver4, MathHelper.PiOver2),
                        Velocity = new FloatBounds(0.2f, 0.25f),
                        VelocityMultiplier = 0.5f,

                        TotalParticleCount = new IntBounds(0),
                        TimeToLive = new FloatBounds(1, 2),
                        ParticleLimit = 100,
                    },
                },
            });
            int psMageIceHit = DatabaseManager.ParticleSystems.Add(new ParticleSystem()
            {
                Emitters = new List<ParticleEmitter>
                {
                    new ParticleEmitter()
                    {
                        ParticleTextures = new[] { texParticleStar },
                        ParticleSize = new FloatBounds(0.075f, 0.125f),
                        SizeMultiplier = 0.5f,

                        SourcePosition = ParticleEmitterSourcePosition.EmitterPosition,
                        SourcePositionX = new FloatBounds(-0.01f, 0.01f),
                        SourcePositionY = new FloatBounds(-0.01f, 0.01f),
                        SourcePositionZ = new FloatBounds(-0.01f, 0.01f),
                        SpawnDelay = new FloatBounds(0),

                        DirectionYaw = new FloatBounds(-MathHelper.Pi, MathHelper.Pi),
                        DirectionPitch = new FloatBounds(-MathHelper.PiOver4, MathHelper.PiOver2),
                        Velocity = new FloatBounds(0.6f, 0.75f),
                        VelocityMultiplier = 0.5f,

                        TotalParticleCount = new IntBounds(50),
                        TimeToLive = new FloatBounds(1, 2),
                        ParticleLimit = 150,
                    },
                },
            });
            int psMageCircle = DatabaseManager.ParticleSystems.Add(new ParticleSystem()
            {
                Emitters = new List<ParticleEmitter>
                {
                    new ParticleEmitter()
                    {
                        ParticleTextures = texMageCircleParticles,
                        ParticleSize = new FloatBounds(0.5f, 1.5f),
                        SizeMultiplier = 0.5f,

                        SourcePosition = ParticleEmitterSourcePosition.RandomInRadius,
                        SourcePositionX = new FloatBounds(2.45f, 2.75f),
                        SourcePositionY = new FloatBounds(-MathHelper.Pi, MathHelper.Pi),
                        SourcePositionZ = new FloatBounds(-0.25f, 0),
                        SpawnDelay = new FloatBounds(1.0f / 150, 1.0f / 200),

                        DirectionYaw = new FloatBounds(-MathHelper.Pi, MathHelper.Pi),
                        DirectionPitch = new FloatBounds(MathHelper.PiOver2, MathHelper.PiOver2),
                        Velocity = new FloatBounds(0.1f, 0.2f),
                        VelocityMultiplier = 1.75f,

                        TotalParticleCount = new IntBounds(0),
                        TimeToLive = new FloatBounds(2, 3),
                        ParticleLimit = 250,
                    },
                },
            });
            #endregion

            #region Models
            Model[][] raceModels = new Model[(byte)CreatureRace.Max][];

            int modelFireball = DatabaseManager.Models.Add(new SpriteModel()
            {
                DefaultTextureID = 3,
                Size = Vector3.One,
                ParticleSystem = DatabaseManager.ParticleSystems[psFireball],
            });
            for (byte race = 0; race < raceModels.Length; race++)
            {
                raceModels[race] = new Model[raceModelAnimations[race].Length];
                for (int i = 0; i < raceModels[race].Length; i++)
                {
                    Model model = new MultiAngleSpriteModel()
                    {
                        Origin = ModelOrigin.Bottom,
                        Animations = raceModelAnimations[race][i],
                        Size = Vector3.One,
                        RelativeSize = true,
                    };
                    raceModels[race][i] = model;
                    DatabaseManager.Models.Add(model);
                }
            }
            int modelBlazingFire = DatabaseManager.Models.Add(new EmptyModel()
            {
                ParticleSystem = DatabaseManager.ParticleSystems[psBlazingFire],
            });
            int modelTree = DatabaseManager.Models.Add(new WorldModel()
            {
                Filename = "Models\\LordaeronTree0S.obj",
                Flags = WorldModelFlags.FlipTextureV | WorldModelFlags.DoubleFaced,
                Origin = ModelOrigin.Bottom,
                Size = new Vector3(1 / 250f),
                RelativeSize = true,
                WindInfluenceZ = 1000,
            });
            int modelToxicCloud = DatabaseManager.Models.Add(new EmptyModel()
            {
                ParticleSystem = DatabaseManager.ParticleSystems[psToxicCloud],
            });
            int modelGrass1 = DatabaseManager.Models.Add(new WorldModel()
            {
                Filename = "Models\\grasspatch.obj",
                Flags = WorldModelFlags.DoubleFaced,
                Origin = ModelOrigin.Bottom,
                Size = new Vector3(0.5f),
                RelativeSize = true,
                WindInfluenceZ = 2,
            });
            int modelGrass2 = DatabaseManager.Models.Add(new WorldModel()
            {
                Filename = "Models\\grasspatch_2.obj",
                Flags = WorldModelFlags.DoubleFaced,
                Origin = ModelOrigin.Bottom,
                Size = new Vector3(0.4f),
                RelativeSize = true,
                WindInfluenceZ = 4,
            });
            int modelGrass3 = DatabaseManager.Models.Add(new WorldModel()
            {
                Filename = "Models\\grasspatch_3.obj",
                Flags = WorldModelFlags.DoubleFaced,
                Origin = ModelOrigin.Bottom,
                Size = new Vector3(0.25f),
                RelativeSize = true,
                WindInfluenceZ = 6,
            });
            int modelMoltenDaggerAoE = DatabaseManager.Models.Add(new DecalModel()
            {
                TextureID = texLavaPool,
                Size = 5,
                ParticleSystem = DatabaseManager.ParticleSystems[psMoltenDaggerAoE],
            });
            int modelMageFireball = DatabaseManager.Models.Add(new SpriteModel()
            {
                DefaultTextureID = texMageFireball[0],
                Animations = new[]
                {
                    new SpriteModelAnimation("Fly", 0.3f, texMageFireball), 
                },
                Size = new Vector3(0.5f),
                ParticleSystem = DatabaseManager.ParticleSystems[psMageFireball],
            });
            int modelMageFireballHit = DatabaseManager.Models.Add(new EmptyModel()
            {
                ParticleSystem = DatabaseManager.ParticleSystems[psMageFireballHit],
            });
            int modelMageIce = DatabaseManager.Models.Add(new SpriteModel()
            {
                DefaultTextureID = texMageIce[0],
                Animations = new[]
                {
                    new SpriteModelAnimation("Fly", 0.5f, texMageIce), 
                },
                Size = new Vector3(0.5f),
                ParticleSystem = DatabaseManager.ParticleSystems[psMageIce],
            });
            int modelMageIceHit = DatabaseManager.Models.Add(new EmptyModel()
            {
                ParticleSystem = DatabaseManager.ParticleSystems[psMageIceHit],
            });
            int modelMageCircle = DatabaseManager.Models.Add(new DecalModel()
            {
                TextureID = texMageCircle,
                Size = 3,
                ParticleSystem = DatabaseManager.ParticleSystems[psMageCircle],
            });
            int modelMageEarthquake = DatabaseManager.Models.Add(new SpriteModel()
            {
                DefaultTextureID = texMageEarthquake[0],
                Animations = new[]
                {
                    new SpriteModelAnimation("Fly", 0.5f, texMageEarthquake), 
                },
                Size = new Vector3(1),
            });
            int modelMageEarthquakeGround = DatabaseManager.Models.Add(new DecalModel()
            {
                TextureID = texMageEarthquakeGround,
                Size = 5,
            });
            int modelCultistShroudAura = DatabaseManager.Models.Add(new EmptyModel()
            {
                TimeAlive = 10,
                Light = new LightData(Color.White, -9, 5),
            });
            int modelCultistMeditationAura = DatabaseManager.Models.Add(new EmptyModel()
            {
                TimeAlive = 5,
                Light = new LightData(Color.White, -10, 5),
            });
            DatabaseManager.Models.Add(new WorldModel()
            {
                Filename = "Models\\AshenBrokenColumn0.OBJ",
                Flags = WorldModelFlags.FlipTextureV | WorldModelFlags.InvertNormals,
                Origin = ModelOrigin.Bottom,
                Size = new Vector3(1f / 160),
                RelativeSize = true,
            });
            DatabaseManager.Models.Add(new WorldModel()
            {
                Filename = "Models\\AshenBrokenColumn1.OBJ",
                Flags = WorldModelFlags.FlipTextureV | WorldModelFlags.InvertNormals,
                Origin = ModelOrigin.Bottom,
                Size = new Vector3(1f / 210),
                RelativeSize = true,
            });
            DatabaseManager.Models.Add(new WorldModel()
            {
                Filename = "Models\\AshenBrokenObilisk0.OBJ",
                Flags = WorldModelFlags.FlipTextureV | WorldModelFlags.InvertNormals,
                Origin = ModelOrigin.Bottom,
                Size = new Vector3(1f / 144),
                RelativeSize = true,
            });
            DatabaseManager.Models.Add(new WorldModel()
            {
                Filename = "Models\\AshenBrokenObilisk1.OBJ",
                Flags = WorldModelFlags.FlipTextureV | WorldModelFlags.InvertNormals,
                Origin = ModelOrigin.Bottom,
                Size = new Vector3(1f / 118),
                RelativeSize = true,
            });
            DatabaseManager.Models.Add(new WorldModel()
            {
                Filename = "Models\\AshenBush0.OBJ",
                Flags = WorldModelFlags.FlipTextureV | WorldModelFlags.DoubleFaced,
                Origin = ModelOrigin.Bottom,
                Size = new Vector3(1f / 115),
                RelativeSize = true,
                WindInfluenceZ = 230,
            });
            DatabaseManager.Models.Add(new WorldModel()
            {
                Filename = "Models\\AshenHollowStump.OBJ",
                Flags = WorldModelFlags.FlipTextureV | WorldModelFlags.InvertNormals,
                Origin = ModelOrigin.Bottom,
                Size = new Vector3(1f / 130),
                RelativeSize = true,
            });
            DatabaseManager.Models.Add(new WorldModel()
            {
                Filename = "Models\\AshenLogAngled.OBJ",
                Flags = WorldModelFlags.FlipTextureV | WorldModelFlags.InvertNormals,
                Origin = ModelOrigin.Bottom,
                Size = new Vector3(1f / 180),
                RelativeSize = true,
            });
            DatabaseManager.Models.Add(new WorldModel()
            {
                Filename = "Models\\AshenObilisk.OBJ",
                Flags = WorldModelFlags.FlipTextureV | WorldModelFlags.InvertNormals,
                Origin = ModelOrigin.Bottom,
                Size = new Vector3(1f / 200),
                RelativeSize = true,
            });
            DatabaseManager.Models.Add(new WorldModel()
            {
                Filename = "Models\\AshenShrooms0.OBJ",
                Flags = WorldModelFlags.FlipTextureV | WorldModelFlags.DoubleFaced,
                Origin = ModelOrigin.Bottom,
                Size = new Vector3(1f / 80),
                RelativeSize = true,
            });
            DatabaseManager.Models.Add(new WorldModel()
            {
                Filename = "Models\\AshenShrooms1.OBJ",
                Flags = WorldModelFlags.FlipTextureV | WorldModelFlags.DoubleFaced,
                Origin = ModelOrigin.Bottom,
                Size = new Vector3(1f / 80),
                RelativeSize = true,
            });
            DatabaseManager.Models.Add(new WorldModel()
            {
                Filename = "Models\\AshenShrooms2.OBJ",
                Flags = WorldModelFlags.FlipTextureV | WorldModelFlags.DoubleFaced,
                Origin = ModelOrigin.Bottom,
                Size = new Vector3(1f / 80),
                RelativeSize = true,
            });
            DatabaseManager.Models.Add(new WorldModel()
            {
                Filename = "Models\\AshenShrooms3.OBJ",
                Flags = WorldModelFlags.FlipTextureV | WorldModelFlags.DoubleFaced,
                Origin = ModelOrigin.Bottom,
                Size = new Vector3(1f / 80),
                RelativeSize = true,
            });

            foreach (KeyValuePair<int, Model> asset in DatabaseManager.Models)
                if (asset.Value.Type == ModelType.WorldModel)
                    ModelHelper.CollectMaterials(asset.Value as WorldModel, "Textures");
            #endregion

            #region Factions
            int factionPlayer = DatabaseManager.Factions.Add(new Faction("Player", Reaction.Neutral));
            int factionAggressiveToEverybody = DatabaseManager.Factions.Add(new Faction("Aggressive to everybody", Reaction.Hostile) { ReactionInsideFaction = Reaction.Hostile });
            int factionAggressiveToPlayer = DatabaseManager.Factions.Add(new Faction("Aggressive to player", Reaction.Friendly));
            int factionNPC = DatabaseManager.Factions.Add(new Faction("NPC", Reaction.Friendly) { ForceSameReactionFromOthers = true });
            int factionGoblin = DatabaseManager.Factions.Add(new Faction("Goblins", Reaction.Hostile));
            int factionWolf = DatabaseManager.Factions.Add(new Faction("Wolves", Reaction.Hostile));
            int factionRat = DatabaseManager.Factions.Add(new Faction("Rats", Reaction.Neutral));

            // Setting specific reactions
            Faction.SetReaction(factionAggressiveToPlayer, factionPlayer, Reaction.Hostile);
            Faction.SetReactionTo(factionGoblin, factionWolf, Reaction.Friendly);
            Faction.SetReactionTo(factionWolf, factionGoblin, Reaction.Neutral);
            Faction.SetReaction(factionWolf, factionRat, Reaction.Hostile);

            // Duel-specific factions
            staticData.DuelFactions = new[]
            {
                DatabaseManager.Factions.Add(new Faction("Player Duel 1", Reaction.Neutral)),
                DatabaseManager.Factions.Add(new Faction("Player Duel 2", Reaction.Neutral)),
                DatabaseManager.Factions.Add(new Faction("Player Duel 3", Reaction.Neutral)),
                DatabaseManager.Factions.Add(new Faction("Player Duel 4", Reaction.Neutral)),
                DatabaseManager.Factions.Add(new Faction("Player Duel 5", Reaction.Neutral)),
            };
            staticData.MaxDuelParticipants = staticData.DuelFactions.Length;
            foreach (int factionID in staticData.DuelFactions)
                Faction.SetReaction(factionID, factionPlayer, Reaction.Hostile);
            #endregion

            #region Spell Visuals
            int svFireball = DatabaseManager.SpellVisuals.Add(new SpellVisual
            {
                Flying = new SpellVisualSet { Model = DatabaseManager.Models[modelFireball] },
            });
            int svBlazingFire = DatabaseManager.SpellVisuals.Add(new SpellVisual
            {
                GroundIdle = new SpellVisualSet { Model = DatabaseManager.Models[modelBlazingFire] },
            });
            int svToxicCloud = DatabaseManager.SpellVisuals.Add(new SpellVisual
            {
                Flying = new SpellVisualSet { Model = DatabaseManager.Models[modelFireball] },
                GroundIdle = new SpellVisualSet { Model = DatabaseManager.Models[modelToxicCloud] },
            });
            int svMoltenDagger = DatabaseManager.SpellVisuals.Add(new SpellVisual
            {
                GroundIdle = new SpellVisualSet { Model = DatabaseManager.Models[modelMoltenDaggerAoE] },
            });
            int svGeneric = DatabaseManager.SpellVisuals.Add(new SpellVisual());
            int svGenericMelee = DatabaseManager.SpellVisuals.Add(new SpellVisual
            {
                Hit = new SpellVisualSet { CasterAnimation = "MeleeAttackHit", TargetAnimation = "MeleeHit" },
                Miss = new SpellVisualSet { CasterAnimation = "MeleeAttackMiss", TargetAnimation = "MeleeMiss" },
            });
            int svMageFireball = DatabaseManager.SpellVisuals.Add(new SpellVisual
            {
                Flying = new SpellVisualSet { Model = DatabaseManager.Models[modelMageFireball], ModelAnimation = "Fly" },
                Hit = new SpellVisualSet { Model = DatabaseManager.Models[modelMageFireballHit], TargetAnimation = "SpellHit" },
            });
            int svMageIce = DatabaseManager.SpellVisuals.Add(new SpellVisual
            {
                Flying = new SpellVisualSet { Model = DatabaseManager.Models[modelMageIce], ModelAnimation = "Fly" },
                Hit = new SpellVisualSet { Model = DatabaseManager.Models[modelMageIceHit], TargetAnimation = "SpellHit" },
            });
            int svMageCircle = DatabaseManager.SpellVisuals.Add(new SpellVisual
            {
                GroundIdle = new SpellVisualSet { Model = DatabaseManager.Models[modelMageCircle] },
            });
            int svMageEarthquake = DatabaseManager.SpellVisuals.Add(new SpellVisual
            {
                Flying = new SpellVisualSet { Model = DatabaseManager.Models[modelMageEarthquake], ModelAnimation = "Fly" },
                GroundIdle = new SpellVisualSet { Model = DatabaseManager.Models[modelMageEarthquakeGround] },
            });
            int svCultistShroud = DatabaseManager.SpellVisuals.Add(new SpellVisual
            {
                Hit = new SpellVisualSet { Model = DatabaseManager.Models[modelCultistShroudAura], TargetAnimation = "SpellHit" },
            });
            int svCultistMeditation = DatabaseManager.SpellVisuals.Add(new SpellVisual
            {
                Hit = new SpellVisualSet { Model = DatabaseManager.Models[modelCultistMeditationAura], TargetAnimation = "SpellHit" },
            });
            #endregion

            #region Spells
            int id = 0;
            int spellFireball = DatabaseManager.Spells.Add(++id, new SpellTemplate(Text.Make("Fireball", "{ruRU Name}"), Text.Make("Conjures a fireball that deals 10 fire damage to the target and 5 fire more damage to all enemies within 20 of the target.", "{ruRU description}"))
            {
                RequiredPowerType = PowerType.Energy,
                RequiredPowerAmount = 10,
                AcceptableTargets = SpellTargetMask.Hostile,
                Icon = 2,
                CastingTime = 0,
                Range = new FloatBounds(0, 30),
                Speed = 10,
                Visual = DatabaseManager.SpellVisuals[svFireball],
                LightData = new LightData(Color.Orange, 5, 7.5f),
                CooldownGroup = 1,
                GlobalCooldown = 1,
                Effects = new[]
                {
                    new SpellEffect(SpellEffectType.SchoolDamage, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        IntValue1 = 10,
                        IntValue2 = 10,
                        IntValue3 = (int)DamageSchool.Fire,
                    },
                    new SpellEffect(SpellEffectType.SchoolDamage, SpellEffectTarget.HostilesAOE, SpellEffectTarget.SpellTarget)
                    {
                        IntValue1 = 5,
                        IntValue2 = 5,
                        IntValue3 = (int)DamageSchool.Fire,
                    },
                },
            });
            int spellHaste = DatabaseManager.Spells.Add(++id, new SpellTemplate(Text.Make("Haste", "{ruRU Name}"), Text.Make("Increases target's run speed by 100% and attack speed by 50% for 5 seconds.", "{ruRU description}"))
            {
                RequiredPowerType = PowerType.Energy,
                RequiredPowerAmount = 20,
                AcceptableTargets = SpellTargetMask.AllAlive,
                Icon = 4,
                CastingTime = 0.25f,
                Range = new FloatBounds(0, 50),
                Speed = 10,
                Visual = DatabaseManager.SpellVisuals[svFireball],
                AuraDuration = 5,
                CooldownGroup = 1,
                CooldownDuration = 10,
                GlobalCooldown = 1,
                Effects = new[]
                {
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.ModRunSpeed,
                        FloatValue1 = 2,
                    },
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.ModWeaponSpeedPercent,
                        FloatValue1 = 0.5f,
                    },
                },
            });
            int spellBlazingFire = DatabaseManager.Spells.Add(++id, new SpellTemplate(Text.Make("Blazing Fire", "{ruRU Name}"), Text.Make("Sets an area 10 meters in diameter ablaze, dealing 5 damage each second to anyone inside it for 5 seconds.", "{ruRU description}"))
            {
                RequiredPowerType = PowerType.Energy,
                RequiredPowerAmount = 35,
                Flags = SpellFlags.GroundTargeted | SpellFlags.IgnoreFacing,
                AcceptableTargets = SpellTargetMask.AllAlive,
                Icon = 5,
                GroundTargetTexture = texGroundTarget,
                CastingTime = 0.25f,
                Range = new FloatBounds(0, 50),
                Radius = 5,
                Visual = DatabaseManager.SpellVisuals[svBlazingFire],
                LightData = new LightData(Color.White, 3, 15),
                AreaEffectDuration = 10,
                CooldownGroup = 1,
                CooldownDuration = 5,
                GlobalCooldown = 1,
                Effects = new[]
                {
                    new SpellEffect(SpellEffectType.AuraAreaEffect, SpellEffectTarget.GroundTarget, SpellEffectTarget.AllAOE)
                    {
                        AuraType = AuraType.ModRunSpeed,
                        FloatValue1 = 3,
                    },
                },
            });
            int spellToxicCloud = DatabaseManager.Spells.Add(++id, new SpellTemplate(Text.Make("Toxic Cloud", "{ruRU Name}"), Text.Make("Throw a ball with compressed toxins inside at the enemy, exploding on impact, dealing 10 ground damage and covering area within 5 meters of the target with deadly toxins for 5 seconds, dealing 3 ground damage every second.", "{ruRU description}"))
            {
                AcceptableTargets = SpellTargetMask.Attackable,
                Icon = 6,
                GroundTargetTexture = texGroundTarget,
                CastingTime = 0.2f,
                Range = new FloatBounds(2, 30),
                Radius = 10,
                Visual = DatabaseManager.SpellVisuals[svToxicCloud],
                LightData = new LightData(Color.DarkGreen, 20),
                AreaEffectDuration = 5,
                CooldownGroup = 1,
                CooldownDuration = 2,
                GlobalCooldown = 1,
                Effects = new[]
                {
                    new SpellEffect(SpellEffectType.PeriodicAreaEffect, SpellEffectTarget.SpellTarget, SpellEffectTarget.HostilesAOE)
                    {
                        AreaEffectPeriodic = 1,
                        IntValue1 = id + 1,
                    },
                },
            });
            DatabaseManager.Spells.Add(++id, new SpellTemplate(Text.Make("Toxic Fumes", "{ruRU Name}"), Text.Make("Inhaling toxic fumes deals 3 ground damage.", "{ruRU description}"))
            {
                Effects = new[]
                {
                    new SpellEffect(SpellEffectType.SchoolDamage, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        IntValue1 = 3,
                        IntValue2 = 3,
                        IntValue3 = (int)DamageSchool.Ground,
                    },
                },
            });
            int spellItemWeaponSpeedPct20 = DatabaseManager.Spells.Add(++id, new SpellTemplate(Text.Make("[Item] Main Weapon Speed +20%"), Text.Make("Increases attack speed for a weapon in your main hand by 20%.", "{ruRU description}"))
            {
                AcceptableTargets = SpellTargetMask.Self,
                Flags = SpellFlags.HiddenInLog | SpellFlags.HiddenAura,
                Effects = new[]
                {
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.Caster, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.ModWeaponSpeedPercent,
                        IntValue1 = 1,
                        FloatValue1 = -0.2f,
                    },
                },
            });
            int spellItemPoisonAxe = DatabaseManager.Spells.Add(++id, new SpellTemplate(Text.Make("[Item] Poison Axe"), Text.Make("Your melee attacks have 10% chance to poison the enemy. Poison deals 3-5 ground damage every second for 20 seconds.", "Ваши атаки в ближнем бою с шансом 10% отравляют врага. Отравление наносит 3-5 единицы урона земли каждую секунду на протяжении 20 секунд."))
            {
                AuraIcon = texIcons + 313,
                AcceptableTargets = SpellTargetMask.Self,
                Flags = SpellFlags.HiddenInLog | SpellFlags.HiddenAura,
                Effects = new[]
                {
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.Caster, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.MeleeHitProc,
                        IntValue1 = id + 1,
                        FloatValue1 = 0.1f,
                    },
                },
            });
            DatabaseManager.Spells.Add(++id, new SpellTemplate(Text.Make("Poison"), Text.Make("Was poisoned by Poison Axe, dealing 3-5 (+1/10 Attack Power) ground damage every second.", "Был отравлен Покрытым ядом топором, получает 3-5 (+1/10 силы атаки) единицы урона земли каждую секунду."))
            {
                AuraIcon = texIcons + 313,
                AcceptableTargets = SpellTargetMask.Hostile,
                Range = new FloatBounds(0, 100),
                AuraDuration = 20,
                Effects = new[]
                {
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.PeriodicTriggerSpellFromCaster,
                        IntValue1 = id + 1,
                        TriggerSpellPeriodic = 1,
                    },
                },
            });
            DatabaseManager.Spells.Add(++id, new SpellTemplate(Text.Make("Poison"), Text.Make(""))
            {
                AcceptableTargets = SpellTargetMask.Hostile,
                Effects = new[]
                {
                    new SpellEffect(SpellEffectType.SchoolDamage, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        IntValue1 = 3,
                        IntValue2 = 5,
                        IntValue3 = (int)DamageSchool.Ground,
                        FloatValue2 = 0.1f,
                    },
                },
                Range = FloatBounds.Infinite,
            });
            int spellItemSpark = DatabaseManager.Spells.Add(++id, new SpellTemplate(Text.Make("[Item] Spark"), Text.Make("Your melee attacks have 30% chance to deal 3-8 (+1/5 Attack Power) air damage to all enemies within 5 meters.", "Ваши атаки в ближнем бою с шансом 30% могут нанести 3-8 (+1/5 силы атаки) единиц урона от стихии воздуха всем врагам в радиусе 5 метров."))
            {
                AcceptableTargets = SpellTargetMask.Self,
                Flags = SpellFlags.HiddenInLog | SpellFlags.HiddenAura,
                Effects = new[]
                {
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.Caster, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.MeleeHitProc,
                        IntValue1 = id + 1,
                        FloatValue1 = 0.3f,
                    },
                },
            });
            DatabaseManager.Spells.Add(++id, new SpellTemplate(Text.Make("Spark"), Text.Make(""))
            {
                AcceptableTargets = SpellTargetMask.Hostile,
                Range = new FloatBounds(0, 100),
                Radius = 5,
                Effects = new[]
                {
                    new SpellEffect(SpellEffectType.SchoolDamage, SpellEffectTarget.HostilesAOE, SpellEffectTarget.Caster)
                    {
                        IntValue1 = 3,
                        IntValue2 = 8,
                        IntValue3 = (int)DamageSchool.Air,
                        FloatValue2 = 0.2f,
                    },
                },
            });
            int spellItemMoltenDagger = DatabaseManager.Spells.Add(++id, new SpellTemplate(Text.Make("[Item] Molten Dagger"), Text.Make("Your melee attacks have 10% chance to create a pool of lava which damages enemies for 10-15 every second.", "Ваши атаки в ближнем бою с шансом 10% могут создать лужу лавы, наносящую ежесекундно 10-15 единиц урона от стихии огня всем врагам в ней."))
            {
                AcceptableTargets = SpellTargetMask.Self,
                Flags = SpellFlags.HiddenInLog | SpellFlags.HiddenAura,
                Effects = new[]
                {
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.Caster, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.MeleeHitProc,
                        IntValue1 = id + 1,
                        FloatValue1 = 0.1f,
                    },
                },
            });
            DatabaseManager.Spells.Add(++id, new SpellTemplate(Text.Make("Molten Dagger AE"), Text.Make(""))
            {
                AcceptableTargets = SpellTargetMask.Hostile,
                Range = new FloatBounds(0, 100),
                Radius = 5,
                Visual = DatabaseManager.SpellVisuals[svMoltenDagger],
                LightData = new LightData(Color.Orange, 3, 10),
                AreaEffectDuration = 5,
                Effects = new[]
                {
                    new SpellEffect(SpellEffectType.PeriodicAreaEffect, SpellEffectTarget.SpellTarget, SpellEffectTarget.HostilesAOE)
                    {
                        AreaEffectPeriodic = 1,
                        IntValue1 = id + 1,
                    },
                },
            });
            DatabaseManager.Spells.Add(++id, new SpellTemplate(Text.Make("Molten Dagger AE Trigger"), Text.Make(""))
            {
                Effects = new[]
                {
                    new SpellEffect(SpellEffectType.SchoolDamage, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        IntValue1 = 10,
                        IntValue2 = 15,
                        IntValue3 = (int)DamageSchool.Fire,
                    },
                },
            });
            int spellItemPotionHealth = DatabaseManager.Spells.Add(++id, new SpellTemplate(Text.Make("[Item] Health Potion +20"), Text.Make("Restores 20-40 health points when drank.", "Восстанавливает 20-40 единиц здоровья при питье."))
            {
                AcceptableTargets = SpellTargetMask.Self,
                Flags = SpellFlags.HiddenInLog | SpellFlags.HiddenAura | SpellFlags.AutoTargetSelf,
                Effects = new[]
                {
                    new SpellEffect(SpellEffectType.Heal, SpellEffectTarget.Caster, SpellEffectTarget.None)
                    {
                        IntValue1 = 20,
                        IntValue2 = 40,
                    },
                },
            });
            int spellItemPotionMana = DatabaseManager.Spells.Add(++id, new SpellTemplate(Text.Make("[Item] Mana Potion +20"), Text.Make("Restores 20-40 mana points when drank.", "Восстанавливает 20-40 единиц маны при питье."))
            {
                AcceptableTargets = SpellTargetMask.Self,
                Flags = SpellFlags.HiddenInLog | SpellFlags.HiddenAura | SpellFlags.AutoTargetSelf,
                Effects = new[]
                {
                    new SpellEffect(SpellEffectType.RestorePower, SpellEffectTarget.Caster, SpellEffectTarget.None)
                    {
                        IntValue1 = 20,
                        IntValue2 = 40,
                        IntValue3 = (int)PowerType.Mana,
                    },
                },
            });
            int spellItemPotionEnergy = DatabaseManager.Spells.Add(++id, new SpellTemplate(Text.Make("[Item] Energy Potion +20"), Text.Make("Restores 20-40 energy points when drank.", "Восстанавливает 20-40 единиц энергии при питье."))
            {
                AcceptableTargets = SpellTargetMask.Self,
                Flags = SpellFlags.HiddenInLog | SpellFlags.HiddenAura | SpellFlags.AutoTargetSelf,
                Effects = new[]
                {
                    new SpellEffect(SpellEffectType.RestorePower, SpellEffectTarget.Caster, SpellEffectTarget.None)
                    {
                        IntValue1 = 20,
                        IntValue2 = 40,
                        IntValue3 = (int)PowerType.Energy,
                    },
                },
            });
            int spellItemPotionSpeed = DatabaseManager.Spells.Add(++id, new SpellTemplate(Text.Make("Speed Potion", "Зелье скорости"), Text.Make("Increases speed by 50% for 20 secs.", "Увеличивает скорость на 50% на протяжении 20 секунд."))
            {
                AuraIcon = texIcons + 168,
                AcceptableTargets = SpellTargetMask.Self,
                Flags = SpellFlags.AutoTargetSelf,
                AuraDuration = 20,
                Effects = new[]
                {
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.Caster, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.ModWalkSpeed,
                        FloatValue1 = 1.5f,
                    },
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.Caster, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.ModRunSpeed,
                        FloatValue1 = 1.5f,
                    },
                },
            });
            int spellWarriorStrike = DatabaseManager.Spells.Add(++id, new SpellTemplate(Text.Make("Forceful Strike"), Text.Make("Deals 150% weapon damage to the victim with a 10% chance to stun them for 5 seconds."))
            {
                AcceptableTargets = SpellTargetMask.Attackable,
                AuraDuration = 5,
                AuraIcon = texIcons + 271,
                Category = SpellCategory.Melee,
                CooldownDuration = 5,
                CooldownGroup = 1,
                Effects = new[]
                {
                    new SpellEffect(SpellEffectType.WeaponDamage, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        IntValue1 = (int)DamageSchool.Physical,
                        FloatValue1 = 1.5f,
                    },
                    new SpellEffect(0.1f, SpellEffectType.ApplyAura, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.Stun,
                    },
                },
                GlobalCooldown = 1,
                Icon = texIcons + 271,
                Range = new FloatBounds(0, 2),
                RequiredPowerType = PowerType.Energy,
                RequiredPowerAmount = 20,
                Visual = DatabaseManager.SpellVisuals[svGenericMelee],
            });
            int spellWarriorThrow = DatabaseManager.Spells.Add(++id, new SpellTemplate(Text.Make("Throw Weapon"), Text.Make("Deals 75% weapon damage to the victim at range."))
            {
                AcceptableTargets = SpellTargetMask.Attackable,
                CastingTime = 0.5f,
                Category = SpellCategory.Ability,
                CooldownDuration = 10,
                CooldownGroup = 1,
                Effects = new[]
                {
                    new SpellEffect(SpellEffectType.WeaponDamage, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        IntValue1 = (int)DamageSchool.Physical,
                        FloatValue1 = 0.75f,
                    },
                },
                GlobalCooldown = 1,
                Icon = texIcons + 299,
                Range = new FloatBounds(2, 15),
                RequiredPowerType = PowerType.Energy,
                RequiredPowerAmount = 10,
                Visual = DatabaseManager.SpellVisuals[svGeneric],
            });
            int spellWarriorDefense = DatabaseManager.Spells.Add(++id, new SpellTemplate(Text.Make("Steel Defense"), Text.Make("Reduces all incoming damage by 50% for 5 seconds, but reduces movement speed by 75%."))
            {
                AcceptableTargets = SpellTargetMask.Self,
                AuraDuration = 5,
                AuraIcon = texIcons + 50,
                Category = SpellCategory.Ability,
                CooldownDuration = 30,
                CooldownGroup = 3,
                Effects = new[]
                {
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.ModDamageTakenPct,
                        FloatValue1 = -0.5f,
                    },
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.ModWalkSpeed,
                        FloatValue1 = 0.25f,
                    },
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.ModRunSpeed,
                        FloatValue1 = 0.25f,
                    },
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.ModSwimSpeed,
                        FloatValue1 = 0.25f,
                    },
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.ModFlySpeed,
                        FloatValue1 = 0.25f,
                    },
                },
                Flags = SpellFlags.AutoTargetSelf,
                GlobalCooldown = 1,
                Icon = texIcons + 50,
                RequiredPowerType = PowerType.Energy,
                RequiredPowerAmount = 35,
                Visual = DatabaseManager.SpellVisuals[svGeneric],
            });
            int spellWarriorFrenzy = DatabaseManager.Spells.Add(++id, new SpellTemplate(Text.Make("Frenzy"), Text.Make("Increases weapon speed by 100% and movement speed by 25% for 10 seconds, but also increases damage taken by 25%."))
            {
                AcceptableTargets = SpellTargetMask.Self,
                AuraDuration = 10,
                AuraIcon = texIcons + 211,
                Category = SpellCategory.Mind,
                CooldownDuration = 60,
                CooldownGroup = 4,
                Effects = new[]
                {
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.ModWeaponSpeedPercent,
                        FloatValue1 = -0.5f,
                    },
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.ModDamageTakenPct,
                        FloatValue1 = 0.25f,
                    },
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.ModRunSpeed,
                        FloatValue1 = 1.25f,
                    },
                },
                Flags = SpellFlags.AutoTargetSelf,
                GlobalCooldown = 1,
                Icon = texIcons + 211,
                RequiredPowerType = PowerType.Energy,
                RequiredPowerAmount = 30,
                Visual = DatabaseManager.SpellVisuals[svGeneric],
            });
            int spellRogueRapid = DatabaseManager.Spells.Add(++id, new SpellTemplate(Text.Make("Rapid Strikes"), Text.Make("Perform 5 strikes over 1.5 sec dealing 30% weapon damage each."))
            {
                AcceptableTargets = SpellTargetMask.Attackable,
                AuraDuration = 1.5f,
                Category = SpellCategory.Melee,
                CooldownDuration = 5,
                CooldownGroup = 1,
                Effects = new[]
                {
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.Caster, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.PeriodicTriggerSpell,
                        IntValue1 = id + 1,
                        TriggerSpellPeriodic = 0.3f,
                    },
                },
                Flags = SpellFlags.HiddenAura,
                GlobalCooldown = 1,
                Icon = texIcons + 276,
                Range = new FloatBounds(0, 2),
                RequiredPowerType = PowerType.Energy,
                RequiredPowerAmount = 20,
            });
            DatabaseManager.Spells.Add(++id, new SpellTemplate(Text.Make("Rapid Strikes"), Text.Make("Perform 5 strikes over 1.5 sec dealing 30% weapon damage each."))
            {
                AcceptableTargets = SpellTargetMask.Self,
                Category = SpellCategory.Melee,
                Effects = new[]
                {
                    new SpellEffect(SpellEffectType.WeaponDamage, SpellEffectTarget.CasterTarget, SpellEffectTarget.None)
                    {
                        FloatValue1 = 0.3f,
                    },
                },
                Icon = texIcons + 276,
                Range = new FloatBounds(0, 2),
                Visual = DatabaseManager.SpellVisuals[svGenericMelee],
            });
            int spellRogueDodge = DatabaseManager.Spells.Add(++id, new SpellTemplate(Text.Make("Dodge"), Text.Make("Gain evasion for 3 sec, making all attacks against you miss."))
            {
                AcceptableTargets = SpellTargetMask.Self,
                AuraDuration = 3,
                AuraIcon = texIcons + 296,
                Category = SpellCategory.Ability,
                CooldownDuration = 15,
                CooldownGroup = 2,
                Effects = new[]
                {
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.Evasion,
                        FloatValue1 = 1,
                    },
                },
                Flags = SpellFlags.AutoTargetSelf,
                GlobalCooldown = 1,
                Icon = texIcons + 296,
                RequiredPowerType = PowerType.Energy,
                RequiredPowerAmount = 30,
                Visual = DatabaseManager.SpellVisuals[svGeneric],
            });
            int spellRogueCut = DatabaseManager.Spells.Add(++id, new SpellTemplate(Text.Make("Cut"), Text.Make("Reduce target's movement speed by 50% and make them bleed for 200% weapon damage over 5 secs."))
            {
                AcceptableTargets = SpellTargetMask.Attackable,
                AuraDuration = 5,
                AuraIcon = texIcons + 277,
                Category = SpellCategory.Melee,
                CooldownDuration = 20,
                CooldownGroup = 3,
                Effects = new[]
                {
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.ModWalkSpeed,
                        FloatValue1 = 0.5f,
                    },
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.ModRunSpeed,
                        FloatValue1 = 0.5f,
                    },
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.ModSwimSpeed,
                        FloatValue1 = 0.5f,
                    },
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.ModFlySpeed,
                        FloatValue1 = 0.5f,
                    },
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.PeriodicTriggerSpellFromCaster,
                        IntValue1 = id + 1,
                        TriggerSpellPeriodic = 1,
                    },
                },
                GlobalCooldown = 1,
                Icon = texIcons + 277,
                Range = new FloatBounds(0, 3),
                RequiredPowerType = PowerType.Energy,
                RequiredPowerAmount = 40,
                Visual = DatabaseManager.SpellVisuals[svGeneric],
            });
            DatabaseManager.Spells.Add(++id, new SpellTemplate(Text.Make("Cut"), Text.Make("Dealing 200% weapon damage over 5 secs."))
            {
                AcceptableTargets = SpellTargetMask.Attackable,
                Effects = new[]
                {
                    new SpellEffect(SpellEffectType.WeaponDamage, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        FloatValue1 = 0.4f,
                    },
                },
                Icon = texIcons + 277,
                Range = FloatBounds.Infinite,
            });
            int spellRogueHide = DatabaseManager.Spells.Add(++id, new SpellTemplate(Text.Make("Hide"), Text.Make("Make yourself invisible for 30 secs at a cost of reduced movement speed. Removed if entered combat."))
            {
                AcceptableTargets = SpellTargetMask.Self,
                AuraDuration = 30,
                AuraIcon = texIcons + 209,
                AuraInterrupt = InterruptMask.EnteringCombat,
                Category = SpellCategory.Ability,
                CooldownDuration = 60,
                CooldownGroup = 4,
                Effects = new[]
                {
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.Invisibility,
                        FloatValue1 = 1,
                    },
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.ModWalkSpeed,
                        FloatValue1 = 0.7f,
                    },
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.ModRunSpeed,
                        FloatValue1 = 0.7f,
                    },
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.ModSwimSpeed,
                        FloatValue1 = 0.7f,
                    },
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.ModFlySpeed,
                        FloatValue1 = 0.7f,
                    },
                },
                Flags = SpellFlags.AutoTargetSelf | SpellFlags.NotInCombat,
                Icon = texIcons + 209,
                RequiredPowerType = PowerType.Energy,
                RequiredPowerAmount = 50,
                Visual = DatabaseManager.SpellVisuals[svGeneric],
            });
            int spellMageFireball = DatabaseManager.Spells.Add(++id, new SpellTemplate(Text.Make("Fireball"), Text.Make("Deals 10-20 (+ Magic Power) fire damage to the target and also half of that damage over 3 seconds."))
            {
                AcceptableTargets = SpellTargetMask.Attackable,
                AuraDuration = 3,
                AuraIcon = texIcons + 229,
                CastingTime = 1,
                Category = SpellCategory.Magic,
                CooldownDuration = 3,
                CooldownGroup = 1,
                Effects = new[]
                {
                    new SpellEffect(SpellEffectType.SchoolDamage, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        IntValue1 = 10,
                        IntValue2 = 20,
                        IntValue3 = (int)DamageSchool.Fire,
                        FloatValue2 = 1,
                    },
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.PeriodicTriggerSpellFromCaster,
                        IntValue1 = id + 1,
                        TriggerSpellPeriodic = 1,
                    },
                },
                GlobalCooldown = 1,
                Icon = texIcons + 229,
                LightData = new LightData(Color.Orange, 2, 5),
                Range = new FloatBounds(0, 20),
                RequiredPowerType = PowerType.Mana,
                RequiredPowerAmount = 40,
                Speed = 4,
                Visual = DatabaseManager.SpellVisuals[svMageFireball],
            });
            DatabaseManager.Spells.Add(++id, new SpellTemplate(Text.Make("Fireball"), Text.Make("Deals 5-10 (+1/2 Magic Power) fire damage to the target over 3 seconds."))
            {
                AcceptableTargets = SpellTargetMask.Attackable,
                Effects = new[]
                {
                    new SpellEffect(SpellEffectType.SchoolDamage, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        IntValue1 = 5,
                        IntValue2 = 10,
                        IntValue3 = (int)DamageSchool.Fire,
                        FloatValue2 = 0.16f,
                    },
                },
                Range = FloatBounds.Infinite,
            });
            int spellMageIce = DatabaseManager.Spells.Add(++id, new SpellTemplate(Text.Make("Ice Bolt"), Text.Make("Deals 5-40 (+3/4 Magic Power) water damage to the target and slows the target by 50% for 3 seconds."))
            {
                AcceptableTargets = SpellTargetMask.Attackable,
                AuraDuration = 3,
                AuraIcon = texIcons + 244,
                CastingTime = 1.25f,
                Category = SpellCategory.Magic,
                CooldownDuration = 3,
                CooldownGroup = 2,
                Effects = new[]
                {
                    new SpellEffect(SpellEffectType.SchoolDamage, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        IntValue1 = 5,
                        IntValue2 = 40,
                        IntValue3 = (int)DamageSchool.Water,
                        FloatValue2 = 0.75f,
                    },
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.ModWalkSpeed,
                        FloatValue1 = 0.5f,
                    },
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.ModRunSpeed,
                        FloatValue1 = 0.5f,
                    },
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.ModSwimSpeed,
                        FloatValue1 = 0.5f,
                    },
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.ModFlySpeed,
                        FloatValue1 = 0.5f,
                    },
                },
                GlobalCooldown = 1,
                Icon = texIcons + 244,
                LightData = new LightData(Color.Cyan, 1, 5),
                Range = new FloatBounds(0, 20),
                RequiredPowerType = PowerType.Mana,
                RequiredPowerAmount = 60,
                Speed = 3,
                Visual = DatabaseManager.SpellVisuals[svMageIce],
            });
            int spellMageCircle = DatabaseManager.Spells.Add(++id, new SpellTemplate(Text.Make("Flame Circle"), Text.Make("Creates a circle of flames on the ground for 5 seconds, dealing 5-10 (+1/5 Magic Power) fire damage to all enemies inside it every second and setting them on fire, dealing additional 5-10 (+1/10 Magic Power) fire damage over 3 seconds."))
            {
                AcceptableTargets = SpellTargetMask.Self,
                AreaEffectDuration = 5,                
                AuraIcon = texIcons + 228,
                CastingTime = 3,
                Category = SpellCategory.Magic,
                CooldownDuration = 20,
                CooldownGroup = 3,
                Effects = new[]
                {
                    new SpellEffect(SpellEffectType.PeriodicAreaEffect, SpellEffectTarget.Caster, SpellEffectTarget.HostilesAOE)
                    {
                        IntValue1 = id + 1,
                        AreaEffectPeriodic = 1,
                    },
                },
                GlobalCooldown = 1,
                Icon = texIcons + 228,
                LightData = new LightData(Color.Orange, 6, 10),
                Radius = 3,
                RequiredPowerType = PowerType.Mana,
                RequiredPowerAmount = 120,
                Visual = DatabaseManager.SpellVisuals[svMageCircle],
            });
            DatabaseManager.Spells.Add(++id, new SpellTemplate(Text.Make("Flame Circle"), Text.Make("Dealing 5-10 (+1/10 Magic Power) fire damage over 3 seconds."))
            {
                AcceptableTargets = SpellTargetMask.Attackable,
                AuraDuration = 3,
                AuraIcon = texIcons + 228,
                Effects = new[]
                {
                    new SpellEffect(SpellEffectType.SchoolDamage, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        IntValue1 = 5,
                        IntValue2 = 10,
                        IntValue3 = (int)DamageSchool.Fire,
                        FloatValue2 = 0.2f,
                    },
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.PeriodicTriggerSpellFromCaster,
                        IntValue1 = id + 1,
                        TriggerSpellPeriodic = 1,
                    },
                },
                Range = FloatBounds.Infinite,
            });
            DatabaseManager.Spells.Add(++id, new SpellTemplate(Text.Make("Flame Circle"), Text.Make("Dealing 5-10 (+1/10 Magic Power) fire damage over 3 seconds."))
            {
                AcceptableTargets = SpellTargetMask.Attackable,
                AuraIcon = texIcons + 228,
                Effects = new[]
                {
                    new SpellEffect(SpellEffectType.SchoolDamage, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        IntValue1 = 5,
                        IntValue2 = 10,
                        IntValue3 = (int)DamageSchool.Fire,
                        FloatValue2 = 0.0333f,
                    },
                },
                Range = FloatBounds.Infinite,
            });
            int spellMageEarthquake = DatabaseManager.Spells.Add(++id, new SpellTemplate(Text.Make("Earthquake"), Text.Make("Deals 20-40 (+2x Magic Power) ground damage to everyone in radius of 4 meters and slows their movement down by 30% for 5 seconds."))
            {
                AcceptableTargets = SpellTargetMask.Attackable,
                AreaEffectDuration = 5,
                AuraDuration = 5,
                AuraIcon = texIcons + 225,
                CastingTime = 5,
                Category = SpellCategory.Magic,
                CooldownDuration = 30,
                CooldownGroup = 4,
                Effects = new[]
                {
                    new SpellEffect(SpellEffectType.SchoolDamage, SpellEffectTarget.HostilesAOE, SpellEffectTarget.GroundTarget)
                    {
                        IntValue1 = 20,
                        IntValue2 = 40,
                        IntValue3 = (int)DamageSchool.Ground,
                        FloatValue2 = 2,
                    },
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.HostilesAOE, SpellEffectTarget.GroundTarget)
                    {
                        AuraType = AuraType.ModWalkSpeed,
                        FloatValue1 = 0.7f,
                    },
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.HostilesAOE, SpellEffectTarget.GroundTarget)
                    {
                        AuraType = AuraType.ModRunSpeed,
                        FloatValue1 = 0.7f,
                    },
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.HostilesAOE, SpellEffectTarget.GroundTarget)
                    {
                        AuraType = AuraType.ModSwimSpeed,
                        FloatValue1 = 0.7f,
                    },
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.HostilesAOE, SpellEffectTarget.GroundTarget)
                    {
                        AuraType = AuraType.ModFlySpeed,
                        FloatValue1 = 0.7f,
                    },
                    new SpellEffect(SpellEffectType.NullAreaEffect, SpellEffectTarget.GroundTarget, SpellEffectTarget.None),
                },
                Flags = SpellFlags.GroundTargeted,
                GlobalCooldown = 1,
                GroundTargetTexture = texGroundTarget,
                Icon = texIcons + 225,
                Radius = 4,
                Range = new FloatBounds(0, 20),
                RequiredPowerType = PowerType.Mana,
                RequiredPowerAmount = 160,
                Speed = 5,
                Visual = DatabaseManager.SpellVisuals[svMageEarthquake],
            });
            int spellCultistSteal = DatabaseManager.Spells.Add(++id, new SpellTemplate(Text.Make("Steal Life"), Text.Make("Deals 15-30 (+ Magic Power) dark damage to the target and heal yourself for 20% of the dealt amount."))
            {
                AcceptableTargets = SpellTargetMask.Attackable,
                Category = SpellCategory.Magic,
                CooldownDuration = 5,
                CooldownGroup = 1,
                Effects = new[]
                {
                    new SpellEffect(SpellEffectType.StealHealth, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        IntValue1 = 15,
                        IntValue2 = 30,
                        IntValue3 = (int)DamageSchool.Dark,
                        FloatValue2 = 1,
                        FloatValue3 = 0.2f,
                    },
                },
                GlobalCooldown = 1,
                Icon = texIcons + 267,
                LightData = new LightData(Color.White, -5, 5),
                Range = new FloatBounds(0, 20),
                RequiredPowerType = PowerType.Mana,
                RequiredPowerAmount = 60,
                Speed = 8,
                Visual = DatabaseManager.SpellVisuals[svGeneric],
            });
            int spellCultistSacrifice = DatabaseManager.Spells.Add(++id, new SpellTemplate(Text.Make("Sacrifice"), Text.Make("Deal dark damage equal to 10% of your maximum health to yourself, deal 300% weapon damage to the target as dark damage."))
            {
                AcceptableTargets = SpellTargetMask.Attackable,
                Category = SpellCategory.Magic,
                CooldownDuration = 10,
                CooldownGroup = 2,
                Effects = new[]
                {
                    new SpellEffect(SpellEffectType.MaxHealthPercentSchoolDamage, SpellEffectTarget.Caster, SpellEffectTarget.None)
                    {
                        IntValue1 = 10,
                        IntValue3 = (int)DamageSchool.Dark,
                    },
                    new SpellEffect(SpellEffectType.WeaponDamage, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        IntValue1 = (int)DamageSchool.Dark,
                        FloatValue1 = 3,
                    },
                },
                GlobalCooldown = 1,
                Icon = texIcons + 275,
                LightData = new LightData(Color.White, -5, 5),
                Range = new FloatBounds(0, 20),
                RequiredPowerType = PowerType.Mana,
                RequiredPowerAmount = 120,
                Speed = 10,
                Visual = DatabaseManager.SpellVisuals[svGeneric],
            });
            int spellCultistShroud = DatabaseManager.Spells.Add(++id, new SpellTemplate(Text.Make("Dark Shroud"), Text.Make("Decrease target's movement speed by 25% and attack speed by 50% for 10 seconds."))
            {
                AcceptableTargets = SpellTargetMask.Attackable,
                AuraDuration = 10,
                AuraIcon = texIcons + 264,
                CastingTime = 1.25f,
                Category = SpellCategory.Magic,
                CooldownDuration = 30,
                CooldownGroup = 3,
                Effects = new[]
                {
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.ModWalkSpeed,
                        FloatValue1 = 0.75f,
                    },
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.ModRunSpeed,
                        FloatValue1 = 0.75f,
                    },
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.ModSwimSpeed,
                        FloatValue1 = 0.75f,
                    },
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.ModFlySpeed,
                        FloatValue1 = 0.75f,
                    },
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.ModWeaponSpeedPercent,
                        FloatValue1 = 0.5f,
                    },
                },
                GlobalCooldown = 1,
                Icon = texIcons + 264,
                Range = new FloatBounds(0, 10),
                RequiredPowerType = PowerType.Mana,
                RequiredPowerAmount = 160,
                Visual = DatabaseManager.SpellVisuals[svCultistShroud],
            });
            int spellCultistMeditation = DatabaseManager.Spells.Add(++id, new SpellTemplate(Text.Make("Unholy Meditation"), Text.Make("Increase mana regeneration speed by 10000% for 5 seconds, but also increase taken damage by 50%. Breaks on any movement."))
            {
                AcceptableTargets = SpellTargetMask.Self,
                AuraDuration = 5,
                AuraIcon = texIcons + 210,
                AuraInterrupt = InterruptMask.DefaultAura | InterruptMask.Moving,
                CastingTime = 1,
                CastingInterrupt = InterruptMask.DefaultCasting | InterruptMask.Moving,
                Category = SpellCategory.Magic,
                CooldownDuration = 60,
                CooldownGroup = 4,
                Effects = new[]
                {
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.ModPowerRegenPct,
                        IntValue1 = (int)PowerType.Mana,
                        FloatValue1 = 100,
                    },
                    new SpellEffect(SpellEffectType.ApplyAura, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        AuraType = AuraType.ModDamageTakenPct,
                        FloatValue1 = 0.5f,
                    },
                },
                Flags = SpellFlags.AutoTargetSelf,
                GlobalCooldown = 1,
                Icon = texIcons + 210,
                Visual = DatabaseManager.SpellVisuals[svCultistMeditation],
            });
            int spellGoblinShaman1 = DatabaseManager.Spells.Add(++id, new SpellTemplate(Text.Make("Fireball"), Text.Make("Deals 30-50 fire damage to the target."))
            {
                AcceptableTargets = SpellTargetMask.Attackable,
                CastingTime = 2,
                Category = SpellCategory.Magic,
                CooldownDuration = 5,
                CooldownGroup = 1,
                Effects = new[]
                {
                    new SpellEffect(SpellEffectType.SchoolDamage, SpellEffectTarget.SpellTarget, SpellEffectTarget.None)
                    {
                        IntValue1 = 30,
                        IntValue2 = 50,
                        IntValue3 = (int)DamageSchool.Fire,
                    },
                },
                GlobalCooldown = 1,
                Icon = texIcons + 229,
                LightData = new LightData(Color.Orange, 2, 5),
                Range = new FloatBounds(0, 20),
                RequiredPowerType = PowerType.Mana,
                RequiredPowerAmount = 80,
                Speed = 5,
                Visual = DatabaseManager.SpellVisuals[svMageFireball],
            });
            #endregion

            #region Items
            // Storing ID for future reference
            int itemPotionHealth, itemPotionEnergy, itemPotionMana;
            int itemBones1, itemBones2, itemMeat, itemFang, itemFur;
            int itemQuestReward1R1, itemQuestReward1R2;
            int itemQuestReward2R1, itemQuestReward2R2;
            int itemQuestReward3R1, itemQuestReward3R2;
            int itemClassStartWarrior1, itemClassStartWarrior2, itemClassStartWarrior3;
            int itemClassStartRogue1, itemClassStartRogue2, itemClassStartRogue3;
            int itemClassStartMage1, itemClassStartMage2, itemClassStartMage3;
            int itemClassStartCultist1, itemClassStartCultist2, itemClassStartCultist3;

            // Random items
            IntBounds itemRandomRange = new IntBounds();
            for (int i = 0; i < 20; i++)
            {
                int r = R.Int(1000);
                ItemTemplate item;
                int index = DatabaseManager.Items.Add(item = new ItemTemplate
                {
                    Name = Text.Make("Random item #" + r, "Случайный предмет №" + r),
                    Type = R.Object(ItemType.Weapon, ItemType.Equipment),
                    Quality = (ItemQuality)R.Int((int)ItemQuality.Max),
                    Description = Text.Make("Generated by the server for testing.", "Сгенерирован сервером для тестирования."),
                    Lore = Text.Make("RNG"),
                    Flags = R.Object((ItemFlags)0, (ItemFlags)0, (ItemFlags)0, (ItemFlags)0, (ItemFlags)0, ItemFlags.SoulboundOnEquip, ItemFlags.SoulboundOnPickup, ItemFlags.SoulboundOnUse, ItemFlags.Unique),
                });
                int statCount = 0;
                switch (item.Type)
                {
                    case ItemType.General:
                        break;
                    case ItemType.Equipment:
                        statCount = R.IntInclusive(0, 3);
                        item.Slot = R.Bool() ? ItemSlot.Accessory : R.Object(ItemSlot.Head, ItemSlot.Neck, ItemSlot.Back, ItemSlot.Shoulder, ItemSlot.Hands, ItemSlot.Wrists, ItemSlot.Chest, ItemSlot.Waist, ItemSlot.Legs, ItemSlot.Feet);
                        break;
                    case ItemType.Weapon:
                        statCount = R.IntInclusive(1, 5);
                        item.Slot = R.Object(ItemSlot.MainHand, ItemSlot.OffHand, ItemSlot.OneHand, ItemSlot.TwoHand);
                        item.WeaponDamage = new IntBounds(R.IntInclusive(5, 10), R.IntInclusive(10, 30));
                        item.WeaponSpeed = new FloatBounds(R.Float(0.5f, 1.0f), R.Float(1.0f, 3.0f));
                        break;
                    case ItemType.Quest:
                        break;
                    case ItemType.Book:
                        break;
                }
                for (int j = 0; j < statCount; j++)
                    item.Mods[(Stat)R.Int((byte)Stat.Max)] = R.Int(-10, 100);
                if (R.Bool())
                    item.SpellTriggers[(byte)ItemSpellUsage.OnEquip] = DatabaseManager.Spells[spellItemWeaponSpeedPct20];

                if (itemRandomRange.Min == 0)
                    itemRandomRange.Min = index;
                itemRandomRange.Max = index;
            }

            // Generic items for loot
            int[] itemRangeCommon1 =
            {
                itemClassStartWarrior1 = itemClassStartRogue1 = itemClassStartMage1 = itemClassStartCultist1 = DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Peasant's Shirt", "Рубашка селянина"),
                    Icon = texIcons + 5,
                    Type = ItemType.Equipment,
                    Quality = ItemQuality.Common,
                    Slot = ItemSlot.Chest,
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.Armor, +3 },
                        { Stat.Swiftness, +1 },
                    },
                }),
                itemClassStartWarrior2 = itemClassStartRogue2 = itemClassStartMage2 = itemClassStartCultist2 = DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Peasant's Shoes", "Ботинки селянина"),
                    Icon = texIcons + 7,
                    Type = ItemType.Equipment,
                    Quality = ItemQuality.Common,
                    Slot = ItemSlot.Feet,
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.Armor, +2 },
                        { Stat.Swiftness, +1 },
                    },
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Worker's Gloves", "Перчатки работника"),
                    Icon = texIcons + 16,
                    Type = ItemType.Equipment,
                    Quality = ItemQuality.Common,
                    Slot = ItemSlot.Hands,
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.Armor, +2 },
                        { Stat.MeleePower, +2 },
                    },
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Simple Helm", "Простой шлем"),
                    Icon = texIcons + 35,
                    Type = ItemType.Equipment,
                    Quality = ItemQuality.Common,
                    Slot = ItemSlot.Head,
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.Armor, +2 },
                        { Stat.PhysicalResistance, +1 },
                    },
                }),
                itemClassStartMage3 = DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Wizard's Hat", "Шляпа волшебника"),
                    Icon = texIcons + 39,
                    Type = ItemType.Equipment,
                    Quality = ItemQuality.Common,
                    Slot = ItemSlot.Head,
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.Armor, +1 },
                        { Stat.MagicPower, +3 },
                        { Stat.Intellect, +2 },
                        { Stat.Spirit, +1 },
                    },
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Wooden Shield", "Деревяный щит"),
                    Icon = texIcons + 52,
                    Type = ItemType.Weapon,
                    Quality = ItemQuality.Common,
                    Slot = ItemSlot.OffHand,
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.PhysicalResistance, +2 },
                        { Stat.Armor, +10 },
                    },
                }),
                itemClassStartWarrior3 = DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Axe", "Топор"),
                    Icon = texIcons + 302,
                    Type = ItemType.Weapon,
                    Quality = ItemQuality.Common,
                    Slot = ItemSlot.OneHand,
                    WeaponDamage = new IntBounds(10, 15),
                    WeaponSpeed = new FloatBounds(1.0f),
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.MeleePower, +1 },
                        { Stat.Swiftness, +1 },
                    },
                }),
                itemClassStartRogue3 = itemClassStartCultist3 = DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Knife", "Нож"),
                    Icon = texIcons + 337,
                    Type = ItemType.Weapon,
                    Quality = ItemQuality.Common,
                    Slot = ItemSlot.OneHand,
                    WeaponDamage = new IntBounds(4, 7),
                    WeaponSpeed = new FloatBounds(0.5f),
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.Swiftness, +2 },
                    },
                }),
            };
            int[] itemRangeCommon2 =
            {
                itemQuestReward1R1 = DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Ornate Shirt", "Украшенная рубашка"),
                    Icon = texIcons + 6,
                    Type = ItemType.Equipment,
                    Quality = ItemQuality.Common,
                    Slot = ItemSlot.Chest,
                    RequiredLevel = new IntBounds(2, 0),
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.Armor, +6 },
                        { Stat.Swiftness, +2 },
                    },
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Ranger's Shoes", "Ботинки охотника"),
                    Icon = texIcons + 8,
                    Type = ItemType.Equipment,
                    Quality = ItemQuality.Common,
                    Slot = ItemSlot.Feet,
                    RequiredLevel = new IntBounds(2, 0),
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.Armor, +4 },
                        { Stat.Swiftness, +2 },
                    },
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Ranger's Gloves", "Перчатки охотника"),
                    Icon = texIcons + 17,
                    Type = ItemType.Equipment,
                    Quality = ItemQuality.Common,
                    Slot = ItemSlot.Hands,
                    RequiredLevel = new IntBounds(2, 0),
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.Armor, +4 },
                        { Stat.MeleePower, +3 },
                        { Stat.Swiftness, +1 },
                    },
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Metal Helm", "Железный шлем"),
                    Icon = texIcons + 36,
                    Type = ItemType.Equipment,
                    Quality = ItemQuality.Common,
                    Slot = ItemSlot.Head,
                    RequiredLevel = new IntBounds(2, 0),
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.Armor, +4 },
                        { Stat.PhysicalResistance, +2 },
                    },
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Mage's Hat", "Шляпа мага"),
                    Icon = texIcons + 39,
                    Type = ItemType.Equipment,
                    Quality = ItemQuality.Common,
                    Slot = ItemSlot.Head,
                    RequiredLevel = new IntBounds(2, 0),
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.Armor, +2 },
                        { Stat.MagicPower, +6 },
                        { Stat.Intellect, +4 },
                        { Stat.Spirit, +3 },
                    },
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Strong Shield", "Укрепленный щит"),
                    Icon = texIcons + 53,
                    Type = ItemType.Weapon,
                    Quality = ItemQuality.Common,
                    Slot = ItemSlot.OffHand,
                    RequiredLevel = new IntBounds(2, 0),
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.PhysicalResistance, +4 },
                        { Stat.Armor, +15 },
                    },
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Strong Axe", "Укрепленный топор"),
                    Icon = texIcons + 303,
                    Type = ItemType.Weapon,
                    Quality = ItemQuality.Common,
                    Slot = ItemSlot.MainHand,
                    WeaponDamage = new IntBounds(14, 18),
                    WeaponSpeed = new FloatBounds(1.2f),
                    RequiredLevel = new IntBounds(2, 0),
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.MeleePower, +3 },
                        { Stat.Swiftness, +2 },
                    },
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Heavy Axe", "Тяжелый топор"),
                    Icon = texIcons + 304,
                    Type = ItemType.Weapon,
                    Quality = ItemQuality.Common,
                    Slot = ItemSlot.TwoHand,
                    WeaponDamage = new IntBounds(26, 44),
                    WeaponSpeed = new FloatBounds(2.3f),
                    RequiredLevel = new IntBounds(2, 0),
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.MeleePower, +5 },
                        { Stat.Swiftness, +3 },
                    },
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Dagger", "Кинжал"),
                    Icon = texIcons + 338,
                    Type = ItemType.Weapon,
                    Quality = ItemQuality.Common,
                    Slot = ItemSlot.OneHand,
                    WeaponDamage = new IntBounds(9, 13),
                    WeaponSpeed = new FloatBounds(0.4f),
                    RequiredLevel = new IntBounds(2, 0),
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.Swiftness, +5 },
                    },
                }),
            };
            int[] itemRangeCommon3 =
            {
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Leather Armor", "Кожанная броня"),
                    Icon = texIcons + 2,
                    Type = ItemType.Equipment,
                    Quality = ItemQuality.Common,
                    Slot = ItemSlot.Chest,
                    RequiredLevel = new IntBounds(3, 0),
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.Armor, +13 },
                        { Stat.PhysicalResistance, +3 },
                        { Stat.Swiftness, +1 },
                    },
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Cotton Shirt", "Мягкая рубашка"),
                    Icon = texIcons + 0,
                    Type = ItemType.Equipment,
                    Quality = ItemQuality.Common,
                    Slot = ItemSlot.Chest,
                    RequiredLevel = new IntBounds(3, 0),
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.Armor, +6 },
                        { Stat.MagicPower, +11 },
                        { Stat.Intellect, +2 },
                        { Stat.Spirit, +1 },
                    },
                }),
                itemQuestReward2R1 = DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Protected Boots", "Укрепленные ботинки"),
                    Icon = texIcons + 9,
                    Type = ItemType.Equipment,
                    Quality = ItemQuality.Common,
                    Slot = ItemSlot.Feet,
                    RequiredLevel = new IntBounds(3, 0),
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.Armor, +9 },
                        { Stat.PhysicalResistance, +2 },
                        { Stat.Swiftness, +1 },
                    },
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Gauntlets", "Тяжелые перчатки"),
                    Icon = texIcons + 21,
                    Type = ItemType.Equipment,
                    Quality = ItemQuality.Common,
                    Slot = ItemSlot.Hands,
                    RequiredLevel = new IntBounds(3, 0),
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.Armor, +8 },
                        { Stat.MeleePower, +5 },
                        { Stat.Swiftness, +1 },
                    },
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Steel Helm", "Стальной шлем"),
                    Icon = texIcons + 37,
                    Type = ItemType.Equipment,
                    Quality = ItemQuality.Common,
                    Slot = ItemSlot.Head,
                    RequiredLevel = new IntBounds(3, 0),
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.Armor, +9 },
                        { Stat.PhysicalResistance, +6 },
                    },
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Tower Shield", "Башенный щит"),
                    Icon = texIcons + 54,
                    Type = ItemType.Weapon,
                    Quality = ItemQuality.Common,
                    Slot = ItemSlot.OffHand,
                    RequiredLevel = new IntBounds(3, 0),
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.PhysicalResistance, +7 },
                        { Stat.Armor, +32 },
                    },
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Fine Axe", "Хороший топор"),
                    Icon = texIcons + 307,
                    Type = ItemType.Weapon,
                    Quality = ItemQuality.Common,
                    Slot = ItemSlot.OneHand,
                    WeaponDamage = new IntBounds(17, 22),
                    WeaponSpeed = new FloatBounds(1.1f),
                    RequiredLevel = new IntBounds(3, 0),
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.MeleePower, +5 },
                        { Stat.Swiftness, +2 },
                        { Stat.Stamina, +1 },
                    },
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Short Katana", "Короткая катана"),
                    Icon = texIcons + 341,
                    Type = ItemType.Weapon,
                    Quality = ItemQuality.Common,
                    Slot = ItemSlot.MainHand,
                    WeaponDamage = new IntBounds(11, 14),
                    WeaponSpeed = new FloatBounds(0.7f),
                    RequiredLevel = new IntBounds(3, 0),
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.MeleePower, +3 },
                        { Stat.Swiftness, +6 },
                    },
                }),
            };
            int[] itemRangeCommon4 =
            {
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Metal Armor", "Кожанная броня"),
                    Icon = texIcons + 3,
                    Type = ItemType.Equipment,
                    Quality = ItemQuality.Common,
                    Slot = ItemSlot.Chest,
                    RequiredLevel = new IntBounds(4, 0),
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.Armor, +18 },
                        { Stat.PhysicalResistance, +7 },
                    },
                }),
                itemQuestReward3R1 = DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Steel Shield", "Стальной щит"),
                    Icon = texIcons + 49,
                    Type = ItemType.Weapon,
                    Quality = ItemQuality.Common,
                    Slot = ItemSlot.OffHand,
                    RequiredLevel = new IntBounds(4, 0),
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.PhysicalResistance, +11 },
                        { Stat.Armor, +43 },
                    },
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Demonic Axe", "Демонический топор"),
                    Icon = texIcons + 312,
                    Type = ItemType.Weapon,
                    Quality = ItemQuality.Common,
                    Slot = ItemSlot.TwoHand,
                    WeaponDamage = new IntBounds(53, 71),
                    WeaponSpeed = new FloatBounds(2.6f),
                    RequiredLevel = new IntBounds(4, 0),
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.MeleePower, +7 },
                        { Stat.Swiftness, +4 },
                        { Stat.DarkResistance, +5 },
                    },
                }),
            };
            int[] itemRangeUncommon1 =
            {
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Strange Hat", "Странная шапка"),
                    Icon = texIcons + 41,
                    Type = ItemType.Equipment,
                    Quality = ItemQuality.Uncommon,
                    Slot = ItemSlot.Head,
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.Armor, +1 },
                        { Stat.MeleePower, +2 },
                        { Stat.MagicPower, +2 },
                        { Stat.MagicResistance, +2 },
                        { Stat.Swiftness, +3 },
                    },
                }),
            };
            int[] itemRangeUncommon2 =
            {
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Metal Shield", "Железный щит"),
                    Icon = texIcons + 47,
                    Type = ItemType.Weapon,
                    Quality = ItemQuality.Uncommon,
                    Slot = ItemSlot.OffHand,
                    RequiredLevel = new IntBounds(2, 0),
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.PhysicalResistance, +6 },
                        { Stat.Armor, +21 },
                    },
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Shadowdagger", "Кинжал теней"),
                    Icon = texIcons + 343,
                    Type = ItemType.Weapon,
                    Quality = ItemQuality.Uncommon,
                    Slot = ItemSlot.OneHand,
                    WeaponDamage = new IntBounds(54, 73),
                    WeaponSpeed = new FloatBounds(0.7f),
                    RequiredLevel = new IntBounds(2, 0),
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.Stamina, -2 },
                        { Stat.Swiftness, +2 },
                        { Stat.MagicResistance, +1 },
                        { Stat.DarkResistance, +3 },
                    },
                }),
            };
            int[] itemRangeUncommon3 =
            {
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Tribal Axe", "Племенной топор"),
                    Icon = texIcons + 309,
                    Type = ItemType.Weapon,
                    Quality = ItemQuality.Uncommon,
                    Slot = ItemSlot.MainHand,
                    WeaponDamage = new IntBounds(21, 28),
                    WeaponSpeed = new FloatBounds(1.3f),
                    RequiredLevel = new IntBounds(3, 0),
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.MeleePower, +8 },
                        { Stat.Swiftness, +4 },
                        { Stat.Stamina, +3 },
                    },
                }),
            };
            int[] itemRangeUncommon4 =
            {
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Knight's Helm", "Шлем рыцаря"),
                    Icon = texIcons + 38,
                    Type = ItemType.Equipment,
                    Quality = ItemQuality.Uncommon,
                    Slot = ItemSlot.Head,
                    RequiredLevel = new IntBounds(4, 0),
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.Armor, +16 },
                        { Stat.PhysicalResistance, +12 },
                    },
                }),
                itemQuestReward3R2 = DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Poison Axe", "Покрытый ядом топор"),
                    Icon = texIcons + 313,
                    Type = ItemType.Weapon,
                    Quality = ItemQuality.Uncommon,
                    Slot = ItemSlot.TwoHand,
                    WeaponDamage = new IntBounds(62, 84),
                    WeaponSpeed = new FloatBounds(2.5f),
                    RequiredLevel = new IntBounds(4, 0),
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.MeleePower, +20 },
                        { Stat.Swiftness, +11 },
                        { Stat.Stamina, -4 },
                        { Stat.GroundResistance, +8 },
                    },
                    SpellTriggers = new[] { null, DatabaseManager.Spells[spellItemPoisonAxe] as SpellTemplateBase, null, null, null },
                }),
            };
            int[] itemRangeRare1 =
            {
            };
            int[] itemRangeRare2 =
            {
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Spark", "Вспышка"),
                    Icon = texIcons + 343,
                    Type = ItemType.Weapon,
                    Quality = ItemQuality.Unique,
                    Slot = ItemSlot.OneHand,
                    Flags = ItemFlags.SoulboundOnEquip | ItemFlags.Unique,
                    WeaponDamage = new IntBounds(17, 35),
                    WeaponSpeed = new FloatBounds(0.3f),
                    RequiredLevel = new IntBounds(2, 0),
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.Swiftness, +7 },
                        { Stat.AirResistance, +9 },
                    },
                    SpellTriggers = new[] { null, DatabaseManager.Spells[spellItemSpark] as SpellTemplateBase, null, null, null },
                }),
            };
            int[] itemRangeRare3 =
            {
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("The Protector", "\"Защитник\""),
                    Icon = texIcons + 50,
                    Type = ItemType.Weapon,
                    Quality = ItemQuality.Rare,
                    Slot = ItemSlot.OffHand,
                    Flags = ItemFlags.SoulboundOnEquip | ItemFlags.Unique,
                    RequiredLevel = new IntBounds(3, 0),
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.PhysicalResistance, +16 },
                        { Stat.Armor, +53 },
                    },
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Golden Axe", "Золотой топор"),
                    Icon = texIcons + 311,
                    Type = ItemType.Weapon,
                    Quality = ItemQuality.Rare,
                    Slot = ItemSlot.TwoHand,
                    Flags = ItemFlags.SoulboundOnEquip,
                    WeaponDamage = new IntBounds(54, 73),
                    WeaponSpeed = new FloatBounds(2.1f),
                    RequiredLevel = new IntBounds(3, 0),
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.MeleePower, +19 },
                        { Stat.Swiftness, +9 },
                        { Stat.Stamina, +8 },
                    },
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Molten Dagger", "Огненный кинжал"),
                    Icon = texIcons + 352,
                    Type = ItemType.Weapon,
                    Quality = ItemQuality.Rare,
                    Slot = ItemSlot.OneHand,
                    Flags = ItemFlags.SoulboundOnEquip,
                    WeaponDamage = new IntBounds(12, 15),
                    WeaponSpeed = new FloatBounds(0.4f),
                    RequiredLevel = new IntBounds(3, 0),
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.MeleePower, +3 },
                        { Stat.Swiftness, -2 },
                        { Stat.FireResistance, +4 },
                    },
                    SpellTriggers = new[] { null, DatabaseManager.Spells[spellItemMoltenDagger] as SpellTemplateBase, null, null, null },
                }),
            };
            int[] itemRangeRare4 =
            {
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Dark Axe", "Тёмный топор"),
                    Icon = texIcons + 315,
                    Type = ItemType.Weapon,
                    Quality = ItemQuality.Rare,
                    Slot = ItemSlot.TwoHand,
                    Flags = ItemFlags.SoulboundOnEquip,
                    WeaponDamage = new IntBounds(71, 93),
                    WeaponSpeed = new FloatBounds(2.8f),
                    RequiredLevel = new IntBounds(4, 0),
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.MeleePower, +25 },
                        { Stat.Swiftness, -3 },
                        { Stat.Stamina, +15 },
                        { Stat.DarkResistance, +9 },
                    },
                }),
            };
            int[] itemRangeAccessories =
            {
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Golden Medal", "Золотая медаль"),
                    Icon = texIcons + 23,
                    Type = ItemType.Equipment,
                    Quality = ItemQuality.Uncommon,
                    Slot = ItemSlot.Accessory,
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.MeleePower, +3 },
                        { Stat.Swiftness, +1 },
                        { Stat.Stamina, +2 },
                        { Stat.MagicResistance, +1 },
                    },
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Silver Medal", "Серебряная медаль"),
                    Icon = texIcons + 24,
                    Type = ItemType.Equipment,
                    Quality = ItemQuality.Uncommon,
                    Slot = ItemSlot.Accessory,
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.MagicPower, +3 },
                        { Stat.Intellect, +2 },
                        { Stat.Spirit, +2 },
                        { Stat.MagicResistance, +1 },
                    },
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Wooden Medal", "Деревяная медаль"),
                    Icon = texIcons + 25,
                    Type = ItemType.Equipment,
                    Quality = ItemQuality.Common,
                    Slot = ItemSlot.Accessory,
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.MeleePower, +1 },
                        { Stat.Swiftness, +1 },
                        { Stat.Stamina, +1 },
                        { Stat.Intellect, +1 },
                        { Stat.MagicResistance, +1 },
                    },
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Golden Necklace", "Золотая цепочка"),
                    Icon = texIcons + 27,
                    Type = ItemType.Equipment,
                    Quality = ItemQuality.Uncommon,
                    Slot = ItemSlot.Neck,
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.MeleePower, +8 },
                        { Stat.Swiftness, +2 },
                        { Stat.Stamina, +3 },
                        { Stat.MagicResistance, +1 },
                    },
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Silver Necklace", "Серебряная цепочка"),
                    Icon = texIcons + 29,
                    Type = ItemType.Equipment,
                    Quality = ItemQuality.Uncommon,
                    Slot = ItemSlot.Neck,
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.MagicPower, +8 },
                        { Stat.Intellect, +3 },
                        { Stat.Spirit, +3 },
                        { Stat.MagicResistance, +2 },
                    },
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Sapphire Necklace", "Цепочка с сапфиром"),
                    Icon = texIcons + 28,
                    Type = ItemType.Equipment,
                    Quality = ItemQuality.Rare,
                    Slot = ItemSlot.Neck,
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.MagicPower, +12 },
                        { Stat.Intellect, +8 },
                        { Stat.Spirit, +4 },
                    },
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Winged Necklace", "Цепочка с крыльями"),
                    Icon = texIcons + 30,
                    Type = ItemType.Equipment,
                    Quality = ItemQuality.Rare,
                    Slot = ItemSlot.Neck,
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.MeleePower, +4 },
                        { Stat.Swiftness, +12 },
                        { Stat.Stamina, +8 },
                    },
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Metal Ring", "Железное кольцо"),
                    Icon = texIcons + 31,
                    Type = ItemType.Equipment,
                    Quality = ItemQuality.Common,
                    Slot = ItemSlot.Accessory,
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.MeleePower, +1 },
                        { Stat.Swiftness, +1 },
                        { Stat.Stamina, +1 },
                    },
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Golden Ring", "Золотое кольцо"),
                    Icon = texIcons + 32,
                    Type = ItemType.Equipment,
                    Quality = ItemQuality.Uncommon,
                    Slot = ItemSlot.Accessory,
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.MeleePower, +3 },
                        { Stat.Swiftness, +2 },
                        { Stat.Stamina, +2 },
                    },
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Silver Ring", "Серебряное кольцо"),
                    Icon = texIcons + 34,
                    Type = ItemType.Equipment,
                    Quality = ItemQuality.Uncommon,
                    Slot = ItemSlot.Accessory,
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.MagicPower, +1 },
                        { Stat.Intellect, +1 },
                        { Stat.Spirit, +1 },
                    },
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Diamond Ring", "Кольцо с брильянтом"),
                    Icon = texIcons + 33,
                    Type = ItemType.Equipment,
                    Quality = ItemQuality.Uncommon,
                    Slot = ItemSlot.Accessory,
                    Mods = new Dictionary<Stat, int>
                    {
                        { Stat.MagicPower, +3 },
                        { Stat.Intellect, +2 },
                        { Stat.Spirit, +2 },
                    },
                }),
            };
            int[] itemRangeGems =
            {
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Agate", "Агат"),
                    Icon = texIcons + 56,
                    Type = ItemType.General,
                    Quality = ItemQuality.Uncommon,
                    Stack = 20,
                    SellPrice = 100,
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Amethyst", "Аметист"),
                    Icon = texIcons + 57,
                    Type = ItemType.General,
                    Quality = ItemQuality.Uncommon,
                    Stack = 20,
                    SellPrice = 120,
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Coal", "Уголь"),
                    Icon = texIcons + 104,
                    Type = ItemType.General,
                    Quality = ItemQuality.Junk,
                    Stack = 50,
                    SellPrice = 12,
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Crystal", "Кристал"),
                    Icon = texIcons + 105,
                    Type = ItemType.General,
                    Quality = ItemQuality.Common,
                    Stack = 10,
                    SellPrice = 40,
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Crystal", "Кристал"),
                    Icon = texIcons + 106,
                    Type = ItemType.General,
                    Quality = ItemQuality.Common,
                    Stack = 10,
                    SellPrice = 40,
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Crystal", "Кристал"),
                    Icon = texIcons + 107,
                    Type = ItemType.General,
                    Quality = ItemQuality.Common,
                    Stack = 10,
                    SellPrice = 40,
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Diamond", "Брильянт"),
                    Icon = texIcons + 108,
                    Type = ItemType.General,
                    Quality = ItemQuality.Rare,
                    Stack = 20,
                    SellPrice = 170,
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Gold Bar", "Слиток золота"),
                    Icon = texIcons + 117,
                    Type = ItemType.General,
                    Quality = ItemQuality.Uncommon,
                    Stack = 5,
                    SellPrice = 70,
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Jade", "Нефрит"),
                    Icon = texIcons + 118,
                    Type = ItemType.General,
                    Quality = ItemQuality.Uncommon,
                    Stack = 20,
                    SellPrice = 110,
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Opal", "Опал"),
                    Icon = texIcons + 132,
                    Type = ItemType.General,
                    Quality = ItemQuality.Uncommon,
                    Stack = 20,
                    SellPrice = 90,
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Ruby", "Рубин"),
                    Icon = texIcons + 138,
                    Type = ItemType.General,
                    Quality = ItemQuality.Uncommon,
                    Stack = 20,
                    SellPrice = 130,
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Sapphire", "Сапфир"),
                    Icon = texIcons + 139,
                    Type = ItemType.General,
                    Quality = ItemQuality.Uncommon,
                    Stack = 20,
                    SellPrice = 125,
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Silver Bar", "Слиток серебра"),
                    Icon = texIcons + 143,
                    Type = ItemType.General,
                    Quality = ItemQuality.Uncommon,
                    Stack = 5,
                    SellPrice = 45,
                }),
            };
            int[] itemRangeConsumables =
            {
                itemPotionHealth = DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Health Potion", "Зелье здоровья"),
                    Icon = texIcons + 180,
                    Type = ItemType.General,
                    Quality = ItemQuality.Uncommon,
                    Flags = ItemFlags.Consumable,
                    Stack = 10,
                    SpellTriggers = new[] { DatabaseManager.Spells[spellItemPotionHealth], null, null, null, null },
                    SellPrice = 90,
                }),
                itemPotionMana = DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Mana Potion", "Зелье маны"),
                    Icon = texIcons + 155,
                    Type = ItemType.General,
                    Quality = ItemQuality.Uncommon,
                    Flags = ItemFlags.Consumable,
                    Stack = 10,
                    SpellTriggers = new[] { DatabaseManager.Spells[spellItemPotionMana], null, null, null, null },
                    SellPrice = 90,
                }),
                itemPotionEnergy = DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Energy Potion", "Зелье энергии"),
                    Icon = texIcons + 188,
                    Type = ItemType.General,
                    Quality = ItemQuality.Uncommon,
                    Flags = ItemFlags.Consumable,
                    Stack = 10,
                    SpellTriggers = new[] { DatabaseManager.Spells[spellItemPotionEnergy], null, null, null, null },
                    SellPrice = 90,
                }),
                itemQuestReward2R2 = DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Speed Potion", "Зелье скорости"),
                    Icon = texIcons + 168,
                    Type = ItemType.General,
                    Quality = ItemQuality.Rare,
                    Flags = ItemFlags.Consumable,
                    Stack = 10,
                    SpellTriggers = new[] { DatabaseManager.Spells[spellItemPotionSpeed], null, null, null, null },
                    SellPrice = 120,
                }),
            };
            int[] itemRangeJunk =
            {
                itemBones1 = DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Bones", "Кости"),
                    Icon = texIcons + 43,
                    Type = ItemType.General,
                    Quality = ItemQuality.Junk,
                    Stack = 5,
                    SellPrice = 5,
                }),
                itemBones2 = DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Bones", "Кости"),
                    Icon = texIcons + 61,
                    Type = ItemType.General,
                    Quality = ItemQuality.Junk,
                    Stack = 10,
                    SellPrice = 2,
                }),
                itemQuestReward1R2 = DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Apple", "Яблоко"),
                    Icon = texIcons + 69,
                    Type = ItemType.General,
                    Quality = ItemQuality.Junk,
                    Stack = 5,
                    SellPrice = 10,
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Bread", "Хлеб"),
                    Icon = texIcons + 71,
                    Type = ItemType.General,
                    Quality = ItemQuality.Junk,
                    Stack = 5,
                    SellPrice = 15,
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Cherry", "Вишня"),
                    Icon = texIcons + 74,
                    Type = ItemType.General,
                    Quality = ItemQuality.Junk,
                    Stack = 10,
                    SellPrice = 7,
                }),
                itemMeat = DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Meat", "Мясо"),
                    Icon = texIcons + 91,
                    Type = ItemType.General,
                    Quality = ItemQuality.Junk,
                    Stack = 5,
                    SellPrice = 13,
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Fabric Scrap", "Кусок ткани"),
                    Icon = texIcons + 110,
                    Type = ItemType.General,
                    Quality = ItemQuality.Junk,
                    Stack = 20,
                    SellPrice = 18,
                }),
                itemFang = DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Fang", "Клык"),
                    Icon = texIcons + 111,
                    Type = ItemType.General,
                    Quality = ItemQuality.Junk,
                    Stack = 20,
                    SellPrice = 8,
                }),
                DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Feather", "Перо"),
                    Icon = texIcons + 112,
                    Type = ItemType.General,
                    Quality = ItemQuality.Junk,
                    Stack = 20,
                    SellPrice = 3,
                }),
                itemFur = DatabaseManager.Items.Add(new ItemTemplate
                {
                    Name = Text.Make("Fur", "Шкура"),
                    Icon = texIcons + 152,
                    Type = ItemType.General,
                    Quality = ItemQuality.Junk,
                    Stack = 5,
                    SellPrice = 16,
                }),
            };

            int itemQuest1 = DatabaseManager.Items.Add(new ItemTemplate
            {
                Name = Text.Make("Goblin Fang", "Клык гоблина"),
                Icon = texIcons + 111,
                Type = ItemType.Quest,
                Quality = ItemQuality.Uncommon,
                Stack = 10,
            });
            #endregion

            #region Loot
            int lootRandomItems = DatabaseManager.Loot.Add(new LootTemplate("Randomly generated items", new LootElement(new LootGroup(new LootElement[itemRandomRange.Max - itemRandomRange.Min + 1]))));
            for (int i = itemRandomRange.Min; i <= itemRandomRange.Max; i++)
                ((LootGroup)DatabaseManager.Loot[lootRandomItems].Elements[0].Item).Elements[i - itemRandomRange.Min] = new LootElement(new LootItem(DatabaseManager.Items[i]));
            ((LootGroup)DatabaseManager.Loot[lootRandomItems].Elements[0].Item).Renormalize();
            int lootTestLoot = DatabaseManager.Loot.Add(new LootTemplate("Test loot",
                new LootElement(1.00f, new IntBounds(3, 5), new LootTemplateReference(DatabaseManager.Loot[lootRandomItems]))
                ));

            Func<int[], LootGroup> lootGroupFromItemIDs = ids =>
            {
                LootGroup group = new LootGroup(new LootElement[ids.Length]);
                for (int i = 0; i < ids.Length; i++)
                {
                    ItemTemplate item = DatabaseManager.Items[ids[i]];
                    group.Elements[i] = new LootElement(new LootItem(item, item.Stacks ? new IntBounds(1, 2) : new IntBounds(1)));
                }
                group.Renormalize();
                return group;
            };

            int lootCommon1 = DatabaseManager.Loot.Add(new LootTemplate("L1.Common", new LootElement(lootGroupFromItemIDs(itemRangeCommon1))));
            int lootCommon2 = DatabaseManager.Loot.Add(new LootTemplate("L2.Common", new LootElement(lootGroupFromItemIDs(itemRangeCommon2))));
            int lootCommon3 = DatabaseManager.Loot.Add(new LootTemplate("L3.Common", new LootElement(lootGroupFromItemIDs(itemRangeCommon3))));
            int lootCommon4 = DatabaseManager.Loot.Add(new LootTemplate("L4.Common", new LootElement(lootGroupFromItemIDs(itemRangeCommon4))));
            int lootUncommon1 = DatabaseManager.Loot.Add(new LootTemplate("L1.Uncommon", new LootElement(lootGroupFromItemIDs(itemRangeUncommon1))));
            int lootUncommon2 = DatabaseManager.Loot.Add(new LootTemplate("L2.Uncommon", new LootElement(lootGroupFromItemIDs(itemRangeUncommon2))));
            int lootUncommon3 = DatabaseManager.Loot.Add(new LootTemplate("L3.Uncommon", new LootElement(lootGroupFromItemIDs(itemRangeUncommon3))));
            int lootUncommon4 = DatabaseManager.Loot.Add(new LootTemplate("L4.Uncommon", new LootElement(lootGroupFromItemIDs(itemRangeUncommon4))));
            int lootRare1 = DatabaseManager.Loot.Add(new LootTemplate("L1.Rare", new LootElement(lootGroupFromItemIDs(itemRangeRare1))));
            int lootRare2 = DatabaseManager.Loot.Add(new LootTemplate("L1.Rare", new LootElement(lootGroupFromItemIDs(itemRangeRare2))));
            int lootRare3 = DatabaseManager.Loot.Add(new LootTemplate("L1.Rare", new LootElement(lootGroupFromItemIDs(itemRangeRare3))));
            int lootRare4 = DatabaseManager.Loot.Add(new LootTemplate("L1.Rare", new LootElement(lootGroupFromItemIDs(itemRangeRare4))));
            int lootAccessories = DatabaseManager.Loot.Add(new LootTemplate(".Accessories", new LootElement(lootGroupFromItemIDs(itemRangeAccessories))));
            int lootConsumables = DatabaseManager.Loot.Add(new LootTemplate(".Consumables", new LootElement(lootGroupFromItemIDs(itemRangeConsumables))));
            int lootGems = DatabaseManager.Loot.Add(new LootTemplate(".Gems", new LootElement(lootGroupFromItemIDs(itemRangeGems))));
            int lootJunk = DatabaseManager.Loot.Add(new LootTemplate(".Junk", new LootElement(lootGroupFromItemIDs(itemRangeJunk))));

            int lootL1 = DatabaseManager.Loot.Add(new LootTemplate("L1",
                new LootElement(0.20f, new LootTemplateReference(lootCommon1)),
                new LootElement(0.10f, new LootTemplateReference(lootUncommon1)),
                new LootElement(0.02f, new LootTemplateReference(lootRare1)),
                new LootElement(0.07f, new LootTemplateReference(lootAccessories)),
                new LootElement(0.10f, new LootTemplateReference(lootConsumables)),
                new LootElement(0.05f, new IntBounds(1, 2), new LootTemplateReference(lootGems)),
                new LootElement(0.30f, new IntBounds(1, 4), new LootTemplateReference(lootJunk))
            ));
            int lootL2 = DatabaseManager.Loot.Add(new LootTemplate("L2",
                new LootElement(0.10f, new LootTemplateReference(lootCommon1)),
                new LootElement(0.20f, new LootTemplateReference(lootCommon2)),
                new LootElement(0.10f, new LootTemplateReference(lootUncommon1)),
                new LootElement(0.10f, new LootTemplateReference(lootUncommon2)),
                new LootElement(0.01f, new LootTemplateReference(lootRare1)),
                new LootElement(0.02f, new LootTemplateReference(lootRare2)),
                new LootElement(0.07f, new LootTemplateReference(lootAccessories)),
                new LootElement(0.10f, new LootTemplateReference(lootConsumables)),
                new LootElement(0.05f, new IntBounds(1, 2), new LootTemplateReference(lootGems)),
                new LootElement(0.30f, new IntBounds(1, 4), new LootTemplateReference(lootJunk))
            ));
            int lootL3 = DatabaseManager.Loot.Add(new LootTemplate("L3",
                new LootElement(0.10f, new LootTemplateReference(lootCommon2)),
                new LootElement(0.20f, new LootTemplateReference(lootCommon3)),
                new LootElement(0.10f, new LootTemplateReference(lootUncommon2)),
                new LootElement(0.10f, new LootTemplateReference(lootUncommon3)),
                new LootElement(0.01f, new LootTemplateReference(lootRare2)),
                new LootElement(0.02f, new LootTemplateReference(lootRare3)),
                new LootElement(0.07f, new LootTemplateReference(lootAccessories)),
                new LootElement(0.10f, new LootTemplateReference(lootConsumables)),
                new LootElement(0.05f, new IntBounds(1, 2), new LootTemplateReference(lootGems)),
                new LootElement(0.60f, new IntBounds(1, 4), new LootTemplateReference(lootJunk))
            ));
            int lootL4 = DatabaseManager.Loot.Add(new LootTemplate("L4",
                new LootElement(0.10f, new LootTemplateReference(lootCommon3)),
                new LootElement(0.20f, new LootTemplateReference(lootCommon4)),
                new LootElement(0.10f, new LootTemplateReference(lootUncommon3)),
                new LootElement(0.10f, new LootTemplateReference(lootUncommon4)),
                new LootElement(0.01f, new LootTemplateReference(lootRare3)),
                new LootElement(0.02f, new LootTemplateReference(lootRare4)),
                new LootElement(0.07f, new LootTemplateReference(lootAccessories)),
                new LootElement(0.10f, new LootTemplateReference(lootConsumables)),
                new LootElement(0.05f, new IntBounds(1, 2), new LootTemplateReference(lootGems)),
                new LootElement(0.30f, new IntBounds(1, 4), new LootTemplateReference(lootJunk))
            ));
            int lootGoblin = DatabaseManager.Loot.Add(new LootTemplate("Goblin",
                new LootElement(0.75f, new LootItem(DatabaseManager.Items[itemQuest1], new IntBounds(1, 2)))
            ));
            int lootWolf = DatabaseManager.Loot.Add(new LootTemplate("Wolf",
                new LootElement(0.30f, new LootItem(DatabaseManager.Items[itemMeat], new IntBounds(1))),
                new LootElement(0.40f, new LootItem(DatabaseManager.Items[itemFang], new IntBounds(1, 2))),
                new LootElement(0.60f, new LootItem(DatabaseManager.Items[itemFur], new IntBounds(1)))
            ));
            int lootRat = DatabaseManager.Loot.Add(new LootTemplate("Rat",
                new LootElement(0.10f, new LootItem(DatabaseManager.Items[itemMeat], new IntBounds(1))),
                new LootElement(0.30f, new LootItem(DatabaseManager.Items[itemFang], new IntBounds(1, 2))),
                new LootElement(0.20f, new LootItem(DatabaseManager.Items[itemFur], new IntBounds(1)))
            ));
            #endregion

            #region Dialog
            int dlgGenericOptionEndDialog = DatabaseManager.DialogOptions.Add(new DialogOption(Text.Make("<end dialog>", "<закончить диалог>"), 0));

            int dlgTestPage1 = DatabaseManager.DialogPages.Add(new DialogPage(Text.Make("Test page text.", "{ruRU Text}")));
            int dlgTestPage2 = DatabaseManager.DialogPages.Add(new DialogPage(Text.Make("You are now being teleported.", "{ruRU Text 2}")));
            int dlgTestOption1 = DatabaseManager.DialogOptions.Add(new DialogOption(Text.Make("Teleport me.", "{ruRU Option}"), 0, dlgTestPage2));

            DatabaseManager.DialogPages[dlgTestPage1].Options.Add(DatabaseManager.DialogOptions[dlgTestOption1]);
            DatabaseManager.DialogPages[dlgTestPage1].Options.Add(DatabaseManager.DialogOptions[dlgGenericOptionEndDialog]);
            DatabaseManager.DialogPages[dlgTestPage2].Options.Add(DatabaseManager.DialogOptions[dlgGenericOptionEndDialog]);
            #endregion

            #region World Objects
            int woTree = DatabaseManager.WorldObjects.Add(new WorldObjectTemplate()
            {
                Name = Text.Make("Tree", "Дерево"),
                Size = SizeFactory.Cylinder(1.5f, 5.0f),
                Model = DatabaseManager.Models[modelTree],
            });
            #endregion

            #region Creatures
            int crTestEnemy = DatabaseManager.Creatures.Add(new CreatureTemplate()
            {
                Name = Text.Make("[TEST] Goblin", "[TEST] Гоблин"),
                Size = SizeFactory.Cylinder(0.5f, 2.0f),
                Model = raceModels[(byte)CreatureRace.Goblin][0],
                LightData = new LightData(new Vector3(R.Float(0.9f, 1.0f), R.Float(0.9f, 1.0f), R.Float(0.9f, 1.0f)), R.Float(0.5f, 1.0f), R.Float(10, 50)),
                Faction = DatabaseManager.Factions[2],
                Family = CreatureFamily.Humanoid,
                Script = DatabaseManager.GetCreatureScript("generic_creature"),
                Loot = DatabaseManager.Loot[lootTestLoot],
                Money = new IntBounds(50, 150),
            });
            int crNPCTest = DatabaseManager.Creatures.Add(new CreatureTemplate()
            {
                Name = Text.Make("NPC Test", "{ruRU NPC Test}"),
                Size = SizeFactory.Cylinder(0.5f, 2.0f),
                Model = raceModels[(byte)PlayerRace.Human][17],
                LightData = new LightData(new Vector3(R.Float(0.9f, 1.0f), R.Float(0.9f, 1.0f), R.Float(0.9f, 1.0f)), R.Float(0.5f, 1.0f), R.Float(10, 50)),
                Faction = DatabaseManager.Factions[4],
                Family = CreatureFamily.Humanoid,
                Dialog = new DialogStart(dlgTestPage1),
                Script = DatabaseManager.GetCreatureScript("npc_test_dialog_teleport"),
            });
            int crGoblinL1 = DatabaseManager.Creatures.Add(new CreatureTemplate()
            {
                Name = Text.Make("Goblin", "Гоблин"),
                Size = SizeFactory.Cylinder(0.5f, 2.0f),
                Model = raceModels[(byte)CreatureRace.Goblin][0],
                LightData = new LightData(new Vector3(R.Float(0.9f, 1.0f), R.Float(0.9f, 1.0f), R.Float(0.9f, 1.0f)), R.Float(0.5f, 1.0f), R.Float(10, 50)),
                Level = new IntBounds(1),
                Faction = DatabaseManager.Factions[factionGoblin],
                Family = CreatureFamily.Humanoid,
                Script = DatabaseManager.GetCreatureScript("generic_creature"),
                Loot = DatabaseManager.Loot.AddAndReturn(new LootTemplate("Goblin.L1",
                    new LootElement(new LootTemplateReference(lootL1)),
                    new LootElement(new LootTemplateReference(lootGoblin))
                )),
                Money = new IntBounds(10, 30),
            });
            int crGoblinL2 = DatabaseManager.Creatures.Add(new CreatureTemplate()
            {
                Name = Text.Make("Goblin Hunter", "Охотник гоблинов"),
                Size = SizeFactory.Cylinder(0.5f, 2.0f),
                Model = raceModels[(byte)CreatureRace.Goblin][2],
                LightData = new LightData(new Vector3(R.Float(0.6f, 0.8f), R.Float(0.9f, 1.0f), R.Float(0.3f, 0.5f)), R.Float(0.5f, 1.0f), R.Float(10, 50)),
                Level = new IntBounds(2),
                Faction = DatabaseManager.Factions[factionGoblin],
                Family = CreatureFamily.Humanoid,
                Script = DatabaseManager.GetCreatureScript("generic_creature"),
                Loot = DatabaseManager.Loot.AddAndReturn(new LootTemplate("Goblin.L2",
                    new LootElement(new LootTemplateReference(lootL2)),
                    new LootElement(new LootTemplateReference(lootGoblin))
                )),
                Money = new IntBounds(20, 50),
            });
            int crGoblinL3 = DatabaseManager.Creatures.Add(new CreatureTemplate()
            {
                Name = Text.Make("Goblin Shaman", "Шаман гоблинов"),
                Size = SizeFactory.Cylinder(0.5f, 2.0f),
                Model = raceModels[(byte)CreatureRace.Goblin][7],
                LightData = new LightData(new Vector3(R.Float(0.3f, 0.5f), R.Float(0.6f, 0.8f), R.Float(0.9f, 1.0f)), R.Float(0.5f, 1.0f), R.Float(10, 50)),
                Level = new IntBounds(3),
                Faction = DatabaseManager.Factions[factionGoblin],
                Family = CreatureFamily.Humanoid,
                CombatType = CreatureCombatType.CasterMana,
                Spells = new[]
                {
                    DatabaseManager.Spells[spellGoblinShaman1],
                },
                Script = DatabaseManager.GetCreatureScript("generic_creature"),
                Loot = DatabaseManager.Loot.AddAndReturn(new LootTemplate("Goblin.L3",
                    new LootElement(new LootTemplateReference(lootL3)),
                    new LootElement(0.5f, new LootItem(DatabaseManager.Items[itemBones1], new IntBounds(1, 2))),
                    new LootElement(0.25f, new LootItem(DatabaseManager.Items[itemBones2], new IntBounds(1, 3))),
                    new LootElement(new LootTemplateReference(lootGoblin))
                )),
                Money = new IntBounds(30, 70),
            });
            int crGoblinL4 = DatabaseManager.Creatures.Add(new CreatureTemplate()
            {
                Name = Text.Make("Goblin Chief", "Вождь гоблинов"),
                Size = SizeFactory.Cylinder(0.75f, 3.0f),
                Model = raceModels[(byte)CreatureRace.Goblin][6],
                LightData = new LightData(new Vector3(R.Float(0.9f, 1.0f), R.Float(0.6f, 0.8f), R.Float(0.3f, 0.5f)), R.Float(0.5f, 1.0f), R.Float(10, 50)),
                Level = new IntBounds(4),
                Faction = DatabaseManager.Factions[factionGoblin],
                Family = CreatureFamily.Humanoid,
                Script = DatabaseManager.GetCreatureScript("generic_creature"),
                Loot = DatabaseManager.Loot.AddAndReturn(new LootTemplate("Goblin.L4",
                    new LootElement(new LootTemplateReference(lootL4)),
                    new LootElement(new LootTemplateReference(lootGoblin))
                )),
                Money = new IntBounds(50, 100),
            });
            int crWolfL1 = DatabaseManager.Creatures.Add(new CreatureTemplate()
            {
                Name = Text.Make("Wolf", "Волк"),
                Size = SizeFactory.Cylinder(0.5f, 2.0f),
                Model = raceModels[(byte)CreatureRace.Wolf][0],
                Level = new IntBounds(1, 2),
                Faction = DatabaseManager.Factions[factionWolf],
                Family = CreatureFamily.Animal,
                Script = DatabaseManager.GetCreatureScript("generic_creature"),
                Loot = DatabaseManager.Loot.AddAndReturn(new LootTemplate("Wolf.L1-2",
                    new LootElement(new LootGroup(
                        new LootElement(0.5f, new LootTemplateReference(lootL1)),
                        new LootElement(0.5f, new LootTemplateReference(lootL2))
                    )),
                    new LootElement(new LootTemplateReference(lootWolf))
                )),
                Money = new IntBounds(5, 10),
            });
            int crWolfL3 = DatabaseManager.Creatures.Add(new CreatureTemplate()
            {
                Name = Text.Make("Fierce Wolf", "Свирепый волк"),
                Size = SizeFactory.Cylinder(0.75f, 3.0f),
                Model = raceModels[(byte)CreatureRace.Wolf][1],
                Level = new IntBounds(3, 4),
                Faction = DatabaseManager.Factions[factionWolf],
                Family = CreatureFamily.Animal,
                Script = DatabaseManager.GetCreatureScript("generic_creature"),
                Loot = DatabaseManager.Loot.AddAndReturn(new LootTemplate("Wolf.L3-4",
                    new LootElement(new LootGroup(
                        new LootElement(0.5f, new LootTemplateReference(lootL3)),
                        new LootElement(0.5f, new LootTemplateReference(lootL4))
                    )),
                    new LootElement(new LootTemplateReference(lootWolf))
                )),
                Money = new IntBounds(20, 30),
            });
            int crRatL1 = DatabaseManager.Creatures.Add(new CreatureTemplate()
            {
                Name = Text.Make("Rat", "Крыса"),
                Size = SizeFactory.Cylinder(0.5f, 2.0f),
                Model = raceModels[(byte)CreatureRace.Rat][0],
                Level = new IntBounds(1, 2),
                Faction = DatabaseManager.Factions[factionRat],
                Family = CreatureFamily.Animal,
                Script = DatabaseManager.GetCreatureScript("generic_creature"),
                Loot = DatabaseManager.Loot.AddAndReturn(new LootTemplate("Rat.L1-2",
                    new LootElement(new LootGroup(
                        new LootElement(0.5f, new LootTemplateReference(lootL1)),
                        new LootElement(0.5f, new LootTemplateReference(lootL2))
                    )),
                    new LootElement(new LootTemplateReference(lootRat))
                )),
                Money = new IntBounds(5, 10),
            });
            int crRatL3 = DatabaseManager.Creatures.Add(new CreatureTemplate()
            {
                Name = Text.Make("Giant Rat", "Большая крыса"),
                Size = SizeFactory.Cylinder(0.75f, 3.0f),
                Model = raceModels[(byte)CreatureRace.Rat][0],
                Level = new IntBounds(3, 4),
                Faction = DatabaseManager.Factions[factionRat],
                Family = CreatureFamily.Animal,
                Script = DatabaseManager.GetCreatureScript("generic_creature"),
                Loot = DatabaseManager.Loot.AddAndReturn(new LootTemplate("Rat.L3-4",
                    new LootElement(new LootGroup(
                        new LootElement(0.5f, new LootTemplateReference(lootL3)),
                        new LootElement(0.5f, new LootTemplateReference(lootL4))
                    )),
                    new LootElement(new LootTemplateReference(lootRat))
                )),
                Money = new IntBounds(15, 25),
            });
            #endregion

            #region Quests
            int quest1 = DatabaseManager.Quests.Add(new Quest
            {
                Title = Text.Make("Green Threat", "Зелёная угроза"),

                AcceptText = Text.Make("Quest AcceptText", "{ruRU Quest AcceptText}"),
                CompletedText = Text.Make("Quest CompletedText", "{ruRU Quest CompletedText}"),
                StatusQueryText = Text.Make("Quest StatusQueryText", "{ruRU Quest StatusQueryText}"),

                RewardMoney = 100,
                RewardExperience = 1000,
                RewardItems = new List<ItemDefinitionBase>
                {
                    new ItemDefinition(DatabaseManager.Items[itemQuestReward1R1], 1),
                    new ItemDefinition(DatabaseManager.Items[itemQuestReward1R2], 5),
                },

                Objectives = new List<QuestObjective>
                {
                    new QuestObjective(QuestObjectiveType.Kill, crGoblinL1, 2),
                    new QuestObjective(QuestObjectiveType.Item, itemQuest1, 5),
                },
            });
            int quest2 = DatabaseManager.Quests.Add(new Quest
            {
                Title = Text.Make("Green Threat", "Зелёная угроза"),

                AcceptText = Text.Make("Quest AcceptText", "{ruRU Quest AcceptText}"),
                CompletedText = Text.Make("Quest CompletedText", "{ruRU Quest CompletedText}"),
                StatusQueryText = Text.Make("Quest StatusQueryText", "{ruRU Quest StatusQueryText}"),

                RewardMoney = 200,
                RewardExperience = 1500,
                RewardItems = new List<ItemDefinitionBase>
                {
                    new ItemDefinition(DatabaseManager.Items[itemQuestReward2R1], 1),
                    new ItemDefinition(DatabaseManager.Items[itemQuestReward2R2], 2),
                },

                Objectives = new List<QuestObjective>
                {
                    new QuestObjective(QuestObjectiveType.Kill, crGoblinL2, 2),
                    new QuestObjective(QuestObjectiveType.Kill, crGoblinL3, 2),
                },

                RequiresQuestsCompleted = new List<Quest>
                {
                    DatabaseManager.Quests[quest1],
                },
            });
            int quest3 = DatabaseManager.Quests.Add(new Quest
            {
                Title = Text.Make("King of the Hill", "Король Горы"),

                AcceptText = Text.Make("Quest AcceptText", "{ruRU Quest AcceptText}"),
                CompletedText = Text.Make("Quest CompletedText", "{ruRU Quest CompletedText}"),
                StatusQueryText = Text.Make("Quest StatusQueryText", "{ruRU Quest StatusQueryText}"),

                RewardMoney = 350,
                RewardExperience = 2000,
                RewardItems = new List<ItemDefinitionBase>
                {
                    new ItemDefinition(DatabaseManager.Items[itemQuestReward3R1], 1),
                    new ItemDefinition(DatabaseManager.Items[itemQuestReward3R2], 1),
                },

                Objectives = new List<QuestObjective>
                {
                    new QuestObjective(QuestObjectiveType.Kill, crGoblinL4, 1) { OverrideText = Text.Make("Become the King of the Hill", "Стань Королём Горы") },
                },

                RequiresQuestsCompleted = new List<Quest>
                {
                    DatabaseManager.Quests[quest2],
                },
            });
            DatabaseManager.Creatures[crNPCTest].Quests.Add(DatabaseManager.Quests[quest1]);
            DatabaseManager.Creatures[crNPCTest].Quests.Add(DatabaseManager.Quests[quest2]);
            DatabaseManager.Creatures[crNPCTest].Quests.Add(DatabaseManager.Quests[quest3]);
            #endregion

            #region Environments
            int envDayStale = DatabaseManager.Environments.Add(new Environment()
            {
                AmbientLight = new Vector3(1.00f),
                FogStart = 200,
                FogEnd = 1000,
                FogColor = Color.LightSkyBlue.ToVector4(),
                Wind = new Vector3(0, 0, 15),
                ChangeDuration = 5,
                ChangeInterval = new FloatBounds(30, 60),
            });
            int envDayCalm = DatabaseManager.Environments.Add(new Environment()
            {
                AmbientLight = new Vector3(0.95f),
                FogStart = 200,
                FogEnd = 1000,
                FogColor = Color.LightSkyBlue.ToVector4(),
                Wind = new Vector3(0.2f, 0.05f, 15),
                ChangeDuration = 5,
                ChangeInterval = new FloatBounds(60, 120),
            });
            int envDayWindy = DatabaseManager.Environments.Add(new Environment()
            {
                AmbientLight = new Vector3(0.75f),
                FogStart = 100,
                FogEnd = 300,
                FogColor = Color.LightSteelBlue.ToVector4(),
                Wind = new Vector3(0.8f, 0.2f, 5),
                ChangeDuration = 15,
                ChangeInterval = new FloatBounds(30, 60),
            });
            int envDayStorm = DatabaseManager.Environments.Add(new Environment()
            {
                AmbientLight = new Vector3(0.5f),
                FogStart = 20,
                FogEnd = 100,
                FogColor = Color.LightGray.ToVector4(),
                Wind = new Vector3(1.5f, 0.6f, 3),
                ChangeDuration = 10,
                ChangeInterval = new FloatBounds(30, 60),
            });
            int envDayFlurry = DatabaseManager.Environments.Add(new Environment()
            {
                AmbientLight = new Vector3(0.3f),
                FogStart = 0,
                FogEnd = 30,
                FogColor = Color.DarkGray.ToVector4(),
                Wind = new Vector3(2.5f, -1.6f, 1),
                ChangeDuration = 5,
                ChangeInterval = new FloatBounds(20, 40),
            });
            int envNightStale = DatabaseManager.Environments.Add(new Environment()
            {
                AmbientLight = new Vector3(0.05f),
                FogStart = 50,
                FogEnd = 200,
                FogColor = Color.Black.ToVector4(),
                Wind = new Vector3(0, 0, 15),
                ChangeDuration = 5,
                ChangeInterval = new FloatBounds(30, 60),
            });

            DatabaseManager.Environments[envDayStale].ChangesTo = new[]
            {
                new EnvironmentChangeEntry(4, envDayCalm),
                new EnvironmentChangeEntry(1, envDayWindy),
            };
            DatabaseManager.Environments[envDayCalm].ChangesTo = new[]
            {
                new EnvironmentChangeEntry(2, envDayCalm),
                new EnvironmentChangeEntry(1, envDayWindy),
            };
            DatabaseManager.Environments[envDayWindy].ChangesTo = new[]
            {
                new EnvironmentChangeEntry(2, envDayCalm),
                new EnvironmentChangeEntry(2, envDayWindy),
                new EnvironmentChangeEntry(1, envDayStorm),
            };
            DatabaseManager.Environments[envDayStorm].ChangesTo = new[]
            {
                new EnvironmentChangeEntry(3, envDayWindy),
                new EnvironmentChangeEntry(2, envDayStorm),
                new EnvironmentChangeEntry(1, envDayFlurry),
            };
            DatabaseManager.Environments[envDayFlurry].ChangesTo = new[]
            {
                new EnvironmentChangeEntry(1, envDayWindy),
                new EnvironmentChangeEntry(3, envDayStorm),
            };
            DatabaseManager.Environments[envNightStale].ChangesTo = new[]
            {
                new EnvironmentChangeEntry(1, envNightStale),
            };
            #endregion

            #region Maps
            /*MapTemplate templateMap = new MapTemplate(32 * 15, 32 * 15, 32 * 15, 128, 128, 32, DatabaseManager.Environments[envDayWindy]);
            templateMap.HeightMap = HeightMapData.FromBitmap((System.Drawing.Bitmap)System.Drawing.Image.FromFile("Maps\\hmaptest_oh.png"), templateMap.SizeX, templateMap.SizeY, templateMap.SizeZ);
            for (int y = 0; y < templateMap.Terrain.DimY; y++)
                for (int x = 0; x < templateMap.Terrain.DimX; x++)
                {
                    templateMap.Terrain.Patches[x, y].BaseMaterialID = matGroundBase;
                    templateMap.Terrain.Patches[x, y].Layer0MaterialID = matGroundLayer0;
                    templateMap.Terrain.Patches[x, y].Layer0AlphaID = tf++;
                    templateMap.Terrain.Patches[x, y].Layer1MaterialID = matGroundLayer1;
                    templateMap.Terrain.Patches[x, y].Layer1AlphaID = tf++;
                    templateMap.Terrain.Patches[x, y].Scale = 8;
                }
            for (int i = 0; i < 10; i++)
                templateMap.Spawns.Add(new MapCreatureSpawn(0, crTestEnemy, new Vector3(R.Float(200), R.Float(200), 0), Rotation.Zero));
            for (int i = 0; i < 10; i++)
                templateMap.Spawns.Add(new MapCreatureSpawn(0, crGoblinL1, new Vector3(R.Float(200), R.Float(200), 0), R.Yaw()));
            for (int i = 0; i < 10; i++)
                templateMap.Spawns.Add(new MapCreatureSpawn(0, crGoblinL2, new Vector3(100 + R.Float(200), R.Float(200), 0), R.Yaw()));
            for (int i = 0; i < 10; i++)
                templateMap.Spawns.Add(new MapCreatureSpawn(0, crGoblinL3, new Vector3(200 + R.Float(200), R.Float(200), 0), R.Yaw()));
            for (int i = 0; i < 5; i++)
                templateMap.Spawns.Add(new MapCreatureSpawn(0, crGoblinL4, new Vector3(300 + R.Float(50), 25 + R.Float(50), 0), R.Yaw()));
            for (int i = 0; i < 10; i++)
                templateMap.Spawns.Add(new MapObjectSpawn(0, woTree, new Vector3(R.Float(200), R.Float(200), 0), new Rotation(R.Float(MathHelper.TwoPi))));
            for (int i = 0; i < 1000; i++)
            {
                Vector3 pos = new Vector3(R.Float(500), R.Float(500), 0);
                pos.Z = templateMap.GetHeight(pos.X, pos.Y);
                templateMap.Statics.Add(new StaticModelObjectBase(DatabaseManager.Models[modelTree] as WorldModel, StaticModelObjectFlags.Solid | StaticModelObjectFlags.GroundedOnTerrain, Shape.Cylinder, pos, new Rotation(R.Float(MathHelper.TwoPi)), SizeFactory.Cylinder(0.75f, 5.0f)));
            }
            for (int i = 0; i < 5000; i++)
            {
                Vector3 pos = new Vector3(R.Float(100), R.Float(100), 0);
                pos.Z = templateMap.GetHeight(pos.X, pos.Y);
                int r = R.Int(100);
                templateMap.Statics.Add(new StaticModelObjectBase(DatabaseManager.Models[r >= 90 ? modelGrass3 : r >= 80 ? modelGrass2 : modelGrass1] as WorldModel, StaticModelObjectFlags.GroundedOnTerrain | StaticModelObjectFlags.CellOcclusion | StaticModelObjectFlags.ReducedVisibilityDistance, Shape.Cylinder, pos, new Rotation(R.Float(MathHelper.TwoPi)), SizeFactory.Cylinder(0, R.Float(0.75f, 1.0f))));
            }
            templateMap.Spawns.Add(new MapCreatureSpawn(0, crNPCTest, new Vector3(15, 15, 0), Rotation.Zero));
            DatabaseManager.Maps.Add(templateMap);

            templateMap = new MapTemplate(32 * 5, 32 * 5, 32 * 5, 64, 64, 64, DatabaseManager.Environments[envNightStale]);
            templateMap.HeightMap = HeightMapData.FromBitmap((System.Drawing.Bitmap)System.Drawing.Image.FromFile("Maps\\hmap2.png"), templateMap.SizeX, templateMap.SizeY, templateMap.SizeZ);
            for (int y = 0; y < templateMap.Terrain.DimY; y++)
                for (int x = 0; x < templateMap.Terrain.DimX; x++)
                {
                    templateMap.Terrain.Patches[x, y].BaseMaterialID = matGroundLayer0;
                    templateMap.Terrain.Patches[x, y].Scale = 8;
                }
            for (int i = 0; i < 10; i++)
                templateMap.Spawns.Add(new MapCreatureSpawn(0, crTestEnemy, new Vector3(R.Float(200), R.Float(200), 0), Rotation.Zero));
            templateMap.Spawns.Add(new MapCreatureSpawn(0, crNPCTest, new Vector3(15, 15, 0), Rotation.Zero));
            DatabaseManager.Maps.Add(templateMap);*/

            MapTemplate templateMap = new MapTemplate(1024, 1024, 1024, 256, 256, 32, DatabaseManager.Environments[envDayWindy]);
            for (int y = 0; y < templateMap.Terrain.DimY; y++)
                for (int x = 0; x < templateMap.Terrain.DimX; x++)
                {
                    templateMap.Terrain.Patches[x, y].BaseMaterialID = matGroundBase;
                    templateMap.Terrain.Patches[x, y].Scale = 8;
                }
            DatabaseManager.Maps.Add(templateMap);

            templateMap = new MapTemplate(512, 512, 512, 128, 128, 64, DatabaseManager.Environments[envNightStale]);
            for (int y = 0; y < templateMap.Terrain.DimY; y++)
                for (int x = 0; x < templateMap.Terrain.DimX; x++)
                {
                    templateMap.Terrain.Patches[x, y].BaseMaterialID = matGroundBase;
                    templateMap.Terrain.Patches[x, y].Scale = 8;
                }
            DatabaseManager.Maps.Add(templateMap);

            staticData.InvalidLocationMapGUID = 1;
            staticData.InvalidLocationPosition = Vector3.Zero;
            staticData.InvalidLocationRotation = Rotation.Zero;
            #endregion

            #region Races
            DatabaseManager.RaceInfos.Add(id = (byte)PlayerRace.Human, new RaceInfo()
            {
                Models = raceModels[id],
                Spells = new[]
                {
                    DatabaseManager.Spells[spellFireball],
                    DatabaseManager.Spells[spellHaste],
                    DatabaseManager.Spells[spellBlazingFire],
                    DatabaseManager.Spells[spellToxicCloud],
                },
            });
            DatabaseManager.RaceInfos.Add(id = (byte)PlayerRace.WoodElf, new RaceInfo()
            {
                Models = raceModels[id],
                Spells = new[]
                {
                    DatabaseManager.Spells[spellFireball],
                    DatabaseManager.Spells[spellHaste],
                    DatabaseManager.Spells[spellBlazingFire],
                    DatabaseManager.Spells[spellToxicCloud],
                },
            });
            DatabaseManager.RaceInfos.Add(id = (byte)PlayerRace.Minotaur, new RaceInfo()
            {
                Models = raceModels[id],
                Spells = new[]
                {
                    DatabaseManager.Spells[spellFireball],
                    DatabaseManager.Spells[spellHaste],
                    DatabaseManager.Spells[spellBlazingFire],
                    DatabaseManager.Spells[spellToxicCloud],
                },
            });
            DatabaseManager.RaceInfos.Add(id = (byte)PlayerRace.Orc, new RaceInfo()
            {
                Models = raceModels[id],
                Spells = new[]
                {
                    DatabaseManager.Spells[spellFireball],
                    DatabaseManager.Spells[spellHaste],
                    DatabaseManager.Spells[spellBlazingFire],
                    DatabaseManager.Spells[spellToxicCloud],
                },
            });
            DatabaseManager.RaceInfos.Add(id = (byte)PlayerRace.Lizardman, new RaceInfo()
            {
                Models = raceModels[id],
                Spells = new[]
                {
                    DatabaseManager.Spells[spellFireball],
                    DatabaseManager.Spells[spellHaste],
                    DatabaseManager.Spells[spellBlazingFire],
                    DatabaseManager.Spells[spellToxicCloud],
                },
            });
            #endregion

            #region Classes
            DatabaseManager.ClassInfos.Add(id = (byte)PlayerClass.Warrior, new ClassInfo()
            {
                Spells = new[]
                {
                    DatabaseManager.Spells[spellWarriorStrike],
                    DatabaseManager.Spells[spellWarriorThrow],
                    DatabaseManager.Spells[spellWarriorDefense],
                    DatabaseManager.Spells[spellWarriorFrenzy],
                },
                Items = new[]
                {
                    new ItemDefinition(DatabaseManager.Items[itemPotionHealth], 1),
                    new ItemDefinition(DatabaseManager.Items[itemPotionEnergy], 1),
                },
                Equipment = new Dictionary<EquipSlot, ItemTemplate>
                {
                    { EquipSlot.Chest, DatabaseManager.Items[itemClassStartWarrior1] },
                    { EquipSlot.Feet, DatabaseManager.Items[itemClassStartWarrior2] },
                    { EquipSlot.MainHand, DatabaseManager.Items[itemClassStartWarrior3] },
                },
            });
            DatabaseManager.ClassInfos.Add(id = (byte)PlayerClass.Rogue, new ClassInfo()
            {
                Spells = new[]
                {
                    DatabaseManager.Spells[spellRogueRapid],
                    DatabaseManager.Spells[spellRogueDodge],
                    DatabaseManager.Spells[spellRogueCut],
                    DatabaseManager.Spells[spellRogueHide],
                },
                Items = new[]
                {
                    new ItemDefinition(DatabaseManager.Items[itemPotionHealth], 1),
                    new ItemDefinition(DatabaseManager.Items[itemPotionEnergy], 1),
                },
                Equipment = new Dictionary<EquipSlot, ItemTemplate>
                {
                    { EquipSlot.Chest, DatabaseManager.Items[itemClassStartRogue1] },
                    { EquipSlot.Feet, DatabaseManager.Items[itemClassStartRogue2] },
                    { EquipSlot.MainHand, DatabaseManager.Items[itemClassStartRogue3] },
                },
            });
            DatabaseManager.ClassInfos.Add(id = (byte)PlayerClass.Mage, new ClassInfo()
            {
                Spells = new[]
                {
                    DatabaseManager.Spells[spellMageFireball],
                    DatabaseManager.Spells[spellMageIce],
                    DatabaseManager.Spells[spellMageCircle],
                    DatabaseManager.Spells[spellMageEarthquake],
                },
                Items = new[]
                {
                    new ItemDefinition(DatabaseManager.Items[itemPotionHealth], 1),
                    new ItemDefinition(DatabaseManager.Items[itemPotionMana], 1),
                },
                Equipment = new Dictionary<EquipSlot, ItemTemplate>
                {
                    { EquipSlot.Chest, DatabaseManager.Items[itemClassStartMage1] },
                    { EquipSlot.Feet, DatabaseManager.Items[itemClassStartMage2] },
                    { EquipSlot.Head, DatabaseManager.Items[itemClassStartMage3] },
                },
            });
            DatabaseManager.ClassInfos.Add(id = (byte)PlayerClass.Cultist, new ClassInfo()
            {
                Spells = new[]
                {
                    DatabaseManager.Spells[spellCultistSteal],
                    DatabaseManager.Spells[spellCultistSacrifice],
                    DatabaseManager.Spells[spellCultistShroud],
                    DatabaseManager.Spells[spellCultistMeditation],
                },
                Items = new[]
                {
                    new ItemDefinition(DatabaseManager.Items[itemPotionHealth], 1),
                    new ItemDefinition(DatabaseManager.Items[itemPotionMana], 1),
                },
                Equipment = new Dictionary<EquipSlot, ItemTemplate>
                {
                    { EquipSlot.Chest, DatabaseManager.Items[itemClassStartCultist1] },
                    { EquipSlot.Feet, DatabaseManager.Items[itemClassStartCultist2] },
                    { EquipSlot.MainHand, DatabaseManager.Items[itemClassStartCultist3] },
                },
            });
            #endregion

            #region Localization
            staticData.LocalizationGame = new GameStrings
            {
                RaceNames = new[]
                {
                    Text.Make("Human", "Человек"),
                    Text.Make("Wood Elf", "Лесной эльф"),
                    Text.Make("Minotaur", "Минотавр"),
                    Text.Make("Orc", "Орк"),
                    Text.Make("Lizardman", "Ящер"),
                },
                ClassNames = new[]
                {
                    Text.Make("Warrior", "Воин"),
                    Text.Make("Rogue", "Разбойник"),
                    Text.Make("Mage", "Маг"),
                    Text.Make("Cultist", "Культист"),
                },
                StatNames = new[]
                {
                    Text.Make("Melee Power", "Ближний бой"),
                    Text.Make("Magic Power", "Сила магии"),
                    Text.Make("Swiftness", "Скорость"),
                    Text.Make("Stamina", "Выносливость"),
                    Text.Make("Intellect", "Интеллект"),
                    Text.Make("Spirit", "Дух"),

                    Text.Make("Physical Resistance", "Сопротивление физическому урону"),
                    Text.Make("Magic Resistance", "Сопротивление магии"),

                    Text.Make("Fire Magic Resistance", "Сопротивление стихии огня"),
                    Text.Make("Water Magic Resistance", "Сопротивление стихии воды"),
                    Text.Make("Ground Magic Resistance", "Сопротивление стихии земли"),
                    Text.Make("Air Magic Resistance", "Сопротивление стихии воздуха"),
                    Text.Make("Light Magic Resistance", "Сопротивление стихии света"),
                    Text.Make("Dark Magic Resistance", "Сопротивление стихии тьмы"),

                    Text.Make("Armor", "Броня"),
                },
                ItemSlotNames = new[]
                {
                    Text.Make("None", "Нет"),

                    Text.Make("Head", "Голова"),
                    Text.Make("Neck", "Шея"),
                    Text.Make("Back", "Спина"),
                    Text.Make("Shoulder", "Плечи"),
                    Text.Make("Hands", "Руки"),
                    Text.Make("Wrists", "Запястья"),
                    Text.Make("Chest", "Грудь"),
                    Text.Make("Waist", "Пояс"),
                    Text.Make("Legs", "Ноги"),
                    Text.Make("Feet", "Ступни"),

                    Text.Make("Accessory", "Аксесуар"),

                    Text.Make("One-handed", "Одноручное оружие"),
                    Text.Make("Two-handed", "Двуручное оружие"),
                    Text.Make("Main-hand", "Правая рука"),
                    Text.Make("Off-hand", "Левая рука"),
                },
                PowerTypeNames = new[]
                {
                    Text.Make("Mana", "Мана"),
                    Text.Make("Energy", "Энергия"),
                },

                MenuCreateCharacterModelPrev = Text.Make("<"),
                MenuCreateCharacterModelNext = Text.Make(">"),
                MenuCreateCharacterAnimStand = Text.Make("Idle", "Стоять"),
                MenuCreateCharacterAnimWalk = Text.Make("Walk", "Движение"),
                MenuCreateCharacterAnimAttack = Text.Make("Attack", "Атака"),
                MenuCreateCharacterName = Text.Make("Enter Name:", "Введите имя:"),
                MenuCreateCharacterRace = Text.Make("Race:", "Раса:"),
                MenuCreateCharacterClass = Text.Make("Class:", "Класс:"),
                MenuCreateCharacterSpells = Text.Make("Following spells will be available to you:", "Вам будут доступны следующие заклинания:"),
                MenuCreateCharacterCreate = Text.Make("Create", "Создать"),
                MenuCreateCharacterResult = new[]
                {
                    Text.Make("OK"),
                    Text.Make("You account is banned and you cannot create new characters.", "Ваша учётная запись заблокирована и вы не можете создавать новых персонажей."),
                    Text.Make("Character creation temporarily disabled.", "Создание персонажей временно приостановлено."),
                    Text.Make("This name is already in use.", "Это имя уже занято."),
                    Text.Make("This name does not correspond to the requirements.", "Это имя не удовлетворяет требованиям."),
                    Text.Make("This race is unavailable to you.", "Эта раса недоступна для вас."),
                    Text.Make("This class is unavailable to you.", "Этот класс недоступен для вас."),
                    Text.Make("You chose an unavailable model.", "Вы выбрали недоступную модель."),
                },

                HealthFormat = Text.Make("{0} / {2} ({3:0}%)"),
                PowerFormat = Text.Make("{4}: {0} / {2} ({3:0}%)"),
                AuraDurationMinutesFormat = Text.Make("{0:0} m", "{0:0} м."),
                AuraDurationTenSecsFormat = Text.Make("{0:0} s", "{0:0} с."),
                AuraDurationSecondsFormat = Text.Make("{0:0.0} s", "{0:0.0} с."),
                CastingFormat = Text.Make("{4} > {5}"),

                AuraCasterFormat = Text.Make("Source: {0}", "Источник: {0}"),

                CharacterWindowTabTitleEquipment = Text.Make("Equipment", "Снаряжение"),
                CharacterWindowTabTitleStats = Text.Make("Stats", "Статы"),
                CharacterWindowTabTitleSpells = Text.Make("Spells", "Заклинания"),
                CharacterWindowStatFormat = Text.Make(": {0}{1: (+0); (-0);}"),
                CharacterWindowWeaponDamageFormat = Text.Make("{0}-{1}"),
                CharacterWindowWeaponInvervalFormat = Text.Make("{0:0.##}-{1:0.##} s", "{0:0.##}-{1:0.##} с."),

                LootWindowTitle = Text.Make("Loot", "Добыча"),
                LootMoneyFormat = Text.Make("{0} Coins", "{0} Монет"),
            
                DialogAcceptQuestOption = Text.Make("Accept", "Принять"),
                DialogDeclineQuestOption = Text.Make("Decline", "Отказаться"),
                DialogCompleteQuestOption = Text.Make("Complete", "Завершить"),

                GroupInviteRequestFormat = Text.Make("{0} wants to invite you to a group.", "{0} хочет пригласить вас в группу."),
                GroupInviteAccept = Text.Make("Accept", "Принять"),
                GroupInviteDecline = Text.Make("Decline", "Отказать"),

                DuelRequestFormat = Text.Make("{0} challenges you to a duel.", "{0} вызывает вас на дуэль."),
                DuelAccept = Text.Make("Accept", "Принять"),
                DuelDecline = Text.Make("Decline", "Отказать"),

                ChatAliasWhisperTo = Text.Make("W To", "Ш"),
                ChatAliasWhisperFrom = Text.Make("W From", "От"),
                ChatTimestampFormat = Text.Make("hh:mm tt", "HH:mm"),

                LogMoneyAdded = Text.Make("Received money", "Получено денег"),
                LogMoneyLost = Text.Make("Lost money", "Потеряно денег"),
                LogItemsAddedSingular = Text.Make("Acquired item", "Получен предмет"),
                LogItemsAddedPlural = Text.Make("Acquired items", "Получены предметы"),
                LogItemsLostSingular = Text.Make("Lost item", "Потерян предмет"),
                LogItemsLostPlural = Text.Make("Lost items", "Потеряны предметы"),
                LogChannelJoined = Text.Make("Joined chat channel", "Вы вступили в канал"),
                LogChannelLeft = Text.Make("Left chat channel", "Вы вышли из канала"),
                LogPartyMemberJoined = Text.Make("joined the party", "вступил(а) в группу"),
                LogPartyMemberLeft = Text.Make("left the party", "вышел(ла) из группы"),
                LogPartyMemberLeader = Text.Make("is now the leader of your party", "стал(а) лидером группы"),
                LogDuelStatusStarted = Text.Make("Duel started", "Дуэль начался"),
                LogDuelStatusFinished = Text.Make("Duel finished", "Дуэль закончился"),
                LogDuelParticipantStatusWin = Text.Make("wins the duel", "побеждает в дуэле"),
                LogDuelParticipantStatusLose = Text.Make("loses the duel", "терпит поражение в деэле"),

                TooltipItemNameSingularFormat = Text.Make("{0}"),
                TooltipItemNamePluralFormat = Text.Make("{1}x {0}"),
                TooltipItemSlotFormat = Text.Make("({0})"),
                TooltipItemRequiredLevelBothFormat = Text.Make("Requires level {0}-{1}", "Требует уровень {0}-{1}"),
                TooltipItemRequiredLevelMinFormat = Text.Make("Requires level {0}", "Требует уровень {0}"),
                TooltipItemRequiredLevelMaxFormat = Text.Make("Requires level below {0}", "Требует уровень ниже {0}"),
                TooltipItemWeaponDamageFormat = Text.Make("Damage: {0}-{1}", "Урон: {0}-{1}"),
                TooltipItemWeaponSpeedFormat = Text.Make("Speed: {0:0.##}-{1:0.##} sec", "Скорость: {0:0.##}-{1:0.##} сек."),
                TooltipItemArmorFormat = Text.Make("{0} Armor", "Броня: {0}"),
                TooltipItemStatModFormat = Text.Make("{1:+0;-0;+0} {0}"),
                TooltipItemSpellTriggerFormats = new[]
                {
                    Text.Make("On use: {0}", "При использовании: {0}"),
                    Text.Make("On equip: {0}", "При одевании: {0}"),
                    Text.Make("On unequip: {0}", "При снимании: {0}"),
                    Text.Make("On pickup: {0}", "При получении: {0}"),
                    Text.Make("On lose: {0}", "При потере: {0}"),
                },
                TooltipItemDescriptionFormat = Text.Make("{0}"),
                TooltipItemLoreFormat = Text.Make("\"{0}\""),
                TooltipItemPriceless = Text.Make("Cannot be sold", "Нельзя продать"),
                TooltipItemPriceFormat = Text.Make("Sell price: {0} g", "Цена продажи: {0} з."),

                TooltipSpellRequiredPowerFormat = Text.Make("{1} {0}"),
                TooltipSpellCooldownFormat = Text.Make("Cooldown: {0} sec", "Перезарядка: {0} сек."),

                TooltipExperienceBarFormat = Text.Make("Current experience: {0}.\nRequired to gain a level: {1}.\n\nEarn experience by killing foes and completing quests. Once you earn enough experience, you will gain a level.", "Текущий опыт: {0}.\nНеобходимо для след. уровня: {1}.\n\nЗарабатывайте опыт убивая врагов и выполняя задания. При накоплении достаточного количества опыта вы получите новый уровень."),
                TooltipExperienceLevelFormat = Text.Make("Current level: {0}.\n\nIncrease level to gaining experience to learn new and improve already known abilities and overcome more difficult challenges.", "Текущий уровень: {0}.\n\nУвеличивайте уровень получая опыт для изучения новых и улучшения уже известных способностей, получая возможность проходить более сложные испытания."),

                ErrorWorldLoginResult = new[]
                {
                    Text.Make("OK"),
                    Text.Make("This world temporarily unavailable", "Этот мир временно недоступен"),
                    Text.Make("You are not logged in", "Вы не авторизированы"),
                    Text.Make("Your account was not found", "Ваша учётная запись не найдена"),
                },
                ErrorWorldEnterResult = new[]
                {
                    Text.Make("OK"),
                    Text.Make("The world you are trying to join is full", "Мир, в который вы пытались войти, заполнен"),
                    Text.Make("The character you selected is invalid", "Персонаж, которого вы выбрали, недоступен"),
                    Text.Make("The character you selected is banned", "Персонаж, которого вы выбрали, заблокирован"),
                    Text.Make("The location you are trying to enter is invalid", "Вы не можете войти в текущее местонахождение"),
                },
                ErrorCharacterCreateResult = new[]
                {
                    Text.Make("OK"),
                    Text.Make("Your account is banned", "Ваша учётная запись заблокирована"),
                    Text.Make("Character creation is temporarily unavailable", "Создание персонажей временно недоступно"),
                    Text.Make("The name you entered is unavailable to you", "Имя, которое вы ввели, недоступно для вас"),
                    Text.Make("The name you entered is inappropriate", "Имя, которое вы ввели, неприемлимо"),
                    Text.Make("The race you chose is unavailable to you", "Раса, которую вы выбрали, недоступна вам"),
                    Text.Make("The class you chose is unavailable to you", "Класс, который вы выбрали, недоступен вам"),
                    Text.Make("The model you selected is invalid", "Модель, которую вы выбрали, недоступна"),
                },
                ErrorGroupInviteResult = new[]
                {
                    Text.Make("OK"),
                    Text.Make("The player you tried to invite was not found", "Игрок, которого вы попытались пригласить, не найден"),
                    Text.Make("The player you tried to invite is already in a group", "Игрок, которого вы попытались пригласить, уже находится в группе"),
                    Text.Make("Your group is full", "Группа заполнена"),
                    Text.Make("You cannot invite yourself", "Вы не можете пригласить себя"),
                },
                ErrorGroupKickResult = new[]
                {
                    Text.Make("OK"),
                    Text.Make("The player you tried to kick was not found", "Игрок, которого вы попытались исключить, не найден"),
                    Text.Make("The player you tried to kick is not in your group", "Игрок, которого вы попытались исключить, не состоит в вашей группе"),
                    Text.Make("You are not the leader of your group", "Вы не являетесь лидером вашей группы"),
                },
                ErrorChatMessageResult = new[]
                {
                    Text.Make("OK"),
                    Text.Make("Nobody hears you", "Вас никто не слышит"),
                    Text.Make("The message sender is unknown", "Неизвестный отправитель сообщения"),
                    Text.Make("The player you tried to message was not found", "Игрок, которому вы попыталсь написать, не найден"),
                    Text.Make("The channel you are trying to speak in was not found", "Канал, в котором вы попытались разговаривать, не найден"),
                    Text.Make("You are globally banned in chat", "Вы глобально заблокированы в чате"),
                    Text.Make("You are banned in this chat channel", "Вы заблокированы в этом канале чата"),
                    Text.Make("Your chatting priviliges are globally disabled", "Ваши привилегии общения в чате были глобально отобраны"),
                    Text.Make("Your chatting priviliges are disabled is this chat channel", "Ваши привилегии общения в чате были отобраны в этом канале чата"),
                    Text.Make("The chat channel you are trying to speak in belongs to another map", "Канал, в котором вы попытались разговаривать, относится к другой карте"),
                },
                ErrorChatChannelJoinResult = new[]
                {
                    Text.Make("OK"),
                    Text.Make("The channel you tried to join was not found", "Канал, к которому вы попытались присоединиться, не найден"),
                    Text.Make("You are banned in the channel you tried to join", "Вы заблонированы в канале, к которому вы попытались присоединиться"),
                    Text.Make("You have already joined this channel", "Вы уже присоединились к этому каналу"),
                },
                ErrorChatChannelLeaveResult = new[]
                {
                    Text.Make("OK"),
                    Text.Make("The channel you tried to leave was not found", "Канал, из которого вы попытались выйти, не найден"),
                    Text.Make("You have not joined this channel", "Вы не присоединены к этому каналу"),
                },
                ErrorItemTakeResult = new[]
                {
                    Text.Make("OK"),
                    Text.Make("You have insufficient space in your bags to take this item", "У вас недостаточно места чтобы взять этот предмет"),
                    Text.Make("You already have an item of that type", "У вас уже есть такой предмет"),
                    Text.Make("The item source was not found", "Источник предмета не найден"),
                    Text.Make("The item was not found", "Предмет не найден"),
                    Text.Make("The item was already looted", "Предмет уже был подобран"),
                },
                ErrorItemStoreResult = new[]
                {
                    Text.Make("OK"),
                    Text.Make("You cannot store an item of that type in this bag", "Вы не можете поместить такой предмет в эту сумку"),
                },
                ErrorItemMoveResult = new[]
                {
                    Text.Make("OK"),
                    Text.Make("You cannot move an item of that type to this bag", "Вы не можете переместить такой предмет в эту сумку"),
                    Text.Make("You cannot swap these items", "Вы не можете поменять эти предметы местами"),
                    Text.Make("The bag you tried to move the item to is invalid", "Сумка, в которую вы попытались переместить этот предмет, не существует"),
                    Text.Make("The bag you tried to move the item to was not found", "Сумка, в которую вы попытались переместить этот предмет, не найдена"),
                    Text.Make("The slot you tried to move the item to is invalid", "Слот, в который вы попытались переместить этот предмет, не существует"),
                    Text.Make("The item you tried to move does not exist", "Предмет, который вы попытались переместить, не найден"),
                    Text.Make("The item you tried to move is not in your inventory", "Предмет, который вы попытались переместить, не находится в вашем инвентаре"),
                },
                ErrorItemDivideResult = new[]
                {
                    Text.Make("OK"),
                    Text.Make("You cannot split an item stack of that type to this bag", "Вы не можете разделить кучу таких предметов в эту сумку"),
                    Text.Make("The slot you tried to split the item stack to is not empty", "Слот, в который вы попытались разделить эту кучу предметов, не пуст"),
                    Text.Make("The bag you tried to split the item stack to is invalid", "Сумка, в которую вы попытались разделить эту кучу предметов, не существует"),
                    Text.Make("The bag you tried to split the item stack to was not found", "Сумка, в которую вы попытались разделить эту кучу предметов, не найдена"),
                    Text.Make("The slot you tried to split the item stack to is invalid", "Слот, в который вы попытались разделить эту кучу предметов, не существует"),
                    Text.Make("The item stack you tried to split does not exist", "Куча предметов, которую вы попытались разделить, не найдена"),
                    Text.Make("The item stack you tried to split is not in your inventory", "Куча предметов, которую вы попытались разделить, не находится в вашем инвентаре"),
                    Text.Make("The item stack you tried to split cannot be split in that quantities", "Куча предметов, которую вы попытались разделить, не может быть разделена в таком количестве"),
                },
                ErrorItemDestroyResult = new[]
                {
                    Text.Make("OK"),
                    Text.Make("The item you tried to destroy does not exist", "Предмет, который вы попытались уничтожить, не найден"),
                    Text.Make("The item you tried to destroy does not belong to you", "Вы не владеете предметом, который вы попытались уничтожить"),
                    Text.Make("The item you tried to destroy is indestructible", "Предмет, который вы попытались уничтожить, не может быть уничтожен"),
                },
                ErrorItemUseResult = new[]
                {
                    Text.Make("OK"),
                    Text.Make("The item you tried to use does not exist", "Предмет, который вы попытались использовать, не найден"),
                    Text.Make("The item you tried to use does not belong to you", "Вы не владеете предметом, который вы попытались использовать"),
                    Text.Make("The item you tried to use cannot be used that way", "Предмет, который вы попытались использовать, не может быть использован таким образом"),
                    Text.Make("The target you tried to use the item on does not exist", "Цель, на которую вы попытались использовать предмет, не найдена"),
                    Text.Make("Item usage failed", "Не удалось использовать предмет"),
                    Text.Make("The item you tried to use requires you to be of higher level", "Вы должны быть более высокого уровня, чтобы использовать этот предмет"),
                    Text.Make("The item you tried to use requires you to be of lower level", "Вы слишком высокого уровня, чтобы использовать этот предмет"),
                },
                ErrorItemEquipResult = new[]
                {
                    Text.Make("OK"),
                    Text.Make("The item you tried to equip does not exist", "Предмет, который вы попытались одеть, не найден"),
                    Text.Make("The item you tried to equip does not belong to you", "Вы не владеете предметом, который вы попытались одеть"),
                    Text.Make("The item you tried to equip is already equipped", "Предмет, который вы попытались одеть, уже одет"),
                    Text.Make("All slots you can equip the item in are not empty", "Все слоты, в которые можно одеть этот предмет, уже заняты"),
                    Text.Make("The item you tried to equip cannot be equipped to that slot", "Предмет, который вы попытались одеть, не может быть одет в этот слот"),
                    Text.Make("The item you tried to equip requires you to be of higher level", "Вы должны быть более высокого уровня, чтобы одеть этот предмет"),
                    Text.Make("The item you tried to equip requires you to be of lower level", "Вы слишком высокого уровня, чтобы одеть этот предмет"),
                    Text.Make("The item you tried to equip cannot be equipped while holding a two-handed weapon", "Вы не можете одеть этот предмет, когда у вас в руках двуручное оружие"),
                },
                ErrorItemUnequipResult = new[]
                {
                    Text.Make("OK"),
                    Text.Make("The item you tried to unequip does not exist", "Предмет, который вы попытались снять, не найден"),
                    Text.Make("The item you tried to unequip is not equipped", "Предмет, который вы попытались снять, не одет"),
                    Text.Make("The slot you tried to unequip the item to is not empty", "Слот, в который вы попытались снять предмет, не пуст"),
                    Text.Make("You cannot unequip an item of that type to this bag", "Вы не можете снять такой предмет в эту сумку"),
                },
                ErrorLootingResult = new[]
                {
                    Text.Make("OK"),
                    Text.Make("The corpse you are trying to loot was not found", "Тело, которое вы попытались осмотреть, не найдено"),
                    Text.Make("The creature you are trying to loot is not dead", "Существо, которое вы попытались осмотреть, не мертво"),
                    Text.Make("The corpse you are trying to loot has already been looted", "Тело, которое вы попытались осмотреть, уже было осмотрено"),
                    Text.Make("You are already looting another corpse", "Вы уже осматриваете другое тело"),
                    Text.Make("The corpse you are trying to loot is already being looted", "Тело, которое вы попытались осмотреть, уже осматривается"),
                    Text.Make("You are not eligible to loot this corpse", "Вам не разрешено осматривать это тело"),
                    Text.Make("The corpse you are trying to loot is too far from you", "Тело, которое вы попытались осмотреть, слишком далеко от вас"),
                },
                ErrorQuestAcceptResult = new[]
                {
                    Text.Make("OK"),
                    Text.Make("The quest you tried to accept was not found", "Задание, которое вы попытались принять, не найдено"),
                    Text.Make("You are already on that quest", "Вы уже выполняете это задание"),
                },
                ErrorQuestRemoveResult = new[]
                {
                    Text.Make("OK"),
                    Text.Make("The quest you tried to remove was not found", "Задание, которое вы попытались отменить, не найдено"),
                    Text.Make("You are not on that quest", "Вы не выполняете это задание"),
                },
                ErrorQuestCompleteResult = new[]
                {
                    Text.Make("OK"),
                },
                ErrorDialogStartResult = new[]
                {
                    Text.Make("OK"),
                    Text.Make("Nobody to talk to", "Не с кем разговаривать"),
                    Text.Make("It does not want to talk", "Он(а) не хочет с вами разговаривать"),
                    Text.Make("You cannot talk this way to players", "Вы не можете разговаривать таким образом с игроками"),
                    Text.Make("You are too far to talk", "Вы слишком далеко для разговора"),
                    Text.Make("The creatures you tried to talk to is incapable at the moment", "Существо, с которым вы попытались поговорить, сейчас не в состоянии для разговора"),
                },
                ErrorDialogSelectOptionResult = new[]
                {
                    Text.Make("OK"),
                    Text.Make("You are not currently talking", "Вы не разговариваете"),
                    Text.Make("You cannot choose this option", "Вы не можете выбрать эту опцию"),
                },
                ErrorCastSpellResult = new[]
                {
                    Text.Make("OK"),
                    Text.Make("This spell is unknown to you", "Это заклинание неизвестно вам"),
                    Text.Make("You are already casting a spell", "Вы уже применяете заклинание"),
                    Text.Make("This spell is on cooldown", "Это заклинание находится на перезарядке"),
                    Text.Make("You cannot cast this spell", "Вы не можете применять это заклинание"),
                    Text.Make("You don't have enough power to cast this spell", "У вас недостаточно сил для этого заклинания"),
                    Text.Make("You have to target somebody", "Вы должны в кого-то целиться"),
                    Text.Make("You cannot cast the spell on this target", "Вы не можете применить это заклинание на эту цель"),
                    Text.Make("The target is dead", "Цель мертва"),
                    Text.Make("The target is out of sight", "Цель не в поле зрения"),
                    Text.Make("The target is too close", "Цель слишком далеко"),
                    Text.Make("The target is too far", "Цель слишком близко"),
                    Text.Make("You are facing the wrong way", "Вы смотрите в неправильном направлении"),
                    Text.Make("You were interrupted", "Применение заклинания было прервано"),
                    Text.Make("You cannot cast this spell in combat", "Вы не можете применить это заклинание в бою"),
                    Text.Make("You can only cast this spell in combat", "Вы можете применить это заклинание только в бою"),
                    Text.Make("You cannot cast this spell while stunned", "Вы не можете применить это заклинание будучи ошеломленным"),
                    Text.Make("You cannot cast this spell while silenced", "Вы не можете применить это заклинание будучи онемевшим"),
                },
                ErrorUnexistingChatChannel = Text.Make("Channel does not exist", "Такого канала не существует."),
            };
            staticData.GlobalChannelNames = new Dictionary<int, Text>
            {
                { ChatManager.CHANNEL_LOCAL_SAY, Text.Make("Say", "Сказать") },
                { ChatManager.CHANNEL_LOCAL_YELL, Text.Make("Yell", "Крикнуть") },
                { ChatManager.CHANNEL_LOCAL_EMOTE, Text.Make("Emote", "Эмоция") },
                { ChatManager.CHANNEL_MAP, Text.Make("Map", "Карта") },
                { ChatManager.CHANNEL_GROUP, Text.Make("Party", "Группа") },
            };
            staticData.GlobalChannelAliases = new Dictionary<int, Text>
            {
                { ChatManager.CHANNEL_LOCAL_SAY, Text.Make("S", "С") },
                { ChatManager.CHANNEL_LOCAL_YELL, Text.Make("Y", "К") },
                { ChatManager.CHANNEL_LOCAL_EMOTE, Text.Make("EM", "Э") },
                { ChatManager.CHANNEL_MAP, Text.Make("M", "М") },
                { ChatManager.CHANNEL_GROUP, Text.Make("P", "Г") },
            };
            #endregion

            #region Demo
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\m1a0_0x0.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\m1a0_1x0.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\m1a0_0x1.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\m1a0_1x1.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\m1a0_2x2.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\m1a0_2x1.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\m1a0_2x0.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\m1a0_3x0.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\m1a0_3x1.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\m1a0_3x2.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\m1a0_1x2.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\m1a0_0x2.png"));
            DatabaseManager.Textures.Add(new TextureInfo("Textures\\m1a1_2x0.png"));
            #endregion
            DatabaseManager.StaticData.Add(1, staticData);

            DatabaseManager.ConnectAccount();
            #region Accounts
            string[] predefinedAccounts = { "testaccount", "u2" };
            foreach (string account in predefinedAccounts)
                if (DatabaseManager.Players.Find(player => player.Name == account) == null)
                    DatabaseManager.Players.Add(new Player(account));

            Guild g = new Guild();
            DatabaseManager.Guilds.Add(g);
            #endregion
            DatabaseManager.CloseAccount();
            DatabaseManager.CloseWorld();
        }

        public static void LoadDatabase(bool preload)
        {
            DatabaseManager.ConnectAccount();
            DatabaseManager.ConnectWorld();
            ScriptLoader.LoadScripts();
            if (preload)
            {
                DatabaseManager.PreloadAccount();
                DatabaseManager.PreloadWorld();
            }

            StaticData staticData = DatabaseManager.StaticData[1];

            PlayerCreatureBase.FriendlyTargetDecalTextureID = staticData.FriendlyTargetDecalTextureID;
            PlayerCreatureBase.NeutralTargetDecalTextureID = staticData.NeutralTargetDecalTextureID;
            PlayerCreatureBase.HostileTargetDecalTextureID = staticData.HostileTargetDecalTextureID;

            DuelBase.MaxParticipants = staticData.MaxDuelParticipants;
            Duel.DuelFactions = staticData.DuelFactions;

            Localization.Game = staticData.LocalizationGame;

            ChatManager.GlobalChannelNames = staticData.GlobalChannelNames;
            ChatManager.GlobalChannelAliases = staticData.GlobalChannelAliases;

            WorldManager.InvalidLocationMapGUID = staticData.InvalidLocationMapGUID;
            WorldManager.InvalidLocationPosition = staticData.InvalidLocationPosition;
            WorldManager.InvalidLocationRotation = staticData.InvalidLocationRotation;
        }
    }
}