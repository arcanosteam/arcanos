﻿using Arcanos.Server.WorldServer.Data;
using Arcanos.Server.WorldServer.World;
using Arcanos.Shared;
using Microsoft.Xna.Framework;

namespace Arcanos.Server.WorldServer.Tests
{
    public static partial class MainTest
    {
        public static void PopulateWorld()
        {
            Map map = new Map(0, DatabaseManager.Maps[1]);
            WorldManager.PublicMaps[map.GUID] = map;
            WorldManager.SetSpawnForInvalidLocation(map.GUID, new Vector3(170, 120, map.Template.GetHeight(170, 120)), Rotation.Zero);

            map = new Map(0, DatabaseManager.Maps[2]);
            WorldManager.PublicMaps[map.GUID] = map;
        }
    }
}