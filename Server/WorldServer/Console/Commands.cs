﻿using System;
using Arcanos.Server.WorldServer.Data;
using Arcanos.Server.WorldServer.Items;
using Arcanos.Server.WorldServer.Net;
using Arcanos.Server.WorldServer.Objects;
using Arcanos.Server.WorldServer.Players;
using Arcanos.Server.WorldServer.Scripting.Game;
using Arcanos.Server.WorldServer.World;
using Arcanos.Shared;
using Arcanos.Shared.Items;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Spells;
using Arcanos.Utilities;
using Arcanos.Utilities.Console;
using Microsoft.Xna.Framework;
using Environment = Arcanos.Server.WorldServer.World.Environment;

namespace Arcanos.Server.WorldServer.Console
{
    public static class Commands
    {
        public const char SEPARATOR = ' ';
        public static readonly CommandGroup InitialGroup;
        public static CommandOutputInfoHandler InfoHandler;
        public static CommandOutputErrorHandler ErrorHandler;
        public static PlayerSession Session;

        static Commands()
        {
            Command.Separator = SEPARATOR;
            CommandGroup.Separator = SEPARATOR;
            ResetOutputHandlers();

            InitialGroup = new CommandGroup(null)
            {
                new Command("exit", "exit", args => true),
                new CommandGroup("account")
                {
                    new Command("create", "account create <username>", args =>
                    {
                        if (DatabaseManager.Players.Find(p => p.Name.ToLower() == args[0].ToLower()) != null)
                            return Error("Player with a username \"{0}\" already exists", args[0]);
                        Player player = new Player(args[0]);
                        int id = DatabaseManager.Players.Add(player);
                        Info("ID: {0}", id);
                        return true;
                    }),
                },
                new TargetedCommand("additem", "additem {player GUID} <id> [count]", (target, args) =>
                {
                    PlayerCreature pc = ObjectManager.GetPlayer(target) as PlayerCreature;
                    if (pc == null)
                        return Error("Player not found");
                    ItemTemplate item = DatabaseManager.Items[int.Parse(args[0])];
                    int count = args.Length > 1 ? int.Parse(args[1]) : 1;
                    if (pc.Character.Inventory.CanStoreItem(item, count) != ItemTakeResult.Ok)
                        return Error("Player can't store the item");
                    pc.Character.Inventory.AddItem(item, count);
                    return true;
                }),
                new SessionCommand("cast", "cast <spell>", args =>
                {
                    int id;
                    if (!int.TryParse(args[0], out id))
                        return Error("Spell ID is not in correct format");
                    CreatureBase target = Session.Creature.Target;
                    if (target == null)
                        return Error("No creature targeted");
                    Session.Creature.CastSpell(target, id, CastSpellFlags.IgnoreAllChecks);
                    return true;
                }),
                new CommandGroup("creature")
                {
                    new SessionCommand("add", "creature add <id>", args =>
                    {
                        int id;
                        if (!int.TryParse(args[0], out id))
                            return Error("Creature Template ID is not in correct format");
                        CreatureTemplate template = DatabaseManager.Creatures[id];
                        if (template == null)
                            return Error("Creature Template with ID {0} not found", id);
                        Session.Creature.Map.Add(new Creature(0, template, Session.Creature.Position, Session.Creature.Rotation));
                        return true;
                    }),
                },
                new CommandGroup("character")
                {
                    new Command("create", "character create <account id or name> <name> <race> <class> [model]", args =>
                    {
                        Player player;
                        {
                            int pID;
                            if (int.TryParse(args[0], out pID))
                                player = DatabaseManager.Players[pID];
                            else
                                player = DatabaseManager.Players.Find(p => p.Name.ToLower() == args[0].ToLower());
                            if (player == null)
                                return Error("Player \"{0}\" not found", args[0]);
                        }
                        PlayerRace playerRace;
                        if (!TryParseEnum(args[2], out playerRace))
                            return Error("Wrong race identifier: \"{0}\"", args[2]);
                        PlayerClass playerClass;
                        if (!TryParseEnum(args[3], out playerClass))
                            return Error("Wrong class identifier: \"{0}\"", args[3]);
                        byte modelIndex = 0;
                        if (args.Length > 4 && !byte.TryParse(args[4], out modelIndex))
                            return Error("Wrong model index: \"{0}\"", args[4]);
                        Character character = new Character(player, args[1], playerRace, playerClass, modelIndex, false);
                        player.Characters.Add(character);
                        int id = DatabaseManager.Characters.Add(character);
                        Info("ID: {0}", id);
                        return true;
                    }),
                },
                new SessionCommand("die", "die", args =>
                {
                    CreatureBase creature = Session.Creature.Target;
                    if (creature == null)
                        return Error("No creature targeted");
                    if (creature.Type.IsPlayer())
                        (creature as PlayerCreature).Die(Session.Creature);
                    else if (creature.Type.IsCreature())
                        (creature as Creature).Die(Session.Creature);
                    return true;
                }),
                new SessionCommand("environment", "environment [id]", args =>
                {
                    Map map = Session.Creature.Map as Map;
                    if (args.Length == 0)
                    {
                        Info("Current environment: {0}\nTime to next change: {1}", (map.Environment as Environment).CurrentID, (map.Environment as Environment).ChangeIntervalLeft);
                        return true;
                    }
                    int id;
                    if (!int.TryParse(args[0], out id))
                        return Error("Environment ID is not in correct format");
                    (map.Environment as Environment).ChangeTo(map, DatabaseManager.Environments[id]);
                    return true;
                }),
                new SessionCommand("faction", "faction [id]", args =>
                {
                    CreatureBase creature = Session.Creature.Target;
                    if (creature == null)
                        return Error("No creature targeted");
                    if (args.Length == 0)
                    {
                        Info("Current faction: {0}", creature.Faction == null ? 0 : creature.Faction.ID);
                        return true;
                    }
                    creature.Faction = DatabaseManager.Factions[int.Parse(args[0])];
                    return true;
                }),
                new SessionCommand("level", "level <level>", args =>
                {
                    int level;
                    if (!int.TryParse(args[0], out level))
                        return Error("Level is not in correct format");
                    if (level < 1)
                        return Error("Level must be >= 1");
                    CreatureBase target = Session.Creature.Target ?? Session.Creature;
                    if (target.Type.IsPlayer())
                        for (int i = 0; i < level; i++)
                            (target as PlayerCreature).Character.GainLevel();
                    else
                        ++target.Level;
                    return true;
                }),
                new CommandGroup("log")
                {
                    new Command("toggle", "log toggle <category>", args =>
                    {
                        LogCategory category;
                        if (!TryParseEnum(args[0], out category))
                            return Error("Wrong category identifier: \"{0}\"", args[0]);
                        Logger.Settings.SetEnabled(category, !Logger.Settings.ShouldLog(category));
                        Info("Logging of category {0} turned {1}", category, Logger.Settings.ShouldLog(category) ? "on" : "off");
                        return true;
                    }),
                    new Command("severity", "log severity <severity>", args =>
                    {
                        LogSeverity severity;
                        if (!TryParseEnum(args[0], out severity))
                            return Error("Wrong severity identifier: \"{0}\"", args[0]);
                        Logger.Settings.MinimumSeverity = severity;
                        Info("Logging severity set to {0}", severity);
                        return true;
                    }),
                },
                new SessionCommand("model", "model [id]", args =>
                {
                    CreatureBase creature = Session.Creature.Target;
                    if (creature == null)
                        return Error("No creature targeted");
                    if (args.Length == 0)
                    {
                        Info("Current model: {0}", creature.Model == null ? 0 : creature.Model.Model.ID);
                        return true;
                    }
                    creature.Model = DatabaseManager.Models[int.Parse(args[0])].CreateInstance(creature);
                    return true;
                }),
                new Command("reload", "reload", args =>
                {
                    DatabaseManager.CloseAccount();
                    DatabaseManager.CloseWorld();
                    DatabaseManager.ConnectAccount();
                    DatabaseManager.ConnectWorld();
                    ScriptLoader.LoadScripts();
                    DatabaseManager.PreloadAccount();
                    DatabaseManager.PreloadWorld();
                    return true;
                }),
                new TargetedCommand("revive", "revive {player GUID} [%health] [%power]", (target, args) =>
                {
                    float healthPct = 1, powerPct = 1;
                    if (args.Length > 0)
                        if (!float.TryParse(args[0], out healthPct))
                            return Error("Health percent is not in correct format");
                    if (args.Length > 1)
                        if (!float.TryParse(args[1], out powerPct))
                            return Error("Power percent is not in correct format");
                    PlayerCreature pc = ObjectManager.GetPlayer(target) as PlayerCreature;
                    if (pc == null)
                        return Error("Player not found");
                    pc.Revive(healthPct, powerPct);
                    return true;
                }),
                new TargetedCommand("teleport", "teleport {creature GUID} x y [z]", (target, args) =>
                {
                    Vector3 pos = Vector3.Zero;
                    CreatureBase creature = ObjectManager.GetCreature(target);
                    if (creature == null)
                        return Error("Creature not found");
                    pos.X = float.Parse(args[0]);
                    pos.Y = float.Parse(args[1]);
                    pos.Z = creature.Map.Template.GetHeight(pos.X, pos.Y);
                    if (args.Length > 2)
                        if (!float.TryParse(args[2], out pos.Z))
                            return Error("Z coordinate is not in correct format");
                    creature.MoveTo(pos);
                    creature.Mover.MoveToPointOverTime(pos, 0.5f);
                    return true;
                }),
                new SessionCommand("xp", "xp <xp>", args =>
                {
                    int xp;
                    if (!int.TryParse(args[0], out xp))
                        return Error("XP is not in correct format");
                    if (xp < 1)
                        return Error("XP must be >= 1");
                    CreatureBase target = Session.Creature.Target ?? Session.Creature;
                    if (target.Type.IsPlayer())
                        (target as PlayerCreature).Character.AddExperience(xp);
                    return true;
                }),
                new CommandGroup("debug")
                {
                    new Command("delay", "delay <ms>", args =>
                    {
                        int delay;
                        if (!int.TryParse(args[0], out delay))
                            return Error("Time is not in correct format");

                        WorldManager.PacketDelay = delay;
                        return true;
                    }),
                    new CommandGroup("item")
                    {
                        new Command("create", "debug item create <id> [count]", args =>
                        {
                            int id, count = 1;
                            if (!int.TryParse(args[0], out id))
                                return Error("Item ID is not in correct format");
                            if (args.Length >= 2)
                                if (!int.TryParse(args[1], out count))
                                    return Error("Item count is not in correct format");

                            Item item = DatabaseManager.Items[id].Create(count) as Item;
                            Info("GUID: {0}", item.GUID);
                            return true;
                        }),
                    },
                    new CommandGroup("inventory")
                    {
                        new CommandGroup("bag")
                        {
                            new TargetedCommand("change", "debug inventory bag change {player guid} <index> <bag item guid> <size>", (target, args) =>
                            {
                                ulong bagItemGUID;
                                byte bagIndex, size;
                                if (!byte.TryParse(args[0], out bagIndex))
                                    return Error("Bag index is not in correct format");
                                if (!ulong.TryParse(args[1], out bagItemGUID))
                                    return Error("Bag item GUID is not in correct format");
                                if (!byte.TryParse(args[2], out size))
                                    return Error("Bag size is not in correct format");

                                PlayerCreature pc = ObjectManager.GetCreature(target) as PlayerCreature;
                                if (pc == null)
                                    return Error("Player not found");
                                Item bagItem = DatabaseManager.GameItems[(int)bagItemGUID];

                                pc.Character.Inventory.ChangeBag(bagIndex, bagItem, size);
                                Info("Success");
                                return true;
                            }),
                            new TargetedCommand("set", "debug inventory bag set {player guid} <index> <slot> <item guid>", (target, args) =>
                            {
                                ulong itemGUID;
                                byte bagIndex, slot;
                                if (!byte.TryParse(args[0], out bagIndex))
                                    return Error("Bag index is not in correct format");
                                if (!byte.TryParse(args[1], out slot))
                                    return Error("Bag slot is not in correct format");
                                if (!ulong.TryParse(args[2], out itemGUID))
                                    return Error("Item GUID is not in correct format");

                                PlayerCreature pc = ObjectManager.GetCreature(target) as PlayerCreature;
                                if (pc == null)
                                    return Error("Player not found");
                                Item item = DatabaseManager.GameItems[(int)itemGUID];

                                pc.Character.Inventory.Bags[bagIndex].AddItem(slot, item);
                                Info("Success");
                                return true;
                            }),
                        },
                        new TargetedCommand("add", "debug inventory add {player guid} <item guid>", (target, args) =>
                        {
                            ulong itemGUID;
                            if (!ulong.TryParse(args[0], out itemGUID))
                                return Error("Item GUID is not in correct format");

                            PlayerCreature pc = ObjectManager.GetCreature(target) as PlayerCreature;
                            if (pc == null)
                                return Error("Player not found");
                            Item item = DatabaseManager.GameItems[(int)itemGUID];

                            pc.Character.Inventory.AddItem(item);
                            Info("Success");
                            return true;
                        }),
                    },
                },
            };
        }

        public static bool Execute(string cmd)
        {
            return InitialGroup.Execute(cmd);
        }
        public static bool Execute(PlayerSession session, string cmd)
        {
            Session = session;
            SetOutputHandlers((format, args) =>
            {
                session.SendSystemMessage(format, args);
                return false;
            }, session.SendSystemMessage);
            TargetedCommand.TargetGUID = session.Creature.Target != null ? session.Creature.Target.GUID : session.Creature.GUID;
            bool result = Execute(cmd);
            TargetedCommand.TargetGUID = 0;
            ResetOutputHandlers();
            Session = null;
            return result;
        }

        public static void Info(string format, params object[] args)
        {
            InfoHandler(format, args);
        }
        public static bool Error(string format, params object[] args)
        {
            return ErrorHandler(format, args);
        }

        public static void SetOutputHandlers(CommandOutputErrorHandler errorHandler, CommandOutputInfoHandler infoHandler)
        {
            InfoHandler = infoHandler;
            ErrorHandler = errorHandler;
            Command.InfoHandler = infoHandler;
            Command.ErrorHandler = errorHandler;
            CommandGroup.InfoHandler = infoHandler;
            CommandGroup.ErrorHandler = errorHandler;
        }
        public static void ResetOutputHandlers()
        {
            SetOutputHandlers(OutputErrorHandler, OutputInfoHandler);
        }
        public static void OutputInfoHandler(string format, params object[] args)
        {
            if (args == null || args.Length == 0)
                System.Console.WriteLine(format);
            else
                System.Console.WriteLine(format, args);
        }
        public static bool OutputErrorHandler(string format, params object[] args)
        {
            if (args == null || args.Length == 0)
                System.Console.WriteLine("Error: " + format);
            else
                System.Console.WriteLine("Error: " + string.Format(format, args));
            return false;
        }

        private static bool TryParseEnum<T>(string value, out T result)
        {
            int index;
            if (int.TryParse(value, out index))
            {
                result = (T)Enum.ToObject(typeof(T), index);
                return true;
            }

            string[] names = Enum.GetNames(typeof(T));
            T[] values = (T[])Enum.GetValues(typeof(T));
            int max = values.Length;
            for (int i = 0; i < max; i++)
                if (names[i].ToLowerInvariant() == value.ToLowerInvariant())
                {
                    result = values[i];
                    return true;
                }

            result = default(T);
            return false;
        }
    }
}