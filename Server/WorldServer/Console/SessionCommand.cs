﻿using Arcanos.Utilities.Console;

namespace Arcanos.Server.WorldServer.Console
{
    public class SessionCommand : Command
    {
        public SessionCommand(string name, CommandHandler handler) : base(name, handler) { }
        public SessionCommand(string name, string syntaxHelp, CommandHandler handler) : base(name, syntaxHelp, handler) { }

        public override bool Execute(string args)
        {
            if (Commands.Session == null)
            {
                Commands.OutputErrorHandler("Command can only be used in a client session.");
                return true;
            }

            return base.Execute(args);
        }
    }
}