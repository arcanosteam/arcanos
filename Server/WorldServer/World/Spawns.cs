﻿using Arcanos.Server.WorldServer.Data;
using Arcanos.Server.WorldServer.Objects;
using Arcanos.Shared;
using Microsoft.Xna.Framework;

namespace Arcanos.Server.WorldServer.World.Spawns
{
    public abstract class MapSpawn
    {
        public readonly ulong GUID;
        public int ID;
        public Vector3 Position;
        public Rotation Rotation;

        protected MapSpawn() { }
        protected MapSpawn(ulong guid, int id, Vector3 pos, Rotation rot)
        {
            GUID = guid;
            ID = id;
            Position = pos;
            Rotation = rot;
        }

        public abstract void Spawn(Map map);
    }
    public class MapObjectSpawn : MapSpawn
    {
        public MapObjectSpawn() { }
        public MapObjectSpawn(ulong guid, int id, Vector3 pos, Rotation rot) : base(guid, id, pos, rot) { }

        public override void Spawn(Map map)
        {
            Vector3 pos = Position;

            float groundZ = map.Template.GetHeight(pos.X, pos.Y);
            if (pos.Z < groundZ)
                pos.Z = groundZ;

            map.Spawn(GUID, DatabaseManager.WorldObjects[ID], pos, Rotation);
        }
    }
    public class MapCreatureSpawn : MapSpawn
    {
        public MapCreatureSpawn() { }
        public MapCreatureSpawn(ulong guid, int id, Vector3 pos, Rotation rot) : base(guid, id, pos, rot) { }

        public override void Spawn(Map map)
        {
            Vector3 pos = Position;
            CreatureTemplate template = DatabaseManager.Creatures[ID];

            // Drop to ground if creature can't fly
            if (!template.CanFly())
                pos.Z = map.Template.GetHeight(pos.X, pos.Y);

            map.Spawn(GUID, template, pos, Rotation);
        }
    }
}