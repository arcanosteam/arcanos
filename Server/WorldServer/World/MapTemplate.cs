﻿using System.Collections.Generic;
using Arcanos.Server.WorldServer.Data;
using Arcanos.Shared.Objects;
using Arcanos.Shared.World;
using Arcanos.Server.WorldServer.World.Spawns;

namespace Arcanos.Server.WorldServer.World
{
    public class MapTemplate : MapTemplateBase
    {
        public readonly List<MapSpawn> Spawns = new List<MapSpawn>();
        public bool Instanced;
        public Environment Environment;

        public MapTemplate() { }
        public MapTemplate(float sizeX, float sizeY, float sizeZ, int hmapDimX, int hmapDimY, int patchSize, Environment env) : this(sizeX, sizeY, sizeZ, DEFAULT_GRID_CELL_SIZE, hmapDimX, hmapDimY, patchSize, env) { }
        public MapTemplate(float sizeX, float sizeY, float sizeZ, float cellSize, int hmapDimX, int hmapDimY, int patchSize, Environment env) : base(sizeX, sizeY, sizeZ, cellSize, hmapDimX, hmapDimY, patchSize)
        {
            Environment = env;
        }

        public override void Apply(MapBase map)
        {
            Map sMap = (Map)map;
            foreach (MapSpawn spawn in Spawns)
                spawn.Spawn(sMap);
            foreach (StaticModelObjectBase smo in Statics)
                map.Add(smo);
        }

        protected override int GetInitialID()
        {
            return DatabaseManager.Maps.GetID(this);
        }
    }
}