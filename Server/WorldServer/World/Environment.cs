using System;
using Arcanos.Server.WorldServer.Data;
using Arcanos.Shared;
using Arcanos.Shared.Data;
using Arcanos.Shared.Net;
using Arcanos.Shared.Timing;
using Arcanos.Shared.World;

namespace Arcanos.Server.WorldServer.World
{
    [SerializationSettings(Method = SerializationMethod.RefByID, ReferenceResolverType = typeof(DatabaseManager), ReferenceResolverMethod = "GetEnvironment")]
    public class Environment : EnvironmentBase
    {
        [SerializationIgnore]
        public int CurrentID;

        public FloatBounds ChangeInterval;
        public EnvironmentChangeEntry[] ChangesTo = new EnvironmentChangeEntry[0];

        [SerializationIgnore]
        public float ChangeIntervalLeft;

        public Environment() { }
        public Environment(Environment source)
        {
            Clone(source);
        }

        public override void Update(MapBase map)
        {
            if (ChangeInterval.Empty)
                return;

            ChangeIntervalLeft -= Time.PerSecond;
            if (ChangeIntervalLeft <= 0)
            {
                Environment to = null;

                float totalChance = 0;
                for (int i = 0; i < ChangesTo.Length; i++)
                    totalChance += ChangesTo[i].Chance;

                float roll = R.Float(totalChance);
                totalChance = 0;
                for (int i = 0; i < ChangesTo.Length; i++)
                {
                    totalChance += ChangesTo[i].Chance;
                    if (roll <= totalChance)
                    {
                        to = DatabaseManager.Environments[ChangesTo[i].EnvironmentID];
                        break;
                    }
                }
                if (to != null)
                    ChangeTo(map as Map, to);
            }
        }

        protected override void Clone(EnvironmentBase baseSource)
        {
            base.Clone(baseSource);
            Environment source = baseSource as Environment;

            CurrentID = source.ID;

            ChangeInterval = source.ChangeInterval;
            ChangesTo = new EnvironmentChangeEntry[source.ChangesTo.Length];
            Array.Copy(source.ChangesTo, ChangesTo, ChangesTo.Length);

            ChangeIntervalLeft = ChangeInterval.Random;
        }

        public void ChangeTo(Map map, Environment env)
        {
            Clone(env);
            map.BroadcastPacket(new SChangeEnvironmentPacket { ID = env.ID });
        }

        protected override int GetInitialID()
        {
            return DatabaseManager.Environments.GetID(this);
        }
    }
    public struct EnvironmentChangeEntry
    {
        public readonly float Chance;
        public readonly int EnvironmentID;

        public EnvironmentChangeEntry(float chance, int envID)
        {
            Chance = chance;
            EnvironmentID = envID;
        }
    }
}