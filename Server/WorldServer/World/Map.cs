﻿using System.Collections.Generic;
using Arcanos.Server.WorldServer.Spells;
using Arcanos.Shared;
using Arcanos.Shared.Grids;
using Arcanos.Shared.Net;
using Arcanos.Shared.Objects;
using Arcanos.Shared.World;
using Arcanos.Server.WorldServer.Objects;
using Microsoft.Xna.Framework;

namespace Arcanos.Server.WorldServer.World
{
    public class Map : MapBase
    {
        public const float DEFAULT_VISIBILITY_DISTANCE = 100;

        public readonly List<PlayerCreature> Players = new List<PlayerCreature>();
        public readonly List<AreaEffect> AreaEffects = new List<AreaEffect>();
        private bool updatingAreaEffects;
        private readonly Queue<AreaEffect> areaEffectsToAdd = new Queue<AreaEffect>();
        private readonly Queue<AreaEffect> areaEffectsToRemove = new Queue<AreaEffect>();
        public float VisibilityDistance = DEFAULT_VISIBILITY_DISTANCE;

        public Map(ulong guid, MapTemplate template) : base(guid, template)
        {
            Environment = new Environment(template.Environment);
        }

        public override void Update()
        {
            base.Update();
            foreach (PlayerCreature pc in Players)
            {
                GridBounds bounds = Grid.GetGridBounds(pc.Position, VisibilityDistance);
                for (int x = bounds.MinX; x <= bounds.MaxX; ++x)
                    for (int y = bounds.MinY; y <= bounds.MaxY; ++y)
                        Grid.Cells[x][y].MarkAsActive();
            }
            Grid.UpdateActiveCells();
            updatingAreaEffects = true;
            foreach (AreaEffect areaEffect in AreaEffects)
                areaEffect.Update();
            while (areaEffectsToAdd.Count != 0)
                AreaEffects.Add(areaEffectsToAdd.Dequeue());
            while (areaEffectsToRemove.Count != 0)
                AreaEffects.Remove(areaEffectsToRemove.Dequeue());
            updatingAreaEffects = false;
        }

        public override void Add(WorldObjectBase wo)
        {
            if (wo.Type.IsPlayer())
                Players.Add(wo as PlayerCreature);
            base.Add(wo);
        }
        public override void Remove(WorldObjectBase wo)
        {
            if (wo.Type.IsPlayer())
                Players.Remove(wo as PlayerCreature);
            base.Remove(wo);
        }
        public void Add(AreaEffect areaEffect)
        {
            if (updatingAreaEffects)
                areaEffectsToAdd.Enqueue(areaEffect);
            else
                AreaEffects.Add(areaEffect);
        }
        public void Remove(AreaEffect areaEffect)
        {
            if (updatingAreaEffects)
                areaEffectsToRemove.Enqueue(areaEffect);
            else
                AreaEffects.Remove(areaEffect);
        }

        public override void MoveObject(WorldObjectBase wo)
        {
            base.MoveObject(wo);
            if (wo.Type.IsCreature())
                foreach (AreaEffect areaEffect in AreaEffects)
                    if (IsAffectedByAreaEffect(wo as CreatureBase, areaEffect) && !areaEffect.AffectedTargets.Contains(wo as CreatureBase))
                        areaEffect.CreatureEnteredArea(wo as CreatureBase);
        }

        public WorldObject Spawn(ulong guid, WorldObjectTemplate template, Vector3 pos)
        {
            return Spawn(guid, template, pos, Rotation.Zero);
        }
        public WorldObject Spawn(ulong guid, WorldObjectTemplate template, Vector3 pos, Rotation rot)
        {
            WorldObject wo = new WorldObject(guid, template, pos, rot);
            Add(wo);
            return wo;
        }
        public Creature Spawn(ulong guid, CreatureTemplate template, Vector3 pos)
        {
            return Spawn(guid, template, pos, Rotation.Zero);
        }
        public Creature Spawn(ulong guid, CreatureTemplate template, Vector3 pos, Rotation rot)
        {
            Creature creature = new Creature(guid, template, pos, rot);
            Add(creature);
            return creature;
        }

        public override IEnumerable<WorldObjectBase> GetVisibleObjects(Vector3 pos)
        {
            return GetNearbyObjects(pos, VisibilityDistance);
        }

        public IEnumerable<AreaEffect> GetAffectingAreaEffects(CreatureBase creature)
        {
            foreach (AreaEffect areaEffect in AreaEffects)
                if (IsAffectedByAreaEffect(creature, areaEffect))
                    yield return areaEffect;
        }
        public bool IsAffectedByAreaEffect(CreatureBase creature, AreaEffect areaEffect)
        {
            return creature.DistanceTo(areaEffect.Position) <= areaEffect.Spell.Radius;
        }
        public bool IsAffectedByAreaEffects(CreatureBase creature)
        {
            foreach (AreaEffect areaEffect in AreaEffects)
                if (IsAffectedByAreaEffect(creature, areaEffect))
                    return true;
            return false;
        }
        public bool IsAffectedByAreaEffectsExcept(CreatureBase creature, AreaEffect exceptAreaEffect)
        {
            foreach (AreaEffect areaEffect in AreaEffects)
                if (areaEffect != exceptAreaEffect && IsAffectedByAreaEffect(creature, areaEffect))
                    return true;
            return false;
        }
        public bool IsAffectedByAreaEffectsOfSpell(CreatureBase creature, SpellTemplate spell)
        {
            foreach (AreaEffect areaEffect in AreaEffects)
                if (areaEffect.Spell == spell && IsAffectedByAreaEffect(creature, areaEffect))
                    return true;
            return false;
        }
        public bool IsAffectedByAreaEffectsOfSpellExcept(CreatureBase creature, SpellTemplate spell, AreaEffect exceptAreaEffect)
        {
            foreach (AreaEffect areaEffect in AreaEffects)
                if (areaEffect != exceptAreaEffect && areaEffect.Spell == spell && IsAffectedByAreaEffect(creature, areaEffect))
                    return true;
            return false;
        }

        public void BroadcastPacket(IWorldPacket packet)
        {
            foreach (PlayerCreature pc in Players)
                pc.SendPacketToSelf(packet);
        }
        public void BroadcastPacket(IWorldPacket packet, byte[] rawData)
        {
            foreach (PlayerCreature pc in Players)
                pc.SendPacketToSelf(packet, rawData);
        }
    }
}