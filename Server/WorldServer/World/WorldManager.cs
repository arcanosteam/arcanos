﻿//#define LIMIT

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using Arcanos.Server.WorldServer.Chat;
using Arcanos.Server.WorldServer.Data;
using Arcanos.Server.WorldServer.Net;
using Arcanos.Server.WorldServer.Players;
using Arcanos.Shared;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Timing;
using Arcanos.Utilities;
using Microsoft.Xna.Framework;

namespace Arcanos.Server.WorldServer.World
{
    public static class WorldManager
    {
        public static readonly Dictionary<ulong, Map> PublicMaps = new Dictionary<ulong, Map>();
        public static readonly Dictionary<ulong, Map> InstanceMaps = new Dictionary<ulong, Map>();

        public static Thread UpdateThread;
        public static bool HaltUpdate;
        public static int PacketDelay;

        public static void Update()
        {
            SessionManager.ProcessPackets();

            foreach (Map map in PublicMaps.Values)
                map.Update();
            foreach (Map map in InstanceMaps.Values)
                map.Update();
        }
        public static void Create()
        {
            ChatManager.CreateGlobalChannels();
        }

        public static Map GetMapByID(int id)
        {
            foreach (KeyValuePair<ulong, Map> map in PublicMaps)
                if (map.Value.Template.ID == id)
                    return map.Value;
            foreach (KeyValuePair<ulong, Map> map in InstanceMaps)
                if (map.Value.Template.ID == id)
                    return map.Value;
            return null;
        }
        public static IEnumerable<Map> GetMapsByID(int id)
        {
            foreach (KeyValuePair<ulong, Map> map in PublicMaps)
                if (map.Value.Template.ID == id)
                    yield return map.Value;
            foreach (KeyValuePair<ulong, Map> map in InstanceMaps)
                if (map.Value.Template.ID == id)
                    yield return map.Value;
        }
        public static Map GetPublicMap(int id)
        {
            foreach (KeyValuePair<ulong, Map> map in PublicMaps)
                if (map.Value.Template.ID == id)
                    return map.Value;
            return null;
        }
        public static Map GetMapFor(Character character, int id)
        {
            Map map;
            if (!DatabaseManager.Maps[id].Instanced)
                return GetPublicMap(id);

            foreach (ulong guid in character.BoundMapsGUID)
                if (InstanceMaps.TryGetValue(guid, out map))
                    if (map.Template.ID == id)
                        return map;
            return null;
        }
        public static Map GetOrCreateMapFor(Character character, int id)
        {
            Map map = GetMapFor(character, id);
            if (map != null)
                return map;

            // TODO: Create instanced map for character

            return map;
        }

        public static Player GetOnlinePlayerByName(string name)
        {
            foreach (PlayerSession session in SessionManager.ConnectedSessions)
                if (session.Player != null && string.Compare(session.Player.Name, name, StringComparison.CurrentCultureIgnoreCase) == 0)
                    return session.Player;
            return null;
        }
        public static Character GetOnlineCharacterByName(string name)
        {
            foreach (PlayerSession session in SessionManager.ConnectedSessions)
                if (session.Player != null && session.Player.CurrentCharacter != null && string.Compare(session.Player.CurrentCharacter.Name, name, StringComparison.CurrentCultureIgnoreCase) == 0)
                    return session.Player.CurrentCharacter;
            return null;
        }

        public static ulong InvalidLocationMapGUID = 1;
        public static Vector3 InvalidLocationPosition = Vector3.Zero;
        public static Rotation InvalidLocationRotation = Rotation.Zero;
        public static void GetSpawnForInvalidLocation(out Map map, out Vector3 position, out Rotation rotation)
        {
            map = ObjectManager.GetMap(InvalidLocationMapGUID) as Map;
            position = InvalidLocationPosition;
            rotation = InvalidLocationRotation;
        }
        public static void SetSpawnForInvalidLocation(ulong mapGUID, Vector3 position, Rotation rotation)
        {
            InvalidLocationMapGUID = mapGUID;
            InvalidLocationPosition = position;
            InvalidLocationRotation = rotation;
        }

        private static Stopwatch timingStopwatch;
        private static bool halted;
#if LIMIT
        private static int updatesCount;
        private static float averageTime;
        private static ulong averageSamples;
#endif
        public static void StartUpdateThread()
        {
            Debug.Assert(UpdateThread == null);

            Time.PerSecond = 0.00001f;
            float secondLeft = 1;
            UpdateThread = new Thread(() =>
            {
                timingStopwatch = new Stopwatch();
                while (!HaltUpdate)
                {
                    if (timingStopwatch.IsRunning)
                    {
                        timingStopwatch.Stop();
                        Time.PerSecond = (float)timingStopwatch.Elapsed.TotalSeconds;
                        timingStopwatch.Reset();
                    }
                    timingStopwatch.Start();

#if LIMIT
                    const int UPS_LIMIT = 1000000000;
                    const float UPS_LIMIT_SECONDS = 1f / UPS_LIMIT;
                    bool slowedDown = false;
                    if (Time.PerSecond < UPS_LIMIT_SECONDS)
                    {
                        Thread.Sleep((int)(1000 * (UPS_LIMIT_SECONDS - Time.PerSecond)));
                        Time.PerSecond = UPS_LIMIT_SECONDS;
                        slowedDown = true;
                    }

                    bool lagSpike = false;
                    if (!slowedDown)
                        if (averageTime == 0)
                            averageTime = Time.PerSecond;
                        else
                        {
                            float averageTimeDuration = averageTime / averageSamples;
                            if ((Time.PerSecond - averageTimeDuration) / averageTimeDuration > 50)
                                lagSpike = true;
                            else
                            {
                                averageTime += Time.PerSecond;
                                ++averageSamples;
                            }
                        }

                    if (updatesCount % UPS_LIMIT == 0 || lagSpike)
                        Logger.Debug(LogCategory.World, "World updated in {0:0.00} ms (avg: {1:0.0000} ms) {2}", Time.PerSecond * 1000, (averageTime / averageSamples) * 1000, lagSpike ? "(lag spike!)" : slowedDown ? "(forced)" : "");
#endif

                    secondLeft -= Time.PerSecond;
                    if (secondLeft <= 0)
                    {
                        secondLeft += 1;
                        Logger.Error(LogCategory.World, "Second!");
                    }

                    Time.Timestamp = DateTime.UtcNow.Ticks / 10000;

                    Update();
                    
#if LIMIT
                    updatesCount++;
#endif
                }
                halted = true;
            }) { Name = "World Update" };
            UpdateThread.Start();
        }
        public static void StopUpdateThread()
        {
            HaltUpdate = true;
            while (!halted)
                Thread.Sleep(5);
        }
    }
}