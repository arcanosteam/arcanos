﻿using System;
using System.Collections.Generic;
using Arcanos.Server.WorldServer.Data;
using Arcanos.Server.WorldServer.Items;
using Arcanos.Server.WorldServer.Spells;
using Arcanos.Shared;
using Arcanos.Shared.Data;
using Arcanos.Shared.Graphics;
using Arcanos.Shared.Items;

namespace Arcanos.Server.WorldServer.Players
{
    public class RaceInfo : Template, IManualSerialization, IManualDeserialization
    {
        public override EditorEntry EditorEntry
        {
            get { return new EditorEntry(ID, Enum.GetNames(typeof(PlayerRace))[ID]); }
        }

        [SerializationSettings(Method = SerializationMethod.Manual)]
        public Model[] Models;
        public SpellTemplate[] Spells;
        public ItemDefinition[] Items;
        public Dictionary<EquipSlot, ItemTemplate> Equipment = new Dictionary<EquipSlot, ItemTemplate>();

        public byte[] Serialize(string member)
        {
            switch (member)
            {
                case "Models":
                {
                    byte[] buffer = new byte[sizeof(int) + Models.Length * sizeof(int)];
                    BitConverter.GetBytes(Models.Length).CopyTo(buffer, 0);
                    for (int i = 0; i < Models.Length; i++)
                        BitConverter.GetBytes(Models[i].ID).CopyTo(buffer, sizeof(int) + i * sizeof(int));
                    return buffer;
                }
            }
            return new byte[0];
        }
        public void Deserialize(string member, byte[] data)
        {
            switch (member)
            {
                case "Models":
                    Models = new Model[BitConverter.ToInt32(data, 0)];
                    for (int i = 0; i < Models.Length; i++)
                        Models[i] = DatabaseManager.Models[BitConverter.ToInt32(data, sizeof(int) + i * sizeof(int))];
                    break;
            }
        }

        protected override int GetInitialID()
        {
            return DatabaseManager.RaceInfos.GetID(this);
        }
    }
}