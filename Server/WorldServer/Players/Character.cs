﻿using System.Collections.Generic;
using Arcanos.Server.WorldServer.Chat;
using Arcanos.Server.WorldServer.Data;
using Arcanos.Server.WorldServer.Items;
using Arcanos.Server.WorldServer.Objects;
using Arcanos.Server.WorldServer.Quests;
using Arcanos.Server.WorldServer.Spells;
using Arcanos.Shared;
using Arcanos.Shared.Chat;
using Arcanos.Shared.Data;
using Arcanos.Shared.Items;
using Arcanos.Shared.Net;
using Arcanos.Shared.Objects;
using Arcanos.Shared.Players;
using Arcanos.Shared.Spells;

namespace Arcanos.Server.WorldServer.Players
{
    public class CharacterSpellBookData
    {
        public struct CooldownInstance
        {
            public int CategoryID;
            public float Left;
            public float Total;
        }
        public readonly List<SpellTemplate> Spells = new List<SpellTemplate>();
        public readonly List<Aura> Auras = new List<Aura>();
        public readonly List<CooldownInstance> Cooldowns = new List<CooldownInstance>();
    }
    [SerializationSettings(Method = SerializationMethod.RefByID, ReferenceResolverType = typeof(DatabaseManager), ReferenceResolverMethod = "GetCharacter")]
    public class Character : CharacterBase, IPostDeserialization
    {
        [SerializationIgnore]
        public new Player Player
        {
            get { return (Player)base.Player; }
        }
        [SerializationIgnore]
        public new PlayerCreature Creature
        {
            get { return (PlayerCreature)base.Creature; }
            set { base.Creature = value; }
        }

        public int Health;
        public int[] Power = new int[(byte)PowerType.Max];
        public int XP;
        public int Money;
        public readonly Stats Stats;
        public readonly Inventory Inventory;
        public readonly Bag Equipment;
        public readonly QuestLog QuestLog;
        public CharacterSpellBookData SpellBook = new CharacterSpellBookData();
        public int GroupID;
        [SerializationIgnore]
        public Group Group
        {
            get { return GroupID == 0 ? null : DatabaseManager.Groups[GroupID]; }
            set { GroupID = value.ID; }
        }
        public int PendingGroupInviteToCharacter;
        public bool JustCreated;
        
        public readonly List<ulong> BoundMapsGUID = new List<ulong>();

        internal Character()
        {
            Inventory = new Inventory(this);
            Equipment = new Bag(Inventory, null, (byte)EquipSlot.Max);
            QuestLog = new QuestLog(this);
            Stats = new Stats(this);
        }
        public Character(Player player, string name, PlayerRace playerRace, PlayerClass playerClass, byte modelIndex, bool dryRun) : base(player, name, playerRace, playerClass, modelIndex)
        {
            if (dryRun)
                return;

            Inventory = new Inventory(this);
            Equipment = new Bag(Inventory, null, (byte)EquipSlot.Max);
            QuestLog = new QuestLog(this);
            Stats = new Stats(this);
            Inventory.Bags[0] = new Bag(Inventory, null, 20);
            Formulas.FillBaseStatsForLevel(Stats, Race, Class, Level);
            Health = Formulas.GetHealthForPlayer(this);
            for (byte i = 0; i < (byte)PowerType.Max; i++)
                Power[i] = Formulas.GetPowerForPlayer(this, (PowerType)i);
            JustCreated = true;
        }

        public void AddMoney(int money)
        {
            SetMoney(Money + money);
        }
        public void TakeMoney(int money)
        {
            SetMoney(Money - money);
        }
        public void SetMoney(int money)
        {
            Money = money < 0 ? 0 : money;
            Player.Session.SendPacket(new SMoneyChangedPacket { Count = Money });
        }
        public void AddExperience(int xp)
        {
            if (xp <= 0)
                return;
            SetExperience(XP + xp);
        }
        public void SetExperience(int xp)
        {
            int xpToLevel = Formulas.CalculateExperienceToNextLevel(Level);
            XP = xp;
            while (XP >= xpToLevel)
            {
                XP -= xpToLevel;
                GainLevel();
                xpToLevel = Formulas.CalculateExperienceToNextLevel(Level);
            }
            Player.Session.SendPacket(new SExperienceChangedPacket { XP = XP, XPToLevel = xpToLevel });
        }
        public void GainLevel()
        {
            Creature.Level = ++Level;
            Creature.SendPacketToVisible(new SLevelUpPacket { GUID = Creature.GUID, Level = Level });

            Formulas.FillBaseStatsForLevel(Stats, Race, Class, Level);
            for (byte i = 0; i < (byte)Stat.Max; ++i)
                Stats.SetBase((Stat)i, Stats.BaseValues[i]);

            Creature.UpdatePlayerStats(false);
        }

        public ItemEquipResult CanEquip(Item item, bool ignoreUsedSlots, out EquipSlot slot)
        {
            for (slot = 0; slot < EquipSlot.Max; ++slot)
                if (Equipment.Items[(byte)slot] == item)
                    return ItemEquipResult.AlreadyEquipped;
            for (slot = 0; slot < EquipSlot.Max; ++slot)
                if ((ignoreUsedSlots || Equipment.Items[(byte)slot] == null) && ItemBase.CanEquipInSlot(item, slot))
                    return CanEquip(item, slot);
            return ItemEquipResult.NoCompatibleSlots;
        }
        public ItemEquipResult CanEquip(Item item, EquipSlot slot)
        {
            return CanEquip(item.Template, slot);
        }
        public ItemEquipResult CanEquip(ItemTemplate item, EquipSlot slot)
        {
            if (!ItemBase.CanEquipInSlot(item, slot))
                return ItemEquipResult.IncompatibleSlot;
            if (item.RequiredLevel.Min != 0 && Level < item.RequiredLevel.Min)
                return ItemEquipResult.LevelTooLow;
            if (item.RequiredLevel.Max != 0 && Level > item.RequiredLevel.Max)
                return ItemEquipResult.LevelTooHigh;

            return ItemEquipResult.Ok;
        }
        public ItemUnequipResult CanUnequip(Item item)
        {
            return CanUnequip(item.Template);
        }
        public ItemUnequipResult CanUnequip(ItemTemplate item)
        {
            return ItemUnequipResult.Ok;
        }
        public ItemEquipResult Equip(Item item, EquipSlot slot)
        {
            if (!JustCreated && item.IsEquipped)
                return ItemEquipResult.AlreadyEquipped;

            ItemEquipResult result = slot == EquipSlot.Max ? CanEquip(item, item.Template.Slot != ItemSlot.Accessory && item.Template.Slot != ItemSlot.OneHand, out slot) : CanEquip(item, slot);
            if (result != ItemEquipResult.Ok)
                return result;

            sbyte originalBagIndex = Inventory.IndexOf(item.Bag);
            sbyte originalSlot = JustCreated ? (sbyte)-1 : item.Bag.SlotOf(item);
            if (!JustCreated && (originalBagIndex == -1 || originalSlot == -1))
                return ItemEquipResult.NotOwner;

            if (item.Template.Slot == ItemSlot.TwoHand && Equipment.Items[(byte)EquipSlot.OffHand] != null)
            {
                byte bagIndex, bagSlot;
                if (!Inventory.FindEmptySlot(out bagIndex, out bagSlot))
                    return ItemEquipResult.UsingTwoHanded;
                if (Unequip(Equipment.Items[(byte)EquipSlot.OffHand] as Item, bagIndex, bagSlot) != ItemUnequipResult.Ok)
                    return ItemEquipResult.UsingTwoHanded;
            }

            if (!JustCreated)
                item.Bag.RemoveItem((byte)originalSlot);

            bool removeFromMainHand = (item.Template.Slot == ItemSlot.OffHand || item.Template.Slot == ItemSlot.OneHand) && Equipment.Items[(byte)EquipSlot.MainHand] != null && Equipment.Items[(byte)EquipSlot.MainHand].Template.Slot == ItemSlot.TwoHand;

            Item equippedItem = Equipment.Items[removeFromMainHand ? (byte)EquipSlot.MainHand : (byte)slot] as Item;
            if (equippedItem != null)
            {
                equippedItem.Unequip();
                Equipment.RemoveItem(equippedItem);
                Inventory.Bags[originalBagIndex].AddItem((byte)originalSlot, equippedItem);
            }
            Equipment.AddItem((byte)slot, item);
            item.Equip();

            return ItemEquipResult.Ok;
        }
        public ItemUnequipResult Unequip(Item item, byte bagIndex, byte slot)
        {
            if (!item.IsEquipped)
                return ItemUnequipResult.NotEquipped;

            if (Inventory.Bags[bagIndex].Items[slot] != null)
                return ItemUnequipResult.SlotOccupied;

            if ((Inventory.Bags[bagIndex] as Bag).CanStoreItem(item, slot) != ItemStoreResult.Ok)
                return ItemUnequipResult.WrongBagType;

            ItemUnequipResult result = CanUnequip(item);
            if (result != ItemUnequipResult.Ok)
                return result;

            item.Unequip();
            Equipment.RemoveItem(item);
            Inventory.Bags[bagIndex].AddItem(slot, item);

            return ItemUnequipResult.Ok;
        }

        public void LeaveGroup()
        {
            if (Group != null)
                Group.RemoveMember(this);
        }

        public void JoinGlobalChatChannels()
        {
            ChatManager.Join(Player, ChatManager.CHANNEL_LOCAL_SAY);
            ChatManager.Join(Player, ChatManager.CHANNEL_LOCAL_YELL);
            ChatManager.Join(Player, ChatManager.CHANNEL_LOCAL_EMOTE);
            ChatManager.Join(Player, ChatManager.CHANNEL_MAP);
        }
        public void SendInitialData()
        {
            Stats.SendInitialData();
            Creature.SendPacketToVisible(new SLevelUpPacket { GUID = Creature.GUID, Level = Level });
            Creature.SendPacketToVisible(new SExperienceChangedPacket { XP = XP, XPToLevel = Formulas.CalculateExperienceToNextLevel(Level) });
            Creature.SendPacketToVisible(new SMoneyChangedPacket { Count = Money, Initial = true });
            for (byte index = 0; index < InventoryBase.BAG_LIMIT; ++index)
                if (Inventory.Bags[index] != null)
                {
                    Item bagItem = Inventory.Bags[index].BagItem as Item;
                    byte size = Inventory.Bags[index].Size;
                    Creature.SendPacketToSelf(new SBagChangedPacket { BagIndex = index, BagItemGUID = bagItem == null ? 0 : bagItem.GUID, BagSize = size });
                }
            for (byte index = 0; index < InventoryBase.BAG_LIMIT; ++index)
            {
                Bag bag = Inventory.Bags[index] as Bag;
                if (bag != null)
                    for (byte i = 0; i < bag.Size; ++i)
                    {
                        Item item = bag.Items[i] as Item;
                        if (item != null)
                            Creature.SendPacketToSelf(new SBagSlotUpdatedPacket { BagIndex = (sbyte)index, BagSlot = i, ItemGUID = item.GUID });
                    }
            }
            for (byte i = 0; i < Equipment.Size; ++i)
            {
                Item item = Equipment.Items[i] as Item;
                if (item != null)
                    Creature.SendPacketToSelf(new SBagSlotUpdatedPacket { BagIndex = -1, BagSlot = i, ItemGUID = item.GUID });
            }
            QuestLog.SendInitialData();
            foreach (SpellTemplate spell in Creature.SpellBook.Spells)
                Creature.SendPacketToSelf(new SKnowsSpellPacket { SpellID = spell.ID });
            Creature.SendPacketToSelf(new SExperienceChangedPacket { XP = XP, XPToLevel = Formulas.CalculateExperienceToNextLevel(Level) });
            foreach (ChatChannel channel in ChatManager.GetJoinedChannels(Player))
            {
                Creature.SendPacketToSelf(new SChatChannelJoinResultPacket
                {
                    Result = ChatChannelJoinResult.Ok,
                    ChannelID = channel.ID,
                    ChannelNameID = channel.Name == null ? 0 : channel.Name.ID,
                    ChannelAliasID = channel.Name == null ? 0 : channel.Alias.ID,
                    CustomChannelName = channel.GetNameForLocale(Player.Session.Locale),
                    ChannelColor = channel.Color,
                });
            }
            Creature.SendInitialDataAbout(Creature as WorldObjectBase);
            Creature.SendInitialDataAbout(Creature as CreatureBase);
            Creature.SendInitialDataAbout(Creature);
        }

        public void Save()
        {
            if (Creature == null)
                return;
            LocationMapGUID = Creature.Map.GUID;
            LocationPosition = Creature.Position;
            LocationRotation = Creature.Rotation;

            Health = Creature.CurrentHealth;
            for (byte i = 0; i < (byte)PowerType.Max; i++)
                Power[i] = Creature.CurrentPower[i];

            SpellBook = new CharacterSpellBookData();
            foreach (SpellTemplate spell in Creature.SpellBook.Spells)
                SpellBook.Spells.Add(spell);
            foreach (Aura aura in Creature.SpellBook.GetAuras())
                SpellBook.Auras.Add(aura);
            foreach (KeyValuePair<int, CooldownInstance> cooldown in Creature.SpellBook.Cooldowns)
                SpellBook.Cooldowns.Add(new CharacterSpellBookData.CooldownInstance { CategoryID = cooldown.Key, Left = cooldown.Value.Left, Total = cooldown.Value.Total });

            DatabaseManager.Characters.Save(ID);
        }
        public void ProcessCharacterSelect() { }
        public void ProcessEnterMap()
        {
            if (JustCreated)
            {
                foreach (ItemDefinition item in GetStartingItems(Race, Class))
                    Inventory.AddItem(item.Template, item.Count);
                foreach (KeyValuePair<EquipSlot, ItemTemplate> pair in GetStartingEquipment(Race, Class))
                    Equip(new Item(0, pair.Value, 1), pair.Key);

                JustCreated = false;
                Save();
            }
        }
        public void ProcessLogout()
        {
            ChatManager.Leave(Player);
            Save();
            if (Creature != null)
            {
                if (Creature.Map != null)
                    Creature.Map.Remove(Creature);
                ObjectManager.Unregister(Creature.GUID);
                Creature = null;
            }
            DatabaseManager.Players.Save(Player.ID);
        }

        protected override int GetInitialID()
        {
            return DatabaseManager.Characters.GetID(this);
        }

        public void MemberDeserialized(string member, object value) { }
        public void Deserialized(object parent)
        {
            Stats.Owner = this;
            QuestLog.Owner = this;
        }

        public static IEnumerable<ItemDefinition> GetStartingItems(PlayerRace playerRace, PlayerClass playerClass)
        {
            foreach (ItemDefinition item in DatabaseManager.ClassInfos[(byte)playerClass].Items)
                yield return item;
            foreach (ItemDefinition item in DatabaseManager.RaceInfos[(byte)playerRace].Items)
                yield return item;
        }
        public static IEnumerable<KeyValuePair<EquipSlot, ItemTemplate>> GetStartingEquipment(PlayerRace playerRace, PlayerClass playerClass)
        {
            foreach (KeyValuePair<EquipSlot, ItemTemplate> pair in DatabaseManager.ClassInfos[(byte)playerClass].Equipment)
                yield return pair;
            foreach (KeyValuePair<EquipSlot, ItemTemplate> pair in DatabaseManager.RaceInfos[(byte)playerRace].Equipment)
                yield return pair;
        }
    }
}