﻿using System.Collections.Generic;
using Arcanos.Server.WorldServer.Data;
using Arcanos.Server.WorldServer.Net;
using Arcanos.Shared.Data;
using Arcanos.Shared.Players;

namespace Arcanos.Server.WorldServer.Players
{
    public class Player : PlayerBase, IPostDeserialization
    {
        [SerializationIgnore]
        public new Character CurrentCharacter
        {
            get { return (Character)base.CurrentCharacter; }
            set { base.CurrentCharacter = value; }
        }
        [SerializationIgnore]
        public PlayerSession Session;

        [SerializeAs(typeof(Character))]
        public override List<CharacterBase> Characters { get; set; }

        protected Player() { }
        public Player(string name) : base(name) { }

        protected override int GetInitialID()
        {
            return DatabaseManager.Players.GetID(this);
        }

        public void MemberDeserialized(string member, object value)
        {
        }
        public void Deserialized(object parent)
        {
            foreach (CharacterBase character in Characters)
                character.Player = this;
        }
    }
}