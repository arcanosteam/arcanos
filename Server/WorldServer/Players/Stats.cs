﻿using Arcanos.Server.WorldServer.Objects;
using Arcanos.Shared;
using Arcanos.Shared.Data;
using Arcanos.Shared.Net;
using Arcanos.Shared.Players;

namespace Arcanos.Server.WorldServer.Players
{
    public class Stats : StatsBase
    {
        [SerializationIgnore]
        public new Character Owner
        {
            get { return base.Owner as Character; }
            set { base.Owner = value; }
        }
        public PlayerCreature Creature
        {
            get { return base.Owner.Creature as PlayerCreature; }
        }

        public Stats(Character owner) : base(owner) { }

        public override void SetBase(Stat stat, int value)
        {
            base.SetBase(stat, value);
            Creature.SendPacketToSelf(new SBaseStatUpdatePacket { Stat = stat, Value = BaseValues[(byte)stat] });
        }
        public override void SetModified(Stat stat, int value)
        {
            base.SetModified(stat, value);
            Creature.SendPacketToSelf(new SModifiedStatUpdatePacket { Stat = stat, Value = ModifiedValues[(byte)stat] });
            ProcessModification(stat, Modifiers[(byte)stat]);
        }
        public override void Modify(Stat stat, int mod)
        {
            base.Modify(stat, mod);
            Creature.SendPacketToSelf(new SModifiedStatUpdatePacket { Stat = stat, Value = ModifiedValues[(byte)stat] });
            ProcessModification(stat, mod);
        }

        private void ProcessModification(Stat stat, int mod)
        {
            switch (stat)
            {
                case Stat.MeleePower:
                    Creature.UpdateAttackPowerStat();
                    break;
                case Stat.Swiftness:
                    Creature.UpdatePowerStat(PowerType.Energy);
                    Creature.UpdateAttackIntervalStat();
                    break;
                case Stat.Stamina:
                    Creature.UpdateHealthStat();
                    break;
                case Stat.Intellect:
                    Creature.UpdatePowerStat(PowerType.Mana);
                    break;
            }
        }

        public void SendInitialData()
        {
            Creature.SendPacketToSelf(new SStatsDataPacket { BaseValues = BaseValues, ModifiedValues = ModifiedValues });
        }
    }
}