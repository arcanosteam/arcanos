﻿using System;
using System.Collections.Generic;
using Arcanos.Server.WorldServer.Data;
using Arcanos.Server.WorldServer.Items;
using Arcanos.Server.WorldServer.Spells;
using Arcanos.Shared;
using Arcanos.Shared.Items;

namespace Arcanos.Server.WorldServer.Players
{
    public class ClassInfo : Template
    {
        public override EditorEntry EditorEntry
        {
            get { return new EditorEntry(ID, Enum.GetNames(typeof(PlayerClass))[ID]); }
        }

        public SpellTemplate[] Spells;
        public ItemDefinition[] Items;
        public Dictionary<EquipSlot, ItemTemplate> Equipment = new Dictionary<EquipSlot, ItemTemplate>();

        protected override int GetInitialID()
        {
            return DatabaseManager.ClassInfos.GetID(this);
        }
    }
}