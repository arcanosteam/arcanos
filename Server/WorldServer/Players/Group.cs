﻿using System;
using System.IO;
using Arcanos.Server.WorldServer.Chat;
using Arcanos.Server.WorldServer.Data;
using Arcanos.Server.WorldServer.Net;
using Arcanos.Server.WorldServer.Objects;
using Arcanos.Shared.Data;
using Arcanos.Shared.Net;
using Arcanos.Shared.Players;

namespace Arcanos.Server.WorldServer.Players
{
    [SerializationSettings(Method = SerializationMethod.RefByID, ReferenceResolverType = typeof(DatabaseManager), ReferenceResolverMethod = "GetGroup")]
    public class Group : GroupBase
    {
        private Group() { }

        public bool IsInGroup(PlayerCreature pc)
        {
            for (byte i = 0; i < MAX_GROUP_MEMBERS; ++i)
                if (Members[i] != null && Members[i].Creature == pc)
                    return true;
            return false;
        }
        public bool IsInGroup(Character character)
        {
            for (byte i = 0; i < MAX_GROUP_MEMBERS; ++i)
                if (Members[i] == character)
                    return true;
            return false;
        }

        public override bool AddMember(byte index, CharacterBase member)
        {
            bool result = base.AddMember(index, member);
            if (result)
            {
                (member as Character).Group = this;
                (member.Creature as PlayerCreature).SendPacketToVisible(new SCharacterChangedGroupPacket { GUID = member.Creature.GUID, GroupID = ID, Index = index });
                SendPacketToMembers(new SGroupMemberJoinedPacket { Index = index, MemberGUID = member.Creature.GUID });
                SendInitialData((member.Player as Player).Session);
                for (byte i = 0; i < MAX_GROUP_MEMBERS; ++i)
                    if (Members[i] != null && Members[i].Creature.Map == member.Creature.Map)
                        (member.Creature as PlayerCreature).AddVisibleObject(Members[i].Creature);
                ChatManager.Join(member.Player as Player, ChatManager.CHANNEL_GROUP);
                if (DatabaseManager.Groups.GetID(this) != -1)
                    DatabaseManager.Groups.Save(ID);
            }
            return result;
        }
        public override bool RemoveMember(byte index)
        {
            Character lastMember = Members[index] as Character;
            bool result = base.RemoveMember(index);
            if (result)
            {
                lastMember.Group = null;
                lastMember.Creature.SendPacketToVisible(new SCharacterChangedGroupPacket { GUID = lastMember.Creature.GUID, GroupID = 0 });
                SendPacketToMembers(new SGroupMemberLeftPacket { Index = index, MemberGUID = lastMember.Creature.GUID });
                ChatManager.Leave(lastMember.Player, ChatManager.CHANNEL_GROUP);
                if (MembersCount == 1)
                {
                    for (byte i = 0; i < MAX_GROUP_MEMBERS; ++i)
                        if (Members[i] != null)
                        {
                            lastMember = Members[i] as Character;
                            break;
                        }
                    lastMember.Group = null;
                    lastMember.Creature.SendPacketToVisible(new SCharacterChangedGroupPacket { GUID = lastMember.Creature.GUID, GroupID = 0 });
                    ChatManager.Leave(lastMember.Player, ChatManager.CHANNEL_GROUP);
                }
                if (DatabaseManager.Groups.GetID(this) != -1)
                    DatabaseManager.Groups.Save(ID);
            }
            return result;
        }
        public override void SetLeader(CharacterBase leader)
        {
            base.SetLeader(leader);
            SendPacketToMembers(new SGroupLeaderChangedPacket { LeaderGUID = Leader == null ? 0 : Leader.Creature.GUID });
            if (DatabaseManager.Groups.GetID(this) != -1)
                DatabaseManager.Groups.Save(ID);
        }

        protected override int GetInitialID()
        {
            return DatabaseManager.Groups.GetID(this);
        }
        public override byte[] Serialize(string member)
        {
            MemoryStream stream = null;
            switch (member)
            {
                case "Leader":
                    return BitConverter.GetBytes(Leader == null ? 0 : Leader.ID);
                case "Members":
                {
                    stream = new MemoryStream(MAX_GROUP_MEMBERS * 4);
                    BinaryWriter writer = new BinaryWriter(stream);
                    for (byte i = 0; i < MAX_GROUP_MEMBERS; ++i)
                        writer.Write(Members[i] == null ? 0 : Members[i].ID);
                    break;
                }
            }
            if (stream == null)
                return new byte[0];
            byte[] result = stream.GetBuffer();
            stream.Close();
            return result;
        }
        public override void Deserialize(string member, byte[] data)
        {
            MemoryStream stream = new MemoryStream(data);
            BinaryReader reader = new BinaryReader(stream);
            switch (member)
            {
                case "Leader":
                {
                    int id = reader.ReadInt32();
                    Leader = id == 0 ? null : DatabaseManager.Characters[id];
                    break;
                }
                case "Members":
                {
                    for (byte i = 0; i < MAX_GROUP_MEMBERS; ++i)
                    {
                        int id = reader.ReadInt32();
                        Members[i] = id == 0 ? null : DatabaseManager.Characters[id];
                    }
                    break;
                }
            }
            stream.Close();
        }

        public static Group MakeGroup(params CharacterBase[] members)
        {
            Group group = new Group();
            DatabaseManager.Groups.Add(group);
            for (int i = 0; i < members.Length; i++)
            {
                (members[i] as Character).LeaveGroup();
                group.AddMember(members[i]);
            }
            DatabaseManager.Groups.Save(group.ID);
            return group;
        }

        public void SendInitialData(PlayerSession session)
        {
            for (byte i = 0; i < MAX_GROUP_MEMBERS; ++i)
                if (Members[i] != null)
                    session.SendPacket(new SGroupMemberJoinedPacket { Initial = true, Index = i, MemberGUID = Members[i].Creature.GUID });
            session.SendPacket(new SGroupLeaderChangedPacket { Initial = true, LeaderGUID = Leader == null ? 0 : Leader.Creature.GUID });
        }

        public void SendPacketToMembers(IWorldPacket packet)
        {
            for (byte i = 0; i < MAX_GROUP_MEMBERS; ++i)
                if (Members[i] != null)
                    if (Members[i].Creature != null)
                        (Members[i].Creature as PlayerCreature).SendPacketToSelf(packet);
        }
    }
}