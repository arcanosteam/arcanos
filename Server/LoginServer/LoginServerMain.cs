﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using Arcanos.Server.LoginServer.Console;
using Arcanos.Server.LoginServer.Data;
using Arcanos.Server.LoginServer.Players;
using Arcanos.Shared.Data;
using Arcanos.Utilities;

namespace Arcanos.Server.LoginServer
{
    public class ArcanosLoginServer
    {
        public static string TokenPath = "Tokens\\";

        static void Main(string[] args)
        {
            SerializationManager.Initialize();
            Logger.Info(LogCategory.Status, "LoginServer started. Build {0}", Assembly.GetExecutingAssembly().GetName().Version.Build);
            Logger.Info(LogCategory.Status, new string('=', 80));
            {
                List<string> typeNames = new List<string>();
                foreach (Type type in Serialization.GetRegisteredTypes())
                    typeNames.Add(type.Name);
                Logger.Debug(LogCategory.General, "Serializable types: {0}", typeNames.Count);
                Logger.Debug(LogCategory.General, "{0}", string.Join(", ", typeNames.ToArray()));
                Logger.Debug(LogCategory.General, new string('=', 80));
            }
            PlayerAccount.PasswordHashFunction = password =>
            {
                byte[] md5 = MD5.Create().ComputeHash(Encoding.ASCII.GetBytes(password));
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < md5.Length; i++)
                    sb.Append(md5[i].ToString("X2"));
                return sb.ToString();
            };
            DatabaseManager.Connect();
            LoginManager.StartListening();
            System.Console.Beep();
            while (true)
            {
                string cmd = System.Console.ReadLine();
                if (cmd.ToLowerInvariant() == "exit")
                    break;
                Commands.Execute(cmd);
            }
            LoginManager.StopListening();
            LoginManager.DropAllConnections();
            DatabaseManager.Close();
            System.Console.Write("Press any key to continue...");
            System.Console.ReadKey(true);
        }
    }
}
