﻿using Arcanos.Server.LoginServer.Players;
using Arcanos.Server.LoginServer.Server;
using Arcanos.Shared.Data;
using Arcanos.Utilities;

namespace Arcanos.Server.LoginServer.Data
{
    public static class SerializationManager
    {
        public static void Initialize()
        {
            Serialization.Register(typeof(PlayerAccount));
            Serialization.Register(typeof(WorldServerEntry));

            Serialization.outputLock = Logger.outputLock;
        }
    }
}