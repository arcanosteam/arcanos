﻿using Arcanos.Server.LoginServer.Players;
using Arcanos.Server.LoginServer.Server;
using Arcanos.Shared.Data;

namespace Arcanos.Server.LoginServer.Data
{
    public static class DatabaseManager
    {
        public static readonly Database<PlayerAccount> PlayerAccounts = new FileDatabase<PlayerAccount>("playeraccounts.serverdb", "PLAC".ToCharArray()) { UploadChanges = true, UploadChangesImmediately = true };
        public static readonly Database<WorldServerEntry> WorldServers = new FileDatabase<WorldServerEntry>("worldservers.serverdb", "WRLS".ToCharArray()) { UploadChanges = true, UploadChangesImmediately = true };

        public static void Connect()
        {
            PlayerAccounts.Connect();
            WorldServers.Connect();
        }
        public static void Close()
        {
            PlayerAccounts.Close();
            WorldServers.Close();
        }
    }
}