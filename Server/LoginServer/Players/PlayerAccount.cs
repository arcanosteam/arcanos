﻿using System;
using Arcanos.Server.LoginServer.Data;
using Arcanos.Shared;

namespace Arcanos.Server.LoginServer.Players
{
    public class PlayerAccount : Template
    {
        public override EditorEntry EditorEntry
        {
            get { return new EditorEntry(ID, Name); }
        }

        public static Func<string, string> PasswordHashFunction;

        public string Name;
        public string PasswordHash;

        protected PlayerAccount() { }
        public PlayerAccount(string name, string password)
        {
            Name = name;
            PasswordHash = PasswordHashFunction(password);
        }

        protected override int GetInitialID()
        {
            return DatabaseManager.PlayerAccounts.GetID(this);
        }
    }
}