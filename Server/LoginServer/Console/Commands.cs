﻿using System;
using Arcanos.Server.LoginServer.Data;
using Arcanos.Server.LoginServer.Players;
using Arcanos.Server.LoginServer.Server;
using Arcanos.Utilities.Console;

namespace Arcanos.Server.LoginServer.Console
{
    public static class Commands
    {
        public const char SEPARATOR = ' ';
        public static readonly CommandGroup InitialGroup;

        static Commands()
        {
            Command.Separator = SEPARATOR;
            Command.InfoHandler = OutputInfo;
            Command.ErrorHandler = OutputError;
            CommandGroup.Separator = SEPARATOR;
            CommandGroup.InfoHandler = OutputInfo;
            CommandGroup.ErrorHandler = OutputError;

            InitialGroup = new CommandGroup(null)
            {
                new Command("exit", "exit", args => true),
                new CommandGroup("account")
                {
                    new Command("create", "account create <username> <password>", args =>
                    {
                        if (DatabaseManager.PlayerAccounts.Find(p => p.Name.ToLower() == args[0].ToLower()) != null)
                            return OutputError("Player with a username \"{0}\" already exists", args[0]);
                        PlayerAccount account = new PlayerAccount(args[0], args[1]);
                        int id = DatabaseManager.PlayerAccounts.Add(account);
                        OutputInfo("ID: {0}", id);
                        return true;
                    }),
                },
                new CommandGroup("server")
                {
                    new Command("add", "server add <name> <url>", args =>
                    {
                        if (DatabaseManager.WorldServers.Find(p => p.Name == args[0]) != null)
                            return OutputError("Server with a name \"{0}\" already exists", args[0]);
                        if (DatabaseManager.WorldServers.Find(p => p.URL == args[1]) != null)
                            return OutputError("Server with a url \"{0}\" already exists", args[1]);
                        WorldServerEntry entry = new WorldServerEntry(args[0], args[1]);
                        int id = DatabaseManager.WorldServers.Add(entry);
                        OutputInfo("ID: {0}", id);
                        return true;
                    }),
                },
            };
        }

        public static void Execute(string cmd)
        {
            InitialGroup.Execute(cmd);
        }

        private static bool TryParseEnum<T>(string value, out T result)
        {
            int index;
            if (int.TryParse(value, out index))
            {
                result = (T)Enum.ToObject(typeof(T), index);
                return true;
            }

            string[] names = Enum.GetNames(typeof(T));
            T[] values = (T[])Enum.GetValues(typeof(T));
            int max = value.Length;
            for (int i = 0; i < max; i++)
                if (names[i].ToLowerInvariant() == value.ToLowerInvariant())
                {
                    result = values[i];
                    return true;
                }

            result = default(T);
            return false;
        }

        public static void OutputInfo(string format, params object[] args)
        {
            if (args == null || args.Length == 0)
                System.Console.WriteLine(format);
            else
                System.Console.WriteLine(format, args);
        }
        public static bool OutputError(string format, params object[] args)
        {
            if (args == null || args.Length == 0)
                System.Console.WriteLine("Error: " + format);
            else
                System.Console.WriteLine("Error: " + string.Format(format, args));
            return false;
        }
    }
}