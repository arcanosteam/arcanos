﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Arcanos.Net;
using Arcanos.Server.LoginServer;
using Arcanos.Server.LoginServer.Data;
using Arcanos.Server.LoginServer.Players;
using Arcanos.Server.LoginServer.Server;
using Arcanos.Shared.Net;
using Arcanos.Shared.Server;
using Arcanos.Utilities;

namespace Arcanos.Server
{
    public static class LoginManager
    {
        public static TcpListener ConnectionListener;
        public static List<LoginSession> ConnectedSessions = new List<LoginSession>();
        private static readonly Random random = new Random();

        public static void StartListening()
        {
            ConnectionListener = new TcpListener(IPAddress.Any, LoginConstants.PORT);
            ConnectionListener.Start();
            ConnectionListener.BeginAcceptTcpClient(ConnectionAccepted, null);
        }
        public static void StopListening()
        {
            ConnectionListener.Stop();
        }
        public static void DropAllConnections()
        {
            foreach (LoginSession session in ConnectedSessions)
            {
                session.Connection.Stop();
                session.Connection.Client.Close();
            }
        }

        public static void SendSystemMessage(string text)
        {
            foreach (LoginSession session in ConnectedSessions)
                session.SendSystemMessage(text);
        }

        private static void ConnectionAccepted(IAsyncResult iar)
        {
            TcpClient client = ConnectionListener.EndAcceptTcpClient(iar);
            ConnectionListener.BeginAcceptTcpClient(ConnectionAccepted, null);

            ConnectionManager<LoginPackets, ILoginPacket> connection = new ConnectionManager<LoginPackets, ILoginPacket>(client);
            connection.PacketReceived += (type, packet, rawData) => Logger.Trace(LogCategory.Packets, "Recv {0}: {1}", type, Logger.Unwrap(packet));
            connection.PacketSent += (type, packet, rawData) => Logger.Trace(LogCategory.Packets, "Sent {0}: {1}", type, Logger.Unwrap(packet));
            LoginPacketsTable.Fill(connection.Reader.PacketTypes);
            LoginPacketsTable.Fill(connection.Writer.PacketTypes);

            connection.AddReadHandler(LoginPackets.CLoginProtocolQuery, (sPacket, rawData) =>
                new SLoginProtocolResponsePacket
                {
                    ProtocolVersion = LoginConstants.PROTOCOL_VERSION,
                });
            connection.AddReadHandler(LoginPackets.CLoginLoginRequest, (MultipleRespondedPacketHandler<ILoginPacket>)LoginRequestHandler);
            connection.Start();

            ConnectedSessions.Add(new LoginSession(connection));
        }

        private static IEnumerable<ILoginPacket> LoginRequestHandler(ILoginPacket sPacket, byte[] rawData)
        {
            CLoginLoginRequestPacket packet = (CLoginLoginRequestPacket)sPacket;
            Logger.Debug(LogCategory.Connection, "Login attempt by {0}@{1}", packet.UsernameOrEmail, packet.Password);
            PlayerAccount account = DatabaseManager.PlayerAccounts.Find(a => a.Name.ToLowerInvariant() == packet.UsernameOrEmail.ToLowerInvariant());
            if (account == null)
            {
                yield return new SLoginLoginResultPacket
                {
                    LoginToken = new byte[16],
                    Result = LoginResult.UsernameNotFound,
                };
                yield break;
            }
            if (account.PasswordHash != PlayerAccount.PasswordHashFunction(packet.Password))
            {
                yield return new SLoginLoginResultPacket
                {
                    LoginToken = new byte[16],
                    Result = LoginResult.IncorrectPassword,
                };
                yield break;
            }
            while (true)
            {
                byte[] token = new byte[16];
                StringBuilder sb = new StringBuilder(16 * 2);
                for (int i = 0; i < 16; i++)
                {
                    byte b = (byte)random.Next(256);
                    token[i] = b;
                    sb.Append(b.ToString("X2"));
                }
                string filename = Path.Combine(ArcanosLoginServer.TokenPath, sb.ToString());
                if (File.Exists(filename))
                    continue;
                StreamWriter sw = new StreamWriter(filename, false, Encoding.UTF8, 64);
                sw.Write(account.Name);
                sw.Close();
                Logger.Info(LogCategory.General, "Successful");
                yield return new SLoginLoginResultPacket
                {
                    LoginToken = token,
                    Result = LoginResult.Ok,
                };
                foreach (KeyValuePair<int, WorldServerEntry> entry in DatabaseManager.WorldServers)
                    yield return new SWorldServerEntryPacket
                    {
                        ID = entry.Key,
                        ServerName = entry.Value.Name,
                        URL = entry.Value.URL,
                    };
                break;
            }
        }
    }
}