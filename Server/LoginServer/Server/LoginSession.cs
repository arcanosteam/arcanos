﻿using System;
using Arcanos.Net;
using Arcanos.Shared.Net;

namespace Arcanos.Server
{
    public class LoginSession
    {
        public ConnectionManager<LoginPackets, ILoginPacket> Connection;

        public LoginSession(ConnectionManager<LoginPackets, ILoginPacket> connection)
        {
            Connection = connection;
        }

        public void SendSystemMessage(string text)
        {
            Connection.Write(new SLoginSystemMessagePacket
            {
                Time = DateTime.Now,
                Text = text,
            });
        }
    }
}